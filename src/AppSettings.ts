export class AppSettings {

    public static TABLE_PROPOSAL_ENTRY_CARDINALITY = 20;
    public static TABLE_PROPOSAL_ENTRY_HEADER = ["ASSET", "PROPOSTA", "CONTRATTO", "SOGGETTO", "STATO",
        "DATA", "SCADENZA", "PRIORITA'", "VENDITORE"];

    public static GRID_AUTO_CARDINALITY = 18;

    public static FILTER_KEY_MATCH_LIKE = 'like';
    public static FILTER_KEY_MATCH_EXACT = 'exact';
    public static FILTER_KEY_MATCH_CHOICE = 'choice';

    public static TTL_PROPOSAL = 8 * 60 * 60 * 1000; // 8 hours in millis

    public static ND_FIELDS_VALUE = 'N/D';
    public static ND_VERSION_DATE_VALUE = 'In Corso';


    /**
     * PAGEABLE
     */

    public static START2PAGE = 0;
    public static ELEMENT2PAGE = 100;
    public static PROPOSALS2PAGE = 10;

    public static scrollStyle = 'minimal-dark';


    /**
     * CONF SPIN
     */

    public static TIMEOUT_SPINNER = 250;


    /**
     * HTTP
     */

    public static HTTP_TIMEOUT_CALLS =  120000;


    /**
     * OK
     */

    public static HTTP_OK = 200;
    public static HTTP_OK_BLOCK = 211;

    /**
     * KO
     */

    public static HTTP_FOUND = 302;
    public static HTTP_BROWSER_NONE = 0;
    public static HTTP_FOUND_MESSAGE: string = 'Http failure response for (unknown url): 0 Unknown Error';


    public static HTTP_KO = 400;
    public static HTTP_ERROR = 500;

    public static AUTH_BDWEB = "BDWEB";
    public static AUTH_BL = "BL";


    /**
     * Profiles KEY
     */

    public static P_FUNCTIONALY = 'bl_user_functionality';


    /**
     * FeelNew default value
     */
    public static DEFAULT_FIRE_COVERAGE = 'FS00';
    public static DEFAULT_RTI_COVERAGE = 'RTI00';


}