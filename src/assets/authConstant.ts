export class AuthConstant {


    /**
     * AUTH BDWEB
     */
    private static NAN = NaN;
    public static DEALER_PREFERENCE = "DEALER_PREFERENCE";
    public static DEALER_PREFERENCE_ESTRATTO_CONTO = "DEALER_PREFERENCE_ESTRATTO_CONTO";
    public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    public static PROP_CONSULTAZIONE_PERSONALI = "PROP_CONSULTAZIONE_PERSONALI";
    public static PROP_CONSULTAZIONE_DEALER_APP = "PROP_CONSULTAZIONE_DEALER_APP";
    public static PROP_CONSULTAZIONE_PROFILO_UTENTE = "PROP_CONSULTAZIONE_PROFILO_UTENTE";
    public static PROP_CONSULTAZIONE_PROFILO_GLOBAL = "PROP_CONSULTAZIONE_PROFILO_GLOBAL";
    public static PROP_CONF_COMMERCIALE = "PROP_CONF_COMMERCIALE";
    public static PROP_CONF_COMMERCIALE_FORNITORE_ESTERNO = "PROP_CONF_COMMERCIALE_FORNITORE_ESTERNO";
    public static PROP_CONF_COMMERCIALE_TERZE_PARTI = "PROP_CONF_COMMERCIALE_TERZE_PARTI";
    public static PROP_CONF_COMMERCIALE_VAL_USATO = "PROP_CONF_COMMERCIALE_VAL_USATO";
    public static PROP_CONF_FINANZIARIA = "PROP_CONF_FINANZIARIA";
    public static PROP_CONF_FIN_DEROGA_SERVIZI = "PROP_CONF_FIN_DEROGA_SERVIZI";
    public static PROP_CONF_FIN_DEROGA_COMMERCIALE = "PROP_CONF_FIN_DEROGA_COMMERCIALE";
    public static PROP_CONF_FIN_PROVVIGIONI = "PROP_CONF_FIN_PROVVIGIONI";
    public static PROP_CONF_FIN_PATTO_RIACQUISTO = "PROP_CONF_FIN_PATTO_RIACQUISTO";
    public static PROP_STAMPA_FRONTALINO = "PROP_STAMPA_FRONTALINO";
    public static PROP_STAMPA_SIMULAZIONE = "PROP_STAMPA_SIMULAZIONE";
    public static PROP_OFFERTA_DEALER = "PROP_OFFERTA_DEALER";
    public static PROP_STAMPA_PROPOSTA = "PROP_STAMPA_PROPOSTA";
    public static PROP_ANAGRAFICA = "PROP_ANAGRAFICA";
    public static PROP_ANAGRAFICA_OMOCODIA = "PROP_ANAGRAFICA_OMOCODIA";
    public static PROP_ISTRUTTORIA = "PROP_ISTRUTTORIA";
    public static PROP_ISTRUTTORIA_INVIO_ISTRUTTORIA = "PROP_ISTRUTTORIA";
    public static PROP_NUMERAZIONE = "PROP_NUMERAZIONE";
    public static PROP_NUMERAZIONE_RICHIESTA_NUMERAZIONE = "PROP_NUMERAZIONE_RICHIESTA_NUMERAZIONE";
    public static PROP_FIRMA_CONTRATTO = "PROP_FIRMA_CONTRATTO";
    public static PROP_FIRMA_CONTRATTO_STAMPA = "PROP_FIRMA_CONTRATTO_STAMPA";
    public static PROP_ATTIVAZIONE = "PROP_ATTIVAZIONE";
    public static PROP_ATTIVAZIONE_ABILITA_ATTIVAZIONE = "PROP_ATTIVAZIONE_ABILITA_ATTIVAZIONE";
    public static PROP_RIF_CONF_COMMERCIALE = "PROP_RIF_CONF_COMMERCIALE";
    public static PROP_RIL_CONF_COMMERCIALE = "PROP_RIL_CONF_COMMERCIALE";
    public static PROP_NOLEGGIO = "PROP_NOLEGGIO";
    public static PROP_NOTE_GESTIONE = "PROP_NOTE_GESTIONE";
    public static PROP_DOC_GESTIONE = "PROP_DOC_GESTIONE";
    public static PROP_CANCELLA = "PROP_CANCELLA";
    public static PROP_DELTA_MOD_ISTR = "PROP_DELTA_MOD_ISTR";
    public static PROP_RESET = "PROP_RESET";

    /**
     * AUTH BL
     */

    public static FUNC_IMPERSONATE = "IMPERSONIFICA";

    /**
     * Consulta storico proposte BL
     */
    public static FUNC_HISTORY_CONSULT_PROPS = "CONSULTA_PROP";




    //REPARTO CREDITO RETAIL
    //Collections Retail
    public static CODA_CREDITO_RETAIL = "CODA_CREDITO_RETAIL";
    public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE"; // gia definito
    public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";

    //Analista_Retail_Credit
    // public static CODA_CREDITO_RETAIL = "CODA_CREDITO_RETAIL"; // gia definito
    public static PROP_CREDITO_RETAIL = "PROP_CREDITO_RETAIL";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG"; // gia definito
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE"; // gia definito
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI"; // gia definito
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE"; // gia definito

    //Coordinatore_Retail_Credit
    // public static CODA_CREDITO_RETAIL = "CODA_CREDITO_RETAIL";
    // public static PROP_CREDITO_RETAIL = "PROP_CREDITO_RETAIL";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";

    //Responsabile_Retail_Credit
    // public static CODA_CREDITO_RETAIL = "CODA_CREDITO_RETAIL";
    // public static PROP_CREDITO_RETAIL = "PROP_CREDITO_RETAIL";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";

    //REPARTO CREDITO CORPORATE
    //Collections Corporate
    public static CODA_CREDITO_CORPORATE = "CODA_CREDITO_CORPORATE";//TODO: da notificare il backend che devono cambiare tutte le altre "CODA_CREDITO_CORPORATE";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";

    //Analista_Corporate_Credit_EMPLOYEE
    // public static CODA_CREDITO_CORPORATE = "CODA_CREDITO_CORPORATE";
    public static PROP_CREDITO_CORPORATE = "PROP_CREDITO_CORPORATE";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";

    //Responsabile_Corporate_Credit_RESPONSABILE
    // public static CODA_CREDITO_CORPORATE = "CODA_CREDITO_CORPORATE";
    // public static PROP_CREDITO_CORPORATE = "PROP_CREDITO_CORPORATE";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";

    //SUPERVISIONE AREA CREDITO
    // Direttore_Credit_Operations   (necessario proporre soluzione FE per gestire l’accesso alla specifica coda)
    // public static CODA_CREDITO_RETAIL = "CODA_CREDITO_RETAIL";
    // public static CODA_CREDITO_CORPORATE = "CODA_CREDITO_CORPORATE";
    // public static PROP_CREDITO_RETAIL = "PROP_CREDITO_RETAIL";
    // public static PROP_CREDITO_CORPORATE = "PROP_CREDITO_CORPORATE";
    // public static PROP_BANCHE_DATI_RISK_MNG = "PROP_BANCHE_DATI_RISK_MNG";
    // public static PROP_CONSULTAZIONE = "PROP_CONSULTAZIONE";
    // public static PROP_CONSULTAZIONE_DOCUMENTI = "PROP_CONSULTAZIONE_DOCUMENTI";
    // public static PROP_CONSULTAZIONE_NOTE = "PROP_CONSULTAZIONE_NOTE";
    public static CODA_DEALER_CONTACT = "FUNC_CODA_DEALER_CONTACT";
    public static CODA_PAYOUT = "FUNC_CODA_PAYOUT";


    public static authConstantKeys = {
        dealer_preference: {
            abilitaEstrattoConto: {
                si: AuthConstant.DEALER_PREFERENCE_ESTRATTO_CONTO,
                no: AuthConstant.NAN
            }
        },
        prop_consultazione: {
            scope: {
                personali: AuthConstant.PROP_CONSULTAZIONE_PERSONALI,
                dealer_app: AuthConstant.PROP_CONSULTAZIONE_DEALER_APP,
                profilo_utente: AuthConstant.PROP_CONSULTAZIONE_PROFILO_UTENTE,
                global: AuthConstant.PROP_CONSULTAZIONE_PROFILO_GLOBAL
            }
        },
        prop_conf_commerciale: {
            abilitaFornitoreEsterno: {
                si: AuthConstant.PROP_CONF_COMMERCIALE_FORNITORE_ESTERNO,
                no: AuthConstant.NAN
            },
            abilitaAllestimentoTerzeParti: {
                si: AuthConstant.PROP_CONF_COMMERCIALE_TERZE_PARTI,
                no: AuthConstant.NAN
            }
        },
        prop_anagrafica: {
            abilitaOmocodia: {
                si: AuthConstant.PROP_ANAGRAFICA_OMOCODIA,
                no: AuthConstant.NAN
            }
        },
        prop_istruttoria: {
            abilitaInvioIstruttoria: {
                si: AuthConstant.PROP_ISTRUTTORIA_INVIO_ISTRUTTORIA,
                no: AuthConstant.NAN
            }
        },

        prop_numerazione: {
            abilitaRichiestaNumerazione: {
                si: AuthConstant.PROP_NUMERAZIONE_RICHIESTA_NUMERAZIONE,
                no: AuthConstant.NAN
            }
        },

        prop_firma_contratto: {
            abilitaStampaContratto: {
                si: AuthConstant.PROP_FIRMA_CONTRATTO_STAMPA,
                no: AuthConstant.NAN
            }
        },

        prop_attivazione: {
            abilitaAttivazione: {
                si: AuthConstant.PROP_ATTIVAZIONE_ABILITA_ATTIVAZIONE,
                no: AuthConstant.NAN
            }
        }
    };



    public static FUNCTIONALITY_STARTER_MAP: Map<string, boolean> = new Map([

        [AuthConstant.FUNC_IMPERSONATE, false],
        [AuthConstant.CODA_CREDITO_RETAIL, false],
        [AuthConstant.PROP_BANCHE_DATI_RISK_MNG, false],
        [AuthConstant.PROP_CONSULTAZIONE_DOCUMENTI, false],
        [AuthConstant.PROP_CONSULTAZIONE_NOTE, false],
        [AuthConstant.PROP_CREDITO_RETAIL, false],
        [AuthConstant.CODA_CREDITO_CORPORATE, false],
        [AuthConstant.PROP_CREDITO_CORPORATE, false],
        [AuthConstant.CODA_DEALER_CONTACT, false],
        [AuthConstant.CODA_PAYOUT, false],

        [AuthConstant.DEALER_PREFERENCE, false],
        [AuthConstant.DEALER_PREFERENCE_ESTRATTO_CONTO, false],
        [AuthConstant.PROP_CONSULTAZIONE, false],
        [AuthConstant.PROP_CONSULTAZIONE_PERSONALI, false],
        [AuthConstant.PROP_CONSULTAZIONE_DEALER_APP, false],
        [AuthConstant.PROP_CONSULTAZIONE_PROFILO_UTENTE, false],
        [AuthConstant.PROP_CONSULTAZIONE_PROFILO_GLOBAL, false],
        [AuthConstant.PROP_CONF_COMMERCIALE, false],
        [AuthConstant.PROP_CONF_COMMERCIALE_FORNITORE_ESTERNO, false],
        [AuthConstant.PROP_CONF_COMMERCIALE_TERZE_PARTI, false],
        [AuthConstant.PROP_CONF_COMMERCIALE_VAL_USATO, false],
        [AuthConstant.PROP_CONF_FINANZIARIA, false],
        [AuthConstant.PROP_CONF_FIN_DEROGA_SERVIZI, false],
        [AuthConstant.PROP_CONF_FIN_DEROGA_COMMERCIALE, false],
        [AuthConstant.PROP_CONF_FIN_PROVVIGIONI, false],
        [AuthConstant.PROP_CONF_FIN_PATTO_RIACQUISTO, false],
        [AuthConstant.PROP_STAMPA_FRONTALINO, false],
        [AuthConstant.PROP_STAMPA_SIMULAZIONE, false],
        [AuthConstant.PROP_OFFERTA_DEALER, false],
        [AuthConstant.PROP_STAMPA_PROPOSTA, false],
        [AuthConstant.PROP_ANAGRAFICA, false],
        [AuthConstant.PROP_ANAGRAFICA_OMOCODIA, false],
        [AuthConstant.PROP_ISTRUTTORIA, false],
        [AuthConstant.PROP_ISTRUTTORIA_INVIO_ISTRUTTORIA, false],
        [AuthConstant.PROP_NUMERAZIONE, false],
        [AuthConstant.PROP_NUMERAZIONE_RICHIESTA_NUMERAZIONE, false],
        [AuthConstant.PROP_FIRMA_CONTRATTO, false],
        [AuthConstant.PROP_FIRMA_CONTRATTO_STAMPA, false],
        [AuthConstant.PROP_ATTIVAZIONE, false],
        [AuthConstant.PROP_ATTIVAZIONE_ABILITA_ATTIVAZIONE, false],
        [AuthConstant.PROP_RIF_CONF_COMMERCIALE, false],
        [AuthConstant.PROP_RIL_CONF_COMMERCIALE, false],
        [AuthConstant.PROP_NOLEGGIO, false],
        [AuthConstant.PROP_NOTE_GESTIONE, false],
        [AuthConstant.PROP_DOC_GESTIONE, false],
        [AuthConstant.PROP_CANCELLA, false],
        [AuthConstant.PROP_DELTA_MOD_ISTR, false],
        [AuthConstant.PROP_RESET, false]
    ]);
}