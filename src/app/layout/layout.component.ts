import {AfterContentInit, AfterViewChecked, Component, OnInit} from '@angular/core';
import {UtilsService} from "../shared/services/utils.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewChecked {


    constructor(private _translate: TranslateService) {

    }

    ngOnInit() {

        // setTimeout(()=> {
        //     UtilsService.translationOptions(this._translate);}, 1000);

    }


    ngAfterViewChecked(): void {
        UtilsService.translationOptions(this._translate);

    }

}
