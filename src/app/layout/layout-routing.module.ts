import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LayoutComponent} from "./layout.component";
import {ProposalsModule} from "../proposals/proposals.module";

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: 'layout', redirectTo: 'proposals', pathMatch: 'prefix' },
            { path: 'proposals', loadChildren: '../proposals/proposals.module#ProposalsModule' }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
