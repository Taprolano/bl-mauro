import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import {SharedModule} from "../shared/shared.module";
import {ProposalsModule} from "../proposals/proposals.module";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    SharedModule,
    ProposalsModule,
    TranslateModule
  ],
  exports: [LayoutComponent]
})
export class LayoutModule { }
