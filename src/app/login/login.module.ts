import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
//import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
       CommonModule,
       TranslateModule,
       LoginRoutingModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
