import { animate, state, style, transition, trigger } from '@angular/animations';

export function routerTransition() {

    return noTransition();
}

export function noTransition() {
    return trigger('routerTransition', []);
}

export function slideToRight() {
    slideHorizontal(true)
}

export function slideToLeft() {
    slideHorizontal(false);
}

export function slideToBottom() {
    slideVertical(false);
}

export function slideToTop() {
    slideVertical(true);
}

function slideHorizontal(right: boolean) {
    return trigger('routerTransition', [
        state('void', style({})),
        state('*', style({})),
        transition(':enter', [
            style({ transform: right ? 'translateX(-100%)' : 'translateX(100%)'}),
            animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
        ]),
        transition(':leave', [
            style({ transform: 'translateX(0%)' }),
            animate('0.5s ease-in-out', style({ transform: right ? 'translateX(100%)' : 'translateX(-100%)' }))
        ])
    ]);
}

function slideVertical(top: boolean) {
    return trigger('routerTransition', [
        state('void', style({})),
        state('*', style({})),
        transition(':enter', [
            style({ transform: top ? 'translateY(100%)' : 'translateY(-100%)' }),
            animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
        ]),
        transition(':leave', [
            style({ transform: 'translateY(0%)' }),
            animate('0.5s ease-in-out', style({ transform: top ? 'translateY(-100%)' : 'translateY(100%)' }))
        ])
    ]);
}