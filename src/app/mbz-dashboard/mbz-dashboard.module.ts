import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbzDashboardComponent } from './mbz-dashboard/mbz-dashboard.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MbzDashboardComponent],
  exports:[MbzDashboardComponent]
})
export class MbzDashboardModule { }
