import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MbzDashboardModule } from "../mbz-dashboard.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { environment } from 'src/environments/environment';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProposalsModule } from 'src/app/proposals/proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { createTranslateLoader } from 'src/app/app.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from 'src/app/app.component';
import { AuthGuard } from 'src/app/shared';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MbzDashboardComponent } from './mbz-dashboard.component';
import {DirectAccessGuard} from "../../shared/guard/direct-access/direct-access.guard";



describe('MbzDashboardComponent', () => {
  let component: MbzDashboardComponent;
  let fixture: ComponentFixture<MbzDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        TranslateModule,
        HttpClientModule,
        MbzDashboardModule,
        ProposalsModule,
        NgxSpinnerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, DirectAccessGuard],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MbzDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
