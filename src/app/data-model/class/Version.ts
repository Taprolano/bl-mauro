import {Normalizer} from "./Normalizer";

export class Version extends Normalizer {

    private _baumuster: string;
    private _modelDescription: string;
    private _netCost?: number;
    private _pricelistDeadline?: string;
    private _fuelTypeCode?: string;
    private _forceSecondHand?: boolean;


    constructor(baumuster: string, modelDescription: string) {
        super();
        this._baumuster = baumuster;
        this._modelDescription = modelDescription;
        this.forceSecondHand = false;
    }

    get fuelTypeCode(): string {
        return this._fuelTypeCode;
    }

    set fuelTypeCode(value: string) {
        this._fuelTypeCode = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get modelDescription(): string {
        return this._modelDescription;
    }

    set modelDescription(value: string) {
        this._modelDescription = value;
    }

    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    get pricelistDeadline(): string {
        return this._pricelistDeadline;
    }

    set pricelistDeadline(value: string) {
        this._pricelistDeadline = value;
    }


    get forceSecondHand(): boolean {
        return this._forceSecondHand;
    }

    set forceSecondHand(value: boolean) {
        this._forceSecondHand = value;
    }
}