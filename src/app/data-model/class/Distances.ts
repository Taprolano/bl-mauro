export class Distances {
    private _annualDistance: number;
    private _costPerUnit: number;
    private _supplyCostUnit: number;
    private _compCommSup: number;


    constructor(annualDistance?: number, costPerUnit?: number, supplyCostUnit?: number, compCommSup?: number) {
        this._annualDistance = annualDistance;
        this._costPerUnit = costPerUnit;
        this._supplyCostUnit = supplyCostUnit;
        this._compCommSup = compCommSup;
    }


    set annualDistance(value: number) {
        this._annualDistance = value;
    }

    set costPerUnit(value: number) {
        this._costPerUnit = value;
    }

    set supplyCostUnit(value: number) {
        this._supplyCostUnit = value;
    }

    set compCommSup(value: number) {
        this._compCommSup = value;
    }

    get annualDistance(): number {
        return this._annualDistance;
    }

    get costPerUnit(): number {
        return this._costPerUnit;
    }

    get supplyCostUnit(): number {
        return this._supplyCostUnit;
    }

    get compCommSup(): number {
        return this._compCommSup;
    }
}

export class DistancesFactory {

    static getInstance(obj: Distances): Distances {
        let d = new Distances();

        if (obj) {
            if (obj.annualDistance != undefined) d.annualDistance = obj.annualDistance;
            if (obj.compCommSup != undefined) d.compCommSup = obj.compCommSup;
            if (obj.supplyCostUnit != undefined) d.supplyCostUnit = obj.supplyCostUnit;
            if (obj.costPerUnit != undefined) d.costPerUnit = obj.costPerUnit;
        }

        return d;
    }


    static getInstanceList(list: any): Distances[] {
        let dList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                dList.push(DistancesFactory.getInstance(elem));
            });
        }
        return dList;
    }
}