

export class ImpUser {


    private _username: string;
    private _mail: string;
    private _role: string;

    private _name: string;
    private _surname: string;


    constructor(username: string, mail: string, role: string, name: string, cognome: string) {
        this._username = username;
        this._mail = mail;
        this._role = role;
        this._name = name;
        this._surname = cognome;
    }

    /**@ignore nomi delle chiavi del backend, per tab relativo - devono corrispondere ai getter (autogenerare dalle var). Da aggiornare se variano. */
    static readonly keys = ["name", "surname", "username", "mail", "role" ];




    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get mail(): string {
        return this._mail;
    }

    set mail(value: string) {
        this._mail = value;
    }

    get role(): string {
        return this._role;
    }

    set role(value: string) {
        this._role = value;
    }


    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get surname(): string {
        return this._surname;
    }

    set surname(value: string) {
        this._surname = value;
    }
}