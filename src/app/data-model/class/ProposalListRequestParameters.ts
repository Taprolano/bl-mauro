import {HttpParams} from "@angular/common/http";
import {UtilsService} from '../../shared/services/utils.service';
import {IProposalRequest} from "../interface/IProposalRequest";
import {LoggerService} from '../../shared/services/logger.service';

export class ProposalListRequestParameters {

    private _attivataA: string;
    private _attivataDa: string;
    private _numeroContratto: string;
    private _numeroProposta: string;
    private _soggetto: string;
    private _stato: number[];
    private _targa: string;
    private _telaio: string;
    private _utils: UtilsService = new UtilsService();
    private _page: number;
    private _size: number;
    private _sort: string; //"[campo],[asc/desc]"

    constructor(private _logger: LoggerService) {
    }

    public get attivataA(): string {
        return this._attivataA;
    }

    public set attivataA(value: string) {
        this._attivataA = value;
    }

    public get attivataDa(): string {
        return this._attivataDa;
    }

    public set attivataDa(value: string) {
        this._attivataDa = value;
    }

    public get numeroContratto(): string {
        return this._numeroContratto;
    }

    public set numeroContratto(value: string) {
        this._numeroContratto = value;
    }

    public get numeroProposta(): string {
        return this._numeroProposta;
    }

    public set numeroProposta(value: string) {
        this._numeroProposta = value;
    }

    public get soggetto(): string {
        return this._soggetto;
    }

    public set soggetto(value: string) {
        this._soggetto = value;
    }

    public get stato(): number[] {
        return this._stato;
    }

    public set stato(value: number[]) {
        this._stato = value;
    }

    public get targa(): string {
        return this._targa;
    }

    public set targa(value: string) {
        this._targa = value;
    }

    public get telaio(): string {
        return this._telaio;
    }

    public set telaio(value: string) {
        this._telaio = value;
    }

    public get page(): number {
        return this._page;
    }

    public set page(value: number) {
        this._page = value;
    }

    public get size(): number {
        return this._size;
    }

    public set size(value: number) {
        this._size = value;
    }
    //param per sorting
    public get sort(): string {
        return this._sort;
    }
    public set sort(value: string) {
        this._sort = value;
    }

    setRequestParametersFromInterface(source: IProposalRequest) {
        this._attivataA = source.activationDateTo;
        this._attivataDa = source.activationDateFrom;
        this._numeroContratto = source.contractNumber;
        this._numeroProposta = source.proposalNumber;
        this._soggetto = source.applicant;
        this._targa = source.licensePlate;
        this._telaio = source.frameNumber;
        this._stato = source.proposalState;
    }

    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        if (!this._utils.isVoid(this._attivataA)) params = params.set('attivataA', this._attivataA);
        if (!this._utils.isVoid(this._attivataDa)) params = params.set('attivataDa', this._attivataDa);
        if (!this._utils.isVoid(this._numeroContratto)) params = params.set('numeroContratto', this._numeroContratto);
        if (!this._utils.isVoid(this._numeroProposta)) params = params.set('numeroProposta', this._numeroProposta);
        if (!this._utils.isVoid(this._soggetto)) params = params.set('soggetto', this._soggetto);
        if (!this._utils.isVoid(this._stato)) params = params.set('stato', this._stato.toString());
        if (!this._utils.isVoid(this._targa)) params = params.set('targa', this._targa);
        if (!this._utils.isVoid(this._telaio)) params = params.set('telaio', this._telaio);
        if (!this._utils.isVoid(this._page)) params = params.set('page', this._page.toString());
        if (!this._utils.isVoid(this._size)) params = params.set('size', this._size.toString());
        if (!this._utils.isVoid(this._sort)) params = params.set('sort', this._sort);
        this._logger.logInfo('ProposalListRequestParameters', 'requestParameters', params);
        //console.log("PARAMS:",params);  //riceve correttamente il sorting
        return params;
    }
}