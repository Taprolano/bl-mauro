export class PayoutQueue {

  private _contratto: string;
  private _blocked: boolean;
  private _processInstanceId: number;
  private _tipoProposta: string;
  private _causaleSospensione: string;
  private _numBlocchi: number;
  private _arrivo: string;
  private _prodotto: string;
  private _richiedente: string;
  private _numNote: number;
  private _numDocMancanti: number;
  private _sospesaDa: string;

  constructor(contratto: string,
    blocked: boolean,
    processInstanceId: number,
    tipoProposta: string,
    causaleSospensione: string,
    numBlocchi: number,
    arrivo: string,
    prodotto: string,
    richiedente: string,
    numNote: number,
    numDocMancanti: number,
    sospesaDa: string) {
      this._contratto = contratto;
  this._blocked = blocked;
  this._processInstanceId = processInstanceId;
  this._tipoProposta = tipoProposta;
  this._causaleSospensione = causaleSospensione;
  this._numBlocchi = numBlocchi;
  this._arrivo = arrivo;
  this._prodotto = prodotto;
  this._richiedente = richiedente;
  this._numNote = numNote;
  this._numDocMancanti = numDocMancanti;
  this._sospesaDa = sospesaDa;

  }

  /**@ignore nomi delle chiavi del backend, per tab relativo - devono corrispondere ai getter (autogenerare dalle var). Da aggiornare se variano. */
  static readonly keys = ["contratto", "arrivo", "tipoProposta", "richiedente",  "sospesaDa", "causaleSospensione", "campagna"];


  public get contratto(): string {
    return this._contratto;
  }
  public set contratto(value: string) {
    this._contratto = value;
  }

  public get blocked(): boolean {
    return this._blocked;
  }
  public set blocked(value: boolean) {
    this._blocked = value;
  }

  public get processInstanceId(): number {
    return this._processInstanceId;
  }
  public set processInstanceId(value: number) {
    this._processInstanceId = value;
  }

  public get tipoProposta(): string {
    return this._tipoProposta;
  }
  public set tipoProposta(value: string) {
    this._tipoProposta = value;
  }

  public get causaleSospensione(): string {
    return this._causaleSospensione;
  }
  public set causaleSospensione(value: string) {
    this._causaleSospensione = value;
  }

  public get numBlocchi(): number {
    return this._numBlocchi;
  }
  public set numBlocchi(value: number) {
    this._numBlocchi = value;
  }

  public get arrivo(): string {
    return this._arrivo;
  }
  public set arrivo(value: string) {
    this._arrivo = value;
  }

  public get prodotto(): string {
    return this._prodotto;
  }
  public set prodotto(value: string) {
    this._prodotto = value;
  }

  public get richiedente(): string {
    return this._richiedente;
  }
  public set richiedente(value: string) {
    this._richiedente = value;
  }

  public get numNote(): number {
    return this._numNote;
  }
  public set numNote(value: number) {
    this._numNote = value;
  }

  public get numDocMancanti(): number {
    return this._numDocMancanti;
  }
  public set numDocMancanti(value: number) {
    this._numDocMancanti = value;
  }

  public get sospesaDa(): string {
    return this._sospesaDa;
  }
  public set sospesaDa(value: string) {
    this._sospesaDa = value;
  }

}