import {FinancialConfigServiceInner} from './FinancialConfigServiceInner';

export class FinancialServiceBodyParameters {
    private _mock?: boolean = false;

    private _assetType: string;
    private _brandId: string;
    private _contractLength: number;
    private _distance: number;
    private _finProductCode: string;
    private _financiedAmount: number;
    private _frequency: number;
    private _service: FinancialConfigServiceInner;
    private _tan: number;
    private _vatPercentage: number;
    private _netCost: number;

    constructor(assetType?: string, brandId?: string, contractLength?: number, distance?: number,
                finProductCode?: string, financiedAmount?: number, frequency?: number,
                service?: FinancialConfigServiceInner, tan?: number, vatPercentage?: number,
                netCost?: number) {
        this._assetType = assetType;
        this._brandId = brandId;
        this._contractLength = contractLength;
        this._distance = distance;
        this._finProductCode = finProductCode;
        this._financiedAmount = financiedAmount;
        this._frequency = frequency;
        this._service = service;
        this._tan = tan;
        this._vatPercentage = vatPercentage;
        this._netCost = netCost;
    }

    /**
     *
     * @returns {HttpParams}
     */
    requestParameters(): Object {
        let params: Object; // set of HttpParams return an immutable object

        params = {
            'assetType': this.assetType,
            'brandId': this.brandId,
            'contractLength': this.contractLength,
            'distance': this.distance,
            'netCost': this.netCost,
            'financiedAmount': this.financiedAmount,
            'frequency': this.frequency,
            'tan': this.tan,
            'finProductCode': this.finProductCode,
            'vatPercentage': this.vatPercentage,
            'service': {
                'supplyTypeCode': this.service.supplyTypeCode,
                'supplyElementCode': this.service.supplyElementCode,
                'supplyTypeDesc': this.service.supplyTypeDesc,
                'supplyElementDesc': this.service.supplyElementDesc,
                'categoryCode': this.service.categoryCode,
                'parentSupplyTypeCode': this.service.parentSupplyTypeCode,
                'defaultSupply': this.service.defaultSupply,
                'defaultElement': this.service.defaultElement,
                'updatable': this.service.updatable,
                'retrieveVat': this.service.retrieveVat,
                'parSupMand': this.service.parSupMand,
                'flagMainSup': this.service.flagMainSup,
                'selezionabile': this.service.selezionabile,
                'flagMandatory': this.service.flagMandatory,
                'tassoProdotto': this.service.tassoProdotto,
                'beneStrumentale': this.service.beneStrumentale,
                'clazz': this.service.clazz,
                'forcedToPC': this.service.forcedToPC,
                'customerProvinceCode': this.service.customerProvinceCode,
                'rtiCoverage': this.service.rtiCoverage,
                'fireTheftCoverage': this.service.fireTheftCoverage,
                'calculationAllowed': this.service.calculationAllowed,
                'hasSatellite': this.service.hasSatellite,
                'hasFireTheftInsurance': this.service.hasFireTheftInsurance,
                'annualDistance': this.service.annualDistance,
                'estensioneGaranziaProposta': this.service.estensioneGaranziaProposta,
                'supplyAmount': this.service.supplyAmount,

            }
        };

        let paramsMock = {
            'assetType': 'MBA',
            'brandId': 'MBF',
            'contractLength': 36,
            'distance': 30,
            'totalAmount': 24000,
            'financiedAmount': 19000,
            'frequency': 1,
            'tan': 7.339,
            'vatPercentage': 22,
            'service': {
                'supplyTypeCode': '39',
                'supplyElementCode': '390',
                'supplyTypeDesc': 'feel New',
                'supplyElementDesc': 'feel New',
                'categoryCode': 'RTI39',
                'parentSupplyTypeCode': '00',
                'defaultSupply': false,
                'defaultElement': false,
                'updatable': false,
                'retrieveVat': false,
                'parSupMand': '0',
                'flagMainSup': '0',
                'selezionabile': '1',
                'flagMandatory': '0',
                'tassoProdotto': false,
                'beneStrumentale': false,
                'clazz': 'RTISupplyServiceDTO',
                'forcedToPC': false,
                'customerProvinceCode': 'BN',
                'rtiCoverage': [
                    {
                        'code': 'RTI12'
                    }],
                'fireTheftCoverage': [
                    {
                        'code': 'FS36'
                    }]
            }


        };

        return this._mock ? paramsMock : params;
    }


    get assetType(): string {
        return this._assetType;
    }

    get brandId(): string {
        return this._brandId;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    get distance(): number {
        return this._distance;
    }

    get finProductCode(): string {
        return this._finProductCode;
    }

    get financiedAmount(): number {
        return this._financiedAmount;
    }

    get frequency(): number {
        return this._frequency;
    }

    get service(): FinancialConfigServiceInner {
        return this._service;
    }

    get tan(): number {
        return this._tan;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    set distance(value: number) {
        this._distance = value;
    }

    set finProductCode(value: string) {
        this._finProductCode = value;
    }

    set financiedAmount(value: number) {
        this._financiedAmount = value;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    set service(value: FinancialConfigServiceInner) {
        this._service = value;
    }

    set tan(value: number) {
        this._tan = value;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }
}