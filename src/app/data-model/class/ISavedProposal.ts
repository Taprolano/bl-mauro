/** Risposta di salvataggio proposta effettuato con successo */
export interface ISavedProposal{
    PROPNUM:string,
    VERSIONNUM:number
}