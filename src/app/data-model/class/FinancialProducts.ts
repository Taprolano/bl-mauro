
export class FinancialProducts {

    private _fpCode: string;
    private _fpDescription: string;
    private _financeCode: string;

    /**
     * @param {string} fpCode
     * @param {string} fpDescription
     * @param {string} financeCode
     */
    constructor(fpCode?: string, fpDescription?: string, financeCode?: string) {
        this._fpCode = fpCode;
        this._fpDescription = fpDescription;
        this._financeCode = financeCode;
    }

    /** @returns {string} */
    get fpCode(): string {
        return this._fpCode;
    }

    /** @returns {string} */
    get fpDescription(): string {
        return this._fpDescription;
    }

    /** @returns {string} */
    get financeCode(): string {
        return this._financeCode;
    }

    /** @param {string} value */
    set fpCode(value: string) {
        this._fpCode = value;
    }

    /** @param {string} value */
    set fpDescription(value: string) {
        this._fpDescription = value;
    }

    /** @param {string} value */
    set financeCode(value: string) {
        this._financeCode = value;
    }
}

export class FinancialProductsFactory{

    getInstance(obj: FinancialProducts): FinancialProducts{
        return new FinancialProducts(obj.fpCode, obj.fpDescription, obj.financeCode);
    }
}