import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HeaderComponent} from '../../shared/components/header/header.component';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from '../../shared/shared.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MbzDashboardModule} from '../../mbz-dashboard/mbz-dashboard.module';
import {ProposalsModule} from '../../proposals/proposals.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {createTranslateLoader} from '../../app.module';
import {AppRoutingModule} from '../../app-routing.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../../../environments/environment.svil';
import {AppComponent} from '../../app.component';
import {AuthGuard} from '../../shared/guard';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AbstractRequestParameters} from './AbstractRequestParameters';
import {CreditQueueAllRequestParameters} from './ProposalListAllRequestParameters';
import {ProposalListRequestParameters} from './ProposalListRequestParameters';
import {LoggerService} from '../../shared/services/logger.service';
import {fileURLToPath} from 'url';
import {VersionRequestParameters} from './VersionRequestParameters';
import {DirectAccessGuard} from '../../shared/guard/direct-access/direct-access.guard';
import {UtilsService} from '../../shared/services/utils.service';
import {IProposalRequest} from '../interface/IProposalRequest';


describe('ProposalListRequestParameters', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('get & set test', () => {

        let propList = new ProposalListRequestParameters(new LoggerService());

        propList.attivataA = ' ';
        propList.attivataDa = ' ';
        propList.numeroContratto = ' ';
        propList.numeroProposta = ' ';
        propList.soggetto = ' ';
        propList.stato = [0, 0];
        propList.targa = ' ';
        propList.telaio = ' ';
        propList.page = 0;
        propList.size = 0;
        propList.sort = ' ';

        expect(propList.attivataA).toEqual(' ');
        expect(propList.attivataDa).toEqual(' ');
        expect(propList.numeroContratto).toEqual(' ');
        expect(propList.numeroProposta).toEqual(' ');
        expect(propList.soggetto).toEqual(' ');
        expect(propList.stato).toEqual([0, 0]);
        expect(propList.targa).toEqual(' ');
        expect(propList.telaio).toEqual(' ');
        expect(propList.page).toEqual(0);
        expect(propList.page).toEqual(0);
        expect(propList.sort).toEqual(' ');

    });

    it('setRequestParametersFromInterface test', () => {

        let propList = new ProposalListRequestParameters(new LoggerService());

        propList.attivataA = ' ';
        propList.attivataDa = ' ';
        propList.numeroContratto = ' ';
        propList.numeroProposta = ' ';
        propList.soggetto = ' ';
        propList.stato = [0, 0];
        propList.targa = ' ';
        propList.telaio = ' ';
        propList.page = 0;
        propList.size = 0;
        propList.sort = ' ';

        let mockIProposalRequest: IProposalRequest = {
            activationDateTo: ' ',
            activationDateFrom: ' ',
            contractNumber: ' ',
            proposalNumber: ' ',
            licensePlate: ' ',
            frameNumber: ' ',
            applicant: ' ',
            proposalState: [0, 0]
        };

        propList.setRequestParametersFromInterface(mockIProposalRequest);

    });

    it('requestParameters test', () => {

        let propList = new ProposalListRequestParameters(new LoggerService());

        propList.attivataA = ' ';
        propList.attivataDa = ' ';
        propList.numeroContratto = ' ';
        propList.numeroProposta = ' ';
        propList.soggetto = ' ';
        propList.stato = [0, 0];
        propList.targa = ' ';
        propList.telaio = ' ';
        propList.page = 0;
        propList.size = 0;
        propList.sort = ' ';

        propList.requestParameters();

    });

});
