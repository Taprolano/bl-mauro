

// proposalsInWork: number;
// proposalsInAnalysis: number;
// proposalsInError: number;
// proposalsAccepted: number;

// "counters": {
//     "countAttive": 0,
//         "countNonAttiveBen": 0,
//         "countInAttivazione": 7,
//         "countInAnalisi": 112
// }


export class ProposalCounter {

    private _countAttive?: number;
    private _countNonAttiveBen?: number;
    private _countInAttivazione?: number;
    private _countInAnalisi?: number;


    constructor(countAttive: number, countNonAttiveBen: number, countInAttivazione: number, countInAnalisi: number) {
        this._countAttive = countAttive;
        this._countNonAttiveBen = countNonAttiveBen;
        this._countInAttivazione = countInAttivazione;
        this._countInAnalisi = countInAnalisi;
    }

    /**
     * BINDING SERVICE BE
     * @returns {number}
     */

    get countAttive(): number {
        return this._countAttive;
    }

    set countAttive(value: number) {
        this._countAttive = value;
    }

    get countNonAttiveBen(): number {
        return this._countNonAttiveBen;
    }

    set countNonAttiveBen(value: number) {
        this._countNonAttiveBen = value;
    }

    get countInAttivazione(): number {
        return this._countInAttivazione;
    }

    set countInAttivazione(value: number) {
        this._countInAttivazione = value;
    }

    get countInAnalisi(): number {
        return this._countInAnalisi;
    }

    set countInAnalisi(value: number) {
        this._countInAnalisi = value;
    }

    /**
     * BINDING HTML
     * @returns {number}
     */

    get proposalsInWork(): number {
        return this._countInAttivazione;
    }

    set proposalsInWork(value: number) {
        this._countInAttivazione = value;
    }

    get proposalsInError(): number {
        return this._countNonAttiveBen;
    }

    set proposalsInError(value: number) {
        this._countNonAttiveBen = value;
    }

    get proposalsAccepted(): number {
        return this._countAttive; 
    }

    set proposalsAccepted(value: number) {
        this._countAttive = value;
    }

    get proposalsInAnalysis(): number {
        return this._countInAnalisi;
    }

    set proposalsInAnalysis(value: number) {
        this._countInAnalisi = value;
    }



}

export class ProposalCounterFactory {

    getInstance(obj:ProposalCounter): ProposalCounter {
        return new ProposalCounter(obj.countAttive, obj.countNonAttiveBen, obj.countInAttivazione, obj.countInAnalisi);
    }

}