import {ConfigurazioneRate, ConfigurazioneRateFactory} from './ConfigurazioneRate';
import {AdditionalProp, AdditionalPropFactory, IAdditionalProp} from './AdditionalProp';
import {FinancialServicesInner, FinancialServicesInnerFactory} from './FinancialServicesInner';
import {FinacialBaseServices} from './FinacialBaseServices';
import {Attachment, AttachmentFactory} from './Attachment';


export class FinancialServices extends FinacialBaseServices {

    private _beneStrumentale: boolean;
    private _cdTer: number;
    private _compCommSup: number;
    private _configurazioneRate: ConfigurazioneRate[];
    private _costAmount: number;
    private _costPerUnit: number;
    private _dealerCommisPercentage: number;
    private _discountAmount: number;
    private _discountPercentage: number;
    private _insuredValue: number;
    private _orderNumber: number;
    private _rate: AdditionalProp[];
    private _regulatedFlag: string;
    private _serviceEntities: FinancialServicesInner[];
    private _supplyCostAmount: number;
    private _supplyCostUnit: number;
    private _supplyDescription: string;
    private _supplyElementCost: number;
    private _supplyElementMargin: number;
    private _supplySeqNum: number;
    private _value: number;
    private _vatPercentage: number;
    private _attachments: Attachment[];
    private _clazz: string;

    constructor(categoryCode?: string, defaultElement?: boolean, defaultSupply?: boolean, financeCode?: string, flagMainSup?: string,
                flagMandatory?: string, parentSupplyTypeCode?: string, parSupMand?: string, selezionabile?: string,
                supplyElementCode?: string, supplyElementDesc?: string, supplyTypeCode?: string, supplyTypeDesc?: string,
                tassoProdotto?: boolean, taxPercentage?: number, updatable?: boolean, beneStrumentale?: boolean, cdTer?: number,
                compCommSup?: number, configurazioneRate?: ConfigurazioneRate[], costAmount?: number, costPerUnit?: number,
                dealerCommisPercentage?: number, discountAmount?: number, discountPercentage?: number, insuredValue?: number,
                orderNumber?: number, rate?: AdditionalProp[], regulatedFlag?: string, serviceEntities?: FinancialServicesInner[],
                supplyCostAmount?: number, supplyCostUnit?: number, supplyDescription?: string, supplyElementCost?: number,
                supplyElementMargin?: number, supplySeqNum?: number, value?: number, vatPercentage?: number,
                attachments?: Attachment[], clazz?: string) {

        super(categoryCode, defaultElement, defaultSupply, financeCode, flagMainSup, flagMandatory, parentSupplyTypeCode, parSupMand,
            selezionabile, supplyElementCode, supplyElementDesc, supplyTypeCode, supplyTypeDesc, tassoProdotto, taxPercentage, updatable);
        this._beneStrumentale = beneStrumentale;
        this._cdTer = cdTer;
        this._compCommSup = compCommSup;
        this._configurazioneRate = configurazioneRate;
        this._costAmount = costAmount;
        this._costPerUnit = costPerUnit;
        this._dealerCommisPercentage = dealerCommisPercentage;
        this._discountAmount = discountAmount;
        this._discountPercentage = discountPercentage;
        this._insuredValue = insuredValue;
        this._orderNumber = orderNumber;
        this._rate = rate;
        this._regulatedFlag = regulatedFlag;
        this._serviceEntities = serviceEntities;
        this._supplyCostAmount = supplyCostAmount;
        this._supplyCostUnit = supplyCostUnit;
        this._supplyDescription = supplyDescription;
        this._supplyElementCost = supplyElementCost;
        this._supplyElementMargin = supplyElementMargin;
        this._supplySeqNum = supplySeqNum;
        this._value = value;
        this._vatPercentage = vatPercentage;
        this._attachments = attachments;
        this._clazz = clazz;
    }


    /** @returns {number} */
    get cdTer(): number {
        return this._cdTer;
    }

    /** @returns {number} */
    get compCommSup(): number {
        return this._compCommSup;
    }

    /** @returns {ConfigurazioneRate} */
    get configurazioneRate(): ConfigurazioneRate[] {
        return this._configurazioneRate;
    }

    /** @returns {number} */
    get costAmount(): number {
        return this._costAmount;
    }

    /** @returns {number} */
    get costPerUnit(): number {
        return this._costPerUnit;
    }

    /** @returns {number} */
    get dealerCommisPercentage(): number {
        return this._dealerCommisPercentage;
    }

    /** @returns {number} */
    get discountAmount(): number {
        return this._discountAmount;
    }

    /** @returns {number} */
    get discountPercentage(): number {
        return this._discountPercentage;
    }

    /** @returns {number} */
    get insuredValue(): number {
        return this._insuredValue;
    }

    /** @returns {number} */
    get orderNumber(): number {
        return this._orderNumber;
    }

    /** @returns {AdditionalProp[]} */
    get rate(): AdditionalProp[] {
        return this._rate;
    }

    /** @returns {string} */
    get regulatedFlag(): string {
        return this._regulatedFlag;
    }

    /** @returns {boolean} */
    get beneStrumentale(): boolean {
        return this._beneStrumentale;
    }


    /** @returns {number} */
    get value(): number {
        return this._value;
    }

    /** @returns {number} */
    get vatPercentage(): number {
        return this._vatPercentage;
    }


    /** @returns {FinancialServicesInner} */
    get serviceEntities(): FinancialServicesInner[] {
        return this._serviceEntities;
    }

    /** @returns {number} */
    get supplyCostAmount(): number {
        return this._supplyCostAmount;
    }

    /** @returns {number} */
    get supplyCostUnit(): number {
        return this._supplyCostUnit;
    }

    /** @returns {string} */
    get supplyDescription(): string {
        return this._supplyDescription;
    }

    /** @returns {number} */
    get supplyElementCost(): number {
        return this._supplyElementCost;
    }

    /** @returns {number} */
    get supplyElementMargin(): number {
        return this._supplyElementMargin;
    }

    /** @returns {number} */
    get supplySeqNum(): number {
        return this._supplySeqNum;
    }

    /** @returns {string} */
    get clazz(): string {
        return this._clazz;
    }

    /*    SET      */

    /** @param {string} value */
    set clazz(value: string) {
        this._clazz = value;
    }

    /** @returns {Attachment[]} */
    get attachments(): Attachment[] {
        return this._attachments;
    }

    /** @param {Attachment[]} value */
    set attachments(value: Attachment[]) {
        this._attachments = value;
    }

    /** @param {boolean} value */
    set beneStrumentale(value: boolean) {
        this._beneStrumentale = value;
    }


    /** @param {number} value */
    set value(value: number) {
        this._value = value;
    }

    /** @param {number} value */
    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    /** @param {number} value */
    set cdTer(value: number) {
        this._cdTer = value;
    }

    /** @param {number} value */
    set compCommSup(value: number) {
        this._compCommSup = value;
    }

    /** @param {ConfigurazioneRate} value */
    set configurazioneRate(value: ConfigurazioneRate[]) {
        this._configurazioneRate = value;
    }

    /** @param {number} value */
    set costAmount(value: number) {
        this._costAmount = value;
    }

    /** @param {number} value */
    set costPerUnit(value: number) {
        this._costPerUnit = value;
    }

    /** @param {number} value */
    set dealerCommisPercentage(value: number) {
        this._dealerCommisPercentage = value;
    }

    /** @param {number} value */
    set discountAmount(value: number) {
        this._discountAmount = value;
    }

    /** @param {number} value */
    set discountPercentage(value: number) {
        this._discountPercentage = value;
    }

    /** @param {number} value */
    set insuredValue(value: number) {
        this._insuredValue = value;
    }

    /** @param {number} value */
    set orderNumber(value: number) {
        this._orderNumber = value;
    }

    /** @param {AdditionalProp[]} value */
    set rate(value: AdditionalProp[]) {
        this._rate = value;
    }

    /** @param {string} value */
    set regulatedFlag(value: string) {
        this._regulatedFlag = value;
    }

    /** @param {FinancialServicesInner} value */
    set serviceEntities(value: FinancialServicesInner[]) {
        this._serviceEntities = value;
    }

    /** @param {number} value */
    set supplyCostAmount(value: number) {
        this._supplyCostAmount = value;
    }

    /** @param {number} value */
    set supplyCostUnit(value: number) {
        this._supplyCostUnit = value;
    }

    /** @param {string} value */
    set supplyDescription(value: string) {
        this._supplyDescription = value;
    }

    /** @param {number} value */
    set supplyElementCost(value: number) {
        this._supplyElementCost = value;
    }

    /** @param {number} value */
    set supplyElementMargin(value: number) {
        this._supplyElementMargin = value;
    }

    /** @param {number} value */
    set supplySeqNum(value: number) {
        this._supplySeqNum = value;
    }
}

export class FinancialServicesFactory {

    static getInstance(obj: FinancialServices): FinancialServices {
        let finServices = new FinancialServices();

        let configurazioneRateFact = new ConfigurazioneRateFactory();
        let configurazioneRate = configurazioneRateFact.getInstanceList(obj.configurazioneRate);

        //let additionalPropFact = new AdditionalPropFactory();
        let rate = AdditionalPropFactory.getInstanceList(obj.rate);

        let attachList = AttachmentFactory.getInstanceList(obj.attachments);

        //  Se diventa una lista, cambiare la riga superiore con questa sottostante
        let financialServicesInnerFact = new FinancialServicesInnerFactory();
        let finServicesInner = financialServicesInnerFact.getInstanceList(obj.serviceEntities);

        if (obj) {
            if (obj.beneStrumentale != undefined) finServices.beneStrumentale = obj.beneStrumentale;
            if (obj.categoryCode != undefined) finServices.categoryCode = obj.categoryCode;
            if (obj.cdTer != undefined) finServices.cdTer = obj.cdTer;
            if (obj.compCommSup != undefined) finServices.compCommSup = obj.compCommSup;
            if (configurazioneRate != undefined) finServices.configurazioneRate = configurazioneRate;
            if (obj.costAmount != undefined) finServices.costAmount = obj.costAmount;
            if (obj.costPerUnit != undefined) finServices.costPerUnit = obj.costPerUnit;
            if (obj.dealerCommisPercentage != undefined) finServices.dealerCommisPercentage = obj.dealerCommisPercentage;
            if (obj.defaultElement != undefined) finServices.defaultElement = obj.defaultElement;
            if (obj.defaultSupply != undefined) finServices.defaultSupply = obj.defaultSupply;
            if (obj.discountAmount != undefined) finServices.discountAmount = obj.discountAmount;
            if (obj.discountPercentage != undefined) finServices.discountPercentage = obj.discountPercentage;
            if (obj.financeCode != undefined) finServices.financeCode = obj.financeCode;
            if (obj.flagMainSup != undefined) finServices.flagMainSup = obj.flagMainSup;
            if (obj.flagMandatory != undefined) finServices.flagMandatory = obj.flagMandatory;
            if (obj.insuredValue != undefined) finServices.insuredValue = obj.insuredValue;
            if (obj.orderNumber != undefined) finServices.orderNumber = obj.orderNumber;
            if (obj.parSupMand != undefined) finServices.parSupMand = obj.parSupMand;
            if (obj.parentSupplyTypeCode != undefined) finServices.parentSupplyTypeCode = obj.parentSupplyTypeCode;
            if (rate != undefined) finServices.rate = rate;
            if (obj.regulatedFlag != undefined) finServices.regulatedFlag = obj.regulatedFlag;
            if (obj.selezionabile != undefined) finServices.selezionabile = obj.selezionabile;
            if (obj.serviceEntities != undefined) finServices.serviceEntities = finServicesInner;
            if (obj.supplyCostAmount != undefined) finServices.supplyCostAmount = obj.supplyCostAmount;
            if (obj.supplyCostUnit != undefined) finServices.supplyCostUnit = obj.supplyCostUnit;
            if (obj.supplyDescription != undefined) finServices.supplyDescription = obj.supplyDescription;
            if (obj.supplyElementCode != undefined) finServices.supplyElementCode = obj.supplyElementCode;
            if (obj.supplyElementCost != undefined) finServices.supplyElementCost = obj.supplyElementCost;
            if (obj.supplyElementDesc != undefined) finServices.supplyElementDesc = obj.supplyElementDesc;
            if (obj.supplyElementMargin != undefined) finServices.supplyElementMargin = obj.supplyElementMargin;
            if (obj.supplySeqNum != undefined) finServices.supplySeqNum = obj.supplySeqNum;
            if (obj.supplyTypeCode != undefined) finServices.supplyTypeCode = obj.supplyTypeCode;
            if (obj.supplyTypeDesc != undefined) finServices.supplyTypeDesc = obj.supplyTypeDesc;
            if (obj.tassoProdotto != undefined) finServices.tassoProdotto = obj.tassoProdotto;
            if (obj.taxPercentage != undefined) finServices.taxPercentage = obj.taxPercentage;
            if (obj.updatable != undefined) finServices.updatable = obj.updatable;
            if (obj.value != undefined) finServices.value = obj.value;
            if (obj.vatPercentage != undefined) finServices.vatPercentage = obj.vatPercentage;
            if (obj.attachments != undefined) finServices.attachments = attachList;
            if (obj.clazz != undefined) finServices.clazz = obj.clazz;
        }

        return finServices;
    }


    static getInstanceList(list: any): FinancialServices[] {
        let financialService = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialService.push(FinancialServicesFactory.getInstance(elem));
            });
        }
        return financialService;
    }

    /*
        static getInstanceListParam(list: any): FinancialServices[] {
            let fs: FinancialServices[] = [];
            if (list && list.length > 0) {
                list.forEach(elem => {
                    fs.push(this.getInstance(elem).requestParameters());
                });
            }
            return fs;
        }*/

}

export interface IFinancialServices {
    beneStrumentale: boolean;
    cdTer: number;
    compCommSup: number;
    configurazioneRate: ConfigurazioneRate[];
    costAmount: number;
    costPerUnit: number;
    dealerCommisPercentage: number;
    discountAmount: number;
    discountPercentage: number;
    insuredValue: number;
    orderNumber: number;
    rate: AdditionalProp[];
    regulatedFlag: string;
    serviceEntities: FinancialServicesInner[];
    supplyAmount: number;
    supplyCostAmount: number;
    supplyCostUnit: number;
    supplyDescription: string;
    supplyElementCost: number;
    supplyElementMargin: number;
    supplySeqNum: number;
    value: number;
    vatPercentage: number;
    attachments: Attachment[];
}