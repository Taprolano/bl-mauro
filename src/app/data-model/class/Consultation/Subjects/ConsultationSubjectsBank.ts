import { UtilsService } from '../../../../shared/services/utils.service';
export class ConsultationSubjectsBank{
  private _progressivo?: number;
  private _sbjCounter?: number;
  private _codSportello?: string;
  private _abi?: string;
  private _cab?: string;
  private _ccbn?: string;
  private _descrBanca?: string;
  private _descrAgenzia?: string;
  private _address?: ConsultationAddress;
  private _numero?: string;
  private _localita?: string;
  private _cap?: string;
  private _provincia?: string;
  private _iban?: string;
  private _ddmStatusCode?: string;
  private _preferredBankAccount?: boolean;
  private _accountHolderName?: string; 


	constructor(progressivo?: number,
    sbjCounter?: number,
    codSportello?: string,
    abi?: string,
    cab?: string,
    ccbn?: string,
    descrBanca?: string,
    descrAgenzia?: string,
    address?: ConsultationAddress,
    numero?: string,
    localita?: string,
    cap?: string,
    provincia?: string,
    iban?: string,
    ddmStatusCode?: string,
    preferredBankAccount?: boolean,
    accountHolderName?: string ) {
    this._progressivo = progressivo;
    this._sbjCounter = sbjCounter;
    this._codSportello = codSportello;
    this._abi = abi;
    this._cab = cab;
    this._ccbn = ccbn;
    this._descrBanca = descrBanca;
    this._descrAgenzia = descrAgenzia;
    this._address = address;
    this._numero = numero;
    this._localita = localita;
    this._cap = cap;
    this._provincia = provincia;
    this._iban = iban;
    this._ddmStatusCode = ddmStatusCode;
    this._preferredBankAccount = preferredBankAccount;
    this._accountHolderName = accountHolderName; 
	}

    /**
     * Getter progressivo
     * @return {number}
     */
	public get progressivo(): number {
		return this._progressivo;
	}

    /**
     * Getter sbjCounter
     * @return {number}
     */
	public get sbjCounter(): number {
		return this._sbjCounter;
	}

    /**
     * Getter codSportello
     * @return {string}
     */
	public get codSportello(): string {
		return this._codSportello;
	}

    /**
     * Getter abi
     * @return {string}
     */
	public get abi(): string {
		return this._abi;
	}

    /**
     * Getter cab
     * @return {string}
     */
	public get cab(): string {
		return this._cab;
	}

    /**
     * Getter ccbn
     * @return {string}
     */
	public get ccbn(): string {
		return this._ccbn;
	}

    /**
     * Getter descrBanca
     * @return {string}
     */
	public get descrBanca(): string {
		return this._descrBanca;
	}

    /**
     * Getter descrAgenzia
     * @return {string}
     */
	public get descrAgenzia(): string {
		return this._descrAgenzia;
	}

    /**
     * Getter address
     * @return {ConsultationAddress}
     */
	public get address(): ConsultationAddress {
		return this._address;
	}

    /**
     * Getter numero
     * @return {string}
     */
	public get numero(): string {
		return this._numero;
	}

    /**
     * Getter localita
     * @return {string}
     */
	public get localita(): string {
		return this._localita;
	}

    /**
     * Getter cap
     * @return {string}
     */
	public get cap(): string {
		return this._cap;
	}

    /**
     * Getter provincia
     * @return {string}
     */
	public get provincia(): string {
		return this._provincia;
	}

    /**
     * Getter iban
     * @return {string}
     */
	public get iban(): string {
		return this._iban;
	}

    /**
     * Getter ddmStatusCode
     * @return {string}
     */
	public get ddmStatusCode(): string {
		return this._ddmStatusCode;
	}

    /**
     * Getter preferredBankAccount
     * @return {boolean}
     */
	public get preferredBankAccount(): boolean {
		return this._preferredBankAccount;
	}

    /**
     * Getter accountHolderName
     * @return {string}
     */
	public get accountHolderName(): string {
		return this._accountHolderName;
	}

    /**
     * Setter progressivo
     * @param {number} value
     */
	public set progressivo(value: number) {
		this._progressivo = value;
	}

    /**
     * Setter sbjCounter
     * @param {number} value
     */
	public set sbjCounter(value: number) {
		this._sbjCounter = value;
	}

    /**
     * Setter codSportello
     * @param {string} value
     */
	public set codSportello(value: string) {
		this._codSportello = value;
	}

    /**
     * Setter abi
     * @param {string} value
     */
	public set abi(value: string) {
		this._abi = value;
	}

    /**
     * Setter cab
     * @param {string} value
     */
	public set cab(value: string) {
		this._cab = value;
	}

    /**
     * Setter ccbn
     * @param {string} value
     */
	public set ccbn(value: string) {
		this._ccbn = value;
	}

    /**
     * Setter descrBanca
     * @param {string} value
     */
	public set descrBanca(value: string) {
		this._descrBanca = value;
	}

    /**
     * Setter descrAgenzia
     * @param {string} value
     */
	public set descrAgenzia(value: string) {
		this._descrAgenzia = value;
	}

    /**
     * Setter address
     * @param {ConsultationAddress} value
     */
	public set address(value: ConsultationAddress) {
		this._address = value;
	}

    /**
     * Setter numero
     * @param {string} value
     */
	public set numero(value: string) {
		this._numero = value;
	}

    /**
     * Setter localita
     * @param {string} value
     */
	public set localita(value: string) {
		this._localita = value;
	}

    /**
     * Setter cap
     * @param {string} value
     */
	public set cap(value: string) {
		this._cap = value;
	}

    /**
     * Setter provincia
     * @param {string} value
     */
	public set provincia(value: string) {
		this._provincia = value;
	}

    /**
     * Setter iban
     * @param {string} value
     */
	public set iban(value: string) {
		this._iban = value;
	}

    /**
     * Setter ddmStatusCode
     * @param {string} value
     */
	public set ddmStatusCode(value: string) {
		this._ddmStatusCode = value;
	}

    /**
     * Setter preferredBankAccount
     * @param {boolean} value
     */
	public set preferredBankAccount(value: boolean) {
		this._preferredBankAccount = value;
	}

    /**
     * Setter accountHolderName
     * @param {string} value
     */
	public set accountHolderName(value: string) {
		this._accountHolderName = value;
	}

}

export class ConsultationSubjectsBankFactory {
  constructor(){
  }
  
  getInstance(obj: ConsultationSubjectsBank): ConsultationSubjectsBank {
    let _utils = new UtilsService;
    let consSubjectsBank = new ConsultationSubjectsBank();
    let consAddressFactory = new ConsultationAddressFactory();

    if(obj) {
      if(!_utils.isVoid(obj.progressivo)) consSubjectsBank.progressivo = obj.progressivo;
      if(!_utils.isVoid(obj.sbjCounter)) consSubjectsBank.sbjCounter = obj.sbjCounter;
      if(!_utils.isVoid(obj.codSportello)) consSubjectsBank.codSportello = obj.codSportello;
      if(!_utils.isVoid(obj.abi)) consSubjectsBank.abi = obj.abi;
      if(!_utils.isVoid(obj.cab)) consSubjectsBank.cab = obj.cab;
      if(!_utils.isVoid(obj.ccbn)) consSubjectsBank.ccbn = obj.ccbn;
      if(!_utils.isVoid(obj.descrBanca)) consSubjectsBank.descrBanca = obj.descrBanca;
      if(!_utils.isVoid(obj.descrAgenzia)) consSubjectsBank.descrAgenzia = obj.descrAgenzia;
      if(!_utils.isVoid(obj.address)) consSubjectsBank.address = consAddressFactory.getInstance(obj.address);
      if(!_utils.isVoid(obj.numero)) consSubjectsBank.numero = obj.numero;
      if(!_utils.isVoid(obj.localita)) consSubjectsBank.localita = obj.localita;
      if(!_utils.isVoid(obj.cap)) consSubjectsBank.cap = obj.cap;
      if(!_utils.isVoid(obj.provincia)) consSubjectsBank.provincia = obj.provincia;
      if(!_utils.isVoid(obj.iban)) consSubjectsBank.iban = obj.iban;
      if(!_utils.isVoid(obj.ddmStatusCode)) consSubjectsBank.ddmStatusCode = obj.ddmStatusCode;
      if(!_utils.isVoid(obj.preferredBankAccount)) consSubjectsBank.preferredBankAccount = obj.preferredBankAccount;
      if(!_utils.isVoid(obj.accountHolderName)) consSubjectsBank.accountHolderName = obj.accountHolderName;
      return consSubjectsBank;
    }
  }
  
  getInstanceList(list: any): ConsultationSubjectsBank[] {
    let cList: ConsultationSubjectsBank[] = [];
    if (list && list.length > 0) {
        list.forEach(elem => {
            cList.push(this.getInstance(elem));
        });
    }
    return cList;
  }
}

export class ConsultationAddress {
  private _progressivo?: number;
  private _cap?: string;
  private _province?: string;
  private _address?: string;
  private _place?: string;
  private _contactInfo?: any[];
  public get contactInfo(): any[] {
    return this._contactInfo;
  }
  public set contactInfo(value: any[]) {
    this._contactInfo = value;
  }

  public get place(): string {
    return this._place;
  }
  public set place(value: string) {
    this._place = value;
  }

  public get progressivo(): number {
    return this._progressivo;
  }
  public set progressivo(value: number) {
    this._progressivo = value;
  }

  public get cap(): string {
    return this._cap;
  }
  public set cap(value: string) {
    this._cap = value;
  }
  
  public get province(): string {
    return this._province;
  }
  public set province(value: string) {
    this._province = value;
  }

  public get address(): string {
    return this._address;
  }
  public set address(value: string) {
    this._address = value;
  }

  constructor(progressivo?: number,
    cap?: string,
    province?: string,
    address?: string,
    place?: string,
    contactInfo?: any[]){

    this._progressivo = progressivo;
    this._cap = cap;
    this._province = province;
    this._address = address;
    this._place = place;
    this._contactInfo = contactInfo;
  }
}

export class ConsultationAddressFactory {
  constructor(){
  }

  getInstance(obj: ConsultationAddress): ConsultationAddress {
    let _utils = new UtilsService;
    let consSubjectsAddress = new ConsultationAddress();

    if(obj) {
      if(!_utils.isVoid(obj.progressivo)) consSubjectsAddress.progressivo = obj.progressivo;
      if(!_utils.isVoid(obj.cap)) consSubjectsAddress.cap = obj.cap;
      if(!_utils.isVoid(obj.province)) consSubjectsAddress.province = obj.province;
      if(!_utils.isVoid(obj.address)) consSubjectsAddress.address = obj.address;
      if(!_utils.isVoid(obj.place)) consSubjectsAddress.place = obj.place;
      if(!_utils.isVoid(obj.contactInfo)) consSubjectsAddress.contactInfo = obj.contactInfo;
      return consSubjectsAddress;
    }
  }
}