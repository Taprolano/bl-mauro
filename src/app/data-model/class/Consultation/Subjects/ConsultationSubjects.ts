import { ConsultationSubjectsBank, ConsultationSubjectsBankFactory } from "./ConsultationSubjectsBank";
import { ConsultationSubjectsDoc, ConsultationSubjectsDocFactory } from './ConsultationSubjectsDoc';
import { UtilsService } from '../../../../shared/services/utils.service';

export class ConsultationSubjects {
  private _type?: string;
  private _legameSoggetto?: any;
  private _legameProposta?: string[];
  private _subjectCode?: number;
  private _codSoggettoLinkato?: number;
	private _codiceLegale?: number;
	private _codPostazione?: number;
  private _codTipoSoggetto?: string;
  private _codSae?: string;
  private _flagPF?: string;
  private _fiscalCode?: string;
  private _ragioneSociale?: string;
  private _flagPrivacy?: string;
  private _banks?: ConsultationSubjectsBank[];
  private _documents?: ConsultationSubjectsDoc[];
  private _addresses?: ConsultationSubjAddress[];
  private _codRuoloSoggetto?: string;
  private _surname?: string;
  private _genderCode?: string;
  private _name?: string;
  private _birthDate?: string;
  private _birthPlace?: string;
  private _gender?: string;
  private _communityPropertyState?: boolean;
  private _married?: boolean;
  private _annualIncome?: number;
  private _dependentFamilyMembers?: number;
  private _household?: number;
  private _landsNumber?: number;
  private _landsValue?: number;
  private _landsMortgage?: number;
  private _buildingsNumber?: number;
  private _buildingsValue?: number;
  private _buildingsMortgage?: number;
  private _carsOwned?: number;
  private _carsUsed?: number;
  private _creditCardType?: boolean;
  private _numFamWorking?: number;
  private _knownByDealer?: boolean;
  private _italianCitizen?: boolean;
  private _otherLoans?: boolean;
  private _signatory?: boolean;
  private _redditoAnnuo?: number;
	private _pep?: boolean;
	private _legalAddress: ConsultationSubjAddress;

	constructor(
		type?: string,
		legameSoggetto?: any,
		legameProposta?: string[],
		subjectCode?: number,
		codSoggettoLinkato?: number,
		codiceLegale?: number,
		codPostazione?: number,
		codTipoSoggetto?: string,
		codSae?: string,
		flagPF?: string,
		fiscalCode?: string,
		ragioneSociale?: string,
		flagPrivacy?: string,
		banks?: ConsultationSubjectsBank[],
		documents?: ConsultationSubjectsDoc[],
		addresses?: ConsultationSubjAddress[],
		codRuoloSoggetto?: string,
		surname?: string,
		genderCode?: string,
		name?: string,
		birthDate?: string,
		birthPlace?: string,
		gender?: string,
		communityPropertyState?: boolean,
		married?: boolean,
		annualIncome?: number,
		dependentFamilyMembers?: number,
		household?: number,
		landsNumber?: number,
		landsValue?: number,
		landsMortgage?: number,
		buildingsNumber?: number,
		buildingsValue?: number,
		buildingsMortgage?: number,
		carsOwned?: number,
		carsUsed?: number,
		creditCardType?: boolean,
		numFamWorking?: number,
		knownByDealer?: boolean,
		italianCitizen?: boolean,
		otherLoans?: boolean,
		signatory?: boolean,
		redditoAnnuo?: number,
		pep?: boolean,
		legalAddress?: ConsultationSubjAddress
	) {

		this._type = type;
		this._legameSoggetto = legameSoggetto;
		this._legameProposta = legameProposta;
		this._subjectCode = subjectCode;
		this._codSoggettoLinkato = codSoggettoLinkato;
		this._codiceLegale = codiceLegale;
		this._codPostazione = codPostazione;
		this._codTipoSoggetto = codTipoSoggetto;
		this._codSae = codSae;
		this._flagPF = flagPF;
		this._fiscalCode = fiscalCode;
		this._ragioneSociale = ragioneSociale;
		this._flagPrivacy = flagPrivacy;
		this.banks = banks;
		this.documents = documents;
		this.addresses = addresses;
		this.codRuoloSoggetto = codRuoloSoggetto;
		this.surname = surname;
		this.genderCode = genderCode;
		this.name = name;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.gender = gender;
		this.communityPropertyState = communityPropertyState;
		this.married = married;
		this.annualIncome = annualIncome;
		this.dependentFamilyMembers = dependentFamilyMembers;
		this.household = household;
		this.landsNumber = landsNumber;
		this.landsValue = landsValue;
		this.landsMortgage = landsMortgage;
		this.buildingsNumber = buildingsNumber;
		this.buildingsValue = buildingsValue;
		this.buildingsMortgage = buildingsMortgage;
		this.carsOwned = carsOwned;
		this.carsUsed = carsUsed;
		this.creditCardType = creditCardType;
		this.numFamWorking = numFamWorking;
		this.knownByDealer = knownByDealer;
		this.italianCitizen = italianCitizen;
		this.otherLoans = otherLoans;
		this.signatory = signatory;
		this.redditoAnnuo = redditoAnnuo;
		this.pep = pep;
		this._legalAddress = legalAddress;
	}


	public get type(): string {
		return this._type;
	}

	public get legameSoggetto(): any {
		return this._legameSoggetto;
	}

	public get legameProposta(): string[] {
		return this._legameProposta;
	}

	public get subjectCode(): number {
		return this._subjectCode;
	}

	public get codTipoSoggetto(): string {
		return this._codTipoSoggetto;
	}

	public get codSae(): string {
		return this._codSae;
	}

	public get flagPF(): string {
		return this._flagPF;
	}

	public get fiscalCode(): string {
		return this._fiscalCode;
	}

	public get ragioneSociale(): string {
		return this._ragioneSociale;
	}

	public get flagPrivacy(): string {
		return this._flagPrivacy;
	}

	public get banks(): ConsultationSubjectsBank[] {
		return this._banks;
	}

	public get documents(): ConsultationSubjectsDoc[] {
		return this._documents;
	}

	public get addresses(): ConsultationSubjAddress[] {
		return this._addresses;
	}

	public get codRuoloSoggetto(): string {
		return this._codRuoloSoggetto;
	}

	public get surname(): string {
		return this._surname;
	}

	public get genderCode(): string {
		return this._genderCode;
	}

	public get name(): string {
		return this._name;
	}

	public get birthDate(): string {
		return this._birthDate;
	}

	public get birthPlace(): string {
		return this._birthPlace;
	}

	public get gender(): string {
		return this._gender;
	}

	public get communityPropertyState(): boolean {
		return this._communityPropertyState;
	}

	public get married(): boolean {
		return this._married;
	}

	public get annualIncome(): number {
		return this._annualIncome;
	}

	public get dependentFamilyMembers(): number {
		return this._dependentFamilyMembers;
	}

	public get household(): number {
		return this._household;
	}

	public get landsNumber(): number {
		return this._landsNumber;
	}

	public get landsValue(): number {
		return this._landsValue;
	}

	public get landsMortgage(): number {
		return this._landsMortgage;
	}

	public get buildingsNumber(): number {
		return this._buildingsNumber;
	}

	public get buildingsValue(): number {
		return this._buildingsValue;
	}

	public get buildingsMortgage(): number {
		return this._buildingsMortgage;
	}

	public get carsOwned(): number {
		return this._carsOwned;
	}

	public get carsUsed(): number {
		return this._carsUsed;
	}

	public get creditCardType(): boolean {
		return this._creditCardType;
	}

	public get numFamWorking(): number {
		return this._numFamWorking;
	}

	public get knownByDealer(): boolean {
		return this._knownByDealer;
	}

	public get italianCitizen(): boolean {
		return this._italianCitizen;
	}

	public get otherLoans(): boolean {
		return this._otherLoans;
	}

	public get signatory(): boolean {
		return this._signatory;
	}

	public get redditoAnnuo(): number {
		return this._redditoAnnuo;
	}

	public get pep(): boolean {
		return this._pep;
	}

	public set type(value: string) {
		this._type = value;
	}

	public set legameSoggetto(value: any) {
		this._legameSoggetto = value;
	}

	public set legameProposta(value: string[]) {
		this._legameProposta = value;
	}

	public set subjectCode(value: number) {
		this._subjectCode = value;
	}

	public set codTipoSoggetto(value: string) {
		this._codTipoSoggetto = value;
	}

	public set codSae(value: string) {
		this._codSae = value;
	}

	public set flagPF(value: string) {
		this._flagPF = value;
	}

	public set fiscalCode(value: string) {
		this._fiscalCode = value;
	}

	public set ragioneSociale(value: string) {
		this._ragioneSociale = value;
	}

	public set flagPrivacy(value: string) {
		this._flagPrivacy = value;
	}

	public set banks(value: ConsultationSubjectsBank[]) {
		this._banks = value;
	}

	public set documents(value: ConsultationSubjectsDoc[]) {
		this._documents = value;
	}

	public set addresses(value: ConsultationSubjAddress[]) {
		this._addresses = value;
	}

	public set codRuoloSoggetto(value: string) {
		this._codRuoloSoggetto = value;
	}

	public set surname(value: string) {
		this._surname = value;
	}

	public set genderCode(value: string) {
		this._genderCode = value;
	}

	public set name(value: string) {
		this._name = value;
	}

	public set birthDate(value: string) {
		this._birthDate = value;
	}

	public set birthPlace(value: string) {
		this._birthPlace = value;
	}

	public set gender(value: string) {
		this._gender = value;
	}

	public set communityPropertyState(value: boolean) {
		this._communityPropertyState = value;
	}

	public set married(value: boolean) {
		this._married = value;
	}

	public set annualIncome(value: number) {
		this._annualIncome = value;
	}

	public set dependentFamilyMembers(value: number) {
		this._dependentFamilyMembers = value;
	}

	public set household(value: number) {
		this._household = value;
	}

	public set landsNumber(value: number) {
		this._landsNumber = value;
	}

	public set landsValue(value: number) {
		this._landsValue = value;
	}

	public set landsMortgage(value: number) {
		this._landsMortgage = value;
	}

	public set buildingsNumber(value: number) {
		this._buildingsNumber = value;
	}

	public set buildingsValue(value: number) {
		this._buildingsValue = value;
	}

	public set buildingsMortgage(value: number) {
		this._buildingsMortgage = value;
	}

	public set carsOwned(value: number) {
		this._carsOwned = value;
	}

	public set carsUsed(value: number) {
		this._carsUsed = value;
	}

	public set creditCardType(value: boolean) {
		this._creditCardType = value;
	}

	public set numFamWorking(value: number) {
		this._numFamWorking = value;
	}

	public set knownByDealer(value: boolean) {
		this._knownByDealer = value;
	}

  public set italianCitizen(value: boolean) {
		this._italianCitizen = value;
	}

  public set otherLoans(value: boolean) {
		this._otherLoans = value;
	}

  public set signatory(value: boolean) {
		this._signatory = value;
	}

  public set redditoAnnuo(value: number) {
		this._redditoAnnuo = value;
	}

  public set pep(value: boolean) {
		this._pep = value;
	}

	public get codPostazione(): number {
		return this._codPostazione;
	}
	public set codPostazione(value: number) {
		this._codPostazione = value;
	}

	public get codiceLegale(): number {
		return this._codiceLegale;
	}
	public set codiceLegale(value: number) {
		this._codiceLegale = value;
	}
  
	public get codSoggettoLinkato(): number {
		return this._codSoggettoLinkato;
	}
	public set codSoggettoLinkato(value: number) {
		this._codSoggettoLinkato = value;
	}

	public get legalAddress(): ConsultationSubjAddress {
		return this._legalAddress;
	}
	public set legalAddress(value: ConsultationSubjAddress) {
		this._legalAddress = value;
	}
  
}

export class ConsultationSubjectsFactory {
	
	constructor(){
	}

	getInstance(obj: ConsultationSubjects): ConsultationSubjects {
		let _utils = new UtilsService();
		let consSubjects = new ConsultationSubjects();
		let consDocumentsFactory = new ConsultationSubjectsDocFactory();
		let consBankFactory = new ConsultationSubjectsBankFactory();
		let consSubjAddressFactory = new ConsultationSubjAddressFactory();

		if(obj) {
			if(!_utils.isVoid(obj.type)) consSubjects.type = obj.type;
			if(!_utils.isVoid(obj.legameSoggetto)) consSubjects.legameSoggetto = obj.legameSoggetto;
			if(!_utils.isVoid(obj.legameProposta)) consSubjects.legameProposta = obj.legameProposta;
			if(!_utils.isVoid(obj.subjectCode)) consSubjects.subjectCode = obj.subjectCode;
			if(!_utils.isVoid(obj.codSoggettoLinkato)) consSubjects.codSoggettoLinkato = obj.codSoggettoLinkato;
			if(!_utils.isVoid(obj.codiceLegale)) consSubjects.codiceLegale = obj.codiceLegale;
			if(!_utils.isVoid(obj.codPostazione)) consSubjects.codPostazione = obj.codPostazione;
			if(!_utils.isVoid(obj.codTipoSoggetto)) consSubjects.codTipoSoggetto = obj.codTipoSoggetto;
			if(!_utils.isVoid(obj.codSae)) consSubjects.codSae = obj.codSae;
			if(!_utils.isVoid(obj.flagPF)) consSubjects.flagPF = obj.flagPF;
			if(!_utils.isVoid(obj.fiscalCode)) consSubjects.fiscalCode = obj.fiscalCode;
			if(!_utils.isVoid(obj.ragioneSociale)) consSubjects.ragioneSociale = obj.ragioneSociale;
			if(!_utils.isVoid(obj.flagPrivacy)) consSubjects.flagPrivacy = obj.flagPrivacy;
			if(!_utils.isVoid(obj.banks)) consSubjects.banks = consBankFactory.getInstanceList(obj.banks);
			if(!_utils.isVoid(obj.documents)) consSubjects.documents = consDocumentsFactory.getInstanceList(obj.documents);
			if(!_utils.isVoid(obj.addresses)) consSubjects.addresses = consSubjAddressFactory.getInstanceList(obj.addresses);
			if(!_utils.isVoid(obj.codRuoloSoggetto)) consSubjects.codRuoloSoggetto = obj.codRuoloSoggetto;
			if(!_utils.isVoid(obj.surname)) consSubjects.surname = obj.surname;
			if(!_utils.isVoid(obj.genderCode)) consSubjects.genderCode = obj.genderCode;
			if(!_utils.isVoid(obj.name)) consSubjects.name = obj.name;
			if(!_utils.isVoid(obj.birthDate)) consSubjects.birthDate = obj.birthDate;
			if(!_utils.isVoid(obj.birthPlace)) consSubjects.birthPlace = obj.birthPlace;
			if(!_utils.isVoid(obj.gender)) consSubjects.gender = obj.gender;
			if(!_utils.isVoid(obj.communityPropertyState)) consSubjects.communityPropertyState = obj.communityPropertyState;
			if(!_utils.isVoid(obj.married)) consSubjects.married = obj.married;
			if(!_utils.isVoid(obj.annualIncome)) consSubjects.annualIncome = obj.annualIncome;
			if(!_utils.isVoid(obj.dependentFamilyMembers)) consSubjects.dependentFamilyMembers = obj.dependentFamilyMembers;
			if(!_utils.isVoid(obj.household)) consSubjects.household = obj.household;
			if(!_utils.isVoid(obj.landsNumber)) consSubjects.landsNumber = obj.landsNumber;
			if(!_utils.isVoid(obj.landsValue)) consSubjects.landsValue = obj.landsValue;
			if(!_utils.isVoid(obj.landsMortgage)) consSubjects.landsMortgage = obj.landsMortgage;
			if(!_utils.isVoid(obj.buildingsNumber)) consSubjects.buildingsNumber = obj.buildingsNumber;
			if(!_utils.isVoid(obj.buildingsValue)) consSubjects.buildingsValue = obj.buildingsValue;
			if(!_utils.isVoid(obj.buildingsMortgage)) consSubjects.buildingsMortgage = obj.buildingsMortgage;
			if(!_utils.isVoid(obj.carsOwned)) consSubjects.carsOwned = obj.carsOwned;
			if(!_utils.isVoid(obj.carsUsed)) consSubjects.carsUsed = obj.carsUsed;
			if(!_utils.isVoid(obj.creditCardType)) consSubjects.creditCardType = obj.creditCardType;
			if(!_utils.isVoid(obj.numFamWorking)) consSubjects.numFamWorking = obj.numFamWorking;
			if(!_utils.isVoid(obj.knownByDealer)) consSubjects.knownByDealer = obj.knownByDealer;
			if(!_utils.isVoid(obj.italianCitizen)) consSubjects.italianCitizen = obj.italianCitizen;
			if(!_utils.isVoid(obj.otherLoans)) consSubjects.otherLoans = obj.otherLoans;
			if(!_utils.isVoid(obj.signatory)) consSubjects.signatory = obj.signatory;
			if(!_utils.isVoid(obj.redditoAnnuo)) consSubjects.redditoAnnuo = obj.redditoAnnuo;
			if(!_utils.isVoid(obj.pep)) consSubjects.pep = obj.pep;
			if(!_utils.isVoid(obj.legalAddress)) consSubjects.legalAddress = consSubjAddressFactory.getInstance(obj.legalAddress);

			return consSubjects;
		}
	}
}

export class ConsultationSubjAddress {
	private _addressCode?: string;
	private _progressivo?: number;
	private _houseNumber?: string;
	private _cap?: string;
	private _province?: string;
	private _address?: string;
	private _place?: string;
	private _contactInfo?: any[];
	private _descTipoIndirizzo?: string;
	private _tempoIndirizzo?: number;
	private _cityCode?: number;
	private _nationCode?: number;

	constructor(addressCode?: string,
		progressivo?: number,
		houseNumber?: string,
		cap?: string,
		province?: string,
		address?: string,
		place?: string,
		contactInfo?: any[],
		descTipoIndirizzo?: string,
		tempoIndirizzo?: number,
		cityCode?: number,
		nationCode?: number) {

			this._addressCode = addressCode;
			this._progressivo = progressivo;
			this._houseNumber = houseNumber;
			this._cap = cap;
			this._province = province;
			this._address = address;
			this._place = place;
			this._contactInfo = contactInfo;
			this._descTipoIndirizzo = descTipoIndirizzo;
			this._tempoIndirizzo = tempoIndirizzo;
			this._cityCode = cityCode;
			this._nationCode = nationCode
	}

	public get addressCode(): string {
		return this._addressCode;
	}
	public set addressCode(value: string) {
		this._addressCode = value;
	}
	
	public get progressivo(): number {
		return this._progressivo;
	}
	public set progressivo(value: number) {
		this._progressivo = value;
	}
	
	public get houseNumber(): string {
		return this._houseNumber;
	}
	public set houseNumber(value: string) {
		this._houseNumber = value;
	}
	
	public get cap(): string {
		return this._cap;
	}
	public set cap(value: string) {
		this._cap = value;
	}
	
	public get province(): string {
		return this._province;
	}
	public set province(value: string) {
		this._province = value;
	}
	
	public get address(): string {
		return this._address;
	}
	public set address(value: string) {
		this._address = value;
	}
	
	public get place(): string {
		return this._place;
	}
	public set place(value: string) {
		this._place = value;
	}
	
	public get contactInfo(): any[] {
		return this._contactInfo;
	}
	public set contactInfo(value: any[]) {
		this._contactInfo = value;
	}
	
	public get descTipoIndirizzo(): string {
		return this._descTipoIndirizzo;
	}
	public set descTipoIndirizzo(value: string) {
		this._descTipoIndirizzo = value;
	}
	
	public get tempoIndirizzo(): number {
		return this._tempoIndirizzo;
	}
	public set tempoIndirizzo(value: number) {
		this._tempoIndirizzo = value;
	}
	
	public get cityCode(): number {
		return this._cityCode;
	}
	public set cityCode(value: number) {
		this._cityCode = value;
	}

	public get nationCode(): number {
		return this._nationCode;
	}
	public set nationCode(value: number) {
		this._nationCode = value;
	}
}

export class ConsultationSubjAddressFactory {

	getInstance(obj: ConsultationSubjAddress): ConsultationSubjAddress {
		let _utils = new UtilsService();
		let consSubAddr = new ConsultationSubjAddress();

		if(obj){
			if(_utils.isVoid(obj.address)) consSubAddr.address = obj.address;
			if(_utils.isVoid(obj.addressCode)) consSubAddr.addressCode = obj.addressCode;
			if(_utils.isVoid(obj.cap)) consSubAddr.cap = obj.cap;
			if(_utils.isVoid(obj.cityCode)) consSubAddr.cityCode = obj.cityCode;
			if(_utils.isVoid(obj.contactInfo)) consSubAddr.contactInfo = obj.contactInfo;
			if(_utils.isVoid(obj.descTipoIndirizzo)) consSubAddr.descTipoIndirizzo = obj.descTipoIndirizzo;
			if(_utils.isVoid(obj.houseNumber)) consSubAddr.houseNumber = obj.houseNumber;
			if(_utils.isVoid(obj.nationCode)) consSubAddr.nationCode = obj.nationCode;
			if(_utils.isVoid(obj.place)) consSubAddr.place = obj.place;
			if(_utils.isVoid(obj.progressivo)) consSubAddr.progressivo = obj.progressivo;
			if(_utils.isVoid(obj.province)) consSubAddr.province = obj.province;
			if(_utils.isVoid(obj.tempoIndirizzo)) consSubAddr.tempoIndirizzo = obj.tempoIndirizzo;

			return consSubAddr;
		}
	}

	getInstanceList(list: any[]): ConsultationSubjAddress[] {
		let cList: ConsultationSubjAddress[] = [];
		if(list && list.length > 0){
			list.forEach(elem => {
				cList.push(this.getInstance(elem));
			})
		}
		return cList;
	}
}