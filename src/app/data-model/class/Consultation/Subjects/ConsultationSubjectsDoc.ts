import { UtilsService } from '../../../../shared/services/utils.service';
export class ConsultationSubjectsDoc {
  private _idTipoDocumento?: string;
  private _numDocumento?: string;
  private _luogoRilascio?: string;
  private _dataRilascio?: string;
  private _dataScadenza?: string;
  private _seqNum?: number;
  private _descrDocumento?: string;
  private _countryCode?: string;
  private _provincia?: string;
  private _agencyCode?: string;

  constructor(
    idTipoDocumento?: string,
    numDocumento?: string,
    luogoRilascio?: string,
    dataRilascio?: string,
    dataScadenza?: string,
    seqNum?: number,
    descrDocumento?: string,
    countryCode?: string,
    provincia?: string,
    agencyCode?: string
  ){
    this._idTipoDocumento = idTipoDocumento;
    this._numDocumento = numDocumento;
    this._luogoRilascio = luogoRilascio;
    this._dataRilascio = dataRilascio;
    this._dataScadenza = dataScadenza;
    this._seqNum = seqNum;
    this._descrDocumento = descrDocumento;
    this._countryCode = countryCode;
    this._provincia = provincia;
    this._agencyCode = agencyCode;
  }


    /**
     * Getter idTipoDocumento
     * @return {string}
     */
	public get idTipoDocumento(): string {
		return this._idTipoDocumento;
	}

    /**
     * Getter numDocumento
     * @return {string}
     */
	public get numDocumento(): string {
		return this._numDocumento;
	}

    /**
     * Getter luogoRilascio
     * @return {string}
     */
	public get luogoRilascio(): string {
		return this._luogoRilascio;
	}

    /**
     * Getter dataRilascio
     * @return {string}
     */
	public get dataRilascio(): string {
		return this._dataRilascio;
	}

    /**
     * Getter dataScadenza
     * @return {string}
     */
	public get dataScadenza(): string {
		return this._dataScadenza;
	}

    /**
     * Getter seqNum
     * @return {number}
     */
	public get seqNum(): number {
		return this._seqNum;
	}

    /**
     * Getter descrDocumento
     * @return {string}
     */
	public get descrDocumento(): string {
		return this._descrDocumento;
	}

    /**
     * Getter countryCode
     * @return {string}
     */
	public get countryCode(): string {
		return this._countryCode;
	}

    /**
     * Getter provincia
     * @return {string}
     */
	public get provincia(): string {
		return this._provincia;
	}

    /**
     * Getter agencyCode
     * @return {string}
     */
	public get agencyCode(): string {
		return this._agencyCode;
	}

    /**
     * Setter idTipoDocumento
     * @param {string} value
     */
	public set idTipoDocumento(value: string) {
		this._idTipoDocumento = value;
	}

    /**
     * Setter numDocumento
     * @param {string} value
     */
	public set numDocumento(value: string) {
		this._numDocumento = value;
	}

    /**
     * Setter luogoRilascio
     * @param {string} value
     */
	public set luogoRilascio(value: string) {
		this._luogoRilascio = value;
	}

    /**
     * Setter dataRilascio
     * @param {string} value
     */
	public set dataRilascio(value: string) {
		this._dataRilascio = value;
	}

    /**
     * Setter dataScadenza
     * @param {string} value
     */
	public set dataScadenza(value: string) {
		this._dataScadenza = value;
	}

    /**
     * Setter seqNum
     * @param {number} value
     */
	public set seqNum(value: number) {
		this._seqNum = value;
	}

    /**
     * Setter descrDocumento
     * @param {string} value
     */
	public set descrDocumento(value: string) {
		this._descrDocumento = value;
	}

    /**
     * Setter countryCode
     * @param {string} value
     */
	public set countryCode(value: string) {
		this._countryCode = value;
	}

    /**
     * Setter provincia
     * @param {string} value
     */
	public set provincia(value: string) {
		this._provincia = value;
	}

    /**
     * Setter agencyCode
     * @param {string} value
     */
	public set agencyCode(value: string) {
		this._agencyCode = value;
	}

}

export class ConsultationSubjectsDocFactory{
  constructor(){

  }

  getInstance(obj: ConsultationSubjectsDoc): ConsultationSubjectsDoc {
    let _utils = new UtilsService();
    let consSubjectsDoc = new ConsultationSubjectsDoc();

    if(obj){
      if(!_utils.isVoid(obj.agencyCode)) consSubjectsDoc.agencyCode = obj.agencyCode;
      if(!_utils.isVoid(obj.countryCode)) consSubjectsDoc.countryCode = obj.countryCode;
      if(!_utils.isVoid(obj.dataRilascio)) consSubjectsDoc.dataRilascio = obj.dataRilascio;
      if(!_utils.isVoid(obj.dataScadenza)) consSubjectsDoc.dataScadenza = obj.dataScadenza;
      if(!_utils.isVoid(obj.descrDocumento)) consSubjectsDoc.descrDocumento = obj.descrDocumento;
      if(!_utils.isVoid(obj.idTipoDocumento)) consSubjectsDoc.idTipoDocumento = obj.idTipoDocumento;
      if(!_utils.isVoid(obj.luogoRilascio)) consSubjectsDoc.luogoRilascio = obj.luogoRilascio;
      if(!_utils.isVoid(obj.numDocumento)) consSubjectsDoc.numDocumento = obj.numDocumento;
      if(!_utils.isVoid(obj.provincia)) consSubjectsDoc.provincia = obj.provincia;
      if(!_utils.isVoid(obj.seqNum)) consSubjectsDoc.seqNum = obj.seqNum;

      return consSubjectsDoc;
    }
  }

  getInstanceList(list: any): ConsultationSubjectsDoc[] {
    let cList: ConsultationSubjectsDoc[] = [];
    if (list && list.length > 0) {
        list.forEach(elem => {
            cList.push(this.getInstance(elem));
        });
    }
    return cList;
  }
}