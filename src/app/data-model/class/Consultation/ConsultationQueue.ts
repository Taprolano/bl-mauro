import {ConsultationCommercialQueue} from "./ConsultationCommercialQueue";
import {ConsultationFinancialQueue} from "./ConsultationFinancialQueue";
import {ConsultationHeaderQueue} from "./ConsultationHeaderQueue";
import {ConsultationProposalProductQueue} from "./ConsultationProposalProductQueue";
import {ConsultationSubjectQueue} from "./ConsultationSubjectQueue";

export class ConsultationQueue {
    private _commercial: ConsultationCommercialQueue;
    private _financial: ConsultationFinancialQueue;
    private _header: ConsultationHeaderQueue;
    private _proposta: ConsultationProposalProductQueue;
    private _soggetto: ConsultationSubjectQueue;


    constructor(commercial?: ConsultationCommercialQueue,
                financial?: ConsultationFinancialQueue,
                header?: ConsultationHeaderQueue,
                proposta?: ConsultationProposalProductQueue,
                soggetto?: ConsultationSubjectQueue) {
        this._commercial = commercial;
        this._financial = financial;
        this._header = header;
        this._proposta = proposta;
        this._soggetto = soggetto;
    }


    set commercial(value: ConsultationCommercialQueue) {
        this._commercial = value;
    }

    set financial(value: ConsultationFinancialQueue) {
        this._financial = value;
    }

    set header(value: ConsultationHeaderQueue) {
        this._header = value;
    }

    set proposta(value: ConsultationProposalProductQueue) {
        this._proposta = value;
    }

    set soggetto(value: ConsultationSubjectQueue) {
        this._soggetto = value;
    }

    get commercial(): ConsultationCommercialQueue {
        return this._commercial;
    }

    get financial(): ConsultationFinancialQueue {
        return this._financial;
    }

    get header(): ConsultationHeaderQueue {
        return this._header;
    }

    get proposta(): ConsultationProposalProductQueue {
        return this._proposta;
    }

    get soggetto(): ConsultationSubjectQueue {
        return this._soggetto;
    }
}

export class ConsultationQueueFactory {
    getInstance(obj: ConsultationQueue): ConsultationQueue {
        let consultationQueue = new ConsultationQueue();
        return consultationQueue;
    }
}

