import {UtilsService} from "../../../shared/services/utils.service";

export class BlockedFields {
    private _tan?: boolean;
    private _rata?: boolean;
    private _balloon?: boolean;
    private _anticipo?: boolean;


    constructor(tan?: boolean, rata?: boolean, balloon?: boolean, anticipo?: boolean) {
        this._tan = tan;
        this._rata = rata;
        this._balloon = balloon;
        this._anticipo = anticipo;
    }

    get tan(): boolean {
        return this._tan;
    }

    set tan(value: boolean) {
        this._tan = value;
    }

    get rata(): boolean {
        return this._rata;
    }

    set rata(value: boolean) {
        this._rata = value;
    }

    get balloon(): boolean {
        return this._balloon;
    }

    set balloon(value: boolean) {
        this._balloon = value;
    }

    get anticipo(): boolean {
        return this._anticipo;
    }

    set anticipo(value: boolean) {
        this._anticipo = value;
    }
}

export class BlockedFieldsFactory{
    getInstance(obj: BlockedFields): BlockedFields{
        let blockedFields = new BlockedFields();
        let _utils: UtilsService = new UtilsService();
        if(!_utils.isVoid(obj)) {
            if(!_utils.isVoid(obj.anticipo)) blockedFields.anticipo = obj.anticipo;
            if(!_utils.isVoid(obj.balloon)) blockedFields.balloon = obj.balloon;
            if(!_utils.isVoid(obj.rata)) blockedFields.rata = obj.rata;
            if(!_utils.isVoid(obj.tan)) blockedFields.tan = obj.tan;
        }

        return blockedFields;
    }

}
