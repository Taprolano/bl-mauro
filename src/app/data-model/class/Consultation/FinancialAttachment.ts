
export class FinancialAttachment{
    private _contentType?: string;
    private _url?: string;
    private _name?: string;


    constructor(contentType?: string, url?: string, name?: string) {
        this._contentType = contentType;
        this._url = url;
        this._name = name;
    }

    /** @returns {string} */
    get contentType(): string {
        return this._contentType;
    }

    /** @returns {string} */
    get url(): string {
        return this._url;
    }

    /** @returns {string} */
    get name(): string {
        return this._name;
    }

    /** @param {string} value */
    set contentType(value: string) {
        this._contentType = value;
    }

    /** @param {string} value */
    set url(value: string) {
        this._url = value;
    }

    /** @param {string} value */
    set name(value: string) {
        this._name = value;
    }
}

export class FinancialAttachmentFactory{

    getInstance(obj: FinancialAttachment): FinancialAttachment{
        let fincialAttachment = new FinancialAttachment();

        if(obj){
            if (obj.contentType != undefined) fincialAttachment.contentType = obj.contentType;
            if (obj.url != undefined) fincialAttachment.url = obj.url;
            if (obj.name != undefined) fincialAttachment.name = obj.name;
        }

        return fincialAttachment;

    }

    getInstanceList(list:any): FinancialAttachment[] {
        let fincialAttachmentList = [];
        if(list && list.length > 0){
            list.forEach(elem => {
                fincialAttachmentList.push(this.getInstance(elem));
            });
        }
        return fincialAttachmentList;
    }
}