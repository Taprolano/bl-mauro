export class ConsultationSubjectQueue {
    private _soggetto?: string;
    private _cfPiva?: string;
    private _tipoSoggetto?: string;
    private _iban?: string;

    constructor(soggetto?: string, cfPiva?: string, tipoSoggetto?: string, iban?: string) {
        this._soggetto = soggetto;
        this._cfPiva = cfPiva;
        this._tipoSoggetto = tipoSoggetto;
        this._iban = iban;
    }

    public get soggetto(): string {
        return this._soggetto;
    }

    public set soggetto(value: string) {
        this._soggetto = value;
    }

    public get cfPiva(): string {
        return this._cfPiva;
    }

    public set cfPiva(value: string) {
        this._cfPiva = value;
    }

    public get tipoSoggetto(): string {
        return this._tipoSoggetto;
    }

    public set tipoSoggetto(value: string) {
        this._tipoSoggetto = value;
    }

    public get iban(): string {
        return this._iban;
    }

    public set iban(value: string) {
        this._iban = value;
    }
}

export class ConsultationSubjectQueueFactory {
    getInstance(obj: ConsultationSubjectQueue): ConsultationSubjectQueue {
        let consultationQueueSubject = new ConsultationSubjectQueue();

        if (obj) {
            if (obj.cfPiva != undefined) consultationQueueSubject.cfPiva = obj.cfPiva;
            if (obj.iban != undefined) consultationQueueSubject.iban = obj.iban;
            if (obj.soggetto != undefined) consultationQueueSubject.soggetto = obj.soggetto;
            if (obj.tipoSoggetto != undefined) consultationQueueSubject.tipoSoggetto = obj.tipoSoggetto;
        }

        return consultationQueueSubject;
    }
}
