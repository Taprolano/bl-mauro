import {UtilsService} from "../../../shared/services/utils.service";

export class ProvvigioniDTO {
    private _provImportoDeltaTasso?: number;
    private _provImportoDeltaTassoPerc?: number;
    private _provImportoServizi?: number;
    private _provImportoServiziPerc?: number;
    private _provImportoSpeseIstruttoria?: number;
    private _provImportoSpeseIstruttoriaPerc?: number;
    private _provImportoTotale?: number;
    private _provImportoVendita?: number;
    private _provImportoVenditaPerc?: number;


    constructor(provImportoDeltaTasso?: number, provImportoDeltaTassoPerc?: number, provImportoServizi?: number,
                provImportoServiziPerc?: number, provImportoSpeseIstruttoria?: number, provImportoSpeseIstruttoriaPerc?: number,
                provImportoTotale?: number, provImportoVendita?: number, provImportoVenditaPerc?: number) {
        this._provImportoDeltaTasso = provImportoDeltaTasso;
        this._provImportoDeltaTassoPerc = provImportoDeltaTassoPerc;
        this._provImportoServizi = provImportoServizi;
        this._provImportoServiziPerc = provImportoServiziPerc;
        this._provImportoSpeseIstruttoria = provImportoSpeseIstruttoria;
        this._provImportoSpeseIstruttoriaPerc = provImportoSpeseIstruttoriaPerc;
        this._provImportoTotale = provImportoTotale;
        this._provImportoVendita = provImportoVendita;
        this._provImportoVenditaPerc = provImportoVenditaPerc;
    }


    set provImportoDeltaTasso(value: number) {
        this._provImportoDeltaTasso = value;
    }

    set provImportoDeltaTassoPerc(value: number) {
        this._provImportoDeltaTassoPerc = value;
    }

    set provImportoServizi(value: number) {
        this._provImportoServizi = value;
    }

    set provImportoServiziPerc(value: number) {
        this._provImportoServiziPerc = value;
    }

    set provImportoSpeseIstruttoria(value: number) {
        this._provImportoSpeseIstruttoria = value;
    }

    set provImportoSpeseIstruttoriaPerc(value: number) {
        this._provImportoSpeseIstruttoriaPerc = value;
    }

    set provImportoTotale(value: number) {
        this._provImportoTotale = value;
    }

    set provImportoVendita(value: number) {
        this._provImportoVendita = value;
    }

    set provImportoVenditaPerc(value: number) {
        this._provImportoVenditaPerc = value;
    }

    get provImportoDeltaTasso(): number {
        return this._provImportoDeltaTasso;
    }

    get provImportoDeltaTassoPerc(): number {
        return this._provImportoDeltaTassoPerc;
    }

    get provImportoServizi(): number {
        return this._provImportoServizi;
    }

    get provImportoServiziPerc(): number {
        return this._provImportoServiziPerc;
    }

    get provImportoSpeseIstruttoria(): number {
        return this._provImportoSpeseIstruttoria;
    }

    get provImportoSpeseIstruttoriaPerc(): number {
        return this._provImportoSpeseIstruttoriaPerc;
    }

    get provImportoTotale(): number {
        return this._provImportoTotale;
    }

    get provImportoVendita(): number {
        return this._provImportoVendita;
    }

    get provImportoVenditaPerc(): number {
        return this._provImportoVenditaPerc;
    }
}

export class ProvvigioniDTOFactory {
    getInstance(obj: ProvvigioniDTO): ProvvigioniDTO {
        let _utils: UtilsService = new UtilsService();
        let provvigioniDTO = new ProvvigioniDTO();

        if (obj) {
            if (!_utils.isVoid(obj.provImportoDeltaTasso)) provvigioniDTO.provImportoDeltaTasso = obj.provImportoDeltaTasso;
            if (!_utils.isVoid(obj.provImportoDeltaTassoPerc)) provvigioniDTO.provImportoDeltaTassoPerc = obj.provImportoDeltaTassoPerc;
            if (!_utils.isVoid(obj.provImportoServizi)) provvigioniDTO.provImportoServizi = obj.provImportoServizi;
            if (!_utils.isVoid(obj.provImportoServiziPerc)) provvigioniDTO.provImportoServiziPerc = obj.provImportoServiziPerc;
            if (!_utils.isVoid(obj.provImportoSpeseIstruttoria)) provvigioniDTO.provImportoSpeseIstruttoria = obj.provImportoSpeseIstruttoria;
            if (!_utils.isVoid(obj.provImportoSpeseIstruttoriaPerc)) provvigioniDTO.provImportoSpeseIstruttoriaPerc = obj.provImportoSpeseIstruttoriaPerc;
            if (!_utils.isVoid(obj.provImportoTotale)) provvigioniDTO.provImportoTotale = obj.provImportoTotale;
            if (!_utils.isVoid(obj.provImportoVendita)) provvigioniDTO.provImportoVendita = obj.provImportoVendita;
            if (!_utils.isVoid(obj.provImportoVenditaPerc)) provvigioniDTO.provImportoVenditaPerc = obj.provImportoVenditaPerc;
        }

        return provvigioniDTO;
    }

    getInstanceList(list: any): ProvvigioniDTO[] {
        let provvigioniDTOlist = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                provvigioniDTOlist.push(this.getInstance(elem));
            });
        }
        return provvigioniDTOlist;
    }
}
