import {ServiceEntity} from "./ServiceEntity";
import {UtilsService} from "../../../shared/services/utils.service";

export class Campaign{
    private _code?: string;
    private _description?: string;
    private _expiryDate?: string;
    private _finalExpiryDate?: string;
    private _internalProposal?: boolean;
    private _mustUseSpIstr?: boolean;
    private _speseIstruttoria?: number;
    private _defaultValue?: boolean;
    private _adminFeeShareOverride?: boolean;
    private _commCamp?: boolean;
    private _dealManuContrComm?: boolean;
    private _flagShare?: boolean;
    private _selFixComAmount?: number;
    private _selFixComPerc?: number;
    private _manuFixContrComm?: number;
    private _manuPercContrComm?: number;
    private _dealPercManuContrComm?: number;
    private _dealFixCom?: number;
    private _dealPercCom?: number;


    constructor(code?: string, description?: string, expiryDate?: string, finalExpiryDate?: string, internalProposal?: boolean, mustUseSpIstr?: boolean, speseIstruttoria?: number, defaultValue?: boolean, adminFeeShareOverride?: boolean, commCamp?: boolean, dealManuContrComm?: boolean, flagShare?: boolean, selFixComAmount?: number, selFixComPerc?: number, manuFixContrComm?: number, manuPercContrComm?: number, dealPercManuContrComm?: number, dealFixCom?: number, dealPercCom?: number) {
        this._code = code;
        this._description = description;
        this._expiryDate = expiryDate;
        this._finalExpiryDate = finalExpiryDate;
        this._internalProposal = internalProposal;
        this._mustUseSpIstr = mustUseSpIstr;
        this._speseIstruttoria = speseIstruttoria;
        this._defaultValue = defaultValue;
        this._adminFeeShareOverride = adminFeeShareOverride;
        this._commCamp = commCamp;
        this._dealManuContrComm = dealManuContrComm;
        this._flagShare = flagShare;
        this._selFixComAmount = selFixComAmount;
        this._selFixComPerc = selFixComPerc;
        this._manuFixContrComm = manuFixContrComm;
        this._manuPercContrComm = manuPercContrComm;
        this._dealPercManuContrComm = dealPercManuContrComm;
        this._dealFixCom = dealFixCom;
        this._dealPercCom = dealPercCom;
    }


    get code(): string {
        return this._code;
    }

    get description(): string {
        return this._description;
    }

    get expiryDate(): string {
        return this._expiryDate;
    }

    get finalExpiryDate(): string {
        return this._finalExpiryDate;
    }

    get internalProposal(): boolean {
        return this._internalProposal;
    }

    get mustUseSpIstr(): boolean {
        return this._mustUseSpIstr;
    }

    get speseIstruttoria(): number {
        return this._speseIstruttoria;
    }

    get defaultValue(): boolean {
        return this._defaultValue;
    }

    get adminFeeShareOverride(): boolean {
        return this._adminFeeShareOverride;
    }

    get commCamp(): boolean {
        return this._commCamp;
    }

    get dealManuContrComm(): boolean {
        return this._dealManuContrComm;
    }

    get flagShare(): boolean {
        return this._flagShare;
    }

    get selFixComAmount(): number {
        return this._selFixComAmount;
    }

    get selFixComPerc(): number {
        return this._selFixComPerc;
    }

    get manuFixContrComm(): number {
        return this._manuFixContrComm;
    }

    get manuPercContrComm(): number {
        return this._manuPercContrComm;
    }

    get dealPercManuContrComm(): number {
        return this._dealPercManuContrComm;
    }

    get dealFixCom(): number {
        return this._dealFixCom;
    }

    get dealPercCom(): number {
        return this._dealPercCom;
    }

    set code(value: string) {
        this._code = value;
    }

    set description(value: string) {
        this._description = value;
    }

    set expiryDate(value: string) {
        this._expiryDate = value;
    }

    set finalExpiryDate(value: string) {
        this._finalExpiryDate = value;
    }

    set internalProposal(value: boolean) {
        this._internalProposal = value;
    }

    set mustUseSpIstr(value: boolean) {
        this._mustUseSpIstr = value;
    }

    set speseIstruttoria(value: number) {
        this._speseIstruttoria = value;
    }

    set defaultValue(value: boolean) {
        this._defaultValue = value;
    }

    set adminFeeShareOverride(value: boolean) {
        this._adminFeeShareOverride = value;
    }

    set commCamp(value: boolean) {
        this._commCamp = value;
    }

    set dealManuContrComm(value: boolean) {
        this._dealManuContrComm = value;
    }

    set flagShare(value: boolean) {
        this._flagShare = value;
    }

    set selFixComAmount(value: number) {
        this._selFixComAmount = value;
    }

    set selFixComPerc(value: number) {
        this._selFixComPerc = value;
    }

    set manuFixContrComm(value: number) {
        this._manuFixContrComm = value;
    }

    set manuPercContrComm(value: number) {
        this._manuPercContrComm = value;
    }

    set dealPercManuContrComm(value: number) {
        this._dealPercManuContrComm = value;
    }

    set dealFixCom(value: number) {
        this._dealFixCom = value;
    }

    set dealPercCom(value: number) {
        this._dealPercCom = value;
    }
}

export class CampaignFactory{
    getInstance(obj: Campaign): Campaign{
        let campaign = new Campaign();
        let _utils: UtilsService = new UtilsService();

        if (obj) {
            if (!_utils.isVoid(obj.code)) campaign.code = obj.code;
            if (!_utils.isVoid(obj.description)) campaign.description = obj.description;
            if (!_utils.isVoid(obj.expiryDate)) campaign.expiryDate = obj.expiryDate;
            if (!_utils.isVoid(obj.finalExpiryDate)) campaign.finalExpiryDate = obj.finalExpiryDate;
            if (!_utils.isVoid(obj.internalProposal)) campaign.internalProposal = obj.internalProposal;
            if (!_utils.isVoid(obj.mustUseSpIstr)) campaign.mustUseSpIstr = obj.mustUseSpIstr;
            if (!_utils.isVoid(obj.speseIstruttoria)) campaign.speseIstruttoria = obj.speseIstruttoria;
            if (!_utils.isVoid(obj.defaultValue)) campaign.defaultValue = obj.defaultValue;
            if (!_utils.isVoid(obj.adminFeeShareOverride)) campaign.adminFeeShareOverride = obj.adminFeeShareOverride;
            if (!_utils.isVoid(obj.commCamp)) campaign.commCamp = obj.commCamp;
            if (!_utils.isVoid(obj.dealManuContrComm)) campaign.dealManuContrComm = obj.dealManuContrComm;
            if (!_utils.isVoid(obj.flagShare)) campaign.flagShare = obj.flagShare;
            if (!_utils.isVoid(obj.selFixComAmount)) campaign.selFixComAmount = obj.selFixComAmount;
            if (!_utils.isVoid(obj.selFixComPerc)) campaign.selFixComPerc = obj.selFixComPerc;
            if (!_utils.isVoid(obj.manuFixContrComm)) campaign.manuFixContrComm = obj.manuFixContrComm;
            if (!_utils.isVoid(obj.manuPercContrComm)) campaign.manuPercContrComm = obj.manuPercContrComm;
            if (!_utils.isVoid(obj.dealPercManuContrComm)) campaign.dealPercManuContrComm = obj.dealPercManuContrComm;
            if (!_utils.isVoid(obj.dealFixCom)) campaign.dealFixCom = obj.dealFixCom;
            if (!_utils.isVoid(obj.dealPercCom)) campaign.dealPercCom = obj.dealPercCom;
        }

        return campaign;
    }

    getInstanceList(list: any): Campaign[] {
        let campaignList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                campaignList.push(this.getInstance(elem));
            });
        }
        return campaignList;
    }

}
