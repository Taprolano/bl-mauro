export class ConsultationHeaderQueue {
    private _proposta?: number;
    private _asset?: string;
    private _soggetto?: string;
    private _tipoSoggetto?: string;
    private _financialProductDescription?: string;
    private _contractLength?: number;
    private _balloon?: number;
    private _anticipo?: number;
    private _rateAmount?: number;
    private _financiedTotalAmount?: number;
    private _venditore?: string;

    constructor(proposta?: number, asset?: string, soggetto?: string, tipoSoggetto?: string,
                financialProductDescription?: string, contractLength?: number, balloon?: number, anticipo?: number,
                rateAmount?: number, financiedTotalAmount?: number, venditore?: string) {
        this._proposta = proposta;
        this._asset = asset;
        this._soggetto = soggetto;
        this._tipoSoggetto = tipoSoggetto;
        this._financialProductDescription = financialProductDescription;
        this._contractLength = contractLength;
        this._balloon = balloon;
        this._anticipo = anticipo;
        this._rateAmount = rateAmount;
        this._financiedTotalAmount = financiedTotalAmount;
        this._venditore = venditore;
    }

    public get proposta(): number {
        return this._proposta;
    }

    public set proposta(value: number) {
        this._proposta = value;
    }

    public get asset(): string {
        return this._asset;
    }

    public set asset(value: string) {
        this._asset = value;
    }

    public get soggetto(): string {
        return this._soggetto;
    }

    public set soggetto(value: string) {
        this._soggetto = value;
    }

    public get tipoSoggetto(): string {
        return this._tipoSoggetto;
    }

    public set tipoSoggetto(value: string) {
        this._tipoSoggetto = value;
    }

    public get financialProductDescription(): string {
        return this._financialProductDescription;
    }

    public set financialProductDescription(value: string) {
        this._financialProductDescription = value;
    }

    public get contractLength(): number {
        return this._contractLength;
    }

    public set contractLength(value: number) {
        this._contractLength = value;
    }

    public get balloon(): number {
        return this._balloon;
    }

    public set balloon(value: number) {
        this._balloon = value;
    }

    public get anticipo(): number {
        return this._anticipo;
    }

    public set anticipo(value: number) {
        this._anticipo = value;
    }

    public get rateAmount(): number {
        return this._rateAmount;
    }

    public set rateAmount(value: number) {
        this._rateAmount = value;
    }

    public get financiedTotalAmount(): number {
        return this._financiedTotalAmount;
    }

    public set financiedTotalAmount(value: number) {
        this._financiedTotalAmount = value;
    }

    public get venditore(): string {
        return this._venditore;
    }

    public set venditore(value: string) {
        this._venditore = value;
    }
}

export class ConsultationHeaderQueueFactory {
    getInstance(obj: ConsultationHeaderQueue): ConsultationHeaderQueue {
        let consultationQueueHeader = new ConsultationHeaderQueue();

        if (obj) {
            if (obj.proposta != undefined)  consultationQueueHeader.proposta = obj.proposta;
            if (obj.asset != undefined)  consultationQueueHeader.asset = obj.asset;
            if (obj.soggetto != undefined)  consultationQueueHeader.soggetto = obj.soggetto;
            if (obj.tipoSoggetto != undefined)  consultationQueueHeader.tipoSoggetto = obj.tipoSoggetto;
            if (obj.financialProductDescription != undefined)  consultationQueueHeader.financialProductDescription = obj.financialProductDescription;
            if (obj.financiedTotalAmount != undefined)  consultationQueueHeader.financiedTotalAmount = obj.financiedTotalAmount;
            if (obj.contractLength != undefined)  consultationQueueHeader.contractLength = obj.contractLength;
            if (obj.balloon != undefined)  consultationQueueHeader.balloon = obj.balloon;
            if (obj.anticipo != undefined)  consultationQueueHeader.anticipo = obj.anticipo;
            if (obj.anticipo != undefined)  consultationQueueHeader.anticipo = obj.anticipo;
            if (obj.rateAmount != undefined)  consultationQueueHeader.rateAmount = obj.rateAmount;
            if (obj.venditore != undefined)  consultationQueueHeader.venditore = obj.venditore;

        }

        return consultationQueueHeader;
    }
}