import {Campaign, CampaignFactory} from "./Campaign";
import {UtilsService} from "../../../shared/services/utils.service";

export class FinancialProcuct {
    private _fpCode?: string;
    private _fpDescription?: string;
    private _defaultValue?: string;
    private _campaigns?: Campaign[];


    constructor(fpCode?: string, fpDescription?: string, defaultValue?: string, campaigns?: Campaign[]) {
        this._fpCode = fpCode;
        this._fpDescription = fpDescription;
        this._defaultValue = defaultValue;
        this._campaigns = campaigns;
    }


    get fpCode(): string {
        return this._fpCode;
    }

    get fpDescription(): string {
        return this._fpDescription;
    }

    get defaultValue(): string {
        return this._defaultValue;
    }

    get campaigns(): Campaign[] {
        return this._campaigns;
    }

    set fpCode(value: string) {
        this._fpCode = value;
    }

    set fpDescription(value: string) {
        this._fpDescription = value;
    }

    set defaultValue(value: string) {
        this._defaultValue = value;
    }

    set campaigns(value: Campaign[]) {
        this._campaigns = value;
    }
}

export class FinancialProcuctFactory {
    getInstance(obj: FinancialProcuct): FinancialProcuct {
        let financialProduct = new FinancialProcuct();
        let _utils: UtilsService = new UtilsService();
        let campaignFactory = new CampaignFactory();

        if (obj) {
            if (!_utils.isVoid(obj.fpCode)) financialProduct.fpCode = obj.fpCode;
            if (!_utils.isVoid(obj.fpDescription)) financialProduct.fpDescription = obj.fpDescription;
            if (!_utils.isVoid(obj.defaultValue)) financialProduct.defaultValue = obj.defaultValue;
            if (!_utils.isVoid(obj.campaigns)) financialProduct.campaigns = campaignFactory.getInstanceList(obj.campaigns);
        }

        return financialProduct;
    }

    getInstanceList(list: any): FinancialProcuct[] {
        let financialProcuctList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialProcuctList.push(this.getInstance(elem));
            });
        }
        return financialProcuctList;
    }
}
