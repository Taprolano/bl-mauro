import {FinancialServicesInner, FinancialServicesInnerFactory} from "./FinancialServiceInner";
import {FinancialAttachment, FinancialAttachmentFactory} from "./FinancialAttachment";
import {Rate, RateFactory} from "./Rate";

export class ServiceEntity {

    private _type?: string;
    private _supplyTypeCode?: string;
    private _supplyTypeDesc?: string;
    private _defaultElement?: boolean;
    private _defaultSupply?: boolean;
    private _updatable?: boolean;
    private _retrieveVat?: boolean;
    private _tassoProdotto?: boolean;
    private _beneStrumentale?: boolean;
    private _rate?: Rate[];
    private _attachments?: FinancialAttachment[];
    private _opzioneServizio?: any;
    private _serviceEntities?: FinancialServicesInner[];


    constructor(type?: string, supplyTypeCode?: string, supplyTypeDesc?: string, defaultElement?: boolean,
                defaultSupply?: boolean, updatable?: boolean, retrieveVat?: boolean, tassoProdotto?: boolean,
                beneStrumentale?: boolean, rate?: Rate[], attachments?: FinancialAttachment[],
                opzioneServizio?: any, serviceEntities?: FinancialServicesInner[]) {
        this._type = type;
        this._supplyTypeCode = supplyTypeCode;
        this._supplyTypeDesc = supplyTypeDesc;
        this._defaultElement = defaultElement;
        this._defaultSupply = defaultSupply;
        this._updatable = updatable;
        this._retrieveVat = retrieveVat;
        this._tassoProdotto = tassoProdotto;
        this._beneStrumentale = beneStrumentale;
        this._rate = rate;
        this._attachments = attachments;
        this._opzioneServizio = opzioneServizio;
        this._serviceEntities = serviceEntities;
    }


    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get supplyTypeCode(): string {
        return this._supplyTypeCode;
    }

    set supplyTypeCode(value: string) {
        this._supplyTypeCode = value;
    }

    get supplyTypeDesc(): string {
        return this._supplyTypeDesc;
    }

    set supplyTypeDesc(value: string) {
        this._supplyTypeDesc = value;
    }

    get defaultElement(): boolean {
        return this._defaultElement;
    }

    set defaultElement(value: boolean) {
        this._defaultElement = value;
    }

    get defaultSupply(): boolean {
        return this._defaultSupply;
    }

    set defaultSupply(value: boolean) {
        this._defaultSupply = value;
    }

    get updatable(): boolean {
        return this._updatable;
    }

    set updatable(value: boolean) {
        this._updatable = value;
    }

    get retrieveVat(): boolean {
        return this._retrieveVat;
    }

    set retrieveVat(value: boolean) {
        this._retrieveVat = value;
    }

    get tassoProdotto(): boolean {
        return this._tassoProdotto;
    }

    set tassoProdotto(value: boolean) {
        this._tassoProdotto = value;
    }

    get beneStrumentale(): boolean {
        return this._beneStrumentale;
    }

    set beneStrumentale(value: boolean) {
        this._beneStrumentale = value;
    }

    get rate(): Rate[] {
        return this._rate;
    }

    set rate(value: Rate[]) {
        this._rate = value;
    }

    get attachments(): FinancialAttachment[] {
        return this._attachments;
    }

    set attachments(value: FinancialAttachment[]) {
        this._attachments = value;
    }

    get opzioneServizio(): any {
        return this._opzioneServizio;
    }

    set opzioneServizio(value: any) {
        this._opzioneServizio = value;
    }

    get serviceEntities(): FinancialServicesInner[] {
        return this._serviceEntities;
    }

    set serviceEntities(value: FinancialServicesInner[]) {
        this._serviceEntities = value;
    }
}

export class ServiceEntityFactory {

    getInstance(obj: ServiceEntity): ServiceEntity {
        let finServices = new ServiceEntity();


        let attachFactory = new FinancialAttachmentFactory();
        let attachList = attachFactory.getInstanceList(obj.attachments);

        let financialServicesInnerFact = new FinancialServicesInnerFactory();
        let finServicesInner = financialServicesInnerFact.getInstanceList(obj.serviceEntities);

        if (obj) {
            if (obj.beneStrumentale != undefined) finServices.beneStrumentale = obj.beneStrumentale;
            if (obj.defaultElement != undefined) finServices.defaultElement = obj.defaultElement;
            if (obj.defaultSupply != undefined) finServices.defaultSupply = obj.defaultSupply;
            if (obj.retrieveVat != undefined) finServices.retrieveVat = obj.retrieveVat;
            if (obj.serviceEntities != undefined) finServices.serviceEntities = finServicesInner;
            if (obj.supplyTypeCode != undefined) finServices.supplyTypeCode = obj.supplyTypeCode;
            if (obj.supplyTypeDesc != undefined) finServices.supplyTypeDesc = obj.supplyTypeDesc;
            if (obj.tassoProdotto != undefined) finServices.tassoProdotto = obj.tassoProdotto;
            if (obj.opzioneServizio != undefined) finServices.opzioneServizio = obj.opzioneServizio;
            if (obj.updatable != undefined) finServices.updatable = obj.updatable;
            if (obj.attachments != undefined) finServices.attachments = attachList;
        }

        return finServices;
    }


    getInstanceList(list: any): ServiceEntity[] {

        let financialService = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialService.push(this.getInstance(elem));
            });
        }
        return financialService;
    }

}
