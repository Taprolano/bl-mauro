import { ConsultationSubjectQueue } from './ConsultationSubjectQueue';
import { ConsultationProposalProductQueue } from './ConsultationProposalProductQueue';

export class ConsultationProposalQueue {
  private _subject?: ConsultationSubjectQueue
  private _product?: ConsultationProposalProductQueue;
  

  constructor(product?: ConsultationProposalProductQueue){
    this._product = product;
  }

  public get product(): ConsultationProposalProductQueue {
    return this._product;
  }
  public set product(value: ConsultationProposalProductQueue) {
    this._product = value;
  }

  public get subject(): ConsultationSubjectQueue {
    return this._subject;
  }
  public set subject(value: ConsultationSubjectQueue) {
    this._subject = value;
  }
}

export class ConsultationProposalQueueFactory {
  getInstance(obj: ConsultationProposalQueue): ConsultationProposalQueue {
    let consultationPropQueue = new ConsultationProposalQueue();
    return consultationPropQueue;
  }
}