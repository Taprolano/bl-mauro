
export class Rate {
    private _anticipo?: boolean;
    private _balloon?: boolean;
    private _bloccato?: boolean;
    private _cashFlowTypeCode?: string;
    private _cfAmount?: number;
    private _cfAmountPercentage?: number;
    private _dataPagamentoRata?: string;
    private _distanza?: number;
    private _molteplicita?: number;
    private _numberOfTimes?: number;
    private _tassoFisso?: boolean;
    private _totalAmount?: number;
    private _vatAmount?: number;
    private _vatPercentage?: number;

    /* COSTRUTTORE */

    /**
     *
     * @param {boolean} anticipo
     * @param {boolean} balloon
     * @param {boolean} bloccato
     * @param {string} cashFlowTypeCode
     * @param {number} cfAmount
     * @param {number} cfAmountPercentage
     * @param {string} dataPagamentoRata
     * @param {number} distanza
     * @param {number} molteplicita
     * @param {number} numberOfTimes
     * @param {boolean} tassoFisso
     * @param {number} totalAmount
     * @param {number} vatAmount
     * @param {number} vatPercentage
     */
    constructor(anticipo?: boolean, balloon?: boolean, bloccato?: boolean, cashFlowTypeCode?: string, cfAmount?: number, cfAmountPercentage?: number, dataPagamentoRata?: string, distanza?: number, molteplicita?: number, numberOfTimes?: number, tassoFisso?: boolean, totalAmount?: number, vatAmount?: number, vatPercentage?: number) {
        this._anticipo = anticipo;
        this._balloon = balloon;
        this._bloccato = bloccato;
        this._cashFlowTypeCode = cashFlowTypeCode;
        this._cfAmount = cfAmount;
        this._cfAmountPercentage = cfAmountPercentage;
        this._dataPagamentoRata = dataPagamentoRata;
        this._distanza = distanza;
        this._molteplicita = molteplicita;
        this._numberOfTimes = numberOfTimes;
        this._tassoFisso = tassoFisso;
        this._totalAmount = totalAmount;
        this._vatAmount = vatAmount;
        this._vatPercentage = vatPercentage;
    }

    /* GET */
    /** @returns {boolean} */
    get anticipo(): boolean {
        return this._anticipo;
    }

    /** @returns {boolean} */
    get balloon(): boolean {
        return this._balloon;
    }

    /** @returns {boolean} */
    get bloccato(): boolean {
        return this._bloccato;
    }

    /** @returns {string} */
    get cashFlowTypeCode(): string {
        return this._cashFlowTypeCode;
    }

    /** @returns {number} */
    get cfAmount(): number {
        return this._cfAmount;
    }

    /** @returns {number} */
    get cfAmountPercentage(): number {
        return this._cfAmountPercentage;
    }

    /** @returns {string} */
    get dataPagamentoRata(): string {
        return this._dataPagamentoRata;
    }

    /** @returns {number} */
    get distanza(): number {
        return this._distanza;
    }

    /** @returns {number} */
    get molteplicita(): number {
        return this._molteplicita;
    }

    /** @returns {number} */
    get numberOfTimes(): number {
        return this._numberOfTimes;
    }

    /** @returns {boolean} */
    get tassoFisso(): boolean {
        return this._tassoFisso;
    }

    /** @returns {number} */
    get totalAmount(): number {
        return this._totalAmount;
    }

    /** @returns {number} */
    get vatAmount(): number {
        return this._vatAmount;
    }

    /** @returns {number} */
    get vatPercentage(): number {
        return this._vatPercentage;
    }


    /* SET */

    /** @param {boolean} value */
    set anticipo(value: boolean) {
        this._anticipo = value;
    }

    /** @param {boolean} value */
    set balloon(value: boolean) {
        this._balloon = value;
    }

    /** @param {boolean} value */
    set bloccato(value: boolean) {
        this._bloccato = value;
    }

    /** @param {string} value */
    set cashFlowTypeCode(value: string) {
        this._cashFlowTypeCode = value;
    }

    /** @param {number} value */
    set cfAmount(value: number) {
        this._cfAmount = value;
    }

    /** @param {number} value */
    set cfAmountPercentage(value: number) {
        this._cfAmountPercentage = value;
    }

    /** @param {string} value */
    set dataPagamentoRata(value: string) {
        this._dataPagamentoRata = value;
    }

    /** @param {number} value */
    set distanza(value: number) {
        this._distanza = value;
    }

    /** @param {number} value */
    set molteplicita(value: number) {
        this._molteplicita = value;
    }

    /** @param {number} value */
    set numberOfTimes(value: number) {
        this._numberOfTimes = value;
    }

    /** @param {boolean} value */
    set tassoFisso(value: boolean) {
        this._tassoFisso = value;
    }

    /** @param {number} value */
    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    /** @param {number} value */
    set vatAmount(value: number) {
        this._vatAmount = value;
    }

    /** @param {number} value */
    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }
}

export class RateFactory{

    /**
     *
     * @param {Rate} obj
     * @returns {Rate}
     */
    getInstance(obj:Rate): Rate {
        let addProp = new Rate();

        if (obj.anticipo != undefined) addProp.anticipo = obj.anticipo;
        if (obj.balloon != undefined) addProp.balloon = obj.balloon;
        if (obj.bloccato != undefined) addProp.bloccato = obj.bloccato;
        if (obj.cashFlowTypeCode != undefined) addProp.cashFlowTypeCode = obj.cashFlowTypeCode;
        if (obj.cfAmount != undefined) addProp.cfAmount = obj.cfAmount;
        if (obj.cfAmountPercentage != undefined) addProp.cfAmountPercentage = obj.cfAmountPercentage;
        if (obj.dataPagamentoRata != undefined) addProp.dataPagamentoRata = obj.dataPagamentoRata;
        if (obj.distanza != undefined) addProp.distanza = obj.distanza;
        if (obj.molteplicita != undefined) addProp.molteplicita = obj.molteplicita;
        if (obj.numberOfTimes != undefined) addProp.numberOfTimes = obj.numberOfTimes;
        if (obj.tassoFisso != undefined) addProp.tassoFisso = obj.tassoFisso;
        if (obj.totalAmount != undefined) addProp.totalAmount = obj.totalAmount;
        if (obj.vatAmount != undefined) addProp.vatAmount = obj.vatAmount;
        if (obj.vatPercentage != undefined) addProp.vatPercentage = obj.vatPercentage;


        return addProp;
    }

    /**
     *
     * @param list
     * @returns {Rate[]}
     */
    getInstanceList(list:any): Rate[] {
        let additionalPropList = [];
        if(list && list.length > 0){
            list.forEach(elem => {
                additionalPropList.push(this.getInstance(elem));
            });
        }
        return additionalPropList;
    }
}
