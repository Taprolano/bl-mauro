import { UtilsService } from '../../../shared/services/utils.service';
export class ConsultationProposalProductQueue {
  private _prodotto?: string;
  private _modello?: string;
  private _versione?: string;
  private _registration?: string;
  private _used?: boolean;
  private _mileage?: string;

  constructor(prodotto?: string, modello?: string, versione?: string, registration?: string, 
    used?: boolean, mileage?: string) {

    this.prodotto = prodotto;
    this.modello = modello;
    this.versione = versione;
    this.registration = registration;
    this.used = used;
    this.mileage = mileage;
  }

  public get prodotto(): string {
    return this._prodotto;
  }
  public set prodotto(value: string) {
    this._prodotto = value;
  }

  public get modello(): string {
    return this._modello;
  }
  public set modello(value: string) {
    this._modello = value;
  }

  public get versione(): string {
    return this._versione;
  }
  public set versione(value: string) {
    this._versione = value;
  }

  public get registration(): string {
    return this._registration;
  }
  public set registration(value: string) {
    this._registration = value;
  }

  public get used(): boolean {
    return this._used;
  }
  public set used(value: boolean) {
    this._used = value;
  }

  public get mileage(): string {
    return this._mileage;
  }
  public set mileage(value: string) {
    this._mileage = value;
  }
}

export class ConsultationProposalProdQueueFactory {
  getInstance(obj: ConsultationProposalProductQueue): ConsultationProposalProductQueue {
    let consultationPropProdQueue = new ConsultationProposalProductQueue();
    let utils = new UtilsService();
    if (!utils.isVoid(obj)) {
      if(!utils.isVoid(obj.mileage)) consultationPropProdQueue.mileage = obj.mileage;
      if(!utils.isVoid(obj.modello)) consultationPropProdQueue.modello = obj.modello;
      if(!utils.isVoid(obj.prodotto)) consultationPropProdQueue.prodotto = obj.prodotto;
      if(!utils.isVoid(obj.registration)) consultationPropProdQueue.registration = obj.registration;
      if(!utils.isVoid(obj.used)) consultationPropProdQueue.used = obj.used;
      if(!utils.isVoid(obj.versione)) consultationPropProdQueue.versione = obj.versione;
    }

    return consultationPropProdQueue;
  }
}