import {ServiceEntity, ServiceEntityFactory} from "./ServiceEntity";
import {BlockedFields, BlockedFieldsFactory} from "./BlockFields";
import {ProvvigioniDTO, ProvvigioniDTOFactory} from "./ProvvigioniDTO";
import {Rate, RateFactory} from "./Rate";
import {FinancialProcuct, FinancialProcuctFactory} from "./FinancialProcuct";
import {UtilsService} from "../../../shared/services/utils.service";
import {IOptional} from "../../interface/IOptional";

export class ConsultationFinancialQueue {

    private _balloonPercentuale: number;
    private _derogaCommerciale: boolean;
    private _operazioneK2K: boolean;
    private _codiceTipoInteresse: string;
    private _dealerBuyBack: boolean;
    private _importoListino: number;
    private _importoFornitura: number;
    private _ipt: number;
    private _version: number;
    private _assetType: string;
    private _financedAmount: number;
    private _rate: Rate[];
    private _assetModel: string;
    private _prodottoFinanziario: string;
    private _used: boolean;
    private _distance: number;
    private _selFixComPerc: number;
    private _dealPercCom: number;
    private _dealFixCom: number;
    private _dealManuContrComm: boolean;
    private _manuFixContrComm: number;
    private _manuPercContrComm: number;
    private _dealPercManuContrComm: number;
    private _flagShare: boolean;
    private _balloon: boolean;
    private _baumuster: string;
    private _brandId: string;
    private _assetClassCode: string;
    private _financialProductCode: string;
    private _contractLength: number;
    private _idProposal: string;
    private _proposalStatus: string;
    private _proposalDate: string;
    private _campaignCode: string;
    private _optionals: IOptional[];
    private _makeCode: boolean;
    private _netCost: number;
    private _mss: number;
    private _adminFeeShareOverride: boolean;
    private _mustUseSpIstr: boolean;
    private _commCamp: boolean;
    private _selFixComAmount: number;
    private _leasing: boolean;
    private _rental: boolean;
    private _dealerCDTer: number;
    private _spread: number;
    private _tassoBase: number;
    private _vatPercentage: number;
    private _sellerSIpercentage: number;
    private _dealerSIpercentage: number;
    private _sellerCommission: number;
    private _valoreCommissione: number;
    private _percentAtDealer: number;
    private _percentAtSales: number;
    private _anticipoPercentuale: number;
    private _speseDiIstruttoria: number;
    private _tipoInteresse: string;
    private _codeIpoteca: string;
    private _totalAmount: number;
    private _importoAnticipo: number;
    private _importoRataBalloon: number;
    private _provImportoVendita: number;
    private _provImportoVenditaPerc: number;
    private _provImportoSpeseIstruttoria: number;
    private _provImportoSpeseIstruttoriaPerc: number;
    private _provImportoServizi: number;
    private _provImportoServiziPerc: number;
    private _provImportoDeltaTasso: number;
    private _provImportoDeltaTassoPerc: number;
    private _provImportoTotale: number;

    constructor(balloonPercentuale?: number,
                derogaCommerciale?: boolean,
                operazioneK2K?: boolean,
                codiceTipoInteresse?: string,
                dealerBuyBack?: boolean,
                importoListino?: number,
                importoFornitura?: number,
                ipt?: number,
                version?: number,
                assetType?: string,
                financedAmount?: number,
                rate?: Rate[],
                ssetModel?: string,
                rodottoFinanziario?: string,
                sed?: boolean,
                istance?: number,
                elFixComPerc?: number,
                ealPercCom?: number,
                dealFixCom?: number,
                dealManuContrComm?: boolean,
                manuFixContrComm?: number,
                manuPercContrComm?: number,
                dealPercManuContrComm?: number,
                flagShare?: boolean,
                balloon?: boolean,
                baumuster?: string,
                brandId?: string,
                assetClassCode?: string,
                financialProductCode?: string,
                contractLength?: number,
                idProposal?: string,
                proposalStatus?: string,
                proposalDate?: string,
                campaignCode?: string,
                optionals?: IOptional[],
                makeCode?: boolean,
                netCost?: number,
                mss?: number,
                adminFeeShareOverride?: boolean,
                mustUseSpIstr?: boolean,
                commCamp?: boolean,
                selFixComAmount?: number,
                leasing?: boolean,
                rental?: boolean,
                dealerCDTer?: number,
                spread?: number,
                tassoBase?: number,
                vatPercentage?: number,
                sellerSIpercentage?: number,
                dealerSIpercentage?: number,
                sellerCommission?: number,
                valoreCommissione?: number,
                percentAtDealer?: number,
                percentAtSales?: number,
                anticipoPercentuale?: number,
                speseDiIstruttoria?: number,
                tipoInteresse?: string,
                codeIpoteca?: string,
                totalAmount?: number,
                importoAnticipo?: number,
                importoRataBalloon?: number,
                provImportoVendita?: number,
                provImportoVenditaPerc?: number,
                provImportoSpeseIstruttoria?: number,
                provImportoSpeseIstruttoriaPerc?: number,
                provImportoServizi?: number,
                provImportoServiziPerc?: number,
                provImportoDeltaTasso?: number,
                provImportoDeltaTassoPerc?: number,
                provImportoTotale?: number
    ) {
        this._balloonPercentuale = balloonPercentuale;
        this._derogaCommerciale = derogaCommerciale;
        this._operazioneK2K = operazioneK2K;
        this._codiceTipoInteresse = codiceTipoInteresse;
        this._dealerBuyBack = dealerBuyBack;
        this._importoListino = importoListino;
        this._importoFornitura = importoFornitura;
        this._ipt = ipt;
        this._version = version;
        this._assetType = assetType;
        this._financedAmount = financedAmount;
        this._rate = rate;
        this._assetModel = ssetModel;
        this._prodottoFinanziario = rodottoFinanziario;
        this._used = sed;
        this._distance = istance;
        this._selFixComPerc = elFixComPerc;
        this._dealPercCom = ealPercCom;
        this._dealFixCom = dealFixCom;
        this._dealManuContrComm = dealManuContrComm;
        this._manuFixContrComm = manuFixContrComm;
        this._manuPercContrComm = manuPercContrComm;
        this._dealPercManuContrComm = dealPercManuContrComm;
        this._flagShare = flagShare;
        this._balloon = balloon;
        this._baumuster = baumuster;
        this._brandId = brandId;
        this._assetClassCode = assetClassCode;
        this._financialProductCode = financialProductCode;
        this._contractLength = contractLength;
        this._idProposal = idProposal;
        this._proposalStatus = proposalStatus;
        this._proposalDate = proposalDate;
        this._campaignCode = campaignCode;
        this._optionals = optionals;
        this._makeCode = makeCode;
        this._netCost = netCost;
        this._mss = mss;
        this._adminFeeShareOverride = adminFeeShareOverride;
        this._mustUseSpIstr = mustUseSpIstr;
        this._commCamp = commCamp;
        this._selFixComAmount = selFixComAmount;
        this._leasing = leasing;
        this._rental = rental;
        this._dealerCDTer = dealerCDTer;
        this._spread = spread;
        this._tassoBase = tassoBase;
        this._vatPercentage = vatPercentage;
        this._sellerSIpercentage = sellerSIpercentage;
        this._dealerSIpercentage = dealerSIpercentage;
        this._sellerCommission = sellerCommission;
        this._valoreCommissione = valoreCommissione;
        this._percentAtDealer = percentAtDealer;
        this._percentAtSales = percentAtSales;
        this._anticipoPercentuale = anticipoPercentuale;
        this._speseDiIstruttoria = speseDiIstruttoria;
        this._tipoInteresse = tipoInteresse;
        this._codeIpoteca = codeIpoteca;
        this._totalAmount = totalAmount;
        this._importoAnticipo = importoAnticipo;
        this._importoRataBalloon = importoRataBalloon;
        this._provImportoVendita = provImportoVendita;
        this._provImportoVenditaPerc = provImportoVenditaPerc;
        this._provImportoSpeseIstruttoria = provImportoSpeseIstruttoria;
        this._provImportoSpeseIstruttoriaPerc = provImportoSpeseIstruttoriaPerc;
        this._provImportoServizi = provImportoServizi;
        this._provImportoServiziPerc = provImportoServiziPerc;
        this._provImportoDeltaTasso = provImportoDeltaTasso;
        this._provImportoDeltaTassoPerc = provImportoDeltaTassoPerc;
        this._provImportoTotale = provImportoTotale;
    }


    get balloonPercentuale(): number {
        return this._balloonPercentuale;
    }

    set balloonPercentuale(value: number) {
        this._balloonPercentuale = value;
    }

    get derogaCommerciale(): boolean {
        return this._derogaCommerciale;
    }

    set derogaCommerciale(value: boolean) {
        this._derogaCommerciale = value;
    }

    get operazioneK2K(): boolean {
        return this._operazioneK2K;
    }

    set operazioneK2K(value: boolean) {
        this._operazioneK2K = value;
    }

    get codiceTipoInteresse(): string {
        return this._codiceTipoInteresse;
    }

    set codiceTipoInteresse(value: string) {
        this._codiceTipoInteresse = value;
    }

    get dealerBuyBack(): boolean {
        return this._dealerBuyBack;
    }

    set dealerBuyBack(value: boolean) {
        this._dealerBuyBack = value;
    }

    get importoListino(): number {
        return this._importoListino;
    }

    set importoListino(value: number) {
        this._importoListino = value;
    }

    get importoFornitura(): number {
        return this._importoFornitura;
    }

    set importoFornitura(value: number) {
        this._importoFornitura = value;
    }

    get ipt(): number {
        return this._ipt;
    }

    set ipt(value: number) {
        this._ipt = value;
    }

    get version(): number {
        return this._version;
    }

    set version(value: number) {
        this._version = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get financedAmount(): number {
        return this._financedAmount;
    }

    set financedAmount(value: number) {
        this._financedAmount = value;
    }

    get rate(): Rate[] {
        return this._rate;
    }

    set rate(value: Rate[]) {
        this._rate = value;
    }

    get assetModel(): string {
        return this._assetModel;
    }

    set assetModel(value: string) {
        this._assetModel = value;
    }

    get prodottoFinanziario(): string {
        return this._prodottoFinanziario;
    }

    set prodottoFinanziario(value: string) {
        this._prodottoFinanziario = value;
    }

    get used(): boolean {
        return this._used;
    }

    set used(value: boolean) {
        this._used = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get selFixComPerc(): number {
        return this._selFixComPerc;
    }

    set selFixComPerc(value: number) {
        this._selFixComPerc = value;
    }

    get dealPercCom(): number {
        return this._dealPercCom;
    }

    set dealPercCom(value: number) {
        this._dealPercCom = value;
    }

    get dealFixCom(): number {
        return this._dealFixCom;
    }

    set dealFixCom(value: number) {
        this._dealFixCom = value;
    }

    get dealManuContrComm(): boolean {
        return this._dealManuContrComm;
    }

    set dealManuContrComm(value: boolean) {
        this._dealManuContrComm = value;
    }

    get manuFixContrComm(): number {
        return this._manuFixContrComm;
    }

    set manuFixContrComm(value: number) {
        this._manuFixContrComm = value;
    }

    get manuPercContrComm(): number {
        return this._manuPercContrComm;
    }

    set manuPercContrComm(value: number) {
        this._manuPercContrComm = value;
    }

    get dealPercManuContrComm(): number {
        return this._dealPercManuContrComm;
    }

    set dealPercManuContrComm(value: number) {
        this._dealPercManuContrComm = value;
    }

    get flagShare(): boolean {
        return this._flagShare;
    }

    set flagShare(value: boolean) {
        this._flagShare = value;
    }

    get balloon(): boolean {
        return this._balloon;
    }

    set balloon(value: boolean) {
        this._balloon = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    get idProposal(): string {
        return this._idProposal;
    }

    set idProposal(value: string) {
        this._idProposal = value;
    }

    get proposalStatus(): string {
        return this._proposalStatus;
    }

    set proposalStatus(value: string) {
        this._proposalStatus = value;
    }

    get proposalDate(): string {
        return this._proposalDate;
    }

    set proposalDate(value: string) {
        this._proposalDate = value;
    }

    get campaignCode(): string {
        return this._campaignCode;
    }

    set campaignCode(value: string) {
        this._campaignCode = value;
    }

    get optionals(): IOptional[] {
        return this._optionals;
    }

    set optionals(value: IOptional[]) {
        this._optionals = value;
    }

    get makeCode(): boolean {
        return this._makeCode;
    }

    set makeCode(value: boolean) {
        this._makeCode = value;
    }

    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    get mss(): number {
        return this._mss;
    }

    set mss(value: number) {
        this._mss = value;
    }

    get adminFeeShareOverride(): boolean {
        return this._adminFeeShareOverride;
    }

    set adminFeeShareOverride(value: boolean) {
        this._adminFeeShareOverride = value;
    }

    get mustUseSpIstr(): boolean {
        return this._mustUseSpIstr;
    }

    set mustUseSpIstr(value: boolean) {
        this._mustUseSpIstr = value;
    }

    get commCamp(): boolean {
        return this._commCamp;
    }

    set commCamp(value: boolean) {
        this._commCamp = value;
    }

    get selFixComAmount(): number {
        return this._selFixComAmount;
    }

    set selFixComAmount(value: number) {
        this._selFixComAmount = value;
    }

    get leasing(): boolean {
        return this._leasing;
    }

    set leasing(value: boolean) {
        this._leasing = value;
    }

    get rental(): boolean {
        return this._rental;
    }

    set rental(value: boolean) {
        this._rental = value;
    }

    get dealerCDTer(): number {
        return this._dealerCDTer;
    }

    set dealerCDTer(value: number) {
        this._dealerCDTer = value;
    }

    get spread(): number {
        return this._spread;
    }

    set spread(value: number) {
        this._spread = value;
    }

    get tassoBase(): number {
        return this._tassoBase;
    }

    set tassoBase(value: number) {
        this._tassoBase = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    get sellerSIpercentage(): number {
        return this._sellerSIpercentage;
    }

    set sellerSIpercentage(value: number) {
        this._sellerSIpercentage = value;
    }

    get dealerSIpercentage(): number {
        return this._dealerSIpercentage;
    }

    set dealerSIpercentage(value: number) {
        this._dealerSIpercentage = value;
    }

    get sellerCommission(): number {
        return this._sellerCommission;
    }

    set sellerCommission(value: number) {
        this._sellerCommission = value;
    }

    get valoreCommissione(): number {
        return this._valoreCommissione;
    }

    set valoreCommissione(value: number) {
        this._valoreCommissione = value;
    }

    get percentAtDealer(): number {
        return this._percentAtDealer;
    }

    set percentAtDealer(value: number) {
        this._percentAtDealer = value;
    }

    get percentAtSales(): number {
        return this._percentAtSales;
    }

    set percentAtSales(value: number) {
        this._percentAtSales = value;
    }

    get anticipoPercentuale(): number {
        return this._anticipoPercentuale;
    }

    set anticipoPercentuale(value: number) {
        this._anticipoPercentuale = value;
    }

    get speseDiIstruttoria(): number {
        return this._speseDiIstruttoria;
    }

    set speseDiIstruttoria(value: number) {
        this._speseDiIstruttoria = value;
    }

    get tipoInteresse(): string {
        return this._tipoInteresse;
    }

    set tipoInteresse(value: string) {
        this._tipoInteresse = value;
    }

    get codeIpoteca(): string {
        return this._codeIpoteca;
    }

    set codeIpoteca(value: string) {
        this._codeIpoteca = value;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    get importoAnticipo(): number {
        return this._importoAnticipo;
    }

    set importoAnticipo(value: number) {
        this._importoAnticipo = value;
    }

    get importoRataBalloon(): number {
        return this._importoRataBalloon;
    }

    set importoRataBalloon(value: number) {
        this._importoRataBalloon = value;
    }

    get provImportoVendita(): number {
        return this._provImportoVendita;
    }

    set provImportoVendita(value: number) {
        this._provImportoVendita = value;
    }

    get provImportoVenditaPerc(): number {
        return this._provImportoVenditaPerc;
    }

    set provImportoVenditaPerc(value: number) {
        this._provImportoVenditaPerc = value;
    }

    get provImportoSpeseIstruttoria(): number {
        return this._provImportoSpeseIstruttoria;
    }

    set provImportoSpeseIstruttoria(value: number) {
        this._provImportoSpeseIstruttoria = value;
    }

    get provImportoSpeseIstruttoriaPerc(): number {
        return this._provImportoSpeseIstruttoriaPerc;
    }

    set provImportoSpeseIstruttoriaPerc(value: number) {
        this._provImportoSpeseIstruttoriaPerc = value;
    }

    get provImportoServizi(): number {
        return this._provImportoServizi;
    }

    set provImportoServizi(value: number) {
        this._provImportoServizi = value;
    }

    get provImportoServiziPerc(): number {
        return this._provImportoServiziPerc;
    }

    set provImportoServiziPerc(value: number) {
        this._provImportoServiziPerc = value;
    }

    get provImportoDeltaTasso(): number {
        return this._provImportoDeltaTasso;
    }

    set provImportoDeltaTasso(value: number) {
        this._provImportoDeltaTasso = value;
    }

    get provImportoDeltaTassoPerc(): number {
        return this._provImportoDeltaTassoPerc;
    }

    set provImportoDeltaTassoPerc(value: number) {
        this._provImportoDeltaTassoPerc = value;
    }

    get provImportoTotale(): number {
        return this._provImportoTotale;
    }

    set provImportoTotale(value: number) {
        this._provImportoTotale = value;
    }
}

export class ConsultationFinancialQueueFactory {
    getInstance(obj: ConsultationFinancialQueue): ConsultationFinancialQueue {
        let consFinacialQueue = new ConsultationFinancialQueue();
        let _utils: UtilsService = new UtilsService();

        let rateFactory = new RateFactory();

        if (obj) {
            /*if (!_utils.isVoid(obj.rate = rate;
            if (!_utils.isVoid(obj.optionals = optionals;*/



            if (!_utils.isVoid(obj.balloonPercentuale)
            if (!_utils.isVoid(obj.derogaCommerciale)
            if (!_utils.isVoid(obj.operazioneK2K)
            if (!_utils.isVoid(obj.codiceTipoInteresse)
            if (!_utils.isVoid(obj.dealerBuyBack)
            if (!_utils.isVoid(obj.importoListino)
            if (!_utils.isVoid(obj.importoFornitura)
            if (!_utils.isVoid(obj.ipt)
            if (!_utils.isVoid(obj.version)
            if (!_utils.isVoid(obj.assetType)
            if (!_utils.isVoid(obj.financedAmount)
            if (!_utils.isVoid(obj.assetModel)
            if (!_utils.isVoid(obj.prodottoFinanziario)
            if (!_utils.isVoid(obj.used)
            if (!_utils.isVoid(obj.distance)
            if (!_utils.isVoid(obj.selFixComPerc)
            if (!_utils.isVoid(obj.dealPercCom)
            if (!_utils.isVoid(obj.dealFixCom)
            if (!_utils.isVoid(obj.dealManuContrComm)
            if (!_utils.isVoid(obj.manuFixContrComm)
            if (!_utils.isVoid(obj.manuPercContrComm)
            if (!_utils.isVoid(obj.dealPercManuContrComm)
            if (!_utils.isVoid(obj.flagShare)
            if (!_utils.isVoid(obj.balloon)
            if (!_utils.isVoid(obj.baumuster)
            if (!_utils.isVoid(obj.brandId)
            if (!_utils.isVoid(obj.assetClassCode)
            if (!_utils.isVoid(obj.financialProductCode)
            if (!_utils.isVoid(obj.contractLength)
            if (!_utils.isVoid(obj.idProposal)
            if (!_utils.isVoid(obj.proposalStatus)
            if (!_utils.isVoid(obj.proposalDate)
            if (!_utils.isVoid(obj.campaignCode)
            if (!_utils.isVoid(obj.makeCode)
            if (!_utils.isVoid(obj.netCost)
            if (!_utils.isVoid(obj.mss)
            if (!_utils.isVoid(obj.adminFeeShareOverride)
            if (!_utils.isVoid(obj.mustUseSpIstr)
            if (!_utils.isVoid(obj.commCamp)
            if (!_utils.isVoid(obj.selFixComAmount)
            if (!_utils.isVoid(obj.leasing)
            if (!_utils.isVoid(obj.rental)
            if (!_utils.isVoid(obj.dealerCDTer)
            if (!_utils.isVoid(obj.spread)
            if (!_utils.isVoid(obj.tassoBase)
            if (!_utils.isVoid(obj.vatPercentage)
            if (!_utils.isVoid(obj.sellerSIpercentage)
            if (!_utils.isVoid(obj.dealerSIpercentage)
            if (!_utils.isVoid(obj.sellerCommission)
            if (!_utils.isVoid(obj.valoreCommissione)
            if (!_utils.isVoid(obj.percentAtDealer)
            if (!_utils.isVoid(obj.percentAtSales)
            if (!_utils.isVoid(obj.anticipoPercentuale)
            if (!_utils.isVoid(obj.speseDiIstruttoria)
            if (!_utils.isVoid(obj.tipoInteresse)
            if (!_utils.isVoid(obj.codeIpoteca)
            if (!_utils.isVoid(obj.totalAmount)
            if (!_utils.isVoid(obj.importoAnticipo)
            if (!_utils.isVoid(obj.importoRataBalloon)
            if (!_utils.isVoid(obj.provImportoVendita)
            if (!_utils.isVoid(obj.provImportoVenditaPerc)
            if (!_utils.isVoid(obj.provImportoSpeseIstruttoria)
            if (!_utils.isVoid(obj.provImportoSpeseIstruttoriaPerc)
            if (!_utils.isVoid(obj.provImportoServizi)
            if (!_utils.isVoid(obj.provImportoServiziPerc)
            if (!_utils.isVoid(obj.provImportoDeltaTasso)
            if (!_utils.isVoid(obj.provImportoDeltaTassoPerc)
            if (!_utils.isVoid(obj.provImportoTotale)


            /*if (!_utils.isVoid(obj.financialProduct)) consFinacialQueue.financialProduct = financiailProductFactory.getInstanceList(obj.financialProduct);
            if (!_utils.isVoid(obj.serviceEntity)) consFinacialQueue.serviceEntity = serviceEntityFactory.getInstanceList(obj.serviceEntity);
            if (!_utils.isVoid(obj.campiBloccati)) consFinacialQueue.campiBloccati = blockFieldFactory.getInstance(obj.campiBloccati);
            if (!_utils.isVoid(obj.provvigioniDTO)) consFinacialQueue.provvigioniDTO = provvigioniDTOFactory.getInstance(obj.provvigioniDTO);
            if (!_utils.isVoid(obj.rate)) consFinacialQueue.rate = rateFactory.getInstanceList(obj.rate);

            if (!_utils.isVoid(obj.anticipoPercentuale)) consFinacialQueue.anticipoPercentuale = obj.anticipoPercentuale;
            if (!_utils.isVoid(obj.assetClassCode)) consFinacialQueue.assetClassCode = obj.assetClassCode;
            if (!_utils.isVoid(obj.assetType)) consFinacialQueue.assetType = obj.assetType;
            if (!_utils.isVoid(obj.balloonPercentuale)) consFinacialQueue.balloonPercentuale = obj.balloonPercentuale;
            if (!_utils.isVoid(obj.baumuster)) consFinacialQueue.baumuster = obj.baumuster;
            if (!_utils.isVoid(obj.brandId)) consFinacialQueue.brandId = obj.brandId;
            if (!_utils.isVoid(obj.campaignCode)) consFinacialQueue.campaignCode = obj.campaignCode;
            if (!_utils.isVoid(obj.campiBloccati)) consFinacialQueue.campiBloccati = obj.campiBloccati;
            if (!_utils.isVoid(obj.codAnticipo)) consFinacialQueue.codAnticipo = obj.codAnticipo;
            if (!_utils.isVoid(obj.codCanoneRata)) consFinacialQueue.codCanoneRata = obj.codCanoneRata;
            if (!_utils.isVoid(obj.codeIpoteca)) consFinacialQueue.codeIpoteca = obj.codeIpoteca;
            if (!_utils.isVoid(obj.contractLength)) consFinacialQueue.contractLength = obj.contractLength;
            if (!_utils.isVoid(obj.contributoCampagna)) consFinacialQueue.contributoCampagna = obj.contributoCampagna;
            if (!_utils.isVoid(obj.contributoConcessionario)) consFinacialQueue.contributoConcessionario = obj.contributoConcessionario;
            if (!_utils.isVoid(obj.derogaBlocchiServiziAssicurativi)) consFinacialQueue.derogaBlocchiServiziAssicurativi = obj.derogaBlocchiServiziAssicurativi;
            if (!_utils.isVoid(obj.derogaCommerciale)) consFinacialQueue.derogaCommerciale = obj.derogaCommerciale;
            if (!_utils.isVoid(obj.derogaContributo)) consFinacialQueue.derogaContributo = obj.derogaContributo;
            if (!_utils.isVoid(obj.distance)) consFinacialQueue.distance = obj.distance;
            if (!_utils.isVoid(obj.fattura)) consFinacialQueue.fattura = obj.fattura;
            if (!_utils.isVoid(obj.financedAmount)) consFinacialQueue.financedAmount = obj.financedAmount;
            if (!_utils.isVoid(obj.financialProductCode)) consFinacialQueue.financialProductCode = obj.financialProductCode;
            if (!_utils.isVoid(obj.frequency)) consFinacialQueue.frequency = obj.frequency;
            if (!_utils.isVoid(obj.importoAnticipo)) consFinacialQueue.importoAnticipo = obj.importoAnticipo;
            if (!_utils.isVoid(obj.importoRataAnticipoServizi)) consFinacialQueue.importoRataAnticipoServizi = obj.importoRataAnticipoServizi;
            if (!_utils.isVoid(obj.importoRataBalloon)) consFinacialQueue.importoRataBalloon = obj.importoRataBalloon;
            if (!_utils.isVoid(obj.importoRataBalloonServizi)) consFinacialQueue.importoRataBalloonServizi = obj.importoRataBalloonServizi;
            if (!_utils.isVoid(obj.importoRataServizi)) consFinacialQueue.importoRataServizi = obj.importoRataServizi;
            if (!_utils.isVoid(obj.incentivoVendita)) consFinacialQueue.incentivoVendita = obj.incentivoVendita;
            if (!_utils.isVoid(obj.ipt)) consFinacialQueue.ipt = obj.ipt;
            if (!_utils.isVoid(obj.mileage)) consFinacialQueue.mileage = obj.mileage;
            if (!_utils.isVoid(obj.mfr)) consFinacialQueue.mfr = obj.mfr;
            if (!_utils.isVoid(obj.mss)) consFinacialQueue.mss = obj.mss;
            if (!_utils.isVoid(obj.netCost)) consFinacialQueue.netCost = obj.netCost;
            if (!_utils.isVoid(obj.operazioneK2K)) consFinacialQueue.operazioneK2K = obj.operazioneK2K;
            if (!_utils.isVoid(obj.provImportoDeltaTasso)) consFinacialQueue.provImportoDeltaTasso = obj.provImportoDeltaTasso;
            if (!_utils.isVoid(obj.provImportoServizi)) consFinacialQueue.provImportoServizi = obj.provImportoServizi;
            if (!_utils.isVoid(obj.provImportoServiziPerc)) consFinacialQueue.provImportoServiziPerc = obj.provImportoServiziPerc;
            if (!_utils.isVoid(obj.provImportoSpeseIstruttoria)) consFinacialQueue.provImportoSpeseIstruttoria = obj.provImportoSpeseIstruttoria;
            if (!_utils.isVoid(obj.provImportoSpeseIstruttoriaPerc)) consFinacialQueue.provImportoSpeseIstruttoriaPerc = obj.provImportoSpeseIstruttoriaPerc;
            if (!_utils.isVoid(obj.provImportoTotale)) consFinacialQueue.provImportoTotale = obj.provImportoTotale;
            if (!_utils.isVoid(obj.provImportoVendita)) consFinacialQueue.provImportoVendita = obj.provImportoVendita;
            if (!_utils.isVoid(obj.provImportoVenditaPerc)) consFinacialQueue.provImportoVenditaPerc = obj.provImportoVenditaPerc;
            if (!_utils.isVoid(obj.provvigioniDTO)) consFinacialQueue.provvigioniDTO = obj.provvigioniDTO;
            if (!_utils.isVoid(obj.rata)) consFinacialQueue.rata = obj.rata;
            if (!_utils.isVoid(obj.rataPercentuale)) consFinacialQueue.rataPercentuale = obj.rataPercentuale;
            if (!_utils.isVoid(obj.richiestaFattura)) consFinacialQueue.richiestaFattura = obj.richiestaFattura;
            if (!_utils.isVoid(obj.speseAnticipo)) consFinacialQueue.speseAnticipo = obj.speseAnticipo;
            if (!_utils.isVoid(obj.speseDiIstruttoria)) consFinacialQueue.speseDiIstruttoria = obj.speseDiIstruttoria;
            if (!_utils.isVoid(obj.speseRataCanone)) consFinacialQueue.speseRataCanone = obj.speseRataCanone;
            if (!_utils.isVoid(obj.spread)) consFinacialQueue.spread = obj.spread;
            if (!_utils.isVoid(obj.taeg)) consFinacialQueue.taeg = obj.taeg;
            if (!_utils.isVoid(obj.tan)) consFinacialQueue.tan = obj.tan;
            if (!_utils.isVoid(obj.tassBase)) consFinacialQueue.tassBase = obj.tassBase;
            if (!_utils.isVoid(obj.tassoBase)) consFinacialQueue.tassoBase = obj.tassoBase;
            if (!_utils.isVoid(obj.tipoInteresse)) consFinacialQueue.tipoInteresse = obj.tipoInteresse;
            if (!_utils.isVoid(obj.totalAmount)) consFinacialQueue.totalAmount = obj.totalAmount;
            if (!_utils.isVoid(obj.used)) consFinacialQueue.used = obj.used;
            if (!_utils.isVoid(obj.vatPercentage)) consFinacialQueue.vatPercentage = obj.vatPercentage;
            if (!_utils.isVoid(obj.importoServiziAssicurativiRata)) consFinacialQueue.importoServiziAssicurativiRata = obj.importoServiziAssicurativiRata;
            if (!_utils.isVoid(obj.importoServiziAccordoAssisetenzaRata)) consFinacialQueue.importoServiziAccordoAssisetenzaRata = obj.importoServiziAccordoAssisetenzaRata;
            if (!_utils.isVoid(obj.importoServiziAssicurativiBalloon)) consFinacialQueue.importoServiziAssicurativiBalloon = obj.importoServiziAssicurativiBalloon;
            if (!_utils.isVoid(obj.importoServiziAccordoAssistenzaBalloon)) consFinacialQueue.importoServiziAccordoAssistenzaBalloon = obj.importoServiziAccordoAssistenzaBalloon;
            if (!_utils.isVoid(obj.importoTotaleRata)) consFinacialQueue.importoTotaleRata = obj.importoTotaleRata;
            if (!_utils.isVoid(obj.importoTotaleBalloon)) consFinacialQueue.importoTotaleBalloon = obj.importoTotaleBalloon;
            if (!_utils.isVoid(obj.kilometraggio)) consFinacialQueue.kilometraggio = obj.kilometraggio;*/

        }

        return consFinacialQueue;
    }
}









