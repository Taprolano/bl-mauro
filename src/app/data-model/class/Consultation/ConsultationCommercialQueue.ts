import {UtilsService} from "../../../shared/services/utils.service";

export class ConsultationCommercialQueue {
    private _ipt?: number;
    private _mss?: string;
    private _netCost?: string;
    private _fornitoreAsset?: string;
    private _vatCode?: string;
    private _chassisNumber?: string;
    private _plate?: string;
    private _registration?: string;
    private _mileage?: string;
    private _totalAmount?: number;
    private _listingPrice?: number;
    private _equipmentTotalAmount?: number;
    private _optionalsTotalAmount?: number;
    private _discountAmount?: number;
    private _discountPerc?: number;
    private _optionals?: ConsultationCommOptional[];

    constructor(ipt?: number, mss?: string, netCost?: string, fornitoreAsset?: string, vatCode?: string,
                chassisNumber?: string, plate?: string, registration?: string, mileage?: string, totalAmount?: number,
                listingPrice?: number, equipmentTotalAmount?: number, optionalsTotalAmount?: number, discountAmount?: number,
                discountPerc?: number, optionals?: ConsultationCommOptional[]) {
        this._ipt = ipt;
        this._mss = mss;
        this._netCost = netCost;
        this._fornitoreAsset = fornitoreAsset;
        this._vatCode = vatCode;
        this._chassisNumber = chassisNumber;
        this._plate = plate;
        this._registration = registration;
        this._mileage = mileage;
        this._totalAmount = totalAmount;
        this._listingPrice = listingPrice;
        this._equipmentTotalAmount = equipmentTotalAmount;
        this._optionalsTotalAmount = optionalsTotalAmount;
        this._discountAmount = discountAmount;
        this._discountPerc = discountPerc;
        this._optionals = optionals;
    }

    public get ipt(): number {
        return this._ipt;
    }

    public set ipt(value: number) {
        this._ipt = value;
    }

    public get mss(): string {
        return this._mss;
    }

    public set mss(value: string) {
        this._mss = value;
    }

    public get netCost(): string {
        return this._netCost;
    }

    public set netCost(value: string) {
        this._netCost = value;
    }

    public get fornitoreAsset(): string {
        return this._fornitoreAsset;
    }

    public set fornitoreAsset(value: string) {
        this._fornitoreAsset = value;
    }

    public get vatCode(): string {
        return this._vatCode;
    }

    public set vatCode(value: string) {
        this._vatCode = value;
    }

    public get chassisNumber(): string {
        return this._chassisNumber;
    }

    public set chassisNumber(value: string) {
        this._chassisNumber = value;
    }

    public get plate(): string {
        return this._plate;
    }

    public set plate(value: string) {
        this._plate = value;
    }

    public get registration(): string {
        return this._registration;
    }

    public set registration(value: string) {
        this._registration = value;
    }

    public get mileage(): string {
        return this._mileage;
    }

    public set mileage(value: string) {
        this._mileage = value;
    }

    public get totalAmount(): number {
        return this._totalAmount;
    }

    public set totalAmount(value: number) {
        this._totalAmount = value;
    }

    public get listingPrice(): number {
        return this._listingPrice;
    }

    public set listingPrice(value: number) {
        this._listingPrice = value;
    }

    public get equipmentTotalAmount(): number {
        return this._equipmentTotalAmount;
    }

    public set equipmentTotalAmount(value: number) {
        this._equipmentTotalAmount = value;
    }

    public get optionalsTotalAmount(): number {
        return this._optionalsTotalAmount;
    }

    public set optionalsTotalAmount(value: number) {
        this._optionalsTotalAmount = value;
    }

    public get discountAmount(): number {
        return this._discountAmount;
    }

    public set discountAmount(value: number) {
        this._discountAmount = value;
    }

    public get discountPerc(): number {
        return this._discountPerc;
    }

    public set discountPerc(value: number) {
        this._discountPerc = value;
    }

    public get optionals(): ConsultationCommOptional[] {
        return this._optionals;
    }

    public set optionals(value: ConsultationCommOptional[]) {
        this._optionals = value;
    }
}

export class ConsultationCommOptional {
    private _type?: string;
    private _extraCode?: string;
    private _discountApplied?: boolean;
    private _extraDescription?: string;
    private _extraNetCost?: number;
    private _extraTotalAmount?: number;
    private _vatCode?: string;

    constructor(type?: string, extraCode?: string, discountApplied?: boolean, extraDescription?: string,
                extraNetCost?: number, extraTotalAmount?: number, vatCode?: string) {

        this._type = type;
        this._extraCode = extraCode;
        this._discountApplied = discountApplied;
        this._extraDescription = extraDescription;
        this._extraNetCost = extraNetCost;
        this._extraTotalAmount = extraTotalAmount;
        this._vatCode = vatCode;

    }

    public get type(): string {
        return this._type;
    }

    public set type(value: string) {
        this._type = value;
    }

    public get extraCode(): string {
        return this._extraCode;
    }

    public set extraCode(value: string) {
        this._extraCode = value;
    }

    public get discountApplied(): boolean {
        return this._discountApplied;
    }

    public set discountApplied(value: boolean) {
        this._discountApplied = value;
    }

    public get extraDescription(): string {
        return this._extraDescription;
    }

    public set extraDescription(value: string) {
        this._extraDescription = value;
    }

    public get extraNetCost(): number {
        return this._extraNetCost;
    }

    public set extraNetCost(value: number) {
        this._extraNetCost = value;
    }

    public get extraTotalAmount(): number {
        return this._extraTotalAmount;
    }

    public set extraTotalAmount(value: number) {
        this._extraTotalAmount = value;
    }

    public get vatCode(): string {
        return this._vatCode;
    }

    public set vatCode(value: string) {
        this._vatCode = value;
    }

}

export class ConsultationCommOptionalFactory {

    constructor() {
    }

    private _utils: UtilsService = new UtilsService();

    getInstance(obj: ConsultationCommOptional): ConsultationCommOptional {
        let consCommercialOptional = new ConsultationCommOptional();
        if (obj) {
            if (!this._utils.isVoid(obj.type)) consCommercialOptional.type = obj.type;
            if (!this._utils.isVoid(obj.extraCode)) consCommercialOptional.extraCode = obj.extraCode;
            if (!this._utils.isVoid(obj.discountApplied)) consCommercialOptional.discountApplied = obj.discountApplied;
            if (!this._utils.isVoid(obj.extraDescription)) consCommercialOptional.extraDescription = obj.extraDescription;
            if (!this._utils.isVoid(obj.extraNetCost)) consCommercialOptional.extraNetCost = obj.extraNetCost;
            if (!this._utils.isVoid(obj.extraTotalAmount)) consCommercialOptional.extraTotalAmount = obj.extraTotalAmount;
            if (!this._utils.isVoid(obj.vatCode)) consCommercialOptional.vatCode = obj.vatCode;
        }
        return consCommercialOptional;
    }

    getInstanceList(list: any): ConsultationCommOptional[] {
        let cList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                cList.push(this.getInstance(elem));
            });
        }
        return cList;
    }
}


export class ConsultationCommercialQueueFactory {


    constructor() {
    }

    getInstance(obj: ConsultationCommercialQueue): ConsultationCommercialQueue {
        let _utils = new UtilsService();
        let consCommercialQueue = new ConsultationCommercialQueue();
        let consCommOptionalFactory = new ConsultationCommOptionalFactory();

        if (obj  ) {
            if (!_utils.isVoid(obj.ipt)) consCommercialQueue.ipt = obj.ipt;
            if (!_utils.isVoid(obj.mss)) consCommercialQueue.mss = obj.mss;
            if (!_utils.isVoid(obj.netCost)) consCommercialQueue.netCost = obj.netCost;
            if (!_utils.isVoid(obj.fornitoreAsset)) consCommercialQueue.fornitoreAsset = obj.fornitoreAsset;
            if (!_utils.isVoid(obj.vatCode)) consCommercialQueue.vatCode = obj.vatCode;
            if (!_utils.isVoid(obj.chassisNumber)) consCommercialQueue.chassisNumber = obj.chassisNumber;
            if (!_utils.isVoid(obj.plate)) consCommercialQueue.plate = obj.plate;
            if (!_utils.isVoid(obj.registration)) consCommercialQueue.registration = obj.registration;
            if (!_utils.isVoid(obj.mileage)) consCommercialQueue.mileage = obj.mileage;
            if (!_utils.isVoid(obj.totalAmount)) consCommercialQueue.ipt = obj.totalAmount;
            if (!_utils.isVoid(obj.listingPrice)) consCommercialQueue.listingPrice = obj.listingPrice;
            if (!_utils.isVoid(obj.equipmentTotalAmount)) consCommercialQueue.equipmentTotalAmount = obj.equipmentTotalAmount;
            if (!_utils.isVoid(obj.optionalsTotalAmount)) consCommercialQueue.optionalsTotalAmount = obj.optionalsTotalAmount;
            if (!_utils.isVoid(obj.discountAmount)) consCommercialQueue.discountAmount = obj.discountAmount;
            if (!_utils.isVoid(obj.discountPerc)) consCommercialQueue.discountPerc = obj.discountPerc;
            if (!_utils.isVoid(obj.optionals)) consCommercialQueue.optionals = consCommOptionalFactory.getInstanceList(obj.optionals);

            return consCommercialQueue;
        }
    }
}


