import {HttpParams} from '@angular/common/http';
import {UtilsService} from '../../shared/services/utils.service';
import {LoggerService} from '../../shared/services/logger.service';

export class FinancialServicesReqParams{

    /** asset Type  */
    private _assetType: string;

    /** baumuster */
    private _baumuster: string;

    /** brand */
    private _brand: string;

    /** campaignCode NOT REQUIRED */
    private _campaignCode: string;

    /** contractLength */
    private _contractLength: number;

    /** finProductCode */
    private _finProductCode: string;

    /** flagUsed */
    private _flagUsed: boolean;

    private _utils: UtilsService = new UtilsService();
    private _logger: LoggerService = new LoggerService();

    constructor(assetType: string, baumuster: string, brand: string, contractLength: number, finProductCode: string, flagUsed: boolean, campaignCode?: string) {
        this._assetType = assetType;
        this._baumuster = baumuster;
        this._brand = brand;
        this._contractLength = contractLength;
        this._finProductCode = finProductCode;
        this._flagUsed = flagUsed;
        this._campaignCode = campaignCode;
    }



    /**
     * imposta i parametri per la request per l'api /financial/servicies
     */
    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        if (!this._utils.isVoid(this._assetType)) params = params.set('assetType', this._assetType);
        if (!this._utils.isVoid(this._baumuster)) params = params.set('baumuster', this._baumuster);
        if (!this._utils.isVoid(this._brand)) params = params.set('brand', this._brand);
        if (!this._utils.isVoid(this._contractLength)) params = params.set('contractLength', (this._contractLength).toString());
        if (!this._utils.isVoid(this._finProductCode)) params = params.set('finProductCode', this._finProductCode);
        if (!this._utils.isVoid(this._flagUsed)) params = params.set('flagUsed', (this._flagUsed).toString());
        if (!this._utils.isVoid(this._campaignCode)) params = params.set('campaignCode', this._campaignCode);

        this._logger.logInfo(FinancialServicesReqParams.name, 'requestParameters', params);
        return params;
    }

    /* GET */

    get assetType(): string {
        return this._assetType;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    get brand(): string {
        return this._brand;
    }

    get campaignCode(): string {
        return this._campaignCode;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    get finProductCode(): string {
        return this._finProductCode;
    }

    get flagUsed(): boolean {
        return this._flagUsed;
    }

    /* SET */

    set assetType(value: string) {
        this._assetType = value;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    set brand(value: string) {
        this._brand = value;
    }

    set campaignCode(value: string) {
        this._campaignCode = value;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    set finProductCode(value: string) {
        this._finProductCode = value;
    }

    set flagUsed(value: boolean) {
        this._flagUsed = value;
    }
}