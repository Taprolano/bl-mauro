import {Attachment} from './Attachment';

export class FinancialCampaign {

    /**
     * Financial selectedCampaign Code
     * @clazz string
     */
    private _fpCode: string;

    /**
     * Financial selectedCampaign description
     * @clazz string
     */
    private _fpDescription: string;

    /**
     * Financial Campaign is strategic or not
     * @clazz boolean
     */
    private _isStrategic: boolean;

    /**
     * Financial Campaign pdf url
     * @clazz string
     */
    private _urlPdf: string;

    /**
     * Financial Campaign Details
     * @clazz CampaignDetail[]
     */
    private _campaignsDetailList: CampaignDetail[];

    private _showVoucher: boolean;

    private _isStandard: boolean;

    /**
     *
     * @param code
     * @param name
     * @param isStrategic
     * @param urlPdf
     * @param campaignsList
     */
    constructor(code: string, name: string, isStrategic: boolean, urlPdf: string, campaignsList: CampaignDetail[], showVoucher: boolean, isStandard?: boolean) {
        this._fpCode = code;
        this._fpDescription = name;
        this._isStrategic = isStrategic;
        this._urlPdf = urlPdf;
        this._campaignsDetailList = campaignsList;
        this._showVoucher = showVoucher;
        this._isStandard = isStandard;
    }

    /**
     * BINDING SERVICE BE
     *
     */
    get campaignsDetailList(): CampaignDetail[] {
        return this._campaignsDetailList;
    }

    set campaignsDetailList(value: CampaignDetail[]) {
        this._campaignsDetailList = value;
    }

    get fpCode(): string {
        return this._fpCode;
    }

    set fpCode(value: string) {
        this._fpCode = value;
    }

    get fpDescription(): string {
        return this._fpDescription;
    }

    set fpDescription(value: string) {
        this._fpDescription = value;
    }

    get isStrategic(): boolean {
        return this._isStrategic;
    }

    set isStrategic(value: boolean) {
        this._isStrategic = value;
    }

    get urlPdf(): string {
        return this._urlPdf;
    }

    set urlPdf(value: string) {
        this._urlPdf = value;
    }


    /**
     * BINDINGS HTML
     */

    get optionalsCampaignsList(): CampaignDetail[] {
        return this._campaignsDetailList;
    }

    set optionalsCampaignsList(value: CampaignDetail[]) {
        this._campaignsDetailList = value;
    }

    get optionalsDescription(): string {
        return this._fpDescription;
    }

    set optionalsDescription(value: string) {
        this._fpDescription = value;
    }

    get optionalsCode(): string {
        return this._fpCode;
    }

    set optionalsCode(value: string) {
        this._fpCode = value;
    }

    get optionalsIsStrategic(): boolean {
        return this._isStrategic;
    }

    set optionalsIsStrategic(value: boolean) {
        this._isStrategic = value;
    }

    get optionalsUrlPdf(): string {
        return this._urlPdf;
    }

    set optionalsUrlPdf(value: string) {
        this._urlPdf = value;
    }


    get showVoucher(): boolean {
        return this._showVoucher;
    }

    set showVoucher(value: boolean) {
        this._showVoucher = value;
    }


    get isStandard(): boolean {
        return this._isStandard;
    }

    set isStandard(value: boolean) {
        this._isStandard = value;
    }
}

export class CampaignDetail {
    defaultValue?: boolean;
    internalProposal?: boolean;
    code?: string;
    expiryDate?: string;
    finalExpiryDate?: string;
    name?: string;
    description?: string;
    tan?: string;
    taeg?: string;
    advance?: string;
    mustUseSpIstr?: boolean;
    speseIstruttoria?: number;
    adminFeeShareOverride?: boolean;
    commCamp?: boolean;
    dealManuContrComm?: boolean;
    flagShare?: boolean;
    selFixComAmount?: number;
    selFixComPerc?: number;
    manuFixContrComm?: number;
    manuPercContrComm?: number;
    dealPercManuContrComm?: number;
    dealFixCom?: number;
    dealPercCom?: number;
    attachment?: Attachment;
    tattica: boolean;
    showVoucher?: boolean;
    voucherMandatory?: boolean;
}


export class FinancialCampaignFactory {

    static getInstance(obj: FinancialCampaign): FinancialCampaign {
        return new FinancialCampaign(obj.fpCode, obj.fpDescription, obj.isStrategic, obj.urlPdf, obj.campaignsDetailList, obj.showVoucher, obj.isStandard);
    }
}
