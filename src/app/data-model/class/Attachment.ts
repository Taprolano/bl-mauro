export class Attachment {
    private _contentType?: string;
    private _url?: string;
    private _name?: string;


    constructor(contentType?: string, url?: string, name?: string) {
        this._contentType = contentType;
        this._url = url;
        this._name = name;
    }

    /** @returns {string} */
    get contentType(): string {
        return this._contentType;
    }

    /** @returns {string} */
    get url(): string {
        return this._url;
    }

    /** @returns {string} */
    get name(): string {
        return this._name;
    }

    /** @param {string} value */
    set contentType(value: string) {
        this._contentType = value;
    }

    /** @param {string} value */
    set url(value: string) {
        this._url = value;
    }

    /** @param {string} value */
    set name(value: string) {
        this._name = value;
    }

    public requestParameters(): IFinancialAttachments {
        let requestBody: IFinancialAttachments = {
            contentType: this._contentType,
            url: this._url,
            name: this._name
        };

        return requestBody;
    }

}

export class AttachmentFactory {

    static getInstance(obj: Attachment): Attachment {
        let fincialAttachment = new Attachment();

        if (obj) {
            if (obj.contentType != undefined) fincialAttachment.contentType = obj.contentType;
            if (obj.url != undefined) fincialAttachment.url = obj.url;
            if (obj.name != undefined) fincialAttachment.name = obj.name;
        }

        return fincialAttachment;

    }

    static getInstanceList(list: any): Attachment[] {
        let fincialAttachmentList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                fincialAttachmentList.push(AttachmentFactory.getInstance(elem));
            });
        }
        return fincialAttachmentList;
    }

    static getInstanceListParam(list: any): Attachment[] {
        let financialAttachmentsList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialAttachmentsList.push(this.getInstance(elem).requestParameters());
            });
        }
        return financialAttachmentsList;
    }

    static getInstanceParams(obj: Attachment): IFinancialAttachments {
        let resp = {
            contentType: obj.contentType,
            url: obj.url,
            name: obj.name
        };

        return resp;
    }

}

export interface IFinancialAttachments {
    contentType?: string;
    url?: string;
    name?: string;
}
