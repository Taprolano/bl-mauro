import {UtilsService} from '../../shared/services/utils.service';
import {LoggerService} from '../../shared/services/logger.service';
import {HttpParams} from '@angular/common/http';

export class CommercialAmountsReqParams {
  
  private _assetType: string;
  private _baumuster: string;
  private _creationTime: number;
  private _id: string;
  private _lastAccessedTime: number;
  private _maxInactiveInterval: number;
  private _new: boolean;
  private _registration: string;
  private _used: boolean;
  
  private _utils: UtilsService = new UtilsService();

  /**
   * 
   * Modello dati request per servizio /commercial/amounts
   */
  constructor(baumuster: string, used: boolean, private _logger: LoggerService) {
    this._baumuster = baumuster;
    this._used = used;
  }
  
  get assetType(): string {
    return this._assetType;
  }
  set assetType(value: string) {
    this._assetType = value;
  }

  get baumuster(): string {
    return this._baumuster;
  }
  set baumuster(value: string) {
    this._baumuster = value;
  }

  get creationTime(): number {
    return this._creationTime;
  }
  set creationTime(value: number) {
    this._creationTime = value;
  }

  get id(): string {
    return this._id;
  }
  set id(value: string) {
    this._id = value;
  }

  get lastAccessedTime(): number {
    return this._lastAccessedTime;
  }
  set lastAccessedTime(value: number) {
    this._lastAccessedTime = value;
  }

  get maxInactiveInterval(): number {
    return this._maxInactiveInterval;
  }
  set maxInactiveInterval(value: number) {
    this._maxInactiveInterval = value;
  }

  get new(): boolean {
    return this._new;
  }
  set new(value: boolean) {
    this._new = value;
  }

  get registration(): string {
    return this._registration;
  }
  set registration(value: string) {
    this._registration = value;
  }

  get used(): boolean {
    return this._used;
  }
  set used(value: boolean) {
    this._used = value;
  }

  /**
   * imposta i parametri per la request per l'api /commercial/amounts
   */
  requestParameters(): HttpParams {
    let params = new HttpParams(); // set of HttpParams return an immutable object

    this._logger.logInfo(CommercialAmountsReqParams.name, 'requestParameters', this);
        //console.log('requestParameters', this);

        if(!this._utils.isVoid(this._assetType)) params = params.set('assetType',this._assetType);
        if(!this._utils.isVoid(this._baumuster)) params = params.set('baumuster',this._baumuster);
        if(!this._utils.isVoid(this._creationTime)) params = params.set('creationTime',this._creationTime.toString());
        if(!this._utils.isVoid(this._id)) params = params.set('id',this._id);
        if(!this._utils.isVoid(this._lastAccessedTime)) params = params.set('lastAccessedTime',this._lastAccessedTime.toString());
        if(!this._utils.isVoid(this._maxInactiveInterval)) params = params.set('maxInactiveInterval',this._maxInactiveInterval.toString());
        if(!this._utils.isVoid(this._new)) params = params.set('new',this._new.toString());
        if(!this._utils.isVoid(this._registration)) params = params.set('registration',this._registration);
        if(!this._utils.isVoid(this._used)) params = params.set('used',this._used.toString());

        return params;
  }
}