import {HttpParams} from "@angular/common/http";
import {UtilsService} from "../../shared/services/utils.service";
import {LoggerService} from '../../shared/services/logger.service';

export class VersionRequestParameters{
    private _assetClassCode: string;
    private _assetType: string;
    private _makeCode: string;
    private _baumuster: string;
    private _modelDescription: string;
    private _offset: number;
    private _page: number;
    private _size: number;
    private _sort: string;
    private _fromNetCost: number;
    private _toNetCost: number;
    private _utils:UtilsService = new UtilsService();


    constructor(assetClassCode: string, assetType: string, makeCode: string,
         private _logger: LoggerService){
        this._assetClassCode = assetClassCode;
        this._assetType = assetType;
        this._makeCode = makeCode;
    }


    get size(): number {
        return this._size;
    }

    set size(value: number) {
        this._size = value;
    }

    get page(): number {
        return this._page;
    }

    set page(value: number) {
        this._page = value;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get makeCode(): string {
        return this._makeCode;
    }

    set makeCode(value: string) {
        this._makeCode = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get modelDescription(): string {
        return this._modelDescription;
    }

    set modelDescription(value: string) {
        this._modelDescription = value;
    }

    get offset(): number {
        return this._offset;
    }

    set offset(value: number) {
        this._offset = value;
    }

    get sort(): string {
        return this._sort;
    }

    set sort(value: string) {
        this._sort = value;
    }

    get fromNetCost(): number {
        return this._fromNetCost;
    }

    set fromNetCost(value: number) {
        this._fromNetCost = value;
    }

    get toNetCost(): number {
        return this._toNetCost;
    }

    set toNetCost(value: number) {
        this._toNetCost = value;
    }


    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        this._logger.logInfo('VersionRequestParameters', 'requestParameters', this);
        //console.log('requestParameters', this);

        if(!this._utils.isVoid(this._assetClassCode)) params = params.set('assetClassCode',this._assetClassCode);
        if(!this._utils.isVoid(this._assetType)) params = params.set('assetType',this._assetType);
        if(!this._utils.isVoid(this._makeCode)) params = params.set('makeCode',this._makeCode);
        if(!this._utils.isVoid(this._baumuster)) params = params.set('baumuster',this._baumuster);
        if(!this._utils.isVoid(this._modelDescription)) params = params.set('modelDescription',this._modelDescription);
        if(!this._utils.isVoid(this._offset)) params = params.set('offset',this._offset.toString());
        if(!this._utils.isVoid(this._page)) params = params.set('page',this._page.toString());
        if(!this._utils.isVoid(this._size)) params = params.set('size',this._size.toString());
        if(!this._utils.isVoid(this._sort)) params = params.set('sort',this._sort);
        if(!this._utils.isVoid(this._fromNetCost)) params = params.set('fromNetCost',this._fromNetCost.toString());
        if(!this._utils.isVoid(this._toNetCost)) params = params.set('toNetCost',this._toNetCost.toString());

        return params;
    }




}
