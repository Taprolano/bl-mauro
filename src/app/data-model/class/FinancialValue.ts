export class FinancialValue {
    private _idParent?: string;
    private _idChild?: string;
    private _code?: number;
    private _descr?: string;
    private _message?: string;
    private _object?: any;
    private _supplyTypeCode?: string;
    private _supplyTypeDesc?: string;
    private _supplyElemCode?: string;
    private _supplyElemDesc?: string;
    private _clazz?: string;
    private _price?: any;
    private _isAssistence?: boolean;
    private _customerServiceCost?: number;
    private _isSelected?: boolean;
    constructor(idParent?: string, idChild?: string, code?: number, descr?: string, message?: string, object?: any, supplyTypeCode?: string,
        supplyTypeDesc?: string, supplyElemCode?: string, supplyElementDesc?: string, clazz?: string, price?: any, isAssistence?: boolean, customerServiceCost?: number, isSelected?: boolean) {
        this._idParent = idParent;
        this._idChild = idChild;
        this._code = code;
        this._descr = descr;
        this._message = message;
        this._object = object;
        this._supplyTypeCode = supplyTypeCode;
        this._supplyTypeDesc = supplyTypeDesc;
        this._supplyElemCode = supplyElemCode;
        this._supplyElemDesc = supplyElementDesc;
        this._clazz = clazz;
        this._price = price;
        this._isAssistence = isAssistence;
        this._customerServiceCost = customerServiceCost;
        this._isSelected = isSelected;
    }


    get idParent(): string {
        return this._idParent;
    }

    get idChild(): string {
        return this._idChild;
    }

    get code(): number {
        return this._code;
    }

    get message(): string {
        return this._message;
    }

    get object(): any {
        return this._object;
    }

    get supplyTypeCode(): string {
        return this._supplyTypeCode;
    }

    get supplyTypeDesc(): string {
        return this._supplyTypeDesc;
    }

    get supplyElemCode(): string {
        return this._supplyElemCode;
    }

    get supplyElemDesc(): string {
        return this._supplyElemDesc;
    }

    get clazz(): string {
        return this._clazz;
    }

    get price(): any {
        return this._price;
    }

    get isAssistence(): boolean {
        return this._isAssistence;
    }

    get descr(): string {
        return this._descr;
    }

    get isSelected(): boolean {
        return this._isSelected;
    }

    set isSelected(value: boolean) {
        this._isSelected = value;
    }

    set descr(value: string) {
        this._descr = value;
    }

    set isAssistence(value: boolean) {
        this._isAssistence = value;
    }

    set price(value: any) {
        this._price = value;
    }

    set idParent(value: string) {
        this._idParent = value;
    }

    set idChild(value: string) {
        this._idChild = value;
    }

    set code(value: number) {
        this._code = value;
    }

    set message(value: string) {
        this._message = value;
    }

    set object(value: any) {
        this._object = value;
    }

    set supplyTypeCode(value: string) {
        this._supplyTypeCode = value;
    }

    set supplyTypeDesc(value: string) {
        this._supplyTypeDesc = value;
    }

    set supplyElemCode(value: string) {
        this._supplyElemCode = value;
    }

    set supplyElemDesc(value: string) {
        this._supplyElemDesc = value;
    }

    set clazz(value: string) {
        this._clazz = value;
    }

    get customerServiceCost(): number {
        return this._customerServiceCost;
    }

    set customerServiceCost(value: number) {
        this._customerServiceCost = value;
    }
}


