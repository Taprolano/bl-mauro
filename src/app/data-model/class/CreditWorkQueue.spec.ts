import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HeaderComponent} from "../../shared/components/header/header.component";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../shared/shared.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MbzDashboardModule} from "../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../proposals/proposals.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {createTranslateLoader} from "../../app.module";
import {AppRoutingModule} from "../../app-routing.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {environment} from "../../../environments/environment.svil";
import {AppComponent} from "../../app.component";
import {AuthGuard} from "../../shared/guard";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CreditCorporateQueue} from "./CreditCorporateQueue";
import {CreditWorkQueue} from "./CreditWorkQueue";
import {DirectAccessGuard} from "../../shared/guard/direct-access/direct-access.guard";



describe('CreditWorkQueue', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('constructor and getter',() =>{
        let cwqueue = new CreditWorkQueue('tipoDiSoggetto','arrivo','assegnatario',
            'richiedente',10,true,10,12,
            'score',13,'',133,'prodotto',null,
            null, null, null, null, null);

        expect(cwqueue.tipoDiSoggetto).toEqual('tipoDiSoggetto');
        expect(cwqueue.arrivo).toEqual('arrivo');
        expect(cwqueue.assegnatario).toEqual('assegnatario');
        expect(cwqueue.richiedente).toEqual('richiedente');
        expect(cwqueue.esposizione).toEqual(10);
        expect(cwqueue.riproposta).toEqual(true);
        expect(cwqueue.numNote).toEqual(10);
        expect(cwqueue.numDocMancanti).toEqual(12);
        expect(cwqueue.score).toEqual('score');
        expect(cwqueue.numBlocchi).toEqual(13);
        expect(cwqueue.proposta).toEqual('');
        expect(cwqueue.processInstanceId).toEqual(133);
        expect(cwqueue.prodotto).toEqual('prodotto');



    });

    it('setter',() =>{

        let cwqueue = new CreditWorkQueue('tipoDiSoggetto','arrivo','assegnatario',
            'richiedente',10,true,10,12,
            'score',13,'',133,'prodotto',null,
            null, null, null, null, null);

        cwqueue.tipoDiSoggetto = 'tipoDiSoggetto';
        cwqueue.arrivo = 'arrivo';
        cwqueue.assegnatario ='assegnatario';
        cwqueue.richiedente = 'richiedente';
        cwqueue.esposizione = 10;
        cwqueue.riproposta = true;
        cwqueue.numNote = 10;
        cwqueue.numDocMancanti = 12;
        cwqueue.score = 'score';
        cwqueue.numBlocchi = 13;
        cwqueue.proposta = '';
        cwqueue.processInstanceId = 133;
        cwqueue.prodotto = 'prodotto';

        expect(cwqueue.tipoDiSoggetto).toEqual('tipoDiSoggetto');
        expect(cwqueue.arrivo).toEqual('arrivo');
        expect(cwqueue.assegnatario).toEqual('assegnatario');
        expect(cwqueue.richiedente).toEqual('richiedente');
        expect(cwqueue.esposizione).toEqual(10);
        expect(cwqueue.riproposta).toEqual(true);
        expect(cwqueue.numNote).toEqual(10);
        expect(cwqueue.numDocMancanti).toEqual(12);
        expect(cwqueue.score).toEqual('score');
        expect(cwqueue.numBlocchi).toEqual(13);
        expect(cwqueue.proposta).toEqual('');
        expect(cwqueue.processInstanceId).toEqual(133);
        expect(cwqueue.prodotto).toEqual('prodotto');

    });

});
