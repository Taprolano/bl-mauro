export class AssetsChassis {

  private _chassis: string;
  private _errors: string[];

  /**
   * BINDING SERVICE BE
   * 
   */

  public get errors(): string[] {
    return this._errors;
  }
  public set errors(value: string[]) {
    this._errors = value;
  }

  public get chassis(): string {
    return this._chassis;
  }
  public set chassis(value: string) {
    this._chassis = value;
  }

  /**
   * BINDING IN HTML
   * 
   */

  public get chassisErrors(): string[] {
    return this._errors;
  }
  public set chassisErrors(value: string[]) {
    this._errors = value;
  }

  public get chassisValue(): string {
    return this._chassis;
  }
  public set chassisValue(value: string) {
    this._chassis = value;
  }
}

export class AssetsChassisFactory {
  getInstance(obj: AssetsChassis): AssetsChassis {
    return new AssetsChassis();
  }
}