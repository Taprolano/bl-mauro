export class DealerContactQueue {

  private _arrivo?: string;
  private _asset?: string;
  private _richiedente?: string;
  private _sospesaDa?: string;
  private _numBlocchi?: number;
  private _numNote?: number;
  private _prodotto?: string;
  private _proposta?: number;
  private _tipoProposta?: number;
  private _causaleSospensione?: string;
  private _campagna?: string;
  private _descrizioneCampagna?: string;
  private _propStatus?: string;

  constructor(
    arrivo?: string,
    asset?: string,
    richiedente?: string,
    sospesaDa?: string,
    numBlocchi?: number,
    numNote?: number,
    prodotto?: string,
    proposta?: number,
    tipoProposta?: number,
    causaleSospensione?: string,
    campagna?: string,
    descrizioneCampagna?: string,
    propStatus?: string){
      this._arrivo = arrivo;
      this._asset = asset;
      this._richiedente = richiedente;
      this._sospesaDa = sospesaDa;
      this._numBlocchi = numBlocchi;
      this._numNote = numNote;
      this._prodotto = prodotto;
      this._proposta = proposta; 
      this._tipoProposta = tipoProposta; 
      this._causaleSospensione = causaleSospensione;
      this._campagna = campagna;
      this._descrizioneCampagna = descrizioneCampagna;
      this._propStatus = propStatus;
  }

  
    /**@ignore nomi delle chiavi del backend, per tab relativo - devono corrispondere ai getter (autogenerare dalle var). Da aggiornare se variano. */
    static readonly keys = ["proposta", "arrivo", "tipoProposta", "richiedente",  /*"sospesaDa",*/ "causaleSospensione"/*, "campagna", "asset"*/];


  /**
   * BINDING BE
   */

  get arrivo(): string {
    return this._arrivo;
  }
  set arrivo(value: string) {
    this._arrivo = value;
  }

  get asset(): string {
    return this._asset;
  }
  set asset(value: string) {
    this._asset = value;
  }

  get richiedente(): string {
    return this._richiedente;
  }
  set richiedente(value: string) {
    this._richiedente = value;
  }

  get sospesaDa(): string {
    return this._sospesaDa;
  }
  set sospesaDa(value: string) {
    this._sospesaDa = value;
  }

  get numBlocchi(): number {
    return this._numBlocchi;
  }
  set numBlocchi(value: number) {
    this._numBlocchi = value;
  }

  get numNote(): number {
    return this._numNote;
  }
  set numNote(value: number) {
    this._numNote = value;
  }
  
  get proposta(): number {
    return this._proposta;
  }
  set proposta(value: number) {
    this._proposta = value;
  }

  get tipoProposta(): number {
    return this._tipoProposta;
  }
  set tipoProposta(value: number) {
    this._tipoProposta = value;
  }

  get prodotto(): string {
    return this._prodotto;
  }
  set prodotto(value: string) {
    this._prodotto = value;
  }
  get campagna(): string {
    return this._campagna;
  }
  set campagna(value: string) {
    this._campagna = value;
  }

  public get descrizioneCampagna(): string {
    return this._descrizioneCampagna;
  }
  public set descrizioneCampagna(value: string) {
    this._descrizioneCampagna = value;
  }

  get causaleSospensione(): string {
    return this._causaleSospensione;
  }
  set causaleSospensione(value: string) {
    this._causaleSospensione = value;
  }

  public get propStatus(): string {
    return this._propStatus;
  }
  public set propStatus(value: string) {
    this._propStatus = value;
  }
}