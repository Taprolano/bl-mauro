import {FinacialBaseServices} from './FinacialBaseServices';
import {UtilsService} from '../../shared/services/utils.service';

export class FinancialConfigServiceInner extends FinacialBaseServices {

    private _calculationAllowed?: boolean;
    private _hasSatellite?: boolean;
    private _hasFireTheftInsurance?: boolean;
    private _forcedToPC?: boolean;
    private _clazz?: string;
    private _annualDistance?: any;
    private _estensioneGaranziaProposta?: number[];
    private _supplyAmount?: number;
    private _customerProvinceCode?: string;
    private _rtiCoverage?: CodeObj[];
    private _fireTheftCoverage?: CodeObj[];
    private _beneStrumentale?: boolean;

    /**
     *
     * @param {string} categoryCode
     * @param {boolean} defaultElement
     * @param {boolean} defaultSupply
     * @param {string} financeCode
     * @param {string} flagMainSup
     * @param {string} flagMandatory
     * @param {string} parentSupplyTypeCode
     * @param {string} parSupMand
     * @param {boolean} retrieveVat
     * @param {string} selezionabile
     * @param {string} supplyElementCode
     * @param {string} supplyElementDesc
     * @param {string} supplyTypeCode
     * @param {string} supplyTypeDesc
     * @param {boolean} tassoProdotto
     * @param {number} taxPercentage
     * @param {boolean} updatable
     * @param {boolean} calculationAllowed
     * @param {boolean} hasSatellite
     * @param {boolean} hasFireTheftInsurance
     * @param {boolean} forcedToPC
     * @param {string} type
     * @param annualDistance
     * @param estensioneGaranziaProposta
     * @param supplyAmount
     * @param customerProvinceCode
     * @param rtiCoverage
     * @param fireTheftCoverage
     * @param beneStrumentale
     */
    constructor(categoryCode?: string, defaultElement?: boolean, defaultSupply?: boolean, financeCode?: string, flagMainSup?: string,
                flagMandatory?: string, parentSupplyTypeCode?: string, parSupMand?: string, retrieveVat?: boolean, selezionabile?: string,
                supplyElementCode?: string, supplyElementDesc?: string, supplyTypeCode?: string, supplyTypeDesc?: string,
                tassoProdotto?: boolean, taxPercentage?: number, updatable?: boolean, calculationAllowed?: boolean,
                hasSatellite?: boolean, hasFireTheftInsurance?: boolean, forcedToPC?: boolean, type?: string, annualDistance?: any,
                estensioneGaranziaProposta?: number[], supplyAmount?: number, customerProvinceCode?: string, rtiCoverage?: CodeObj[],
                fireTheftCoverage?: CodeObj[], beneStrumentale?: boolean) {
        super(categoryCode, defaultElement, defaultSupply, financeCode, flagMainSup, flagMandatory, parentSupplyTypeCode, parSupMand,
            selezionabile, supplyElementCode, supplyElementDesc, supplyTypeCode, supplyTypeDesc, tassoProdotto, taxPercentage, updatable, retrieveVat);
        this._calculationAllowed = calculationAllowed;
        this._hasSatellite = hasSatellite;
        this._hasFireTheftInsurance = hasFireTheftInsurance;
        this._forcedToPC = forcedToPC;
        this._clazz = type;
        this._annualDistance = annualDistance;
        this._supplyAmount = supplyAmount;
        this._beneStrumentale = beneStrumentale;
    }

    get calculationAllowed(): boolean {
        return this._calculationAllowed;
    }

    get hasSatellite(): boolean {
        return this._hasSatellite;
    }

    get hasFireTheftInsurance(): boolean {
        return this._hasFireTheftInsurance;
    }

    get forcedToPC(): boolean {
        return this._forcedToPC;
    }

    get clazz(): string {
        return this._clazz;
    }

    get annualDistance(): any {
        return this._annualDistance;
    }


    get estensioneGaranziaProposta(): number[] {
        return this._estensioneGaranziaProposta;
    }

    get supplyAmount(): number {
        return this._supplyAmount;
    }

    get customerProvinceCode(): string {
        return this._customerProvinceCode;
    }

    get rtiCoverage(): CodeObj[] {
        return this._rtiCoverage;
    }

    get fireTheftCoverage(): CodeObj[] {
        return this._fireTheftCoverage;
    }


    get beneStrumentale(): boolean {
        return this._beneStrumentale;
    }

    set beneStrumentale(value: boolean) {
        this._beneStrumentale = value;
    }

    set customerProvinceCode(value: string) {
        this._customerProvinceCode = value;
    }

    set rtiCoverage(value: CodeObj[]) {
        this._rtiCoverage = value;
    }

    set fireTheftCoverage(value: CodeObj[]) {
        this._fireTheftCoverage = value;
    }

    set supplyAmount(value: number) {
        this._supplyAmount = value;
    }

    set estensioneGaranziaProposta(value: number[]) {
        this._estensioneGaranziaProposta = value;
    }

    set annualDistance(value: any) {
        this._annualDistance = value;
    }

    set clazz(value: string) {
        this._clazz = value;
    }

    set calculationAllowed(value: boolean) {
        this._calculationAllowed = value;
    }

    set hasSatellite(value: boolean) {
        this._hasSatellite = value;
    }

    set hasFireTheftInsurance(value: boolean) {
        this._hasFireTheftInsurance = value;
    }

    set forcedToPC(value: boolean) {
        this._forcedToPC = value;
    }
}

export class FinancialConfigServiceInnerFactory {

    static getInstance(service: FinancialConfigServiceInner): FinancialConfigServiceInner {
        let financialConfigServiceInner = new FinancialConfigServiceInner();

        if (service) {
            financialConfigServiceInner.fireTheftCoverage = service.fireTheftCoverage != undefined ? service.fireTheftCoverage : [];
            financialConfigServiceInner.customerProvinceCode = service.customerProvinceCode != undefined ? service.customerProvinceCode : '';
            financialConfigServiceInner.rtiCoverage = service.rtiCoverage != undefined ? service.rtiCoverage : [];
            financialConfigServiceInner.annualDistance = service.annualDistance != undefined ? service.clazz : '';
            financialConfigServiceInner.estensioneGaranziaProposta = service.estensioneGaranziaProposta != undefined ? service.estensioneGaranziaProposta : [];
            financialConfigServiceInner.clazz = service.clazz != undefined ? service.clazz : '';
            financialConfigServiceInner.supplyTypeCode = service.supplyTypeCode != undefined ? service.supplyTypeCode : '';
            financialConfigServiceInner.supplyElementCode = service.supplyElementCode != undefined ? service.supplyElementCode : '';
            financialConfigServiceInner.supplyElementDesc = service.supplyElementDesc != undefined ? service.supplyElementDesc : '';
            financialConfigServiceInner.categoryCode = service.categoryCode != undefined ? service.categoryCode : '';
            financialConfigServiceInner.parentSupplyTypeCode = service.parentSupplyTypeCode != undefined ? service.parentSupplyTypeCode : '';
            financialConfigServiceInner.defaultSupply = service.defaultSupply != undefined ? service.defaultSupply : false;
            financialConfigServiceInner.defaultElement = service.defaultElement != undefined ? service.defaultElement : false;
            financialConfigServiceInner.updatable = service.updatable != undefined ? service.updatable : false;
            financialConfigServiceInner.parSupMand = service.parSupMand != undefined ? service.parSupMand : '';
            financialConfigServiceInner.flagMainSup = service.flagMainSup != undefined ? service.flagMainSup : '';
            financialConfigServiceInner.flagMandatory = service.flagMandatory != undefined ? service.flagMandatory : '';
            financialConfigServiceInner.retrieveVat = service.retrieveVat != undefined ? service.retrieveVat : false;
            financialConfigServiceInner.tassoProdotto = service.tassoProdotto != undefined ? service.tassoProdotto : false;
            financialConfigServiceInner.calculationAllowed = service.calculationAllowed != undefined ? service.calculationAllowed : false;
            financialConfigServiceInner.hasSatellite = service.hasSatellite != undefined ? service.hasSatellite : false;
            financialConfigServiceInner.hasFireTheftInsurance = service.hasFireTheftInsurance != undefined ? service.hasFireTheftInsurance : false;
            financialConfigServiceInner.forcedToPC = service.forcedToPC != undefined ? service.forcedToPC : false;
            financialConfigServiceInner.supplyAmount = service.supplyAmount != undefined ? service.supplyAmount : null;
            financialConfigServiceInner.beneStrumentale = service.beneStrumentale != undefined ? service.beneStrumentale : false;

            return financialConfigServiceInner;

        }
    }

    getInstanceList(list: any): FinancialConfigServiceInner[] {
        let financialConfigServiceInnerList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialConfigServiceInnerList.push(FinancialConfigServiceInnerFactory.getInstance(elem));
            });
        }
        return financialConfigServiceInnerList;
    }
}

export class CodeObj {
    private _code: string;

    constructor(code: string) {
        this._code = code;
    }

    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }
}