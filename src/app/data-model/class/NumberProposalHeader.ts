export class NumberProposalHeader {

  private _countAttive?: number;
  
  private _countInControlloDocumentale?: number;
  
  private _countInIstruttoria?: number;
  
  private _countSospese?: number;

  private _countAttivateOggi?: number;

  private _countLavorabili?: number;

  private _countTouchAndGo?: number;

  private _countUrgenti?: number;

  constructor(countAttive: number, countInControlloDocumentale: number,
    countInIstruttoria: number, countSospese: number, countAttivateOggi: number, countLavorabili: number,
              countTouchAndGo: number, countUrgenti: number) {
      this._countAttive = countAttive;
      this._countInControlloDocumentale = countInControlloDocumentale;
      this._countInIstruttoria = countInIstruttoria;
      this._countSospese = countSospese;
      this._countAttivateOggi = countAttivateOggi;
      this._countLavorabili = countLavorabili;
      this._countTouchAndGo = countTouchAndGo;
      this._countUrgenti = countUrgenti;
  }

  /**
     * BINDING BE
     * @returns {number}
     */

  public get countAttive(): number {
    return this._countAttive;
  }
  public set countAttive(value: number) {
    this._countAttive = value;
  }

  public get countInControlloDocumentale(): number {
    return this._countInControlloDocumentale;
  }
  public set countInControlloDocumentale(value: number) {
    this._countInControlloDocumentale = value;
  }

  public get countInIstruttoria(): number {
    return this._countInIstruttoria;
  }
  public set countInIstruttoria(value: number) {
    this._countInIstruttoria = value;
  }

  public get countSospese(): number {
    return this._countSospese;
  }
  public set countSospese(value: number) {
    this._countSospese = value;
  }

  get countAttivateOggi(): number {
    return this._countAttivateOggi;
  }

  set countAttivateOggi(value: number) {
    this._countAttivateOggi = value;
  }

  get countLavorabili(): number {
    return this._countLavorabili;
  }

  set countLavorabili(value: number) {
    this._countLavorabili = value;
  }

  get countTouchAndGo(): number {
    return this._countTouchAndGo;
  }

  set countTouchAndGo(value: number) {
    this._countTouchAndGo = value;
  }

  get countUrgenti(): number {
    return this._countUrgenti;
  }

  set countUrgenti(value: number) {
    this._countUrgenti = value;
  }
}