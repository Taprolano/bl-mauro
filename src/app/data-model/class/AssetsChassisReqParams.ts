import {HttpParams} from '@angular/common/http';
import {LoggerService} from '../../shared/services/logger.service';
import {UtilsService} from '../../shared/services/utils.service';

export class AssetsChassisReqParams {

  private _baumuster: string;
  private _chassis: string;
  private _makeCode: string;

  private _utils: UtilsService = new UtilsService();

  /**
   * 
   * Modello dati request per servizio /assets/chassis/{chassis}
   */
  constructor(baumuster: string, chassis: string, makeCode: string, private _logger: LoggerService) {
    this._baumuster = baumuster;
    this._chassis = chassis;
    this._makeCode = makeCode;
  }

  public get makeCode(): string {
    return this._makeCode;
  }
  public set makeCode(value: string) {
    this._makeCode = value;
  }

  public get chassis(): string {
    return this._chassis;
  }
  public set chassis(value: string) {
    this._chassis = value;
  }

  public get baumuster(): string {
    return this._baumuster;
  }
  public set baumuster(value: string) {
    this._baumuster = value;
  }
  
  /**
   * imposta i parametri per la request per l'api /assets/chassis/{chassis}
   */
  requestParameters(): HttpParams {
    let params = new HttpParams(); // set of HttpParams return an immutable object

    this._logger.logInfo(AssetsChassisReqParams.name, 'requestParameters', this);

    if(!this._utils.isVoid(this._baumuster)) params = params.set('baumuster',this._baumuster);
    if(!this._utils.isVoid(this._chassis)) params = params.set('chassis',this._chassis);
    if(!this._utils.isVoid(this._makeCode)) params = params.set('makeCode',this._makeCode);

    return params;
  }
}