import {UtilsService} from '../../shared/services/utils.service';
import {HttpParams} from '@angular/common/http';

export class ListValuesEnhancedReqParams {

    private _assetClassCode: string;
    private _assetType: string;
    private _baumuster: string;
    private _brandId: string;
    private _campaignCode: string;
    private _contractLength: number;
    private _financialProductCode: string;
    private _financiedAmount: number;
    private _listCode: string;
    private _minimumKilometers: number;

    private _utils: UtilsService = new UtilsService();

    constructor(assetClassCode: string,
                assetType: string,
                baumuster: string,
                brandId: string,
                campaignCode: string,
                contractLength: number,
                financialProductCode: string,
                financiedAmount: number,
                listCode: string,
                minimumKilometers: number) {

        this._assetClassCode = assetClassCode;
        this._assetType = assetType;
        this._baumuster = baumuster;
        this._brandId = brandId;
        this._campaignCode = campaignCode;
        this._contractLength = contractLength;
        this._financialProductCode = financialProductCode;
        this._financiedAmount = financiedAmount;
        this._listCode = listCode;
        this._minimumKilometers = minimumKilometers;
    }

    /**
     * BINDING SERVICE BE
     *
     */
    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get campaignCode(): string {
        return this._campaignCode;
    }

    set campaignCode(value: string) {
        this._campaignCode = value;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get financiedAmount(): number {
        return this._financiedAmount;
    }

    set financiedAmount(value: number) {
        this._financiedAmount = value;
    }

    get listCode(): string {
        return this._listCode;
    }

    set listCode(value: string) {
        this._listCode = value;
    }

    get minimumKilometers(): number {
        return this._minimumKilometers;
    }

    set minimumKilometers(value: number) {
        this._minimumKilometers = value;
    }

    get utils(): UtilsService {
        return this._utils;
    }

    set utils(value: UtilsService) {
        this._utils = value;
    }

    /**
     * BINDINGS HTML
     */

    get optionalassetClassCode(): string {
        return this._assetClassCode;
    }

    set optionalassetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get optionalassetType(): string {
        return this._assetType;
    }

    set optionalassetType(value: string) {
        this._assetType = value;
    }

    get optionalbaumuster(): string {
        return this._baumuster;
    }

    set optionalbaumuster(value: string) {
        this._baumuster = value;
    }

    get optionalbrandId(): string {
        return this._brandId;
    }

    set optionalbrandId(value: string) {
        this._brandId = value;
    }

    get optionalcampaignCode(): string {
        return this._campaignCode;
    }

    set optionalcampaignCode(value: string) {
        this._campaignCode = value;
    }

    get optionalcontractLength(): number {
        return this._contractLength;
    }

    set optionalcontractLength(value: number) {
        this._contractLength = value;
    }

    get optionalfinancialProductCode(): string {
        return this._financialProductCode;
    }

    set optionalfinancialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get optionalfinanciedAmount(): number {
        return this._financiedAmount;
    }

    set optionalfinanciedAmount(value: number) {
        this._financiedAmount = value;
    }

    get optionallistCode(): string {
        return this._listCode;
    }

    set optionallistCode(value: string) {
        this._listCode = value;
    }

    get optionalminimumKilometers(): number {
        return this._minimumKilometers;
    }

    set optionalminimumKilometers(value: number) {
        this._minimumKilometers = value;
    }

    get optionalutils(): UtilsService {
        return this._utils;
    }

    set optionalutils(value: UtilsService) {
        this._utils = value;
    }

    /**
     * imposta i parametri per la request per l'api /listofvaluesenhanced
     */
    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        if (!this._utils.isVoid(this._assetClassCode)) params = params.set('assetClassCode', this._assetClassCode);
        if (!this._utils.isVoid(this._assetType)) params = params.set('assetType', this._assetType);
        if (!this._utils.isVoid(this._baumuster)) params = params.set('baumuster', this._baumuster);
        if (!this._utils.isVoid(this._brandId)) params = params.set('brandId', this._brandId);
        if (!this._utils.isVoid(this._campaignCode)) params = params.set('campaignCode', this._campaignCode);
        if (!this._utils.isVoid(this._contractLength)) params = params.set('contractLength', this._contractLength.toString());
        if (!this._utils.isVoid(this._financialProductCode)) params = params.set('financialProductCode', this._financialProductCode);
        if (!this._utils.isVoid(this._financiedAmount)) params = params.set('financiedAmount', this._financiedAmount.toString());
        if (!this._utils.isVoid(this._listCode)) params = params.set('listCode', this._listCode);
        if (!this._utils.isVoid(this._minimumKilometers)) params = params.set('minimumKilometers', this._minimumKilometers.toString());

        return params;
    }

}
