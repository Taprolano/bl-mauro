import {HttpParams} from '@angular/common/http';
import {FinancialConfigServiceInner} from './FinancialConfigServiceInner';


export class FinancialConfigServiceBodyParameters {

    private _assetType: string;
    private _brandId: string;
    private _contractLength: number;
    private _distance: number;
    private _financialProductCode: string;
    private _financiedAmount: number;
    private _frequency: number;
    private _service: FinancialConfigServiceInner;
    private _tan: number;
//    private _totalAmount: number;
    private _netCost: number;
    private _vatPercentage: number;

    constructor(assetType?: string, brandId?: string, contractLength?: number, distance?: number,
                financialProductCode?: string, financiedAmount?: number, frequency?: number,
                service?: FinancialConfigServiceInner, tan?: number, netCost?: number, vatPercentage?: number) {
        this._assetType = assetType;
        this._brandId = brandId;
        this._contractLength = contractLength;
        this._distance = distance;
        this._financialProductCode = financialProductCode;
        this._financiedAmount = financiedAmount;
        this._frequency = frequency;
        this._service = service;
        this._tan = tan;
//        this._totalAmount = totalAmount;
        this._netCost = netCost;
        this._vatPercentage = vatPercentage;
    }

    /**
     *
     * @returns {HttpParams}
     */
    requestParameters(): Object {
        let params: Object; // set of HttpParams return an immutable object

        params =   {
            "assetType": this.assetType,
            "brandId": this.brandId,
            "contractLength":this.contractLength,
            "distance":this.distance,
            "netCost": this.netCost,
            "financiedAmount": this.financiedAmount,
            "frequency": this.frequency,
            "tan": this.tan,
            "finProductCode": this.financialProductCode,
            "vatPercentage": this.vatPercentage,
            "service":{
                "clazz":this.service.clazz,
                "supplyTypeCode": this.service.supplyTypeCode,
                "supplyElementCode": this.service.supplyElementCode,
                "supplyTypeDesc": this.service.supplyTypeDesc,
                "supplyElementDesc": this.service.supplyElementDesc,
                "categoryCode": this.service.categoryCode,
                "parentSupplyTypeCode": this.service.parentSupplyTypeCode,
                "defaultSupply": this.service.defaultSupply,
                "defaultElement": this.service.defaultElement,
                "updatable": this.service.updatable,
                "retrieveVat": this.service.retrieveVat,
                "parSupMand": this.service.parSupMand,
                "flagMainSup": this.service.flagMainSup,
                "selezionabile": this.service.selezionabile,
                "flagMandatory": this.service.flagMandatory,
                "tassoProdotto": this.service.tassoProdotto,
                "calculationAllowed": this.service.calculationAllowed,
                "hasSatellite": this.service.hasSatellite,
                "hasFireTheftInsurance": this.service.hasFireTheftInsurance,
                "forcedToPC": this.service.forcedToPC
            }
        };

        return params;
    }


    get assetType(): string {
        return this._assetType;
    }

    get brandId(): string {
        return this._brandId;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    get distance(): number {
        return this._distance;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    get financiedAmount(): number {
        return this._financiedAmount;
    }

    get frequency(): number {
        return this._frequency;
    }

    get service(): FinancialConfigServiceInner {
        return this._service;
    }

    get tan(): number {
        return this._tan;
    }

    /*
    get totalAmount(): number {
        return this._totalAmount;
    }
    */

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    /* SET */

    set assetType(value: string) {
        this._assetType = value;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    set distance(value: number) {
        this._distance = value;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    set financiedAmount(value: number) {
        this._financiedAmount = value;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    set service(value: FinancialConfigServiceInner) {
        this._service = value;
    }

    set tan(value: number) {
        this._tan = value;
    }

    /*
    set totalAmount(value: number) {
        this._totalAmount = value;
    }
    */

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }


    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }
}

