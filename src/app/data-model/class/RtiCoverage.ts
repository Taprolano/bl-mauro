import {UtilsService} from '../../shared/services/utils.service';
import {FinancialConfigServiceResponseInner, FinancialConfigServiceResponseInnerFactory} from './FinancialConfigServiceResponseInner';

export class RtiCoverage {
    private _code: string;
    private _description: string;
    private _coverageLength: number;


    constructor(code?: string, description?: string, coverageLength?: number) {
        this._code = code;
        this._description = description;
        this._coverageLength = coverageLength;
    }


    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get coverageLength(): number {
        return this._coverageLength;
    }

    set coverageLength(value: number) {
        this._coverageLength = value;
    }
}

export class RtiCoverageFactory {

    static getInstance(obj: RtiCoverage): RtiCoverage {
        let utils: UtilsService = new UtilsService();
        let rti = new RtiCoverage();

        if (obj) {
            rti.code = !utils.isUndefided(obj.code) ? obj.code : '';
            rti.description = !utils.isUndefided(obj.description) ? obj.description : '';
            rti.coverageLength = !utils.isUndefided(obj.coverageLength) ? obj.coverageLength : null;
        }

        return utils.formatOBject4Output(rti);
    }

    static getInstanceList(list: any): RtiCoverage[] {
        let rtiList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                rtiList.push(RtiCoverageFactory.getInstance(elem));
            });
        }
        return rtiList;
    }
}