import {HttpParams} from '@angular/common/http';
import {UtilsService} from '../../shared/services/utils.service';

export class CheckCompatibilityReqParamas{

    /**
     * Supplyer Element Code
     */
    private _supplyElemCodes:string;

    /**
     * Supplyer Element Code List
     */
    private _listSupplyElemCodes?:string[];

    private _messageType?:string;

    private  _utils: UtilsService= new UtilsService();

    /**
     *
     * @param {string} supplyElemCodes
     * @param {string[]} listSupplyElemCodes
     * @param messageType
     */
    constructor(supplyElemCodes: string, listSupplyElemCodes?: string[], messageType?:string) {
        this._supplyElemCodes = supplyElemCodes;
        this._listSupplyElemCodes = listSupplyElemCodes;
        this._messageType = messageType;
    }

    /**
     *
     * @returns {HttpParams}
     */
    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        if (!this._utils.isVoid(this.supplyElemCodes)) params = params.set('supplyElemCodes', this._supplyElemCodes);

        if (!this._utils.isVoid(this.listSupplyElemCodes)) params = params.set('listSupplyElemCodes', this._listSupplyElemCodes.toString());


        return params;
    }



    /**
     *
     * @returns {string}
     */
    get supplyElemCodes(): string {
        return this._supplyElemCodes;
    }

    /**
     *
     * @returns {string[]}
     */
    get listSupplyElemCodes(): string[] {
        return this._listSupplyElemCodes;
    }


    get messageType(): string {
        return this._messageType;
    }

    set messageType(value: string) {
        this._messageType = value;
    }

    get utils(): UtilsService {
        return this._utils;
    }

    set utils(value: UtilsService) {
        this._utils = value;
    }

    /**
     *
     * @param {string} value
     */
    set supplyElemCodes(value: string) {
        this._supplyElemCodes = value;
    }

    /**
     *
     * @param {string[]} value
     */
    set listSupplyElemCodes(value: string[]) {
        this._listSupplyElemCodes = value;
    }
}

export class checkCompatibility{
    private _code?: string;
    private _message?: string;
    private _messageType?: string;


    constructor(code?: string, message?: string, messageType?: string) {
        this._code = code;
        this._message = message;
        this._messageType = messageType;
    }


    get code(): string {
        return this._code;
    }

    get message(): string {
        return this._message;
    }

    get messageType(): string {
        return this._messageType;
    }


    set code(value: string) {
        this._code = value;
    }

    set message(value: string) {
        this._message = value;
    }

    set messageType(value: string) {
        this._messageType = value;
    }
}