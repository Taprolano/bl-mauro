export class ListValuesEnhanced {

    private _listCode?: string;
    private _values?: ValueListEnhanced[];

    constructor(listCode: string, values: ValueListEnhanced[]) {
        this._listCode = listCode;
        this._values = values;
    }

    /**
     * BINDING SERVICE BE
     *
     */

    public get values(): ValueListEnhanced[] {
        return this._values;
    }

    public set values(value: ValueListEnhanced[]) {
        this._values = value;
    }

    public get listCode(): string {
        return this._listCode;
    }

    public set listCode(value: string) {
        this._listCode = value;
    }

    /**
     * BINDINGS IN HTML
     */

    public get listOfvalues(): ValueListEnhanced[] {
        return this._values;
    }

    public set listOfvalues(value: ValueListEnhanced[]) {
        this._values = value;
    }

    public get listOfvaluesCode(): string {
        return this._listCode;
    }

    public set listOfvaluesCode(value: string) {
        this._listCode = value;
    }

}

export class ValueListEnhanced {
    key: string;
    value: string;
    defaultValue: boolean;
    additionalProperties: {
        key: string,
        value: string
    }[];
}

export class ListValuesEnhancedFactory {
    getInstance(obj: ListValuesEnhanced): ListValuesEnhanced {
        return new ListValuesEnhanced(obj.listCode, obj.values);
    }
}
