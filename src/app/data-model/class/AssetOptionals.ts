export class AssetOptionals {
  
  private _content?: any[];
  private _pageable?: Pageable;
  private _totalPages?: number;
  private _totalElements?: number;

  constructor(content: any[], pageable: Pageable, totalPages: number, totalElements: number) {
    this._content = content;
    this._pageable = pageable;
    this._totalPages = totalPages;
    this._totalElements = totalElements;
  }

  /**
   * BINDING SERVICE BE
   * 
   */

  get content(): any[] {
    return this._content;
  }
  set content(value: any[]) {
    this._content = value;
  }

  get pageable(): Pageable {
    return this._pageable;
  }
  set pageable(value: Pageable) {
    this._pageable = value;
  }

  get totalPages(): number {
    return this._totalPages;
  }
  set totalPages(value: number) {
    this._totalPages = value;
  }

  get totalElements(): number {
    return this._totalElements;
  }
  set totalElements(value: number) {
    this._totalElements = value;
  }

  /**
   * BINDINGS HTML
   */

  get optionalsContent(): any[] {
    return this._content;
  }
  set optionalsContent(value: any[]) {
    this._content = value;
  }

  get optionalsPageable(): Pageable {
    return this._pageable;
  }
  set optionalsPageable(value: Pageable) {
    this._pageable = value;
  }

  get optionalsTotalPages(): number {
    return this._totalPages;
  }
  set optionalsTotalPages(value: number) {
    this._totalPages = value;
  }

  get optionalsTotalElements(): number {
    return this._totalElements;
  }
  set optionalsTotalElements(value: number) {
    this._totalElements = value;
  }
}

export class Pageable {
  sort: Sort;
  pageSize: number;
  pageNumber: number;
  offset: number;
  unpaged: boolean;
  paged: boolean
}

export class Sort {
  unsorted: boolean;
  sorted: boolean;
  empty: boolean
}

export class OptionalElement {
  code: string;
  description: string;
  netCost: number;
  netCostVat: number;
  discount: boolean;
  premium?: boolean;
  selected?: boolean;
  premiumAdjustment ?: number;
}

export class AssetOptionalsFactory {

  getInstance(obj: AssetOptionals): AssetOptionals {
    return new AssetOptionals(obj.content, obj.pageable, obj.totalPages, obj.totalElements);
  }

}
