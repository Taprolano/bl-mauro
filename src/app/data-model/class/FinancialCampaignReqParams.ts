import {UtilsService} from '../../shared/services/utils.service';
import {LoggerService} from '../../shared/services/logger.service';
import {HttpParams} from '@angular/common/http';
import {CommercialAmountsReqParams} from './CommercialAmountsReqParams';


export class FinancialCampaignReqParams {

    /**
     * @ignore
     */
    private _baumuster: string;
    /**
     * @ignore
     */
    private _brandId: string;
    /**
     * @ignore
     */
    private _used: boolean;

    /**
     * @ignore
     */
    private _assetClassCode: string;

    /**
     * @ignore
     */
    private _assetType: string;

    /**
     *
     * @clazz {UtilsService}
     * @private
     */
    private _utils: UtilsService = new UtilsService();
    /**
     *
     * @clazz {LoggerService}
     * @private
     */
    private _logger: LoggerService = new LoggerService();

    /**
     *
     * @param baum
     * @param brand
     * @param assCode
     * @param assType
     * @param us
     */
    constructor(private baum: string, private brand: string, private assCode: string, private assType: string, private us: boolean) {
        this._baumuster = baum;
        this._brandId = brand;
        this._used = us;
        this._assetClassCode = assCode;
        this._assetType = assType;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get used(): boolean {
        return this._used;
    }

    set used(value: boolean) {
        this._used = value;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    /**
     * imposta i parametri per la request per l'api /financial/campaigns
     */
    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        this._logger.logInfo(CommercialAmountsReqParams.name, 'requestParameters2', this);

        if (!this._utils.isVoid(this._baumuster)) params = params.set('baumuster', this._baumuster);
        if (!this._utils.isVoid(this._brandId)) params = params.set('brandId', this._brandId);
        if (!this._utils.isVoid(this._used)) params = params.set('used', this._used.toString());
        if (!this._utils.isVoid(this._assetClassCode)) params = params.set('assetClassCode', this._assetClassCode);
        if (!this._utils.isVoid(this._assetType)) params = params.set('assetType', this._assetType);
        return params;
    }


}