export class FinancialServicesInner {

    private _clazz: string;
    private _supplyTypeCode: string;
    private _supplyElementCode: string;
    private _supplyTypeDesc: string;
    private _supplyElementDesc: string;
    private _categoryCode: string;
    private _parentSupplyTypeCode: string;
    private _defaultSupply: boolean;
    private _defaultElement: boolean;
    private _updatable: boolean;
    private _retrieveVat: boolean;
    private _parSupMand: string;
    private _flagMainSup: string;
    private _selezionabile: string;
    private _flagMandatory: boolean;
    private _tassoProdotto: boolean;
    private _supplyAmount: number;
    private _flagSconto: boolean;
    private _flagGratis: boolean;
    private _customerServiceCost: number;

    constructor(type?: string, supplyTypeCode?: string, supplyElementCode?: string, supplyTypeDesc?: string, supplyElementDesc?: string,
                categoryCode?: string, parentSupplyTypeCode?: string, defaultSupply?: boolean, defaultElement?: boolean,
                updatable?: boolean, retrieveVat?: boolean, parSupMand?: string, flagMainSup?: string, selezionabile?: string,
                flagMandatory?: boolean, tassoProdotto?: boolean, supplyAmount?: number, flagSconto?: boolean, flagGratis?: boolean, customerServiceCost?: number) {
        this._clazz = type;
        this._supplyTypeCode = supplyTypeCode;
        this._supplyElementCode = supplyElementCode;
        this._supplyTypeDesc = supplyTypeDesc;
        this._supplyElementDesc = supplyElementDesc;
        this._categoryCode = categoryCode;
        this._parentSupplyTypeCode = parentSupplyTypeCode;
        this._defaultSupply = defaultSupply;
        this._defaultElement = defaultElement;
        this._updatable = updatable;
        this._retrieveVat = retrieveVat;
        this._parSupMand = parSupMand;
        this._flagMainSup = flagMainSup;
        this._selezionabile = selezionabile;
        this._flagMandatory = flagMandatory;
        this._tassoProdotto = tassoProdotto;
        this._supplyAmount = supplyAmount;
        this._flagSconto = flagSconto;
        this._flagGratis = flagGratis;
        this._customerServiceCost = customerServiceCost;
    }

    /* GET */

    get clazz(): string {
        return this._clazz;
    }

    get supplyTypeCode(): string {
        return this._supplyTypeCode;
    }

    get supplyElementCode(): string {
        return this._supplyElementCode;
    }

    get supplyTypeDesc(): string {
        return this._supplyTypeDesc;
    }

    get supplyElementDesc(): string {
        return this._supplyElementDesc;
    }

    get categoryCode(): string {
        return this._categoryCode;
    }

    get parentSupplyTypeCode(): string {
        return this._parentSupplyTypeCode;
    }

    get defaultSupply(): boolean {
        return this._defaultSupply;
    }

    get defaultElement(): boolean {
        return this._defaultElement;
    }

    get updatable(): boolean {
        return this._updatable;
    }

    get retrieveVat(): boolean {
        return this._retrieveVat;
    }

    get parSupMand(): string {
        return this._parSupMand;
    }

    get flagMainSup(): string {
        return this._flagMainSup;
    }

    get selezionabile(): string {
        return this._selezionabile;
    }

    get flagMandatory(): boolean {
        return this._flagMandatory;
    }

    get tassoProdotto(): boolean {
        return this._tassoProdotto;
    }

    get supplyAmount(): number {
        return this._supplyAmount;
    }

    get flagSconto(): boolean {
        return this._flagSconto;
    }

    get flagGratis(): boolean {
        return this._flagGratis;
    }

    /* SET */

    set flagSconto(value: boolean) {
        this._flagSconto = value;
    }

    set flagGratis(value: boolean) {
        this._flagGratis = value;
    }

    set supplyAmount(value: number) {
        this._supplyAmount = value;
    }

    set clazz(value: string) {
        this._clazz = value;
    }

    set supplyTypeCode(value: string) {
        this._supplyTypeCode = value;
    }

    set supplyElementCode(value: string) {
        this._supplyElementCode = value;
    }

    set supplyTypeDesc(value: string) {
        this._supplyTypeDesc = value;
    }

    set supplyElementDesc(value: string) {
        this._supplyElementDesc = value;
    }

    set categoryCode(value: string) {
        this._categoryCode = value;
    }

    set parentSupplyTypeCode(value: string) {
        this._parentSupplyTypeCode = value;
    }

    set defaultSupply(value: boolean) {
        this._defaultSupply = value;
    }

    set defaultElement(value: boolean) {
        this._defaultElement = value;
    }

    set updatable(value: boolean) {
        this._updatable = value;
    }

    set retrieveVat(value: boolean) {
        this._retrieveVat = value;
    }

    set parSupMand(value: string) {
        this._parSupMand = value;
    }

    set flagMainSup(value: string) {
        this._flagMainSup = value;
    }

    set selezionabile(value: string) {
        this._selezionabile = value;
    }

    set flagMandatory(value: boolean) {
        this._flagMandatory = value;
    }

    set tassoProdotto(value: boolean) {
        this._tassoProdotto = value;
    }


    get customerServiceCost(): number {
        return this._customerServiceCost;
    }

    set customerServiceCost(value: number) {
        this._customerServiceCost = value;
    }
}

export class FinancialServicesInnerFactory {

    static getInstance(obj: FinancialServicesInner): FinancialServicesInner {
        let finServicesInner = new FinancialServicesInner();

        if (obj) {
            if (obj.clazz != undefined) finServicesInner.clazz = obj.clazz;
            if (obj.supplyTypeCode != undefined) finServicesInner.supplyTypeCode = obj.supplyTypeCode;
            if (obj.supplyElementCode != undefined) finServicesInner.supplyElementCode = obj.supplyElementCode;
            if (obj.supplyTypeDesc != undefined) finServicesInner.supplyTypeDesc = obj.supplyTypeDesc;
            if (obj.supplyElementDesc != undefined) finServicesInner.supplyElementDesc = obj.supplyElementDesc;
            if (obj.categoryCode != undefined) finServicesInner.categoryCode = obj.categoryCode;
            if (obj.parentSupplyTypeCode != undefined) finServicesInner.parentSupplyTypeCode = obj.parentSupplyTypeCode;
            if (obj.defaultSupply != undefined) finServicesInner.defaultSupply = obj.defaultSupply;
            if (obj.defaultElement != undefined) finServicesInner.defaultElement = obj.defaultElement;
            if (obj.updatable != undefined) finServicesInner.updatable = obj.updatable;
            if (obj.retrieveVat != undefined) finServicesInner.retrieveVat = obj.retrieveVat;
            if (obj.parSupMand != undefined) finServicesInner.parSupMand = obj.parSupMand;
            if (obj.flagMainSup != undefined) finServicesInner.flagMainSup = obj.flagMainSup;
            if (obj.selezionabile != undefined) finServicesInner.selezionabile = obj.selezionabile;
            if (obj.flagMandatory != undefined) finServicesInner.flagMandatory = obj.flagMandatory;
            if (obj.tassoProdotto != undefined) finServicesInner.tassoProdotto = obj.tassoProdotto;
            if (obj.supplyAmount != undefined) finServicesInner.supplyAmount = obj.supplyAmount;
            if (obj.flagGratis != undefined) finServicesInner.flagGratis = obj.flagGratis;
            if (obj.flagSconto != undefined) finServicesInner.flagSconto = obj.flagSconto;
            if (obj.customerServiceCost != undefined) finServicesInner.customerServiceCost = obj.customerServiceCost;
        }
        return finServicesInner;
    }

    getInstanceList(list: any): FinancialServicesInner[] {
        let financialServiceInner = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialServiceInner.push(FinancialServicesInnerFactory.getInstance(elem));
            });
        }
        return financialServiceInner;
    }
}