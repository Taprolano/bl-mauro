import {PaginationData} from "./PaginationData";

export class PaginatedObject<T> {
    private _pageData: PaginationData;
    private _object?: T;

    constructor(pagination: PaginationData, object:T = null) {
        this._pageData = pagination;
        this._object = !!object ? object: null;
    };

    get pageData(): PaginationData {
        return this._pageData;
    }

    set pageData(value: PaginationData) {
        this._pageData = value;
    }

    get object(): T {
        return this._object;
    }

    set object(value: T) {
        this._object = value;
    }

}
