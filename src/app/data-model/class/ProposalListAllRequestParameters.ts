import {HttpParams} from "@angular/common/http";
import {AbstractRequestParameters} from "./AbstractRequestParameters";

export class CreditQueueAllRequestParameters extends AbstractRequestParameters {

    private _asset: string;
    private _cds: string[];
    private _clickAndGo: string;
    private _concessionaria: string;
    private _contratto: string;
    private _deroga: string[];
    private _errorBancaDati: boolean;   
    private _k2k: boolean;
    private _nomeSogg: string;
    private _telaio: string;
    private _tipoProp: string;
    private _tipoSogg: string[];
    private _tipoCoda: string;
    private _vista: string;
    private _proposta: string;


    constructor(asset?: string, cds?: string[], clickAndGo?: string, concessionaria?: string, contratto?: string,
                deroga?: string[], errorBancaDati?: boolean, k2k?: boolean, nomeSogg?: string, telaio?: string,
                tipoProp?: string, tipoSogg?: string[], tipoCoda?: string, vista?: string) {
        super();
        this._asset = asset;
        this._cds = cds;
        this._clickAndGo = clickAndGo;
        this._concessionaria = concessionaria;
        this._contratto = contratto;
        this._deroga = deroga;
        this._errorBancaDati = errorBancaDati;
        this._k2k = k2k;
        this._nomeSogg = nomeSogg;
        this._telaio = telaio;
        this._tipoProp = tipoProp;
        this._tipoSogg = tipoSogg;
        this._tipoCoda = tipoCoda;
        this._vista = vista;
    }

    setParamsFromFilter(source: AbstractRequestParameters) {
        let filter = <CreditQueueAllRequestParameters>source;
        this._asset = filter.asset;
        this._cds = filter.cds;
        this._clickAndGo = filter.clickAndGo;
        this._concessionaria = filter.concessionaria;
        this._contratto = filter.contratto;
        this._deroga = filter.deroga;
        this._errorBancaDati = filter.errorBancaDati;
        this._k2k = filter.k2k;
        this._nomeSogg = filter.nomeSogg;
        this._telaio = filter.telaio;
        this._tipoProp = filter.tipoProp;
        this._tipoSogg = filter.tipoSogg;
        this._tipoCoda = filter.tipoCoda;
        this._vista = filter.vista;
        this._proposta = filter.proposta;
    }

    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        if (!this._utils.isVoid(this._asset)) params = params.set('asset', this._asset);
        if (!this._utils.isVoid(this._clickAndGo)) params = params.set('clickAndGo', this._clickAndGo);
        if (!this._utils.isVoid(this._concessionaria)) params = params.set('concessionaria', this._concessionaria);
        if (!this._utils.isVoid(this._contratto)) params = params.set('contratto', this._contratto);
        if (!this._utils.isVoid(this._tipoProp)) params = params.set('tipoProp', this._tipoProp);
        if (!this._utils.isVoid(this._nomeSogg)) params = params.set('nomeSogg', this._nomeSogg);
        if (!this._utils.isVoid(this._telaio)) params = params.set('telaio', this._telaio);
        if (!this._utils.isVoid(this._errorBancaDati)) params = params.set('errorBancaDati', this._errorBancaDati.toString());
        if (!this._utils.isVoid(this._k2k)) params = params.set('k2k', this._k2k.toString());
        if (!this._utils.isVoid(this.page)) params = params.set('page', this._page.toString());
        if (!this._utils.isVoid(this.size)) params = params.set('size', this._size.toString());
        if (!this._utils.isVoid(this.sort)) params = params.set('sort', this._sort);
        if (!this._utils.isVoid(this._tipoCoda)) params = params.set('tipoCoda', this._tipoCoda);
        if (!this._utils.isVoid(this._vista)) params = params.set('vista', this._vista);
        if (!this._utils.isVoid(this._proposta)) params = params.set('proposta', this._proposta);
        if (!this._utils.isVoid(this._tipoSogg)) {
            this._tipoSogg.forEach((item) => {
                params = params.append('tipoSogg', item);
            });
        }
        if (!this._utils.isVoid(this._deroga)) {
            this._deroga.forEach((item) => {
                params = params.append('deroga', item);
            });
        }
        if (!this._utils.isVoid(this._cds)) {
            this._cds.forEach((item) => {
                params = params.append('cds', item);
            });
        }
        this._logger.logInfo('CreditQueueAllRequestParameters', 'requestParameters (be-proposal)',
            params);
        return params;
    }

    public get asset(): string {
        return this._asset;
    }

    public set asset(value: string) {
        this._asset = value;
    }

    public get cds(): string[] {
        return this._cds;
    }

    public set cds(value: string[]) {
        this._cds = value;
    }

    public get clickAndGo(): string {
        return this._clickAndGo;
    }

    public set clickAndGo(value: string) {
        this._clickAndGo = value;
    }

    public get concessionaria(): string {
        return this._concessionaria;
    }

    public set concessionaria(value: string) {
        this._concessionaria = value;
    }

    public get contratto(): string {
        return this._contratto;
    }

    public set contratto(value: string) {
        this._contratto = value;
    }

    public get tipoSogg(): string[] {
        return this._tipoSogg;
    }

    public set tipoSogg(value: string[]) {
        this._tipoSogg = value;
    }

    public get deroga(): string[] {
        return this._deroga;
    }

    public set deroga(value: string[]) {
        this._deroga = value;
    }

    public get errorBancaDati(): boolean {
        return this._errorBancaDati;
    }
    public set errorBancaDati(value: boolean) {
        this._errorBancaDati = value;
    }

    public get telaio(): string {
        return this._telaio;
    }

    public set telaio(value: string) {
        this._telaio = value;
    }

    public get nomeSogg(): string {
        return this._nomeSogg;
    }
    public set nomeSogg(value: string) {
        this._nomeSogg = value;
    }

    public get tipoProp(): string {
        return this._tipoProp;
    }
    public set tipoProp(value: string) {
        this._tipoProp = value;
    }

    public get k2k(): boolean {
        return this._k2k;
    }
    public set k2k(value: boolean) {
        this._k2k = value;
    }

    public get tipoCoda(): string {
        return this._tipoCoda;
    }
    public set tipoCoda(value: string) {
        this._tipoCoda = value;
    }

    public get vista(): string {
        return this._vista;
    }
    public set vista(value: string) {
        this._vista = value;
    }

    public get proposta(): string {
        return this._proposta;
    }
    public set proposta(value: string) {
        this._proposta = value;
    }
}