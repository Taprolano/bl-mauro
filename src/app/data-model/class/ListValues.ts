export class ListValues {

  private _listCode?: string;
  private _values?: ValueList[];

  constructor(listCode: string, values: ValueList[]) {
    this._listCode = listCode;
    this._values = values;
  }

  /**
   * BINDING SERVICE BE
   * 
   */

  public get values(): ValueList[] {
    return this._values;
  }
  public set values(value: ValueList[]) {
    this._values = value;
  }

  public get listCode(): string {
    return this._listCode;
  }
  public set listCode(value: string) {
    this._listCode = value;
  }

  /**
   * BINDINGS IN HTML
   */

  public get listOfvalues(): ValueList[] {
    return this._values;
  }
  public set listOfvalues(value: ValueList[]) {
    this._values = value;
  }

  public get listOfvaluesCode(): string {
    return this._listCode;
  }
  public set listOfvaluesCode(value: string) {
    this._listCode = value;
  } 

}

export class ValueList {
  key: string;
  value: string;
  additionalProperties?: ValueList[];
}

export class ListValuesFactory {
  getInstance(obj: ListValues): ListValues {
    return new ListValues(obj.listCode, obj.values);
  }
}