import {UtilsService} from '../../../shared/services/utils.service';
import {IFinancialOptionals} from '../FinancialOptionals';

export class Optional {

    private _discountApplied: string;
    private _extraCode: string;
    private _extraDescription: string;
    private _extraDiscountAmount: number;
    private _extraNetCost: number;
    private _extraRvAdjustment: number;
    private _extraVatAmount: number;
    private _extraVatPercentage: number;


    get discountApplied(): string {
        return this._discountApplied;
    }

    set discountApplied(value: string) {
        this._discountApplied = value;
    }

    get extraCode(): string {
        return this._extraCode;
    }

    set extraCode(value: string) {
        this._extraCode = value;
    }

    get extraDescription(): string {
        return this._extraDescription;
    }

    set extraDescription(value: string) {
        this._extraDescription = value;
    }

    get extraDiscountAmount(): number {
        return this._extraDiscountAmount;
    }

    set extraDiscountAmount(value: number) {
        this._extraDiscountAmount = value;
    }

    get extraNetCost(): number {
        return this._extraNetCost;
    }

    set extraNetCost(value: number) {
        this._extraNetCost = value;
    }

    get extraRvAdjustment(): number {
        return this._extraRvAdjustment;
    }

    set extraRvAdjustment(value: number) {
        this._extraRvAdjustment = value;
    }

    get extraVatAmount(): number {
        return this._extraVatAmount;
    }

    set extraVatAmount(value: number) {
        this._extraVatAmount = value;
    }

    get extraVatPercentage(): number {
        return this._extraVatPercentage;
    }

    set extraVatPercentage(value: number) {
        this._extraVatPercentage = value;
    }

    public requestParameters(): ISaveSimulationOfferOptionals {
        let requestBody: ISaveSimulationOfferOptionals = {
            discountApplied: this._discountApplied,
            extraCode: this._extraCode,
            extraDescription: this._extraDescription,
            extraDiscountAmount: this._extraDiscountAmount,
            extraNetCost: this._extraNetCost,
            extraRvAdjustment: this._extraRvAdjustment,
            extraVatAmount: this._extraVatAmount,
            extraVatPercentage: this._extraVatPercentage
        };

        return requestBody;
    }

}

export class SaveDealerOfferOptionalFactory {

    static getInstance(obj: Optional): Optional {

        let utils: UtilsService = new UtilsService();
        let optional = new Optional();

        if (obj) {
            if (!utils.isUndefided(obj.discountApplied)) optional.discountApplied = obj.discountApplied;
            if (!utils.isUndefided(obj.extraCode)) optional.extraCode = obj.extraCode;
            if (!utils.isUndefided(obj.extraDescription)) optional.extraDescription = obj.extraDescription;
            if (!utils.isUndefided(obj.extraDiscountAmount)) optional.extraDiscountAmount = obj.extraDiscountAmount;
            if (!utils.isUndefided(obj.extraNetCost)) optional.extraNetCost = obj.extraNetCost;
            if (!utils.isUndefided(obj.extraRvAdjustment)) optional.extraRvAdjustment = obj.extraRvAdjustment;
            if (!utils.isUndefided(obj.extraVatAmount)) optional.extraVatAmount = obj.extraVatAmount;
            if (!utils.isUndefided(obj.extraVatPercentage)) optional.extraVatPercentage = obj.extraVatPercentage;
        }

        return optional;
    }


    static getInstanceList(list: any): Optional[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferOptionalFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

    static getInstanceListParam(list: any): ISaveSimulationOfferOptionals[] {
        let saveSimulationOfferOptionalsList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                saveSimulationOfferOptionalsList.push(this.getInstance(elem).requestParameters());
            });
        }
        return saveSimulationOfferOptionalsList;
    }

}

export interface ISaveSimulationOfferOptionals {

    discountApplied: string;
    extraCode: string;
    extraDescription: string;
    extraDiscountAmount: number;
    extraNetCost: number;
    extraRvAdjustment: number;
    extraVatAmount: number;
    extraVatPercentage: number;

}
