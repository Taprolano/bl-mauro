import {UtilsService} from '../../../shared/services/utils.service';
import {ISaveSimulationOfferOptionals} from './Optional';
import {ContractDet} from './ContractDet';

export class PropEccedenza {

    private _eccedenzaEurotax: string;
    private _number: string;
    private _quotazioneEurotax: string;
    private _valoreEurotax: string;
    private _version: string;


    get eccedenzaEurotax(): string {
        return this._eccedenzaEurotax;
    }

    set eccedenzaEurotax(value: string) {
        this._eccedenzaEurotax = value;
    }

    get number(): string {
        return this._number;
    }

    set number(value: string) {
        this._number = value;
    }

    get quotazioneEurotax(): string {
        return this._quotazioneEurotax;
    }

    set quotazioneEurotax(value: string) {
        this._quotazioneEurotax = value;
    }

    get valoreEurotax(): string {
        return this._valoreEurotax;
    }

    set valoreEurotax(value: string) {
        this._valoreEurotax = value;
    }

    get version(): string {
        return this._version;
    }

    set version(value: string) {
        this._version = value;
    }

}

export class SaveDealerOfferPropEccedenzaFactory {

    static getInstance(obj: PropEccedenza): PropEccedenza {

        let utils: UtilsService = new UtilsService();
        let prop = new PropEccedenza();

        if (obj) {
            if (!utils.isUndefided(obj.eccedenzaEurotax)) prop.eccedenzaEurotax = obj.eccedenzaEurotax;
            if (!utils.isUndefided(obj.number)) prop.number = obj.number;
            if (!utils.isUndefided(obj.quotazioneEurotax)) prop.quotazioneEurotax = obj.quotazioneEurotax;
            if (!utils.isUndefided(obj.valoreEurotax)) prop.valoreEurotax = obj.valoreEurotax;
            if (!utils.isUndefided(obj.version)) prop.version = obj.version;
        }

        return prop;
    }


    static getInstanceList(list: any): PropEccedenza[] {
        let propList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                propList.push(SaveDealerOfferPropEccedenzaFactory.getInstance(elem));
            });
        }
        return propList;
    }

    static getInstanceParams(obj: PropEccedenza): Object {

        let requestBody = {};

        if (!!obj) {
            requestBody = {
                eccedenzaEurotax: obj.eccedenzaEurotax,
                number: obj.number,
                quotazioneEurotax: obj.quotazioneEurotax,
                valoreEurotax: obj.valoreEurotax,
                version: obj.version
            };

        }

        return requestBody;

    }

}


