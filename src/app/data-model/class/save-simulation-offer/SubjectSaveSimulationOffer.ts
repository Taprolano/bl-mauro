import {UtilsService} from '../../../shared/services/utils.service';
import {Address, IAddress, SaveDealerOfferAddressFactory} from './Address';
import {Bank, IBank, SaveDealerOfferBankFactory} from './Bank';
import {SbjDocument, SaveDealerOfferSbjDocumentFactory, ISbjDocument} from './SbjDocument';
import {IPHEntGovern, PHEntGovern, SaveDealerOfferPHEntGovernFactory} from './PHEntGovern';
import {ContractDet} from './ContractDet';
import {ISaveSimulationOfferOptionals} from './Optional';

export class SubjectSaveSimulationOffer {

    private _address_Preffered_Account: string;
    private _addresses: Address[];
    private _banks: Bank[];
    private _bp_Bank_Preffered_Account: string;
    private _cdTer: number;
    private _codINPS: string;
    private _codPostazione: number;
    private _codRae: string;
    private _codRuoloSoggetto: string;
    private _codSae: string;
    private _codSoggettoLinkato: number;
    private _codTipoSoggetto: string;
    private _codiceLegale: number;
    private _corporate: boolean;
    private _documents: SbjDocument[];
    private _fiscalCode: string;
    private _flagPF: string;
    private _flagPrivacy: string;
    private _legalAddress: Address;
    private _legameProposta: string[];
    private _legameSoggetto: object;
    private _legamiPhoenixEnteGovernativa: PHEntGovern[];
    private _preferredAddressAccount: Address;
    private _preferredBankAccount: Bank;
    private _ragioneSociale: string;
    private _retail: boolean;
    private _ruolo: string;
    private _ruoloSocieta: string;
    private _settMerceologico: string;
    private _subjectCode: number;
    private _uniqueKey: string;


    get address_Preffered_Account(): string {
        return this._address_Preffered_Account;
    }

    set address_Preffered_Account(value: string) {
        this._address_Preffered_Account = value;
    }

    get addresses(): Address[] {
        return this._addresses;
    }

    set addresses(value: Address[]) {
        this._addresses = value;
    }

    get banks(): Bank[] {
        return this._banks;
    }

    set banks(value: Bank[]) {
        this._banks = value;
    }

    get bp_Bank_Preffered_Account(): string {
        return this._bp_Bank_Preffered_Account;
    }

    set bp_Bank_Preffered_Account(value: string) {
        this._bp_Bank_Preffered_Account = value;
    }

    get cdTer(): number {
        return this._cdTer;
    }

    set cdTer(value: number) {
        this._cdTer = value;
    }

    get codINPS(): string {
        return this._codINPS;
    }

    set codINPS(value: string) {
        this._codINPS = value;
    }

    get codPostazione(): number {
        return this._codPostazione;
    }

    set codPostazione(value: number) {
        this._codPostazione = value;
    }

    get codRae(): string {
        return this._codRae;
    }

    set codRae(value: string) {
        this._codRae = value;
    }

    get codRuoloSoggetto(): string {
        return this._codRuoloSoggetto;
    }

    set codRuoloSoggetto(value: string) {
        this._codRuoloSoggetto = value;
    }

    get codSae(): string {
        return this._codSae;
    }

    set codSae(value: string) {
        this._codSae = value;
    }

    get codSoggettoLinkato(): number {
        return this._codSoggettoLinkato;
    }

    set codSoggettoLinkato(value: number) {
        this._codSoggettoLinkato = value;
    }

    get codTipoSoggetto(): string {
        return this._codTipoSoggetto;
    }

    set codTipoSoggetto(value: string) {
        this._codTipoSoggetto = value;
    }

    get codiceLegale(): number {
        return this._codiceLegale;
    }

    set codiceLegale(value: number) {
        this._codiceLegale = value;
    }

    get corporate(): boolean {
        return this._corporate;
    }

    set corporate(value: boolean) {
        this._corporate = value;
    }

    get documents(): SbjDocument[] {
        return this._documents;
    }

    set documents(value: SbjDocument[]) {
        this._documents = value;
    }

    get fiscalCode(): string {
        return this._fiscalCode;
    }

    set fiscalCode(value: string) {
        this._fiscalCode = value;
    }

    get flagPF(): string {
        return this._flagPF;
    }

    set flagPF(value: string) {
        this._flagPF = value;
    }

    get flagPrivacy(): string {
        return this._flagPrivacy;
    }

    set flagPrivacy(value: string) {
        this._flagPrivacy = value;
    }

    get legalAddress(): Address {
        return this._legalAddress;
    }

    set legalAddress(value: Address) {
        this._legalAddress = value;
    }

    get legameProposta(): string[] {
        return this._legameProposta;
    }

    set legameProposta(value: string[]) {
        this._legameProposta = value;
    }

    get legameSoggetto(): object {
        return this._legameSoggetto;
    }

    set legameSoggetto(value: object) {
        this._legameSoggetto = value;
    }

    get legamiPhoenixEnteGovernativa(): PHEntGovern[] {
        return this._legamiPhoenixEnteGovernativa;
    }

    set legamiPhoenixEnteGovernativa(value: PHEntGovern[]) {
        this._legamiPhoenixEnteGovernativa = value;
    }

    get preferredAddressAccount(): Address {
        return this._preferredAddressAccount;
    }

    set preferredAddressAccount(value: Address) {
        this._preferredAddressAccount = value;
    }

    get preferredBankAccount(): Bank {
        return this._preferredBankAccount;
    }

    set preferredBankAccount(value: Bank) {
        this._preferredBankAccount = value;
    }

    get ragioneSociale(): string {
        return this._ragioneSociale;
    }

    set ragioneSociale(value: string) {
        this._ragioneSociale = value;
    }

    get retail(): boolean {
        return this._retail;
    }

    set retail(value: boolean) {
        this._retail = value;
    }

    get ruolo(): string {
        return this._ruolo;
    }

    set ruolo(value: string) {
        this._ruolo = value;
    }

    get ruoloSocieta(): string {
        return this._ruoloSocieta;
    }

    set ruoloSocieta(value: string) {
        this._ruoloSocieta = value;
    }

    get settMerceologico(): string {
        return this._settMerceologico;
    }

    set settMerceologico(value: string) {
        this._settMerceologico = value;
    }

    get subjectCode(): number {
        return this._subjectCode;
    }

    set subjectCode(value: number) {
        this._subjectCode = value;
    }

    get uniqueKey(): string {
        return this._uniqueKey;
    }

    set uniqueKey(value: string) {
        this._uniqueKey = value;
    }

    public requestParameters(): ISubjectSaveSimulationOffer {
        let requestBody: ISubjectSaveSimulationOffer = {
            address_Preffered_Account: this._address_Preffered_Account,
            addresses: SaveDealerOfferAddressFactory.getInstanceListParam(this._addresses),
            banks: SaveDealerOfferBankFactory.getInstanceListParam(this._banks),
            bp_Bank_Preffered_Account: this._bp_Bank_Preffered_Account,
            cdTer: this._cdTer,
            codINPS: this._codINPS,
            codPostazione: this._codPostazione,
            codRae: this._codRae,
            codRuoloSoggetto: this._codRuoloSoggetto,
            codSae: this._codSae,
            codSoggettoLinkato: this._codSoggettoLinkato,
            codTipoSoggetto: this._codTipoSoggetto,
            codiceLegale: this._codiceLegale,
            corporate: this._corporate,
            documents: SaveDealerOfferSbjDocumentFactory.getInstanceListParam(this._documents),
            fiscalCode: this._fiscalCode,
            flagPF: this._flagPF,
            flagPrivacy: this._flagPrivacy,
            legalAddress: SaveDealerOfferAddressFactory.getInstanceParams(this._legalAddress),
            legameProposta: [],
            legameSoggetto: {},
            legamiPhoenixEnteGovernativa: SaveDealerOfferPHEntGovernFactory.getInstanceListParam(this._legamiPhoenixEnteGovernativa),
            preferredAddressAccount: SaveDealerOfferAddressFactory.getInstanceParams(this._preferredAddressAccount),
            preferredBankAccount: SaveDealerOfferBankFactory.getInstanceParams(this._preferredBankAccount),
            ragioneSociale: this._ragioneSociale,
            retail: this._retail,
            ruolo: this._ruolo,
            ruoloSocieta: this._ruoloSocieta,
            settMerceologico: this._settMerceologico,
            subjectCode: this._subjectCode,
            uniqueKey: this._uniqueKey
        };

        return requestBody;
    }

}

export class SaveDealerOfferSubjectFactory {

    static getInstance(obj: SubjectSaveSimulationOffer): SubjectSaveSimulationOffer {

        let addressFactory = new SaveDealerOfferAddressFactory();
        let addresses = addressFactory.getInstanceList(obj.addresses);

        let banksFactory = new SaveDealerOfferBankFactory();
        let banks = banksFactory.getInstanceList(obj.banks);

        let documentsFactory = new SaveDealerOfferSbjDocumentFactory();
        let documents = documentsFactory.getInstanceList(obj.documents);

        let legamiPhoenixEnteGovernativaFactory = new SaveDealerOfferPHEntGovernFactory();
        let legamiPhoenixEnteGovernativa = legamiPhoenixEnteGovernativaFactory.getInstanceList(obj.legamiPhoenixEnteGovernativa);

        let legalAddress = SaveDealerOfferAddressFactory.getInstance(obj.legalAddress);
        let preferredAddressAccount = SaveDealerOfferAddressFactory.getInstance(obj.preferredAddressAccount);
        let preferredBankAccount = SaveDealerOfferBankFactory.getInstance(obj.preferredBankAccount);

        let utils: UtilsService = new UtilsService();
        let subject = new SubjectSaveSimulationOffer();

        if (obj) {
            if (!utils.isUndefided(obj.address_Preffered_Account)) subject.address_Preffered_Account = obj.address_Preffered_Account;
            if (!utils.isUndefided(addresses)) subject.addresses = addresses;
            if (!utils.isUndefided(banks)) subject.banks = banks;
            if (!utils.isUndefided(obj.bp_Bank_Preffered_Account)) subject.bp_Bank_Preffered_Account = obj.bp_Bank_Preffered_Account;
            if (!utils.isUndefided(obj.cdTer)) subject.cdTer = obj.cdTer;
            if (!utils.isUndefided(obj.codINPS)) subject.codINPS = obj.codINPS;
            if (!utils.isUndefided(obj.codPostazione)) subject.codPostazione = obj.codPostazione;
            if (!utils.isUndefided(obj.codRae)) subject.codRae = obj.codRae;
            if (!utils.isUndefided(obj.codRuoloSoggetto)) subject.codRuoloSoggetto = obj.codRuoloSoggetto;
            if (!utils.isUndefided(obj.codSae)) subject.codSae = obj.codSae;
            if (!utils.isUndefided(obj.codSoggettoLinkato)) subject.codSoggettoLinkato = obj.codSoggettoLinkato;
            if (!utils.isUndefided(obj.codTipoSoggetto)) subject.codTipoSoggetto = obj.codTipoSoggetto;
            if (!utils.isUndefided(obj.codiceLegale)) subject.codiceLegale = obj.codiceLegale;
            if (!utils.isUndefided(obj.corporate)) subject.corporate = obj.corporate;
            if (!utils.isUndefided(documents)) subject.documents = documents;
            if (!utils.isUndefided(obj.fiscalCode)) subject.fiscalCode = obj.fiscalCode;
            if (!utils.isUndefided(obj.flagPF)) subject.flagPF = obj.flagPF;
            if (!utils.isUndefided(obj.flagPrivacy)) subject.flagPrivacy = obj.flagPrivacy;
            if (!utils.isUndefided(legalAddress)) subject.legalAddress = legalAddress;
            if (!utils.isUndefided(obj.legameProposta)) subject.legameProposta = obj.legameProposta;
            if (!utils.isUndefided(obj.legameSoggetto)) subject.legameSoggetto = obj.legameSoggetto;
            if (!utils.isUndefided(legamiPhoenixEnteGovernativa)) subject.legamiPhoenixEnteGovernativa = legamiPhoenixEnteGovernativa;
            if (!utils.isUndefided(preferredAddressAccount)) subject.preferredAddressAccount = preferredAddressAccount;
            if (!utils.isUndefided(preferredBankAccount)) subject.preferredBankAccount = preferredBankAccount;
            if (!utils.isUndefided(obj.ragioneSociale)) subject.ragioneSociale = obj.ragioneSociale;
            if (!utils.isUndefided(obj.retail)) subject.retail = obj.retail;
            if (!utils.isUndefided(obj.ruolo)) subject.ruolo = obj.ruolo;
            if (!utils.isUndefided(obj.ruoloSocieta)) subject.ruoloSocieta = obj.ruoloSocieta;
            if (!utils.isUndefided(obj.settMerceologico)) subject.settMerceologico = obj.settMerceologico;
            if (!utils.isUndefided(obj.subjectCode)) subject.subjectCode = obj.subjectCode;
            if (!utils.isUndefided(obj.uniqueKey)) subject.uniqueKey = obj.uniqueKey;
        }

        return subject;
    }

    static getInstanceParams(obj: SubjectSaveSimulationOffer): Object {
        let resp = {
            address_Preffered_Account: obj.address_Preffered_Account,
            addresses: SaveDealerOfferAddressFactory.getInstanceListParam(obj.addresses),
            banks: SaveDealerOfferBankFactory.getInstanceListParam(obj.banks),
            bp_Bank_Preffered_Account: obj.bp_Bank_Preffered_Account,
            cdTer: obj.cdTer,
            codINPS: obj.codINPS,
            codPostazione: obj.codPostazione,
            codRae: obj.codRae,
            codRuoloSoggetto: obj.codRuoloSoggetto,
            codSae: obj.codSae,
            codSoggettoLinkato: obj.codSoggettoLinkato,
            codTipoSoggetto: obj.codTipoSoggetto,
            codiceLegale: obj.codiceLegale,
            corporate: obj.corporate,
            documents: SaveDealerOfferSbjDocumentFactory.getInstanceListParam(obj.documents),
            fiscalCode: obj.fiscalCode,
            flagPF: obj.flagPF,
            flagPrivacy: obj.flagPrivacy,
            legalAddress: SaveDealerOfferAddressFactory.getInstanceParams(obj.legalAddress),
            legameProposta: [],
            legameSoggetto: {},
            legamiPhoenixEnteGovernativa: SaveDealerOfferPHEntGovernFactory.getInstanceListParam(obj.legamiPhoenixEnteGovernativa),
            preferredAddressAccount: SaveDealerOfferAddressFactory.getInstanceParams(obj.preferredAddressAccount),
            preferredBankAccount: SaveDealerOfferBankFactory.getInstanceListParam(obj.preferredBankAccount),
            ragioneSociale: obj.ragioneSociale,
            retail: obj.retail,
            ruolo: obj.ruolo,
            ruoloSocieta: obj.ruoloSocieta,
            settMerceologico: obj.settMerceologico,
            subjectCode: obj.subjectCode,
            uniqueKey: obj.uniqueKey
        };

        return resp;
    }

    static getInstanceListParam(list: any): ISubjectSaveSimulationOffer[] {
        let subjectSaveSimulationOfferList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                subjectSaveSimulationOfferList.push(this.getInstance(elem).requestParameters());
            });
        }
        return subjectSaveSimulationOfferList;
    }

    getInstanceList(list: any): SubjectSaveSimulationOffer[] {
        let subjectList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                subjectList.push(SaveDealerOfferSubjectFactory.getInstance(elem));
            });
        }
        return subjectList;
    }

}

export interface ISubjectSaveSimulationOffer {

    address_Preffered_Account: string;
    addresses: IAddress[];
    banks: IBank[];
    bp_Bank_Preffered_Account: string;
    cdTer: number;
    codINPS: string;
    codPostazione: number;
    codRae: string;
    codRuoloSoggetto: string;
    codSae: string;
    codSoggettoLinkato: number;
    codTipoSoggetto: string;
    codiceLegale: number;
    corporate: boolean;
    documents: ISbjDocument[];
    fiscalCode: string;
    flagPF: string;
    flagPrivacy: string;
    legalAddress: IAddress;
    legameProposta: string[];
    legameSoggetto: object;
    legamiPhoenixEnteGovernativa: IPHEntGovern[];
    preferredAddressAccount: IAddress;
    preferredBankAccount: IBank;
    ragioneSociale: string;
    retail: boolean;
    ruolo: string;
    ruoloSocieta: string;
    settMerceologico: string;
    subjectCode: number;
    uniqueKey: string;

}
