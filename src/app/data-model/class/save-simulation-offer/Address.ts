import {UtilsService} from '../../../shared/services/utils.service';
import {ContactInfo, SaveDealerOfferContactInfoFactory} from './ContactInfo';

export class Address {

    private _address: string;
    private _addressCode: string;
    private _cap: string;
    private _capExtended: string;
    private _cityCode: number;
    private _cityExtended: string;
    private _codLocalita: string;
    private _contactInfo: ContactInfo[];
    private _descTipoIndirizzo: string;
    private _email: string;
    private _houseNumber: string;
    private _nationCode: number;
    private _place: string;
    private _preferredAddressAccount: boolean;
    private _progressivo: number;
    private _province: string;
    private _tempoIndirizzo: number;
    private _type: string;


    get address(): string {
        return this._address;
    }

    set address(value: string) {
        this._address = value;
    }

    get addressCode(): string {
        return this._addressCode;
    }

    set addressCode(value: string) {
        this._addressCode = value;
    }

    get cap(): string {
        return this._cap;
    }

    set cap(value: string) {
        this._cap = value;
    }

    get capExtended(): string {
        return this._capExtended;
    }

    set capExtended(value: string) {
        this._capExtended = value;
    }

    get cityCode(): number {
        return this._cityCode;
    }

    set cityCode(value: number) {
        this._cityCode = value;
    }

    get cityExtended(): string {
        return this._cityExtended;
    }

    set cityExtended(value: string) {
        this._cityExtended = value;
    }

    get codLocalita(): string {
        return this._codLocalita;
    }

    set codLocalita(value: string) {
        this._codLocalita = value;
    }

    get contactInfo(): ContactInfo[] {
        return this._contactInfo;
    }

    set contactInfo(value: ContactInfo[]) {
        this._contactInfo = value;
    }

    get descTipoIndirizzo(): string {
        return this._descTipoIndirizzo;
    }

    set descTipoIndirizzo(value: string) {
        this._descTipoIndirizzo = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get houseNumber(): string {
        return this._houseNumber;
    }

    set houseNumber(value: string) {
        this._houseNumber = value;
    }

    get nationCode(): number {
        return this._nationCode;
    }

    set nationCode(value: number) {
        this._nationCode = value;
    }

    get place(): string {
        return this._place;
    }

    set place(value: string) {
        this._place = value;
    }

    get preferredAddressAccount(): boolean {
        return this._preferredAddressAccount;
    }

    set preferredAddressAccount(value: boolean) {
        this._preferredAddressAccount = value;
    }

    get progressivo(): number {
        return this._progressivo;
    }

    set progressivo(value: number) {
        this._progressivo = value;
    }

    get province(): string {
        return this._province;
    }

    set province(value: string) {
        this._province = value;
    }

    get tempoIndirizzo(): number {
        return this._tempoIndirizzo;
    }

    set tempoIndirizzo(value: number) {
        this._tempoIndirizzo = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    public requestParameters(): IAddress {
        let requestBody: IAddress = {
            address: this._address,
            addressCode: this._addressCode,
            cap: this._cap,
            capExtended: this._capExtended,
            cityCode: this._cityCode,
            cityExtended: this._cityExtended,
            codLocalita: this._codLocalita,
            contactInfo: SaveDealerOfferContactInfoFactory.getInstanceListParam(this._contactInfo),
            descTipoIndirizzo: this._descTipoIndirizzo,
            email: this._email,
            houseNumber: this._houseNumber,
            nationCode: this._nationCode,
            place: this._place,
            preferredAddressAccount: this._preferredAddressAccount,
            progressivo: this._progressivo,
            province: this._province,
            tempoIndirizzo: this._tempoIndirizzo,
            type: this._type
        };

        return requestBody;
    }

}

export class SaveDealerOfferAddressFactory {

    static getInstance(obj: Address): Address {

        let utils: UtilsService = new UtilsService();
        let address = new Address();

        if (obj) {

            let contactInfoFactory = new SaveDealerOfferContactInfoFactory();
            let contactInfo = contactInfoFactory.getInstanceList(obj.contactInfo);

            if (!utils.isUndefided(obj.address)) address.address = obj.address;
            if (!utils.isUndefided(obj.addressCode)) address.addressCode = obj.addressCode;
            if (!utils.isUndefided(obj.cap)) address.cap = obj.cap;
            if (!utils.isUndefided(obj.capExtended)) address.capExtended = obj.capExtended;
            if (!utils.isUndefided(obj.cityCode)) address.cityCode = obj.cityCode;
            if (!utils.isUndefided(obj.cityExtended)) address.cityExtended = obj.cityExtended;
            if (!utils.isUndefided(obj.codLocalita)) address.codLocalita = obj.codLocalita;
            if (!utils.isUndefided(contactInfo)) address.contactInfo = contactInfo;
            if (!utils.isUndefided(obj.descTipoIndirizzo)) address.descTipoIndirizzo = obj.descTipoIndirizzo;
            if (!utils.isUndefided(obj.houseNumber)) address.houseNumber = obj.houseNumber;
            if (!utils.isUndefided(obj.nationCode)) address.nationCode = obj.nationCode;
            if (!utils.isUndefided(obj.place)) address.place = obj.place;
            if (!utils.isUndefided(obj.preferredAddressAccount)) address.preferredAddressAccount = obj.preferredAddressAccount;
            if (!utils.isUndefided(obj.progressivo)) address.progressivo = obj.progressivo;
            if (!utils.isUndefided(obj.province)) address.province = obj.province;
            if (!utils.isUndefided(obj.tempoIndirizzo)) address.tempoIndirizzo = obj.tempoIndirizzo;
            if (!utils.isUndefided(obj.type)) address.type = obj.type;

        }

        return address;
    }

    static getInstanceListParam(list: any): IAddress[] {
        let addressList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                addressList.push(this.getInstance(elem).requestParameters());
            });
        }
        return addressList;
    }

    static getInstanceParams(obj: Address): IAddress {
        let resp = {
            address: obj.address,
            addressCode: obj.addressCode,
            cap: obj.cap,
            capExtended: obj.capExtended,
            cityCode: obj.cityCode,
            cityExtended: obj.cityExtended,
            codLocalita: obj.codLocalita,
            contactInfo: SaveDealerOfferContactInfoFactory.getInstanceListParam(obj.contactInfo),
            descTipoIndirizzo: obj.descTipoIndirizzo,
            email: obj.email,
            houseNumber: obj.houseNumber,
            nationCode: obj.nationCode,
            place: obj.place,
            preferredAddressAccount: obj.preferredAddressAccount,
            progressivo: obj.progressivo,
            province: obj.province,
            tempoIndirizzo: obj.tempoIndirizzo,
            type: obj.type
        };

        return resp;
    }

    getInstanceList(list: any): Address[] {
        let addressList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                addressList.push(SaveDealerOfferAddressFactory.getInstance(elem));
            });
        }
        return addressList;
    }

}

export interface IAddress {

    address: string;
    addressCode: string;
    cap: string;
    capExtended: string;
    cityCode: number;
    cityExtended: string;
    codLocalita: string;
    contactInfo: ContactInfo[];
    descTipoIndirizzo: string;
    email: string;
    houseNumber: string;
    nationCode: number;
    place: string;
    preferredAddressAccount: boolean;
    progressivo: number;
    province: string;
    tempoIndirizzo: number;
    type: string;

}
