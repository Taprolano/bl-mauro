import {HttpParams} from '@angular/common/http';
import {Preparation, PreparationFactory} from './Preparation';
import {Optional, SaveDealerOfferOptionalFactory} from './Optional';
import {PropEccedenza, SaveDealerOfferPropEccedenzaFactory} from './PropEccedenza';
import {SaveSimulationOfferProposal, SaveDealerOfferProposalFactory} from './SaveSimulationOfferProposal';
import {CashFlow, SaveDealerOfferCashFlowFactory} from './CashFlow';
import {SaveDealerOfferSupplyServiceFactory, SupplyService} from './SupplyService';
import {IBlockedFields} from "../../../proposals/work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/interface/IBlockedFields";
import {UtilsService} from '../../../shared/services/utils.service';
import {AttachmentFactory} from '../Attachment';

export class BeSaveSimulationOfferReqParams {

    private _adminFeeShareOverride: boolean;
    private _allestimenti: Preparation[];
    private _anticipoNetto: number;
    private _anticipoPercentuale: number;
    private _assetClassCode: string;
    private _assetModel: string;
    private _assetType: string;
    private _balloon: boolean;
    private _balloonPercentuale: number;
    private _baumuster: string;
    private _brandId: string;
    private _campaignCode: string;
    private _campiBloccati: IBlockedFields;
    private _campiReadOnly: IBlockedFields;
    private _codAnticipo: string;
    private _codCanoneRata: string;
    private _codeIpoteca: string;
    private _codiceGruppoDegrado: string;
    private _codiceTipoInteresse: string;
    private _commCamp: boolean;
    private _contractEnd: string; // data
    private _contractLength: number;
    private _contributoCampagna: number;
    private _contributoConcessionario: number;
    private _contributoRetention: number;
    private _currentMileAge: number;
    private _customerFullName: string;
    private _customerLastName: string;
    private _customerMailAddress: string;
    private _customerName: string;
    private _customerPhoneNumber: string;
    private _dealFixCom: number;
    private _dealManuContrComm: boolean;
    private _dealPercCom: number;
    private _dealPercManuContrComm: number;
    private _dealerBuyBack: boolean;
    private _dealerCDTer: number;
    private _dealerSIpercentage: number;
    private _decorrenza: string;
    private _derogaBlocchiServiziAssicurativi: boolean;
    private _derogaCommerciale: boolean;
    private _derogaContributo: boolean;
    private _derogatedRateDeltaCommission: number;
    private _derogatedSellerCommission: number;
    private _distance: number;
    private _fattura: boolean;
    private _financedAmount: number;
    private _financialProductCode: string;
    private _flagShare: boolean;
    private _franchised: boolean;
    private _frequency: number;
    private _idProposal: string;
    private _importoAnticipo: number;
    private _importoBalloonNetto: number;
    private _importoFornitura: number;
    private _importoListino: number;
    private _importoRataAnticipoServizi: number;
    private _importoRataBalloon: number;
    private _importoRataBalloonServizi: number;
    private _importoRataServizi: number;
    private _incentivoVendita: number;
    private _ipt: number;
    private _kilometraggio: number;
    private _leasing: boolean;
    private _makeCode: string;
    private _manuFixContrComm: number;
    private _manuPercContrComm: number;
    private _marca: string;
    private _mfr: number;
    private _mss: number;
    private _mustUseSpIstr: boolean;
    private _netCost: number;
    private _operazioneK2K: boolean;
    private _optionals: Optional[];
    private _percentAtDealer: number;
    private _percentAtSales: number;
    private _percentualeDiListino: number;
    private _prodottoFinanziario: string;
    private _propEccedenza: PropEccedenza;
    private _proposal: SaveSimulationOfferProposal;
    private _proposalDate: string; // data
    private _proposalStatus: string;
    private _provImportoDeltaTasso: number;
    private _provImportoDeltaTassoPerc: number;
    private _provImportoServizi: number;
    private _provImportoServiziPerc: number;
    private _provImportoSpeseIstruttoria: number;
    private _provImportoSpeseIstruttoriaPerc: number;
    private _provImportoTotale: number;
    private _provImportoVendita: number;
    private _provImportoVenditaPerc: number;
    private _rata: number;
    private _rataNetta: number;
    private _rataPercentuale: number;
    private _rate: CashFlow[];
    private _registrationDate: string; // data
    private _rental: boolean;
    private _richiestaFattura: boolean;
    private _selFixComAmount: number;
    private _selFixComPerc: number;
    private _sellerCommission: number;
    private _sellerSIpercentage: number;
    private _serviceEntities: SupplyService[];
    private _speseAnticipo: number;
    private _speseDiIstruttoria: number;
    private _speseRataCanone: number;
    private _spread: number;
    private _supplyServices: SupplyService[];
    private _taeg: number;
    private _tan: number;
    private _tassoBase: number;
    private _tipoInteresse: string;
    private _tipoTasso: string;
    private _totalAmount: number;
    private _totalCreditAmount: number;
    private _totalCustomerAmount: number;
    private _used: boolean;
    private _valoreCommissione: number;
    private _vatCode: string;
    private _vatPercentage: number;
    private _version: number;
    private _voucherCode: string;


    constructor(adminFeeShareOverride?: boolean, allestimenti?: Preparation[], anticipoNetto?: number, anticipoPercentuale?: number,
                assetClassCode?: string, assetModel?: string, assetType?: string, balloon?: boolean, balloonPercentuale?: number,
                baumuster?: string, brandId?: string, campaignCode?: string, campiBloccati?: IBlockedFields, campiReadOnly?: IBlockedFields, codAnticipo?: string,
                codCanoneRata?: string, codeIpoteca?: string, codiceGruppoDegrado?: string, codiceTipoInteresse?: string, commCamp?: boolean,
                contractEnd?: string, contractLength?: number, contributoCampagna?: number, contributoConcessionario?: number,
                contributoRetention?: number, currentMileAge?: number, customerFullName?: string, customerLastName?: string,
                customerMailAddress?: string, customerName?: string, customerPhoneNumber?: string, dealFixCom?: number, dealManuContrComm?: boolean,
                dealPercCom?: number, dealPercManuContrComm?: number, dealerBuyBack?: boolean, dealerCDTer?: number, dealerSIpercentage?: number,
                decorrenza?: string, derogaBlocchiServiziAssicurativi?: boolean, derogaCommerciale?: boolean, derogaContributo?: boolean,
                derogatedRateDeltaCommission?: number, derogatedSellerCommission?: number, distance?: number, fattura?: boolean,
                financedAmount?: number, financialProductCode?: string, flagShare?: boolean, franchised?: boolean, frequency?: number,
                idProposal?: string, importoAnticipo?: number, importoBalloonNetto?: number, importoFornitura?: number, importoListino?: number,
                importoRataAnticipoServizi?: number, importoRataBalloon?: number, importoRataBalloonServizi?: number, importoRataServizi?: number,
                incentivoVendita?: number, ipt?: number, kilometraggio?: number, leasing?: boolean, makeCode?: string, manuFixContrComm?: number,
                manuPercContrComm?: number, marca?: string, mfr?: number, mss?: number, mustUseSpIstr?: boolean, netCost?: number,
                operazioneK2K?: boolean, optionals?: Optional[], percentAtDealer?: number, percentAtSales?: number, percentualeDiListino?: number,
                prodottoFinanziario?: string, propEccedenza?: PropEccedenza, proposal?: SaveSimulationOfferProposal, proposalDate?: string, proposalStatus?: string,
                provImportoDeltaTasso?: number, provImportoDeltaTassoPerc?: number, provImportoServizi?: number, provImportoServiziPerc?: number,
                provImportoSpeseIstruttoria?: number, provImportoSpeseIstruttoriaPerc?: number, provImportoTotale?: number,
                provImportoVendita?: number, provImportoVenditaPerc?: number, rata?: number, rataNetta?: number, rataPercentuale?: number,
                rate?: CashFlow[], registrationDate?: string, rental?: boolean, richiestaFattura?: boolean, selFixComAmount?: number,
                selFixComPerc?: number, sellerCommission?: number, sellerSIpercentage?: number, serviceEntities?: SupplyService[],
                speseAnticipo?: number, speseDiIstruttoria?: number, speseRataCanone?: number, spread?: number, supplyServices?: SupplyService[],
                taeg?: number, tan?: number, tassoBase?: number, tipoInteresse?: string, tipoTasso?: string, totalAmount?: number,
                totalCreditAmount?: number, totalCustomerAmount?: number, used?: boolean, valoreCommissione?: number, vatCode?: string,
                vatPercentage?: number, version?: number, voucherCode?: string) {

        this._adminFeeShareOverride = adminFeeShareOverride;
        this._allestimenti = allestimenti;
        this._anticipoNetto = anticipoNetto;
        this._anticipoPercentuale = anticipoPercentuale;
        this._assetClassCode = assetClassCode;
        this._assetModel = assetModel;
        this._assetType = assetType;
        this._balloon = balloon;
        this._balloonPercentuale = balloonPercentuale;
        this._baumuster = baumuster;
        this._brandId = brandId;
        this._campaignCode = campaignCode;
        this._campiBloccati = campiBloccati;
        this._campiReadOnly = campiReadOnly;
        this._codAnticipo = codAnticipo;
        this._codCanoneRata = codCanoneRata;
        this._codeIpoteca = codeIpoteca;
        this._codiceGruppoDegrado = codiceGruppoDegrado;
        this._codiceTipoInteresse = codiceTipoInteresse;
        this._commCamp = commCamp;
        this._contractEnd = contractEnd;
        this._contractLength = contractLength;
        this._contributoCampagna = contributoCampagna;
        this._contributoConcessionario = contributoConcessionario;
        this._contributoRetention = contributoRetention;
        this._currentMileAge = currentMileAge;
        this._customerFullName = customerFullName;
        this._customerLastName = customerLastName;
        this._customerMailAddress = customerMailAddress;
        this._customerName = customerName;
        this._customerPhoneNumber = customerPhoneNumber;
        this._dealFixCom = dealFixCom;
        this._dealManuContrComm = dealManuContrComm;
        this._dealPercCom = dealPercCom;
        this._dealPercManuContrComm = dealPercManuContrComm;
        this._dealerBuyBack = dealerBuyBack;
        this._dealerCDTer = dealerCDTer;
        this._dealerSIpercentage = dealerSIpercentage;
        this._decorrenza = decorrenza;
        this._derogaBlocchiServiziAssicurativi = derogaBlocchiServiziAssicurativi;
        this._derogaCommerciale = derogaCommerciale;
        this._derogaContributo = derogaContributo;
        this._derogatedRateDeltaCommission = derogatedRateDeltaCommission;
        this._derogatedSellerCommission = derogatedSellerCommission;
        this._distance = distance;
        this._fattura = fattura;
        this._financedAmount = financedAmount;
        this._financialProductCode = financialProductCode;
        this._flagShare = flagShare;
        this._franchised = franchised;
        this._frequency = frequency;
        this._idProposal = idProposal;
        this._importoAnticipo = importoAnticipo;
        this._importoBalloonNetto = importoBalloonNetto;
        this._importoFornitura = importoFornitura;
        this._importoListino = importoListino;
        this._importoRataAnticipoServizi = importoRataAnticipoServizi;
        this._importoRataBalloon = importoRataBalloon;
        this._importoRataBalloonServizi = importoRataBalloonServizi;
        this._importoRataServizi = importoRataServizi;
        this._incentivoVendita = incentivoVendita;
        this._ipt = ipt;
        this._kilometraggio = kilometraggio;
        this._leasing = leasing;
        this._makeCode = makeCode;
        this._manuFixContrComm = manuFixContrComm;
        this._manuPercContrComm = manuPercContrComm;
        this._marca = marca;
        this._mfr = mfr;
        this._mss = mss;
        this._mustUseSpIstr = mustUseSpIstr;
        this._netCost = netCost;
        this._operazioneK2K = operazioneK2K;
        this._optionals = optionals;
        this._percentAtDealer = percentAtDealer;
        this._percentAtSales = percentAtSales;
        this._percentualeDiListino = percentualeDiListino;
        this._prodottoFinanziario = prodottoFinanziario;
        this._propEccedenza = propEccedenza;
        this._proposal = proposal;
        this._proposalDate = proposalDate;
        this._proposalStatus = proposalStatus;
        this._provImportoDeltaTasso = provImportoDeltaTasso;
        this._provImportoDeltaTassoPerc = provImportoDeltaTassoPerc;
        this._provImportoServizi = provImportoServizi;
        this._provImportoServiziPerc = provImportoServiziPerc;
        this._provImportoSpeseIstruttoria = provImportoSpeseIstruttoria;
        this._provImportoSpeseIstruttoriaPerc = provImportoSpeseIstruttoriaPerc;
        this._provImportoTotale = provImportoTotale;
        this._provImportoVendita = provImportoVendita;
        this._provImportoVenditaPerc = provImportoVenditaPerc;
        this._rata = rata;
        this._rataNetta = rataNetta;
        this._rataPercentuale = rataPercentuale;
        this._rate = rate;
        this._registrationDate = registrationDate;
        this._rental = rental;
        this._richiestaFattura = richiestaFattura;
        this._selFixComAmount = selFixComAmount;
        this._selFixComPerc = selFixComPerc;
        this._sellerCommission = sellerCommission;
        this._sellerSIpercentage = sellerSIpercentage;
        this._serviceEntities = serviceEntities;
        this._speseAnticipo = speseAnticipo;
        this._speseDiIstruttoria = speseDiIstruttoria;
        this._speseRataCanone = speseRataCanone;
        this._spread = spread;
        this._supplyServices = supplyServices;
        this._taeg = taeg;
        this._tan = tan;
        this._tassoBase = tassoBase;
        this._tipoInteresse = tipoInteresse;
        this._tipoTasso = tipoTasso;
        this._totalAmount = totalAmount;
        this._totalCreditAmount = totalCreditAmount;
        this._totalCustomerAmount = totalCustomerAmount;
        this._used = used;
        this._valoreCommissione = valoreCommissione;
        this._vatCode = vatCode;
        this._vatPercentage = vatPercentage;
        this._version = version;
        this._voucherCode = voucherCode;

    }


    get adminFeeShareOverride(): boolean {
        return this._adminFeeShareOverride;
    }

    set adminFeeShareOverride(value: boolean) {
        this._adminFeeShareOverride = value;
    }

    get allestimenti(): Preparation[] {
        return this._allestimenti;
    }

    set allestimenti(value: Preparation[]) {
        this._allestimenti = value;
    }

    get anticipoNetto(): number {
        return this._anticipoNetto;
    }

    set anticipoNetto(value: number) {
        this._anticipoNetto = value;
    }

    get anticipoPercentuale(): number {
        return this._anticipoPercentuale;
    }

    set anticipoPercentuale(value: number) {
        this._anticipoPercentuale = value;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get assetModel(): string {
        return this._assetModel;
    }

    set assetModel(value: string) {
        this._assetModel = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get balloon(): boolean {
        return this._balloon;
    }

    set balloon(value: boolean) {
        this._balloon = value;
    }

    get balloonPercentuale(): number {
        return this._balloonPercentuale;
    }

    set balloonPercentuale(value: number) {
        this._balloonPercentuale = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get campaignCode(): string {
        return this._campaignCode;
    }

    set campaignCode(value: string) {
        this._campaignCode = value;
    }

    get campiBloccati(): IBlockedFields {
        return this._campiBloccati;
    }

    set campiBloccati(value: IBlockedFields) {
        this._campiBloccati = value;
    }

    get campiReadOnly(): IBlockedFields {
        return this._campiReadOnly;
    }

    set campiReadOnly(value: IBlockedFields) {
        this._campiReadOnly = value;
    }

    get codAnticipo(): string {
        return this._codAnticipo;
    }

    set codAnticipo(value: string) {
        this._codAnticipo = value;
    }

    get codCanoneRata(): string {
        return this._codCanoneRata;
    }

    set codCanoneRata(value: string) {
        this._codCanoneRata = value;
    }

    get codeIpoteca(): string {
        return this._codeIpoteca;
    }

    set codeIpoteca(value: string) {
        this._codeIpoteca = value;
    }

    get codiceGruppoDegrado(): string {
        return this._codiceGruppoDegrado;
    }

    set codiceGruppoDegrado(value: string) {
        this._codiceGruppoDegrado = value;
    }

    get codiceTipoInteresse(): string {
        return this._codiceTipoInteresse;
    }

    set codiceTipoInteresse(value: string) {
        this._codiceTipoInteresse = value;
    }

    get commCamp(): boolean {
        return this._commCamp;
    }

    set commCamp(value: boolean) {
        this._commCamp = value;
    }

    get contractEnd(): string {
        return this._contractEnd;
    }

    set contractEnd(value: string) {
        this._contractEnd = value;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    get contributoCampagna(): number {
        return this._contributoCampagna;
    }

    set contributoCampagna(value: number) {
        this._contributoCampagna = value;
    }

    get contributoConcessionario(): number {
        return this._contributoConcessionario;
    }

    set contributoConcessionario(value: number) {
        this._contributoConcessionario = value;
    }

    get contributoRetention(): number {
        return this._contributoRetention;
    }

    set contributoRetention(value: number) {
        this._contributoRetention = value;
    }

    get currentMileAge(): number {
        return this._currentMileAge;
    }

    set currentMileAge(value: number) {
        this._currentMileAge = value;
    }

    get customerFullName(): string {
        return this._customerFullName;
    }

    set customerFullName(value: string) {
        this._customerFullName = value;
    }

    get customerLastName(): string {
        return this._customerLastName;
    }

    set customerLastName(value: string) {
        this._customerLastName = value;
    }

    get customerMailAddress(): string {
        return this._customerMailAddress;
    }

    set customerMailAddress(value: string) {
        this._customerMailAddress = value;
    }

    get customerName(): string {
        return this._customerName;
    }

    set customerName(value: string) {
        this._customerName = value;
    }

    get customerPhoneNumber(): string {
        return this._customerPhoneNumber;
    }

    set customerPhoneNumber(value: string) {
        this._customerPhoneNumber = value;
    }

    get dealFixCom(): number {
        return this._dealFixCom;
    }

    set dealFixCom(value: number) {
        this._dealFixCom = value;
    }

    get dealManuContrComm(): boolean {
        return this._dealManuContrComm;
    }

    set dealManuContrComm(value: boolean) {
        this._dealManuContrComm = value;
    }

    get dealPercCom(): number {
        return this._dealPercCom;
    }

    set dealPercCom(value: number) {
        this._dealPercCom = value;
    }

    get dealPercManuContrComm(): number {
        return this._dealPercManuContrComm;
    }

    set dealPercManuContrComm(value: number) {
        this._dealPercManuContrComm = value;
    }

    get dealerBuyBack(): boolean {
        return this._dealerBuyBack;
    }

    set dealerBuyBack(value: boolean) {
        this._dealerBuyBack = value;
    }

    get dealerCDTer(): number {
        return this._dealerCDTer;
    }

    set dealerCDTer(value: number) {
        this._dealerCDTer = value;
    }

    get dealerSIpercentage(): number {
        return this._dealerSIpercentage;
    }

    set dealerSIpercentage(value: number) {
        this._dealerSIpercentage = value;
    }

    get decorrenza(): string {
        return this._decorrenza;
    }

    set decorrenza(value: string) {
        this._decorrenza = value;
    }

    get derogaBlocchiServiziAssicurativi(): boolean {
        return this._derogaBlocchiServiziAssicurativi;
    }

    set derogaBlocchiServiziAssicurativi(value: boolean) {
        this._derogaBlocchiServiziAssicurativi = value;
    }

    get derogaCommerciale(): boolean {
        return this._derogaCommerciale;
    }

    set derogaCommerciale(value: boolean) {
        this._derogaCommerciale = value;
    }

    get derogaContributo(): boolean {
        return this._derogaContributo;
    }

    set derogaContributo(value: boolean) {
        this._derogaContributo = value;
    }

    get derogatedRateDeltaCommission(): number {
        return this._derogatedRateDeltaCommission;
    }

    set derogatedRateDeltaCommission(value: number) {
        this._derogatedRateDeltaCommission = value;
    }

    get derogatedSellerCommission(): number {
        return this._derogatedSellerCommission;
    }

    set derogatedSellerCommission(value: number) {
        this._derogatedSellerCommission = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get fattura(): boolean {
        return this._fattura;
    }

    set fattura(value: boolean) {
        this._fattura = value;
    }

    get financedAmount(): number {
        return this._financedAmount;
    }

    set financedAmount(value: number) {
        this._financedAmount = value;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get flagShare(): boolean {
        return this._flagShare;
    }

    set flagShare(value: boolean) {
        this._flagShare = value;
    }

    get franchised(): boolean {
        return this._franchised;
    }

    set franchised(value: boolean) {
        this._franchised = value;
    }

    get frequency(): number {
        return this._frequency;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    get idProposal(): string {
        return this._idProposal;
    }

    set idProposal(value: string) {
        this._idProposal = value;
    }

    get importoAnticipo(): number {
        return this._importoAnticipo;
    }

    set importoAnticipo(value: number) {
        this._importoAnticipo = value;
    }

    get importoBalloonNetto(): number {
        return this._importoBalloonNetto;
    }

    set importoBalloonNetto(value: number) {
        this._importoBalloonNetto = value;
    }

    get importoFornitura(): number {
        return this._importoFornitura;
    }

    set importoFornitura(value: number) {
        this._importoFornitura = value;
    }

    get importoListino(): number {
        return this._importoListino;
    }

    set importoListino(value: number) {
        this._importoListino = value;
    }

    get importoRataAnticipoServizi(): number {
        return this._importoRataAnticipoServizi;
    }

    set importoRataAnticipoServizi(value: number) {
        this._importoRataAnticipoServizi = value;
    }

    get importoRataBalloon(): number {
        return this._importoRataBalloon;
    }

    set importoRataBalloon(value: number) {
        this._importoRataBalloon = value;
    }

    get importoRataBalloonServizi(): number {
        return this._importoRataBalloonServizi;
    }

    set importoRataBalloonServizi(value: number) {
        this._importoRataBalloonServizi = value;
    }

    get importoRataServizi(): number {
        return this._importoRataServizi;
    }

    set importoRataServizi(value: number) {
        this._importoRataServizi = value;
    }

    get incentivoVendita(): number {
        return this._incentivoVendita;
    }

    set incentivoVendita(value: number) {
        this._incentivoVendita = value;
    }

    get ipt(): number {
        return this._ipt;
    }

    set ipt(value: number) {
        this._ipt = value;
    }

    get kilometraggio(): number {
        return this._kilometraggio;
    }

    set kilometraggio(value: number) {
        this._kilometraggio = value;
    }

    get leasing(): boolean {
        return this._leasing;
    }

    set leasing(value: boolean) {
        this._leasing = value;
    }

    get makeCode(): string {
        return this._makeCode;
    }

    set makeCode(value: string) {
        this._makeCode = value;
    }

    get manuFixContrComm(): number {
        return this._manuFixContrComm;
    }

    set manuFixContrComm(value: number) {
        this._manuFixContrComm = value;
    }

    get manuPercContrComm(): number {
        return this._manuPercContrComm;
    }

    set manuPercContrComm(value: number) {
        this._manuPercContrComm = value;
    }

    get marca(): string {
        return this._marca;
    }

    set marca(value: string) {
        this._marca = value;
    }

    get mfr(): number {
        return this._mfr;
    }

    set mfr(value: number) {
        this._mfr = value;
    }

    get mss(): number {
        return this._mss;
    }

    set mss(value: number) {
        this._mss = value;
    }

    get mustUseSpIstr(): boolean {
        return this._mustUseSpIstr;
    }

    set mustUseSpIstr(value: boolean) {
        this._mustUseSpIstr = value;
    }

    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    get operazioneK2K(): boolean {
        return this._operazioneK2K;
    }

    set operazioneK2K(value: boolean) {
        this._operazioneK2K = value;
    }

    get optionals(): Optional[] {
        return this._optionals;
    }

    set optionals(value: Optional[]) {
        this._optionals = value;
    }

    get percentAtDealer(): number {
        return this._percentAtDealer;
    }

    set percentAtDealer(value: number) {
        this._percentAtDealer = value;
    }

    get percentAtSales(): number {
        return this._percentAtSales;
    }

    set percentAtSales(value: number) {
        this._percentAtSales = value;
    }

    get percentualeDiListino(): number {
        return this._percentualeDiListino;
    }

    set percentualeDiListino(value: number) {
        this._percentualeDiListino = value;
    }

    get prodottoFinanziario(): string {
        return this._prodottoFinanziario;
    }

    set prodottoFinanziario(value: string) {
        this._prodottoFinanziario = value;
    }

    get propEccedenza(): PropEccedenza {
        return this._propEccedenza;
    }

    set propEccedenza(value: PropEccedenza) {
        this._propEccedenza = value;
    }

    get proposal(): SaveSimulationOfferProposal {
        return this._proposal;
    }

    set proposal(value: SaveSimulationOfferProposal) {
        this._proposal = value;
    }

    get proposalDate(): string {
        return this._proposalDate;
    }

    set proposalDate(value: string) {
        this._proposalDate = value;
    }

    get proposalStatus(): string {
        return this._proposalStatus;
    }

    set proposalStatus(value: string) {
        this._proposalStatus = value;
    }

    get provImportoDeltaTasso(): number {
        return this._provImportoDeltaTasso;
    }

    set provImportoDeltaTasso(value: number) {
        this._provImportoDeltaTasso = value;
    }

    get provImportoDeltaTassoPerc(): number {
        return this._provImportoDeltaTassoPerc;
    }

    set provImportoDeltaTassoPerc(value: number) {
        this._provImportoDeltaTassoPerc = value;
    }

    get provImportoServizi(): number {
        return this._provImportoServizi;
    }

    set provImportoServizi(value: number) {
        this._provImportoServizi = value;
    }

    get provImportoServiziPerc(): number {
        return this._provImportoServiziPerc;
    }

    set provImportoServiziPerc(value: number) {
        this._provImportoServiziPerc = value;
    }

    get provImportoSpeseIstruttoria(): number {
        return this._provImportoSpeseIstruttoria;
    }

    set provImportoSpeseIstruttoria(value: number) {
        this._provImportoSpeseIstruttoria = value;
    }

    get provImportoSpeseIstruttoriaPerc(): number {
        return this._provImportoSpeseIstruttoriaPerc;
    }

    set provImportoSpeseIstruttoriaPerc(value: number) {
        this._provImportoSpeseIstruttoriaPerc = value;
    }

    get provImportoTotale(): number {
        return this._provImportoTotale;
    }

    set provImportoTotale(value: number) {
        this._provImportoTotale = value;
    }

    get provImportoVendita(): number {
        return this._provImportoVendita;
    }

    set provImportoVendita(value: number) {
        this._provImportoVendita = value;
    }

    get provImportoVenditaPerc(): number {
        return this._provImportoVenditaPerc;
    }

    set provImportoVenditaPerc(value: number) {
        this._provImportoVenditaPerc = value;
    }

    get rata(): number {
        return this._rata;
    }

    set rata(value: number) {
        this._rata = value;
    }

    get rataNetta(): number {
        return this._rataNetta;
    }

    set rataNetta(value: number) {
        this._rataNetta = value;
    }

    get rataPercentuale(): number {
        return this._rataPercentuale;
    }

    set rataPercentuale(value: number) {
        this._rataPercentuale = value;
    }

    get rate(): CashFlow[] {
        return this._rate;
    }

    set rate(value: CashFlow[]) {
        this._rate = value;
    }

    get registrationDate(): string {
        return this._registrationDate;
    }

    set registrationDate(value: string) {
        this._registrationDate = value;
    }

    get rental(): boolean {
        return this._rental;
    }

    set rental(value: boolean) {
        this._rental = value;
    }

    get richiestaFattura(): boolean {
        return this._richiestaFattura;
    }

    set richiestaFattura(value: boolean) {
        this._richiestaFattura = value;
    }

    get selFixComAmount(): number {
        return this._selFixComAmount;
    }

    set selFixComAmount(value: number) {
        this._selFixComAmount = value;
    }

    get selFixComPerc(): number {
        return this._selFixComPerc;
    }

    set selFixComPerc(value: number) {
        this._selFixComPerc = value;
    }

    get sellerCommission(): number {
        return this._sellerCommission;
    }

    set sellerCommission(value: number) {
        this._sellerCommission = value;
    }

    get sellerSIpercentage(): number {
        return this._sellerSIpercentage;
    }

    set sellerSIpercentage(value: number) {
        this._sellerSIpercentage = value;
    }

    get serviceEntities(): SupplyService[] {
        return this._serviceEntities;
    }

    set serviceEntities(value: SupplyService[]) {
        this._serviceEntities = value;
    }

    get speseAnticipo(): number {
        return this._speseAnticipo;
    }

    set speseAnticipo(value: number) {
        this._speseAnticipo = value;
    }

    get speseDiIstruttoria(): number {
        return this._speseDiIstruttoria;
    }

    set speseDiIstruttoria(value: number) {
        this._speseDiIstruttoria = value;
    }

    get speseRataCanone(): number {
        return this._speseRataCanone;
    }

    set speseRataCanone(value: number) {
        this._speseRataCanone = value;
    }

    get spread(): number {
        return this._spread;
    }

    set spread(value: number) {
        this._spread = value;
    }

    get supplyServices(): SupplyService[] {
        return this._supplyServices;
    }

    set supplyServices(value: SupplyService[]) {
        this._supplyServices = value;
    }

    get taeg(): number {
        return this._taeg;
    }

    set taeg(value: number) {
        this._taeg = value;
    }

    get tan(): number {
        return this._tan;
    }

    set tan(value: number) {
        this._tan = value;
    }

    get tassoBase(): number {
        return this._tassoBase;
    }

    set tassoBase(value: number) {
        this._tassoBase = value;
    }

    get tipoInteresse(): string {
        return this._tipoInteresse;
    }

    set tipoInteresse(value: string) {
        this._tipoInteresse = value;
    }

    get tipoTasso(): string {
        return this._tipoTasso;
    }

    set tipoTasso(value: string) {
        this._tipoTasso = value;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    get totalCreditAmount(): number {
        return this._totalCreditAmount;
    }

    set totalCreditAmount(value: number) {
        this._totalCreditAmount = value;
    }

    get totalCustomerAmount(): number {
        return this._totalCustomerAmount;
    }

    set totalCustomerAmount(value: number) {
        this._totalCustomerAmount = value;
    }

    get used(): boolean {
        return this._used;
    }

    set used(value: boolean) {
        this._used = value;
    }

    get valoreCommissione(): number {
        return this._valoreCommissione;
    }

    set valoreCommissione(value: number) {
        this._valoreCommissione = value;
    }

    get vatCode(): string {
        return this._vatCode;
    }

    set vatCode(value: string) {
        this._vatCode = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    get version(): number {
        return this._version;
    }

    set version(value: number) {
        this._version = value;
    }

    get voucherCode(): string {
        return this._voucherCode;
    }

    set voucherCode(value: string) {
        this._voucherCode = value;
    }

    requestParameters(): Object {

        let params: Object;

        params = {
            'adminFeeShareOverride': this._adminFeeShareOverride,
            'allestimenti': PreparationFactory.getInstanceListParam(this._allestimenti),
            'anticipoNetto': this._anticipoNetto,
            'anticipoPercentuale': this._anticipoPercentuale,
            'assetClassCode': this._assetClassCode,
            'assetModel': this._assetModel,
            'assetType': this._assetType,
            'balloon': this._balloon,
            'balloonPercentuale': this._balloonPercentuale,
            'baumuster': this._baumuster,
            'brandId': this._brandId,
            'campaignCode': this._campaignCode,
            'campiBloccati': this._campiBloccati,
            'campiReadOnly': this._campiReadOnly,
            'codAnticipo': this._codAnticipo,
            'codCanoneRata': this._codCanoneRata,
            'codeIpoteca': this._codeIpoteca,
            'codiceGruppoDegrado': this._codiceGruppoDegrado,
            'codiceTipoInteresse': this._codiceTipoInteresse,
            'commCamp': this._commCamp,
            'contractEnd': this._contractEnd,
            'contractLength': this._contractLength,
            'contributoCampagna': this._contributoCampagna,
            'contributoConcessionario': this._contributoConcessionario,
            'contributoRetention': this._contributoRetention,
            'currentMileAge': this._currentMileAge,
            'customerFullName': this._customerFullName,
            'customerLastName': this._customerLastName,
            'customerMailAddress': this._customerMailAddress,
            'customerName': this._customerName,
            'customerPhoneNumber': this._customerPhoneNumber,
            'dealFixCom': this._dealFixCom,
            'dealManuContrComm': this._dealManuContrComm,
            'dealPercCom': this._dealPercCom,
            'dealPercManuContrComm': this._dealPercManuContrComm,
            'dealerBuyBack': this._dealerBuyBack,
            'dealerCDTer': this._dealerCDTer,
            'dealerSIpercentage': this._dealerSIpercentage,
            'decorrenza': this._decorrenza,
            'derogaBlocchiServiziAssicurativi': this._derogaBlocchiServiziAssicurativi,
            'derogaCommerciale': this._derogaCommerciale,
            'derogaContributo': this._derogaContributo,
            'derogatedRateDeltaCommission': this._derogatedRateDeltaCommission,
            'derogatedSellerCommission': this._derogatedSellerCommission,
            'distance': this._distance,
            'fattura': this._fattura,
            'financedAmount': this._financedAmount,
            'financialProductCode': this._financialProductCode,
            'flagShare': this._flagShare,
            'franchised': this._franchised,
            'frequency': this._frequency,
            'idProposal': this._idProposal,
            'importoAnticipo': this._importoAnticipo,
            'importoBalloonNetto': this._importoBalloonNetto,
            'importoFornitura': this._importoFornitura,
            'importoListino': this._importoListino,
            'importoRataAnticipoServizi': this._importoRataAnticipoServizi,
            'importoRataBalloon': this._importoRataBalloon,
            'importoRataBalloonServizi': this._importoRataBalloonServizi,
            'importoRataServizi': this._importoRataServizi,
            'incentivoVendita': this._incentivoVendita,
            'ipt': this._ipt,
            'kilometraggio': this._kilometraggio,
            'leasing': this._leasing,
            'makeCode': this._makeCode,
            'manuFixContrComm': this._manuFixContrComm,
            'manuPercContrComm': this._manuPercContrComm,
            'marca': this._marca,
            'mfr': this._mfr,
            'mss': this._mss,
            'mustUseSpIstr': this._mustUseSpIstr,
            'netCost': this._netCost,
            'operazioneK2K': this._operazioneK2K,
            'optionals': SaveDealerOfferOptionalFactory.getInstanceListParam(this._optionals),
            'percentAtDealer': this._percentAtDealer,
            'percentAtSales': this._percentAtSales,
            'percentualeDiListino': this._percentualeDiListino,
            'prodottoFinanziario': this._prodottoFinanziario,
            'propEccedenza': SaveDealerOfferPropEccedenzaFactory.getInstanceParams(this.propEccedenza),
            'proposal': SaveDealerOfferProposalFactory.getInstanceParams(this.proposal),
            'proposalDate': this._proposalDate,
            'proposalStatus': this._proposalStatus,
            'provImportoDeltaTasso': this._provImportoDeltaTasso,
            'provImportoDeltaTassoPerc': this._provImportoDeltaTassoPerc,
            'provImportoServizi': this._provImportoServizi,
            'provImportoServiziPerc': this._provImportoServiziPerc,
            'provImportoSpeseIstruttoria': this._provImportoSpeseIstruttoria,
            'provImportoSpeseIstruttoriaPerc': this._provImportoSpeseIstruttoriaPerc,
            'provImportoTotale': this._provImportoTotale,
            'provImportoVendita': this._provImportoVendita,
            'provImportoVenditaPerc': this._provImportoVenditaPerc,
            'rata': this._rata,
            'rataNetta': this._rataNetta,
            'rataPercentuale': this._rataPercentuale,
            'rate': SaveDealerOfferCashFlowFactory.getInstanceListParam(this._rate),
            'registrationDate': this._registrationDate,
            'rental': this._rental,
            'richiestaFattura': this._richiestaFattura,
            'selFixComAmount': this._selFixComAmount,
            'selFixComPerc': this._selFixComPerc,
            'sellerCommission': this._sellerCommission,
            'sellerSIpercentage': this._sellerSIpercentage,
            'serviceEntities': SaveDealerOfferSupplyServiceFactory.getInstanceListParam(this._serviceEntities),
            'speseAnticipo': this._speseAnticipo,
            'speseDiIstruttoria': this._speseDiIstruttoria,
            'speseRataCanone': this._speseRataCanone,
            'spread': this._spread,
            'supplyServices': SaveDealerOfferSupplyServiceFactory.getInstanceListParam(this._supplyServices),
            'taeg': this._taeg,
            'tan': this._tan,
            'tassoBase': this._tassoBase,
            'tipoInteresse': this._tipoInteresse,
            'tipoTasso': this._tipoTasso,
            'totalAmount': this._totalAmount,
            'totalCreditAmount': this._totalCreditAmount,
            'totalCustomerAmount': this._totalCustomerAmount,
            'used': this._used,
            'valoreCommissione': this._valoreCommissione,
            'vatCode': this._vatCode,
            'vatPercentage': this._vatPercentage,
            'version': this._version,
            'voucherCode': this._voucherCode
        };

        return params;
    }

}

export class BeSaveSimulationOfferReqParamsFactory {

    static getInstance(obj: BeSaveSimulationOfferReqParams): BeSaveSimulationOfferReqParams {

        let utils: UtilsService = new UtilsService();
        let simulationOffer = new BeSaveSimulationOfferReqParams();

        let allestimenti = PreparationFactory.getInstanceList(obj.allestimenti);
        let optionals = SaveDealerOfferOptionalFactory.getInstanceList(obj.optionals);
        let propEccedenza = SaveDealerOfferPropEccedenzaFactory.getInstance(obj.propEccedenza);
        let proposal = SaveDealerOfferProposalFactory.getInstance(obj.proposal);
        let cashFlowFactory = new SaveDealerOfferCashFlowFactory();
        let rate = cashFlowFactory.getInstanceList(obj.rate);
        let servicesFactory = new SaveDealerOfferSupplyServiceFactory();
        let serviceEntities = servicesFactory.getInstanceList(obj.serviceEntities);
        let supplyServices = servicesFactory.getInstanceList(obj.supplyServices);

        console.log('OBJ in factory -> ' + JSON.stringify(obj));
        console.log('campo used in obj -> ' + JSON.stringify(obj.used));

        if (obj) {
            if (!utils.isUndefided(obj.adminFeeShareOverride)) simulationOffer.adminFeeShareOverride = obj.adminFeeShareOverride;
            if (!utils.isUndefided(allestimenti)) simulationOffer.allestimenti = allestimenti;
            if (!utils.isUndefided(obj.allestimenti)) simulationOffer.allestimenti = obj.allestimenti;
            if (!utils.isUndefided(obj.anticipoPercentuale)) simulationOffer.anticipoPercentuale = obj.anticipoPercentuale;
            if (!utils.isUndefided(obj.assetClassCode)) simulationOffer.assetClassCode = obj.assetClassCode;
            if (!utils.isUndefided(obj.assetModel)) simulationOffer.assetModel = obj.assetModel;
            if (!utils.isUndefided(obj.assetType)) simulationOffer.assetType = obj.assetType;
            if (!utils.isUndefided(obj.balloon)) simulationOffer.balloon = obj.balloon;
            if (!utils.isUndefided(obj.balloonPercentuale)) simulationOffer.balloonPercentuale = obj.balloonPercentuale;
            if (!utils.isUndefided(obj.baumuster)) simulationOffer.baumuster = obj.baumuster;
            if (!utils.isUndefided(obj.brandId)) simulationOffer.brandId = obj.brandId;
            if (!utils.isUndefided(obj.campaignCode)) simulationOffer.campaignCode = obj.campaignCode;
            if (!utils.isUndefided(obj.campiBloccati)) simulationOffer.campiBloccati = obj.campiBloccati;
            if (!utils.isUndefided(obj.campiReadOnly)) simulationOffer.campiReadOnly = obj.campiReadOnly;
            if (!utils.isUndefided(obj.codAnticipo)) simulationOffer.codAnticipo = obj.codAnticipo;
            if (!utils.isUndefided(obj.codCanoneRata)) simulationOffer.codCanoneRata = obj.codCanoneRata;
            if (!utils.isUndefided(obj.codeIpoteca)) simulationOffer.codeIpoteca = obj.codeIpoteca;
            if (!utils.isUndefided(obj.codiceGruppoDegrado)) simulationOffer.codiceGruppoDegrado = obj.codiceGruppoDegrado;
            if (!utils.isUndefided(obj.codiceTipoInteresse)) simulationOffer.codiceTipoInteresse = obj.codiceTipoInteresse;
            if (!utils.isUndefided(obj.commCamp)) simulationOffer.commCamp = obj.commCamp;
            if (!utils.isUndefided(obj.contractEnd)) simulationOffer.contractEnd = obj.contractEnd;
            if (!utils.isUndefided(obj.contractLength)) simulationOffer.contractLength = obj.contractLength;
            if (!utils.isUndefided(obj.contributoCampagna)) simulationOffer.contributoCampagna = obj.contributoCampagna;
            if (!utils.isUndefided(obj.contributoConcessionario)) simulationOffer.contributoConcessionario = obj.contributoConcessionario;
            if (!utils.isUndefided(obj.contributoRetention)) simulationOffer.contributoRetention = obj.contributoRetention;
            if (!utils.isUndefided(obj.currentMileAge)) simulationOffer.currentMileAge = obj.currentMileAge;
            if (!utils.isUndefided(obj.customerFullName)) simulationOffer.customerFullName = obj.customerFullName;
            if (!utils.isUndefided(obj.customerLastName)) simulationOffer.customerLastName = obj.customerLastName;
            if (!utils.isUndefided(obj.customerMailAddress)) simulationOffer.customerMailAddress = obj.customerMailAddress;
            if (!utils.isUndefided(obj.customerName)) simulationOffer.customerName = obj.customerName;
            if (!utils.isUndefided(obj.customerPhoneNumber)) simulationOffer.customerPhoneNumber = obj.customerPhoneNumber;
            if (!utils.isUndefided(obj.dealFixCom)) simulationOffer.dealFixCom = obj.dealFixCom;
            if (!utils.isUndefided(obj.dealManuContrComm)) simulationOffer.dealManuContrComm = obj.dealManuContrComm;
            if (!utils.isUndefided(obj.dealPercCom)) simulationOffer.dealPercCom = obj.dealPercCom;
            if (!utils.isUndefided(obj.dealPercManuContrComm)) simulationOffer.dealPercManuContrComm = obj.dealPercManuContrComm;
            if (!utils.isUndefided(obj.dealerBuyBack)) simulationOffer.dealerBuyBack = obj.dealerBuyBack;
            if (!utils.isUndefided(obj.dealerCDTer)) simulationOffer.dealerCDTer = obj.dealerCDTer;
            if (!utils.isUndefided(obj.dealerSIpercentage)) simulationOffer.dealerSIpercentage = obj.dealerSIpercentage;
            if (!utils.isUndefided(obj.decorrenza)) simulationOffer.decorrenza = obj.decorrenza;
            if (!utils.isUndefided(obj.derogaBlocchiServiziAssicurativi)) simulationOffer.derogaBlocchiServiziAssicurativi = obj.derogaBlocchiServiziAssicurativi;
            if (!utils.isUndefided(obj.derogaCommerciale)) simulationOffer.derogaCommerciale = obj.derogaCommerciale;
            if (!utils.isUndefided(obj.derogaContributo)) simulationOffer.derogaContributo = obj.derogaContributo;
            if (!utils.isUndefided(obj.derogatedRateDeltaCommission)) simulationOffer.derogatedRateDeltaCommission = obj.derogatedRateDeltaCommission;
            if (!utils.isUndefided(obj.derogatedSellerCommission)) simulationOffer.derogatedSellerCommission = obj.derogatedSellerCommission;
            if (!utils.isUndefided(obj.distance)) simulationOffer.distance = obj.distance;
            if (!utils.isUndefided(obj.fattura)) simulationOffer.fattura = obj.fattura;
            if (!utils.isUndefided(obj.financedAmount)) simulationOffer.financedAmount = obj.financedAmount;
            if (!utils.isUndefided(obj.financialProductCode)) simulationOffer.financialProductCode = obj.financialProductCode;
            if (!utils.isUndefided(obj.flagShare)) simulationOffer.flagShare = obj.flagShare;
            if (!utils.isUndefided(obj.franchised)) simulationOffer.franchised = obj.franchised;
            if (!utils.isUndefided(obj.frequency)) simulationOffer.frequency = obj.frequency;
            if (!utils.isUndefided(obj.idProposal)) simulationOffer.idProposal = obj.idProposal;
            if (!utils.isUndefided(obj.importoAnticipo)) simulationOffer.importoAnticipo = obj.importoAnticipo;
            if (!utils.isUndefided(obj.importoBalloonNetto)) simulationOffer.importoBalloonNetto = obj.importoBalloonNetto;
            if (!utils.isUndefided(obj.importoFornitura)) simulationOffer.importoFornitura = obj.importoFornitura;
            if (!utils.isUndefided(obj.importoListino)) simulationOffer.importoListino = obj.importoListino;
            if (!utils.isUndefided(obj.importoRataAnticipoServizi)) simulationOffer.importoRataAnticipoServizi = obj.importoRataAnticipoServizi;
            if (!utils.isUndefided(obj.importoRataBalloon)) simulationOffer.importoRataBalloon = obj.importoRataBalloon;
            if (!utils.isUndefided(obj.importoRataBalloonServizi)) simulationOffer.importoRataBalloonServizi = obj.importoRataBalloonServizi;
            if (!utils.isUndefided(obj.importoRataServizi)) simulationOffer.importoRataServizi = obj.importoRataServizi;
            if (!utils.isUndefided(obj.incentivoVendita)) simulationOffer.incentivoVendita = obj.incentivoVendita;
            if (!utils.isUndefided(obj.ipt)) simulationOffer.ipt = obj.ipt;
            if (!utils.isUndefided(obj.kilometraggio)) simulationOffer.kilometraggio = obj.kilometraggio;
            if (!utils.isUndefided(obj.leasing)) simulationOffer.leasing = obj.leasing;
            if (!utils.isUndefided(obj.makeCode)) simulationOffer.makeCode = obj.makeCode;
            if (!utils.isUndefided(obj.manuFixContrComm)) simulationOffer.manuFixContrComm = obj.manuFixContrComm;
            if (!utils.isUndefided(obj.manuPercContrComm)) simulationOffer.manuPercContrComm = obj.manuPercContrComm;
            if (!utils.isUndefided(obj.marca)) simulationOffer.marca = obj.marca;
            if (!utils.isUndefided(obj.mfr)) simulationOffer.mfr = obj.mfr;
            if (!utils.isUndefided(obj.mss)) simulationOffer.mss = obj.mss;
            if (!utils.isUndefided(obj.mustUseSpIstr)) simulationOffer.mustUseSpIstr = obj.mustUseSpIstr;
            if (!utils.isUndefided(obj.netCost)) simulationOffer.netCost = obj.netCost;
            if (!utils.isUndefided(obj.operazioneK2K)) simulationOffer.operazioneK2K = obj.operazioneK2K;
            if (!utils.isUndefided(optionals)) simulationOffer.optionals = optionals;
            if (!utils.isUndefided(obj.percentAtDealer)) simulationOffer.percentAtDealer = obj.percentAtDealer;
            if (!utils.isUndefided(obj.percentAtSales)) simulationOffer.percentAtSales = obj.percentAtSales;
            if (!utils.isUndefided(obj.percentualeDiListino)) simulationOffer.percentualeDiListino = obj.percentualeDiListino;
            if (!utils.isUndefided(obj.prodottoFinanziario)) simulationOffer.prodottoFinanziario = obj.prodottoFinanziario;
            if (!utils.isUndefided(propEccedenza)) simulationOffer.propEccedenza = propEccedenza;
            if (!utils.isUndefided(proposal)) simulationOffer.proposal = proposal;
            if (!utils.isUndefided(obj.proposalDate)) simulationOffer.proposalDate = obj.proposalDate;
            if (!utils.isUndefided(obj.proposalStatus)) simulationOffer.proposalStatus = obj.proposalStatus;
            if (!utils.isUndefided(obj.provImportoDeltaTasso)) simulationOffer.provImportoDeltaTasso = obj.provImportoDeltaTasso;
            if (!utils.isUndefided(obj.provImportoDeltaTassoPerc)) simulationOffer.provImportoDeltaTassoPerc = obj.provImportoDeltaTassoPerc;
            if (!utils.isUndefided(obj.provImportoServizi)) simulationOffer.provImportoServizi = obj.provImportoServizi;
            if (!utils.isUndefided(obj.provImportoServiziPerc)) simulationOffer.provImportoServiziPerc = obj.provImportoServiziPerc;
            if (!utils.isUndefided(obj.provImportoSpeseIstruttoria)) simulationOffer.provImportoSpeseIstruttoria = obj.provImportoSpeseIstruttoria;
            if (!utils.isUndefided(obj.provImportoSpeseIstruttoriaPerc)) simulationOffer.provImportoSpeseIstruttoriaPerc = obj.provImportoSpeseIstruttoriaPerc;
            if (!utils.isUndefided(obj.provImportoTotale)) simulationOffer.provImportoTotale = obj.provImportoTotale;
            if (!utils.isUndefided(obj.provImportoVendita)) simulationOffer.provImportoVendita = obj.provImportoVendita;
            if (!utils.isUndefided(obj.provImportoVenditaPerc)) simulationOffer.provImportoVenditaPerc = obj.provImportoVenditaPerc;
            if (!utils.isUndefided(obj.rata)) simulationOffer.rata = obj.rata;
            if (!utils.isUndefided(obj.rataNetta)) simulationOffer.rataNetta = obj.rataNetta;
            if (!utils.isUndefided(obj.rataPercentuale)) simulationOffer.rataPercentuale = obj.rataPercentuale;
            if (!utils.isUndefided(rate)) simulationOffer.rate = rate;
            if (!utils.isUndefided(obj.registrationDate)) simulationOffer.registrationDate = obj.registrationDate;
            if (!utils.isUndefided(obj.rental)) simulationOffer.rental = obj.rental;
            if (!utils.isUndefided(obj.richiestaFattura)) simulationOffer.richiestaFattura = obj.richiestaFattura;
            if (!utils.isUndefided(obj.selFixComAmount)) simulationOffer.selFixComAmount = obj.selFixComAmount;
            if (!utils.isUndefided(obj.selFixComPerc)) simulationOffer.selFixComPerc = obj.selFixComPerc;
            if (!utils.isUndefided(obj.sellerCommission)) simulationOffer.sellerCommission = obj.sellerCommission;
            if (!utils.isUndefided(obj.sellerSIpercentage)) simulationOffer.sellerSIpercentage = obj.sellerSIpercentage;
            if (!utils.isUndefided(serviceEntities)) simulationOffer.serviceEntities = serviceEntities;
            if (!utils.isUndefided(obj.speseAnticipo)) simulationOffer.speseAnticipo = obj.speseAnticipo;
            if (!utils.isUndefided(obj.speseDiIstruttoria)) simulationOffer.speseDiIstruttoria = obj.speseDiIstruttoria;
            if (!utils.isUndefided(obj.speseRataCanone)) simulationOffer.speseRataCanone = obj.speseRataCanone;
            if (!utils.isUndefided(obj.spread)) simulationOffer.spread = obj.spread;
            if (!utils.isUndefided(supplyServices)) simulationOffer.supplyServices = supplyServices;
            if (!utils.isUndefided(obj.taeg)) simulationOffer.taeg = obj.taeg;
            if (!utils.isUndefided(obj.tan)) simulationOffer.tan = obj.tan;
            if (!utils.isUndefided(obj.tassoBase)) simulationOffer.tassoBase = obj.tassoBase;
            if (!utils.isUndefided(obj.tipoInteresse)) simulationOffer.tipoInteresse = obj.tipoInteresse;
            if (!utils.isUndefided(obj.tipoTasso)) simulationOffer.tipoTasso = obj.tipoTasso;
            if (!utils.isUndefided(obj.totalAmount)) simulationOffer.totalAmount = obj.totalAmount;
            if (!utils.isUndefided(obj.totalCreditAmount)) simulationOffer.totalCreditAmount = obj.totalCreditAmount;
            if (!utils.isUndefided(obj.totalCustomerAmount)) simulationOffer.totalCustomerAmount = obj.totalCustomerAmount;
            if (!utils.isUndefided(obj.used)) simulationOffer.used = obj.used;
            if (!utils.isUndefided(obj.valoreCommissione)) simulationOffer.valoreCommissione = obj.valoreCommissione;
            if (!utils.isUndefided(obj.vatCode)) simulationOffer.vatCode = obj.vatCode;
            if (!utils.isUndefided(obj.vatPercentage)) simulationOffer.vatPercentage = obj.vatPercentage;
            if (!utils.isUndefided(obj.version)) simulationOffer.version = obj.version;
            if (!utils.isUndefided(obj.voucherCode)) simulationOffer.voucherCode = obj.voucherCode;
        }

        return simulationOffer;
    }

}

