import {UtilsService} from '../../../shared/services/utils.service';
import {ISaveSimulationOfferOptionals} from './Optional';

export class PHEntGovern {

    private _childId: string;
    private _parentBpId: string;
    private _relationType: string;
    private _validFrom: string; // data
    private _validUntil: string; // data


    get childId(): string {
        return this._childId;
    }

    set childId(value: string) {
        this._childId = value;
    }

    get parentBpId(): string {
        return this._parentBpId;
    }

    set parentBpId(value: string) {
        this._parentBpId = value;
    }

    get relationType(): string {
        return this._relationType;
    }

    set relationType(value: string) {
        this._relationType = value;
    }

    get validFrom(): string {
        return this._validFrom;
    }

    set validFrom(value: string) {
        this._validFrom = value;
    }

    get validUntil(): string {
        return this._validUntil;
    }

    set validUntil(value: string) {
        this._validUntil = value;
    }

    public requestParameters(): IPHEntGovern {
        let requestBody: IPHEntGovern = {
            childId: this._childId,
            parentBpId: this._parentBpId,
            relationType: this._relationType,
            validFrom: this._validFrom,
            validUntil: this._validUntil
        };

        return requestBody;
    }

}

export class SaveDealerOfferPHEntGovernFactory {

    static getInstance(obj: PHEntGovern): PHEntGovern {

        let utils: UtilsService = new UtilsService();
        let ph = new PHEntGovern();

        if (obj) {
            if (!utils.isUndefided(obj.childId)) ph.childId = obj.childId;
            if (!utils.isUndefided(obj.parentBpId)) ph.parentBpId = obj.parentBpId;
            if (!utils.isUndefided(obj.relationType)) ph.relationType = obj.relationType;
            if (!utils.isUndefided(obj.validFrom)) ph.validFrom = obj.validFrom;
            if (!utils.isUndefided(obj.validUntil)) ph.validUntil = obj.validUntil;
        }

        return ph;
    }

    static getInstanceListParam(list: any): IPHEntGovern[] {
        let iPHEntGovernList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                iPHEntGovernList.push(this.getInstance(elem).requestParameters());
            });
        }
        return iPHEntGovernList;
    }

    getInstanceList(list: any): PHEntGovern[] {
        let phList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                phList.push(SaveDealerOfferPHEntGovernFactory.getInstance(elem));
            });
        }
        return phList;
    }


}

export interface IPHEntGovern {

    childId: string;
    parentBpId: string;
    relationType: string;
    validFrom: string;
    validUntil: string;

}
