import {UtilsService} from '../../../shared/services/utils.service';
import {AssetsSaveSimulationOffer, SaveDealerOfferAssetsFactory} from './AssetsSaveSimulationOffer';
import {ContractDet, SaveDealerOfferContractDetFactory} from './ContractDet';
import {SupplyService, SaveDealerOfferSupplyServiceFactory} from './SupplyService';
import {PropEccedenza} from './PropEccedenza';

export class CommercialProposalSaveSimulationOffer {

    private _assets: AssetsSaveSimulationOffer[];
    private _contract: ContractDet;
    private _modified: boolean;
    private _services: SupplyService[];

    get assets(): AssetsSaveSimulationOffer[] {
        return this._assets;
    }

    set assets(value: AssetsSaveSimulationOffer[]) {
        this._assets = value;
    }

    get contract(): ContractDet {
        return this._contract;
    }

    set contract(value: ContractDet) {
        this._contract = value;
    }

    get modified(): boolean {
        return this._modified;
    }

    set modified(value: boolean) {
        this._modified = value;
    }

    get services(): SupplyService[] {
        return this._services;
    }

    set services(value: SupplyService[]) {
        this._services = value;
    }
}

export class SaveDealerOfferCommPropFactory {

    static getInstance(obj: CommercialProposalSaveSimulationOffer): CommercialProposalSaveSimulationOffer {

        let assets = SaveDealerOfferAssetsFactory.getInstanceList(obj.assets);

        let contract = SaveDealerOfferContractDetFactory.getInstance(obj.contract);

        let servicesFactory = new SaveDealerOfferSupplyServiceFactory();
        let services = servicesFactory.getInstanceList(obj.services);

        let utils: UtilsService = new UtilsService();
        let prop = new CommercialProposalSaveSimulationOffer();

        if (obj) {
            if (!utils.isUndefided(assets)) prop.assets = assets;
            if (!utils.isUndefided(contract)) prop.contract = contract;
            if (!utils.isUndefided(obj.modified)) prop.modified = obj.modified;
            if (!utils.isUndefided(services)) prop.services = services;
        }

        return prop;
    }


    static getInstanceList(list: any): CommercialProposalSaveSimulationOffer[] {
        let commList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                commList.push(SaveDealerOfferCommPropFactory.getInstance(elem));
            });
        }
        return commList;
    }

    static getInstanceParams(obj: CommercialProposalSaveSimulationOffer): Object {

        let assets = SaveDealerOfferAssetsFactory.getInstanceListParam(obj.assets);
        let contract = SaveDealerOfferContractDetFactory.getInstanceParams(obj.contract);
        let services = SaveDealerOfferSupplyServiceFactory.getInstanceListParam(obj.services);


        let requestBody = {
            assets: assets,
            contract: contract,
            modified: obj.modified,
            services: services
        };

        return requestBody;
    }

}