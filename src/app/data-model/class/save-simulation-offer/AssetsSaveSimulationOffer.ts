import {UtilsService} from '../../../shared/services/utils.service';
import {ISaveSimulationOfferOptionals, Optional, SaveDealerOfferOptionalFactory} from './Optional';
import {IResidValueConfig, ResidValueConfig, SaveDealerOfferResidValueConfigFactory} from './ResidValueConfig';
import {ConfigurazioneRateFactory} from '../ConfigurazioneRate';
import {IFinancialOptionals} from '../FinancialOptionals';

export class AssetsSaveSimulationOffer {

    private _amountTrade: number;
    private _annualMileage: number;
    private _assetClassCode: string;
    private _assetType: string;
    private _assetTypeDesc: string;
    private _baumuster: string;
    private _beneStrumentale: string;
    private _bhp: number;
    private _brandId: string;
    private _capacityPerAxis: number;
    private _categoryCode: string;
    private _cdTer: number;
    private _chassisNumber: string;
    private _codSocieta: number;
    private _currentMileage: number;
    private _dealerRv: number;
    private _discountAmount: number;
    private _discountPercentage: number;
    private _engineCapacity: number;
    private _franchised: boolean;
    private _fuelType: string;
    private _imagePath: string;
    private _invoiceDate: string; // data
    private _invoiceNumber: string;
    private _ipt: number;
    private _kilowatt: number;
    private _listingPrice: number;
    private _makeCode: string;
    private _makeDesc: string;
    private _modelDescription: string;
    private _netCost: number;
    private _numberOfTyres: number;
    private _offerFilename: string;
    private _optionals: Optional[];
    private _orderNumber: number;
    private _plate: string;
    private _productType: string; // enum
    private _registrationDate: string; // data
    private _residualValueConfiguration: ResidValueConfig;
    private _rvEnabled: boolean;
    private _rvTypeCode: number;
    private _systemRv: number;
    private _used: boolean;
    private _vatCode: string;
    private _vatPercentage: number;
    private _visibleAsNew: string;


    get amountTrade(): number {
        return this._amountTrade;
    }

    set amountTrade(value: number) {
        this._amountTrade = value;
    }

    get annualMileage(): number {
        return this._annualMileage;
    }

    set annualMileage(value: number) {
        this._annualMileage = value;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get assetTypeDesc(): string {
        return this._assetTypeDesc;
    }

    set assetTypeDesc(value: string) {
        this._assetTypeDesc = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get beneStrumentale(): string {
        return this._beneStrumentale;
    }

    set beneStrumentale(value: string) {
        this._beneStrumentale = value;
    }

    get bhp(): number {
        return this._bhp;
    }

    set bhp(value: number) {
        this._bhp = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get capacityPerAxis(): number {
        return this._capacityPerAxis;
    }

    set capacityPerAxis(value: number) {
        this._capacityPerAxis = value;
    }

    get categoryCode(): string {
        return this._categoryCode;
    }

    set categoryCode(value: string) {
        this._categoryCode = value;
    }

    get cdTer(): number {
        return this._cdTer;
    }

    set cdTer(value: number) {
        this._cdTer = value;
    }

    get chassisNumber(): string {
        return this._chassisNumber;
    }

    set chassisNumber(value: string) {
        this._chassisNumber = value;
    }

    get codSocieta(): number {
        return this._codSocieta;
    }

    set codSocieta(value: number) {
        this._codSocieta = value;
    }

    get currentMileage(): number {
        return this._currentMileage;
    }

    set currentMileage(value: number) {
        this._currentMileage = value;
    }

    get dealerRv(): number {
        return this._dealerRv;
    }

    set dealerRv(value: number) {
        this._dealerRv = value;
    }

    get discountAmount(): number {
        return this._discountAmount;
    }

    set discountAmount(value: number) {
        this._discountAmount = value;
    }

    get discountPercentage(): number {
        return this._discountPercentage;
    }

    set discountPercentage(value: number) {
        this._discountPercentage = value;
    }

    get engineCapacity(): number {
        return this._engineCapacity;
    }

    set engineCapacity(value: number) {
        this._engineCapacity = value;
    }

    get franchised(): boolean {
        return this._franchised;
    }

    set franchised(value: boolean) {
        this._franchised = value;
    }

    get fuelType(): string {
        return this._fuelType;
    }

    set fuelType(value: string) {
        this._fuelType = value;
    }

    get imagePath(): string {
        return this._imagePath;
    }

    set imagePath(value: string) {
        this._imagePath = value;
    }

    get invoiceDate(): string {
        return this._invoiceDate;
    }

    set invoiceDate(value: string) {
        this._invoiceDate = value;
    }

    get invoiceNumber(): string {
        return this._invoiceNumber;
    }

    set invoiceNumber(value: string) {
        this._invoiceNumber = value;
    }

    get ipt(): number {
        return this._ipt;
    }

    set ipt(value: number) {
        this._ipt = value;
    }

    get kilowatt(): number {
        return this._kilowatt;
    }

    set kilowatt(value: number) {
        this._kilowatt = value;
    }

    get listingPrice(): number {
        return this._listingPrice;
    }

    set listingPrice(value: number) {
        this._listingPrice = value;
    }

    get makeCode(): string {
        return this._makeCode;
    }

    set makeCode(value: string) {
        this._makeCode = value;
    }

    get makeDesc(): string {
        return this._makeDesc;
    }

    set makeDesc(value: string) {
        this._makeDesc = value;
    }

    get modelDescription(): string {
        return this._modelDescription;
    }

    set modelDescription(value: string) {
        this._modelDescription = value;
    }

    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    get numberOfTyres(): number {
        return this._numberOfTyres;
    }

    set numberOfTyres(value: number) {
        this._numberOfTyres = value;
    }

    get offerFilename(): string {
        return this._offerFilename;
    }

    set offerFilename(value: string) {
        this._offerFilename = value;
    }

    get optionals(): Optional[] {
        return this._optionals;
    }

    set optionals(value: Optional[]) {
        this._optionals = value;
    }

    get orderNumber(): number {
        return this._orderNumber;
    }

    set orderNumber(value: number) {
        this._orderNumber = value;
    }

    get plate(): string {
        return this._plate;
    }

    set plate(value: string) {
        this._plate = value;
    }

    get productType(): string {
        return this._productType;
    }

    set productType(value: string) {
        this._productType = value;
    }

    get registrationDate(): string {
        return this._registrationDate;
    }

    set registrationDate(value: string) {
        this._registrationDate = value;
    }

    get residualValueConfiguration(): ResidValueConfig {
        return this._residualValueConfiguration;
    }

    set residualValueConfiguration(value: ResidValueConfig) {
        this._residualValueConfiguration = value;
    }

    get rvEnabled(): boolean {
        return this._rvEnabled;
    }

    set rvEnabled(value: boolean) {
        this._rvEnabled = value;
    }

    get rvTypeCode(): number {
        return this._rvTypeCode;
    }

    set rvTypeCode(value: number) {
        this._rvTypeCode = value;
    }

    get systemRv(): number {
        return this._systemRv;
    }

    set systemRv(value: number) {
        this._systemRv = value;
    }

    get used(): boolean {
        return this._used;
    }

    set used(value: boolean) {
        this._used = value;
    }

    get vatCode(): string {
        return this._vatCode;
    }

    set vatCode(value: string) {
        this._vatCode = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    get visibleAsNew(): string {
        return this._visibleAsNew;
    }

    set visibleAsNew(value: string) {
        this._visibleAsNew = value;
    }

    public requestParameters(): IAssetsSaveSimulationOffer {
        let requestBody: IAssetsSaveSimulationOffer = {
            amountTrade: this._amountTrade,
            annualMileage: this._annualMileage,
            assetClassCode: this._assetClassCode,
            assetType: this._assetType,
            assetTypeDesc: this._assetTypeDesc,
            baumuster: this._baumuster,
            beneStrumentale: this._beneStrumentale,
            bhp: this._bhp,
            brandId: this._brandId,
            capacityPerAxis: this._capacityPerAxis,
            categoryCode: this._categoryCode,
            cdTer: this._cdTer,
            chassisNumber: this._chassisNumber,
            codSocieta: this._codSocieta,
            currentMileage: this._currentMileage,
            dealerRv: this._dealerRv,
            discountAmount: this._discountAmount,
            discountPercentage: this._discountPercentage,
            engineCapacity: this._engineCapacity,
            franchised: this._franchised,
            fuelType: this._fuelType,
            imagePath: this._imagePath,
            invoiceDate: this._invoiceDate,
            invoiceNumber: this._invoiceNumber,
            ipt: this._ipt,
            kilowatt: this._kilowatt,
            listingPrice: this._listingPrice,
            makeCode: this._makeCode,
            makeDesc: this._makeDesc,
            modelDescription: this._modelDescription,
            netCost: this._netCost,
            numberOfTyres: this._numberOfTyres,
            offerFilename: this._offerFilename,
            optionals: SaveDealerOfferOptionalFactory.getInstanceListParam(this._optionals),
            orderNumber: this._orderNumber,
            plate: this._plate,
            productType: this._productType,
            registrationDate: this._registrationDate,
            residualValueConfiguration: SaveDealerOfferResidValueConfigFactory.getInstanceParams(this._residualValueConfiguration),
            rvEnabled: this._rvEnabled,
            rvTypeCode: this._rvTypeCode,
            systemRv: this._systemRv,
            used: this._used,
            vatCode: this._vatCode,
            vatPercentage: this._vatPercentage,
            visibleAsNew: this._visibleAsNew
        };

        return requestBody;
    }

}

export class SaveDealerOfferAssetsFactory {

    static getInstance(obj: AssetsSaveSimulationOffer): AssetsSaveSimulationOffer {

        let utils: UtilsService = new UtilsService();
        let assets = new AssetsSaveSimulationOffer();

        let optionals = SaveDealerOfferOptionalFactory.getInstanceList(obj.optionals);

        let resid = SaveDealerOfferResidValueConfigFactory.getInstance(obj.residualValueConfiguration);

        if (obj) {
            if (!utils.isUndefided(obj.amountTrade)) assets.amountTrade = obj.amountTrade;
            if (!utils.isUndefided(obj.annualMileage)) assets.annualMileage = obj.annualMileage;
            if (!utils.isUndefided(obj.assetClassCode)) assets.assetClassCode = obj.assetClassCode;
            if (!utils.isUndefided(obj.assetType)) assets.assetType = obj.assetType;
            if (!utils.isUndefided(obj.assetTypeDesc)) assets.assetTypeDesc = obj.assetTypeDesc;
            if (!utils.isUndefided(obj.baumuster)) assets.baumuster = obj.baumuster;
            if (!utils.isUndefided(obj.beneStrumentale)) assets.beneStrumentale = obj.beneStrumentale;
            if (!utils.isUndefided(obj.bhp)) assets.bhp = obj.bhp;
            if (!utils.isUndefided(obj.brandId)) assets.brandId = obj.brandId;
            if (!utils.isUndefided(obj.capacityPerAxis)) assets.capacityPerAxis = obj.capacityPerAxis;
            if (!utils.isUndefided(obj.categoryCode)) assets.categoryCode = obj.categoryCode;
            if (!utils.isUndefided(obj.cdTer)) assets.cdTer = obj.cdTer;
            if (!utils.isUndefided(obj.chassisNumber)) assets.chassisNumber = obj.chassisNumber;
            if (!utils.isUndefided(obj.codSocieta)) assets.codSocieta = obj.codSocieta;
            if (!utils.isUndefided(obj.currentMileage)) assets.currentMileage = obj.currentMileage;
            if (!utils.isUndefided(obj.dealerRv)) assets.dealerRv = obj.dealerRv;
            if (!utils.isUndefided(obj.discountAmount)) assets.discountAmount = obj.discountAmount;
            if (!utils.isUndefided(obj.discountPercentage)) assets.discountPercentage = obj.discountPercentage;
            if (!utils.isUndefided(obj.engineCapacity)) assets.engineCapacity = obj.engineCapacity;
            if (!utils.isUndefided(obj.franchised)) assets.franchised = obj.franchised;
            if (!utils.isUndefided(obj.fuelType)) assets.fuelType = obj.fuelType;
            if (!utils.isUndefided(obj.imagePath)) assets.imagePath = obj.imagePath;
            if (!utils.isUndefided(obj.invoiceDate)) assets.invoiceDate = obj.invoiceDate;
            if (!utils.isUndefided(obj.invoiceNumber)) assets.invoiceNumber = obj.invoiceNumber;
            if (!utils.isUndefided(obj.ipt)) assets.ipt = obj.ipt;
            if (!utils.isUndefided(obj.kilowatt)) assets.kilowatt = obj.kilowatt;
            if (!utils.isUndefided(obj.listingPrice)) assets.listingPrice = obj.listingPrice;
            if (!utils.isUndefided(obj.makeCode)) assets.makeCode = obj.makeCode;
            if (!utils.isUndefided(obj.makeDesc)) assets.makeDesc = obj.makeDesc;
            if (!utils.isUndefided(obj.modelDescription)) assets.modelDescription = obj.modelDescription;
            if (!utils.isUndefided(obj.netCost)) assets.netCost = obj.netCost;
            if (!utils.isUndefided(obj.numberOfTyres)) assets.numberOfTyres = obj.numberOfTyres;
            if (!utils.isUndefided(obj.offerFilename)) assets.offerFilename = obj.offerFilename;
            if (!utils.isUndefided(optionals)) assets.optionals = optionals;
            if (!utils.isUndefided(obj.orderNumber)) assets.orderNumber = obj.orderNumber;
            if (!utils.isUndefided(obj.plate)) assets.plate = obj.plate;
            if (!utils.isUndefided(obj.productType)) assets.productType = obj.productType;
            if (!utils.isUndefided(obj.registrationDate)) assets.registrationDate = obj.registrationDate;
            if (!utils.isUndefided(resid)) assets.residualValueConfiguration = resid;
            if (!utils.isUndefided(obj.rvEnabled)) assets.rvEnabled = obj.rvEnabled;
            if (!utils.isUndefided(obj.rvTypeCode)) assets.rvTypeCode = obj.rvTypeCode;
            if (!utils.isUndefided(obj.systemRv)) assets.systemRv = obj.systemRv;
            if (!utils.isUndefided(obj.used)) assets.used = obj.used;
            if (!utils.isUndefided(obj.vatCode)) assets.vatCode = obj.vatCode;
            if (!utils.isUndefided(obj.vatPercentage)) assets.vatPercentage = obj.vatPercentage;
            if (!utils.isUndefided(obj.visibleAsNew)) assets.visibleAsNew = obj.visibleAsNew;

        }

        return assets;
    }


    static getInstanceList(list: any): AssetsSaveSimulationOffer[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferAssetsFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

    static getInstanceListParam(list: any): IAssetsSaveSimulationOffer[] {
        let assetsSaveSimulationOfferList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                assetsSaveSimulationOfferList.push(this.getInstance(elem).requestParameters());
            });
        }
        return assetsSaveSimulationOfferList;
    }

}

export interface IAssetsSaveSimulationOffer {

    amountTrade: number;
    annualMileage: number;
    assetClassCode: string;
    assetType: string;
    assetTypeDesc: string;
    baumuster: string;
    beneStrumentale: string;
    bhp: number;
    brandId: string;
    capacityPerAxis: number;
    categoryCode: string;
    cdTer: number;
    chassisNumber: string;
    codSocieta: number;
    currentMileage: number;
    dealerRv: number;
    discountAmount: number;
    discountPercentage: number;
    engineCapacity: number;
    franchised: boolean;
    fuelType: string;
    imagePath: string;
    invoiceDate: string;
    invoiceNumber: string;
    ipt: number;
    kilowatt: number;
    listingPrice: number;
    makeCode: string;
    makeDesc: string;
    modelDescription: string;
    netCost: number;
    numberOfTyres: number;
    offerFilename: string;
    optionals: ISaveSimulationOfferOptionals[];
    orderNumber: number;
    plate: string;
    productType: string;
    registrationDate: string;
    residualValueConfiguration: IResidValueConfig;
    rvEnabled: boolean;
    rvTypeCode: number;
    systemRv: number;
    used: boolean;
    vatCode: string;
    vatPercentage: number;
    visibleAsNew: string;

}
