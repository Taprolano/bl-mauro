import {UtilsService} from '../../../shared/services/utils.service';
import {AssetsSaveSimulationOffer} from './AssetsSaveSimulationOffer';

export class ContractDet {

    private _amountTrade: number;
    private _assetAge: number;
    private _balloonAmount: number;
    private _contractEnd: string; // data
    private _contractNumber: string;
    private _contractStart: string; // data
    private _customerRas: string;
    private _feelNewSat: boolean;
    private _feelNewVat: boolean;
    private _feelStarSatCode: string;
    private _feelStarVat: boolean;
    private _feelSureSat: boolean;
    private _feelSureVat: boolean;
    private _manufactureDate: string;


    get amountTrade(): number {
        return this._amountTrade;
    }

    set amountTrade(value: number) {
        this._amountTrade = value;
    }

    get assetAge(): number {
        return this._assetAge;
    }

    set assetAge(value: number) {
        this._assetAge = value;
    }

    get balloonAmount(): number {
        return this._balloonAmount;
    }

    set balloonAmount(value: number) {
        this._balloonAmount = value;
    }

    get contractEnd(): string {
        return this._contractEnd;
    }

    set contractEnd(value: string) {
        this._contractEnd = value;
    }

    get contractNumber(): string {
        return this._contractNumber;
    }

    set contractNumber(value: string) {
        this._contractNumber = value;
    }

    get contractStart(): string {
        return this._contractStart;
    }

    set contractStart(value: string) {
        this._contractStart = value;
    }

    get customerRas(): string {
        return this._customerRas;
    }

    set customerRas(value: string) {
        this._customerRas = value;
    }

    get feelNewSat(): boolean {
        return this._feelNewSat;
    }

    set feelNewSat(value: boolean) {
        this._feelNewSat = value;
    }

    get feelNewVat(): boolean {
        return this._feelNewVat;
    }

    set feelNewVat(value: boolean) {
        this._feelNewVat = value;
    }

    get feelStarSatCode(): string {
        return this._feelStarSatCode;
    }

    set feelStarSatCode(value: string) {
        this._feelStarSatCode = value;
    }

    get feelStarVat(): boolean {
        return this._feelStarVat;
    }

    set feelStarVat(value: boolean) {
        this._feelStarVat = value;
    }

    get feelSureSat(): boolean {
        return this._feelSureSat;
    }

    set feelSureSat(value: boolean) {
        this._feelSureSat = value;
    }

    get feelSureVat(): boolean {
        return this._feelSureVat;
    }

    set feelSureVat(value: boolean) {
        this._feelSureVat = value;
    }

    get manufactureDate(): string {
        return this._manufactureDate;
    }

    set manufactureDate(value: string) {
        this._manufactureDate = value;
    }
}

export class SaveDealerOfferContractDetFactory {

    static getInstance(obj: ContractDet): ContractDet {

        let utils: UtilsService = new UtilsService();
        let det = new ContractDet();

        if (obj) {
            if (!utils.isUndefided(obj.amountTrade)) det.amountTrade = obj.amountTrade;
            if (!utils.isUndefided(obj.assetAge)) det.assetAge = obj.assetAge;
            if (!utils.isUndefided(obj.balloonAmount)) det.balloonAmount = obj.balloonAmount;
            if (!utils.isUndefided(obj.contractEnd)) det.contractEnd = obj.contractEnd;
            if (!utils.isUndefided(obj.contractNumber)) det.contractNumber = obj.contractNumber;
            if (!utils.isUndefided(obj.contractStart)) det.contractStart = obj.contractStart;
            if (!utils.isUndefided(obj.customerRas)) det.customerRas = obj.customerRas;
            if (!utils.isUndefided(obj.feelNewSat)) det.feelNewSat = obj.feelNewSat;
            if (!utils.isUndefided(obj.feelNewVat)) det.feelNewVat = obj.feelNewVat;
            if (!utils.isUndefided(obj.feelStarSatCode)) det.feelStarSatCode = obj.feelStarSatCode;
            if (!utils.isUndefided(obj.feelStarVat)) det.feelStarVat = obj.feelStarVat;
            if (!utils.isUndefided(obj.feelSureSat)) det.feelSureSat = obj.feelSureSat;
            if (!utils.isUndefided(obj.feelSureVat)) det.feelSureVat = obj.feelSureVat;
            if (!utils.isUndefided(obj.manufactureDate)) det.manufactureDate = obj.manufactureDate;
        }

        return det;
    }


    static getInstanceList(list: any): ContractDet[] {
        let contractList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                contractList.push(SaveDealerOfferContractDetFactory.getInstance(elem));
            });
        }
        return contractList;
    }

    static getInstanceParams(obj: ContractDet): Object {
        let resp = {
            amountTrade: obj.amountTrade,
            assetAge: obj.assetAge,
            balloonAmount: obj.balloonAmount,
            contractEnd: obj.contractEnd,
            contractNumber: obj.contractNumber,
            contractStart: obj.contractStart,
            customerRas: obj.customerRas,
            feelNewSat: obj.feelNewSat,
            feelNewVat: obj.feelNewVat,
            feelStarSatCode: obj.feelStarSatCode,
            feelStarVat: obj.feelStarVat,
            feelSureSat: obj.feelSureSat,
            feelSureVat: obj.feelSureVat,
            manufactureDate: obj.manufactureDate
        };

        return resp;
    }

}