import {UtilsService} from '../../../shared/services/utils.service';
import {ISaveSimulationOfferOptionals} from './Optional';

export class SbjDocument {

    private _agencyCode: string;
    private _countryCode: string;
    private _dataRilascio: string; // data
    private _dataScadenza: string; // data
    private _descrDocumento: string;
    private _idTipoDocumento: string;
    private _luogoRilascio: string;
    private _numDocumento: string;
    private _provincia: string;
    private _seqNum: number;


    get agencyCode(): string {
        return this._agencyCode;
    }

    set agencyCode(value: string) {
        this._agencyCode = value;
    }

    get countryCode(): string {
        return this._countryCode;
    }

    set countryCode(value: string) {
        this._countryCode = value;
    }

    get dataRilascio(): string {
        return this._dataRilascio;
    }

    set dataRilascio(value: string) {
        this._dataRilascio = value;
    }

    get dataScadenza(): string {
        return this._dataScadenza;
    }

    set dataScadenza(value: string) {
        this._dataScadenza = value;
    }

    get descrDocumento(): string {
        return this._descrDocumento;
    }

    set descrDocumento(value: string) {
        this._descrDocumento = value;
    }

    get idTipoDocumento(): string {
        return this._idTipoDocumento;
    }

    set idTipoDocumento(value: string) {
        this._idTipoDocumento = value;
    }

    get luogoRilascio(): string {
        return this._luogoRilascio;
    }

    set luogoRilascio(value: string) {
        this._luogoRilascio = value;
    }

    get numDocumento(): string {
        return this._numDocumento;
    }

    set numDocumento(value: string) {
        this._numDocumento = value;
    }

    get provincia(): string {
        return this._provincia;
    }

    set provincia(value: string) {
        this._provincia = value;
    }

    get seqNum(): number {
        return this._seqNum;
    }

    set seqNum(value: number) {
        this._seqNum = value;
    }

    public requestParameters(): ISbjDocument {
        let requestBody: ISbjDocument = {
            agencyCode: this._agencyCode,
            countryCode: this._countryCode,
            dataRilascio: this._dataRilascio,
            dataScadenza: this._dataScadenza,
            descrDocumento: this._descrDocumento,
            idTipoDocumento: this._idTipoDocumento,
            luogoRilascio: this._luogoRilascio,
            numDocumento: this._numDocumento,
            provincia: this._provincia,
            seqNum: this._seqNum
        };

        return requestBody;
    }

}

export class SaveDealerOfferSbjDocumentFactory {

    static getInstance(obj: SbjDocument): SbjDocument {

        let utils: UtilsService = new UtilsService();
        let document = new SbjDocument();

        if (obj) {
            if (!utils.isUndefided(obj.agencyCode)) document.agencyCode = obj.agencyCode;
            if (!utils.isUndefided(obj.countryCode)) document.countryCode = obj.countryCode;
            if (!utils.isUndefided(obj.dataRilascio)) document.dataRilascio = obj.dataRilascio;
            if (!utils.isUndefided(obj.dataScadenza)) document.dataScadenza = obj.dataScadenza;
            if (!utils.isUndefided(obj.descrDocumento)) document.descrDocumento = obj.descrDocumento;
            if (!utils.isUndefided(obj.idTipoDocumento)) document.idTipoDocumento = obj.idTipoDocumento;
            if (!utils.isUndefided(obj.luogoRilascio)) document.luogoRilascio = obj.luogoRilascio;
            if (!utils.isUndefided(obj.numDocumento)) document.numDocumento = obj.numDocumento;
            if (!utils.isUndefided(obj.provincia)) document.provincia = obj.provincia;
            if (!utils.isUndefided(obj.seqNum)) document.seqNum = obj.seqNum;
        }

        return document;
    }

    static getInstanceListParam(list: any): ISbjDocument[] {
        let sbjDocumentList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                sbjDocumentList.push(this.getInstance(elem).requestParameters());
            });
        }
        return sbjDocumentList;
    }

    getInstanceList(list: any): SbjDocument[] {
        let documentList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                documentList.push(SaveDealerOfferSbjDocumentFactory.getInstance(elem));
            });
        }
        return documentList;
    }

}

export interface ISbjDocument {

    agencyCode: string;
    countryCode: string;
    dataRilascio: string;
    dataScadenza: string;
    descrDocumento: string;
    idTipoDocumento: string;
    luogoRilascio: string;
    numDocumento: string;
    provincia: string;
    seqNum: number;

}
