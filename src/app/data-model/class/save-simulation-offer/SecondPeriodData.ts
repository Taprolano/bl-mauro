import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class SecondPeriodData {

    private _balloonAmount: number;
    private _firstPaymentDate: string; // data
    private _instalmentAmount: number;
    private _lastPaymentDate: string; // data
    private _length: number;
    private _paymentMethodFees: number;
    private _taeg: number;
    private _tan: number;


    get balloonAmount(): number {
        return this._balloonAmount;
    }

    set balloonAmount(value: number) {
        this._balloonAmount = value;
    }

    get firstPaymentDate(): string {
        return this._firstPaymentDate;
    }

    set firstPaymentDate(value: string) {
        this._firstPaymentDate = value;
    }

    get instalmentAmount(): number {
        return this._instalmentAmount;
    }

    set instalmentAmount(value: number) {
        this._instalmentAmount = value;
    }

    get lastPaymentDate(): string {
        return this._lastPaymentDate;
    }

    set lastPaymentDate(value: string) {
        this._lastPaymentDate = value;
    }

    get length(): number {
        return this._length;
    }

    set length(value: number) {
        this._length = value;
    }

    get paymentMethodFees(): number {
        return this._paymentMethodFees;
    }

    set paymentMethodFees(value: number) {
        this._paymentMethodFees = value;
    }

    get taeg(): number {
        return this._taeg;
    }

    set taeg(value: number) {
        this._taeg = value;
    }

    get tan(): number {
        return this._tan;
    }

    set tan(value: number) {
        this._tan = value;
    }
}

export class SaveDealerOfferSecondPeriodDataFactory {

    static getInstance(obj: SecondPeriodData): SecondPeriodData {

        let utils: UtilsService = new UtilsService();
        let data = new SecondPeriodData();

        if (obj) {
            if (!utils.isUndefided(obj.balloonAmount)) data.balloonAmount = obj.balloonAmount;
            if (!utils.isUndefided(obj.firstPaymentDate)) data.firstPaymentDate = obj.firstPaymentDate;
            if (!utils.isUndefided(obj.instalmentAmount)) data.instalmentAmount = obj.instalmentAmount;
            if (!utils.isUndefided(obj.lastPaymentDate)) data.lastPaymentDate = obj.lastPaymentDate;
            if (!utils.isUndefided(obj.length)) data.length = obj.length;
            if (!utils.isUndefided(obj.paymentMethodFees)) data.paymentMethodFees = obj.paymentMethodFees;
            if (!utils.isUndefided(obj.taeg)) data.taeg = obj.taeg;
            if (!utils.isUndefided(obj.tan)) data.tan = obj.tan;
        }

        return data;
    }

    static getInstanceParams(obj: SecondPeriodData): Object {
        let resp = {
            balloonAmount: obj.balloonAmount,
            firstPaymentDate: obj.firstPaymentDate,
            instalmentAmount: obj.instalmentAmount,
            lastPaymentDate: obj.lastPaymentDate,
            length: obj.length,
            paymentMethodFees: obj.paymentMethodFees,
            taeg: obj.taeg,
            tan: obj.tan
        };

        return resp;
    }

    getInstanceList(list: any): SecondPeriodData[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferSecondPeriodDataFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

}
