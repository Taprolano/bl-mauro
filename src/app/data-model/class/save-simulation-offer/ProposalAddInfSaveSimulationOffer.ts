import {UtilsService} from '../../../shared/services/utils.service';
import {FourEyes, SaveDealerOfferFourEyesFactory} from './FourEyes';
import {SaveDealerOfferSupplyServOptionFactory} from './SupplyServOption';
import {ContractDet} from './ContractDet';

export class ProposalAddInfSaveSimulationOffer {

    private _fourEyes: FourEyes;
    private _provisionalApproval: boolean;
    private _stockChassis: string;
    private _tipoCard: string;


    get fourEyes(): FourEyes {
        return this._fourEyes;
    }

    set fourEyes(value: FourEyes) {
        this._fourEyes = value;
    }

    get provisionalApproval(): boolean {
        return this._provisionalApproval;
    }

    set provisionalApproval(value: boolean) {
        this._provisionalApproval = value;
    }

    get stockChassis(): string {
        return this._stockChassis;
    }

    set stockChassis(value: string) {
        this._stockChassis = value;
    }

    get tipoCard(): string {
        return this._tipoCard;
    }

    set tipoCard(value: string) {
        this._tipoCard = value;
    }
}

export class SaveDealerOfferProposalAddInfFactory {

    static getInstance(obj: ProposalAddInfSaveSimulationOffer): ProposalAddInfSaveSimulationOffer {

        let fourEyes = SaveDealerOfferFourEyesFactory.getInstance(obj.fourEyes);

        let utils: UtilsService = new UtilsService();
        let addInfo = new ProposalAddInfSaveSimulationOffer();

        if (obj) {
            if (!utils.isUndefided(fourEyes)) addInfo.fourEyes = fourEyes;
            if (!utils.isUndefided(addInfo.provisionalApproval)) addInfo.provisionalApproval = addInfo.provisionalApproval;
            if (!utils.isUndefided(addInfo.stockChassis)) addInfo.stockChassis = addInfo.stockChassis;
            if (!utils.isUndefided(addInfo.tipoCard)) addInfo.tipoCard = addInfo.tipoCard;
        }

        return addInfo;
    }

    static getInstanceParams(obj: ProposalAddInfSaveSimulationOffer): Object {
        let resp = {
            fourEyes: SaveDealerOfferFourEyesFactory.getInstanceParams(obj.fourEyes),
            provisionalApproval: obj.provisionalApproval,
            stockChassis: obj.stockChassis,
            tipoCard: obj.tipoCard
        };

        return resp;
    }

    getInstanceList(list: any): ProposalAddInfSaveSimulationOffer[] {
        let addInfoList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                addInfoList.push(SaveDealerOfferProposalAddInfFactory.getInstance(elem));
            });
        }
        return addInfoList;
    }

}
