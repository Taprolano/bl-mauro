import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class Derogation {

    private _approvalid: string;
    private _authLevelCode: string;
    private _deroDocPath: string;
    private _mbfsiLoss: number;
    private _newdealercontribution: number;
    private _newpaymentexpenses: number;
    private _newratedeltacommission: number;
    private _newsellercommission: number;
    private _newstartupexpenses: number;
    private _olddealercontribution: number;
    private _oldpaymentexpenses: number;
    private _oldratedeltacommission: number;
    private _oldsellercommission: number;
    private _oldstartupexpenses: number;
    private _reminderdate: string; // data
    private _reminderid: string;
    private _requestingid: string;
    private _rorac: number;
    private _status: string;
    private _updateDate: string; // dateTime
    private _updateUser: string;


    get approvalid(): string {
        return this._approvalid;
    }

    set approvalid(value: string) {
        this._approvalid = value;
    }

    get authLevelCode(): string {
        return this._authLevelCode;
    }

    set authLevelCode(value: string) {
        this._authLevelCode = value;
    }

    get deroDocPath(): string {
        return this._deroDocPath;
    }

    set deroDocPath(value: string) {
        this._deroDocPath = value;
    }

    get mbfsiLoss(): number {
        return this._mbfsiLoss;
    }

    set mbfsiLoss(value: number) {
        this._mbfsiLoss = value;
    }

    get newdealercontribution(): number {
        return this._newdealercontribution;
    }

    set newdealercontribution(value: number) {
        this._newdealercontribution = value;
    }

    get newpaymentexpenses(): number {
        return this._newpaymentexpenses;
    }

    set newpaymentexpenses(value: number) {
        this._newpaymentexpenses = value;
    }

    get newratedeltacommission(): number {
        return this._newratedeltacommission;
    }

    set newratedeltacommission(value: number) {
        this._newratedeltacommission = value;
    }

    get newsellercommission(): number {
        return this._newsellercommission;
    }

    set newsellercommission(value: number) {
        this._newsellercommission = value;
    }

    get newstartupexpenses(): number {
        return this._newstartupexpenses;
    }

    set newstartupexpenses(value: number) {
        this._newstartupexpenses = value;
    }

    get olddealercontribution(): number {
        return this._olddealercontribution;
    }

    set olddealercontribution(value: number) {
        this._olddealercontribution = value;
    }

    get oldpaymentexpenses(): number {
        return this._oldpaymentexpenses;
    }

    set oldpaymentexpenses(value: number) {
        this._oldpaymentexpenses = value;
    }

    get oldratedeltacommission(): number {
        return this._oldratedeltacommission;
    }

    set oldratedeltacommission(value: number) {
        this._oldratedeltacommission = value;
    }

    get oldsellercommission(): number {
        return this._oldsellercommission;
    }

    set oldsellercommission(value: number) {
        this._oldsellercommission = value;
    }

    get oldstartupexpenses(): number {
        return this._oldstartupexpenses;
    }

    set oldstartupexpenses(value: number) {
        this._oldstartupexpenses = value;
    }

    get reminderdate(): string {
        return this._reminderdate;
    }

    set reminderdate(value: string) {
        this._reminderdate = value;
    }

    get reminderid(): string {
        return this._reminderid;
    }

    set reminderid(value: string) {
        this._reminderid = value;
    }

    get requestingid(): string {
        return this._requestingid;
    }

    set requestingid(value: string) {
        this._requestingid = value;
    }

    get rorac(): number {
        return this._rorac;
    }

    set rorac(value: number) {
        this._rorac = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get updateDate(): string {
        return this._updateDate;
    }

    set updateDate(value: string) {
        this._updateDate = value;
    }

    get updateUser(): string {
        return this._updateUser;
    }

    set updateUser(value: string) {
        this._updateUser = value;
    }
}

export class SaveDealerOfferDerogationFactory {

    static getInstance(obj: Derogation): Derogation {

        let utils: UtilsService = new UtilsService();
        let derogation = new Derogation();

        if (obj) {
            if (!utils.isUndefided(obj.approvalid)) derogation.approvalid = obj.approvalid;
            if (!utils.isUndefided(obj.authLevelCode)) derogation.authLevelCode = obj.authLevelCode;
            if (!utils.isUndefided(obj.deroDocPath)) derogation.deroDocPath = obj.deroDocPath;
            if (!utils.isUndefided(obj.mbfsiLoss)) derogation.mbfsiLoss = obj.mbfsiLoss;
            if (!utils.isUndefided(obj.newdealercontribution)) derogation.newdealercontribution = obj.newdealercontribution;
            if (!utils.isUndefided(obj.newpaymentexpenses)) derogation.newpaymentexpenses = obj.newpaymentexpenses;
            if (!utils.isUndefided(obj.newratedeltacommission)) derogation.newratedeltacommission = obj.newratedeltacommission;
            if (!utils.isUndefided(obj.newsellercommission)) derogation.newsellercommission = obj.newsellercommission;
            if (!utils.isUndefided(obj.newstartupexpenses)) derogation.newstartupexpenses = obj.newstartupexpenses;
            if (!utils.isUndefided(obj.olddealercontribution)) derogation.olddealercontribution = obj.olddealercontribution;
            if (!utils.isUndefided(obj.oldpaymentexpenses)) derogation.oldpaymentexpenses = obj.oldpaymentexpenses;
            if (!utils.isUndefided(obj.oldratedeltacommission)) derogation.oldratedeltacommission = obj.oldratedeltacommission;
            if (!utils.isUndefided(obj.oldsellercommission)) derogation.oldsellercommission = obj.oldsellercommission;
            if (!utils.isUndefided(obj.oldstartupexpenses)) derogation.oldstartupexpenses = obj.oldstartupexpenses;
            if (!utils.isUndefided(obj.reminderdate)) derogation.reminderdate = obj.reminderdate;
            if (!utils.isUndefided(obj.reminderid)) derogation.reminderid = obj.reminderid;
            if (!utils.isUndefided(obj.requestingid)) derogation.requestingid = obj.requestingid;
            if (!utils.isUndefided(obj.rorac)) derogation.rorac = obj.rorac;
            if (!utils.isUndefided(obj.status)) derogation.status = obj.status;
            if (!utils.isUndefided(obj.updateDate)) derogation.updateDate = obj.updateDate;
            if (!utils.isUndefided(obj.updateUser)) derogation.updateUser = obj.updateUser;

        }
        return derogation;
    }

    static getInstanceParams(obj: Derogation): Object {
        let resp = {
            approvalid: obj.approvalid,
            authLevelCode: obj.authLevelCode,
            deroDocPath: obj.deroDocPath,
            mbfsiLoss: obj.mbfsiLoss,
            newdealercontribution: obj.newdealercontribution,
            newpaymentexpenses: obj.newpaymentexpenses,
            newratedeltacommission: obj.newratedeltacommission,
            newsellercommission: obj.newsellercommission,
            newstartupexpenses: obj.newstartupexpenses,
            olddealercontribution: obj.olddealercontribution,
            oldpaymentexpenses: obj.oldpaymentexpenses,
            oldratedeltacommission: obj.oldratedeltacommission,
            oldsellercommission: obj.oldsellercommission,
            oldstartupexpenses: obj.oldstartupexpenses,
            reminderdate: obj.reminderdate,
            reminderid: obj.reminderid,
            requestingid: obj.requestingid,
            rorac: obj.rorac,
            status: obj.status,
            updateDate: obj.updateDate,
            updateUser: obj.updateUser
        };

        return resp;
    }

    getInstanceList(list: any): Derogation[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferDerogationFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

}
