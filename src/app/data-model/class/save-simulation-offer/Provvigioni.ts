import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class Provvigioni {

    private _dealerProvImportoDeltaTasso: number;
    private _dealerProvImportoDeltaTassoPerc: number;
    private _dealerProvImportoServizi: number;
    private _dealerProvImportoServiziPerc: number;
    private _dealerProvImportoSpeseIstruttoria: number;
    private _dealerProvImportoSpeseIstruttoriaPerc: number;
    private _dealerProvImportoTotale: number;
    private _dealerProvImportoVendita: number;
    private _dealerProvImportoVenditaPerc: number;
    private _provImportoDeltaTasso: number;
    private _provImportoDeltaTassoPerc: number;
    private _provImportoServizi: number;
    private _provImportoServiziPerc: number;
    private _provImportoSpeseIstruttoria: number;
    private _provImportoSpeseIstruttoriaPerc: number;
    private _provImportoTotale: number;
    private _provImportoVendita: number;
    private _provImportoVenditaPerc: number;


    get dealerProvImportoDeltaTasso(): number {
        return this._dealerProvImportoDeltaTasso;
    }

    set dealerProvImportoDeltaTasso(value: number) {
        this._dealerProvImportoDeltaTasso = value;
    }

    get dealerProvImportoDeltaTassoPerc(): number {
        return this._dealerProvImportoDeltaTassoPerc;
    }

    set dealerProvImportoDeltaTassoPerc(value: number) {
        this._dealerProvImportoDeltaTassoPerc = value;
    }

    get dealerProvImportoServizi(): number {
        return this._dealerProvImportoServizi;
    }

    set dealerProvImportoServizi(value: number) {
        this._dealerProvImportoServizi = value;
    }

    get dealerProvImportoServiziPerc(): number {
        return this._dealerProvImportoServiziPerc;
    }

    set dealerProvImportoServiziPerc(value: number) {
        this._dealerProvImportoServiziPerc = value;
    }

    get dealerProvImportoSpeseIstruttoria(): number {
        return this._dealerProvImportoSpeseIstruttoria;
    }

    set dealerProvImportoSpeseIstruttoria(value: number) {
        this._dealerProvImportoSpeseIstruttoria = value;
    }

    get dealerProvImportoSpeseIstruttoriaPerc(): number {
        return this._dealerProvImportoSpeseIstruttoriaPerc;
    }

    set dealerProvImportoSpeseIstruttoriaPerc(value: number) {
        this._dealerProvImportoSpeseIstruttoriaPerc = value;
    }

    get dealerProvImportoTotale(): number {
        return this._dealerProvImportoTotale;
    }

    set dealerProvImportoTotale(value: number) {
        this._dealerProvImportoTotale = value;
    }

    get dealerProvImportoVendita(): number {
        return this._dealerProvImportoVendita;
    }

    set dealerProvImportoVendita(value: number) {
        this._dealerProvImportoVendita = value;
    }

    get dealerProvImportoVenditaPerc(): number {
        return this._dealerProvImportoVenditaPerc;
    }

    set dealerProvImportoVenditaPerc(value: number) {
        this._dealerProvImportoVenditaPerc = value;
    }

    get provImportoDeltaTasso(): number {
        return this._provImportoDeltaTasso;
    }

    set provImportoDeltaTasso(value: number) {
        this._provImportoDeltaTasso = value;
    }

    get provImportoDeltaTassoPerc(): number {
        return this._provImportoDeltaTassoPerc;
    }

    set provImportoDeltaTassoPerc(value: number) {
        this._provImportoDeltaTassoPerc = value;
    }

    get provImportoServizi(): number {
        return this._provImportoServizi;
    }

    set provImportoServizi(value: number) {
        this._provImportoServizi = value;
    }

    get provImportoServiziPerc(): number {
        return this._provImportoServiziPerc;
    }

    set provImportoServiziPerc(value: number) {
        this._provImportoServiziPerc = value;
    }

    get provImportoSpeseIstruttoria(): number {
        return this._provImportoSpeseIstruttoria;
    }

    set provImportoSpeseIstruttoria(value: number) {
        this._provImportoSpeseIstruttoria = value;
    }

    get provImportoSpeseIstruttoriaPerc(): number {
        return this._provImportoSpeseIstruttoriaPerc;
    }

    set provImportoSpeseIstruttoriaPerc(value: number) {
        this._provImportoSpeseIstruttoriaPerc = value;
    }

    get provImportoTotale(): number {
        return this._provImportoTotale;
    }

    set provImportoTotale(value: number) {
        this._provImportoTotale = value;
    }

    get provImportoVendita(): number {
        return this._provImportoVendita;
    }

    set provImportoVendita(value: number) {
        this._provImportoVendita = value;
    }

    get provImportoVenditaPerc(): number {
        return this._provImportoVenditaPerc;
    }

    set provImportoVenditaPerc(value: number) {
        this._provImportoVenditaPerc = value;
    }
}

export class SaveDealerOfferProvvigioniFactory {

    static getInstance(obj: Provvigioni): Provvigioni {

        let utils: UtilsService = new UtilsService();
        let provvigioni = new Provvigioni();

        if (obj) {
            if (!utils.isUndefided(obj.dealerProvImportoDeltaTasso)) provvigioni.dealerProvImportoDeltaTasso = obj.dealerProvImportoDeltaTasso;
            if (!utils.isUndefided(obj.dealerProvImportoDeltaTassoPerc)) provvigioni.dealerProvImportoDeltaTassoPerc = obj.dealerProvImportoDeltaTassoPerc;
            if (!utils.isUndefided(obj.dealerProvImportoServizi)) provvigioni.dealerProvImportoServizi = obj.dealerProvImportoServizi;
            if (!utils.isUndefided(obj.dealerProvImportoServiziPerc)) provvigioni.dealerProvImportoServiziPerc = obj.dealerProvImportoServiziPerc;
            if (!utils.isUndefided(obj.dealerProvImportoSpeseIstruttoria)) provvigioni.dealerProvImportoSpeseIstruttoria = obj.dealerProvImportoSpeseIstruttoria;
            if (!utils.isUndefided(obj.dealerProvImportoSpeseIstruttoriaPerc)) provvigioni.dealerProvImportoSpeseIstruttoriaPerc = obj.dealerProvImportoSpeseIstruttoriaPerc;
            if (!utils.isUndefided(obj.dealerProvImportoTotale)) provvigioni.dealerProvImportoTotale = obj.dealerProvImportoTotale;
            if (!utils.isUndefided(obj.dealerProvImportoVendita)) provvigioni.dealerProvImportoVendita = obj.dealerProvImportoVendita;
            if (!utils.isUndefided(obj.dealerProvImportoVenditaPerc)) provvigioni.dealerProvImportoVenditaPerc = obj.dealerProvImportoVenditaPerc;
            if (!utils.isUndefided(obj.provImportoDeltaTasso)) provvigioni.provImportoDeltaTasso = obj.provImportoDeltaTasso;
            if (!utils.isUndefided(obj.provImportoDeltaTassoPerc)) provvigioni.provImportoDeltaTassoPerc = obj.provImportoDeltaTassoPerc;
            if (!utils.isUndefided(obj.provImportoServizi)) provvigioni.provImportoServizi = obj.provImportoServizi;
            if (!utils.isUndefided(obj.provImportoServiziPerc)) provvigioni.provImportoServiziPerc = obj.provImportoServiziPerc;
            if (!utils.isUndefided(obj.provImportoSpeseIstruttoria)) provvigioni.provImportoSpeseIstruttoria = obj.provImportoSpeseIstruttoria;
            if (!utils.isUndefided(obj.provImportoSpeseIstruttoriaPerc)) provvigioni.provImportoSpeseIstruttoriaPerc = obj.provImportoSpeseIstruttoriaPerc;
            if (!utils.isUndefided(obj.provImportoTotale)) provvigioni.provImportoTotale = obj.provImportoTotale;
            if (!utils.isUndefided(obj.provImportoVendita)) provvigioni.provImportoVendita = obj.provImportoVendita;
            if (!utils.isUndefided(obj.provImportoVenditaPerc)) provvigioni.provImportoVenditaPerc = obj.provImportoVenditaPerc;
        }

        return provvigioni;
    }

    static getInstanceParams(obj: Provvigioni): Object {
        let resp = {
            dealerProvImportoDeltaTasso: obj.dealerProvImportoDeltaTasso,
            dealerProvImportoDeltaTassoPerc: obj.dealerProvImportoDeltaTassoPerc,
            dealerProvImportoServizi: obj.dealerProvImportoServizi,
            dealerProvImportoServiziPerc: obj.dealerProvImportoServiziPerc,
            dealerProvImportoSpeseIstruttoria: obj.dealerProvImportoSpeseIstruttoria,
            dealerProvImportoSpeseIstruttoriaPerc: obj.dealerProvImportoSpeseIstruttoriaPerc,
            dealerProvImportoTotale: obj.dealerProvImportoTotale,
            dealerProvImportoVendita: obj.dealerProvImportoVendita,
            dealerProvImportoVenditaPerc: obj.dealerProvImportoVenditaPerc,
            provImportoDeltaTasso: obj.provImportoDeltaTasso,
            provImportoDeltaTassoPerc: obj.provImportoDeltaTassoPerc,
            provImportoServizi: obj.provImportoServizi,
            provImportoServiziPerc: obj.provImportoServiziPerc,
            provImportoSpeseIstruttoria: obj.provImportoSpeseIstruttoria,
            provImportoSpeseIstruttoriaPerc: obj.provImportoSpeseIstruttoriaPerc,
            provImportoTotale: obj.provImportoTotale,
            provImportoVendita: obj.provImportoVendita,
            provImportoVenditaPerc: obj.provImportoVenditaPerc
        };

        return resp;
    }


    getInstanceList(list: any): Provvigioni[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferProvvigioniFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

}
