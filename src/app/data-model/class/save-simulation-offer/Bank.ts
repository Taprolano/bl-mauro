import {UtilsService} from '../../../shared/services/utils.service';
import {Address, IAddress, SaveDealerOfferAddressFactory} from './Address';
import {ISaveSimulationOfferOptionals} from './Optional';
import {ContractDet} from './ContractDet';

export class Bank {

    private _abi: string;
    private _accountHolderName: string;
    private _address: Address;
    private _cab: string;
    private _cap: string;
    private _ccbn: string;
    private _codSportello: string;
    private _ddmActivationCode: string;
    private _ddmStatusCode: string;
    private _descrAgenzia: string;
    private _descrBanca: string;
    private _iban: string;
    private _lastDdmRef: string;
    private _localita: string;
    private _numero: string;
    private _preferredBankAccount: boolean;
    private _progressivo: number;
    private _provincia: string;
    private _sbjCounter: number;


    get abi(): string {
        return this._abi;
    }

    set abi(value: string) {
        this._abi = value;
    }

    get accountHolderName(): string {
        return this._accountHolderName;
    }

    set accountHolderName(value: string) {
        this._accountHolderName = value;
    }

    get address(): Address {
        return this._address;
    }

    set address(value: Address) {
        this._address = value;
    }

    get cab(): string {
        return this._cab;
    }

    set cab(value: string) {
        this._cab = value;
    }

    get cap(): string {
        return this._cap;
    }

    set cap(value: string) {
        this._cap = value;
    }

    get ccbn(): string {
        return this._ccbn;
    }

    set ccbn(value: string) {
        this._ccbn = value;
    }

    get codSportello(): string {
        return this._codSportello;
    }

    set codSportello(value: string) {
        this._codSportello = value;
    }

    get ddmActivationCode(): string {
        return this._ddmActivationCode;
    }

    set ddmActivationCode(value: string) {
        this._ddmActivationCode = value;
    }

    get ddmStatusCode(): string {
        return this._ddmStatusCode;
    }

    set ddmStatusCode(value: string) {
        this._ddmStatusCode = value;
    }

    get descrAgenzia(): string {
        return this._descrAgenzia;
    }

    set descrAgenzia(value: string) {
        this._descrAgenzia = value;
    }

    get descrBanca(): string {
        return this._descrBanca;
    }

    set descrBanca(value: string) {
        this._descrBanca = value;
    }

    get iban(): string {
        return this._iban;
    }

    set iban(value: string) {
        this._iban = value;
    }

    get lastDdmRef(): string {
        return this._lastDdmRef;
    }

    set lastDdmRef(value: string) {
        this._lastDdmRef = value;
    }

    get localita(): string {
        return this._localita;
    }

    set localita(value: string) {
        this._localita = value;
    }

    get numero(): string {
        return this._numero;
    }

    set numero(value: string) {
        this._numero = value;
    }

    get preferredBankAccount(): boolean {
        return this._preferredBankAccount;
    }

    set preferredBankAccount(value: boolean) {
        this._preferredBankAccount = value;
    }

    get progressivo(): number {
        return this._progressivo;
    }

    set progressivo(value: number) {
        this._progressivo = value;
    }

    get provincia(): string {
        return this._provincia;
    }

    set provincia(value: string) {
        this._provincia = value;
    }

    get sbjCounter(): number {
        return this._sbjCounter;
    }

    set sbjCounter(value: number) {
        this._sbjCounter = value;
    }

    public requestParameters(): IBank {
        let requestBody: IBank = {
            abi: this._abi,
            accountHolderName: this._accountHolderName,
            address: SaveDealerOfferAddressFactory.getInstanceParams(this._address),
            cab: this._cab,
            cap: this._cap,
            ccbn: this._ccbn,
            codSportello: this._codSportello,
            ddmActivationCode: this._ddmActivationCode,
            ddmStatusCode: this._ddmStatusCode,
            descrAgenzia: this._descrAgenzia,
            descrBanca: this._descrBanca,
            iban: this._iban,
            lastDdmRef: this._lastDdmRef,
            localita: this._localita,
            numero: this._numero,
            preferredBankAccount: this._preferredBankAccount,
            progressivo: this._progressivo,
            provincia: this._provincia,
            sbjCounter: this._sbjCounter
        };

        return requestBody;
    }

}

export class SaveDealerOfferBankFactory {

    static getInstance(obj: Bank): Bank {

        let utils: UtilsService = new UtilsService();
        let bank = new Bank();

        if (obj) {

            let address = SaveDealerOfferAddressFactory.getInstance(obj.address);

            if (!utils.isUndefided(obj.abi)) bank.abi = obj.abi;
            if (!utils.isUndefided(obj.accountHolderName)) bank.accountHolderName = obj.accountHolderName;
            if (!utils.isUndefided(address)) bank.address = address;
            if (!utils.isUndefided(obj.cab)) bank.cab = obj.cab;
            if (!utils.isUndefided(obj.ccbn)) bank.ccbn = obj.ccbn;
            if (!utils.isUndefided(obj.codSportello)) bank.codSportello = obj.codSportello;
            if (!utils.isUndefided(obj.ddmActivationCode)) bank.ddmActivationCode = obj.ddmActivationCode;
            if (!utils.isUndefided(obj.ddmStatusCode)) bank.ddmStatusCode = obj.ddmStatusCode;
            if (!utils.isUndefided(obj.descrAgenzia)) bank.descrAgenzia = obj.descrAgenzia;
            if (!utils.isUndefided(obj.descrBanca)) bank.descrBanca = obj.descrBanca;
            if (!utils.isUndefided(obj.iban)) bank.iban = obj.iban;
            if (!utils.isUndefided(obj.lastDdmRef)) bank.lastDdmRef = obj.lastDdmRef;
            if (!utils.isUndefided(obj.localita)) bank.localita = obj.localita;
            if (!utils.isUndefided(obj.numero)) bank.numero = obj.numero;
            if (!utils.isUndefided(obj.preferredBankAccount)) bank.preferredBankAccount = obj.preferredBankAccount;
            if (!utils.isUndefided(obj.progressivo)) bank.progressivo = obj.progressivo;
            if (!utils.isUndefided(obj.provincia)) bank.provincia = obj.provincia;
            if (!utils.isUndefided(obj.sbjCounter)) bank.sbjCounter = obj.sbjCounter;

        }

        return bank;
    }

    static getInstanceListParam(list: any): IBank[] {
        let bankList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                bankList.push(this.getInstance(elem).requestParameters());
            });
        }
        return bankList;
    }

    static getInstanceParams(obj: Bank): IBank {
        let resp = {
            abi: obj.abi,
            accountHolderName: obj.accountHolderName,
            address: obj.address,
            cab: obj.cab,
            cap: obj.cap,
            ccbn: obj.ccbn,
            codSportello: obj.codSportello,
            ddmActivationCode: obj.ddmActivationCode,
            ddmStatusCode: obj.ddmStatusCode,
            descrAgenzia: obj.descrAgenzia,
            descrBanca: obj.descrBanca,
            iban: obj.iban,
            lastDdmRef: obj.lastDdmRef,
            localita: obj.localita,
            numero: obj.numero,
            preferredBankAccount: obj.preferredBankAccount,
            progressivo: obj.progressivo,
            provincia: obj.provincia,
            sbjCounter: obj.sbjCounter
        };

        return resp;
    }

    getInstanceList(list: any): Bank[] {
        let bankList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                bankList.push(SaveDealerOfferBankFactory.getInstance(elem));
            });
        }
        return bankList;
    }

}

export interface IBank {

    abi: string;
    accountHolderName: string;
    address: IAddress;
    cab: string;
    cap: string;
    ccbn: string;
    codSportello: string;
    ddmActivationCode: string;
    ddmStatusCode: string;
    descrAgenzia: string;
    descrBanca: string;
    iban: string;
    lastDdmRef: string;
    localita: string;
    numero: string;
    preferredBankAccount: boolean;
    progressivo: number;
    provincia: string;
    sbjCounter: number;

}
