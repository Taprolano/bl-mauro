import {UtilsService} from '../../../shared/services/utils.service';
import {CampaignSaveSimulationOffer, SaveDealerOfferCampaignFactory} from './CampaignSaveSimulationOffer';
import {PropEccedenza, SaveDealerOfferPropEccedenzaFactory} from './PropEccedenza';
import {Derogation, SaveDealerOfferDerogationFactory} from './Derogation';
import {Provvigioni, SaveDealerOfferProvvigioniFactory} from './Provvigioni';
import {CashFlow, SaveDealerOfferCashFlowFactory} from './CashFlow';
import {SecondPeriodData, SaveDealerOfferSecondPeriodDataFactory} from './SecondPeriodData';
import {ContractDet} from './ContractDet';

export class FinancialProposalSaveSimulationOffer {

    private _amountRateSup: number;
    private _anticipo: boolean;
    private _anticipoPercentuale: number;
    private _balloon: boolean;
    private _baseRateTypeCode: string;
    private _campaign: CampaignSaveSimulationOffer;
    private _campaignType: string;
    private _code: string;
    private _codeIpoteca: string;
    private _codiceMetodoPagamentoAnticipo: string;
    private _codiceMetodoPagamentoBalloon: string;
    private _codiceMetodoPagamentoRate: string;
    private _commRateCode: string;
    private _conditionCode: string;
    private _contractLength: number;
    private _dataPrimoPagamento: string; // data
    private _dealerCommissionPercentage: number;
    private _dealerSIpercentage: number;
    private _derogation: Derogation;
    private _description: string;
    private _distance: number;
    private _eccedenza: PropEccedenza;
    private _expiryDate: string; // data
    private _finalExpiryDate: string; // data
    private _financeCode: string;
    private _financedTotalAmount: number;
    private _financialProductCode: string;
    private _financialProposalDate: string; // data
    private _fixedVariable: string;
    private _frequency: number;
    private _guarRequiredNumber: number;
    private _importoAnticipo: number;
    private _importoRataBalloon: number;
    private _internalProposal: boolean;
    private _ipoteca: boolean;
    private _irr: number;
    private _irrRate: number;
    private _ivaAnticipo: number;
    private _ivaBalloon: number;
    private _ivaRate: number;
    private _lastFinancialCalculationDate: string; // data
    private _limiteCommissioneMax: number;
    private _limiteCommissioneMin: number;
    private _mbfRate: number;
    private _mfrRate: number;
    private _modified: boolean;
    private _numRate: number;
    private _percentAtDealer: number;
    private _percentAtSales: number;
    private _pmsRateEffective: number;
    private _pmsRateNominal: number;
    private _portfolioRateCode: string;
    private _provvigioni: Provvigioni;
    private _rate: CashFlow[];
    private _rateAmount: number;
    private _rateTotalAmount: number;
    private _secondPeriodData: SecondPeriodData;
    private _sellerCommission: number;
    private _sellerSIpercentage: number;
    private _speseDiIstruttoria: number;
    private _spread: number;
    private _taegRate: number;
    private _tanRate: number;
    private _tassoBase: number;
    private _tegRate: number;
    private _tipoInteresse: string;
    private _tipoPianoPagamenti: string;
    private _totalAmount: number;
    private _valoreCommissione: number;
    private _vatCode: string;
    private _vatPercentage: number;


    get amountRateSup(): number {
        return this._amountRateSup;
    }

    set amountRateSup(value: number) {
        this._amountRateSup = value;
    }

    get anticipo(): boolean {
        return this._anticipo;
    }

    set anticipo(value: boolean) {
        this._anticipo = value;
    }

    get anticipoPercentuale(): number {
        return this._anticipoPercentuale;
    }

    set anticipoPercentuale(value: number) {
        this._anticipoPercentuale = value;
    }

    get balloon(): boolean {
        return this._balloon;
    }

    set balloon(value: boolean) {
        this._balloon = value;
    }

    get baseRateTypeCode(): string {
        return this._baseRateTypeCode;
    }

    set baseRateTypeCode(value: string) {
        this._baseRateTypeCode = value;
    }

    get campaign(): CampaignSaveSimulationOffer {
        return this._campaign;
    }

    set campaign(value: CampaignSaveSimulationOffer) {
        this._campaign = value;
    }

    get campaignType(): string {
        return this._campaignType;
    }

    set campaignType(value: string) {
        this._campaignType = value;
    }

    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }

    get codeIpoteca(): string {
        return this._codeIpoteca;
    }

    set codeIpoteca(value: string) {
        this._codeIpoteca = value;
    }

    get codiceMetodoPagamentoAnticipo(): string {
        return this._codiceMetodoPagamentoAnticipo;
    }

    set codiceMetodoPagamentoAnticipo(value: string) {
        this._codiceMetodoPagamentoAnticipo = value;
    }

    get codiceMetodoPagamentoBalloon(): string {
        return this._codiceMetodoPagamentoBalloon;
    }

    set codiceMetodoPagamentoBalloon(value: string) {
        this._codiceMetodoPagamentoBalloon = value;
    }

    get codiceMetodoPagamentoRate(): string {
        return this._codiceMetodoPagamentoRate;
    }

    set codiceMetodoPagamentoRate(value: string) {
        this._codiceMetodoPagamentoRate = value;
    }

    get commRateCode(): string {
        return this._commRateCode;
    }

    set commRateCode(value: string) {
        this._commRateCode = value;
    }

    get conditionCode(): string {
        return this._conditionCode;
    }

    set conditionCode(value: string) {
        this._conditionCode = value;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    get dataPrimoPagamento(): string {
        return this._dataPrimoPagamento;
    }

    set dataPrimoPagamento(value: string) {
        this._dataPrimoPagamento = value;
    }

    get dealerCommissionPercentage(): number {
        return this._dealerCommissionPercentage;
    }

    set dealerCommissionPercentage(value: number) {
        this._dealerCommissionPercentage = value;
    }

    get dealerSIpercentage(): number {
        return this._dealerSIpercentage;
    }

    set dealerSIpercentage(value: number) {
        this._dealerSIpercentage = value;
    }

    get derogation(): Derogation {
        return this._derogation;
    }

    set derogation(value: Derogation) {
        this._derogation = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get eccedenza(): PropEccedenza {
        return this._eccedenza;
    }

    set eccedenza(value: PropEccedenza) {
        this._eccedenza = value;
    }

    get expiryDate(): string {
        return this._expiryDate;
    }

    set expiryDate(value: string) {
        this._expiryDate = value;
    }

    get finalExpiryDate(): string {
        return this._finalExpiryDate;
    }

    set finalExpiryDate(value: string) {
        this._finalExpiryDate = value;
    }

    get financeCode(): string {
        return this._financeCode;
    }

    set financeCode(value: string) {
        this._financeCode = value;
    }

    get financedTotalAmount(): number {
        return this._financedTotalAmount;
    }

    set financedTotalAmount(value: number) {
        this._financedTotalAmount = value;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get financialProposalDate(): string {
        return this._financialProposalDate;
    }

    set financialProposalDate(value: string) {
        this._financialProposalDate = value;
    }

    get fixedVariable(): string {
        return this._fixedVariable;
    }

    set fixedVariable(value: string) {
        this._fixedVariable = value;
    }

    get frequency(): number {
        return this._frequency;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    get guarRequiredNumber(): number {
        return this._guarRequiredNumber;
    }

    set guarRequiredNumber(value: number) {
        this._guarRequiredNumber = value;
    }

    get importoAnticipo(): number {
        return this._importoAnticipo;
    }

    set importoAnticipo(value: number) {
        this._importoAnticipo = value;
    }

    get importoRataBalloon(): number {
        return this._importoRataBalloon;
    }

    set importoRataBalloon(value: number) {
        this._importoRataBalloon = value;
    }

    get internalProposal(): boolean {
        return this._internalProposal;
    }

    set internalProposal(value: boolean) {
        this._internalProposal = value;
    }

    get ipoteca(): boolean {
        return this._ipoteca;
    }

    set ipoteca(value: boolean) {
        this._ipoteca = value;
    }

    get irr(): number {
        return this._irr;
    }

    set irr(value: number) {
        this._irr = value;
    }

    get irrRate(): number {
        return this._irrRate;
    }

    set irrRate(value: number) {
        this._irrRate = value;
    }

    get ivaAnticipo(): number {
        return this._ivaAnticipo;
    }

    set ivaAnticipo(value: number) {
        this._ivaAnticipo = value;
    }

    get ivaBalloon(): number {
        return this._ivaBalloon;
    }

    set ivaBalloon(value: number) {
        this._ivaBalloon = value;
    }

    get ivaRate(): number {
        return this._ivaRate;
    }

    set ivaRate(value: number) {
        this._ivaRate = value;
    }

    get lastFinancialCalculationDate(): string {
        return this._lastFinancialCalculationDate;
    }

    set lastFinancialCalculationDate(value: string) {
        this._lastFinancialCalculationDate = value;
    }

    get limiteCommissioneMax(): number {
        return this._limiteCommissioneMax;
    }

    set limiteCommissioneMax(value: number) {
        this._limiteCommissioneMax = value;
    }

    get limiteCommissioneMin(): number {
        return this._limiteCommissioneMin;
    }

    set limiteCommissioneMin(value: number) {
        this._limiteCommissioneMin = value;
    }

    get mbfRate(): number {
        return this._mbfRate;
    }

    set mbfRate(value: number) {
        this._mbfRate = value;
    }

    get mfrRate(): number {
        return this._mfrRate;
    }

    set mfrRate(value: number) {
        this._mfrRate = value;
    }

    get modified(): boolean {
        return this._modified;
    }

    set modified(value: boolean) {
        this._modified = value;
    }

    get numRate(): number {
        return this._numRate;
    }

    set numRate(value: number) {
        this._numRate = value;
    }

    get percentAtDealer(): number {
        return this._percentAtDealer;
    }

    set percentAtDealer(value: number) {
        this._percentAtDealer = value;
    }

    get percentAtSales(): number {
        return this._percentAtSales;
    }

    set percentAtSales(value: number) {
        this._percentAtSales = value;
    }

    get pmsRateEffective(): number {
        return this._pmsRateEffective;
    }

    set pmsRateEffective(value: number) {
        this._pmsRateEffective = value;
    }

    get pmsRateNominal(): number {
        return this._pmsRateNominal;
    }

    set pmsRateNominal(value: number) {
        this._pmsRateNominal = value;
    }

    get portfolioRateCode(): string {
        return this._portfolioRateCode;
    }

    set portfolioRateCode(value: string) {
        this._portfolioRateCode = value;
    }

    get provvigioni(): Provvigioni {
        return this._provvigioni;
    }

    set provvigioni(value: Provvigioni) {
        this._provvigioni = value;
    }

    get rate(): CashFlow[] {
        return this._rate;
    }

    set rate(value: CashFlow[]) {
        this._rate = value;
    }

    get rateAmount(): number {
        return this._rateAmount;
    }

    set rateAmount(value: number) {
        this._rateAmount = value;
    }

    get rateTotalAmount(): number {
        return this._rateTotalAmount;
    }

    set rateTotalAmount(value: number) {
        this._rateTotalAmount = value;
    }

    get secondPeriodData(): SecondPeriodData {
        return this._secondPeriodData;
    }

    set secondPeriodData(value: SecondPeriodData) {
        this._secondPeriodData = value;
    }

    get sellerCommission(): number {
        return this._sellerCommission;
    }

    set sellerCommission(value: number) {
        this._sellerCommission = value;
    }

    get sellerSIpercentage(): number {
        return this._sellerSIpercentage;
    }

    set sellerSIpercentage(value: number) {
        this._sellerSIpercentage = value;
    }

    get speseDiIstruttoria(): number {
        return this._speseDiIstruttoria;
    }

    set speseDiIstruttoria(value: number) {
        this._speseDiIstruttoria = value;
    }

    get spread(): number {
        return this._spread;
    }

    set spread(value: number) {
        this._spread = value;
    }

    get taegRate(): number {
        return this._taegRate;
    }

    set taegRate(value: number) {
        this._taegRate = value;
    }

    get tanRate(): number {
        return this._tanRate;
    }

    set tanRate(value: number) {
        this._tanRate = value;
    }

    get tassoBase(): number {
        return this._tassoBase;
    }

    set tassoBase(value: number) {
        this._tassoBase = value;
    }

    get tegRate(): number {
        return this._tegRate;
    }

    set tegRate(value: number) {
        this._tegRate = value;
    }

    get tipoInteresse(): string {
        return this._tipoInteresse;
    }

    set tipoInteresse(value: string) {
        this._tipoInteresse = value;
    }

    get tipoPianoPagamenti(): string {
        return this._tipoPianoPagamenti;
    }

    set tipoPianoPagamenti(value: string) {
        this._tipoPianoPagamenti = value;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    get valoreCommissione(): number {
        return this._valoreCommissione;
    }

    set valoreCommissione(value: number) {
        this._valoreCommissione = value;
    }

    get vatCode(): string {
        return this._vatCode;
    }

    set vatCode(value: string) {
        this._vatCode = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }
}

export class SaveDealerOfferFinancialProposalFactory {

    static getInstance(obj: FinancialProposalSaveSimulationOffer): FinancialProposalSaveSimulationOffer {

        let saveDealerOfferCashFlowFactory = new SaveDealerOfferCashFlowFactory();
        let rate = saveDealerOfferCashFlowFactory.getInstanceList(obj.rate);

        let campaign = SaveDealerOfferCampaignFactory.getInstance(obj.campaign);
        let derogation = SaveDealerOfferDerogationFactory.getInstance(obj.derogation);
        let propEccedenza = SaveDealerOfferPropEccedenzaFactory.getInstance(obj.eccedenza);
        let provvigioni = SaveDealerOfferProvvigioniFactory.getInstance(obj.provvigioni);
        let secondPeriodData = SaveDealerOfferSecondPeriodDataFactory.getInstance(obj.secondPeriodData);


        let utils: UtilsService = new UtilsService();
        let finProp = new FinancialProposalSaveSimulationOffer();

        if (obj) {
            if (!utils.isUndefided(obj.amountRateSup)) finProp.amountRateSup = obj.amountRateSup;
            if (!utils.isUndefided(obj.anticipo)) finProp.anticipo = obj.anticipo;
            if (!utils.isUndefided(obj.anticipoPercentuale)) finProp.anticipoPercentuale = obj.anticipoPercentuale;
            if (!utils.isUndefided(obj.balloon)) finProp.balloon = obj.balloon;
            if (!utils.isUndefided(obj.baseRateTypeCode)) finProp.baseRateTypeCode = obj.baseRateTypeCode;
            if (!utils.isUndefided(campaign)) finProp.campaign = campaign;
            if (!utils.isUndefided(obj.campaignType)) finProp.campaignType = obj.campaignType;
            if (!utils.isUndefided(obj.code)) finProp.code = obj.code;
            if (!utils.isUndefided(obj.codeIpoteca)) finProp.codeIpoteca = obj.codeIpoteca;
            if (!utils.isUndefided(obj.codiceMetodoPagamentoAnticipo)) finProp.codiceMetodoPagamentoAnticipo = obj.codiceMetodoPagamentoAnticipo;
            if (!utils.isUndefided(obj.codiceMetodoPagamentoBalloon)) finProp.codiceMetodoPagamentoBalloon = obj.codiceMetodoPagamentoBalloon;
            if (!utils.isUndefided(obj.codiceMetodoPagamentoRate)) finProp.codiceMetodoPagamentoRate = obj.codiceMetodoPagamentoRate;
            if (!utils.isUndefided(obj.commRateCode)) finProp.commRateCode = obj.commRateCode;
            if (!utils.isUndefided(obj.conditionCode)) finProp.conditionCode = obj.conditionCode;
            if (!utils.isUndefided(obj.contractLength)) finProp.contractLength = obj.contractLength;
            if (!utils.isUndefided(obj.dataPrimoPagamento)) finProp.dataPrimoPagamento = obj.dataPrimoPagamento;
            if (!utils.isUndefided(obj.dealerCommissionPercentage)) finProp.dealerCommissionPercentage = obj.dealerCommissionPercentage;
            if (!utils.isUndefided(obj.dealerSIpercentage)) finProp.dealerSIpercentage = obj.dealerSIpercentage;
            if (!utils.isUndefided(derogation)) finProp.derogation = derogation;
            if (!utils.isUndefided(obj.description)) finProp.description = obj.description;
            if (!utils.isUndefided(obj.distance)) finProp.distance = obj.distance;
            if (!utils.isUndefided(propEccedenza)) finProp.eccedenza = propEccedenza;
            if (!utils.isUndefided(obj.expiryDate)) finProp.expiryDate = obj.expiryDate;
            if (!utils.isUndefided(obj.finalExpiryDate)) finProp.finalExpiryDate = obj.finalExpiryDate;
            if (!utils.isUndefided(obj.financeCode)) finProp.financeCode = obj.financeCode;
            if (!utils.isUndefided(obj.financedTotalAmount)) finProp.financedTotalAmount = obj.financedTotalAmount;
            if (!utils.isUndefided(obj.financialProductCode)) finProp.financialProductCode = obj.financialProductCode;
            if (!utils.isUndefided(obj.financialProposalDate)) finProp.financialProposalDate = obj.financialProposalDate;
            if (!utils.isUndefided(obj.fixedVariable)) finProp.fixedVariable = obj.fixedVariable;
            if (!utils.isUndefided(obj.frequency)) finProp.frequency = obj.frequency;
            if (!utils.isUndefided(obj.guarRequiredNumber)) finProp.guarRequiredNumber = obj.guarRequiredNumber;
            if (!utils.isUndefided(obj.importoAnticipo)) finProp.importoAnticipo = obj.importoAnticipo;
            if (!utils.isUndefided(obj.importoRataBalloon)) finProp.importoRataBalloon = obj.importoRataBalloon;
            if (!utils.isUndefided(obj.internalProposal)) finProp.internalProposal = obj.internalProposal;
            if (!utils.isUndefided(obj.ipoteca)) finProp.ipoteca = obj.ipoteca;
            if (!utils.isUndefided(obj.irr)) finProp.irr = obj.irr;
            if (!utils.isUndefided(obj.irrRate)) finProp.irrRate = obj.irrRate;
            if (!utils.isUndefided(obj.ivaAnticipo)) finProp.ivaAnticipo = obj.ivaAnticipo;
            if (!utils.isUndefided(obj.ivaBalloon)) finProp.ivaBalloon = obj.ivaBalloon;
            if (!utils.isUndefided(obj.ivaRate)) finProp.ivaRate = obj.ivaRate;
            if (!utils.isUndefided(obj.lastFinancialCalculationDate)) finProp.lastFinancialCalculationDate = obj.lastFinancialCalculationDate;
            if (!utils.isUndefided(obj.limiteCommissioneMax)) finProp.limiteCommissioneMax = obj.limiteCommissioneMax;
            if (!utils.isUndefided(obj.limiteCommissioneMin)) finProp.limiteCommissioneMin = obj.limiteCommissioneMin;
            if (!utils.isUndefided(obj.mbfRate)) finProp.mbfRate = obj.mbfRate;
            if (!utils.isUndefided(obj.mfrRate)) finProp.mfrRate = obj.mfrRate;
            if (!utils.isUndefided(obj.modified)) finProp.modified = obj.modified;
            if (!utils.isUndefided(obj.numRate)) finProp.numRate = obj.numRate;
            if (!utils.isUndefided(obj.percentAtDealer)) finProp.percentAtDealer = obj.percentAtDealer;
            if (!utils.isUndefided(obj.percentAtSales)) finProp.percentAtSales = obj.percentAtSales;
            if (!utils.isUndefided(obj.pmsRateEffective)) finProp.pmsRateEffective = obj.pmsRateEffective;
            if (!utils.isUndefided(obj.pmsRateNominal)) finProp.pmsRateNominal = obj.pmsRateNominal;
            if (!utils.isUndefided(obj.portfolioRateCode)) finProp.portfolioRateCode = obj.portfolioRateCode;
            if (!utils.isUndefided(provvigioni)) finProp.provvigioni = provvigioni;
            if (!utils.isUndefided(rate)) finProp.rate = rate;
            if (!utils.isUndefided(obj.rateAmount)) finProp.rateAmount = obj.rateAmount;
            if (!utils.isUndefided(obj.rateTotalAmount)) finProp.rateTotalAmount = obj.rateTotalAmount;
            if (!utils.isUndefided(secondPeriodData)) finProp.secondPeriodData = secondPeriodData;
            if (!utils.isUndefided(obj.sellerCommission)) finProp.sellerCommission = obj.sellerCommission;
            if (!utils.isUndefided(obj.sellerSIpercentage)) finProp.sellerSIpercentage = obj.sellerSIpercentage;
            if (!utils.isUndefided(obj.speseDiIstruttoria)) finProp.speseDiIstruttoria = obj.speseDiIstruttoria;
            if (!utils.isUndefided(obj.spread)) finProp.spread = obj.spread;
            if (!utils.isUndefided(obj.taegRate)) finProp.taegRate = obj.taegRate;
            if (!utils.isUndefided(obj.tanRate)) finProp.tanRate = obj.tanRate;
            if (!utils.isUndefided(obj.tassoBase)) finProp.tassoBase = obj.tassoBase;
            if (!utils.isUndefided(obj.tegRate)) finProp.tegRate = obj.tegRate;
            if (!utils.isUndefided(obj.tipoInteresse)) finProp.tipoInteresse = obj.tipoInteresse;
            if (!utils.isUndefided(obj.tipoPianoPagamenti)) finProp.tipoPianoPagamenti = obj.tipoPianoPagamenti;
            if (!utils.isUndefided(obj.totalAmount)) finProp.totalAmount = obj.totalAmount;
            if (!utils.isUndefided(obj.valoreCommissione)) finProp.valoreCommissione = obj.valoreCommissione;
            if (!utils.isUndefided(obj.vatCode)) finProp.vatCode = obj.vatCode;
            if (!utils.isUndefided(obj.vatPercentage)) finProp.vatPercentage = obj.vatPercentage;

        }

        return finProp;
    }

    static getInstanceParams(obj: FinancialProposalSaveSimulationOffer): Object {
        let resp = {
            amountRateSup: obj.amountRateSup,
            anticipo: obj.anticipo,
            anticipoPercentuale: obj.anticipoPercentuale,
            balloon: obj.balloon,
            baseRateTypeCode: obj.baseRateTypeCode,
            campaign: SaveDealerOfferCampaignFactory.getInstanceParams(obj.campaign),
            campaignType: obj.campaignType,
            code: obj.code,
            codeIpoteca: obj.codeIpoteca,
            codiceMetodoPagamentoAnticipo: obj.codiceMetodoPagamentoAnticipo,
            codiceMetodoPagamentoBalloon: obj.codiceMetodoPagamentoBalloon,
            codiceMetodoPagamentoRate: obj.codiceMetodoPagamentoRate,
            commRateCode: obj.commRateCode,
            conditionCode: obj.conditionCode,
            contractLength: obj.contractLength,
            dataPrimoPagamento: obj.dataPrimoPagamento,
            dealerCommissionPercentage: obj.dealerCommissionPercentage,
            dealerSIpercentage: obj.dealerSIpercentage,
            derogation: SaveDealerOfferDerogationFactory.getInstanceParams(obj.derogation),
            description: obj.description,
            distance: obj.distance,
            eccedenza: SaveDealerOfferPropEccedenzaFactory.getInstanceParams(obj.eccedenza),
            expiryDate: obj.expiryDate,
            finalExpiryDate: obj.finalExpiryDate,
            financeCode: obj.financeCode,
            financedTotalAmount: obj.financedTotalAmount,
            financialProductCode: obj.financialProductCode,
            financialProposalDate: obj.financialProposalDate,
            fixedVariable: obj.fixedVariable,
            frequency: obj.frequency,
            guarRequiredNumber: obj.guarRequiredNumber,
            importoAnticipo: obj.importoAnticipo,
            importoRataBalloon: obj.importoRataBalloon,
            internalProposal: obj.internalProposal,
            ipoteca: obj.ipoteca,
            irr: obj.irr,
            irrRate: obj.irrRate,
            ivaAnticipo: obj.ivaAnticipo,
            ivaBalloon: obj.ivaBalloon,
            ivaRate: obj.ivaRate,
            lastFinancialCalculationDate: obj.lastFinancialCalculationDate,
            limiteCommissioneMax: obj.limiteCommissioneMax,
            limiteCommissioneMin: obj.limiteCommissioneMin,
            mbfRate: obj.mbfRate,
            mfrRate: obj.mfrRate,
            modified: obj.modified,
            numRate: obj.numRate,
            percentAtDealer: obj.percentAtDealer,
            percentAtSales: obj.percentAtSales,
            pmsRateEffective: obj.pmsRateEffective,
            pmsRateNominal: obj.pmsRateNominal,
            portfolioRateCode: obj.portfolioRateCode,
            provvigioni: SaveDealerOfferProvvigioniFactory.getInstanceParams(obj.provvigioni),
            rate: SaveDealerOfferCashFlowFactory.getInstanceListParam(obj.rate),
            rateAmount: obj.rateAmount,
            rateTotalAmount: obj.rateTotalAmount,
            secondPeriodData: SaveDealerOfferSecondPeriodDataFactory.getInstanceParams(obj.secondPeriodData),
            sellerCommission: obj.sellerCommission,
            sellerSIpercentage: obj.sellerSIpercentage,
            speseDiIstruttoria: obj.speseDiIstruttoria,
            spread: obj.spread,
            taegRate: obj.taegRate,
            tanRate: obj.tanRate,
            tassoBase: obj.tassoBase,
            tegRate: obj.tegRate,
            tipoInteresse: obj.tipoInteresse,
            tipoPianoPagamenti: obj.tipoPianoPagamenti,
            totalAmount: obj.totalAmount,
            valoreCommissione: obj.valoreCommissione,
            vatCode: obj.vatCode,
            vatPercentage: obj.vatPercentage
        };

        return resp;
    }

    getInstanceList(list: any): FinancialProposalSaveSimulationOffer[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferFinancialProposalFactory.getInstance(elem));
            });
        }
        return optionalList;
    }


}
