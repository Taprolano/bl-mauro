import {UtilsService} from '../../../shared/services/utils.service';
import {PropEccedenza} from './PropEccedenza';

export class ResidValueConfig {

    private _assetTypeCode: string;
    private _performResidualValueCheck: boolean;
    private _weight: number;

    get assetTypeCode(): string {
        return this._assetTypeCode;
    }

    set assetTypeCode(value: string) {
        this._assetTypeCode = value;
    }

    get performResidualValueCheck(): boolean {
        return this._performResidualValueCheck;
    }

    set performResidualValueCheck(value: boolean) {
        this._performResidualValueCheck = value;
    }

    get weight(): number {
        return this._weight;
    }

    set weight(value: number) {
        this._weight = value;
    }
}

export class SaveDealerOfferResidValueConfigFactory {

    static getInstance(obj: ResidValueConfig): ResidValueConfig {

        let utils: UtilsService = new UtilsService();
        let resid = new ResidValueConfig();

        if (obj) {
            if (!utils.isUndefided(obj.assetTypeCode)) resid.assetTypeCode = obj.assetTypeCode;
            if (!utils.isUndefided(obj.performResidualValueCheck)) resid.performResidualValueCheck = obj.performResidualValueCheck;
            if (!utils.isUndefided(obj.weight)) resid.weight = obj.weight;
        }

        return resid;
    }

    static getInstanceParams(obj: ResidValueConfig): IResidValueConfig {

        let requestBody: IResidValueConfig = {
            assetTypeCode: obj.assetTypeCode,
            performResidualValueCheck: obj.performResidualValueCheck,
            weight: obj.weight
        };

        return requestBody;
    }

    getInstanceList(list: any): ResidValueConfig[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferResidValueConfigFactory.getInstance(elem));
            });
        }
        return optionalList;
    }


}

export interface IResidValueConfig {

    assetTypeCode: string;
    performResidualValueCheck: boolean;
    weight: number;

}
