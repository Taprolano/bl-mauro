import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class CompanyCondition {

    private _anticipoMax: number;
    private _anticipoMin: number;
    private _dataScadenza: string; // data
    private _decorrenzaMax: number;
    private _decorrenzaMin: number;
    private _durataMax: number;
    private _durataMin: number;
    private _frequenzaMax: number;
    private _frequenzaMin: number;
    private _id: string;
    private _numPagMax: number;
    private _numPagMin: number;
    private _riscattoMax: number;
    private _riscattoMin: number;


    get anticipoMax(): number {
        return this._anticipoMax;
    }

    set anticipoMax(value: number) {
        this._anticipoMax = value;
    }

    get anticipoMin(): number {
        return this._anticipoMin;
    }

    set anticipoMin(value: number) {
        this._anticipoMin = value;
    }

    get dataScadenza(): string {
        return this._dataScadenza;
    }

    set dataScadenza(value: string) {
        this._dataScadenza = value;
    }

    get decorrenzaMax(): number {
        return this._decorrenzaMax;
    }

    set decorrenzaMax(value: number) {
        this._decorrenzaMax = value;
    }

    get decorrenzaMin(): number {
        return this._decorrenzaMin;
    }

    set decorrenzaMin(value: number) {
        this._decorrenzaMin = value;
    }

    get durataMax(): number {
        return this._durataMax;
    }

    set durataMax(value: number) {
        this._durataMax = value;
    }

    get durataMin(): number {
        return this._durataMin;
    }

    set durataMin(value: number) {
        this._durataMin = value;
    }

    get frequenzaMax(): number {
        return this._frequenzaMax;
    }

    set frequenzaMax(value: number) {
        this._frequenzaMax = value;
    }

    get frequenzaMin(): number {
        return this._frequenzaMin;
    }

    set frequenzaMin(value: number) {
        this._frequenzaMin = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get numPagMax(): number {
        return this._numPagMax;
    }

    set numPagMax(value: number) {
        this._numPagMax = value;
    }

    get numPagMin(): number {
        return this._numPagMin;
    }

    set numPagMin(value: number) {
        this._numPagMin = value;
    }

    get riscattoMax(): number {
        return this._riscattoMax;
    }

    set riscattoMax(value: number) {
        this._riscattoMax = value;
    }

    get riscattoMin(): number {
        return this._riscattoMin;
    }

    set riscattoMin(value: number) {
        this._riscattoMin = value;
    }
}

export class SaveDealerOfferCompanyConditionFactory {

    static getInstance(obj: CompanyCondition): CompanyCondition {

        let utils: UtilsService = new UtilsService();
        let compCondition = new CompanyCondition();

        if (obj) {
            if (!utils.isUndefided(obj.anticipoMax)) compCondition.anticipoMax = obj.anticipoMax;
            if (!utils.isUndefided(obj.anticipoMin)) compCondition.anticipoMin = obj.anticipoMin;
            if (!utils.isUndefided(obj.dataScadenza)) compCondition.dataScadenza = obj.dataScadenza;
            if (!utils.isUndefided(obj.decorrenzaMax)) compCondition.decorrenzaMax = obj.decorrenzaMax;
            if (!utils.isUndefided(obj.decorrenzaMin)) compCondition.decorrenzaMin = obj.decorrenzaMin;
            if (!utils.isUndefided(obj.durataMax)) compCondition.durataMax = obj.durataMax;
            if (!utils.isUndefided(obj.durataMin)) compCondition.durataMin = obj.durataMin;
            if (!utils.isUndefided(obj.frequenzaMax)) compCondition.frequenzaMax = obj.frequenzaMax;
            if (!utils.isUndefided(obj.frequenzaMin)) compCondition.frequenzaMin = obj.frequenzaMin;
            if (!utils.isUndefided(obj.id)) compCondition.id = obj.id;
            if (!utils.isUndefided(obj.numPagMax)) compCondition.numPagMax = obj.numPagMax;
            if (!utils.isUndefided(obj.numPagMin)) compCondition.numPagMin = obj.numPagMin;
            if (!utils.isUndefided(obj.riscattoMax)) compCondition.riscattoMax = obj.riscattoMax;
            if (!utils.isUndefided(obj.riscattoMin)) compCondition.riscattoMin = obj.riscattoMin;
        }

        return compCondition;
    }

    static getInstanceParams(obj: CompanyCondition): Object {
        let resp = {
            anticipoMax: obj.anticipoMax,
            anticipoMin: obj.anticipoMin,
            dataScadenza: obj.dataScadenza,
            decorrenzaMax: obj.decorrenzaMax,
            decorrenzaMin: obj.decorrenzaMin,
            durataMax: obj.durataMax,
            durataMin: obj.durataMin,
            frequenzaMax: obj.frequenzaMax,
            frequenzaMin: obj.frequenzaMin,
            id: obj.id,
            numPagMax: obj.numPagMax,
            numPagMin: obj.numPagMin,
            riscattoMax: obj.riscattoMin,
            riscattoMin: obj.riscattoMin
        };

        return resp;
    }

    getInstanceList(list: any): CompanyCondition[] {
        let compConditionlList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                compConditionlList.push(SaveDealerOfferCompanyConditionFactory.getInstance(elem));
            });
        }
        return compConditionlList;
    }

}
