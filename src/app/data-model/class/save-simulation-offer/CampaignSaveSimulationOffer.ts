import {UtilsService} from '../../../shared/services/utils.service';
import {CompanyCondition, SaveDealerOfferCompanyConditionFactory} from './CompanyCondition';
import {Attachment, AttachmentFactory} from '../Attachment';

export class CampaignSaveSimulationOffer {

    private _adminFeeShareOverride: boolean;
    private _attachment: Attachment;
    private _campaignType: string;
    private _code: string;
    private _commCamp: boolean;
    private _currentCompanyCondition: CompanyCondition;
    private _dealFixCom: number;
    private _dealManuContrComm: boolean;
    private _dealPercCom: number;
    private _dealPercManuContrComm: number;
    private _defaultValue: boolean;
    private _description: string;
    private _expiryDate: string; // data
    private _fbrMaxBalloonCompensation: number;
    private _finalExpiryDate: string; // data
    private _flagShare: boolean;
    private _internalProposal: boolean;
    private _manuFixContrComm: number;
    private _manuPercContrComm: number;
    private _mustUseSpIstr: boolean;
    private _selFixComAmount: number;
    private _selFixComPerc: number;
    private _showVoucher: boolean;
    private _speseIstruttoria: number;
    private _tattica: boolean;
    private _voucherMandatory: boolean;


    get adminFeeShareOverride(): boolean {
        return this._adminFeeShareOverride;
    }

    set adminFeeShareOverride(value: boolean) {
        this._adminFeeShareOverride = value;
    }

    get attachment(): Attachment {
        return this._attachment;
    }

    set attachment(value: Attachment) {
        this._attachment = value;
    }

    get campaignType(): string {
        return this._campaignType;
    }

    set campaignType(value: string) {
        this._campaignType = value;
    }

    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }

    get commCamp(): boolean {
        return this._commCamp;
    }

    set commCamp(value: boolean) {
        this._commCamp = value;
    }

    get currentCompanyCondition(): CompanyCondition {
        return this._currentCompanyCondition;
    }

    set currentCompanyCondition(value: CompanyCondition) {
        this._currentCompanyCondition = value;
    }

    get dealFixCom(): number {
        return this._dealFixCom;
    }

    set dealFixCom(value: number) {
        this._dealFixCom = value;
    }

    get dealManuContrComm(): boolean {
        return this._dealManuContrComm;
    }

    set dealManuContrComm(value: boolean) {
        this._dealManuContrComm = value;
    }

    get dealPercCom(): number {
        return this._dealPercCom;
    }

    set dealPercCom(value: number) {
        this._dealPercCom = value;
    }

    get dealPercManuContrComm(): number {
        return this._dealPercManuContrComm;
    }

    set dealPercManuContrComm(value: number) {
        this._dealPercManuContrComm = value;
    }

    get defaultValue(): boolean {
        return this._defaultValue;
    }

    set defaultValue(value: boolean) {
        this._defaultValue = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get expiryDate(): string {
        return this._expiryDate;
    }

    set expiryDate(value: string) {
        this._expiryDate = value;
    }

    get fbrMaxBalloonCompensation(): number {
        return this._fbrMaxBalloonCompensation;
    }

    set fbrMaxBalloonCompensation(value: number) {
        this._fbrMaxBalloonCompensation = value;
    }

    get finalExpiryDate(): string {
        return this._finalExpiryDate;
    }

    set finalExpiryDate(value: string) {
        this._finalExpiryDate = value;
    }

    get flagShare(): boolean {
        return this._flagShare;
    }

    set flagShare(value: boolean) {
        this._flagShare = value;
    }

    get internalProposal(): boolean {
        return this._internalProposal;
    }

    set internalProposal(value: boolean) {
        this._internalProposal = value;
    }

    get manuFixContrComm(): number {
        return this._manuFixContrComm;
    }

    set manuFixContrComm(value: number) {
        this._manuFixContrComm = value;
    }

    get manuPercContrComm(): number {
        return this._manuPercContrComm;
    }

    set manuPercContrComm(value: number) {
        this._manuPercContrComm = value;
    }

    get mustUseSpIstr(): boolean {
        return this._mustUseSpIstr;
    }

    set mustUseSpIstr(value: boolean) {
        this._mustUseSpIstr = value;
    }

    get selFixComAmount(): number {
        return this._selFixComAmount;
    }

    set selFixComAmount(value: number) {
        this._selFixComAmount = value;
    }

    get selFixComPerc(): number {
        return this._selFixComPerc;
    }

    set selFixComPerc(value: number) {
        this._selFixComPerc = value;
    }

    get showVoucher(): boolean {
        return this._showVoucher;
    }

    set showVoucher(value: boolean) {
        this._showVoucher = value;
    }

    get speseIstruttoria(): number {
        return this._speseIstruttoria;
    }

    set speseIstruttoria(value: number) {
        this._speseIstruttoria = value;
    }

    get tattica(): boolean {
        return this._tattica;
    }

    set tattica(value: boolean) {
        this._tattica = value;
    }

    get voucherMandatory(): boolean {
        return this._voucherMandatory;
    }

    set voucherMandatory(value: boolean) {
        this._voucherMandatory = value;
    }
}

export class SaveDealerOfferCampaignFactory {

    static getInstance(obj: CampaignSaveSimulationOffer): CampaignSaveSimulationOffer {

        let attachments = AttachmentFactory.getInstance(obj.attachment);
        let currentCompanyCondition = SaveDealerOfferCompanyConditionFactory.getInstance(obj.currentCompanyCondition);

        let utils: UtilsService = new UtilsService();
        let campaign = new CampaignSaveSimulationOffer();

        if (obj) {
            if (!utils.isUndefided(obj.adminFeeShareOverride)) campaign.adminFeeShareOverride = obj.adminFeeShareOverride;
            if (!utils.isUndefided(attachments)) campaign.attachment = attachments;
            if (!utils.isUndefided(obj.campaignType)) campaign.campaignType = obj.campaignType;
            if (!utils.isUndefided(obj.code)) campaign.code = obj.code;
            if (!utils.isUndefided(obj.commCamp)) campaign.commCamp = obj.commCamp;
            if (!utils.isUndefided(currentCompanyCondition)) campaign.currentCompanyCondition = currentCompanyCondition;
            if (!utils.isUndefided(obj.dealFixCom)) campaign.dealFixCom = obj.dealFixCom;
            if (!utils.isUndefided(obj.dealManuContrComm)) campaign.dealManuContrComm = obj.dealManuContrComm;
            if (!utils.isUndefided(obj.defaultValue)) campaign.defaultValue = obj.defaultValue;
            if (!utils.isUndefided(obj.description)) campaign.description = obj.description;
            if (!utils.isUndefided(obj.expiryDate)) campaign.expiryDate = obj.expiryDate;
            if (!utils.isUndefided(obj.fbrMaxBalloonCompensation)) campaign.fbrMaxBalloonCompensation = obj.fbrMaxBalloonCompensation;
            if (!utils.isUndefided(obj.finalExpiryDate)) campaign.finalExpiryDate = obj.finalExpiryDate;
            if (!utils.isUndefided(obj.flagShare)) campaign.flagShare = obj.flagShare;
            if (!utils.isUndefided(obj.internalProposal)) campaign.internalProposal = obj.internalProposal;
            if (!utils.isUndefided(obj.manuFixContrComm)) campaign.manuFixContrComm = obj.manuFixContrComm;
            if (!utils.isUndefided(obj.manuPercContrComm)) campaign.manuPercContrComm = obj.manuPercContrComm;
            if (!utils.isUndefided(obj.mustUseSpIstr)) campaign.mustUseSpIstr = obj.mustUseSpIstr;
            if (!utils.isUndefided(obj.selFixComAmount)) campaign.selFixComAmount = obj.selFixComAmount;
            if (!utils.isUndefided(obj.selFixComPerc)) campaign.selFixComPerc = obj.selFixComPerc;
            if (!utils.isUndefided(obj.showVoucher)) campaign.showVoucher = obj.showVoucher;
            if (!utils.isUndefided(obj.speseIstruttoria)) campaign.speseIstruttoria = obj.speseIstruttoria;
            if (!utils.isUndefided(obj.tattica)) campaign.tattica = obj.tattica;
            if (!utils.isUndefided(obj.voucherMandatory)) campaign.voucherMandatory = obj.voucherMandatory;
        }

        return campaign;
    }

    static getInstanceParams(obj: CampaignSaveSimulationOffer): Object {
        let resp = {
            adminFeeShareOverride: obj.adminFeeShareOverride,
            attachment: AttachmentFactory.getInstanceParams(obj.attachment),
            campaignType: obj.campaignType,
            code: obj.code,
            commCamp: obj.commCamp,
            currentCompanyCondition: SaveDealerOfferCompanyConditionFactory.getInstanceParams(obj.currentCompanyCondition),
            dealFixCom: obj.dealFixCom,
            dealManuContrComm: obj.dealManuContrComm,
            dealPercCom: obj.dealPercCom,
            dealPercManuContrComm: obj.dealPercManuContrComm,
            defaultValue: obj.defaultValue,
            description: obj.description,
            expiryDate: obj.expiryDate,
            fbrMaxBalloonCompensation: obj.fbrMaxBalloonCompensation,
            finalExpiryDate: obj.finalExpiryDate,
            flagShare: obj.flagShare,
            internalProposal: obj.internalProposal,
            manuFixContrComm: obj.manuFixContrComm,
            manuPercContrComm: obj.manuPercContrComm,
            mustUseSpIstr: obj.mustUseSpIstr,
            selFixComAmount: obj.selFixComAmount,
            selFixComPerc: obj.selFixComPerc,
            showVoucher: obj.showVoucher,
            speseIstruttoria: obj.speseIstruttoria,
            tattica: obj.tattica,
            voucherMandatory: obj.voucherMandatory
        };

        return resp;
    }

    getInstanceList(list: any): CampaignSaveSimulationOffer[] {
        let campaignList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                campaignList.push(SaveDealerOfferCampaignFactory.getInstance(elem));
            });
        }
        return campaignList;
    }

}
