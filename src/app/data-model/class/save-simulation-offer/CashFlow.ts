import {UtilsService} from '../../../shared/services/utils.service';
import {ISaveSimulationOfferOptionals} from './Optional';

export class CashFlow {

    private _anticipo: boolean;
    private _balloon: boolean;
    private _bloccato: boolean;
    private _brandId: string;
    private _cashFlowTypeCode: string;
    private _cashFlowTypeDesc: string;
    private _cdTer: number;
    private _cfAmount: number;
    private _cfAmountMult: number;
    private _cfAmountPercentage: number;
    private _cfFixed: string;
    private _cfFreqCode: string;
    private _cfSeqNum: number;
    private _cmpg: string;
    private _dataPagamentoRata: string; // data
    private _daysFromPreviousPayment: number;
    private _defBpFreqCode: string;
    private _defaultBpNumOfPaym: number;
    private _distanza: number;
    private _financialProductCode: string;
    private _mandatory: string;
    private _molteplicita: number;
    private _numberOfTimes: number;
    private _paymMetUpdatable: string;
    private _paymentDate: string; // data
    private _sequenceNumber: number;
    private _singleMultiple: string;
    private _supplyTypeCode: string;
    private _totalAmount: number;
    private _vatAmount: number;
    private _vatCode: string;
    private _vatPercentage: number;


    get anticipo(): boolean {
        return this._anticipo;
    }

    set anticipo(value: boolean) {
        this._anticipo = value;
    }

    get balloon(): boolean {
        return this._balloon;
    }

    set balloon(value: boolean) {
        this._balloon = value;
    }

    get bloccato(): boolean {
        return this._bloccato;
    }

    set bloccato(value: boolean) {
        this._bloccato = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get cashFlowTypeCode(): string {
        return this._cashFlowTypeCode;
    }

    set cashFlowTypeCode(value: string) {
        this._cashFlowTypeCode = value;
    }

    get cashFlowTypeDesc(): string {
        return this._cashFlowTypeDesc;
    }

    set cashFlowTypeDesc(value: string) {
        this._cashFlowTypeDesc = value;
    }

    get cdTer(): number {
        return this._cdTer;
    }

    set cdTer(value: number) {
        this._cdTer = value;
    }

    get cfAmount(): number {
        return this._cfAmount;
    }

    set cfAmount(value: number) {
        this._cfAmount = value;
    }

    get cfAmountMult(): number {
        return this._cfAmountMult;
    }

    set cfAmountMult(value: number) {
        this._cfAmountMult = value;
    }

    get cfAmountPercentage(): number {
        return this._cfAmountPercentage;
    }

    set cfAmountPercentage(value: number) {
        this._cfAmountPercentage = value;
    }

    get cfFixed(): string {
        return this._cfFixed;
    }

    set cfFixed(value: string) {
        this._cfFixed = value;
    }

    get cfFreqCode(): string {
        return this._cfFreqCode;
    }

    set cfFreqCode(value: string) {
        this._cfFreqCode = value;
    }

    get cfSeqNum(): number {
        return this._cfSeqNum;
    }

    set cfSeqNum(value: number) {
        this._cfSeqNum = value;
    }

    get cmpg(): string {
        return this._cmpg;
    }

    set cmpg(value: string) {
        this._cmpg = value;
    }

    get dataPagamentoRata(): string {
        return this._dataPagamentoRata;
    }

    set dataPagamentoRata(value: string) {
        this._dataPagamentoRata = value;
    }

    get daysFromPreviousPayment(): number {
        return this._daysFromPreviousPayment;
    }

    set daysFromPreviousPayment(value: number) {
        this._daysFromPreviousPayment = value;
    }

    get defBpFreqCode(): string {
        return this._defBpFreqCode;
    }

    set defBpFreqCode(value: string) {
        this._defBpFreqCode = value;
    }

    get defaultBpNumOfPaym(): number {
        return this._defaultBpNumOfPaym;
    }

    set defaultBpNumOfPaym(value: number) {
        this._defaultBpNumOfPaym = value;
    }

    get distanza(): number {
        return this._distanza;
    }

    set distanza(value: number) {
        this._distanza = value;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get mandatory(): string {
        return this._mandatory;
    }

    set mandatory(value: string) {
        this._mandatory = value;
    }

    get molteplicita(): number {
        return this._molteplicita;
    }

    set molteplicita(value: number) {
        this._molteplicita = value;
    }

    get numberOfTimes(): number {
        return this._numberOfTimes;
    }

    set numberOfTimes(value: number) {
        this._numberOfTimes = value;
    }

    get paymMetUpdatable(): string {
        return this._paymMetUpdatable;
    }

    set paymMetUpdatable(value: string) {
        this._paymMetUpdatable = value;
    }

    get paymentDate(): string {
        return this._paymentDate;
    }

    set paymentDate(value: string) {
        this._paymentDate = value;
    }

    get sequenceNumber(): number {
        return this._sequenceNumber;
    }

    set sequenceNumber(value: number) {
        this._sequenceNumber = value;
    }

    get singleMultiple(): string {
        return this._singleMultiple;
    }

    set singleMultiple(value: string) {
        this._singleMultiple = value;
    }

    get supplyTypeCode(): string {
        return this._supplyTypeCode;
    }

    set supplyTypeCode(value: string) {
        this._supplyTypeCode = value;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    get vatAmount(): number {
        return this._vatAmount;
    }

    set vatAmount(value: number) {
        this._vatAmount = value;
    }

    get vatCode(): string {
        return this._vatCode;
    }

    set vatCode(value: string) {
        this._vatCode = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    public requestParameters(): ICashFlow {
        let requestBody: ICashFlow = {
            anticipo: this._anticipo,
            balloon: this._balloon,
            bloccato: this._bloccato,
            brandId: this._brandId,
            cashFlowTypeCode: this._cashFlowTypeCode,
            cashFlowTypeDesc: this._cashFlowTypeDesc,
            cdTer: this._cdTer,
            cfAmount: this._cfAmount,
            cfAmountMult: this._cfAmountMult,
            cfAmountPercentage: this._cfAmountPercentage,
            cfFixed: this._cfFixed,
            cfFreqCode: this._cfFreqCode,
            cfSeqNum: this._cfSeqNum,
            cmpg: this._cmpg,
            dataPagamentoRata: this._dataPagamentoRata,
            daysFromPreviousPayment: this._daysFromPreviousPayment,
            defBpFreqCode: this._defBpFreqCode,
            defaultBpNumOfPaym: this._defaultBpNumOfPaym,
            distanza: this._distanza,
            financialProductCode: this._financialProductCode,
            mandatory: this._mandatory,
            molteplicita: this._molteplicita,
            numberOfTimes: this._numberOfTimes,
            paymMetUpdatable: this._paymMetUpdatable,
            paymentDate: this._paymentDate,
            sequenceNumber: this._sequenceNumber,
            singleMultiple: this._singleMultiple,
            supplyTypeCode: this._supplyTypeCode,
            totalAmount: this._totalAmount,
            vatAmount: this._vatAmount,
            vatCode: this._vatCode,
            vatPercentage: this._vatPercentage
        };

        return requestBody;
    }

}

export class SaveDealerOfferCashFlowFactory {

    static getInstance(obj: CashFlow): CashFlow {

        let utils: UtilsService = new UtilsService();
        let cashFlow = new CashFlow();

        if (obj) {
            if (!utils.isUndefided(obj.anticipo)) cashFlow.anticipo = obj.anticipo;
            if (!utils.isUndefided(obj.balloon)) cashFlow.balloon = obj.balloon;
            if (!utils.isUndefided(obj.bloccato)) cashFlow.bloccato = obj.bloccato;
            if (!utils.isUndefided(obj.brandId)) cashFlow.brandId = obj.brandId;
            if (!utils.isUndefided(obj.cashFlowTypeCode)) cashFlow.cashFlowTypeCode = obj.cashFlowTypeCode;
            if (!utils.isUndefided(obj.cashFlowTypeDesc)) cashFlow.cashFlowTypeDesc = obj.cashFlowTypeDesc;
            if (!utils.isUndefided(obj.cdTer)) cashFlow.cdTer = obj.cdTer;
            if (!utils.isUndefided(obj.cfAmount)) cashFlow.cfAmount = obj.cfAmount;
            if (!utils.isUndefided(obj.cfAmountMult)) cashFlow.cfAmountMult = obj.cfAmountMult;
            if (!utils.isUndefided(obj.cfAmountPercentage)) cashFlow.cfAmountPercentage = obj.cfAmountPercentage;
            if (!utils.isUndefided(obj.cfFixed)) cashFlow.cfFixed = obj.cfFixed;
            if (!utils.isUndefided(obj.cfFreqCode)) cashFlow.cfFreqCode = obj.cfFreqCode;
            if (!utils.isUndefided(obj.cfSeqNum)) cashFlow.cfSeqNum = obj.cfSeqNum;
            if (!utils.isUndefided(obj.cmpg)) cashFlow.cmpg = obj.cmpg;
            if (!utils.isUndefided(obj.dataPagamentoRata)) cashFlow.dataPagamentoRata = obj.dataPagamentoRata;
            if (!utils.isUndefided(obj.daysFromPreviousPayment)) cashFlow.daysFromPreviousPayment = obj.daysFromPreviousPayment;
            if (!utils.isUndefided(obj.defBpFreqCode)) cashFlow.defBpFreqCode = obj.defBpFreqCode;
            if (!utils.isUndefided(obj.defaultBpNumOfPaym)) cashFlow.defaultBpNumOfPaym = obj.defaultBpNumOfPaym;
            if (!utils.isUndefided(obj.distanza)) cashFlow.distanza = obj.distanza;
            if (!utils.isUndefided(obj.financialProductCode)) cashFlow.financialProductCode = obj.financialProductCode;
            if (!utils.isUndefided(obj.mandatory)) cashFlow.mandatory = obj.mandatory;
            if (!utils.isUndefided(obj.molteplicita)) cashFlow.molteplicita = obj.molteplicita;
            if (!utils.isUndefided(obj.numberOfTimes)) cashFlow.numberOfTimes = obj.numberOfTimes;
            if (!utils.isUndefided(obj.paymMetUpdatable)) cashFlow.paymMetUpdatable = obj.paymMetUpdatable;
            if (!utils.isUndefided(obj.paymentDate)) cashFlow.paymentDate = obj.paymentDate;
            if (!utils.isUndefided(obj.sequenceNumber)) cashFlow.sequenceNumber = obj.sequenceNumber;
            if (!utils.isUndefided(obj.singleMultiple)) cashFlow.singleMultiple = obj.singleMultiple;
            if (!utils.isUndefided(obj.supplyTypeCode)) cashFlow.supplyTypeCode = obj.supplyTypeCode;
            if (!utils.isUndefided(obj.totalAmount)) cashFlow.totalAmount = obj.totalAmount;
            if (!utils.isUndefided(obj.vatAmount)) cashFlow.vatAmount = obj.vatAmount;
            if (!utils.isUndefided(obj.vatCode)) cashFlow.vatCode = obj.vatCode;
            if (!utils.isUndefided(obj.vatPercentage)) cashFlow.vatPercentage = obj.vatPercentage;

        }

        return cashFlow;
    }

    static getInstanceListParam(list: any): ICashFlow[] {
        let cashFlowList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                cashFlowList.push(this.getInstance(elem).requestParameters());
            });
        }
        return cashFlowList;
    }

    getInstanceList(list: any): CashFlow[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferCashFlowFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

}

export interface ICashFlow {

    anticipo: boolean;
    balloon: boolean;
    bloccato: boolean;
    brandId: string;
    cashFlowTypeCode: string;
    cashFlowTypeDesc: string;
    cdTer: number;
    cfAmount: number;
    cfAmountMult: number;
    cfAmountPercentage: number;
    cfFixed: string;
    cfFreqCode: string;
    cfSeqNum: number;
    cmpg: string;
    dataPagamentoRata: string;
    daysFromPreviousPayment: number;
    defBpFreqCode: string;
    defaultBpNumOfPaym: number;
    distanza: number;
    financialProductCode: string;
    mandatory: string;
    molteplicita: number;
    numberOfTimes: number;
    paymMetUpdatable: string;
    paymentDate: string;
    sequenceNumber: number;
    singleMultiple: string;
    supplyTypeCode: string;
    totalAmount: number;
    vatAmount: number;
    vatCode: string;
    vatPercentage: number;

}
