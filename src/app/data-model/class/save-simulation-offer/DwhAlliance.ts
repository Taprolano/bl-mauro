import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class DwhAlliance {

    private _allianceID: string;
    private _allianceName: string;

    get allianceID(): string {
        return this._allianceID;
    }

    set allianceID(value: string) {
        this._allianceID = value;
    }

    get allianceName(): string {
        return this._allianceName;
    }

    set allianceName(value: string) {
        this._allianceName = value;
    }
}

export class SaveDealerOfferDwhAllianceFactory {

    static getInstance(obj: DwhAlliance): DwhAlliance {

        let utils: UtilsService = new UtilsService();
        let dwh = new DwhAlliance();

        if (obj) {
            if (!utils.isUndefided(obj.allianceID)) dwh.allianceID = obj.allianceID;
            if (!utils.isUndefided(obj.allianceName)) dwh.allianceName = obj.allianceName;
        }

        return dwh;
    }

    static getInstanceParams(obj: DwhAlliance): Object {
        let resp = {
                allianceID: obj.allianceID,
                allianceName: obj.allianceName
            };

        return resp;
    }

    getInstanceList(list: any): DwhAlliance[] {
        let dwhList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                dwhList.push(SaveDealerOfferDwhAllianceFactory.getInstance(elem));
            });
        }
        return dwhList;
    }

}
