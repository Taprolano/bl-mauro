import {UtilsService} from '../../../shared/services/utils.service';

export class SupplyServOption {

    private _customerAmount: number;
    private _customerCostDiscount: number;
    private _dealerContribPerc: number;
    private _dealerFixContrib: number;
    private _fixedCost: number;
    private _manuContribPerc: number;
    private _manuFixedContrib: number;
    private _mbiContribPerc: number;
    private _mbiFixedContrib: number;
    private _provvDiscount: number;


    get customerAmount(): number {
        return this._customerAmount;
    }

    set customerAmount(value: number) {
        this._customerAmount = value;
    }

    get customerCostDiscount(): number {
        return this._customerCostDiscount;
    }

    set customerCostDiscount(value: number) {
        this._customerCostDiscount = value;
    }

    get dealerContribPerc(): number {
        return this._dealerContribPerc;
    }

    set dealerContribPerc(value: number) {
        this._dealerContribPerc = value;
    }

    get dealerFixContrib(): number {
        return this._dealerFixContrib;
    }

    set dealerFixContrib(value: number) {
        this._dealerFixContrib = value;
    }

    get fixedCost(): number {
        return this._fixedCost;
    }

    set fixedCost(value: number) {
        this._fixedCost = value;
    }

    get manuContribPerc(): number {
        return this._manuContribPerc;
    }

    set manuContribPerc(value: number) {
        this._manuContribPerc = value;
    }

    get manuFixedContrib(): number {
        return this._manuFixedContrib;
    }

    set manuFixedContrib(value: number) {
        this._manuFixedContrib = value;
    }

    get mbiContribPerc(): number {
        return this._mbiContribPerc;
    }

    set mbiContribPerc(value: number) {
        this._mbiContribPerc = value;
    }

    get mbiFixedContrib(): number {
        return this._mbiFixedContrib;
    }

    set mbiFixedContrib(value: number) {
        this._mbiFixedContrib = value;
    }

    get provvDiscount(): number {
        return this._provvDiscount;
    }

    set provvDiscount(value: number) {
        this._provvDiscount = value;
    }
}

export class SaveDealerOfferSupplyServOptionFactory {

    static getInstance(obj: SupplyServOption): SupplyServOption {

        let utils: UtilsService = new UtilsService();
        let option = new SupplyServOption();

        if (obj) {
            if (!utils.isUndefided(obj.customerAmount)) option.customerAmount = obj.customerAmount;
            if (!utils.isUndefided(obj.customerCostDiscount)) option.customerCostDiscount = obj.customerCostDiscount;
            if (!utils.isUndefided(obj.dealerContribPerc)) option.dealerContribPerc = obj.dealerContribPerc;
            if (!utils.isUndefided(obj.dealerFixContrib)) option.dealerFixContrib = obj.dealerFixContrib;
            if (!utils.isUndefided(obj.fixedCost)) option.fixedCost = obj.fixedCost;
            if (!utils.isUndefided(obj.manuContribPerc)) option.manuContribPerc = obj.manuContribPerc;
            if (!utils.isUndefided(obj.manuFixedContrib)) option.manuFixedContrib = obj.manuFixedContrib;
            if (!utils.isUndefided(obj.mbiContribPerc)) option.mbiContribPerc = obj.mbiContribPerc;
            if (!utils.isUndefided(obj.mbiFixedContrib)) option.mbiFixedContrib = obj.mbiFixedContrib;
            if (!utils.isUndefided(obj.provvDiscount)) option.provvDiscount = obj.provvDiscount;
        }

        return option;
    }

    static getInstanceParams(obj: SupplyServOption): ISupplyServOption {

        let requestBody: ISupplyServOption = {
            customerAmount: obj.customerAmount,
            customerCostDiscount: obj.customerCostDiscount,
            dealerContribPerc: obj.dealerContribPerc,
            dealerFixContrib: obj.dealerFixContrib,
            fixedCost: obj.fixedCost,
            manuContribPerc: obj.manuContribPerc,
            manuFixedContrib: obj.manuFixedContrib,
            mbiContribPerc: obj.mbiContribPerc,
            mbiFixedContrib: obj.mbiFixedContrib,
            provvDiscount: obj.provvDiscount
        };

        return requestBody;
    }

    getInstanceList(list: any): SupplyServOption[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferSupplyServOptionFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

}

export interface ISupplyServOption {

    customerAmount: number;
    customerCostDiscount: number;
    dealerContribPerc: number;
    dealerFixContrib: number;
    fixedCost: number;
    manuContribPerc: number;
    manuFixedContrib: number;
    mbiContribPerc: number;
    mbiFixedContrib: number;
    provvDiscount: number;

}
