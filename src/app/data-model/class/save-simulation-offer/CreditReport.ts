import {UtilsService} from '../../../shared/services/utils.service';
import {DwhAlliance, SaveDealerOfferDwhAllianceFactory} from './DwhAlliance';
import {SaveDealerOfferSupplyServOptionFactory} from './SupplyServOption';
import {ContractDet} from './ContractDet';

export class CreditReport {

    private _clientExposure: number;
    private _customerGroup: DwhAlliance;
    private _exposure: number;
    private _exposureMBFSI: number;
    private _guarrantorGroup: DwhAlliance;
    private _multipleGroup: boolean;
    private _notInFeeUnpaidAmount: number;
    private _rentalClientExposure: number;
    private _rentalExposure: number;
    private _rentalUnpaidExposure: number;
    private _unpaidClientMBFSI: number;
    private _unpaidClientRental: number;


    get clientExposure(): number {
        return this._clientExposure;
    }

    set clientExposure(value: number) {
        this._clientExposure = value;
    }

    get customerGroup(): DwhAlliance {
        return this._customerGroup;
    }

    set customerGroup(value: DwhAlliance) {
        this._customerGroup = value;
    }

    get exposure(): number {
        return this._exposure;
    }

    set exposure(value: number) {
        this._exposure = value;
    }

    get exposureMBFSI(): number {
        return this._exposureMBFSI;
    }

    set exposureMBFSI(value: number) {
        this._exposureMBFSI = value;
    }

    get guarrantorGroup(): DwhAlliance {
        return this._guarrantorGroup;
    }

    set guarrantorGroup(value: DwhAlliance) {
        this._guarrantorGroup = value;
    }

    get multipleGroup(): boolean {
        return this._multipleGroup;
    }

    set multipleGroup(value: boolean) {
        this._multipleGroup = value;
    }

    get notInFeeUnpaidAmount(): number {
        return this._notInFeeUnpaidAmount;
    }

    set notInFeeUnpaidAmount(value: number) {
        this._notInFeeUnpaidAmount = value;
    }

    get rentalClientExposure(): number {
        return this._rentalClientExposure;
    }

    set rentalClientExposure(value: number) {
        this._rentalClientExposure = value;
    }

    get rentalExposure(): number {
        return this._rentalExposure;
    }

    set rentalExposure(value: number) {
        this._rentalExposure = value;
    }

    get rentalUnpaidExposure(): number {
        return this._rentalUnpaidExposure;
    }

    set rentalUnpaidExposure(value: number) {
        this._rentalUnpaidExposure = value;
    }

    get unpaidClientMBFSI(): number {
        return this._unpaidClientMBFSI;
    }

    set unpaidClientMBFSI(value: number) {
        this._unpaidClientMBFSI = value;
    }

    get unpaidClientRental(): number {
        return this._unpaidClientRental;
    }

    set unpaidClientRental(value: number) {
        this._unpaidClientRental = value;
    }
}

export class SaveDealerOfferCreditReportFactory {

    static getInstance(obj: CreditReport): CreditReport {

        let utils: UtilsService = new UtilsService();
        let report = new CreditReport();

        if (obj) {

            let customerGroup = SaveDealerOfferDwhAllianceFactory.getInstance(obj.customerGroup);
            let guarrantorGroup = SaveDealerOfferDwhAllianceFactory.getInstance(obj.guarrantorGroup);

            if (!utils.isUndefided(obj.clientExposure)) report.clientExposure = obj.clientExposure;
            if (!utils.isUndefided(customerGroup)) report.customerGroup = customerGroup;
            if (!utils.isUndefided(obj.exposure)) report.exposure = obj.exposure;
            if (!utils.isUndefided(obj.exposureMBFSI)) report.exposureMBFSI = obj.exposureMBFSI;
            if (!utils.isUndefided(guarrantorGroup)) report.guarrantorGroup = guarrantorGroup;
            if (!utils.isUndefided(obj.multipleGroup)) report.multipleGroup = obj.multipleGroup;
            if (!utils.isUndefided(obj.notInFeeUnpaidAmount)) report.notInFeeUnpaidAmount = obj.notInFeeUnpaidAmount;
            if (!utils.isUndefided(obj.rentalClientExposure)) report.rentalClientExposure = obj.rentalClientExposure;
            if (!utils.isUndefided(obj.rentalExposure)) report.rentalExposure = obj.rentalExposure;
            if (!utils.isUndefided(obj.rentalUnpaidExposure)) report.rentalUnpaidExposure = obj.rentalUnpaidExposure;
            if (!utils.isUndefided(obj.unpaidClientMBFSI)) report.unpaidClientMBFSI = obj.unpaidClientMBFSI;
            if (!utils.isUndefided(obj.unpaidClientRental)) report.unpaidClientRental = obj.unpaidClientRental;
        }

        return report;
    }

    static getInstanceParams(obj: CreditReport): Object {
        let resp = {
            clientExposure: obj.clientExposure,
            customerGroup: SaveDealerOfferDwhAllianceFactory.getInstanceParams(obj.customerGroup),
            exposure: obj.exposure,
            exposureMBFSI: obj.exposureMBFSI,
            guarrantorGroup: SaveDealerOfferDwhAllianceFactory.getInstanceParams(obj.guarrantorGroup),
            multipleGroup: obj.multipleGroup,
            notInFeeUnpaidAmount: obj.notInFeeUnpaidAmount,
            rentalClientExposure: obj.rentalClientExposure,
            rentalExposure: obj.rentalExposure,
            rentalUnpaidExposure: obj.rentalUnpaidExposure,
            unpaidClientMBFSI: obj.unpaidClientMBFSI,
            unpaidClientRental: obj.unpaidClientRental
        };

        return resp;
    }

    getInstanceList(list: any): CreditReport[] {
        let reportList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                reportList.push(SaveDealerOfferCreditReportFactory.getInstance(elem));
            });
        }
        return reportList;
    }

}
