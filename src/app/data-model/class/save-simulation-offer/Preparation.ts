import {UtilsService} from '../../../shared/services/utils.service';
import {IFinancialOptionals} from '../FinancialOptionals';

export class Preparation {

    private _netCost: number;
    private _vat: number;


    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    get vat(): number {
        return this._vat;
    }

    set vat(value: number) {
        this._vat = value;
    }

    public requestParameters(): IPreparation {
        let requestBody: IPreparation = {
            netCost: this._netCost,
            vat: this._vat
        };

        return requestBody;
    }

}


export class PreparationFactory {

    static getInstance(obj: Preparation): Preparation {

        let utils: UtilsService = new UtilsService();
        let preparation = new Preparation();

        if (obj) {
            if (!utils.isUndefided(obj.netCost)) preparation.netCost = obj.netCost;
            if (!utils.isUndefided(obj.vat)) preparation.vat = obj.vat;
        }

        return preparation;
    }


    static getInstanceList(list: any): Preparation[] {
        let preparationList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                preparationList.push(PreparationFactory.getInstance(elem));
            });
        }
        return preparationList;
    }

    static getInstanceListParam(list: any): IPreparation[] {
        let preparationList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                preparationList.push(this.getInstance(elem).requestParameters());
            });
        }
        return preparationList;
    }

}

export interface IPreparation {

    netCost: number;
    vat: number;

}