import {UtilsService} from '../../../shared/services/utils.service';
import {ISaveSimulationOfferOptionals} from './Optional';

export class ContactInfo {

    private _code: number;
    private _contactInfo: string;
    private _type: string; // enum


    get code(): number {
        return this._code;
    }

    set code(value: number) {
        this._code = value;
    }

    get contactInfo(): string {
        return this._contactInfo;
    }

    set contactInfo(value: string) {
        this._contactInfo = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    public requestParameters(): IContactInfo {
        let requestBody: IContactInfo = {
            code: this._code,
            contactInfo: this._contactInfo,
            type: this._type
        };

        return requestBody;
    }

}

export class SaveDealerOfferContactInfoFactory {

    static getInstance(obj: ContactInfo): ContactInfo {

        let utils: UtilsService = new UtilsService();
        let optional = new ContactInfo();

        if (obj) {
            if (!utils.isUndefided(obj.code)) optional.code = obj.code;
            if (!utils.isUndefided(obj.contactInfo)) optional.contactInfo = obj.contactInfo;
            if (!utils.isUndefided(obj.type)) optional.type = obj.type;
        }

        return optional;
    }

    static getInstanceListParam(list: any): ContactInfo[] {
        let contactInfoList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                contactInfoList.push(this.getInstance(elem).requestParameters());
            });
        }
        return contactInfoList;
    }


    getInstanceList(list: any): ContactInfo[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferContactInfoFactory.getInstance(elem));
            });
        }
        return optionalList;
    }

}

export interface IContactInfo {

    code: number;
    contactInfo: string;
    type: string;
}
