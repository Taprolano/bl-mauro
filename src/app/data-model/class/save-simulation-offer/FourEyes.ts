import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class FourEyes {

    private _approvalFlowCode: string;
    private _directorSignDerogation: boolean;
    private _directorSignature: boolean;
    private _mls: boolean;
    private _secondApprovalCode: number;
    private _secondApprovalID: string;
    private _secondApprovalRequested: string;
    private _sufficientApprovalAmount: boolean;


    get approvalFlowCode(): string {
        return this._approvalFlowCode;
    }

    set approvalFlowCode(value: string) {
        this._approvalFlowCode = value;
    }

    get directorSignDerogation(): boolean {
        return this._directorSignDerogation;
    }

    set directorSignDerogation(value: boolean) {
        this._directorSignDerogation = value;
    }

    get directorSignature(): boolean {
        return this._directorSignature;
    }

    set directorSignature(value: boolean) {
        this._directorSignature = value;
    }

    get mls(): boolean {
        return this._mls;
    }

    set mls(value: boolean) {
        this._mls = value;
    }

    get secondApprovalCode(): number {
        return this._secondApprovalCode;
    }

    set secondApprovalCode(value: number) {
        this._secondApprovalCode = value;
    }

    get secondApprovalID(): string {
        return this._secondApprovalID;
    }

    set secondApprovalID(value: string) {
        this._secondApprovalID = value;
    }

    get secondApprovalRequested(): string {
        return this._secondApprovalRequested;
    }

    set secondApprovalRequested(value: string) {
        this._secondApprovalRequested = value;
    }

    get sufficientApprovalAmount(): boolean {
        return this._sufficientApprovalAmount;
    }

    set sufficientApprovalAmount(value: boolean) {
        this._sufficientApprovalAmount = value;
    }
}

export class SaveDealerOfferFourEyesFactory {

    static getInstance(obj: FourEyes): FourEyes {

        let utils: UtilsService = new UtilsService();
        let fourEyes = new FourEyes();

        if (obj) {
            if (!utils.isUndefided(obj.approvalFlowCode)) fourEyes.approvalFlowCode = obj.approvalFlowCode;
            if (!utils.isUndefided(obj.directorSignDerogation)) fourEyes.directorSignDerogation = obj.directorSignDerogation;
            if (!utils.isUndefided(obj.directorSignature)) fourEyes.directorSignature = obj.directorSignature;
            if (!utils.isUndefided(obj.mls)) fourEyes.mls = obj.mls;
            if (!utils.isUndefided(obj.secondApprovalCode)) fourEyes.secondApprovalCode = obj.secondApprovalCode;
            if (!utils.isUndefided(obj.secondApprovalID)) fourEyes.secondApprovalID = obj.secondApprovalID;
            if (!utils.isUndefided(obj.secondApprovalRequested)) fourEyes.secondApprovalRequested = obj.secondApprovalRequested;
            if (!utils.isUndefided(obj.sufficientApprovalAmount)) fourEyes.sufficientApprovalAmount = obj.sufficientApprovalAmount;
        }

        return fourEyes;
    }

    static getInstanceParams(obj: FourEyes): Object {
        let resp = {
            approvalFlowCode: obj.approvalFlowCode,
            directorSignDerogation: obj.directorSignDerogation,
            directorSignature: obj.directorSignature,
            mls: obj.mls,
            secondApprovalCode: obj.secondApprovalCode,
            secondApprovalID: obj.secondApprovalID,
            secondApprovalRequested: obj.secondApprovalRequested,
            sufficientApprovalAmount: obj.sufficientApprovalAmount
        };

        return resp;
    }

    getInstanceList(list: any): FourEyes[] {
        let fourEyesList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                fourEyesList.push(SaveDealerOfferFourEyesFactory.getInstance(elem));
            });
        }
        return fourEyesList;
    }

}
