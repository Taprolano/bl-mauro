import {UtilsService} from '../../../shared/services/utils.service';
import {CommercialProposalSaveSimulationOffer, SaveDealerOfferCommPropFactory} from './CommercialProposalSaveSimulationOffer';
import {CreditReport, SaveDealerOfferCreditReportFactory} from './CreditReport';
import {FinancialProposalSaveSimulationOffer, SaveDealerOfferFinancialProposalFactory} from './FinancialProposalSaveSimulationOffer';
import {K2K, SaveDealerOfferK2KFactory} from './K2K';
import {Optional, SaveDealerOfferOptionalFactory} from './Optional';
import {ProposalAddInfSaveSimulationOffer, SaveDealerOfferProposalAddInfFactory} from './ProposalAddInfSaveSimulationOffer';
import {CashFlow, SaveDealerOfferCashFlowFactory} from './CashFlow';
import {SecondPeriodData, SaveDealerOfferSecondPeriodDataFactory} from './SecondPeriodData';
import {SubjectSaveSimulationOffer, SaveDealerOfferSubjectFactory} from './SubjectSaveSimulationOffer';

export class SaveSimulationOfferProposal {

    private _anticipoPercentuale: number;
    private _appRejCode: number;
    private _approvalExpiryDate: string; // data
    private _assegnatario: string;
    private _assetClassCode: string;
    private _assetType: string;
    private _attivateOggiPauout: boolean;
    private _baumuster: string;
    private _brandId: string;
    private _campaignCode: string;
    private _cancellationCode: string;
    private _cardType: string;
    private _casStatus: string; // enum
    private _causaleSospensione: string;
    private _cds: string;
    private _clickAndGo: boolean;
    private _clickAndGoFinalized: boolean;
    private _codPostazione: number;
    private _codSocieta: number;
    private _codTerzi: number;
    private _codeIpoteca: string;
    private _codiceMetodoPagamentoAnticipo: string;
    private _codiceMetodoPagamentoBalloon: string;
    private _codiceMetodoPagamentoRate: string;
    private _commercial: CommercialProposalSaveSimulationOffer;
    private _concessionario: string;
    private _contractLength: number;
    private _craError: boolean;
    private _creditReport: CreditReport;
    private _currencyCode: string;
    private _dataScadenzaClausolaSospensiva: string; // data
    private _dccpay: boolean;
    private _dealerCDTer: number;
    private _dealerTypeCode: string;
    private _descStatoProposta: string;
    private _disableAutomaticAccept: boolean;
    private _distance: number;
    private _earlyNew: boolean;
    private _esposizione: number;
    private _financedTotalAmount: number;
    private _financial: FinancialProposalSaveSimulationOffer;
    private _financialProductCode: string;
    private _financialProposalDate: string; // data
    private _flagStampa: boolean;
    private _forceDate: boolean;
    private _frequency: number;
    private _graphometric: boolean;
    private _guarantorPrivacy: boolean;
    private _idProposal: string;
    private _importoAnticipo: number;
    private _importoRataBalloon: number;
    private _invoiceRequest: boolean;
    private _k2K: boolean;
    private _k2kInfo: K2K;
    private _lastApprovalId: string;
    private _lastCredAppId: string;
    private _lastFinancialCalculationDate: string; // data
    private _lastNote: string;
    private _leasing: boolean;
    private _numDocUncompleted: number;
    private _numNotes: number;
    private _numeroContratto: string;
    private _optionals: Optional[];
    private _origSystemCode: string;
    private _pendingStatusCode: number;
    private _productLineCode: string;
    private _propFinanziamento: boolean;
    private _propStatusDescr: string;
    private _propSuspend: boolean;
    private _propSuspendPayout: boolean;
    private _propaddinf: ProposalAddInfSaveSimulationOffer;
    private _proposalDate: string; // data
    private _proposalStatus: string; // enum
    private _provisionalApproval: boolean;
    private _rate: CashFlow[];
    private _rental: boolean;
    private _residualValue: number;
    private _resistDocumentRequest: string;
    private _retailCorporate: string;
    private _riproposta: boolean;
    private _sabatiniSelected: boolean;
    private _secondPeriodData: SecondPeriodData;
    private _secondaAppravazione: boolean;
    private _sellerCDTer: number;
    private _speseDiIstruttoria: number;
    private _subjects: SubjectSaveSimulationOffer[];
    private _suspectBehaviour: string;
    private _taeg: number;
    private _tan: number;
    private _tipoInteresse: string;
    private _tipoPianoPagamenti: string;
    private _tipoProposta: string;
    private _tipoVoucher: string;
    private _totalAmount: number;
    private _type: string;
    private _typeClickAndGo: string;
    private _typeDeroga: string;
    private _updateUser: string;
    private _urgente: boolean;
    private _used: boolean;
    private _utenteSospensione: string;
    private _vatPercentage: number;
    private _version: number;
    private _visibleAnticipo: boolean;
    private _visibleBalloon: boolean;


    get anticipoPercentuale(): number {
        return this._anticipoPercentuale;
    }

    set anticipoPercentuale(value: number) {
        this._anticipoPercentuale = value;
    }

    get appRejCode(): number {
        return this._appRejCode;
    }

    set appRejCode(value: number) {
        this._appRejCode = value;
    }

    get approvalExpiryDate(): string {
        return this._approvalExpiryDate;
    }

    set approvalExpiryDate(value: string) {
        this._approvalExpiryDate = value;
    }

    get assegnatario(): string {
        return this._assegnatario;
    }

    set assegnatario(value: string) {
        this._assegnatario = value;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    get assetType(): string {
        return this._assetType;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    get attivateOggiPauout(): boolean {
        return this._attivateOggiPauout;
    }

    set attivateOggiPauout(value: boolean) {
        this._attivateOggiPauout = value;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    get brandId(): string {
        return this._brandId;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    get campaignCode(): string {
        return this._campaignCode;
    }

    set campaignCode(value: string) {
        this._campaignCode = value;
    }

    get cancellationCode(): string {
        return this._cancellationCode;
    }

    set cancellationCode(value: string) {
        this._cancellationCode = value;
    }

    get cardType(): string {
        return this._cardType;
    }

    set cardType(value: string) {
        this._cardType = value;
    }

    get casStatus(): string {
        return this._casStatus;
    }

    set casStatus(value: string) {
        this._casStatus = value;
    }

    get causaleSospensione(): string {
        return this._causaleSospensione;
    }

    set causaleSospensione(value: string) {
        this._causaleSospensione = value;
    }

    get cds(): string {
        return this._cds;
    }

    set cds(value: string) {
        this._cds = value;
    }

    get clickAndGo(): boolean {
        return this._clickAndGo;
    }

    set clickAndGo(value: boolean) {
        this._clickAndGo = value;
    }

    get clickAndGoFinalized(): boolean {
        return this._clickAndGoFinalized;
    }

    set clickAndGoFinalized(value: boolean) {
        this._clickAndGoFinalized = value;
    }

    get codPostazione(): number {
        return this._codPostazione;
    }

    set codPostazione(value: number) {
        this._codPostazione = value;
    }

    get codSocieta(): number {
        return this._codSocieta;
    }

    set codSocieta(value: number) {
        this._codSocieta = value;
    }

    get codTerzi(): number {
        return this._codTerzi;
    }

    set codTerzi(value: number) {
        this._codTerzi = value;
    }

    get codeIpoteca(): string {
        return this._codeIpoteca;
    }

    set codeIpoteca(value: string) {
        this._codeIpoteca = value;
    }

    get codiceMetodoPagamentoAnticipo(): string {
        return this._codiceMetodoPagamentoAnticipo;
    }

    set codiceMetodoPagamentoAnticipo(value: string) {
        this._codiceMetodoPagamentoAnticipo = value;
    }

    get codiceMetodoPagamentoBalloon(): string {
        return this._codiceMetodoPagamentoBalloon;
    }

    set codiceMetodoPagamentoBalloon(value: string) {
        this._codiceMetodoPagamentoBalloon = value;
    }

    get codiceMetodoPagamentoRate(): string {
        return this._codiceMetodoPagamentoRate;
    }

    set codiceMetodoPagamentoRate(value: string) {
        this._codiceMetodoPagamentoRate = value;
    }

    get commercial(): CommercialProposalSaveSimulationOffer {
        return this._commercial;
    }

    set commercial(value: CommercialProposalSaveSimulationOffer) {
        this._commercial = value;
    }

    get concessionario(): string {
        return this._concessionario;
    }

    set concessionario(value: string) {
        this._concessionario = value;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    get craError(): boolean {
        return this._craError;
    }

    set craError(value: boolean) {
        this._craError = value;
    }

    get creditReport(): CreditReport {
        return this._creditReport;
    }

    set creditReport(value: CreditReport) {
        this._creditReport = value;
    }

    get currencyCode(): string {
        return this._currencyCode;
    }

    set currencyCode(value: string) {
        this._currencyCode = value;
    }

    get dataScadenzaClausolaSospensiva(): string {
        return this._dataScadenzaClausolaSospensiva;
    }

    set dataScadenzaClausolaSospensiva(value: string) {
        this._dataScadenzaClausolaSospensiva = value;
    }

    get dccpay(): boolean {
        return this._dccpay;
    }

    set dccpay(value: boolean) {
        this._dccpay = value;
    }

    get dealerCDTer(): number {
        return this._dealerCDTer;
    }

    set dealerCDTer(value: number) {
        this._dealerCDTer = value;
    }

    get dealerTypeCode(): string {
        return this._dealerTypeCode;
    }

    set dealerTypeCode(value: string) {
        this._dealerTypeCode = value;
    }

    get descStatoProposta(): string {
        return this._descStatoProposta;
    }

    set descStatoProposta(value: string) {
        this._descStatoProposta = value;
    }

    get disableAutomaticAccept(): boolean {
        return this._disableAutomaticAccept;
    }

    set disableAutomaticAccept(value: boolean) {
        this._disableAutomaticAccept = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get earlyNew(): boolean {
        return this._earlyNew;
    }

    set earlyNew(value: boolean) {
        this._earlyNew = value;
    }

    get esposizione(): number {
        return this._esposizione;
    }

    set esposizione(value: number) {
        this._esposizione = value;
    }

    get financedTotalAmount(): number {
        return this._financedTotalAmount;
    }

    set financedTotalAmount(value: number) {
        this._financedTotalAmount = value;
    }

    get financial(): FinancialProposalSaveSimulationOffer {
        return this._financial;
    }

    set financial(value: FinancialProposalSaveSimulationOffer) {
        this._financial = value;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }

    get financialProposalDate(): string {
        return this._financialProposalDate;
    }

    set financialProposalDate(value: string) {
        this._financialProposalDate = value;
    }

    get flagStampa(): boolean {
        return this._flagStampa;
    }

    set flagStampa(value: boolean) {
        this._flagStampa = value;
    }

    get forceDate(): boolean {
        return this._forceDate;
    }

    set forceDate(value: boolean) {
        this._forceDate = value;
    }

    get frequency(): number {
        return this._frequency;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    get graphometric(): boolean {
        return this._graphometric;
    }

    set graphometric(value: boolean) {
        this._graphometric = value;
    }

    get guarantorPrivacy(): boolean {
        return this._guarantorPrivacy;
    }

    set guarantorPrivacy(value: boolean) {
        this._guarantorPrivacy = value;
    }

    get idProposal(): string {
        return this._idProposal;
    }

    set idProposal(value: string) {
        this._idProposal = value;
    }

    get importoAnticipo(): number {
        return this._importoAnticipo;
    }

    set importoAnticipo(value: number) {
        this._importoAnticipo = value;
    }

    get importoRataBalloon(): number {
        return this._importoRataBalloon;
    }

    set importoRataBalloon(value: number) {
        this._importoRataBalloon = value;
    }

    get invoiceRequest(): boolean {
        return this._invoiceRequest;
    }

    set invoiceRequest(value: boolean) {
        this._invoiceRequest = value;
    }

    get k2K(): boolean {
        return this._k2K;
    }

    set k2K(value: boolean) {
        this._k2K = value;
    }

    get k2kInfo(): K2K {
        return this._k2kInfo;
    }

    set k2kInfo(value: K2K) {
        this._k2kInfo = value;
    }

    get lastApprovalId(): string {
        return this._lastApprovalId;
    }

    set lastApprovalId(value: string) {
        this._lastApprovalId = value;
    }

    get lastCredAppId(): string {
        return this._lastCredAppId;
    }

    set lastCredAppId(value: string) {
        this._lastCredAppId = value;
    }

    get lastFinancialCalculationDate(): string {
        return this._lastFinancialCalculationDate;
    }

    set lastFinancialCalculationDate(value: string) {
        this._lastFinancialCalculationDate = value;
    }

    get lastNote(): string {
        return this._lastNote;
    }

    set lastNote(value: string) {
        this._lastNote = value;
    }

    get leasing(): boolean {
        return this._leasing;
    }

    set leasing(value: boolean) {
        this._leasing = value;
    }

    get numDocUncompleted(): number {
        return this._numDocUncompleted;
    }

    set numDocUncompleted(value: number) {
        this._numDocUncompleted = value;
    }

    get numNotes(): number {
        return this._numNotes;
    }

    set numNotes(value: number) {
        this._numNotes = value;
    }

    get numeroContratto(): string {
        return this._numeroContratto;
    }

    set numeroContratto(value: string) {
        this._numeroContratto = value;
    }

    get optionals(): Optional[] {
        return this._optionals;
    }

    set optionals(value: Optional[]) {
        this._optionals = value;
    }

    get origSystemCode(): string {
        return this._origSystemCode;
    }

    set origSystemCode(value: string) {
        this._origSystemCode = value;
    }

    get pendingStatusCode(): number {
        return this._pendingStatusCode;
    }

    set pendingStatusCode(value: number) {
        this._pendingStatusCode = value;
    }

    get productLineCode(): string {
        return this._productLineCode;
    }

    set productLineCode(value: string) {
        this._productLineCode = value;
    }

    get propFinanziamento(): boolean {
        return this._propFinanziamento;
    }

    set propFinanziamento(value: boolean) {
        this._propFinanziamento = value;
    }

    get propStatusDescr(): string {
        return this._propStatusDescr;
    }

    set propStatusDescr(value: string) {
        this._propStatusDescr = value;
    }

    get propSuspend(): boolean {
        return this._propSuspend;
    }

    set propSuspend(value: boolean) {
        this._propSuspend = value;
    }

    get propSuspendPayout(): boolean {
        return this._propSuspendPayout;
    }

    set propSuspendPayout(value: boolean) {
        this._propSuspendPayout = value;
    }

    get propaddinf(): ProposalAddInfSaveSimulationOffer {
        return this._propaddinf;
    }

    set propaddinf(value: ProposalAddInfSaveSimulationOffer) {
        this._propaddinf = value;
    }

    get proposalDate(): string {
        return this._proposalDate;
    }

    set proposalDate(value: string) {
        this._proposalDate = value;
    }

    get proposalStatus(): string {
        return this._proposalStatus;
    }

    set proposalStatus(value: string) {
        this._proposalStatus = value;
    }

    get provisionalApproval(): boolean {
        return this._provisionalApproval;
    }

    set provisionalApproval(value: boolean) {
        this._provisionalApproval = value;
    }

    get rate(): CashFlow[] {
        return this._rate;
    }

    set rate(value: CashFlow[]) {
        this._rate = value;
    }

    get rental(): boolean {
        return this._rental;
    }

    set rental(value: boolean) {
        this._rental = value;
    }

    get residualValue(): number {
        return this._residualValue;
    }

    set residualValue(value: number) {
        this._residualValue = value;
    }

    get resistDocumentRequest(): string {
        return this._resistDocumentRequest;
    }

    set resistDocumentRequest(value: string) {
        this._resistDocumentRequest = value;
    }

    get retailCorporate(): string {
        return this._retailCorporate;
    }

    set retailCorporate(value: string) {
        this._retailCorporate = value;
    }

    get riproposta(): boolean {
        return this._riproposta;
    }

    set riproposta(value: boolean) {
        this._riproposta = value;
    }

    get sabatiniSelected(): boolean {
        return this._sabatiniSelected;
    }

    set sabatiniSelected(value: boolean) {
        this._sabatiniSelected = value;
    }

    get secondPeriodData(): SecondPeriodData {
        return this._secondPeriodData;
    }

    set secondPeriodData(value: SecondPeriodData) {
        this._secondPeriodData = value;
    }

    get secondaAppravazione(): boolean {
        return this._secondaAppravazione;
    }

    set secondaAppravazione(value: boolean) {
        this._secondaAppravazione = value;
    }

    get sellerCDTer(): number {
        return this._sellerCDTer;
    }

    set sellerCDTer(value: number) {
        this._sellerCDTer = value;
    }

    get speseDiIstruttoria(): number {
        return this._speseDiIstruttoria;
    }

    set speseDiIstruttoria(value: number) {
        this._speseDiIstruttoria = value;
    }

    get subjects(): SubjectSaveSimulationOffer[] {
        return this._subjects;
    }

    set subjects(value: SubjectSaveSimulationOffer[]) {
        this._subjects = value;
    }

    get suspectBehaviour(): string {
        return this._suspectBehaviour;
    }

    set suspectBehaviour(value: string) {
        this._suspectBehaviour = value;
    }

    get taeg(): number {
        return this._taeg;
    }

    set taeg(value: number) {
        this._taeg = value;
    }

    get tan(): number {
        return this._tan;
    }

    set tan(value: number) {
        this._tan = value;
    }

    get tipoInteresse(): string {
        return this._tipoInteresse;
    }

    set tipoInteresse(value: string) {
        this._tipoInteresse = value;
    }

    get tipoPianoPagamenti(): string {
        return this._tipoPianoPagamenti;
    }

    set tipoPianoPagamenti(value: string) {
        this._tipoPianoPagamenti = value;
    }

    get tipoProposta(): string {
        return this._tipoProposta;
    }

    set tipoProposta(value: string) {
        this._tipoProposta = value;
    }

    get tipoVoucher(): string {
        return this._tipoVoucher;
    }

    set tipoVoucher(value: string) {
        this._tipoVoucher = value;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get typeClickAndGo(): string {
        return this._typeClickAndGo;
    }

    set typeClickAndGo(value: string) {
        this._typeClickAndGo = value;
    }

    get typeDeroga(): string {
        return this._typeDeroga;
    }

    set typeDeroga(value: string) {
        this._typeDeroga = value;
    }

    get updateUser(): string {
        return this._updateUser;
    }

    set updateUser(value: string) {
        this._updateUser = value;
    }

    get urgente(): boolean {
        return this._urgente;
    }

    set urgente(value: boolean) {
        this._urgente = value;
    }

    get used(): boolean {
        return this._used;
    }

    set used(value: boolean) {
        this._used = value;
    }

    get utenteSospensione(): string {
        return this._utenteSospensione;
    }

    set utenteSospensione(value: string) {
        this._utenteSospensione = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    get version(): number {
        return this._version;
    }

    set version(value: number) {
        this._version = value;
    }

    get visibleAnticipo(): boolean {
        return this._visibleAnticipo;
    }

    set visibleAnticipo(value: boolean) {
        this._visibleAnticipo = value;
    }

    get visibleBalloon(): boolean {
        return this._visibleBalloon;
    }

    set visibleBalloon(value: boolean) {
        this._visibleBalloon = value;
    }
}

export class SaveDealerOfferProposalFactory {

    static getInstance(obj: SaveSimulationOfferProposal): SaveSimulationOfferProposal {

        let optionals = SaveDealerOfferOptionalFactory.getInstanceList(obj.optionals);

        let rateFactory = new SaveDealerOfferCashFlowFactory();
        let rate = rateFactory.getInstanceList(obj.rate);

        let subjectsFactory = new SaveDealerOfferSubjectFactory();
        let subjects = subjectsFactory.getInstanceList(obj.subjects);

        let commercial = SaveDealerOfferCommPropFactory.getInstance(obj.commercial);
        let creditReport = SaveDealerOfferCreditReportFactory.getInstance(obj.creditReport);
        let financial = SaveDealerOfferFinancialProposalFactory.getInstance(obj.financial);
        let k2kInfo = SaveDealerOfferK2KFactory.getInstance(obj.k2kInfo);
        let propaddinf = SaveDealerOfferProposalAddInfFactory.getInstance(obj.propaddinf);
        let secondPeriodData = SaveDealerOfferSecondPeriodDataFactory.getInstance(obj.secondPeriodData);

        let utils: UtilsService = new UtilsService();
        let prop = new SaveSimulationOfferProposal();

        if (obj) {
            if (!utils.isUndefided(obj.anticipoPercentuale)) prop.anticipoPercentuale = obj.anticipoPercentuale;
            if (!utils.isUndefided(obj.appRejCode)) prop.appRejCode = obj.appRejCode;
            if (!utils.isUndefided(obj.approvalExpiryDate)) prop.approvalExpiryDate = obj.approvalExpiryDate;
            if (!utils.isUndefided(obj.assegnatario)) prop.assegnatario = obj.assegnatario;
            if (!utils.isUndefided(obj.assetClassCode)) prop.assetClassCode = obj.assetClassCode;
            if (!utils.isUndefided(obj.assetType)) prop.assetType = obj.assetType;
            if (!utils.isUndefided(obj.attivateOggiPauout)) prop.attivateOggiPauout = obj.attivateOggiPauout;
            if (!utils.isUndefided(obj.baumuster)) prop.baumuster = obj.baumuster;
            if (!utils.isUndefided(obj.brandId)) prop.brandId = obj.brandId;
            if (!utils.isUndefided(obj.campaignCode)) prop.campaignCode = obj.campaignCode;
            if (!utils.isUndefided(obj.cancellationCode)) prop.cancellationCode = obj.cancellationCode;
            if (!utils.isUndefided(obj.cardType)) prop.cardType = obj.cardType;
            if (!utils.isUndefided(obj.casStatus)) prop.casStatus = obj.casStatus;
            if (!utils.isUndefided(obj.causaleSospensione)) prop.causaleSospensione = obj.causaleSospensione;
            if (!utils.isUndefided(obj.cds)) prop.cds = obj.cds;
            if (!utils.isUndefided(obj.clickAndGo)) prop.clickAndGo = obj.clickAndGo;
            if (!utils.isUndefided(obj.clickAndGoFinalized)) prop.clickAndGoFinalized = obj.clickAndGoFinalized;
            if (!utils.isUndefided(obj.codPostazione)) prop.codPostazione = obj.codPostazione;
            if (!utils.isUndefided(obj.codSocieta)) prop.codSocieta = obj.codSocieta;
            if (!utils.isUndefided(obj.codTerzi)) prop.codTerzi = obj.codTerzi;
            if (!utils.isUndefided(obj.codeIpoteca)) prop.codeIpoteca = obj.codeIpoteca;
            if (!utils.isUndefided(obj.codiceMetodoPagamentoAnticipo)) prop.codiceMetodoPagamentoAnticipo = obj.codiceMetodoPagamentoAnticipo;
            if (!utils.isUndefided(obj.codiceMetodoPagamentoBalloon)) prop.codiceMetodoPagamentoBalloon = obj.codiceMetodoPagamentoBalloon;
            if (!utils.isUndefided(obj.codiceMetodoPagamentoRate)) prop.codiceMetodoPagamentoRate = obj.codiceMetodoPagamentoRate;
            if (!utils.isUndefided(commercial)) prop.commercial = commercial;
            if (!utils.isUndefided(obj.concessionario)) prop.concessionario = obj.concessionario;
            if (!utils.isUndefided(obj.contractLength)) prop.contractLength = obj.contractLength;
            if (!utils.isUndefided(obj.craError)) prop.craError = obj.craError;
            if (!utils.isUndefided(creditReport)) prop.creditReport = creditReport;
            if (!utils.isUndefided(obj.currencyCode)) prop.currencyCode = obj.currencyCode;
            if (!utils.isUndefided(obj.dataScadenzaClausolaSospensiva)) prop.dataScadenzaClausolaSospensiva = obj.dataScadenzaClausolaSospensiva;
            if (!utils.isUndefided(obj.dccpay)) prop.dccpay = obj.dccpay;
            if (!utils.isUndefided(obj.dealerCDTer)) prop.dealerCDTer = obj.dealerCDTer;
            if (!utils.isUndefided(obj.dealerTypeCode)) prop.dealerTypeCode = obj.dealerTypeCode;
            if (!utils.isUndefided(obj.descStatoProposta)) prop.descStatoProposta = obj.descStatoProposta;
            if (!utils.isUndefided(obj.disableAutomaticAccept)) prop.disableAutomaticAccept = obj.disableAutomaticAccept;
            if (!utils.isUndefided(obj.distance)) prop.distance = obj.distance;
            if (!utils.isUndefided(obj.earlyNew)) prop.earlyNew = obj.earlyNew;
            if (!utils.isUndefided(obj.esposizione)) prop.esposizione = obj.esposizione;
            if (!utils.isUndefided(obj.financedTotalAmount)) prop.financedTotalAmount = obj.financedTotalAmount;
            if (!utils.isUndefided(financial)) prop.financial = financial;
            if (!utils.isUndefided(obj.financialProductCode)) prop.financialProductCode = obj.financialProductCode;
            if (!utils.isUndefided(obj.financialProposalDate)) prop.financialProposalDate = obj.financialProposalDate;
            if (!utils.isUndefided(obj.flagStampa)) prop.flagStampa = obj.flagStampa;
            if (!utils.isUndefided(obj.forceDate)) prop.forceDate = obj.forceDate;
            if (!utils.isUndefided(obj.frequency)) prop.frequency = obj.frequency;
            if (!utils.isUndefided(obj.graphometric)) prop.graphometric = obj.graphometric;
            if (!utils.isUndefided(obj.guarantorPrivacy)) prop.guarantorPrivacy = obj.guarantorPrivacy;
            if (!utils.isUndefided(obj.idProposal)) prop.idProposal = obj.idProposal;
            if (!utils.isUndefided(obj.importoAnticipo)) prop.importoAnticipo = obj.importoAnticipo;
            if (!utils.isUndefided(obj.importoRataBalloon)) prop.importoRataBalloon = obj.importoRataBalloon;
            if (!utils.isUndefided(obj.invoiceRequest)) prop.invoiceRequest = obj.invoiceRequest;
            if (!utils.isUndefided(obj.k2K)) prop.k2K = obj.k2K;
            if (!utils.isUndefided(k2kInfo)) prop.k2kInfo = k2kInfo;
            if (!utils.isUndefided(obj.lastApprovalId)) prop.lastApprovalId = obj.lastApprovalId;
            if (!utils.isUndefided(obj.lastCredAppId)) prop.lastCredAppId = obj.lastCredAppId;
            if (!utils.isUndefided(obj.lastFinancialCalculationDate)) prop.lastFinancialCalculationDate = obj.lastFinancialCalculationDate;
            if (!utils.isUndefided(obj.lastNote)) prop.lastNote = obj.lastNote;
            if (!utils.isUndefided(obj.leasing)) prop.leasing = obj.leasing;
            if (!utils.isUndefided(obj.numDocUncompleted)) prop.numDocUncompleted = obj.numDocUncompleted;
            if (!utils.isUndefided(obj.numNotes)) prop.numNotes = obj.numNotes;
            if (!utils.isUndefided(obj.numeroContratto)) prop.numeroContratto = obj.numeroContratto;
            if (!utils.isUndefided(optionals)) prop.optionals = optionals;
            if (!utils.isUndefided(obj.origSystemCode)) prop.origSystemCode = obj.origSystemCode;
            if (!utils.isUndefided(obj.pendingStatusCode)) prop.pendingStatusCode = obj.pendingStatusCode;
            if (!utils.isUndefided(obj.productLineCode)) prop.productLineCode = obj.productLineCode;
            if (!utils.isUndefided(obj.propFinanziamento)) prop.propFinanziamento = obj.propFinanziamento;
            if (!utils.isUndefided(obj.propStatusDescr)) prop.propStatusDescr = obj.propStatusDescr;
            if (!utils.isUndefided(obj.propSuspend)) prop.propSuspend = obj.propSuspend;
            if (!utils.isUndefided(obj.propSuspendPayout)) prop.propSuspendPayout = obj.propSuspendPayout;
            if (!utils.isUndefided(propaddinf)) prop.propaddinf = propaddinf;
            if (!utils.isUndefided(obj.proposalDate)) prop.proposalDate = obj.proposalDate;
            if (!utils.isUndefided(obj.proposalStatus)) prop.proposalStatus = obj.proposalStatus;
            if (!utils.isUndefided(obj.provisionalApproval)) prop.provisionalApproval = obj.provisionalApproval;
            if (!utils.isUndefided(rate)) prop.rate = rate;
            if (!utils.isUndefided(obj.rental)) prop.rental = obj.rental;
            if (!utils.isUndefided(obj.residualValue)) prop.residualValue = obj.residualValue;
            if (!utils.isUndefided(obj.resistDocumentRequest)) prop.resistDocumentRequest = obj.resistDocumentRequest;
            if (!utils.isUndefided(obj.retailCorporate)) prop.retailCorporate = obj.retailCorporate;
            if (!utils.isUndefided(obj.riproposta)) prop.riproposta = obj.riproposta;
            if (!utils.isUndefided(obj.sabatiniSelected)) prop.sabatiniSelected = obj.sabatiniSelected;
            if (!utils.isUndefided(secondPeriodData)) prop.secondPeriodData = secondPeriodData;
            if (!utils.isUndefided(obj.secondaAppravazione)) prop.secondaAppravazione = obj.secondaAppravazione;
            if (!utils.isUndefided(obj.sellerCDTer)) prop.sellerCDTer = obj.sellerCDTer;
            if (!utils.isUndefided(obj.speseDiIstruttoria)) prop.speseDiIstruttoria = obj.speseDiIstruttoria;
            if (!utils.isUndefided(subjects)) prop.subjects = subjects;
            if (!utils.isUndefided(obj.suspectBehaviour)) prop.suspectBehaviour = obj.suspectBehaviour;
            if (!utils.isUndefided(obj.taeg)) prop.taeg = obj.taeg;
            if (!utils.isUndefided(obj.tan)) prop.tan = obj.tan;
            if (!utils.isUndefided(obj.tipoInteresse)) prop.tipoInteresse = obj.tipoInteresse;
            if (!utils.isUndefided(obj.tipoPianoPagamenti)) prop.tipoPianoPagamenti = obj.tipoPianoPagamenti;
            if (!utils.isUndefided(obj.tipoProposta)) prop.tipoProposta = obj.tipoProposta;
            if (!utils.isUndefided(obj.tipoVoucher)) prop.tipoVoucher = obj.tipoVoucher;
            if (!utils.isUndefided(obj.totalAmount)) prop.totalAmount = obj.totalAmount;
            if (!utils.isUndefided(obj.type)) prop.type = obj.type;
            if (!utils.isUndefided(obj.typeClickAndGo)) prop.typeClickAndGo = obj.typeClickAndGo;
            if (!utils.isUndefided(obj.typeDeroga)) prop.typeDeroga = obj.typeDeroga;
            if (!utils.isUndefided(obj.updateUser)) prop.updateUser = obj.updateUser;
            if (!utils.isUndefided(obj.urgente)) prop.urgente = obj.urgente;
            if (!utils.isUndefided(obj.used)) prop.used = obj.used;
            if (!utils.isUndefided(obj.utenteSospensione)) prop.utenteSospensione = obj.utenteSospensione;
            if (!utils.isUndefided(obj.vatPercentage)) prop.vatPercentage = obj.vatPercentage;
            if (!utils.isUndefided(obj.version)) prop.version = obj.version;
            if (!utils.isUndefided(obj.visibleAnticipo)) prop.visibleAnticipo = obj.visibleAnticipo;
            if (!utils.isUndefided(obj.visibleBalloon)) prop.visibleBalloon = obj.visibleBalloon;


        }

        return prop;
    }


    static getInstanceList(list: any): SaveSimulationOfferProposal[] {
        let proposalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                proposalList.push(SaveDealerOfferProposalFactory.getInstance(elem));
            });
        }
        return proposalList;
    }

    static getInstanceParams(obj: SaveSimulationOfferProposal): Object {

        let requestBody = {};

        if (!!obj) {

            let optionals = SaveDealerOfferOptionalFactory.getInstanceList(obj.optionals);

            let rateFactory = new SaveDealerOfferCashFlowFactory();
            let rate = rateFactory.getInstanceList(obj.rate);

            let subjects = SaveDealerOfferSubjectFactory.getInstanceListParam(obj.subjects);

            let commercial = SaveDealerOfferCommPropFactory.getInstanceParams(obj.commercial);
            let creditReport = SaveDealerOfferCreditReportFactory.getInstanceParams(obj.creditReport);
            let financial = SaveDealerOfferFinancialProposalFactory.getInstanceParams(obj.financial);
            let k2kInfo = SaveDealerOfferK2KFactory.getInstanceParams(obj.k2kInfo);
            let propaddinf = SaveDealerOfferProposalAddInfFactory.getInstanceParams(obj.propaddinf);
            let secondPeriodData = SaveDealerOfferSecondPeriodDataFactory.getInstanceParams(obj.secondPeriodData);

            let utils: UtilsService = new UtilsService();
            let prop = new SaveSimulationOfferProposal();

            requestBody = {
                anticipoPercentuale: obj.anticipoPercentuale,
                appRejCode: obj.appRejCode,
                approvalExpiryDate: obj.approvalExpiryDate,
                assegnatario: obj.assegnatario,
                assetClassCode: obj.assetClassCode,
                assetType: obj.assetType,
                attivateOggiPauout: obj.attivateOggiPauout,
                baumuster: obj.baumuster,
                brandId: obj.brandId,
                campaignCode: obj.campaignCode,
                cancellationCode: obj.cancellationCode,
                cardType: obj.cardType,
                casStatus: obj.casStatus,
                causaleSospensione: obj.causaleSospensione,
                cds: obj.cds,
                clickAndGo: obj.clickAndGo,
                clickAndGoFinalized: obj.clickAndGoFinalized,
                codPostazione: obj.codPostazione,
                codSocieta: obj.codSocieta,
                codTerzi: obj.codTerzi,
                codeIpoteca: obj.codeIpoteca,
                codiceMetodoPagamentoAnticipo: obj.codiceMetodoPagamentoAnticipo,
                codiceMetodoPagamentoBalloon: obj.codiceMetodoPagamentoBalloon,
                codiceMetodoPagamentoRate: obj.codiceMetodoPagamentoRate,
                commercial: commercial,
                concessionario: obj.concessionario,
                contractLength: obj.contractLength,
                craError: obj.craError,
                creditReport: creditReport,
                currencyCode: obj.currencyCode,
                dataScadenzaClausolaSospensiva: obj.dataScadenzaClausolaSospensiva,
                dccpay: obj.dccpay,
                dealerCDTer: obj.dealerCDTer,
                dealerTypeCode: obj.dealerTypeCode,
                descStatoProposta: obj.descStatoProposta,
                disableAutomaticAccept: obj.disableAutomaticAccept,
                distance: obj.distance,
                earlyNew: obj.earlyNew,
                esposizione: obj.esposizione,
                financedTotalAmount: obj.financedTotalAmount,
                financial: financial,
                financialProductCode: obj.financialProductCode,
                financialProposalDate: obj.financialProposalDate,
                flagStampa: obj.flagStampa,
                forceDate: obj.forceDate,
                frequency: obj.frequency,
                graphometric: obj.graphometric,
                guarantorPrivacy: obj.guarantorPrivacy,
                idProposal: obj.idProposal,
                importoAnticipo: obj.importoAnticipo,
                importoRataBalloon: obj.importoRataBalloon,
                invoiceRequest: obj.invoiceRequest,
                k2K: obj.k2K,
                k2kInfo: k2kInfo,
                lastApprovalId: obj.lastApprovalId,
                lastCredAppId: obj.lastCredAppId,
                lastFinancialCalculationDate: obj.lastFinancialCalculationDate,
                lastNote: obj.lastNote,
                leasing: obj.leasing,
                numDocUncompleted: obj.numDocUncompleted,
                numNotes: obj.numNotes,
                numeroContratto: obj.numeroContratto,
                optionals: optionals,
                origSystemCode: obj.origSystemCode,
                pendingStatusCode: obj.pendingStatusCode,
                productLineCode: obj.productLineCode,
                propFinanziamento: obj.propFinanziamento,
                propStatusDescr: obj.propStatusDescr,
                propSuspend: obj.propSuspend,
                propSuspendPayout: obj.propSuspendPayout,
                propaddinf: propaddinf,
                proposalDate: obj.proposalDate,
                proposalStatus: obj.proposalStatus,
                provisionalApproval: obj.provisionalApproval,
                rate: rate,
                rental: obj.rental,
                residualValue: obj.residualValue,
                resistDocumentRequest: obj.resistDocumentRequest,
                retailCorporate: obj.retailCorporate,
                riproposta: obj.riproposta,
                sabatiniSelected: obj.sabatiniSelected,
                secondPeriodData: secondPeriodData,
                secondaAppravazione: obj.secondaAppravazione,
                sellerCDTer: obj.sellerCDTer,
                speseDiIstruttoria: obj.speseDiIstruttoria,
                subjects: subjects,
                suspectBehaviour: obj.suspectBehaviour,
                taeg: obj.taeg,
                tan: obj.tan,
                tipoInteresse: obj.tipoInteresse,
                tipoPianoPagamenti: obj.tipoPianoPagamenti,
                tipoProposta: obj.tipoProposta,
                tipoVoucher: obj.tipoVoucher,
                totalAmount: obj.totalAmount,
                type: obj.type,
                typeClickAndGo: obj.typeClickAndGo,
                typeDeroga: obj.typeDeroga,
                updateUser: obj.updateUser,
                urgente: obj.urgente,
                used: obj.used,
                utenteSospensione: obj.utenteSospensione,
                vatPercentage: obj.vatPercentage,
                version: obj.version,
                visibleAnticipo: obj.visibleAnticipo,
                visibleBalloon: obj.visibleBalloon

            };

        }

        return requestBody;

    }

}
