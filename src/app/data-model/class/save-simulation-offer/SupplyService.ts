import {UtilsService} from '../../../shared/services/utils.service';
import {SupplyServOption, SaveDealerOfferSupplyServOptionFactory, ISupplyServOption} from './SupplyServOption';
import {CashFlow, ICashFlow, SaveDealerOfferCashFlowFactory} from './CashFlow';
import {Attachment, AttachmentFactory} from '../Attachment';

export class SupplyService {

    private _anticipo: boolean;
    private _attachments: Attachment[];
    private _balloon: boolean;
    private _beneStrumentale: boolean;
    private _categoryCode: string;
    private _cdTer: number;
    private _clazz: string;
    private _codSocieta: number;
    private _codTerzi: number;
    private _compCommSup: number;
    private _costAmount: number;
    private _costPerUnit: number;
    private _customerServiceCost: number;
    private _dealerCommisPercentage: number;
    private _defaultElement: boolean;
    private _defaultSupply: boolean;
    private _discountAmount: number;
    private _discountPercentage: number;
    private _flagGratis: boolean;
    private _flagMainSup: string;
    private _flagMandatory: boolean;
    private _flagSconto: boolean;
    private _frequenzaAnticipo: string;
    private _frequenzaBalloon: string;
    private _frequenzaRate: string;
    private _insuredValue: number;
    private _ipt: number;
    private _ivaAnticipo: number;
    private _ivaBalloon: number;
    private _ivaRate: number;
    private _netCost: number;
    private _opzioneServizio: SupplyServOption;
    private _orderNumber: number;
    private _parSupMand: string;
    private _parentSupplyTypeCode: string;
    private _rate: CashFlow[];
    private _regulatedFlag: string;
    private _retrieveVat: boolean;
    private _selected: boolean;
    private _selezionabile: string;
    private _serviceEntities: Object[];
    private _supplyAmount: number;
    private _supplyCostAmount: number;
    private _supplyCostUnit: number;
    private _supplyDescription: string;
    private _supplyElementCode: string;
    private _supplyElementCost: number;
    private _supplyElementDesc: string;
    private _supplyElementMargin: number;
    private _supplySeqNum: number;
    private _supplyTypeCode: string;
    private _supplyTypeDesc: string;
    private _tassoProdotto: boolean;
    private _taxPercentage: number;
    private _updatable: boolean;
    private _value: number;
    private _vatPercentage: number;


    get anticipo(): boolean {
        return this._anticipo;
    }

    set anticipo(value: boolean) {
        this._anticipo = value;
    }

    get attachments(): Attachment[] {
        return this._attachments;
    }

    set attachments(value: Attachment[]) {
        this._attachments = value;
    }

    get balloon(): boolean {
        return this._balloon;
    }

    set balloon(value: boolean) {
        this._balloon = value;
    }

    get beneStrumentale(): boolean {
        return this._beneStrumentale;
    }

    set beneStrumentale(value: boolean) {
        this._beneStrumentale = value;
    }

    get categoryCode(): string {
        return this._categoryCode;
    }

    set categoryCode(value: string) {
        this._categoryCode = value;
    }

    get cdTer(): number {
        return this._cdTer;
    }

    set cdTer(value: number) {
        this._cdTer = value;
    }

    get clazz(): string {
        return this._clazz;
    }

    set clazz(value: string) {
        this._clazz = value;
    }

    get codSocieta(): number {
        return this._codSocieta;
    }

    set codSocieta(value: number) {
        this._codSocieta = value;
    }

    get codTerzi(): number {
        return this._codTerzi;
    }

    set codTerzi(value: number) {
        this._codTerzi = value;
    }

    get compCommSup(): number {
        return this._compCommSup;
    }

    set compCommSup(value: number) {
        this._compCommSup = value;
    }

    get costAmount(): number {
        return this._costAmount;
    }

    set costAmount(value: number) {
        this._costAmount = value;
    }

    get costPerUnit(): number {
        return this._costPerUnit;
    }

    set costPerUnit(value: number) {
        this._costPerUnit = value;
    }

    get customerServiceCost(): number {
        return this._customerServiceCost;
    }

    set customerServiceCost(value: number) {
        this._customerServiceCost = value;
    }

    get dealerCommisPercentage(): number {
        return this._dealerCommisPercentage;
    }

    set dealerCommisPercentage(value: number) {
        this._dealerCommisPercentage = value;
    }

    get defaultElement(): boolean {
        return this._defaultElement;
    }

    set defaultElement(value: boolean) {
        this._defaultElement = value;
    }

    get defaultSupply(): boolean {
        return this._defaultSupply;
    }

    set defaultSupply(value: boolean) {
        this._defaultSupply = value;
    }

    get discountAmount(): number {
        return this._discountAmount;
    }

    set discountAmount(value: number) {
        this._discountAmount = value;
    }

    get discountPercentage(): number {
        return this._discountPercentage;
    }

    set discountPercentage(value: number) {
        this._discountPercentage = value;
    }

    get flagGratis(): boolean {
        return this._flagGratis;
    }

    set flagGratis(value: boolean) {
        this._flagGratis = value;
    }

    get flagMainSup(): string {
        return this._flagMainSup;
    }

    set flagMainSup(value: string) {
        this._flagMainSup = value;
    }

    get flagMandatory(): boolean {
        return this._flagMandatory;
    }

    set flagMandatory(value: boolean) {
        this._flagMandatory = value;
    }

    get flagSconto(): boolean {
        return this._flagSconto;
    }

    set flagSconto(value: boolean) {
        this._flagSconto = value;
    }

    get frequenzaAnticipo(): string {
        return this._frequenzaAnticipo;
    }

    set frequenzaAnticipo(value: string) {
        this._frequenzaAnticipo = value;
    }

    get frequenzaBalloon(): string {
        return this._frequenzaBalloon;
    }

    set frequenzaBalloon(value: string) {
        this._frequenzaBalloon = value;
    }

    get frequenzaRate(): string {
        return this._frequenzaRate;
    }

    set frequenzaRate(value: string) {
        this._frequenzaRate = value;
    }

    get insuredValue(): number {
        return this._insuredValue;
    }

    set insuredValue(value: number) {
        this._insuredValue = value;
    }

    get ipt(): number {
        return this._ipt;
    }

    set ipt(value: number) {
        this._ipt = value;
    }

    get ivaAnticipo(): number {
        return this._ivaAnticipo;
    }

    set ivaAnticipo(value: number) {
        this._ivaAnticipo = value;
    }

    get ivaBalloon(): number {
        return this._ivaBalloon;
    }

    set ivaBalloon(value: number) {
        this._ivaBalloon = value;
    }

    get ivaRate(): number {
        return this._ivaRate;
    }

    set ivaRate(value: number) {
        this._ivaRate = value;
    }

    get netCost(): number {
        return this._netCost;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    get opzioneServizio(): SupplyServOption {
        return this._opzioneServizio;
    }

    set opzioneServizio(value: SupplyServOption) {
        this._opzioneServizio = value;
    }

    get orderNumber(): number {
        return this._orderNumber;
    }

    set orderNumber(value: number) {
        this._orderNumber = value;
    }

    get parSupMand(): string {
        return this._parSupMand;
    }

    set parSupMand(value: string) {
        this._parSupMand = value;
    }

    get parentSupplyTypeCode(): string {
        return this._parentSupplyTypeCode;
    }

    set parentSupplyTypeCode(value: string) {
        this._parentSupplyTypeCode = value;
    }

    get rate(): CashFlow[] {
        return this._rate;
    }

    set rate(value: CashFlow[]) {
        this._rate = value;
    }

    get regulatedFlag(): string {
        return this._regulatedFlag;
    }

    set regulatedFlag(value: string) {
        this._regulatedFlag = value;
    }

    get retrieveVat(): boolean {
        return this._retrieveVat;
    }

    set retrieveVat(value: boolean) {
        this._retrieveVat = value;
    }

    get selected(): boolean {
        return this._selected;
    }

    set selected(value: boolean) {
        this._selected = value;
    }

    get selezionabile(): string {
        return this._selezionabile;
    }

    set selezionabile(value: string) {
        this._selezionabile = value;
    }

    get serviceEntities(): Object[] {
        return this._serviceEntities;
    }

    set serviceEntities(value: Object[]) {
        this._serviceEntities = value;
    }

    get supplyAmount(): number {
        return this._supplyAmount;
    }

    set supplyAmount(value: number) {
        this._supplyAmount = value;
    }

    get supplyCostAmount(): number {
        return this._supplyCostAmount;
    }

    set supplyCostAmount(value: number) {
        this._supplyCostAmount = value;
    }

    get supplyCostUnit(): number {
        return this._supplyCostUnit;
    }

    set supplyCostUnit(value: number) {
        this._supplyCostUnit = value;
    }

    get supplyDescription(): string {
        return this._supplyDescription;
    }

    set supplyDescription(value: string) {
        this._supplyDescription = value;
    }

    get supplyElementCode(): string {
        return this._supplyElementCode;
    }

    set supplyElementCode(value: string) {
        this._supplyElementCode = value;
    }

    get supplyElementCost(): number {
        return this._supplyElementCost;
    }

    set supplyElementCost(value: number) {
        this._supplyElementCost = value;
    }

    get supplyElementDesc(): string {
        return this._supplyElementDesc;
    }

    set supplyElementDesc(value: string) {
        this._supplyElementDesc = value;
    }

    get supplyElementMargin(): number {
        return this._supplyElementMargin;
    }

    set supplyElementMargin(value: number) {
        this._supplyElementMargin = value;
    }

    get supplySeqNum(): number {
        return this._supplySeqNum;
    }

    set supplySeqNum(value: number) {
        this._supplySeqNum = value;
    }

    get supplyTypeCode(): string {
        return this._supplyTypeCode;
    }

    set supplyTypeCode(value: string) {
        this._supplyTypeCode = value;
    }

    get supplyTypeDesc(): string {
        return this._supplyTypeDesc;
    }

    set supplyTypeDesc(value: string) {
        this._supplyTypeDesc = value;
    }

    get tassoProdotto(): boolean {
        return this._tassoProdotto;
    }

    set tassoProdotto(value: boolean) {
        this._tassoProdotto = value;
    }

    get taxPercentage(): number {
        return this._taxPercentage;
    }

    set taxPercentage(value: number) {
        this._taxPercentage = value;
    }

    get updatable(): boolean {
        return this._updatable;
    }

    set updatable(value: boolean) {
        this._updatable = value;
    }

    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }

    public requestParameters(): ISupplyService {
        let requestBody: ISupplyService = {
            anticipo: this._anticipo,
            attachments: AttachmentFactory.getInstanceListParam(this._attachments),
            balloon: this._balloon,
            beneStrumentale: this._beneStrumentale,
            categoryCode: this._categoryCode,
            cdTer: this._cdTer,
            clazz: this._clazz,
            codSocieta: this._codSocieta,
            codTerzi: this._codTerzi,
            compCommSup: this._compCommSup,
            costAmount: this._costAmount,
            costPerUnit: this._costPerUnit,
            customerServiceCost: this._customerServiceCost,
            dealerCommisPercentage: this._dealerCommisPercentage,
            defaultElement: this._defaultElement,
            defaultSupply: this._defaultSupply,
            discountAmount: this._discountAmount,
            discountPercentage: this._discountPercentage,
            flagGratis: this._flagGratis,
            flagMainSup: this._flagMainSup,
            flagMandatory: this._flagMandatory,
            flagSconto: this._flagSconto,
            frequenzaAnticipo: this._frequenzaAnticipo,
            frequenzaBalloon: this._frequenzaBalloon,
            frequenzaRate: this._frequenzaRate,
            insuredValue: this._insuredValue,
            ipt: this._ipt,
            ivaAnticipo: this._ivaAnticipo,
            ivaBalloon: this._ivaBalloon,
            ivaRate: this._ivaRate,
            netCost: this._netCost,
            opzioneServizio: SaveDealerOfferSupplyServOptionFactory.getInstanceParams(this._opzioneServizio),
            orderNumber: this._orderNumber,
            parSupMand: this._parSupMand,
            parentSupplyTypeCode: this._parentSupplyTypeCode,
            rate: SaveDealerOfferCashFlowFactory.getInstanceListParam(this._rate),
            regulatedFlag: this._regulatedFlag,
            retrieveVat: this._retrieveVat,
            selected: this._selected,
            selezionabile: this._selezionabile,
            serviceEntities: [],
            supplyAmount: this._supplyAmount,
            supplyCostAmount: this._supplyCostAmount,
            supplyCostUnit: this._supplyCostUnit,
            supplyDescription: this._supplyDescription,
            supplyElementCode: this._supplyElementCode,
            supplyElementCost: this._supplyElementCost,
            supplyElementDesc: this._supplyElementDesc,
            supplyElementMargin: this._supplyElementMargin,
            supplySeqNum: this._supplySeqNum,
            supplyTypeCode: this._supplyTypeCode,
            supplyTypeDesc: this._supplyTypeDesc,
            tassoProdotto: this._tassoProdotto,
            taxPercentage: this._taxPercentage,
            updatable: this._updatable,
            value: this._value,
            vatPercentage: this._vatPercentage
        };

        return requestBody;
    }

}

export class SaveDealerOfferSupplyServiceFactory {

    static getInstance(obj: SupplyService): SupplyService {

        let attachments = AttachmentFactory.getInstanceList(obj.attachments);

        let option = SaveDealerOfferSupplyServOptionFactory.getInstance(obj.opzioneServizio);

        let cashFlowFactory = new SaveDealerOfferCashFlowFactory();
        let cashFlow = cashFlowFactory.getInstanceList(obj.rate);

        let utils: UtilsService = new UtilsService();
        let supply = new SupplyService();

        if (obj) {
            if (!utils.isUndefided(obj.anticipo)) supply.anticipo = obj.anticipo;
            if (!utils.isUndefided(attachments)) supply.attachments = attachments;
            if (!utils.isUndefided(obj.balloon)) supply.balloon = obj.balloon;
            if (!utils.isUndefided(obj.beneStrumentale)) supply.beneStrumentale = obj.beneStrumentale;
            if (!utils.isUndefided(obj.categoryCode)) supply.categoryCode = obj.categoryCode;
            if (!utils.isUndefided(obj.cdTer)) supply.cdTer = obj.cdTer;
            if (!utils.isUndefided(obj.clazz)) supply.clazz = obj.clazz;
            if (!utils.isUndefided(obj.codSocieta)) supply.codSocieta = obj.codSocieta;
            if (!utils.isUndefided(obj.codTerzi)) supply.codTerzi = obj.codTerzi;
            if (!utils.isUndefided(obj.compCommSup)) supply.compCommSup = obj.compCommSup;
            if (!utils.isUndefided(obj.costAmount)) supply.costAmount = obj.costAmount;
            if (!utils.isUndefided(obj.costPerUnit)) supply.costPerUnit = obj.costPerUnit;
            if (!utils.isUndefided(obj.customerServiceCost)) supply.customerServiceCost = obj.customerServiceCost;
            if (!utils.isUndefided(obj.dealerCommisPercentage)) supply.dealerCommisPercentage = obj.dealerCommisPercentage;
            if (!utils.isUndefided(obj.defaultElement)) supply.defaultElement = obj.defaultElement;
            if (!utils.isUndefided(obj.defaultSupply)) supply.defaultSupply = obj.defaultSupply;
            if (!utils.isUndefided(obj.discountAmount)) supply.discountAmount = obj.discountAmount;
            if (!utils.isUndefided(obj.discountPercentage)) supply.discountPercentage = obj.discountPercentage;
            if (!utils.isUndefided(obj.flagGratis)) supply.flagGratis = obj.flagGratis;
            if (!utils.isUndefided(obj.flagMainSup)) supply.flagMainSup = obj.flagMainSup;
            if (!utils.isUndefided(obj.flagMandatory)) supply.flagMandatory = obj.flagMandatory;
            if (!utils.isUndefided(obj.flagSconto)) supply.flagSconto = obj.flagSconto;
            if (!utils.isUndefided(obj.frequenzaAnticipo)) supply.frequenzaAnticipo = obj.frequenzaAnticipo;
            if (!utils.isUndefided(obj.frequenzaBalloon)) supply.frequenzaBalloon = obj.frequenzaBalloon;
            if (!utils.isUndefided(obj.frequenzaRate)) supply.frequenzaRate = obj.frequenzaRate;
            if (!utils.isUndefided(obj.insuredValue)) supply.insuredValue = obj.insuredValue;
            if (!utils.isUndefided(obj.ipt)) supply.ipt = obj.ipt;
            if (!utils.isUndefided(obj.ivaAnticipo)) supply.ivaAnticipo = obj.ivaAnticipo;
            if (!utils.isUndefided(obj.ivaBalloon)) supply.ivaBalloon = obj.ivaBalloon;
            if (!utils.isUndefided(obj.ivaRate)) supply.ivaRate = obj.ivaRate;
            if (!utils.isUndefided(obj.netCost)) supply.netCost = obj.netCost;
            if (!utils.isUndefided(option)) supply.opzioneServizio = option;
            if (!utils.isUndefided(obj.orderNumber)) supply.orderNumber = obj.orderNumber;
            if (!utils.isUndefided(obj.parSupMand)) supply.parSupMand = obj.parSupMand;
            if (!utils.isUndefided(obj.parentSupplyTypeCode)) supply.parentSupplyTypeCode = obj.parentSupplyTypeCode;
            if (!utils.isUndefided(cashFlow)) supply.rate = cashFlow;
            if (!utils.isUndefided(obj.regulatedFlag)) supply.regulatedFlag = obj.regulatedFlag;
            if (!utils.isUndefided(obj.retrieveVat)) supply.retrieveVat = obj.retrieveVat;
            if (!utils.isUndefided(obj.selected)) supply.selected = obj.selected;
            if (!utils.isUndefided(obj.selezionabile)) supply.selezionabile = obj.selezionabile;
            if (!utils.isUndefided(obj.serviceEntities)) supply.serviceEntities = obj.serviceEntities;
            if (!utils.isUndefided(obj.supplyAmount)) supply.supplyAmount = obj.supplyAmount;
            if (!utils.isUndefided(obj.supplyCostAmount)) supply.supplyCostAmount = obj.supplyCostAmount;
            if (!utils.isUndefided(obj.supplyCostUnit)) supply.supplyCostUnit = obj.supplyCostUnit;
            if (!utils.isUndefided(obj.supplyDescription)) supply.supplyDescription = obj.supplyDescription;
            if (!utils.isUndefided(obj.supplyElementCode)) supply.supplyElementCode = obj.supplyElementCode;
            if (!utils.isUndefided(obj.supplyElementCost)) supply.supplyElementCost = obj.supplyElementCost;
            if (!utils.isUndefided(obj.supplyElementDesc)) supply.supplyElementDesc = obj.supplyElementDesc;
            if (!utils.isUndefided(obj.supplyElementMargin)) supply.supplyElementMargin = obj.supplyElementMargin;
            if (!utils.isUndefided(obj.supplySeqNum)) supply.supplySeqNum = obj.supplySeqNum;
            if (!utils.isUndefided(obj.supplyTypeCode)) supply.supplyTypeCode = obj.supplyTypeCode;
            if (!utils.isUndefided(obj.supplyTypeDesc)) supply.supplyTypeDesc = obj.supplyTypeDesc;
            if (!utils.isUndefided(obj.tassoProdotto)) supply.tassoProdotto = obj.tassoProdotto;
            if (!utils.isUndefided(obj.taxPercentage)) supply.taxPercentage = obj.taxPercentage;
            if (!utils.isUndefided(obj.updatable)) supply.updatable = obj.updatable;
            if (!utils.isUndefided(obj.value)) supply.value = obj.value;
            if (!utils.isUndefided(obj.vatPercentage)) supply.vatPercentage = obj.vatPercentage;
        }

        return supply;
    }

    static getInstanceListParam(list: any): ISupplyService[] {
        let supplyServiceList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                supplyServiceList.push(this.getInstance(elem).requestParameters());
            });
        }
        return supplyServiceList;
    }

    getInstanceList(list: any): SupplyService[] {
        let optionalList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                optionalList.push(SaveDealerOfferSupplyServiceFactory.getInstance(elem));
            });
        }
        return optionalList;
    }


}

export interface ISupplyService {

    anticipo: boolean;
    attachments: Attachment[];
    balloon: boolean;
    beneStrumentale: boolean;
    categoryCode: string;
    cdTer: number;
    clazz: string;
    codSocieta: number;
    codTerzi: number;
    compCommSup: number;
    costAmount: number;
    costPerUnit: number;
    customerServiceCost: number;
    dealerCommisPercentage: number;
    defaultElement: boolean;
    defaultSupply: boolean;
    discountAmount: number;
    discountPercentage: number;
    flagGratis: boolean;
    flagMainSup: string;
    flagMandatory: boolean;
    flagSconto: boolean;
    frequenzaAnticipo: string;
    frequenzaBalloon: string;
    frequenzaRate: string;
    insuredValue: number;
    ipt: number;
    ivaAnticipo: number;
    ivaBalloon: number;
    ivaRate: number;
    netCost: number;
    opzioneServizio: ISupplyServOption;
    orderNumber: number;
    parSupMand: string;
    parentSupplyTypeCode: string;
    rate: ICashFlow[];
    regulatedFlag: string;
    retrieveVat: boolean;
    selected: boolean;
    selezionabile: string;
    serviceEntities: object[];
    supplyAmount: number;
    supplyCostAmount: number;
    supplyCostUnit: number;
    supplyDescription: string;
    supplyElementCode: string;
    supplyElementCost: number;
    supplyElementDesc: string;
    supplyElementMargin: number;
    supplySeqNum: number;
    supplyTypeCode: string;
    supplyTypeDesc: string;
    tassoProdotto: boolean;
    taxPercentage: number;
    updatable: boolean;
    value: number;
    vatPercentage: number;

}
