import {UtilsService} from '../../../shared/services/utils.service';
import {ContractDet} from './ContractDet';

export class K2K {

    private _buyback: boolean;
    private _contractNumber: string;
    private _rate: number;
    private _residualDebt: number;


    get buyback(): boolean {
        return this._buyback;
    }

    set buyback(value: boolean) {
        this._buyback = value;
    }

    get contractNumber(): string {
        return this._contractNumber;
    }

    set contractNumber(value: string) {
        this._contractNumber = value;
    }

    get rate(): number {
        return this._rate;
    }

    set rate(value: number) {
        this._rate = value;
    }

    get residualDebt(): number {
        return this._residualDebt;
    }

    set residualDebt(value: number) {
        this._residualDebt = value;
    }
}

export class SaveDealerOfferK2KFactory {

    static getInstance(obj: K2K): K2K {

        let utils: UtilsService = new UtilsService();
        let k2k = new K2K();

        if (obj) {
            if (!utils.isUndefided(obj.buyback)) k2k.buyback = obj.buyback;
            if (!utils.isUndefided(obj.contractNumber)) k2k.contractNumber = obj.contractNumber;
            if (!utils.isUndefided(obj.rate)) k2k.rate = obj.rate;
            if (!utils.isUndefided(obj.residualDebt)) k2k.residualDebt = obj.residualDebt;
        }

        return k2k;
    }

    static getInstanceParams(obj: K2K): Object {
        let resp = {
            buyback: obj.buyback,
            contractNumber: obj.contractNumber,
            rate: obj.rate,
            residualDebt: obj.residualDebt
        };

        return resp;
    }

    getInstanceList(list: any): K2K[] {
        let k2kList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                k2kList.push(SaveDealerOfferK2KFactory.getInstance(elem));
            });
        }
        return k2kList;
    }

}
