import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HeaderComponent} from "../../shared/components/header/header.component";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../shared/shared.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MbzDashboardModule} from "../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../proposals/proposals.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {createTranslateLoader} from "../../app.module";
import {AppRoutingModule} from "../../app-routing.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {environment} from "../../../environments/environment.svil";
import {AppComponent} from "../../app.component";
import {AuthGuard} from "../../shared/guard";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {AbstractRequestParameters} from "./AbstractRequestParameters";
import {CreditQueueAllRequestParameters} from "./ProposalListAllRequestParameters";
import {DirectAccessGuard} from "../../shared/guard/direct-access/direct-access.guard";



describe('ProposalAllRequestParameters', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should import parameters from filter', () => {
        let filter = new CreditQueueAllRequestParameters('asset',['cds'],'cag','concessionaria',
            'cont',['deroga'],true,false,'nomeS','telaio','tipoProp',
            ['tipoSogg'],'tipoCoda','vista');

        let reqParam = new CreditQueueAllRequestParameters;

        reqParam.setParamsFromFilter(filter);

        expect(reqParam.asset).toEqual(filter.asset);
        expect(reqParam.cds).toEqual(filter.cds);
        expect(reqParam.clickAndGo).toEqual(filter.clickAndGo);
        expect(reqParam.concessionaria).toEqual(filter.concessionaria);
        expect(reqParam.contratto).toEqual(filter.contratto);
        expect(reqParam.deroga).toEqual(filter.deroga);
        expect(reqParam.errorBancaDati).toEqual(filter.errorBancaDati);
        expect(reqParam.k2k).toEqual(filter.k2k);
        expect(reqParam.nomeSogg).toEqual(filter.nomeSogg);
        expect(reqParam.telaio).toEqual(filter.telaio);
        expect(reqParam.tipoProp).toEqual(filter.tipoProp);
        expect(reqParam.tipoSogg).toEqual(filter.tipoSogg);
        expect(reqParam.tipoCoda).toEqual(filter.tipoCoda);
        expect(reqParam.vista).toEqual(filter.vista);

        filter.asset = ' ';
        filter.cds = ['test'];
        filter.clickAndGo = ' ';
        filter.concessionaria = ' ';
        filter.contratto = ' ';
        filter.deroga = [];
        filter.errorBancaDati = false;
        filter.k2k = false;
        filter.nomeSogg = ' ';
        filter.telaio = ' ';
        filter.tipoProp = ' ';
        filter.tipoSogg = ['test'];
        filter.tipoCoda = ' ';
        filter.vista = ' ';
        filter.proposta = ' ';


    });

    it('parameters absent in the filter should be undefined', () => {
        let filter = new CreditQueueAllRequestParameters('asset');

        let reqParam = new CreditQueueAllRequestParameters;

        reqParam.setParamsFromFilter(filter);

        expect(reqParam.asset).toEqual(filter.asset);

        expect(reqParam.cds).toBeUndefined();
        expect(reqParam.clickAndGo).toBeUndefined();
        expect(reqParam.concessionaria).toBeUndefined();
        expect(reqParam.contratto).toBeUndefined();
        expect(reqParam.deroga).toBeUndefined();
        expect(reqParam.errorBancaDati).toBeUndefined();
        expect(reqParam.k2k).toBeUndefined();
        expect(reqParam.nomeSogg).toBeUndefined();
        expect(reqParam.telaio).toBeUndefined();
        expect(reqParam.tipoProp).toBeUndefined();
        expect(reqParam.tipoSogg).toBeUndefined();
        expect(reqParam.tipoCoda).toBeUndefined();
        expect(reqParam.vista).toBeUndefined();


    });

    it('requestParameters test', () => {
        let filter = new CreditQueueAllRequestParameters('asset',['cds'],'cag','concessionaria',
            'cont',['deroga'],true,false,'nomeS','telaio','tipoProp',
            ['tipoSogg'],'tipoCoda','vista');

        filter.requestParameters();

    });

});
