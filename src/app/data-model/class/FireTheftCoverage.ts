import {UtilsService} from '../../shared/services/utils.service';

export class FireTheftCoverage{

    private _code?: string;
    private _description?: string;
    private _coverageLength?: number;


    constructor(code?: string, description?: string, coverageLength?: number) {
        this._code = code;
        this._description = description;
        this._coverageLength = coverageLength;
    }

    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get coverageLength(): number {
        return this._coverageLength;
    }

    set coverageLength(value: number) {
        this._coverageLength = value;
    }


}

export class FireTheftCoverageFactory{



    static getInstance(obj: FireTheftCoverage): FireTheftCoverage{
        let fireTheftCoverage = new FireTheftCoverage();
        let _utils = new UtilsService();

        if(obj){
            if (obj.code != undefined) fireTheftCoverage.code = obj.code;
            if (obj.description != undefined) fireTheftCoverage.description = obj.description;
            if (obj.coverageLength != undefined) fireTheftCoverage.coverageLength = obj.coverageLength;
        }

        return _utils.formatOBject4Output(fireTheftCoverage);

    }

    getInstanceList(list:any): FireTheftCoverage[] {
        let fireTheftCov = [];
        if(list && list.length > 0){
            list.forEach(elem => {
                fireTheftCov.push(FireTheftCoverageFactory.getInstance(elem));
            });
        }
        return fireTheftCov;
    }


}