import {FinancialOptionals, FinancialOptionalsFactory} from './FinancialOptionals';
import {AdditionalProp, AdditionalPropFactory} from './AdditionalProp';
import {UtilsService} from '../../shared/services/utils.service';
import {LoggerService} from '../../shared/services/logger.service';
import {IBlockedFields} from '../../proposals/work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/interface/IBlockedFields';
import {IFinancialPlans} from '../interface/IFinancialPlans';
import {FinancialServices, FinancialServicesFactory} from './FinancialServices';

export class FinancialPlans {


    private _campiBloccati: IBlockedFields;
    private _rataPercentuale: number;
    private _rata: number;
    private _contributoConcessionario: number;
    private _codiceTipoInteresse: string;
    private _dealerBuyBack: boolean;
    private _contributoCampagna: number;
    private _incentivoVendita: number;
    private _speseAnticipo: number;
    private _speseRataCanone: number;
    private _importoFornitura: number;
    private _percentualeDiListino: number;
    private _used: boolean;
    private _optionals: FinancialOptionals[];
    private _netCost: number;
    private _assetType: string;
    private _brandId: string;
    private _baumuster: string;
    private _tan: number;
    private _vatPercentage: number;
    private _codeIpoteca: string;
    private _rate: AdditionalProp[];
    private _totalAmount: number;
    private _spread: number;
    private _mustUseSpIstr: boolean;
    private _selFixComPerc: number;
    private _dealFixCom: number;
    private _dealPercCom: number;
    private _assetClassCode: string;
    private _speseDiIstruttoria: number;
    private _financialProductCode: string;
    private _importoAnticipo: number;
    private _anticipoPercentuale: number;
    private _financedAmount: number;
    private _sellerCommission: number;
    private _sellerSIpercentage: number;
    private _valoreCommissione: number;
    private _manuPercContrComm: number;
    private _provImportoVendita: number;
    private _dealPercManuContrComm: number;
    private _percentAtDealer: number;
    private _manuFixContrComm: number;
    private _percentAtSales: number;
    private _provImportoDeltaTasso: number;
    private _provImportoSpeseIstruttoria: number;
    private _selFixComAmount: number;
    private _dealerSIpercentage: number;
    private _provImportoServizi: number;
    private _provImportoTotale: number;
    private _taeg: number;
    private _provImportoSpeseIstruttoriaPerc: number;
    private _mfr: number;
    private _tassoBase: number;
    private _commCamp: boolean;
    private _flagShare: boolean;
    private _provImportoVenditaPerc: number;
    private _provImportoDeltaTassoPerc: number;
    private _provImportoServiziPerc: number;
    private _adminFeeShareOverride: boolean;
    private _dealManuContrComm: boolean;
    private _mss: number;
    private _ipt: number;
    private _balloonPercentuale: number;
    private _importoRataBalloon: number;
    private _leasing: boolean;
    private _contractLength: number;
    private _distance: number;
    private _frequency: number;
    private _totalCreditAmount;
    private _totalCustomerAmount;
    private _importoListino;
    private _derogaBlocchiServiziAssicurativi;
    private _derogaCommerciale;
    private _derogaContributo;
    private _fattura;
    private _codAnticipo;
    private _codCanoneRata;
    private _campaignCode;
    private _serviceEntities: FinancialServices[];
    private _voucherCode: string;
    private _contributoRetention: number;
    private _k2k: boolean;
    private _vatCode: string;
    private _customerFullName:string;
    private _kilometraggio: number;

    /**
     * @type {UtilsService}
     * @private
     */
    private _utils: UtilsService = new UtilsService();
    /**
     *
     * @type {LoggerService}
     * @private
     */
    private _logger: LoggerService = new LoggerService();

    constructor(assetClassCode?: string, assetType?: string, baumuster?: string, brandId?: string, netCost?: number, optionals?: FinancialOptionals[],
                totalAmount?: number, used?: boolean, vatPercentage?: number, mss?: number, ipt?: number, importoListino?: number,
                finProductCode?: string, anticipoPercentuale?: number, campiBloccati?: IBlockedFields, codeIpoteca?: string, codiceTipoInteresse?: string,
                contributoCampagna?: number, contributoConcessionario?: number, importoAnticipo?: number, incentivoVendita?: number, mfr?: number,
                rata?: number, rataPercentuale?: number, rate?: AdditionalProp[], speseAnticipo?: number, speseRataCanone?: number,
                provImportoTotale?: number, provImportoDeltaTassoPerc?: number, provImportoServizi?: number, provImportoDeltaTasso?: number,
                provImportoSpeseIstruttoria?: number, provImportoServiziPerc?: number, provImportoVendita?: number, provImportoVenditaPerc?: number,
                provImportoSpeseIstruttoriaPerc?: number, speseDiIstruttoria?: number, spread?: number, taeg?: number, tan?: number, tassBase?: number,
                dealerBuyBack?: boolean, importoFornitura?: number, percentualeDiListino?: number, mustUseSpIstr?: boolean, selFixComPerc?: number,
                dealFixCom?: number, dealPercCom?: number, financedAmount?: number, sellerCommission?: number, sellerSIpercentage?: number,
                valoreCommissione?: number, manuPercContrComm?: number, dealPercManuContrComm?: number, percentAtDealer?: number, manuFixContrComm?: number,
                percentAtSales?: number, selFixComAmount?: number, dealerSIpercentage?: number, commCamp?: boolean, flagShare?: boolean,
                adminFeeShareOverride?: boolean, dealManuContrComm?: boolean, balloonPercentuale?: number, importoRataBalloon?: number, leasing?: boolean,
                contractLength?: number, distance?: number, frequency?: number, totalCreditAmount ?: number, totalCustomerAmount ?: number,
                derogaBlocchiServiziAssicurativi ?: boolean, derogaCommerciale ?: boolean, derogaContributo ?: boolean, fattura ?: boolean,
                codAnticipo?: string, codCanoneRata ?: string, serviceEntities?: FinancialServices[], campaignCode?: string, voucherCode?: string,
                contributoRetention?: number, k2k?:boolean, vatCode?:string, kilometraggio?: number) {

        this._assetClassCode = assetClassCode;
        this._assetType = assetType;
        this._baumuster = baumuster;
        this._brandId = brandId;
        this._netCost = netCost;
        this._optionals = optionals;
        this._totalAmount = totalAmount;
        this._used = used;
        this._vatPercentage = vatPercentage < 1 ? vatPercentage * 100 : vatPercentage;
        this._mss = mss;
        this._financialProductCode = finProductCode;

        this._ipt = ipt;
        this._anticipoPercentuale = anticipoPercentuale;
        this._campiBloccati = campiBloccati;
        this._codeIpoteca = codeIpoteca;
        this._codiceTipoInteresse = codiceTipoInteresse;
        this._contributoCampagna = contributoCampagna;
        this._contributoConcessionario = contributoConcessionario;
        this._importoAnticipo = importoAnticipo;
        this._incentivoVendita = incentivoVendita;
        this._mfr = mfr;
        this._rata = rata;
        this._rataPercentuale = rataPercentuale;
        this._rate = rate;
        this._provImportoTotale = provImportoTotale;
        this._provImportoDeltaTassoPerc = provImportoDeltaTassoPerc;
        this._provImportoServizi = provImportoServizi;
        this._provImportoDeltaTasso = provImportoDeltaTasso;
        this._provImportoSpeseIstruttoria = provImportoSpeseIstruttoria;
        this._provImportoServiziPerc = provImportoServiziPerc;
        this._provImportoVendita = provImportoVendita;
        this._provImportoVenditaPerc = provImportoVenditaPerc;
        this._provImportoSpeseIstruttoriaPerc = provImportoSpeseIstruttoriaPerc;
        this._speseDiIstruttoria = speseDiIstruttoria;
        this._speseRataCanone = speseRataCanone;
        this._speseAnticipo = speseAnticipo;
        this._spread = spread;
        this._taeg = taeg;
        this._tan = tan;
        this._tassoBase = tassBase;
        this._contractLength = contractLength;
        this._distance = distance;
        this._frequency = frequency;
        this._totalCreditAmount = totalCreditAmount;
        this._totalCustomerAmount = totalCustomerAmount;
        this._balloonPercentuale = balloonPercentuale;
        this._importoRataBalloon = importoRataBalloon;
        this._importoListino = importoListino;
        this._derogaBlocchiServiziAssicurativi = derogaBlocchiServiziAssicurativi;
        this._derogaCommerciale = derogaCommerciale;
        this._derogaContributo = derogaContributo;
        this._fattura = fattura;
        this._codAnticipo = codAnticipo;
        this._codCanoneRata = codCanoneRata;
        this._campaignCode = campaignCode;
        this._kilometraggio = kilometraggio;
        /*
        CAMPI RIMOSSI
        this._tipoInteresse = tipoInteresse;
        this._anticipo = anticipo;
        this._balloonPercentuale = balloonPercentuale;
        this._importoRataBalloon = importoRataBalloon;

        this._operazioneK2K = operazioneK2K;
        this._campaignCode = campaignCode; // TODO: inserito nuovamente aggiungerlo nel costrutture -> pericoloso in pre-review
        this._richiestaFattura = richiestaFattura;*/

        //CAMPI AGGIUNTI
        this._dealerBuyBack = dealerBuyBack;
        this._importoFornitura = importoFornitura;
        this._percentualeDiListino = percentualeDiListino;
        this._mustUseSpIstr = mustUseSpIstr;
        this._selFixComPerc = selFixComPerc;
        this._dealFixCom = dealFixCom;
        this._dealPercCom = dealPercCom;
        this._financedAmount = financedAmount;
        this._sellerCommission = sellerCommission;
        this._sellerSIpercentage = sellerSIpercentage;
        this._valoreCommissione = valoreCommissione;
        this._manuPercContrComm = manuPercContrComm;
        this._dealPercManuContrComm = dealPercManuContrComm;
        this._percentAtDealer = percentAtDealer;
        this._manuFixContrComm = manuFixContrComm;
        this._percentAtSales = percentAtSales;
        this._selFixComAmount = selFixComAmount;
        this._dealerSIpercentage = dealerSIpercentage;
        this._commCamp = commCamp;
        this._flagShare = flagShare;
        this._adminFeeShareOverride = adminFeeShareOverride;
        this._dealManuContrComm = dealManuContrComm;
        this._serviceEntities = serviceEntities;

        // campi nuovi
        this._voucherCode = voucherCode;
        this._contributoRetention = contributoRetention;
        this._k2k = k2k;
        this._vatCode = vatCode;

    }


    setInitialValue(assetClassCode: string, assetType: string, baumuster: string, brandId: string, netCost: number, optionals: any[], totalAmount: number, used: boolean, vatPercentage: number) {
        this._assetClassCode = assetClassCode;
        this._assetType = assetType;
        this._baumuster = baumuster;
        this._brandId = brandId;
        this._netCost = netCost;
        this._optionals = optionals;
        this._totalAmount = totalAmount;
        this._used = used;
        this._vatPercentage = vatPercentage;
    }

    get anticipoPercentuale(): number {
        return this._anticipoPercentuale;
    }

    get assetClassCode(): string {
        return this._assetClassCode;
    }

    get assetType(): string {
        return this._assetType;
    }

    get baumuster(): string {
        return this._baumuster;
    }

    get brandId(): string {
        return this._brandId;
    }

    get campiBloccati(): IBlockedFields {
        return this._campiBloccati;
    }

    get codeIpoteca(): string {
        return this._codeIpoteca;
    }

    get contributoCampagna(): number {
        return this._contributoCampagna;
    }

    get contributoConcessionario(): number {
        return this._contributoConcessionario;
    }

    get codiceTipoInteresse(): string {
        return this._codiceTipoInteresse;
    }

    get speseRataCanone(): number {
        return this._speseRataCanone;
    }

    get financialProductCode(): string {
        return this._financialProductCode;
    }

    get importoAnticipo(): number {
        return this._importoAnticipo;
    }

    get incentivoVendita(): number {
        return this._incentivoVendita;
    }

    get mfr(): number {
        return this._mfr;
    }

    get netCost(): number {
        return this._netCost;
    }

    get optionals(): FinancialOptionals[] {
        return this._optionals;
    }

    get rata(): number {
        return this._rata;
    }

    get rataPercentuale(): number {
        return this._rataPercentuale;
    }

    get rate(): AdditionalProp[] {
        return this._rate;
    }

    get provImportoTotale(): number {
        return this._provImportoTotale;
    }

    get provImportoDeltaTassoPerc(): number {
        return this._provImportoDeltaTassoPerc;
    }

    get provImportoServizi(): number {
        return this._provImportoServizi;
    }

    get provImportoDeltaTasso(): number {
        return this._provImportoDeltaTasso;
    }

    get provImportoSpeseIstruttoria(): number {
        return this._provImportoSpeseIstruttoria;
    }

    get provImportoServiziPerc(): number {
        return this._provImportoServiziPerc;
    }

    get provImportoVendita(): number {
        return this._provImportoVendita;
    }

    get provImportoVenditaPerc(): number {
        return this._provImportoVenditaPerc;
    }

    get provImportoSpeseIstruttoriaPerc(): number {
        return this._provImportoSpeseIstruttoriaPerc;
    }

    get speseDiIstruttoria(): number {
        return this._speseDiIstruttoria;
    }

    get spread(): number {
        return this._spread;
    }

    get speseAnticipo(): number {
        return this._speseAnticipo;
    }

    get taeg(): number {
        return this._taeg;
    }

    get tan(): number {
        return this._tan;
    }

    get tassoBase(): number {
        return this._tassoBase;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    get used(): boolean {
        return this._used;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }

    get voucherCode(): string {
        return this._voucherCode;
    }

    get contributoRetention(): number {
        return this._contributoRetention;
    }

    get k2k(): boolean {
        return this._k2k;
    }

    set k2k(value: boolean) {
        this._k2k = value;
    }

    set voucherCode(value: string) {
        this._voucherCode = value;
    }

    set contributoRetention(value: number) {
        this._contributoRetention = value;
    }

    set anticipoPercentuale(value: number) {
        this._anticipoPercentuale = value;
    }

    set assetClassCode(value: string) {
        this._assetClassCode = value;
    }

    set assetType(value: string) {
        this._assetType = value;
    }

    set baumuster(value: string) {
        this._baumuster = value;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    set campiBloccati(value: IBlockedFields) {
        this._campiBloccati = value;
    }

    set codeIpoteca(value: string) {
        this._codeIpoteca = value;
    }

    set contributoCampagna(value: number) {
        this._contributoCampagna = value;
    }

    set contributoConcessionario(value: number) {
        this._contributoConcessionario = value;
    }


    set speseRataCanone(value: number) {
        this._speseRataCanone = value;
    }

    set codiceTipoInteresse(value: string) {
        this._codiceTipoInteresse = value;
    }

    set financialProductCode(value: string) {
        this._financialProductCode = value;
    }


    get serviceEntities(): FinancialServices[] {
        return this._serviceEntities;
    }

    set serviceEntities(value: FinancialServices[]) {
        this._serviceEntities = value;
    }

    get campaignCode() {
        return this._campaignCode;
    }

    set campaignCode(value) {
        this._campaignCode = value;
    }

    set importoAnticipo(value: number) {
        this._importoAnticipo = value;
    }

    set incentivoVendita(value: number) {
        this._incentivoVendita = value;
    }

    set mfr(value: number) {
        this._mfr = value;
    }

    set netCost(value: number) {
        this._netCost = value;
    }

    set optionals(value: FinancialOptionals[]) {
        this._optionals = value;
    }

    set provImportoTotale(value: number) {
        this._provImportoTotale = value;
    }

    set provImportoDeltaTassoPerc(value: number) {
        this._provImportoDeltaTassoPerc = value;
    }

    set provImportoServizi(value: number) {
        this._provImportoServizi = value;
    }

    set provImportoDeltaTasso(value: number) {
        this._provImportoDeltaTasso = value;
    }

    set provImportoSpeseIstruttoria(value: number) {
        this._provImportoSpeseIstruttoria = value;
    }

    set provImportoServiziPerc(value: number) {
        this._provImportoServiziPerc = value;
    }

    set provImportoVendita(value: number) {
        this._provImportoVendita = value;
    }

    set provImportoVenditaPerc(value: number) {
        this._provImportoVenditaPerc = value;
    }

    set provImportoSpeseIstruttoriaPerc(value: number) {
        this._provImportoSpeseIstruttoriaPerc = value;
    }

    set rata(value: number) {
        this._rata = value;
    }

    set rataPercentuale(value: number) {
        this._rataPercentuale = value;
    }

    set rate(value: AdditionalProp[]) {
        this._rate = value;
    }

    set speseDiIstruttoria(value: number) {
        this._speseDiIstruttoria = value;
    }

    set spread(value: number) {
        this._spread = value;
    }

    set speseAnticipo(value: number) {
        this._speseAnticipo = value;
    }

    set taeg(value: number) {
        this._taeg = value;
    }

    set tan(value: number) {
        this._tan = value;
    }

    set tassoBase(value: number) {
        this._tassoBase = value;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    set used(value: boolean) {
        this._used = value;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }


    get mustUseSpIstr(): boolean {
        return this._mustUseSpIstr;
    }

    set mustUseSpIstr(value: boolean) {
        this._mustUseSpIstr = value;
    }

    get dealerBuyBack(): boolean {
        return this._dealerBuyBack;
    }

    set dealerBuyBack(value: boolean) {
        this._dealerBuyBack = value;
    }

    get importoFornitura(): number {
        return this._importoFornitura;
    }

    set importoFornitura(value: number) {
        this._importoFornitura = value;
    }

    get percentualeDiListino(): number {
        return this._percentualeDiListino;
    }

    set percentualeDiListino(value: number) {
        this._percentualeDiListino = value;
    }

    get selFixComPerc(): number {
        return this._selFixComPerc;
    }

    set selFixComPerc(value: number) {
        this._selFixComPerc = value;
    }

    get dealFixCom(): number {
        return this._dealFixCom;
    }

    set dealFixCom(value: number) {
        this._dealFixCom = value;
    }

    get dealPercCom(): number {
        return this._dealPercCom;
    }

    set dealPercCom(value: number) {
        this._dealPercCom = value;
    }

    get financedAmount(): number {
        return this._financedAmount;
    }

    set financedAmount(value: number) {
        this._financedAmount = value;
    }

    get sellerCommission(): number {
        return this._sellerCommission;
    }

    set sellerCommission(value: number) {
        this._sellerCommission = value;
    }

    get sellerSIpercentage(): number {
        return this._sellerSIpercentage;
    }

    set sellerSIpercentage(value: number) {
        this._sellerSIpercentage = value;
    }

    get valoreCommissione(): number {
        return this._valoreCommissione;
    }

    set valoreCommissione(value: number) {
        this._valoreCommissione = value;
    }

    get manuPercContrComm(): number {
        return this._manuPercContrComm;
    }

    set manuPercContrComm(value: number) {
        this._manuPercContrComm = value;
    }

    get dealPercManuContrComm(): number {
        return this._dealPercManuContrComm;
    }

    set dealPercManuContrComm(value: number) {
        this._dealPercManuContrComm = value;
    }

    get percentAtDealer(): number {
        return this._percentAtDealer;
    }

    set percentAtDealer(value: number) {
        this._percentAtDealer = value;
    }

    get manuFixContrComm(): number {
        return this._manuFixContrComm;
    }

    set manuFixContrComm(value: number) {
        this._manuFixContrComm = value;
    }

    get percentAtSales(): number {
        return this._percentAtSales;
    }

    set percentAtSales(value: number) {
        this._percentAtSales = value;
    }

    get selFixComAmount(): number {
        return this._selFixComAmount;
    }

    set selFixComAmount(value: number) {
        this._selFixComAmount = value;
    }

    get dealerSIpercentage(): number {
        return this._dealerSIpercentage;
    }

    set dealerSIpercentage(value: number) {
        this._dealerSIpercentage = value;
    }

    get commCamp(): boolean {
        return this._commCamp;
    }

    set commCamp(value: boolean) {
        this._commCamp = value;
    }

    get flagShare(): boolean {
        return this._flagShare;
    }

    set flagShare(value: boolean) {
        this._flagShare = value;
    }

    get adminFeeShareOverride(): boolean {
        return this._adminFeeShareOverride;
    }

    set adminFeeShareOverride(value: boolean) {
        this._adminFeeShareOverride = value;
    }

    get dealManuContrComm(): boolean {
        return this._dealManuContrComm;
    }

    set dealManuContrComm(value: boolean) {
        this._dealManuContrComm = value;
    }

    get utils(): UtilsService {
        return this._utils;
    }

    set utils(value: UtilsService) {
        this._utils = value;
    }

    get logger(): LoggerService {
        return this._logger;
    }

    set logger(value: LoggerService) {
        this._logger = value;
    }


    get balloonPercentuale(): number {
        return this._balloonPercentuale;
    }

    set balloonPercentuale(value: number) {
        this._balloonPercentuale = value;
    }

    get importoRataBalloon(): number {
        return this._importoRataBalloon;
    }

    set importoRataBalloon(value: number) {
        this._importoRataBalloon = value;
    }

    get leasing(): boolean {
        return this._leasing;
    }

    set leasing(value: boolean) {
        this._leasing = value;
    }

    get mss(): any {
        return this._mss;
    }

    set mss(value: any) {
        this._mss = value;
    }

    get ipt(): any {
        return this._ipt;
    }

    set ipt(value: any) {
        this._ipt = value;
    }


    get contractLength(): number {
        return this._contractLength;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get frequency(): number {
        return this._frequency;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    get totalCreditAmount() {
        return this._totalCreditAmount;
    }

    set totalCreditAmount(value) {
        this._totalCreditAmount = value;
    }


    get totalCustomerAmount() {
        return this._totalCustomerAmount;
    }

    set totalCustomerAmount(value) {
        this._totalCustomerAmount = value;
    }

    get importoListino() {
        return this._importoListino;
    }

    set importoListino(value) {
        this._importoListino = value;
    }


    get derogaBlocchiServiziAssicurativi() {
        return this._derogaBlocchiServiziAssicurativi;
    }

    set derogaBlocchiServiziAssicurativi(value) {
        this._derogaBlocchiServiziAssicurativi = value;
    }

    get derogaCommerciale() {
        return this._derogaCommerciale;
    }

    set derogaCommerciale(value) {
        this._derogaCommerciale = value;
    }

    get derogaContributo() {
        return this._derogaContributo;
    }

    set derogaContributo(value) {
        this._derogaContributo = value;
    }

    get fattura() {
        return this._fattura;
    }

    set fattura(value) {
        this._fattura = value;
    }


    get codAnticipo() {
        return this._codAnticipo;
    }

    set codAnticipo(value) {
        this._codAnticipo = value;
    }

    get codCanoneRata() {
        return this._codCanoneRata;
    }

    set codCanoneRata(value) {
        this._codCanoneRata = value;
    }


    get vatCode(): string {
        return this._vatCode;
    }

    set vatCode(value: string) {
        this._vatCode = value;
    }

    get customerFullName(): string {
        return this._customerFullName;
    }

    set customerFullName(value: string) {
        this._customerFullName = value;
    }

    get kilometraggio(): number {
        return this._kilometraggio;
    }

    set kilometraggio(value: number) {
        this._kilometraggio = value;
    }

    /**
     * imposta i parametri per la request per l'api /financial/plans
     */
    public requestParameters(): IFinancialPlans {
        let requestBody: IFinancialPlans = {
            anticipoPercentuale: this._anticipoPercentuale,
            assetClassCode: this._assetClassCode,
            assetType: this._assetType,
            baumuster: this._baumuster,
            brandId: this._brandId,
            campiBloccati: this._campiBloccati,
            codeIpoteca: this._codeIpoteca,
            codiceTipoInteresse: this._codiceTipoInteresse,
            contributoCampagna: this._contributoCampagna,
            contributoConcessionario: this._contributoConcessionario,
            financialProductCode: this._financialProductCode,
            incentivoVendita: this._incentivoVendita,
            mfr: this._mfr,
            importoAnticipo: this._importoAnticipo,
            netCost: this._netCost,
            optionals: FinancialOptionalsFactory.getInstanceListParam(this._optionals),
            provImportoTotale: this._provImportoTotale,
            provImportoDeltaTassoPerc: this._provImportoDeltaTassoPerc,
            provImportoServizi: this._provImportoServizi,
            provImportoDeltaTasso: this._provImportoDeltaTasso,
            provImportoSpeseIstruttoria: this._provImportoSpeseIstruttoria,
            provImportoServiziPerc: this._provImportoServiziPerc,
            provImportoVendita: this._provImportoVendita,
            provImportoVenditaPerc: this._provImportoVenditaPerc,
            provImportoSpeseIstruttoriaPerc: this._provImportoSpeseIstruttoriaPerc,
            rata: this._rata,
            rataPercentuale: this._rataPercentuale,
            rate: AdditionalPropFactory.getInstanceListParam(this._rate),
            speseDiIstruttoria: this._speseDiIstruttoria,
            speseAnticipo: this._speseAnticipo,
            spread: this._spread,
            taeg: this._taeg,
            tan: this._tan,
            tassoBase: this._tassoBase,
            totalAmount: this._totalAmount,
            used: this._used,
            vatPercentage: this._vatPercentage,
            dealerBuyBack: this._dealerBuyBack,
            importoFornitura: this._importoFornitura,
            percentualeDiListino: this._percentualeDiListino,
            mustUseSpIstr: this._mustUseSpIstr,
            selFixComPerc: this._selFixComPerc,
            dealFixCom: this._dealFixCom,
            dealPercCom: this._dealPercCom,
            financedAmount: this._financedAmount,
            sellerCommission: this._sellerCommission,
            sellerSIpercentage: this._sellerSIpercentage,
            valoreCommissione: this._valoreCommissione,
            manuPercContrComm: this._manuPercContrComm,
            dealPercManuContrComm: this._dealPercManuContrComm,
            percentAtDealer: this._percentAtDealer,
            manuFixContrComm: this._manuFixContrComm,
            percentAtSales: this._percentAtSales,
            selFixComAmount: this._selFixComAmount,
            dealerSIpercentage: this._dealerSIpercentage,
            commCamp: this._commCamp,
            flagShare: this._flagShare,
            adminFeeShareOverride: this._adminFeeShareOverride,
            dealManuContrComm: this._dealManuContrComm,
            balloonPercentuale: this._balloonPercentuale,
            importoRataBalloon: this._importoRataBalloon,
            leasing: this._leasing,
            mss: this._mss,
            ipt: this._ipt,
            contractLength: this._contractLength,
            distance: this._distance,
            frequency: this._frequency,
            totalCreditAmount: this._totalCreditAmount,
            totalCustomerAmount: this._totalCustomerAmount,
            importoListino: this._importoListino,
            derogaBlocchiServiziAssicurativi: this._derogaBlocchiServiziAssicurativi,
            derogaCommerciale: this._derogaCommerciale,
            derogaContributo: this._derogaContributo,
            fattura: this._fattura,
            codAnticipo: this._codAnticipo,
            codCanoneRata: this._codCanoneRata,
            campaignCode: this._campaignCode,
            serviceEntities: this._utils.formatOBject4Output(this._serviceEntities),
            voucherCode: this._voucherCode,
            contributoRetention: this._contributoRetention,
            operazioneK2K: this._k2k,
            vatCode: this.vatCode,
            customerFullName: this._customerFullName,
            kilometraggio: this._kilometraggio
        };

        this._logger.logInfo(FinancialPlans.name, 'requestBody', requestBody);

        return requestBody;
    }


}

export class FinancialPlansFactory {
    //TODO: ristrutturare la factory

    static getInstance(obj: FinancialPlans): FinancialPlans {
        return new FinancialPlans(obj.assetClassCode, obj.assetType, obj.baumuster, obj.brandId, obj.netCost, obj.optionals,
            obj.totalAmount, obj.used, obj.vatPercentage, obj.mss, obj.ipt, obj.importoListino, obj.financialProductCode, obj.anticipoPercentuale,
            obj.campiBloccati, obj.codeIpoteca, obj.codiceTipoInteresse, obj.contributoCampagna, obj.contributoConcessionario, obj.importoAnticipo,
            obj.incentivoVendita, obj.mfr, obj.rata, obj.rataPercentuale, obj.rate, obj.speseAnticipo, obj.speseRataCanone, obj.provImportoTotale,
            obj.provImportoDeltaTassoPerc, obj.provImportoServizi, obj.provImportoDeltaTasso, obj.provImportoSpeseIstruttoria, obj.provImportoServiziPerc,
            obj.provImportoVendita, obj.provImportoVenditaPerc, obj.provImportoSpeseIstruttoriaPerc, obj.speseDiIstruttoria, obj.spread, obj.taeg,
            obj.tan, obj.tassoBase, obj.dealerBuyBack, obj.importoFornitura, obj.percentualeDiListino, obj.mustUseSpIstr, obj.selFixComPerc,
            obj.dealFixCom, obj.dealPercCom, obj.financedAmount, obj.sellerCommission, obj.sellerSIpercentage, obj.valoreCommissione,
            obj.manuPercContrComm, obj.dealPercManuContrComm, obj.percentAtDealer, obj.manuFixContrComm, obj.percentAtSales, obj.selFixComAmount,
            obj.dealerSIpercentage, obj.commCamp, obj.flagShare, obj.adminFeeShareOverride, obj.dealManuContrComm, obj.balloonPercentuale,
            obj.importoRataBalloon, obj.leasing, obj.contractLength, obj.distance, obj.frequency, obj.totalCreditAmount, obj.totalCustomerAmount,
            obj.derogaBlocchiServiziAssicurativi, obj.derogaCommerciale, obj.derogaContributo, obj.fattura, obj.codAnticipo, obj.codCanoneRata,
            obj.serviceEntities, obj.campaignCode, obj.voucherCode, obj.contributoRetention, obj.k2k, obj.vatCode,obj.kilometraggio);

        
    }
}
























































