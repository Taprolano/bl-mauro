// import {MbcposElement} from './MbcposElement';

export class ConfigurazioneRate {

    private _anticipo : boolean;
    private _balloon : boolean;
    private _codiceMetodoPagamentoAnticipo :  string;
    private _codiceMetodoPagamentoBalloon :  string;
    private _codiceMetodoPagamentoRate :  string;
    private _dfFrequenzaAnticipo :  string;
    private _dfFrequenzaBalloon :  string;
    private _dfFrequenzaRate :  string;
    private _ipoteca : boolean;
    private _ivaAnticipo : number;
    private _ivaBalloon : number;
    private _ivaRate : number;
    private _labelAnticipo :  string;
    private _labelBalloon :  string;
    private _labelRate :  string;
    private _metodoPagamentoAnticipoModificabile : boolean;
    private _metodoPagamentoBalloonModificabile : boolean;
    private _metodoPagamentoRateModificabile : boolean;
    private _modificaNumeroPagamentiAnticipo : boolean;
    private _modificaNumeroPagamentiBalloon : boolean;
    private _modificaNumeroPagamentiRate : boolean;
    private _vatCodeAnticipo :  string;
    private _vatCodeBalloon :  string;
    private _vatCodeRate : string;

    /* COSTRUTTORE */

    /**
     *
     * @param {boolean} anticipo
     * @param {boolean} balloon
     * @param {string} codiceMetodoPagamentoAnticipo
     * @param {string} codiceMetodoPagamentoBalloon
     * @param {string} codiceMetodoPagamentoRate
     * @param {string} dfFrequenzaAnticipo
     * @param {string} dfFrequenzaBalloon
     * @param {string} dfFrequenzaRate
     * @param {boolean} ipoteca
     * @param {number} ivaAnticipo
     * @param {number} ivaBalloon
     * @param {number} ivaRate
     * @param {string} labelAnticipo
     * @param {string} labelBalloon
     * @param {string} labelRate
     * @param {boolean} metodoPagamentoAnticipoModificabile
     * @param {boolean} metodoPagamentoBalloonModificabile
     * @param {boolean} metodoPagamentoRateModificabile
     * @param {boolean} modificaNumeroPagamentiAnticipo
     * @param {boolean} modificaNumeroPagamentiBalloon
     * @param {boolean} modificaNumeroPagamentiRate
     * @param {string} vatCodeAnticipo
     * @param {string} vatCodeBalloon
     * @param {string} vatCodeRate
     */
    constructor(anticipo?: boolean, balloon?: boolean, codiceMetodoPagamentoAnticipo?: string, codiceMetodoPagamentoBalloon?: string, codiceMetodoPagamentoRate?: string, dfFrequenzaAnticipo?: string, dfFrequenzaBalloon?: string, dfFrequenzaRate?: string, ipoteca?: boolean, ivaAnticipo?: number, ivaBalloon?: number, ivaRate?: number, labelAnticipo?: string, labelBalloon?: string, labelRate?: string, metodoPagamentoAnticipoModificabile?: boolean, metodoPagamentoBalloonModificabile?: boolean, metodoPagamentoRateModificabile?: boolean, modificaNumeroPagamentiAnticipo?: boolean, modificaNumeroPagamentiBalloon?: boolean, modificaNumeroPagamentiRate?: boolean, vatCodeAnticipo?: string, vatCodeBalloon?: string, vatCodeRate?: string) {
        this._anticipo = anticipo;
        this._balloon = balloon;
        this._codiceMetodoPagamentoAnticipo = codiceMetodoPagamentoAnticipo;
        this._codiceMetodoPagamentoBalloon = codiceMetodoPagamentoBalloon;
        this._codiceMetodoPagamentoRate = codiceMetodoPagamentoRate;
        this._dfFrequenzaAnticipo = dfFrequenzaAnticipo;
        this._dfFrequenzaBalloon = dfFrequenzaBalloon;
        this._dfFrequenzaRate = dfFrequenzaRate;
        this._ipoteca = ipoteca;
        this._ivaAnticipo = ivaAnticipo;
        this._ivaBalloon = ivaBalloon;
        this._ivaRate = ivaRate;
        this._labelAnticipo = labelAnticipo;
        this._labelBalloon = labelBalloon;
        this._labelRate = labelRate;
        this._metodoPagamentoAnticipoModificabile = metodoPagamentoAnticipoModificabile;
        this._metodoPagamentoBalloonModificabile = metodoPagamentoBalloonModificabile;
        this._metodoPagamentoRateModificabile = metodoPagamentoRateModificabile;
        this._modificaNumeroPagamentiAnticipo = modificaNumeroPagamentiAnticipo;
        this._modificaNumeroPagamentiBalloon = modificaNumeroPagamentiBalloon;
        this._modificaNumeroPagamentiRate = modificaNumeroPagamentiRate;
        this._vatCodeAnticipo = vatCodeAnticipo;
        this._vatCodeBalloon = vatCodeBalloon;
        this._vatCodeRate = vatCodeRate;
    }

    /* GET */

    /** @returns {boolean} */
    get anticipo(): boolean {
        return this._anticipo;
    }

    /** @returns {boolean} */
    get balloon(): boolean {
        return this._balloon;
    }

    /** @returns {string} */
    get codiceMetodoPagamentoAnticipo(): string {
        return this._codiceMetodoPagamentoAnticipo;
    }

    /** @returns {string} */
    get codiceMetodoPagamentoBalloon(): string {
        return this._codiceMetodoPagamentoBalloon;
    }

    /** @returns {string} */
    get codiceMetodoPagamentoRate(): string {
        return this._codiceMetodoPagamentoRate;
    }

    /** @returns {string} */
    get dfFrequenzaAnticipo(): string {
        return this._dfFrequenzaAnticipo;
    }

    /** @returns {string} */
    get dfFrequenzaBalloon(): string {
        return this._dfFrequenzaBalloon;
    }

    /** @returns {string} */
    get dfFrequenzaRate(): string {
        return this._dfFrequenzaRate;
    }

    /** @returns {boolean} */
    get ipoteca(): boolean {
        return this._ipoteca;
    }

    /** @returns {number} */
    get ivaAnticipo(): number {
        return this._ivaAnticipo;
    }

    /** @returns {number} */
    get ivaBalloon(): number {
        return this._ivaBalloon;
    }

    /** @returns {number} */
    get ivaRate(): number {
        return this._ivaRate;
    }

    /** @returns {string} */
    get labelAnticipo(): string {
        return this._labelAnticipo;
    }

    /** @returns {string} */
    get labelBalloon(): string {
        return this._labelBalloon;
    }

    /** @returns {string} */
    get labelRate(): string {
        return this._labelRate;
    }

    /** @returns {boolean} */
    get metodoPagamentoAnticipoModificabile(): boolean {
        return this._metodoPagamentoAnticipoModificabile;
    }

    /** @returns {boolean} */
    get metodoPagamentoBalloonModificabile(): boolean {
        return this._metodoPagamentoBalloonModificabile;
    }

    /** @returns {boolean} */
    get metodoPagamentoRateModificabile(): boolean {
        return this._metodoPagamentoRateModificabile;
    }

    /** @returns {boolean} */
    get modificaNumeroPagamentiAnticipo(): boolean {
        return this._modificaNumeroPagamentiAnticipo;
    }

    /** @returns {boolean} */
    get modificaNumeroPagamentiBalloon(): boolean {
        return this._modificaNumeroPagamentiBalloon;
    }

    /** @returns {boolean} */
    get modificaNumeroPagamentiRate(): boolean {
        return this._modificaNumeroPagamentiRate;
    }

    /** @returns {string} */
    get vatCodeAnticipo(): string {
        return this._vatCodeAnticipo;
    }

    /** @returns {string} */
    get vatCodeBalloon(): string {
        return this._vatCodeBalloon;
    }

    /** @returns {string} */
    get vatCodeRate(): string {
        return this._vatCodeRate;
    }


    /* SET */
    /** @param {boolean} value */
    set anticipo(value: boolean) {
        this._anticipo = value;
    }

    /** @param {boolean} value */
    set balloon(value: boolean) {
        this._balloon = value;
    }

    /** @param {string} value */
    set codiceMetodoPagamentoAnticipo(value: string) {
        this._codiceMetodoPagamentoAnticipo = value;
    }

    /** @param {string} value */
    set codiceMetodoPagamentoBalloon(value: string) {
        this._codiceMetodoPagamentoBalloon = value;
    }

    /** @param {string} value */
    set codiceMetodoPagamentoRate(value: string) {
        this._codiceMetodoPagamentoRate = value;
    }

    /** @param {string} value */
    set dfFrequenzaAnticipo(value: string) {
        this._dfFrequenzaAnticipo = value;
    }

    /** @param {string} value */
    set dfFrequenzaBalloon(value: string) {
        this._dfFrequenzaBalloon = value;
    }

    /** @param {string} value */
    set dfFrequenzaRate(value: string) {
        this._dfFrequenzaRate = value;
    }

    /** @param {boolean} value */
    set ipoteca(value: boolean) {
        this._ipoteca = value;
    }

    /** @param {number} value */
    set ivaAnticipo(value: number) {
        this._ivaAnticipo = value;
    }

    /** @param {number} value */
    set ivaBalloon(value: number) {
        this._ivaBalloon = value;
    }

    /** @param {number} value */
    set ivaRate(value: number) {
        this._ivaRate = value;
    }

    /** @param {string} value */
    set labelAnticipo(value: string) {
        this._labelAnticipo = value;
    }

    /** @param {string} value */
    set labelBalloon(value: string) {
        this._labelBalloon = value;
    }

    /** @param {string} value */
    set labelRate(value: string) {
        this._labelRate = value;
    }

    /** @param {boolean} value */
    set metodoPagamentoAnticipoModificabile(value: boolean) {
        this._metodoPagamentoAnticipoModificabile = value;
    }

    /** @param {boolean} value */
    set metodoPagamentoBalloonModificabile(value: boolean) {
        this._metodoPagamentoBalloonModificabile = value;
    }

    /** @param {boolean} value */
    set metodoPagamentoRateModificabile(value: boolean) {
        this._metodoPagamentoRateModificabile = value;
    }

    /** @param {boolean} value */
    set modificaNumeroPagamentiAnticipo(value: boolean) {
        this._modificaNumeroPagamentiAnticipo = value;
    }

    /** @param {boolean} value */
    set modificaNumeroPagamentiBalloon(value: boolean) {
        this._modificaNumeroPagamentiBalloon = value;
    }

    /** @param {boolean} value */
    set modificaNumeroPagamentiRate(value: boolean) {
        this._modificaNumeroPagamentiRate = value;
    }

    /** @param {string} value */
    set vatCodeAnticipo(value: string) {
        this._vatCodeAnticipo = value;
    }

    /** @param {string} value */
    set vatCodeBalloon(value: string) {
        this._vatCodeBalloon = value;
    }

    /** @param {string} value */
    set vatCodeRate(value: string) {
        this._vatCodeRate = value;
    }
}


/**
 * Per normalizzazione dati ricevuti
 */
export class ConfigurazioneRateFactory {

    /**
     * Normalizza elemento
     * @param {ConfigurazioneRate} obj
     * @returns {ConfigurazioneRate}
     */
    static getInstance(obj:ConfigurazioneRate): ConfigurazioneRate {
        let confRate = new ConfigurazioneRate();

        if(obj.anticipo != undefined) confRate.anticipo = obj.anticipo;
        if(obj.balloon != undefined) confRate.balloon = obj.balloon;
        if(obj.codiceMetodoPagamentoAnticipo != undefined) confRate.codiceMetodoPagamentoAnticipo = obj.codiceMetodoPagamentoAnticipo;
        if(obj.codiceMetodoPagamentoBalloon != undefined) confRate.codiceMetodoPagamentoBalloon = obj.codiceMetodoPagamentoBalloon;
        if(obj.codiceMetodoPagamentoRate != undefined) confRate.codiceMetodoPagamentoRate = obj.codiceMetodoPagamentoRate;
        if(obj.dfFrequenzaAnticipo != undefined) confRate.dfFrequenzaAnticipo = obj.dfFrequenzaAnticipo;
        if(obj.dfFrequenzaBalloon != undefined) confRate.dfFrequenzaBalloon = obj.dfFrequenzaBalloon;
        if(obj.dfFrequenzaRate != undefined) confRate.dfFrequenzaRate = obj.dfFrequenzaRate;
        if(obj.ipoteca != undefined) confRate.ipoteca = obj.ipoteca;
        if(obj.ivaAnticipo != undefined) confRate.ivaAnticipo = obj.ivaAnticipo;
        if(obj.ivaBalloon != undefined) confRate.ivaBalloon = obj.ivaBalloon;
        if(obj.ivaRate != undefined) confRate.ivaRate = obj.ivaRate;
        if(obj.labelAnticipo != undefined) confRate.labelAnticipo = obj.labelAnticipo;
        if(obj.labelBalloon != undefined) confRate.labelBalloon = obj.labelBalloon;
        if(obj.labelRate != undefined) confRate.labelRate = obj.labelRate;
        if(obj.metodoPagamentoAnticipoModificabile != undefined) confRate.metodoPagamentoAnticipoModificabile = obj.metodoPagamentoAnticipoModificabile;
        if(obj.metodoPagamentoBalloonModificabile != undefined) confRate.metodoPagamentoBalloonModificabile = obj.metodoPagamentoBalloonModificabile;
        if(obj.metodoPagamentoRateModificabile != undefined) confRate.metodoPagamentoRateModificabile = obj.metodoPagamentoRateModificabile;
        if(obj.modificaNumeroPagamentiAnticipo != undefined) confRate.metodoPagamentoRateModificabile = obj.metodoPagamentoRateModificabile;
        if(obj.modificaNumeroPagamentiBalloon != undefined) confRate.modificaNumeroPagamentiBalloon = obj.modificaNumeroPagamentiBalloon;
        if(obj.modificaNumeroPagamentiRate != undefined) confRate.modificaNumeroPagamentiRate = obj.modificaNumeroPagamentiRate;
        if(obj.vatCodeAnticipo != undefined) confRate.vatCodeAnticipo = obj.vatCodeAnticipo;
        if(obj.vatCodeBalloon != undefined) confRate.vatCodeBalloon = obj.vatCodeBalloon;
        if(obj.vatCodeRate != undefined) confRate.vatCodeRate = obj.vatCodeRate;

        return confRate;
    }

    /**
     * Normalizza lista di elementi
     * @param list
     * @returns {ConfigurazioneRate[]}
     */
    getInstanceList(list:any): ConfigurazioneRate[] {
        let configurazioneRateList = [];
        if(list && list.length > 0){
            list.forEach(elem => {
                configurazioneRateList.push(ConfigurazioneRateFactory.getInstance(elem));
            });
        }
        return configurazioneRateList;
    }

}