import {UtilsService} from "../../shared/services/utils.service";
import {LoggerService} from "../../shared/services/logger.service";
import {HttpParams} from "@angular/common/http";

export abstract class AbstractRequestParameters {

    /**@ignore*/
    protected readonly _utils: UtilsService = new UtilsService();

    /** @ignore */
    protected _page: number;
    /** @ignore */
    protected _size: number;
    /** Stringa di ordinamento*/
    protected _sort:string;

    protected _logger: LoggerService = new LoggerService();

    /**@ignore*/
    constructor() {}

    /**
     * Funzione che ritorna i parametri impostati per la richiesta
     * @returns {HttpParams}
     */
    abstract requestParameters(): HttpParams

    /**
     * Setta i parametri in base al filtro in input
     */
     setParamsFromFilter(object:AbstractRequestParameters){};

    /* Getters e Setters */

    /**@returns {string}*/
    public get page(): number {
        return this._page;
    }
    /**@param {string} value*/
    public set page(value: number) {
        this._page = value;
    }

    /**@returns {string}*/
    public get size(): number {
        return this._size;
    }
    /**@param {string} value*/
    public set size(value: number) {
        this._size = value;
    }

    /**@returns {string}*/
    public get sort(): string {
        return this._sort;
    }
    /**@param {string} value*/
    public set sort(value: string) {
        this._sort = value;
    }

}