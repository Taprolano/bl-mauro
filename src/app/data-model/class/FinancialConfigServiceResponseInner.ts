import {FinacialBaseServices} from './FinacialBaseServices';
import {FireTheftCoverage, FireTheftCoverageFactory} from './FireTheftCoverage';
import {Distances, DistancesFactory} from './Distances';
import {RtiCoverage, RtiCoverageFactory} from './RtiCoverage';
import {UtilsService} from '../../shared/services/utils.service';

export class FinancialConfigServiceResponseInner extends FinacialBaseServices {

    private _beneStrumentale?: boolean;
    private _fireTheftCoverage?: FireTheftCoverage[];
    private _rtiCoverage?: RtiCoverage[];
    private _hasFireTheftInsurance?: boolean;
    private _hasSatellite?: boolean;
    private _province?: string[];
    private _clazz?: string;
    private _distances?: Distances[];
    private _estensioneGaranziaProposta?: number[];


    constructor(categoryCode?: string, defaultElement?: boolean, defaultSupply?: boolean, financeCode?: string, flagMainSup?: string,
                flagMandatory?: string, parentSupplyTypeCode?: string, parSupMand?: string, selezionabile?: string,
                supplyElementCode?: string, supplyElementDesc?: string, supplyTypeCode?: string, supplyTypeDesc?: string,
                tassoProdotto?: boolean, taxPercentage?: number, updatable?: boolean, beneStrumentale?: boolean,
                fireTheftCoverage?: any[], hasFireTheftInsurance?: boolean, hasSatellite?: boolean, type?: string, province?: string[],
                distances?: Distances, estensioneGaranziaProposta?: number[], rtiCoverge?: RtiCoverage[]) {

        super(categoryCode, defaultElement, defaultSupply, financeCode, flagMainSup, flagMandatory, parentSupplyTypeCode, parSupMand,
            selezionabile, supplyElementCode, supplyElementDesc, supplyTypeCode, supplyTypeDesc, tassoProdotto, taxPercentage, updatable);

        this._beneStrumentale = beneStrumentale;
        this._fireTheftCoverage = fireTheftCoverage;
        this._hasFireTheftInsurance = hasFireTheftInsurance;
        this._hasSatellite = hasSatellite;
        this._province = province;
        this._clazz = type;
        this._estensioneGaranziaProposta = estensioneGaranziaProposta;
        this._rtiCoverage = rtiCoverge;
    }

    get distances(): Distances[] {
        return this._distances;
    }

    set distances(value: Distances[]) {
        this._distances = value;
    }


    get beneStrumentale(): boolean {
        return this._beneStrumentale;
    }

    get fireTheftCoverage(): FireTheftCoverage[] {
        return this._fireTheftCoverage;
    }

    get hasFireTheftInsurance(): boolean {
        return this._hasFireTheftInsurance;
    }

    get hasSatellite(): boolean {
        return this._hasSatellite;
    }

    get clazz(): string {
        return this._clazz;
    }

    get province(): string[] {
        return this._province;
    }

    get estensioneGaranziaProposta(): number[] {
        return this._estensioneGaranziaProposta;
    }


    get rtiCoverage(): RtiCoverage[] {
        return this._rtiCoverage;
    }

    set rtiCoverage(value: RtiCoverage[]) {
        this._rtiCoverage = value;
    }

    set estensioneGaranziaProposta(value: number[]) {
        this._estensioneGaranziaProposta = value;
    }

    set province(value: string[]) {
        this._province = value;
    }

    set beneStrumentale(value: boolean) {
        this._beneStrumentale = value;
    }

    set fireTheftCoverage(value: FireTheftCoverage[]) {
        this._fireTheftCoverage = value;
    }

    set hasFireTheftInsurance(value: boolean) {
        this._hasFireTheftInsurance = value;
    }

    set hasSatellite(value: boolean) {
        this._hasSatellite = value;
    }

    set clazz(value: string) {
        this._clazz = value;
    }
}

export class FinancialConfigServiceResponseInnerFactory {


    static getInstance(obj: FinancialConfigServiceResponseInner): FinancialConfigServiceResponseInner {
        let finConfigSI = new FinancialConfigServiceResponseInner();
        let fireTheftCovF = new FireTheftCoverageFactory();
        let _utils = new UtilsService();

        if (obj) {
            if (obj.beneStrumentale != undefined) finConfigSI.beneStrumentale = obj.beneStrumentale;
            if (obj.fireTheftCoverage != undefined) finConfigSI.fireTheftCoverage = fireTheftCovF.getInstanceList(obj.fireTheftCoverage);
            if (obj.hasSatellite != undefined) finConfigSI.hasSatellite = obj.hasSatellite;
            if (obj.hasFireTheftInsurance != undefined) finConfigSI.hasFireTheftInsurance = obj.hasFireTheftInsurance;
            if (obj.province != undefined) finConfigSI.province = obj.province;
            if (obj.clazz != undefined) finConfigSI.clazz = obj.clazz;
            if (obj.distances != undefined) finConfigSI.distances = DistancesFactory.getInstanceList(obj.distances);
            if (obj.estensioneGaranziaProposta != undefined) finConfigSI.estensioneGaranziaProposta = obj.estensioneGaranziaProposta;
            if (obj.rtiCoverage != undefined) finConfigSI.rtiCoverage = RtiCoverageFactory.getInstanceList(obj.rtiCoverage);

        }

        return _utils.formatOBject4Output(finConfigSI);
    }

    getInstanceList(list: any): FinancialConfigServiceResponseInner[] {
        let financialServiceR = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialServiceR.push(FinancialConfigServiceResponseInnerFactory.getInstance(elem));
            });
        }
        return financialServiceR;
    }
}

