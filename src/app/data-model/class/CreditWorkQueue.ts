


// {
//     "tipoDiSoggetto": "Societa'",
//     "arrivo": "17:01:2012 11:00",
//     "assegnatario": "Strocchia",
//     "richiedente": "FORDENT FORNITURE DENTALI SRL",
//     "esposizione": "98453.61",
//     "riproposta": true,
//     "numNote": 0,
//     "numDocMancanti": 0,
//     "score": "null",
//     "numBlocchi": 1,
//     "proposta": 1000900097
// }



export class CreditWorkQueue {

    private _tipoDiSoggetto?:string;
    private _arrivo?: string;
    private _assegnatario?: string;
    private _richiedente?: string;
    private _esposizione?: number;
    private _riproposta?: boolean;
    private _numNote?: number;
    private _numDocMancanti?: number;
    private _score?: string;
    private _numBlocchi?: number;
    private _proposta?: string;
    private _processInstanceId?: number;
    private _prodotto?: string;
    private _blocked?: boolean;
    private _istruttoria?: string;
    private _lastNote?: string;
    private _urgente?: boolean;
    private _campagna?: string;
    private _descrizioneCampagna?: string;


    constructor(tipoDiSoggetto: string, arrivo: string, assegnatario: string, richiedente: string, esposizione: number,
                riproposta: boolean, numNote: number, numDocMancanti: number, score: string, numBlocchi: number,
                proposta: string, processInstanceId: number, prodotto: string, blocked: boolean, istruttoria: string,
                urgente: boolean, lastNote: string, campagna: string, descrizioneCampagna: string) {
        this._tipoDiSoggetto = tipoDiSoggetto;
        this._arrivo = arrivo;
        this._assegnatario = assegnatario;
        this._richiedente = richiedente;
        this._esposizione = esposizione;
        this._riproposta = riproposta;
        this._numNote = numNote;
        this._numDocMancanti = numDocMancanti;
        this._score = score;
        this._numBlocchi = numBlocchi;
        this._proposta = proposta;
        this._processInstanceId = processInstanceId;
        this._prodotto = prodotto;
        this._blocked = blocked;
        this._istruttoria = istruttoria;
        this._urgente = urgente;
        this._lastNote = lastNote;
        this._campagna = campagna;
        this._descrizioneCampagna = descrizioneCampagna;
    }




    /**@ignore nomi delle chiavi del backend, per tab relativo - devono corrispondere ai getter (autogenerare dalle var). Da aggiornare se variano. */
    static readonly keys = ["proposta", "richiedente", "tipoDiSoggetto", "arrivo", "istruttoria", "prodotto", "assegnatario", "score", "esposizione", "campagna" /*, "riproposta", "numNote", "numDocMancanti", "numBlocchi"*/];


    /**
     * BINDING BE
     * @returns {string}
     */

    get tipoDiSoggetto(): string {
        return this._tipoDiSoggetto;
    }

    set tipoDiSoggetto(value: string) {
        this._tipoDiSoggetto = value;
    }

    get arrivo(): string {
        return this._arrivo;
    }

    set arrivo(value: string) {
        this._arrivo = value;
    }

    get assegnatario(): string {
        return this._assegnatario;
    }

    set assegnatario(value: string) {
        this._assegnatario = value;
    }

    get richiedente(): string {
        return this._richiedente;
    }

    set richiedente(value: string) {
        this._richiedente = value;
    }

    get esposizione(): number {
        return this._esposizione;
    }

    set esposizione(value: number) {
        this._esposizione = value;
    }

    get riproposta(): boolean {
        return this._riproposta;
    }

    set riproposta(value: boolean) {
        this._riproposta = value;
    }

    get numNote(): number {
        return this._numNote;
    }

    set numNote(value: number) {
        this._numNote = value;
    }

    get numDocMancanti(): number {
        return this._numDocMancanti;
    }

    set numDocMancanti(value: number) {
        this._numDocMancanti = value;
    }

    get score(): string {
        return this._score;
    }

    set score(value: string) {
        this._score = value;
    }

    get numBlocchi(): number {
        return this._numBlocchi;
    }

    set numBlocchi(value: number) {
        this._numBlocchi = value;
    }

    get proposta(): string {
        return this._proposta;
    }

    set proposta(value: string) {
        this._proposta = value;
    }

    get processInstanceId(): number {
        return this._processInstanceId;
    }

    set processInstanceId(value: number) {
        this._processInstanceId = value;
    }

    get prodotto(): string {
        return this._prodotto;
    }

    set prodotto(value: string) {
        this._prodotto = value;
    }

    get istruttoria(): string {
        return this._istruttoria;
    }

    set istruttoria(value: string) {
        this._istruttoria = value;
    }

    get blocked(): boolean {
        return this._blocked;
    }

    set blocked(value: boolean) {
        this._blocked = value;
    }

    get urgente(): boolean {
        return this._urgente;
    }

    set urgente(value: boolean) {
        this._urgente = value;
    }

    get lastNote(): string {
        return this._lastNote;
    }

    set lastNote(value: string) {
        this._lastNote = value;
    }

    get campagna(): string {
        return this._campagna;
    }

    set campagna(value: string) {
        this._campagna = value;
    }
    get descrizioneCampagna(): string {
        return this._descrizioneCampagna;
    }

    set descrizioneCampagna(value: string) {
        this._descrizioneCampagna = value;
    }

    /**
     * BINDING HTML
     * @returns {string}
     */
}