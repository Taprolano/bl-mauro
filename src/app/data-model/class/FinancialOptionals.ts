// import {AdditionalProp} from './AdditionalProp';

export class FinancialOptionals {

    private _discountApplied: string;
    private _extraDescription: string;
    private _extraNetCost: number;
    private _extradiscountamt: number;
    private _extrarvadjust: number;
    private _extravatamt: number;
    private _extravatperc: number;
    private _vatCode: string;


    constructor(discountApplied: string, extraDescription: string, extraNetCost: number, extradiscountamt: number, extrarvadjust: number, extravatamt: number, extravatperc: number, vatCode: string) {
        this._discountApplied = discountApplied;
        this._extraDescription = extraDescription;
        this._extraNetCost = extraNetCost;
        this._extradiscountamt = extradiscountamt;
        this._extrarvadjust = extrarvadjust;
        this._extravatamt = extravatamt;
        this._extravatperc = extravatperc;
        this._vatCode = vatCode;
    }

    get discountApplied(): string {
        return this._discountApplied;
    }

    get extraDescription(): string {
        return this._extraDescription;
    }

    get extraNetCost(): number {
        return this._extraNetCost;
    }

    get extradiscountamt(): number {
        return this._extradiscountamt;
    }

    get extrarvadjust(): number {
        return this._extrarvadjust;
    }

    get extravatamt(): number {
        return this._extravatamt;
    }

    get extravatperc(): number {
        return this._extravatperc;
    }

    get vatCode(): string {
        return this._vatCode;
    }


    set discountApplied(value: string) {
        this._discountApplied = value;
    }

    set extraDescription(value: string) {
        this._extraDescription = value;
    }

    set extraNetCost(value: number) {
        this._extraNetCost = value;
    }

    set extradiscountamt(value: number) {
        this._extradiscountamt = value;
    }

    set extrarvadjust(value: number) {
        this._extrarvadjust = value;
    }

    set extravatamt(value: number) {
        this._extravatamt = value;
    }

    set extravatperc(value: number) {
        this._extravatperc = value;
    }

    set vatCode(value: string) {
        this._vatCode = value;
    }

    public requestParameters(): IFinancialOptionals {
        let requestBody: IFinancialOptionals = {
            discountApplied: this._discountApplied,
            extraDescription: this._extraDescription,
            extraNetCost: this._extraNetCost,
            extradiscountamt: this._extradiscountamt,
            extrarvadjust: this._extrarvadjust,
            extravatamt: this._extravatamt,
            extravatperc: this._extravatperc,
            vatCode: this._vatCode
        };

        return requestBody;
    }
}

export class FinancialOptionalsFactory {

    static getInstance(obj: FinancialOptionals): FinancialOptionals {
        return new FinancialOptionals(obj.discountApplied, obj.extraDescription, obj.extraNetCost, obj.extradiscountamt, obj.extrarvadjust, obj.extravatamt, obj.extravatperc, obj.vatCode);
    }

    static getInstanceList(list: any): FinancialOptionals[] {
        let financialOptionalsList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialOptionalsList.push(this.getInstance(elem));
            });
        }
        return financialOptionalsList;
    }

    static getInstanceListParam(list: any): IFinancialOptionals[] {
        let financialOptionalsList = [];
        if (list && list.length > 0) {
            list.forEach(elem => {
                financialOptionalsList.push(this.getInstance(elem).requestParameters());
            });
        }
        return financialOptionalsList;
    }
}

export interface IFinancialOptionals {

    discountApplied: string;
    extraDescription: string;
    extraNetCost: number;
    extradiscountamt: number;
    extrarvadjust: number;
    extravatamt: number;
    extravatperc: number;
    vatCode: string;

}