import {Version} from "./Version";
import {IVersion} from "../interface/IVersion";
import {UtilsService} from "../../shared/services/utils.service";
import {Proposal} from "./Proposal";
import {IProposals} from "../interface/IProposals";
import {LoggerService} from '../../shared/services/logger.service';

export abstract class Normalizer {

    static getNormalizedData(version: Version) : IVersion {
/*
    baumuster: "16933212092"
    modelDescription: "A 180 Coupé Executive"
    netCost: 15733.33
    pricelistDeadline: "29-12-2009"
*/
        // console.log(`[${Normalizer.name}] getNormalizedData version:`, version);
        let iVersion: IVersion= {
            productCode: null,
            version: null,
            fuel: null,
            catalog:null,
            price: null,
            forceSecondHand: false,
        };
        let _utils:UtilsService = new UtilsService();

        iVersion.productCode = !_utils.isVoid(version.baumuster)
            ?version.baumuster:null;
        iVersion.catalog = !_utils.isVoid(version.pricelistDeadline)
            ?version.pricelistDeadline:null;
        iVersion.version = !_utils.isVoid(version.modelDescription)
            ?version.modelDescription:null;
        iVersion.price = !_utils.isVoid(version.netCost)
            ?version.netCost:null;
        iVersion.fuel = !_utils.isVoid(version.fuelTypeCode)
            ?version.fuelTypeCode:null;
        iVersion.forceSecondHand = !_utils.isVoid(version.forceSecondHand)
            ?version.forceSecondHand: false;



        // console.log(`[${Normalizer.name}] getNormalizedData iVersion:`, iVersion);

        return iVersion;

    }

    static getListNormalizedData(versions: Version[]): IVersion[] {
        let logger: LoggerService = new LoggerService();
        // console.log(`[${Normalizer.name}] getListNormalizedData versions:`, versions);

        let iVersions:IVersion[];
        let _utils:UtilsService = new UtilsService();

        if(!_utils.isVoid(versions) && versions.length>0) {

            iVersions = [];

            for (let v of versions) {
                iVersions.push(this.getNormalizedData(v));
            }
        }

        logger.logInfo('Normalizer', 'getListNormalizedData', `iVersions:`, iVersions)
        // console.log(`[${Normalizer.name}] getListNormalizedData iVersions:`, iVersions);

        return iVersions;

    }

    static getNormalizedProposalData(proposal: Proposal): IProposals {
        
        let iProposal = {
            asset: null,
            proposal: null,
            contract: null,
            subject: null,
            state: null,
            date: null,
            deadline: null,
            priority: null,
            seller: null
        };

        let _utils:UtilsService = new UtilsService();

        iProposal.proposal = !_utils.isVoid(proposal.proposta) ?proposal.proposta:null;
        iProposal.contract = !_utils.isVoid(proposal.contratto) ?proposal.contratto:null;
        iProposal.subject = !_utils.isVoid(proposal.soggetto) ?proposal.soggetto:null;
        iProposal.state = !_utils.isVoid(proposal.stato) ?proposal.stato:null;
        iProposal.date = !_utils.isVoid(proposal.data) ?proposal.data:null;
        iProposal.deadline = !_utils.isVoid(proposal.scadenza) ?proposal.scadenza:null;
        iProposal.seller = !_utils.isVoid(proposal.venditore) ?proposal.venditore:null;

        return iProposal;
    }

    static getListProposalNormalized(proposalList: Proposal[]): IProposals[] {

        let iProposals: IProposals[];
        let _utils:UtilsService = new UtilsService();

        if(!_utils.isVoid(proposalList) && proposalList.length>0) {
            
            iProposals = [];

            for (let prop of proposalList) {
                iProposals.push(this.getNormalizedProposalData(prop))
            }
        }

        return iProposals;
    }



}
