import {NotificationMessage} from "../../shared/components/notification/NotificationMessage";

export class BeResponse<T> {
    private _code: number;
    private _message: NotificationMessage;
    private _object?: T;

    constructor(code: number, message: NotificationMessage, object:T = null) {
        this._code = code;
        this._message = message;
        this._object = !!object ?object: null;
    };

    get code(): number {
        return this._code;
    }

    set code(value: number) {
        this._code = value;
    }

    get message(): NotificationMessage {
        return this._message;
    }

    set message(value: NotificationMessage) {
        this._message = value;
    }

    get object(): T {
        return this._object;
    }

    set object(value: T) {
        this._object = value;
    }

}
