import {HttpParams} from '@angular/common/http';
import {LoggerService} from '../../shared/services/logger.service';
import {UtilsService} from '../../shared/services/utils.service';

export class AssetOptionalsReqParams {

  private _baumuster: string;
  private _code: string;
  private _description: string;
  private _page: number;
  private _size: number;
  private _sort: string;
  
  private _utils: UtilsService = new UtilsService();

  /**
   * 
   * Modello dati request per servizio /assets/optionals
   */
  constructor(baumuster: string, private _logger: LoggerService) {
    this._baumuster = baumuster;
  }

  get sort(): string {
    return this._sort;
  }
  set sort(value: string) {
    this._sort = value;
  }

  get size(): number {
    return this._size;
  }
  set size(value: number) {
    this._size = value;
  }

  get page(): number {
    return this._page;
  }
  set page(value: number) {
    this._page = value;
  }
  
  get description(): string {
    return this._description;
  }
  set description(value: string) {
    this._description = value;
  }

  get code(): string {
    return this._code;
  }
  set code(value: string) {
    this._code = value;
  }

  get baumuster(): string {
    return this._baumuster;
  }
  set baumuster(value: string) {
    this._baumuster = value;
  }

  /**
   * imposta i parametri per la request per l'api /assets/optionals
   */
  requestParameters(): HttpParams {
    let params = new HttpParams(); // set of HttpParams return an immutable object

    this._logger.logInfo(AssetOptionalsReqParams.name, 'requestParameters', this);

    if(!this._utils.isVoid(this._baumuster)) params = params.set('baumuster',this._baumuster);
    if(!this._utils.isVoid(this._code)) params = params.set('code',this._code);
    if(!this._utils.isVoid(this._description)) params = params.set('description',this._description);
    if(!this._utils.isVoid(this._page)) params = params.set('page',this._page.toString());
    if(!this._utils.isVoid(this._size)) params = params.set('size',this._size.toString());
    if(!this._utils.isVoid(this._sort)) params = params.set('sort',this._sort);

    return params;
  }
}