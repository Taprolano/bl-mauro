import {FinancialConfigServiceResponseInner, FinancialConfigServiceResponseInnerFactory} from './FinancialConfigServiceResponseInner';

export class FinancialConfigServiceResponse {

    private _assetType?: string;
    private _brandId?: string;
    private _contractLength?: number;
    private _distance?: number;
    private _financiedAmount?: number;
    private _frequency?: number;
    private _service?: FinancialConfigServiceResponseInner;
    private _tan?: number;
    private _totalAmount?: number;
    private _vatPercentage?: number;

    constructor(assetType?: string, brandId?: string, contractLength?: number, distance?: number,
                financiedAmount?: number, frequency?: number, service?: FinancialConfigServiceResponseInner,
                tan?: number, totalAmount?: number, vatPercentage?: number,) {
        this._assetType = assetType;
        this._brandId = brandId;
        this._contractLength = contractLength;
        this._distance = distance;
        this._financiedAmount = financiedAmount;
        this._frequency = frequency;
        this._service = service;
        this._tan = tan;
        this._totalAmount = totalAmount;
        this._vatPercentage = vatPercentage;
        this._distance = distance;
    }


    get assetType(): string {
        return this._assetType;
    }

    get brandId(): string {
        return this._brandId;
    }

    get contractLength(): number {
        return this._contractLength;
    }

    get distance(): number {
        return this._distance;
    }

    get financiedAmount(): number {
        return this._financiedAmount;
    }

    get frequency(): number {
        return this._frequency;
    }

    get service(): FinancialConfigServiceResponseInner {
        return this._service;
    }

    get tan(): number {
        return this._tan;
    }

    get totalAmount(): number {
        return this._totalAmount;
    }

    get vatPercentage(): number {
        return this._vatPercentage;
    }


    set assetType(value: string) {
        this._assetType = value;
    }

    set brandId(value: string) {
        this._brandId = value;
    }

    set contractLength(value: number) {
        this._contractLength = value;
    }

    set distance(value: number) {
        this._distance = value;
    }

    set financiedAmount(value: number) {
        this._financiedAmount = value;
    }

    set frequency(value: number) {
        this._frequency = value;
    }

    set service(value: FinancialConfigServiceResponseInner) {
        this._service = value;
    }

    set tan(value: number) {
        this._tan = value;
    }

    set totalAmount(value: number) {
        this._totalAmount = value;
    }

    set vatPercentage(value: number) {
        this._vatPercentage = value;
    }
}

export class FinancialConfigServiceResponseFactory {

    static getInstance(obj: FinancialConfigServiceResponse): FinancialConfigServiceResponse {

        let finConfigS = new FinancialConfigServiceResponse();


        if (obj) {
            if (obj.assetType != undefined) finConfigS.assetType = obj.assetType;
            if (obj.brandId != undefined) finConfigS.brandId = obj.brandId;
            if (obj.contractLength != undefined) finConfigS.contractLength = obj.contractLength;
            if (obj.distance != undefined) finConfigS.distance = obj.distance;
            if (obj.financiedAmount != undefined) finConfigS.financiedAmount = obj.financiedAmount;
            if (obj.service != undefined) finConfigS.service = FinancialConfigServiceResponseInnerFactory.getInstance(obj.service);
            if (obj.tan != undefined) finConfigS.tan = obj.tan;
            if (obj.totalAmount != undefined) finConfigS.totalAmount = obj.totalAmount;
            if (obj.vatPercentage != undefined) finConfigS.vatPercentage = obj.vatPercentage;


        }

        return finConfigS;
    }

}

