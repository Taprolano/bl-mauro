import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HeaderComponent} from "../../shared/components/header/header.component";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../shared/shared.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MbzDashboardModule} from "../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../proposals/proposals.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {createTranslateLoader} from "../../app.module";
import {AppRoutingModule} from "../../app-routing.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {environment} from "../../../environments/environment.svil";
import {AppComponent} from "../../app.component";
import {AuthGuard} from "../../shared/guard";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CreditCorporateQueue} from "./CreditCorporateQueue";
import {DirectAccessGuard} from "../../shared/guard/direct-access/direct-access.guard";



describe('CreditCorporateQueue', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('constructor and getter',() =>{
       let ccqueue = new CreditCorporateQueue('arrivo','richiedente','esposizione',10,
           'tipoProposta','prodotto',100);

        expect(ccqueue.arrivo).toEqual('arrivo');
        expect(ccqueue.richiedente).toEqual('richiedente');
        expect(ccqueue.esposizione).toEqual('esposizione');
        expect(ccqueue.numBlocchi).toEqual(10);
        expect(ccqueue.tipoProposta).toEqual('tipoProposta');
        expect(ccqueue.prodotto).toEqual('prodotto');
        expect(ccqueue.proposta).toEqual(100);

    });

    it('setter',() =>{
        let ccqueue = new CreditCorporateQueue();

        ccqueue.arrivo = 'arrivo';
        ccqueue.richiedente = 'richiedente';
        ccqueue.esposizione = 'esposizione';
        ccqueue.numBlocchi = 10;
        ccqueue.tipoProposta = 'tipoProposta';
        ccqueue.prodotto = 'prodotto';
        ccqueue.proposta = 10;


        expect(ccqueue.arrivo).toEqual('arrivo');
        expect(ccqueue.richiedente).toEqual('richiedente');
        expect(ccqueue.esposizione).toEqual('esposizione');
        expect(ccqueue.numBlocchi).toEqual(10);
        expect(ccqueue.tipoProposta).toEqual('tipoProposta');
        expect(ccqueue.prodotto).toEqual('prodotto');
        expect(ccqueue.proposta).toEqual(10);

    });

});
