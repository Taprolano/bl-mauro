export class CommercialAmounts {

  private _ipt?: number;
  private _mss?: number;

  constructor(ipt: number, mss: number){
    this._ipt = ipt;
    this._mss = mss;
  }

  /**
   * BINDING SERVICE BE
   * 
   */

  get ipt(): number {
    return this._ipt;
  }
  set ipt(value: number) {
    this._ipt = value;
  }

  get mss(): number {
    return this._mss;
  }
  set mss(value: number) {
    this._mss = value;
  }

  /**
   * BINDINGS HTML
   */

  get commercialIpt(): number {
    return this._ipt;
  }
  set commercialIpt(value: number) {
    this._ipt = value;
  }

  get messaSuStrada(): number {
    return this._mss;
  }
  set messaSuStrada(value: number) {
    this._mss = value;
  }

}

export class CommercialAmountsFactory {

  getInstance(obj: CommercialAmounts): CommercialAmounts {
    return new CommercialAmounts(obj.ipt, obj.mss);
  }
}