export class NumberProposalTab {

  private _numAssegnateAMe?: number;
  private _numBloccate?: number;
  private _numDaAttivare?: number;
  private _numDdm?: number;
  private _numDocumentazionIncompleta?: number;
  private _numEscluseRifRil?: number;
  private _numFirmaCartacea?: number;
  private _numFirmaGrafometrica?: number;
  private _numInControlloDocumentale?: number;
  private _numInInstruttoria?: number;
  private _numLavorabili?: number;
  private _numRifRil?: number;
  private _numRiproposta?: number;
  private _numSecondaApprovazione?: number;
  private _numSospese?: number;
  private _numUrgenti?: number;
  private _numTotali?: number;
  
  constructor(numAssegnateAMe: number,
    numBloccate: number,
    numDaAttivare: number,
    numDdm: number,
    numDocumentazionIncompleta: number,
    numEscluseRifRil: number,
    numFirmaCartacea: number,
    numFirmaGrafometrica: number,
    numInControlloDocumentale: number,
    numInInstruttoria: number,
    numLavorabili: number,
    numRifRil: number,
    numRiproposta: number,
    numSecondaApprovazione: number,
    numSospese: number,
    numUrgenti: number,
    numTotali: number) {

      this._numAssegnateAMe = numAssegnateAMe;
      this._numBloccate = numBloccate;
      this._numDaAttivare = numDaAttivare;
      this._numDdm = numDdm;
      this._numDocumentazionIncompleta = numDocumentazionIncompleta;
      this._numEscluseRifRil = numEscluseRifRil; 
      this._numFirmaCartacea = numFirmaCartacea;
      this._numFirmaGrafometrica = numFirmaGrafometrica;
      this._numInControlloDocumentale = numInControlloDocumentale; 
      this._numInInstruttoria = numInInstruttoria; 
      this._numLavorabili = numLavorabili;
      this._numRifRil = numRifRil;
      this._numRiproposta = numRiproposta;
      this._numSecondaApprovazione = numSecondaApprovazione;
      this._numSospese = numSospese;
      this._numUrgenti = numUrgenti;
      this._numTotali = numTotali;

  }

  /**
     * BINDING BE
     * @returns {string}
     */


  public get numAssegnateAMe(): number {
    return this._numAssegnateAMe;
  }
  public set numAssegnateAMe(value: number) {
    this._numAssegnateAMe = value;
  }

  public get numBloccate(): number {
    return this._numBloccate;
  }
  public set numBloccate(value: number) {
    this._numBloccate = value;
  }

  public get numDaAttivare(): number {
    return this._numDaAttivare;
  }
  public set numDaAttivare(value: number) {
    this._numDaAttivare = value;
  }

  public get numDdm(): number {
    return this._numDdm;
  }
  public set numDdm(value: number) {
    this._numDdm = value;
  }

  public get numDocumentazionIncompleta(): number {
    return this._numDocumentazionIncompleta;
  }
  public set numDocumentazionIncompleta(value: number) {
    this._numDocumentazionIncompleta = value;
  }

  public get numEscluseRifRil(): number {
    return this._numEscluseRifRil;
  }
  public set numEscluseRifRil(value: number) {
    this._numEscluseRifRil = value;
  }

  public get numFirmaCartacea(): number {
    return this._numFirmaCartacea;
  }
  public set numFirmaCartacea(value: number) {
    this._numFirmaCartacea = value;
  }

  public get numFirmaGrafometrica(): number {
    return this._numFirmaGrafometrica;
  }
  public set numFirmaGrafometrica(value: number) {
    this._numFirmaGrafometrica = value;
  }

  public get numInControlloDocumentale(): number {
    return this._numInControlloDocumentale;
  }
  public set numInControlloDocumentale(value: number) {
    this._numInControlloDocumentale = value;
  }
  
  public get numInInstruttoria(): number {
    return this._numInInstruttoria;
  }
  public set numInInstruttoria(value: number) {
    this._numInInstruttoria = value;
  }
  
  public get numLavorabili(): number {
    return this._numLavorabili;
  }
  public set numLavorabili(value: number) {
    this._numLavorabili = value;
  }
  
  public get numRifRil(): number {
    return this._numRifRil;
  }
  public set numRifRil(value: number) {
    this._numRifRil = value;
  }
  
  public get numRiproposta(): number {
    return this._numRiproposta;
  }
  public set numRiproposta(value: number) {
    this._numRiproposta = value;
  }
  
  public get numSecondaApprovazione(): number {
    return this._numSecondaApprovazione;
  }
  public set numSecondaApprovazione(value: number) {
    this._numSecondaApprovazione = value;
  }
  
  public get numSospese(): number {
    return this._numSospese;
  }
  public set numSospese(value: number) {
    this._numSospese = value;
  }

  public get numUrgenti(): number {
    return this._numUrgenti;
  }
  public set numUrgenti(value: number) {
    this._numUrgenti = value;
  }

  public get numTotali(): number {
    return this._numTotali;
  }
  public set numTotali(value: number) {
    this._numTotali = value;
  }

  /**
     * BINDING HTML
     * @returns {string}
     */
}