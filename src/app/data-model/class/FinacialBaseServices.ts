
export class FinacialBaseServices{

    private _categoryCode : string;
    private _defaultElement : boolean;
    private _defaultSupply : boolean;
    private _financeCode : string;
    private _flagMainSup : string;
    private _flagMandatory : string;
    private _parentSupplyTypeCode : string;
    private _parSupMand : string;
    private _retrieveVat?: boolean;
    private _selezionabile : string;
    private _supplyElementCode : string;
    private _supplyElementDesc : string;
    private _supplyTypeCode : string;
    private _supplyTypeDesc : string;
    private _tassoProdotto : boolean;
    private _taxPercentage : number;
    private _updatable : boolean;


    /**
     *
     * @param {string} categoryCode
     * @param {boolean} defaultElement
     * @param {boolean} defaultSupply
     * @param {string} financeCode
     * @param {string} flagMainSup
     * @param {string} flagMandatory
     * @param {string} parentSupplyTypeCode
     * @param {string} parSupMand
     * @param {boolean} retrieveVat
     * @param {string} selezionabile
     * @param {string} supplyElementCode
     * @param {string} supplyElementDesc
     * @param {string} supplyTypeCode
     * @param {string} supplyTypeDesc
     * @param {boolean} tassoProdotto
     * @param {number} taxPercentage
     * @param {boolean} updatable
     */
    constructor(categoryCode?: string, defaultElement?: boolean, defaultSupply?: boolean, financeCode?: string, flagMainSup?: string,
                flagMandatory?: string, parentSupplyTypeCode?: string, parSupMand?: string, selezionabile?: string,
                supplyElementCode?: string, supplyElementDesc?: string, supplyTypeCode?: string, supplyTypeDesc?: string,
                tassoProdotto?: boolean, taxPercentage?: number, updatable?: boolean, retrieveVat?: boolean) {

        this._categoryCode = categoryCode;
        this._defaultElement = defaultElement;
        this._defaultSupply = defaultSupply;
        this._financeCode = financeCode;
        this._flagMainSup = flagMainSup;
        this._flagMandatory = flagMandatory;
        this._parentSupplyTypeCode = parentSupplyTypeCode;
        this._parSupMand = parSupMand;
        this._retrieveVat = retrieveVat;
        this._selezionabile = selezionabile;
        this._supplyElementCode = supplyElementCode;
        this._supplyElementDesc = supplyElementDesc;
        this._supplyTypeCode = supplyTypeCode;
        this._supplyTypeDesc = supplyTypeDesc;
        this._tassoProdotto = tassoProdotto;
        this._taxPercentage = taxPercentage;
        this._updatable = updatable;
    }

    /** @returns {string} */
    get categoryCode(): string {
        return this._categoryCode;
    }

    /** @returns {boolean} */
    get defaultElement(): boolean {
        return this._defaultElement;
    }

    /** @returns {boolean} */
    get defaultSupply(): boolean {
        return this._defaultSupply;
    }

    /** @returns {string} */
    get financeCode(): string {
        return this._financeCode;
    }

    /** @returns {string} */
    get flagMainSup(): string {
        return this._flagMainSup;
    }

    /** @returns {string} */
    get flagMandatory(): string {
        return this._flagMandatory;
    }

    /** @returns {string} */
    get parSupMand(): string {
        return this._parSupMand;
    }

    /** @returns {string} */
    get parentSupplyTypeCode(): string {
        return this._parentSupplyTypeCode;
    }

    /** @returns {boolean} */
    get retrieveVat(): boolean {
        return this._retrieveVat;
    }

    /** @returns {string} */
    get selezionabile(): string {
        return this._selezionabile;
    }

    /** @returns {string} */
    get supplyElementCode(): string {
        return this._supplyElementCode;
    }

    /** @returns {string} */
    get supplyElementDesc(): string {
        return this._supplyElementDesc;
    }

    /** @returns {string} */
    get supplyTypeCode(): string {
        return this._supplyTypeCode;
    }

    /** @returns {string} */
    get supplyTypeDesc(): string {
        return this._supplyTypeDesc;
    }

    /** @returns {boolean} */
    get tassoProdotto(): boolean {
        return this._tassoProdotto;
    }

    /** @returns {number} */
    get taxPercentage(): number {
        return this._taxPercentage;
    }

    /** @returns {boolean} */
    get updatable(): boolean {
        return this._updatable;
    }

    /** @param {string} value */
    set categoryCode(value: string) {
        this._categoryCode = value;
    }

    /** @param {boolean} value */
    set defaultElement(value: boolean) {
        this._defaultElement = value;
    }

    /** @param {boolean} value */
    set defaultSupply(value: boolean) {
        this._defaultSupply = value;
    }

    /** @param {string} value */
    set financeCode(value: string) {
        this._financeCode = value;
    }

    /** @param {string} value */
    set flagMainSup(value: string) {
        this._flagMainSup = value;
    }

    /** @param {string} value */
    set flagMandatory(value: string) {
        this._flagMandatory = value;
    }

    /** @param {string} value */
    set parSupMand(value: string) {
        this._parSupMand = value;
    }

    /** @param {string} value */
    set parentSupplyTypeCode(value: string) {
        this._parentSupplyTypeCode = value;
    }


    /** @param {boolean} value */
    set retrieveVat(value: boolean) {
        this._retrieveVat = value;
    }

    /** @param {string} value */
    set selezionabile(value: string) {
        this._selezionabile = value;
    }



    /** @param {string} value */
    set supplyElementCode(value: string) {
        this._supplyElementCode = value;
    }


    /** @param {string} value */
    set supplyElementDesc(value: string) {
        this._supplyElementDesc = value;
    }

    /** @param {string} value */
    set supplyTypeCode(value: string) {
        this._supplyTypeCode = value;
    }

    /** @param {string} value */
    set supplyTypeDesc(value: string) {
        this._supplyTypeDesc = value;
    }

    set tassoProdotto(value: boolean) {
        this._tassoProdotto = value;
    }

    /** @param {number} value */
    set taxPercentage(value: number) {
        this._taxPercentage = value;
    }

    /** @param {boolean} value */
    set updatable(value: boolean) {
        this._updatable = value;
    }


}

