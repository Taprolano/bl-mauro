import {HttpParams} from "@angular/common/http";
import {UtilsService} from "../../shared/services/utils.service";

export class ProposalConsultationRequestParameters{
    private _idProposal: string;
    private _processInstanceID: number;
    private _tab?: string;

    private _utils: UtilsService = new UtilsService();

    constructor(idProposal: string, processInstanceID: number, tab?: string) {
        this._idProposal = idProposal;
        this._processInstanceID = processInstanceID;
        this._tab = tab;
    }


    requestParameters(): HttpParams {
        let params = new HttpParams();

        if (!this._utils.isVoid(this._idProposal)) params = params.set('idProposal', this._idProposal);
        if (!this._utils.isVoid(this._processInstanceID)) params = params.set('processInstanceID', this._processInstanceID.toString());
        if (!this._utils.isVoid(this._tab)) params = params.set('tab', this._tab);

        return params;
    }

    /** @returns {string} */
    get idProposal(): string {
        return this._idProposal;
    }

    /** @returns {number} */
    get processInstanceID(): number {
        return this._processInstanceID;
    }

    /** @param {string} value */
    set idProposal(value: string) {
        this._idProposal = value;
    }

    /** @param {number} value */
    set processInstanceID(value: number) {
        this._processInstanceID = value;
    }

    /** @returns {string} */
    get tab(): string {
        return this._tab;
    }

    /** @param {string} value */
    set tab(value: string) {
        this._tab = value;
    }
}