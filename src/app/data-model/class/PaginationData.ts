/**
 * Informazioni sulla paginazione della richiesta effettuata
 */
export class PaginationData{
  /** Pagina descritta dalla risposta */
  currentPage?:number;
  /** Numero di pagine complessivo */
  totalPages:number;
  /** NUmero complessivo di elementi tra tutte le pagine */
  totalElements:number;
  /** Dimensione di ogni pagina */
  pageSize?:number;
  /** @ignore ??? */
  offset?:number;

  /**
   * @param {number} totalElements
   * @param {number} totalPages
   * @param {number} currentPage
   * @param {number} pageSize
   * @param {number} offset
   */
  constructor(totalElements: number, totalPages: number, currentPage?: number, pageSize?: number, offset?: number) {
      this.currentPage = currentPage;
      this.totalPages = totalPages;
      this.totalElements = totalElements;
      this.pageSize = pageSize;
      this.offset = offset;
  }

}