import {UtilsService} from '../../shared/services/utils.service';
import {LoggerService} from '../../shared/services/logger.service';
import {HttpParams} from '@angular/common/http';

export class ListValuesReqParams {
  
  private _creationTime: number;
  private _id: string;
  private _lastAccessedTime: number;
  private _listCode: string;
  private _maxInactiveInterval: number;
  private _new: boolean;
  private _valueNames: string[];

  private _utils: UtilsService = new UtilsService();

  /**
   * 
   * Modello dati request per servizio /listofvalues
   */
  constructor(private _logger: LoggerService) {}

  get valueNames(): string[] {
    return this._valueNames;
  }
  set valueNames(value: string[]) {
    this._valueNames = value;
  }

  get new(): boolean {
    return this._new;
  }
  set new(value: boolean) {
    this._new = value;
  }

  get maxInactiveInterval(): number {
    return this._maxInactiveInterval;
  }
  set maxInactiveInterval(value: number) {
    this._maxInactiveInterval = value;
  }

  get listCode(): string {
    return this._listCode;
  }
  set listCode(value: string) {
    this._listCode = value;
  }

  get lastAccessedTime(): number {
    return this._lastAccessedTime;
  }
  set lastAccessedTime(value: number) {
    this._lastAccessedTime = value;
  }

  get id(): string {
    return this._id;
  }
  set id(value: string) {
    this._id = value;
  }

  get creationTime(): number {
    return this._creationTime;
  }
  set creationTime(value: number) {
    this._creationTime = value;
  }

  /**
   * imposta i parametri per la request per l'api /listofvalues
   */
  requestParameters(): HttpParams {
    let params = new HttpParams(); // set of HttpParams return an immutable object

    this._logger.logInfo(ListValuesReqParams.name, 'requestParameters', this);

    if(!this._utils.isVoid(this._creationTime)) params = params.set('creationTime', this._creationTime.toString());
    if(!this._utils.isVoid(this._id)) params = params.set('id', this._id);
    if(!this._utils.isVoid(this._lastAccessedTime)) params = params.set('lastAccessedTime', this._lastAccessedTime.toString());
    if(!this._utils.isVoid(this._listCode)) params = params.set('listCode', this._listCode);
    if(!this._utils.isVoid(this._maxInactiveInterval)) params = params.set('_maxInactiveInterval', this._maxInactiveInterval.toString());
    if(!this._utils.isVoid(this._new)) params = params.set('new', this._new.toString());
    if(!this._utils.isVoid(this._valueNames)) params = params.set('valueNames', this._valueNames.toString());

    return params;
  }
}