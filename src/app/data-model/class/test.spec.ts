import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HeaderComponent} from '../../shared/components/header/header.component';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from '../../shared/shared.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MbzDashboardModule} from '../../mbz-dashboard/mbz-dashboard.module';
import {ProposalsModule} from '../../proposals/proposals.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {createTranslateLoader} from '../../app.module';
import {AppRoutingModule} from '../../app-routing.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../../../environments/environment.svil';
import {AppComponent} from '../../app.component';
import {AuthGuard} from '../../shared/guard';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ProposalListRequestParameters} from './ProposalListRequestParameters';
import {LoggerService} from '../../shared/services/logger.service';
import {VersionRequestParameters} from './VersionRequestParameters';
import {UtilsService} from '../../shared/services/utils.service';
import {AssetOptionals, AssetOptionalsFactory, Pageable} from './AssetOptionals';
import {AssetOptionalsReqParams} from './AssetOptionalsReqParams';
import {AssetsChassisReqParams} from './AssetsChassisReqParams';
import {CommercialAmounts, CommercialAmountsFactory} from './CommercialAmounts';
import {CommercialAmountsReqParams} from './CommercialAmountsReqParams';
import {ListValuesReqParams} from './ListValuesReqParams';
import {ListValues, ValueList} from './ListValues';
import {Proposal} from './Proposal';
import {Version} from './Version';
import {Router} from '@angular/router';
import {HeaderService} from '../../shared/services/header.service';
import {MalihuScrollbarService} from 'ngx-malihu-scrollbar';
import {ProposalsComponent} from '../../proposals/proposals.component';
import {ProposalCounter} from './ProposalCounter';
import {AssetsChassis, AssetsChassisFactory} from './AssetsChassis';
import {CreditCorporateQueue} from './CreditCorporateQueue';
import {CreditWorkQueue} from './CreditWorkQueue';
import {DealerContactQueue} from './DealerContactQueue';
import {ImpUser} from './ImpUser';
import {ImpUserRequestParams} from './ImpUserRequestParams';
import {IVersion} from '../interface/IVersion';
import {NumberProposalHeader} from './NumberProposalHeader';
import {NumberProposalTab} from './NumberProposalTab';
import {DirectAccessGuard} from '../../shared/guard/direct-access/direct-access.guard';
import {PaginationData} from './PaginationData';
import {PaginatedObject} from './PaginatedObject';
import {PayoutQueue} from './PayoutQueue';
import {NotificationMessage} from '../../shared/components/notification/NotificationMessage';
import {BeResponse} from './Response';
import {BlockedFields, BlockedFieldsFactory} from './Consultation/BlockFields';
import {Campaign, CampaignFactory} from './Consultation/Campaign';
import {
    ConsultationCommercialQueue,
    ConsultationCommercialQueueFactory,
    ConsultationCommOptional, ConsultationCommOptionalFactory
} from './Consultation/ConsultationCommercialQueue';
import {ConsultationFinancialQueue, ConsultationFinancialQueueFactory} from './Consultation/ConsultationFinancialQueue';
import {FinancialProcuct, FinancialProcuctFactory} from './Consultation/FinancialProcuct';
import {ServiceEntity, ServiceEntityFactory} from './Consultation/ServiceEntity';
import {ProvvigioniDTO, ProvvigioniDTOFactory} from './Consultation/ProvvigioniDTO';
import {Rate, RateFactory} from './Consultation/Rate';
import {ConsultationHeaderQueue, ConsultationHeaderQueueFactory} from './Consultation/ConsultationHeaderQueue';
import {ConsultationProposalProdQueueFactory, ConsultationProposalProductQueue} from './Consultation/ConsultationProposalProductQueue';
import {ConsultationProposalQueue, ConsultationProposalQueueFactory} from './Consultation/ConsultationProposalQueue';
import {ConsultationSubjectQueue, ConsultationSubjectQueueFactory} from './Consultation/ConsultationSubjectQueue';
import {ConsultationQueue, ConsultationQueueFactory} from './Consultation/ConsultationQueue';
import {FinancialAttachment, FinancialAttachmentFactory} from './Consultation/FinancialAttachment';
import {FinancialServicesInner, FinancialServicesInnerFactory} from './Consultation/FinancialServiceInner';
import any = jasmine.any;
import {ProposalConsultationRequestParameters} from './ProposalConsultationRequestParameters';
import {Generics} from './utils/Generics';
import {GdAutoModel} from '../../../data_mock/GdAutoModel';
import {TdProposalEntry} from '../../../data_mock/TdProposalEntry';


describe('test', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    let _utils: UtilsService = new UtilsService();
    let _logger: LoggerService = new LoggerService();

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should VersionRequestParameters', () => {

        let utils: UtilsService = new UtilsService();
        expect(utils).toBeDefined();

        let logger: LoggerService = new LoggerService();
        expect(logger).toBeDefined();

    });

    it('should AssetOption', () => {

        let content;
        let pageable: Pageable;
        let totalPages: number;
        let totalElements: number;

        let ao = new AssetOptionals(content, pageable, totalPages, totalElements);
        expect(ao).toBeDefined();

        ao.content = [10];
        expect(ao.content[0]).toEqual(10);

        ao.pageable = pageable;
        expect(ao.pageable).toEqual(pageable);

        ao.totalPages = 10;
        expect(ao.totalPages).toEqual(10);

        ao.optionalsContent = [10];
        expect(ao.optionalsContent[0]).toEqual(10);

        ao.optionalsPageable = pageable;
        expect(ao.optionalsPageable).toEqual(pageable);

        ao.optionalsTotalPages = 10;
        expect(ao.optionalsTotalPages).toEqual(10);

        ao.totalElements = 10;
        expect(ao.totalElements).toEqual(10);

        ao.optionalsTotalElements = 10;
        expect(ao.optionalsTotalElements).toEqual(10);

        ao.optionalsTotalPages = 10;
        expect(ao.optionalsTotalElements).toEqual(10);

        let aof = new AssetOptionalsFactory();
        expect(aof).toBeDefined();

    });

    it('should AssetOptionalsReqParams', () => {
        let logger: LoggerService = new LoggerService();
        let assetReq = new AssetOptionalsReqParams('baumuster', logger);

        expect(assetReq).toBeDefined();

        assetReq.page = 10;
        assetReq.sort = 'desc';
        assetReq.size = 20;

        expect(assetReq.page).toEqual(10);
        expect(assetReq.sort).toEqual('desc');
        expect(assetReq.size).toEqual(20);

        assetReq.baumuster = 'baumuster';
        assetReq.code = '12345';
        assetReq.description = 'description';

        expect(assetReq.baumuster).toEqual('baumuster');
        expect(assetReq.code).toEqual('12345');
        expect(assetReq.description).toEqual('description');


        let httpP = assetReq.requestParameters();
        expect(httpP).toBeDefined();
        expect(httpP.get('baumuster')).toEqual('baumuster');
        expect(httpP.get('code')).toEqual('12345');
        expect(httpP.get('description')).toEqual('description');

        expect(httpP.get('page')).toEqual('10');
        expect(httpP.get('sort')).toEqual('desc');
        expect(httpP.get('size')).toEqual('20');
    });

    it('should AssetsChassisReqParams', () => {

        let logger: LoggerService = new LoggerService();
        let baumuster: string;
        let chassis: string;
        let makeCode: string;

        let acrq = new AssetsChassisReqParams(baumuster, chassis, makeCode, logger);
        expect(acrq).toBeDefined();

        acrq.baumuster = 'baumuster';
        expect(acrq.baumuster).toEqual('baumuster');

        acrq.chassis = 'chassis';
        expect(acrq.chassis).toEqual('chassis');

        acrq.makeCode = 'makeCode';
        expect(acrq.makeCode).toEqual('makeCode');


        let httpP = acrq.requestParameters();
        expect(httpP).toBeDefined();

        expect(httpP.get('baumuster')).toEqual('baumuster');
        expect(httpP.get('chassis')).toEqual('chassis');
        expect(httpP.get('makeCode')).toEqual('makeCode');


    });

    it('should AssetsChassis', () => {

        let ac = new AssetsChassis();

        ac.errors = ['error'];
        expect(ac.errors[0]).toEqual('error');

        ac.chassis = 'chassis';
        expect(ac.chassis).toEqual('chassis');

        ac.chassisErrors = ['chassisErrors'];
        expect(ac.chassisErrors[0]).toEqual('chassisErrors');

        ac.chassisValue = 'chassisValue';
        expect(ac.chassisValue).toEqual('chassisValue');

        let acf = new AssetsChassisFactory();
        expect(acf).toBeDefined();

    });

    it('should CommercialAmounts', () => {
        let ipt: number = 10;
        let mss: number = 10;


        let ca = new CommercialAmounts(ipt, mss);
        expect(ca).toBeDefined();

        ca.ipt = 20;
        expect(ca.ipt).toEqual(20);

        ca.mss = 20;
        expect(ca.mss).toEqual(20);

        ca.commercialIpt = 20;
        expect(ca.commercialIpt).toEqual(20);

        ca.messaSuStrada = 20;
        expect(ca.messaSuStrada).toEqual(20);

        let caf = new CommercialAmountsFactory();
        expect(caf).toBeDefined();

    });

    it('should CommercialAmountsReqParams', () => {
        let utils: UtilsService = new UtilsService();
        let logger: LoggerService = new LoggerService();

        expect(utils).toBeDefined();
        expect(logger).toBeDefined();

        let baumuster: string = 'baumuster';
        let used: boolean = true;

        let carq = new CommercialAmountsReqParams(baumuster, used, logger);
        expect(carq).toBeDefined();

        carq.assetType = 'carq';
        expect(carq.assetType).toEqual('carq');

        carq.baumuster = 'carq';
        expect(carq.baumuster).toEqual('carq');

        carq.registration = 'carq';
        expect(carq.registration).toEqual('carq');

        carq.registration = 'carq';
        expect(carq.registration).toEqual('carq');

        carq.id = 'carq';
        expect(carq.id).toEqual('carq');

        carq.creationTime = 10;
        expect(carq.creationTime).toEqual(10);

        carq.lastAccessedTime = 10;
        expect(carq.lastAccessedTime).toEqual(10);

        carq.maxInactiveInterval = 10;
        expect(carq.maxInactiveInterval).toEqual(10);

        carq.new = true;
        expect(carq.new).toEqual(true);

        carq.used = true;
        expect(carq.used).toEqual(true);

        let httpP = carq.requestParameters();
        expect(httpP).toBeDefined();

        expect(httpP.get('assetType')).toEqual(carq.assetType.toString());
        expect(httpP.get('baumuster')).toEqual(carq.baumuster.toString());
        expect(httpP.get('creationTime')).toEqual(carq.creationTime.toString());

        expect(httpP.get('id')).toEqual(carq.id.toString());
        expect(httpP.get('lastAccessedTime')).toEqual(carq.lastAccessedTime.toString());
        expect(httpP.get('maxInactiveInterval')).toEqual(carq.maxInactiveInterval.toString());
        expect(httpP.get('new')).toEqual(carq.new.toString());
        expect(httpP.get('registration')).toEqual(carq.registration.toString());
        expect(httpP.get('used')).toEqual(carq.used.toString());

    });

    it('should CreditCorporateQueue', () => {
        let arrivo: string = 'arrivo';
        let richiedente: string = 'richiedente';
        let esposizione: string = 'esposizione';
        let numBlocchi: number = 20;
        let tipoProposta: string = 'tipoProposta';
        let prodotto: string = 'prodotto';
        let proposta: number = 20;

        let ccq = new CreditCorporateQueue(arrivo, richiedente, esposizione, numBlocchi, tipoProposta, prodotto, proposta);

        ccq.arrivo = 'arrivo';
        expect(ccq.arrivo).toEqual('arrivo');

        ccq.richiedente = 'arrivo';
        expect(ccq.richiedente).toEqual('arrivo');

        ccq.esposizione = 'arrivo';
        expect(ccq.esposizione).toEqual('arrivo');

        ccq.numBlocchi = 10;
        expect(ccq.numBlocchi).toEqual(10);

        ccq.tipoProposta = 'arrivo';
        expect(ccq.tipoProposta).toEqual('arrivo');

        ccq.prodotto = 'arrivo';
        expect(ccq.prodotto).toEqual('arrivo');

        ccq.proposta = 10;
        expect(ccq.proposta).toEqual(10);

        ccq.prodotto = 'prodotto';
        expect(ccq.prodotto).toEqual('prodotto');

    });

    it('should CreditWorkQueue', () => {
        let tipoDiSoggetto: string = 'string';
        let arrivo: string = 'string';
        let assegnatario: string = 'string';
        let richiedente: string = 'string';
        let esposizione: string = 'string';
        let riproposta: boolean = false;
        let numNote: number = 10;
        let numDocMancanti: number = 10;
        let score: string = 'string';
        let numBlocchi: number = 10;
        let proposta: number = 10;
        let processInstanceId: number = 10;
        let prodotto: string = 'string';

        let cfq = new CreditWorkQueue(tipoDiSoggetto, arrivo, assegnatario, richiedente, numNote, riproposta,
            numNote, numDocMancanti, score, numBlocchi, proposta.toString(), processInstanceId, prodotto, null,
            null, null, null, null, null);

        cfq.tipoDiSoggetto = 'arrivo';
        expect(cfq.tipoDiSoggetto).toEqual('arrivo');

        cfq.arrivo = 'arrivo';
        expect(cfq.arrivo).toEqual('arrivo');

        cfq.assegnatario = 'arrivo';
        expect(cfq.assegnatario).toEqual('arrivo');

        cfq.richiedente = 'arrivo';
        expect(cfq.richiedente).toEqual('arrivo');

        cfq.esposizione = numNote;
        expect(cfq.esposizione).toEqual(10);

        cfq.riproposta = false;
        expect(cfq.riproposta).toBeFalsy();

        cfq.numNote = 10;
        expect(cfq.numNote).toEqual(10);

        cfq.numBlocchi = 10;
        expect(cfq.numBlocchi).toEqual(10);

        cfq.numDocMancanti = 10;
        expect(cfq.numDocMancanti).toEqual(10);

        cfq.score = 'arrivo';
        expect(cfq.score).toEqual('arrivo');

        cfq.proposta = '10';
        expect(cfq.proposta).toEqual('10');

        cfq.processInstanceId = 10;
        expect(cfq.processInstanceId).toEqual(10);

        cfq.prodotto = 'prodotto';
        expect(cfq.prodotto).toEqual('prodotto');

    });

    it('should DealerContactQueue ', () => {
        let arrivo: string = 'string';
        let asset: string = 'string';
        let richiedente: string = 'string';
        let sospesaDa: string = 'string';
        let numBlocchi: number = 10;
        let numNote: number = 10;
        let prodotto: string = 'string';
        let proposta: number = 10;
        let tipoProposta: number = 10;
        let causaleSospensione: string = 'string';
        let campagna: string = 'string';

        let cfq = new DealerContactQueue(arrivo, asset, richiedente, sospesaDa, numBlocchi,
            numNote, prodotto, proposta, tipoProposta, causaleSospensione, campagna);

        cfq.arrivo = 'arrivo';
        expect(cfq.arrivo).toEqual('arrivo');

        cfq.asset = 'arrivo';
        expect(cfq.asset).toEqual('arrivo');

        cfq.richiedente = 'arrivo';
        expect(cfq.richiedente).toEqual('arrivo');

        cfq.sospesaDa = 'arrivo';
        expect(cfq.sospesaDa).toEqual('arrivo');

        cfq.numBlocchi = 10;
        expect(cfq.numBlocchi).toEqual(10);

        cfq.numNote = 10;
        expect(cfq.numNote).toEqual(10);

        cfq.prodotto = 'arrivo';
        expect(cfq.prodotto).toEqual('arrivo');

        cfq.proposta = 10;
        expect(cfq.proposta).toEqual(10);

        cfq.tipoProposta = 10;
        expect(cfq.tipoProposta).toEqual(10);

        cfq.causaleSospensione = 'arrivo';
        expect(cfq.causaleSospensione).toEqual('arrivo');

        cfq.campagna = 'arrivo';
        expect(cfq.campagna).toEqual('arrivo');
    });

    it('should DealerContactQueue ', () => {
        let arrivo: string = 'string';
        let asset: string = 'string';
        let richiedente: string = 'string';
        let sospesaDa: string = 'string';
        let numBlocchi: number = 10;
        let numNote: number = 10;
        let prodotto: string = 'string';
        let proposta: number = 10;
        let tipoProposta: number = 10;
        let causaleSospensione: string = 'string';
        let campagna: string = 'string';

        let cfq = new DealerContactQueue(arrivo, asset, richiedente, sospesaDa, numBlocchi,
            numNote, prodotto, proposta, tipoProposta, causaleSospensione, campagna);

        cfq.arrivo = 'arrivo';
        expect(cfq.arrivo).toEqual('arrivo');

        cfq.asset = 'arrivo';
        expect(cfq.asset).toEqual('arrivo');

        cfq.richiedente = 'arrivo';
        expect(cfq.richiedente).toEqual('arrivo');

        cfq.sospesaDa = 'arrivo';
        expect(cfq.sospesaDa).toEqual('arrivo');

        cfq.numBlocchi = 10;
        expect(cfq.numBlocchi).toEqual(10);

        cfq.numNote = 10;
        expect(cfq.numNote).toEqual(10);

        cfq.prodotto = 'arrivo';
        expect(cfq.prodotto).toEqual('arrivo');

        cfq.proposta = 10;
        expect(cfq.proposta).toEqual(10);

        cfq.tipoProposta = 10;
        expect(cfq.tipoProposta).toEqual(10);

        cfq.causaleSospensione = 'arrivo';
        expect(cfq.causaleSospensione).toEqual('arrivo');

        cfq.campagna = 'arrivo';
        expect(cfq.campagna).toEqual('arrivo');
    });

    it('should ImpUser', () => {
        let username: string = 'string';
        let mail: string = 'string';
        let role: string = 'string';
        let name: string = 'string';
        let surname: string = 'string';

        let iu = new ImpUser(username, mail, role, name, surname);

        iu.username = 'username';
        expect(iu.username).toEqual('username');

        iu.mail = 'mail';
        expect(iu.mail).toEqual('mail');

        iu.role = 'role';
        expect(iu.role).toEqual('role');

        iu.name = 'name';
        expect(iu.name).toEqual('name');

        iu.surname = 'surname';
        expect(iu.surname).toEqual('surname');
    });

    it('should ImpUserRequestParams', () => {
        let iurq = new ImpUserRequestParams();
        let iurq2 = new ImpUserRequestParams();
        expect(iurq).toBeDefined();

        iurq.username = 'username';
        expect(iurq.username).toEqual('username');

        iurq.setParamsFromFilter(iurq2);
        let filter = <ImpUserRequestParams>iurq2;

        filter.username = 'username';
        expect(filter.username).toEqual('username');

        let httpP = iurq.requestParameters();
        expect(httpP).toBeDefined();


    });

    it('should ListValues', () => {
        let _listCode: string = 'list';

        let val: ValueList = {
            key: 'string',
            value: 'string'
        };

        let _values: ValueList[] = [val];

        let lv = new ListValues(_listCode, _values);
        expect(lv).toBeDefined();

        lv.values = [val];
        expect(lv.values[0]).toEqual(val);

        lv.listOfvalues = [val];
        expect(lv.listOfvalues[0]).toEqual(val);

        lv.listOfvaluesCode = 'ciao';
        expect(lv.listOfvaluesCode).toEqual('ciao');

        lv.listCode = 'ciao';
        expect(lv.listCode).toEqual('ciao');


    });

    it('should ListValuesReqParams ', () => {
        let utils: UtilsService = new UtilsService();
        let logger: LoggerService = new LoggerService();
        expect(utils).toBeDefined();

        let lvrq = new ListValuesReqParams(logger);
        expect(lvrq).toBeDefined();

        lvrq.valueNames = ['10'];
        expect(lvrq.valueNames[0]).toEqual('10');

        lvrq.new = true;
        expect(lvrq.new).toBeTruthy();

        lvrq.maxInactiveInterval = 10;
        expect(lvrq.maxInactiveInterval).toEqual(10);

        lvrq.listCode = 'listCode';
        expect(lvrq.listCode).toEqual('listCode');

        lvrq.lastAccessedTime = 10;
        expect(lvrq.lastAccessedTime).toEqual(10);

        lvrq.id = 'id';
        expect(lvrq.id).toEqual('id');

        lvrq.creationTime = 10;
        expect(lvrq.creationTime).toEqual(10);

        let httpP = lvrq.requestParameters();
        expect(httpP).toBeDefined();

        expect(httpP.get('creationTime')).toEqual(lvrq.creationTime.toString());
        expect(httpP.get('id')).toEqual(lvrq.id);
        expect(httpP.get('lastAccessedTime')).toEqual(lvrq.lastAccessedTime.toString());
        expect(httpP.get('listCode')).toEqual(lvrq.listCode);
        expect(httpP.get('_maxInactiveInterval')).toEqual(lvrq.maxInactiveInterval.toString());
        expect(httpP.get('new')).toEqual(lvrq.new.toString());

        expect(httpP.get('valueNames')).toBeTruthy();
        expect(httpP.get('valueNames')).toEqual(lvrq.valueNames.toString());


    });

    it('should Normalizer', () => {
        let iVersion: IVersion = {
            productCode: null,
            version: null,
            fuel: null,
            catalog: null,
            price: null,
            forceSecondHand: false,
        };

        expect(iVersion).toBeDefined();
    });

    it('should NumberProposalHeader ', () => {
        let countAttive: number;
        let countInControlloDocumentale: number;
        let countInIstruttoria: number;
        let countSospese: number;
        let countAttivateOggi: number;
        let countLavorabili: number;
        let countTouchAndGo: number;
        let countUrgenti: number;

        let nph = new NumberProposalHeader(countAttive, countInControlloDocumentale, countInIstruttoria, countSospese, countAttivateOggi, countLavorabili, countTouchAndGo, countUrgenti);
        expect(nph).toBeDefined();

        nph.countAttive = 10;
        expect(nph.countAttive).toEqual(10);

        nph.countInControlloDocumentale = 10;
        expect(nph.countInControlloDocumentale).toEqual(10);

        nph.countInIstruttoria = 10;
        expect(nph.countInIstruttoria).toEqual(10);

        nph.countSospese = 10;
        expect(nph.countSospese).toEqual(10);

        nph.countAttivateOggi = 10;
        expect(nph.countAttivateOggi).toEqual(10);

        nph.countLavorabili = 10;
        expect(nph.countLavorabili).toEqual(10);

        nph.countTouchAndGo = 10;
        expect(nph.countTouchAndGo).toEqual(10);

        nph.countUrgenti = 10;
        expect(nph.countUrgenti).toEqual(10);
    });

    it('should NumberProposalTab ', () => {
        let numAssegnateAMe: number;
        let numBloccate: number;
        let numDaAttivare: number;
        let numDdm: number;
        let numDocumentazionIncompleta: number;
        let numEscluseRifRil: number;
        let numFirmaCartacea: number;
        let numFirmaGrafometrica: number;
        let numInControlloDocumentale: number;
        let numInInstruttoria: number;
        let numLavorabili: number;
        let numRifRil: number;
        let numRiproposta: number;
        let numSecondaApprovazione: number;
        let numSospese: number;
        let numUrgenti: number;
        let numTotali: number;

        let npt = new NumberProposalTab(
            numAssegnateAMe,
            numBloccate,
            numDaAttivare,
            numDdm,
            numDocumentazionIncompleta,
            numEscluseRifRil,
            numFirmaCartacea,
            numFirmaGrafometrica,
            numInControlloDocumentale,
            numInInstruttoria,
            numLavorabili,
            numRifRil,
            numRiproposta,
            numSecondaApprovazione,
            numSospese,
            numUrgenti,
            numTotali
        );

        expect(npt).toBeDefined();

        npt.numAssegnateAMe = 10;
        expect(npt.numAssegnateAMe).toEqual(10);

        npt.numBloccate = 10;
        expect(npt.numBloccate).toEqual(10);

        npt.numDaAttivare = 10;
        expect(npt.numDaAttivare).toEqual(10);

        npt.numDdm = 10;
        expect(npt.numDdm).toEqual(10);

        npt.numDocumentazionIncompleta = 10;
        expect(npt.numDocumentazionIncompleta).toEqual(10);

        npt.numEscluseRifRil = 10;
        expect(npt.numEscluseRifRil).toEqual(10);

        npt.numFirmaCartacea = 10;
        expect(npt.numFirmaCartacea).toEqual(10);

        npt.numFirmaGrafometrica = 10;
        expect(npt.numFirmaGrafometrica).toEqual(10);

        npt.numInControlloDocumentale = 10;
        expect(npt.numInControlloDocumentale).toEqual(10);

        npt.numInInstruttoria = 10;
        expect(npt.numInInstruttoria).toEqual(10);

        npt.numLavorabili = 10;
        expect(npt.numLavorabili).toEqual(10);

        npt.numRifRil = 10;
        expect(npt.numRifRil).toEqual(10);

        npt.numRiproposta = 10;
        expect(npt.numRiproposta).toEqual(10);

        npt.numSecondaApprovazione = 10;
        expect(npt.numSecondaApprovazione).toEqual(10);

        npt.numSospese = 10;
        expect(npt.numSospese).toEqual(10);

        npt.numUrgenti = 10;
        expect(npt.numUrgenti).toEqual(10);

        npt.numTotali = 10;
        expect(npt.numTotali).toEqual(10);

    });

    it('Proposal', () => {

        let p = new Proposal();
        expect(p).toBeDefined();

        p.venditore = 'venditore';
        expect(p.venditore).toEqual('venditore');

        p.data = 'data';
        expect(p.data).toEqual('data');

        p.contratto = 'contratto';
        expect(p.contratto).toEqual('contratto');

        p.scadenza = 'scadenza';
        expect(p.scadenza).toEqual('scadenza');

        p.proposta = 'proposta';
        expect(p.proposta).toEqual('proposta');

        p.soggetto = 'soggetto';
        expect(p.soggetto).toEqual('soggetto');

        p.modello = 'modello';
        expect(p.modello).toEqual('modello');

        p.stato = 10;
        expect(p.stato).toEqual(10);

        p.numeroNote = 10;
        expect(p.numeroNote).toEqual(10);

        p.chatPresente = false;
        expect(p.chatPresente).toBeFalsy();
    });

    it('Version', () => {

        let baumuster: string = 'baumuster';
        let modelDescription: string = 'modelDescription';

        let v = new Version(baumuster, modelDescription);
        expect(v).toBeDefined();

        v.baumuster = 'ciao';
        expect(v.baumuster).toEqual('ciao');

        v.fuelTypeCode = 'ciao';
        expect(v.fuelTypeCode).toEqual('ciao');

        v.modelDescription = 'ciao';
        expect(v.modelDescription).toEqual('ciao');

        v.pricelistDeadline = 'ciao';
        expect(v.pricelistDeadline).toEqual('ciao');

        v.netCost = 10;
        expect(v.netCost).toEqual(10);

        v.forceSecondHand = false;
        expect(v.forceSecondHand).toBeFalsy();
    });

    it('Proposal', () => {

        let v = new Proposal();
        expect(v).toBeDefined();

        v.venditore = 'ciao';
        expect(v.venditore).toEqual('ciao');

        v.data = 'ciao';
        expect(v.data).toEqual('ciao');

        v.contratto = 'ciao';
        expect(v.contratto).toEqual('ciao');

        v.scadenza = 'ciao';
        expect(v.scadenza).toEqual('ciao');

        v.proposta = 'ciao';
        expect(v.proposta).toEqual('ciao');

        v.soggetto = 'ciao';
        expect(v.soggetto).toEqual('ciao');

        v.modello = 'ciao';
        expect(v.modello).toEqual('ciao');

        v.stato = 10;
        expect(v.stato).toEqual(10);

        v.numeroNote = 10;
        expect(v.numeroNote).toEqual(10);

        v.chatPresente = false;
        expect(v.chatPresente).toBeFalsy();
    });

    it('ProposalsComponent', () => {

        let router: Router;
        let headerService: HeaderService;
        let mScrollbarService: MalihuScrollbarService;

        let v = new ProposalsComponent(router, headerService, mScrollbarService);

        expect(v).toBeDefined();

        let showEffect = false;
        expect(showEffect).toBeFalsy();
    });

    it('ProposalCounter', () => {
        let countAttive: number, countNonAttiveBen: number, countInAttivazione: number, countInAnalisi: number;
        let pc = new ProposalCounter(countAttive, countNonAttiveBen, countInAttivazione, countInAnalisi);

        expect(pc).toBeDefined();

        pc.countAttive = 10;
        expect(pc.countAttive).toEqual(10);

        pc.countNonAttiveBen = 10;
        expect(pc.countNonAttiveBen).toEqual(10);

        pc.countInAttivazione = 10;
        expect(pc.countInAttivazione).toEqual(10);

        pc.countInAnalisi = 10;
        expect(pc.countInAnalisi).toEqual(10);

        pc.proposalsInWork = 10;
        expect(pc.proposalsInWork).toEqual(10);

        pc.proposalsInError = 10;
        expect(pc.proposalsInError).toEqual(10);

        pc.proposalsAccepted = 10;
        expect(pc.proposalsAccepted).toEqual(10);

        pc.proposalsInAnalysis = 10;
        expect(pc.proposalsInAnalysis).toEqual(10);

    });

    it('should version', () => {
        let version = new VersionRequestParameters('', '', '', _logger);

        version.assetClassCode = 'prova';
        let assetClassCode = version.assetClassCode;
        expect(version.assetClassCode).toEqual(assetClassCode);

        version.assetType = 'provaT';
        let assetType = version.assetType;
        expect(version.assetType).toEqual(assetType);

        version.makeCode = 'makeCode';
        let makeCode = version.makeCode;
        expect(version.makeCode).toEqual(makeCode);

        version.page = 1;
        let page = version.page;
        expect(version.page).toEqual(page);

        version.size = 1;
        let size = version.size;
        expect(version.size).toEqual(size);

        version.sort = 'asc';
        let sort = version.sort;
        expect(version.sort).toEqual(sort);

        version.baumuster = 'baumuster';
        let baumuster = version.baumuster;
        expect(version.baumuster).toEqual(baumuster);

        version.modelDescription = 'modelDescription';
        let modelDescription = version.modelDescription;
        expect(version.modelDescription).toEqual(modelDescription);

        version.offset = 10;
        let offset = version.offset;
        expect(version.offset).toEqual(offset);

        version.fromNetCost = 10;
        let fromNetCost = version.fromNetCost;
        expect(version.fromNetCost).toEqual(fromNetCost);

        version.toNetCost = 10;
        let toNetCost = version.toNetCost;
        expect(version.toNetCost).toEqual(toNetCost);
    });

    it('should ProposalListRequestParameters', () => {
        let proposal = new ProposalListRequestParameters(_logger);

        proposal.attivataA = 'oggi';
        let attivoA = proposal.attivataA;
        expect(proposal.attivataA).toEqual(attivoA);

        proposal.attivataDa = 'domani';
        let attivoDa = proposal.attivataDa;
        expect(proposal.attivataDa).toEqual(attivoDa);

        proposal.numeroContratto = 'numeroContratto';
        let numeroContratto = proposal.numeroContratto;
        expect(proposal.numeroContratto).toEqual(numeroContratto);

        proposal.page = 1;
        let page = proposal.page;
        expect(proposal.page).toEqual(page);

        proposal.size = 1;
        let size = proposal.size;
        expect(proposal.size).toEqual(size);

        proposal.sort = 'asc';
        let sort = proposal.sort;
        expect(proposal.sort).toEqual(sort);

        proposal.numeroProposta = 'numeroProposta';
        let numeroProposta = proposal.numeroProposta;
        expect(proposal.numeroProposta).toEqual(numeroProposta);

        proposal.soggetto = 'soggetto';
        let soggetto = proposal.soggetto;
        expect(proposal.soggetto).toEqual(soggetto);

        proposal.stato = [100, 20];
        let stato = proposal.stato;
        expect(proposal.stato).toEqual(stato);

        proposal.targa = 'targa';
        let targa = proposal.targa;
        expect(proposal.targa).toEqual(targa);

        proposal.telaio = 'telaio';
        let telaio = proposal.telaio;
        expect(proposal.telaio).toEqual(telaio);

    });

    it('should PaginatedObject', () => {

        let pagData: PaginationData;
        let object: Object;

        let pagObj = new PaginatedObject(pagData, object);
        expect(pagObj).toBeDefined();

        pagObj.pageData = null;
        expect(pagObj.pageData).toEqual(null);
        pagObj.object = {};
        expect(pagObj.object).toEqual({});

    });

    it('should PaginationData', () => {
        let totalElements = 200;
        let totalPages = 40;

        let pagData = new PaginationData(totalElements, totalPages);
        expect(pagData).toBeDefined();
        expect(pagData.totalElements).toEqual(200);
        expect(pagData.totalPages).toEqual(40);
        expect(pagData.currentPage).toBeUndefined();
        expect(pagData.pageSize).toBeUndefined();
        expect(pagData.offset).toBeUndefined();
    });

    it('should PayoutQueue', () => {

        let contratto = ' ';
        let blocked = false;
        let processInstanceId = 0;
        let tipoProposta = ' ';
        let causaleSospensione = ' ';
        let numBlocchi = 0;
        let arrivo = ' ';
        let prodotto = ' ';
        let richiedente = ' ';
        let numNote = 0;
        let numDocMancanti = 0;
        let sospesaDa = ' ';

        let payout = new PayoutQueue(contratto, blocked, processInstanceId, tipoProposta, causaleSospensione, numBlocchi, arrivo, prodotto, richiedente, numNote, numDocMancanti, sospesaDa);

        expect(payout).toBeDefined();
        expect(payout.contratto).toEqual(' ');
        expect(payout.blocked).toEqual(false);
        expect(payout.processInstanceId).toEqual(0);
        expect(payout.tipoProposta).toEqual(' ');
        expect(payout.causaleSospensione).toEqual(' ');
        expect(payout.numBlocchi).toEqual(0);
        expect(payout.arrivo).toEqual(' ');
        expect(payout.prodotto).toEqual(' ');
        expect(payout.richiedente).toEqual(' ');
        expect(payout.numNote).toEqual(0);
        expect(payout.numDocMancanti).toEqual(0);
        expect(payout.sospesaDa).toEqual(' ');

        payout.contratto = ' ';
        payout.blocked = false;
        payout.processInstanceId = 0;
        payout.tipoProposta = ' ';
        payout.causaleSospensione = ' ';
        payout.numBlocchi = 0;
        payout.arrivo = ' ';
        payout.prodotto = ' ';
        payout.richiedente = ' ';
        payout.numNote = 0;
        payout.numDocMancanti = 0;
        payout.sospesaDa = ' ';

    });

    it('should Response', () => {

        let mockMess = new NotificationMessage(' ', ' ', ' ');
        let mockObj = new Object();

        let code = 0;
        let message = mockMess;
        let object = mockObj;

        let resp = new BeResponse<any>(code, message, object);

        resp.code = code;
        resp.message = message;
        resp.object = object;

        expect(resp.code).toEqual(0);
        expect(resp.message).toEqual(mockMess);
        expect(resp.object).toEqual(mockObj);
    });

    it('should Version', () => {

        let baumuster = ' ';
        let modelDescription = ' ';
        let netCost = 0;
        let pricelistDeadline = ' ';
        let fuelTypeCode = ' ';
        let forceSecondHand = false;

        let mockVersion = new Version(baumuster, modelDescription);

        mockVersion.baumuster = ' ';
        mockVersion.modelDescription = ' ';
        mockVersion.netCost = 0;
        mockVersion.pricelistDeadline = ' ';
        mockVersion.fuelTypeCode = ' ';
        mockVersion.forceSecondHand = false;

        expect(mockVersion.baumuster).toEqual(' ');
        expect(mockVersion.modelDescription).toEqual(' ');
        expect(mockVersion.netCost).toEqual(0);
        expect(mockVersion.pricelistDeadline).toEqual(' ');
        expect(mockVersion.fuelTypeCode).toEqual(' ');
        expect(mockVersion.forceSecondHand).toEqual(false);

    });

    it('VersionRequestParameters test', () => {

        let assetClassCode = ' ';
        let assetType = ' ';
        let makeCode = ' ';
        let baumuster = ' ';
        let modelDescription = ' ';
        let offset = 0;
        let page = 0;
        let size = 0;
        let sort = ' ';
        let fromNetCost = 0;
        let toNetCost = 0;

        let versRequest = new VersionRequestParameters(assetClassCode, assetType, makeCode, new LoggerService());

        versRequest.assetClassCode = ' ';
        versRequest.assetType = ' ';
        versRequest.makeCode = ' ';
        versRequest.baumuster = ' ';
        versRequest.modelDescription = ' ';
        versRequest.offset = 0;
        versRequest.page = 0;
        versRequest.size = 0;
        versRequest.sort = ' ';
        versRequest.fromNetCost = 0;
        versRequest.toNetCost = 0;

        expect(versRequest.assetClassCode).toEqual(' ');
        expect(versRequest.assetType).toEqual(' ');
        expect(versRequest.makeCode).toEqual(' ');
        expect(versRequest.modelDescription).toEqual(' ');
        expect(versRequest.offset).toEqual(0);
        expect(versRequest.page).toEqual(0);
        expect(versRequest.size).toEqual(0);
        expect(versRequest.sort).toEqual(' ');
        expect(versRequest.fromNetCost).toEqual(0);
        expect(versRequest.toNetCost).toEqual(0);

        versRequest.requestParameters();

    });

    it('BlockedFields test', () => {

        let mockBlockedFields = new BlockedFields();

        mockBlockedFields.tan = false;
        mockBlockedFields.rata = false;
        mockBlockedFields.balloon = false;
        mockBlockedFields.anticipo = false;

        expect(mockBlockedFields.tan).toEqual(false);
        expect(mockBlockedFields.rata).toEqual(false);
        expect(mockBlockedFields.balloon).toEqual(false);
        expect(mockBlockedFields.anticipo).toEqual(false);

        let mockBlockedFieldsFactory = new BlockedFieldsFactory();
        mockBlockedFieldsFactory.getInstance(mockBlockedFields);

    });

    it('Campaign test', () => {

        let mockCampaign = new Campaign();

        mockCampaign.code = ' ';
        mockCampaign.description = ' ';
        mockCampaign.expiryDate = ' ';
        mockCampaign.finalExpiryDate = ' ';
        mockCampaign.internalProposal = false;
        mockCampaign.mustUseSpIstr = false;
        mockCampaign.speseIstruttoria = 0;
        mockCampaign.defaultValue = false;
        mockCampaign.adminFeeShareOverride = false;
        mockCampaign.commCamp = false;
        mockCampaign.dealManuContrComm = false;
        mockCampaign.flagShare = false;
        mockCampaign.selFixComAmount = 0;
        mockCampaign.selFixComPerc = 0;
        mockCampaign.manuFixContrComm = 0;
        mockCampaign.manuPercContrComm = 0;
        mockCampaign.dealPercManuContrComm = 0;
        mockCampaign.dealFixCom = 0;
        mockCampaign.dealPercCom = 0;

        expect(mockCampaign.code).toEqual(' ');
        expect(mockCampaign.description).toEqual(' ');
        expect(mockCampaign.expiryDate).toEqual(' ');
        expect(mockCampaign.finalExpiryDate).toEqual(' ');
        expect(mockCampaign.internalProposal).toEqual(false);
        expect(mockCampaign.mustUseSpIstr).toEqual(false);
        expect(mockCampaign.speseIstruttoria).toEqual(0);
        expect(mockCampaign.defaultValue).toEqual(false);
        expect(mockCampaign.adminFeeShareOverride).toEqual(false);
        expect(mockCampaign.commCamp).toEqual(false);
        expect(mockCampaign.dealManuContrComm).toEqual(false);
        expect(mockCampaign.flagShare).toEqual(false);
        expect(mockCampaign.selFixComAmount).toEqual(0);
        expect(mockCampaign.selFixComPerc).toEqual(0);
        expect(mockCampaign.manuFixContrComm).toEqual(0);
        expect(mockCampaign.manuPercContrComm).toEqual(0);
        expect(mockCampaign.dealPercManuContrComm).toEqual(0);
        expect(mockCampaign.dealFixCom).toEqual(0);
        expect(mockCampaign.dealPercCom).toEqual(0);

        let mockCampaignFactory = new CampaignFactory();
        mockCampaignFactory.getInstance(mockCampaign);

    });

    it('ConsultationCommercialQueue test', () => {

        let mockConsultationCommercialQueue = new ConsultationCommercialQueue();

        mockConsultationCommercialQueue.ipt = 0;
        mockConsultationCommercialQueue.mss = ' ';
        mockConsultationCommercialQueue.netCost = ' ';
        mockConsultationCommercialQueue.fornitoreAsset = ' ';
        mockConsultationCommercialQueue.vatCode = ' ';
        mockConsultationCommercialQueue.chassisNumber = ' ';
        mockConsultationCommercialQueue.plate = ' ';
        mockConsultationCommercialQueue.registration = ' ';
        mockConsultationCommercialQueue.mileage = ' ';
        mockConsultationCommercialQueue.totalAmount = 0;
        mockConsultationCommercialQueue.listingPrice = 0;
        mockConsultationCommercialQueue.equipmentTotalAmount = 0;
        mockConsultationCommercialQueue.optionalsTotalAmount = 0;
        mockConsultationCommercialQueue.discountAmount = 0;
        mockConsultationCommercialQueue.discountPerc = 0;
        mockConsultationCommercialQueue.optionals = [];

        expect(mockConsultationCommercialQueue.ipt).toEqual(0);
        expect(mockConsultationCommercialQueue.mss).toEqual(' ');
        expect(mockConsultationCommercialQueue.netCost).toEqual(' ');
        expect(mockConsultationCommercialQueue.fornitoreAsset).toEqual(' ');
        expect(mockConsultationCommercialQueue.vatCode).toEqual(' ');
        expect(mockConsultationCommercialQueue.chassisNumber).toEqual(' ');
        expect(mockConsultationCommercialQueue.plate).toEqual(' ');
        expect(mockConsultationCommercialQueue.registration).toEqual(' ');
        expect(mockConsultationCommercialQueue.mileage).toEqual(' ');
        expect(mockConsultationCommercialQueue.totalAmount).toEqual(0);
        expect(mockConsultationCommercialQueue.listingPrice).toEqual(0);
        expect(mockConsultationCommercialQueue.equipmentTotalAmount).toEqual(0);
        expect(mockConsultationCommercialQueue.optionalsTotalAmount).toEqual(0);
        expect(mockConsultationCommercialQueue.discountAmount).toEqual(0);
        expect(mockConsultationCommercialQueue.discountPerc).toEqual(0);
        expect(mockConsultationCommercialQueue.optionals).toEqual([]);

        let mockConsultationCommercialQueueFactory = new ConsultationCommercialQueueFactory();
        mockConsultationCommercialQueueFactory.getInstance(mockConsultationCommercialQueue);

    });

    it('ConsultationCommOptional test', () => {

        let mockConsultationCommOptional = new ConsultationCommOptional();

        mockConsultationCommOptional.type = ' ';
        mockConsultationCommOptional.extraCode = ' ';
        mockConsultationCommOptional.discountApplied = false;
        mockConsultationCommOptional.extraDescription = ' ';
        mockConsultationCommOptional.extraNetCost = 0;
        mockConsultationCommOptional.extraTotalAmount = 0;
        mockConsultationCommOptional.vatCode = ' ';

        expect(mockConsultationCommOptional.type).toEqual(' ');
        expect(mockConsultationCommOptional.extraCode).toEqual(' ');
        expect(mockConsultationCommOptional.discountApplied).toEqual(false);
        expect(mockConsultationCommOptional.extraDescription).toEqual(' ');
        expect(mockConsultationCommOptional.extraNetCost).toEqual(0);
        expect(mockConsultationCommOptional.extraTotalAmount).toEqual(0);
        expect(mockConsultationCommOptional.vatCode).toEqual(' ');

        let mockConsultationCommOptionalFactory = new ConsultationCommOptionalFactory();
        mockConsultationCommOptionalFactory.getInstance(mockConsultationCommOptional);

        let mockConsultationCommOptionalList = [];
        mockConsultationCommOptionalList.push(mockConsultationCommOptional);

        mockConsultationCommOptionalFactory.getInstanceList(mockConsultationCommOptionalList);

    });

    it('ConsultationFinancialQueue test', () => {

        let mockBlockField = new BlockedFields();
        let mockProvvigioniDTO = new ProvvigioniDTO();

        let mockConsultationFinancialQueue = new ConsultationFinancialQueue();

        mockConsultationFinancialQueue.financialProduct = [];
        mockConsultationFinancialQueue.serviceEntity = [];
        mockConsultationFinancialQueue.anticipoPercentuale = 0;
        mockConsultationFinancialQueue.assetClassCode = ' ';
        mockConsultationFinancialQueue.assetType = ' ';
        mockConsultationFinancialQueue.balloonPercentuale = 0;
        mockConsultationFinancialQueue.baumuster = ' ';
        mockConsultationFinancialQueue.brandId = ' ';
        mockConsultationFinancialQueue.campaignCode = ' ';
        mockConsultationFinancialQueue.campiBloccati = mockBlockField;
        mockConsultationFinancialQueue.codAnticipo = ' ';
        mockConsultationFinancialQueue.codCanoneRata = ' ';
        mockConsultationFinancialQueue.codeIpoteca = ' ';
        mockConsultationFinancialQueue.codiceTipoInteresse = ' ';
        mockConsultationFinancialQueue.contractLength = 0;
        mockConsultationFinancialQueue.contributoCampagna = 0;
        mockConsultationFinancialQueue.contributoConcessionario = 0;
        mockConsultationFinancialQueue.derogaBlocchiServiziAssicurativi = false;
        mockConsultationFinancialQueue.derogaCommerciale = false;
        mockConsultationFinancialQueue.derogaContributo = false;
        mockConsultationFinancialQueue.distance = 0;
        mockConsultationFinancialQueue.fattura = false;
        mockConsultationFinancialQueue.financedAmount = 0;
        mockConsultationFinancialQueue.financialProductCode = ' ';
        mockConsultationFinancialQueue.frequency = 0;
        mockConsultationFinancialQueue.importoAnticipo = 0;
        mockConsultationFinancialQueue.importoRataAnticipoServizi = 0;
        mockConsultationFinancialQueue.importoRataBalloon = 0;
        mockConsultationFinancialQueue.importoRataBalloonServizi = 0;
        mockConsultationFinancialQueue.importoRataServizi = 0;
        mockConsultationFinancialQueue.incentivoVendita = 0;
        mockConsultationFinancialQueue.ipt = 0;
        mockConsultationFinancialQueue.mileage = 0;
        mockConsultationFinancialQueue.mfr = 0;
        mockConsultationFinancialQueue.mss = 0;
        mockConsultationFinancialQueue.netCost = 0;
        mockConsultationFinancialQueue.operazioneK2K = false;
        mockConsultationFinancialQueue.provImportoDeltaTasso = 0;
        mockConsultationFinancialQueue.provImportoServizi = 0;
        mockConsultationFinancialQueue.provImportoServiziPerc = 0;
        mockConsultationFinancialQueue.provImportoSpeseIstruttoria = 0;
        mockConsultationFinancialQueue.provImportoSpeseIstruttoriaPerc = 0;
        mockConsultationFinancialQueue.provImportoTotale = 0;
        mockConsultationFinancialQueue.provImportoVendita = 0;
        mockConsultationFinancialQueue.provImportoVenditaPerc = 0;
        mockConsultationFinancialQueue.provvigioniDTO = mockProvvigioniDTO;
        mockConsultationFinancialQueue.rata = 0;
        mockConsultationFinancialQueue.rataPercentuale = 0;
        mockConsultationFinancialQueue.rate = [];
        mockConsultationFinancialQueue.richiestaFattura = false;
        mockConsultationFinancialQueue.speseAnticipo = 0;
        mockConsultationFinancialQueue.speseDiIstruttoria = 0;
        mockConsultationFinancialQueue.speseRataCanone = 0;
        mockConsultationFinancialQueue.spread = 0;
        mockConsultationFinancialQueue.taeg = 0;
        mockConsultationFinancialQueue.tan = 0;
        mockConsultationFinancialQueue.tassBase = 0;
        mockConsultationFinancialQueue.tassoBase = 0;
        mockConsultationFinancialQueue.tipoInteresse = ' ';
        mockConsultationFinancialQueue.totalAmount = 0;
        mockConsultationFinancialQueue.used = false;
        mockConsultationFinancialQueue.vatPercentage = 0;
        mockConsultationFinancialQueue.importoServiziAssicurativiRata = 0;
        mockConsultationFinancialQueue.importoServiziAccordoAssisetenzaRata = 0;
        mockConsultationFinancialQueue.importoServiziAssicurativiBalloon = 0;
        mockConsultationFinancialQueue.importoServiziAccordoAssistenzaBalloon = 0;
        mockConsultationFinancialQueue.importoTotaleRata = 0;
        mockConsultationFinancialQueue.importoTotaleBalloon = 0;

        expect(mockConsultationFinancialQueue.financialProduct).toEqual([]);
        expect(mockConsultationFinancialQueue.serviceEntity).toEqual([]);
        expect(mockConsultationFinancialQueue.anticipoPercentuale).toEqual(0);
        expect(mockConsultationFinancialQueue.assetClassCode).toEqual(' ');
        expect(mockConsultationFinancialQueue.assetType).toEqual(' ');
        expect(mockConsultationFinancialQueue.balloonPercentuale).toEqual(0);
        expect(mockConsultationFinancialQueue.baumuster).toEqual(' ');
        expect(mockConsultationFinancialQueue.brandId).toEqual(' ');
        expect(mockConsultationFinancialQueue.campaignCode).toEqual(' ');
        expect(mockConsultationFinancialQueue.campiBloccati).toEqual(mockBlockField);
        expect(mockConsultationFinancialQueue.codAnticipo).toEqual(' ');
        expect(mockConsultationFinancialQueue.codCanoneRata).toEqual(' ');
        expect(mockConsultationFinancialQueue.codeIpoteca).toEqual(' ');
        expect(mockConsultationFinancialQueue.codiceTipoInteresse).toEqual(' ');
        expect(mockConsultationFinancialQueue.contractLength).toEqual(0);
        expect(mockConsultationFinancialQueue.contributoCampagna).toEqual(0);
        expect(mockConsultationFinancialQueue.contributoConcessionario).toEqual(0);
        expect(mockConsultationFinancialQueue.derogaBlocchiServiziAssicurativi).toEqual(false);
        expect(mockConsultationFinancialQueue.derogaCommerciale).toEqual(false);
        expect(mockConsultationFinancialQueue.derogaContributo).toEqual(false);
        expect(mockConsultationFinancialQueue.distance).toEqual(0);
        expect(mockConsultationFinancialQueue.fattura).toEqual(false);
        expect(mockConsultationFinancialQueue.financedAmount).toEqual(0);
        expect(mockConsultationFinancialQueue.financialProductCode).toEqual(' ');
        expect(mockConsultationFinancialQueue.frequency).toEqual(0);
        expect(mockConsultationFinancialQueue.importoAnticipo).toEqual(0);
        expect(mockConsultationFinancialQueue.importoRataAnticipoServizi).toEqual(0);
        expect(mockConsultationFinancialQueue.importoRataBalloon).toEqual(0);
        expect(mockConsultationFinancialQueue.importoRataBalloonServizi).toEqual(0);
        expect(mockConsultationFinancialQueue.importoRataServizi).toEqual(0);
        expect(mockConsultationFinancialQueue.ipt).toEqual(0);
        expect(mockConsultationFinancialQueue.mileage).toEqual(0);
        expect(mockConsultationFinancialQueue.mfr).toEqual(0);
        expect(mockConsultationFinancialQueue.mss).toEqual(0);
        expect(mockConsultationFinancialQueue.netCost).toEqual(0);
        expect(mockConsultationFinancialQueue.contributoConcessionario).toEqual(0);
        expect(mockConsultationFinancialQueue.operazioneK2K).toEqual(false);
        expect(mockConsultationFinancialQueue.provImportoDeltaTasso).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoServizi).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoServiziPerc).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoSpeseIstruttoria).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoSpeseIstruttoriaPerc).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoTotale).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoVendita).toEqual(0);
        expect(mockConsultationFinancialQueue.provImportoVenditaPerc).toEqual(0);
        expect(mockConsultationFinancialQueue.provvigioniDTO).toEqual(mockProvvigioniDTO);
        expect(mockConsultationFinancialQueue.rata).toEqual(0);
        expect(mockConsultationFinancialQueue.rataPercentuale).toEqual(0);
        expect(mockConsultationFinancialQueue.rate).toEqual([]);
        expect(mockConsultationFinancialQueue.richiestaFattura).toEqual(false);
        expect(mockConsultationFinancialQueue.speseAnticipo).toEqual(0);
        expect(mockConsultationFinancialQueue.speseDiIstruttoria).toEqual(0);
        expect(mockConsultationFinancialQueue.speseRataCanone).toEqual(0);
        expect(mockConsultationFinancialQueue.spread).toEqual(0);
        expect(mockConsultationFinancialQueue.taeg).toEqual(0);
        expect(mockConsultationFinancialQueue.tan).toEqual(0);
        expect(mockConsultationFinancialQueue.tassBase).toEqual(0);
        expect(mockConsultationFinancialQueue.tassoBase).toEqual(0);
        expect(mockConsultationFinancialQueue.tipoInteresse).toEqual(' ');
        expect(mockConsultationFinancialQueue.totalAmount).toEqual(0);
        expect(mockConsultationFinancialQueue.used).toEqual(false);
        expect(mockConsultationFinancialQueue.vatPercentage).toEqual(0);
        expect(mockConsultationFinancialQueue.importoServiziAssicurativiRata).toEqual(0);
        expect(mockConsultationFinancialQueue.importoServiziAccordoAssisetenzaRata).toEqual(0);
        expect(mockConsultationFinancialQueue.importoServiziAssicurativiBalloon).toEqual(0);
        expect(mockConsultationFinancialQueue.importoServiziAccordoAssistenzaBalloon).toEqual(0);
        expect(mockConsultationFinancialQueue.importoTotaleRata).toEqual(0);
        expect(mockConsultationFinancialQueue.importoTotaleBalloon).toEqual(0);

        let mockConsultationFinancialQueueFactory = new ConsultationFinancialQueueFactory();
        mockConsultationFinancialQueueFactory.getInstance(mockConsultationFinancialQueue);

    });

    it('ConsultationHeaderQueue test', () => {

        let mockConsultationHeaderQueue = new ConsultationHeaderQueue();

        mockConsultationHeaderQueue.proposta = 0;
        mockConsultationHeaderQueue.asset = ' ';
        mockConsultationHeaderQueue.soggetto = ' ';
        mockConsultationHeaderQueue.tipoSoggetto = ' ';
        mockConsultationHeaderQueue.financialProductDescription = ' ';
        mockConsultationHeaderQueue.contractLength = 0;
        mockConsultationHeaderQueue.balloon = 0;
        mockConsultationHeaderQueue.anticipo = 0;
        mockConsultationHeaderQueue.rateAmount = 0;
        mockConsultationHeaderQueue.financiedTotalAmount = 0;
        mockConsultationHeaderQueue.venditore = ' ';

        expect(mockConsultationHeaderQueue.proposta).toEqual(0);
        expect(mockConsultationHeaderQueue.asset).toEqual(' ');
        expect(mockConsultationHeaderQueue.soggetto).toEqual(' ');
        expect(mockConsultationHeaderQueue.tipoSoggetto).toEqual(' ');
        expect(mockConsultationHeaderQueue.financialProductDescription).toEqual(' ');
        expect(mockConsultationHeaderQueue.contractLength).toEqual(0);
        expect(mockConsultationHeaderQueue.balloon).toEqual(0);
        expect(mockConsultationHeaderQueue.anticipo).toEqual(0);
        expect(mockConsultationHeaderQueue.rateAmount).toEqual(0);
        expect(mockConsultationHeaderQueue.financiedTotalAmount).toEqual(0);
        expect(mockConsultationHeaderQueue.venditore).toEqual(' ');

        let mockConsultationHeaderQueueFactory = new ConsultationHeaderQueueFactory();
        mockConsultationHeaderQueueFactory.getInstance(mockConsultationHeaderQueue);

    });

    it('ConsultationProposalProductQueue test', () => {

        let mockConsultationProposalProductQueue = new ConsultationProposalProductQueue();

        mockConsultationProposalProductQueue.prodotto = ' ';
        mockConsultationProposalProductQueue.modello = ' ';
        mockConsultationProposalProductQueue.versione = ' ';
        mockConsultationProposalProductQueue.registration = ' ';
        mockConsultationProposalProductQueue.used = false;
        mockConsultationProposalProductQueue.mileage = ' ';

        expect(mockConsultationProposalProductQueue.prodotto).toEqual(' ');
        expect(mockConsultationProposalProductQueue.modello).toEqual(' ');
        expect(mockConsultationProposalProductQueue.versione).toEqual(' ');
        expect(mockConsultationProposalProductQueue.registration).toEqual(' ');
        expect(mockConsultationProposalProductQueue.used).toEqual(false);
        expect(mockConsultationProposalProductQueue.mileage).toEqual(' ');

        let mockConsultationProposalProdQueueFactory = new ConsultationProposalProdQueueFactory();
        mockConsultationProposalProdQueueFactory.getInstance(mockConsultationProposalProductQueue);

    });

    it('ConsultationProposalQueue test', () => {

        let mockConsSubQu: ConsultationSubjectQueue = new ConsultationSubjectQueue();
        let mockConsPropQu: ConsultationProposalProductQueue = new ConsultationProposalProductQueue();

        let mockConsultationProposalQueue = new ConsultationProposalQueue();

        mockConsultationProposalQueue.subject = mockConsSubQu;
        mockConsultationProposalQueue.product = mockConsPropQu;

        expect(mockConsultationProposalQueue.subject).toEqual(mockConsSubQu);
        expect(mockConsultationProposalQueue.product).toEqual(mockConsPropQu);

        let mockConsultationProposalQueueFactory = new ConsultationProposalQueueFactory();
        mockConsultationProposalQueueFactory.getInstance(mockConsultationProposalQueue);

    });

    it('ConsultationQueue test', () => {

        let mockConsSubQu: ConsultationSubjectQueue = new ConsultationSubjectQueue();
        let mockConsPropQu: ConsultationProposalProductQueue = new ConsultationProposalProductQueue();
        let mockConsCommQu: ConsultationCommercialQueue = new ConsultationCommercialQueue();
        let mockConsFinQu: ConsultationFinancialQueue = new ConsultationFinancialQueue();
        let mockConsQu: ConsultationHeaderQueue = new ConsultationHeaderQueue();

        let mockConsultationQueue = new ConsultationQueue();

        mockConsultationQueue.commercial = mockConsCommQu;
        mockConsultationQueue.financial = mockConsFinQu;
        mockConsultationQueue.header = mockConsQu;
        mockConsultationQueue.proposta = mockConsPropQu;
        mockConsultationQueue.soggetto = mockConsSubQu;

        expect(mockConsultationQueue.commercial).toEqual(mockConsCommQu);
        expect(mockConsultationQueue.financial).toEqual(mockConsFinQu);
        expect(mockConsultationQueue.header).toEqual(mockConsQu);
        expect(mockConsultationQueue.proposta).toEqual(mockConsPropQu);
        expect(mockConsultationQueue.soggetto).toEqual(mockConsSubQu);

        let mockConsultationQueueFactory = new ConsultationQueueFactory();
        mockConsultationQueueFactory.getInstance(mockConsultationQueue);

    });

    it('ConsultationSubjectQueue test', () => {

        let mockConsultationSubjectQueue = new ConsultationSubjectQueue();

        mockConsultationSubjectQueue.soggetto = ' ';
        mockConsultationSubjectQueue.cfPiva = ' ';
        mockConsultationSubjectQueue.tipoSoggetto = ' ';
        mockConsultationSubjectQueue.iban = ' ';

        expect(mockConsultationSubjectQueue.soggetto).toEqual(' ');
        expect(mockConsultationSubjectQueue.cfPiva).toEqual(' ');
        expect(mockConsultationSubjectQueue.tipoSoggetto).toEqual(' ');
        expect(mockConsultationSubjectQueue.iban).toEqual(' ');

        let mockConsultationSubjectQueueFactory = new ConsultationSubjectQueueFactory();
        mockConsultationSubjectQueueFactory.getInstance(mockConsultationSubjectQueue);

    });

    it('FinancialAttachment test', () => {

        let mockFinancialAttachment = new FinancialAttachment();

        mockFinancialAttachment.contentType = ' ';
        mockFinancialAttachment.url = ' ';
        mockFinancialAttachment.name = ' ';

        expect(mockFinancialAttachment.contentType).toEqual(' ');
        expect(mockFinancialAttachment.url).toEqual(' ');
        expect(mockFinancialAttachment.name).toEqual(' ');

        let mockFinancialAttachmentFactory = new FinancialAttachmentFactory();
        mockFinancialAttachmentFactory.getInstance(mockFinancialAttachment);

        let mockFinAttList: FinancialAttachment[] = [];
        mockFinAttList.push(mockFinancialAttachment);

        mockFinancialAttachmentFactory.getInstanceList(mockFinAttList);

    });

    it('FinancialProcuct test', () => {

        let mockFinancialProcuct = new FinancialProcuct();

        mockFinancialProcuct.fpCode = ' ';
        mockFinancialProcuct.fpDescription = ' ';
        mockFinancialProcuct.defaultValue = ' ';
        mockFinancialProcuct.campaigns = [];

        expect(mockFinancialProcuct.fpCode).toEqual(' ');
        expect(mockFinancialProcuct.fpDescription).toEqual(' ');
        expect(mockFinancialProcuct.defaultValue).toEqual(' ');
        expect(mockFinancialProcuct.campaigns).toEqual([]);

        let mockFinancialProcuctFactory = new FinancialProcuctFactory();
        mockFinancialProcuctFactory.getInstance(mockFinancialProcuct);

        let mockFinProdList: FinancialProcuct[] = [];
        mockFinProdList.push(mockFinancialProcuct);

        mockFinancialProcuctFactory.getInstanceList(mockFinProdList);

    });

    it('FinancialServicesInner test', () => {

        let mockFinancialServicesInner = new FinancialServicesInner();

        //mockFinancialServicesInner.type = ' ';
        mockFinancialServicesInner.supplyTypeCode = ' ';
        mockFinancialServicesInner.supplyElementCode = ' ';
        mockFinancialServicesInner.supplyTypeDesc = ' ';
        mockFinancialServicesInner.supplyElementDesc = ' ';
        mockFinancialServicesInner.categoryCode = ' ';
        mockFinancialServicesInner.parentSupplyTypeCode = ' ';
        mockFinancialServicesInner.defaultSupply = false;
        mockFinancialServicesInner.defaultElement = false;
        mockFinancialServicesInner.updatable = false;
        mockFinancialServicesInner.retrieveVat = false;
        mockFinancialServicesInner.parSupMand = ' ';
        mockFinancialServicesInner.flagMainSup = ' ';
        mockFinancialServicesInner.selezionabile = ' ';
        mockFinancialServicesInner.flagMandatory = ' ';
        mockFinancialServicesInner.tassoProdotto = false;
        mockFinancialServicesInner.beneStrumentale = false;

        //expect(mockFinancialServicesInner.type).toEqual(' ');
        expect(mockFinancialServicesInner.supplyTypeCode).toEqual(' ');
        expect(mockFinancialServicesInner.supplyElementCode).toEqual(' ');
        expect(mockFinancialServicesInner.supplyTypeDesc).toEqual(' ');
        expect(mockFinancialServicesInner.supplyElementDesc).toEqual(' ');
        expect(mockFinancialServicesInner.categoryCode).toEqual(' ');
        expect(mockFinancialServicesInner.parentSupplyTypeCode).toEqual(' ');
        expect(mockFinancialServicesInner.defaultSupply).toEqual(false);
        expect(mockFinancialServicesInner.defaultElement).toEqual(false);
        expect(mockFinancialServicesInner.updatable).toEqual(false);
        expect(mockFinancialServicesInner.retrieveVat).toEqual(false);
        expect(mockFinancialServicesInner.parSupMand).toEqual(' ');
        expect(mockFinancialServicesInner.flagMainSup).toEqual(' ');
        expect(mockFinancialServicesInner.selezionabile).toEqual(' ');
        expect(mockFinancialServicesInner.flagMandatory).toEqual(' ');
        expect(mockFinancialServicesInner.tassoProdotto).toEqual(false);
        expect(mockFinancialServicesInner.beneStrumentale).toEqual(false);

        let mockFinancialServicesInnerFactory = new FinancialServicesInnerFactory();
        mockFinancialServicesInnerFactory.getInstance(mockFinancialServicesInner);

        let mockFinServInnList: FinancialServicesInner[] = [];
        mockFinServInnList.push(mockFinancialServicesInner);

        mockFinancialServicesInnerFactory.getInstanceList(mockFinServInnList);

    });

    it('ProvvigioniDTO test', () => {

        let mockProvvigioniDTO = new ProvvigioniDTO();

        mockProvvigioniDTO.provImportoDeltaTasso = 0;
        mockProvvigioniDTO.provImportoDeltaTassoPerc = 0;
        mockProvvigioniDTO.provImportoServizi = 0;
        mockProvvigioniDTO.provImportoServiziPerc = 0;
        mockProvvigioniDTO.provImportoSpeseIstruttoria = 0;
        mockProvvigioniDTO.provImportoSpeseIstruttoriaPerc = 0;
        mockProvvigioniDTO.provImportoTotale = 0;
        mockProvvigioniDTO.provImportoVendita = 0;
        mockProvvigioniDTO.provImportoVenditaPerc = 0;

        expect(mockProvvigioniDTO.provImportoDeltaTasso).toEqual(0);
        expect(mockProvvigioniDTO.provImportoDeltaTassoPerc).toEqual(0);
        expect(mockProvvigioniDTO.provImportoServizi).toEqual(0);
        expect(mockProvvigioniDTO.provImportoServiziPerc).toEqual(0);
        expect(mockProvvigioniDTO.provImportoSpeseIstruttoria).toEqual(0);
        expect(mockProvvigioniDTO.provImportoSpeseIstruttoriaPerc).toEqual(0);
        expect(mockProvvigioniDTO.provImportoTotale).toEqual(0);
        expect(mockProvvigioniDTO.provImportoVendita).toEqual(0);
        expect(mockProvvigioniDTO.provImportoVenditaPerc).toEqual(0);

        let mockProvvigioniDTOFactory = new ProvvigioniDTOFactory();
        mockProvvigioniDTOFactory.getInstance(mockProvvigioniDTO);

        let mockProvvList: ProvvigioniDTO[] = [];
        mockProvvList.push(mockProvvigioniDTO);

        mockProvvigioniDTOFactory.getInstanceList(mockProvvList);

    });

    it('Rate test', () => {

        let mockRate = new Rate();

        mockRate.anticipo = false;
        mockRate.balloon = false;
        mockRate.bloccato = false;
        mockRate.cashFlowTypeCode = ' ';
        mockRate.cfAmount = 0;
        mockRate.cfAmountPercentage = 0;
        mockRate.dataPagamentoRata = ' ';
        mockRate.distanza = 0;
        mockRate.molteplicita = 0;
        mockRate.numberOfTimes = 0;
        mockRate.tassoFisso = false;
        mockRate.totalAmount = 0;
        mockRate.vatAmount = 0;
        mockRate.vatPercentage = 0;

        expect(mockRate.anticipo).toEqual(false);
        expect(mockRate.balloon).toEqual(false);
        expect(mockRate.bloccato).toEqual(false);
        expect(mockRate.cashFlowTypeCode).toEqual(' ');
        expect(mockRate.cfAmount).toEqual(0);
        expect(mockRate.cfAmountPercentage).toEqual(0);
        expect(mockRate.dataPagamentoRata).toEqual(' ');
        expect(mockRate.distanza).toEqual(0);
        expect(mockRate.molteplicita).toEqual(0);
        expect(mockRate.numberOfTimes).toEqual(0);
        expect(mockRate.tassoFisso).toEqual(false);
        expect(mockRate.totalAmount).toEqual(0);
        expect(mockRate.vatAmount).toEqual(0);
        expect(mockRate.vatPercentage).toEqual(0);

        let mockRateFactory = new RateFactory();
        mockRateFactory.getInstance(mockRate);

        let mockRateList: Rate[] = [];
        mockRateList.push(mockRate);

        mockRateFactory.getInstanceList(mockRateList);

    });

    it('ServiceEntity test', () => {

        let mockServiceEntity = new ServiceEntity();

        mockServiceEntity.type = ' ';
        mockServiceEntity.supplyTypeCode = ' ';
        mockServiceEntity.supplyTypeDesc = ' ';
        mockServiceEntity.defaultElement = false;
        mockServiceEntity.defaultSupply = false;
        mockServiceEntity.updatable = false;
        mockServiceEntity.retrieveVat = false;
        mockServiceEntity.tassoProdotto = false;
        mockServiceEntity.beneStrumentale = false;
        mockServiceEntity.rate = [];
        mockServiceEntity.attachments = [];
        mockServiceEntity.opzioneServizio = any;
        mockServiceEntity.serviceEntities = [];

        expect(mockServiceEntity.type).toEqual(' ');
        expect(mockServiceEntity.supplyTypeCode).toEqual(' ');
        expect(mockServiceEntity.supplyTypeDesc).toEqual(' ');
        expect(mockServiceEntity.defaultElement).toEqual(false);
        expect(mockServiceEntity.defaultSupply).toEqual(false);
        expect(mockServiceEntity.updatable).toEqual(false);
        expect(mockServiceEntity.retrieveVat).toEqual(false);
        expect(mockServiceEntity.tassoProdotto).toEqual(false);
        expect(mockServiceEntity.beneStrumentale).toEqual(false);
        expect(mockServiceEntity.rate).toEqual([]);
        expect(mockServiceEntity.attachments).toEqual([]);
        expect(mockServiceEntity.opzioneServizio).toEqual(any);
        expect(mockServiceEntity.serviceEntities).toEqual([]);

        let mockServiceEntityFactory = new ServiceEntityFactory();
        mockServiceEntityFactory.getInstance(mockServiceEntity);

        let mockServEntList: ServiceEntity[] = [];
        mockServEntList.push(mockServiceEntity);

        mockServiceEntityFactory.getInstanceList(mockServEntList);

    });

    it('ProposalConsultationRequestParameters test', () => {

        let idProposal = ' ';
        let processInstanceID = 0;

        let propConsReqParam = new ProposalConsultationRequestParameters(idProposal, processInstanceID);
        expect(propConsReqParam).toBeDefined();

        propConsReqParam.idProposal = ' ';
        propConsReqParam.processInstanceID = 0;

        expect(propConsReqParam.idProposal).toEqual(' ');
        expect(propConsReqParam.processInstanceID).toEqual(0);

        propConsReqParam.requestParameters();

    });

    it('Generics test', () => {

        let mockGenerics = new Generics();

        mockGenerics.introspectField2Str('test');

    });

    it('GdAutoModel test', () => {

        let mock = new GdAutoModel(5, null);
        let res = GdAutoModel.SINGLE_AUTO_ELEMENT;

        mock.fillContent();

    });

    it('TdProposalEntry test', () => {

        let mock = new TdProposalEntry(5, null);
        let res = TdProposalEntry.SINGLE_PROPOSAL_ENTRY;

        mock.fillContent();

    });


});
