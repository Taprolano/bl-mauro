import {AbstractRequestParameters} from "./AbstractRequestParameters";
import {HttpParams} from "@angular/common/http";
import {CreditQueueAllRequestParameters} from "./ProposalListAllRequestParameters";


export class ImpUserRequestParams extends AbstractRequestParameters {


    private _username: string;


    setParamsFromFilter(source: AbstractRequestParameters) {
        let filter = <ImpUserRequestParams>source;
        this._username = filter.username;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    requestParameters(): HttpParams {
        let params = new HttpParams(); // set of HttpParams return an immutable object

        if (!this._utils.isVoid(this._username)) params = params.set('username', this._username);
        this._logger.logInfo(ImpUserRequestParams.name, 'requestParameters (be-proposal)',
            params);
        return params;
    }




}