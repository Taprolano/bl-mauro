import {Normalizer} from "./Normalizer";

export class Proposal extends Normalizer {

  private _venditore?: string;
  private _chatPresente?: boolean;
  private _data?: string;
  private _numeroNote?: number;
  private _contratto?: string;
  private _stato?: number;
  private _scadenza?: string;
  private _proposta?: string;
  private _soggetto?: string;
  private _modello?: string;

  constructor() {
    super();
  }

  public get venditore(): string {
    return this._venditore;
  }
  public set venditore(value: string) {
    this._venditore = value;
  }

  public get chatPresente(): boolean {
    return this._chatPresente;
  }
  public set chatPresente(value: boolean) {
    this._chatPresente = value;
  }

  public get data(): string {
    return this._data;
  }
  public set data(value: string) {
    this._data = value;
  }

  public get numeroNote(): number {
    return this._numeroNote;
  }
  public set numeroNote(value: number) {
    this._numeroNote = value;
  }

  public get contratto(): string {
    return this._contratto;
  }
  public set contratto(value: string) {
    this._contratto = value;
  }

  public get stato(): number {
    return this._stato;
  }
  public set stato(value: number) {
    this._stato = value;
  }

  public get scadenza(): string {
    return this._scadenza;
  }
  public set scadenza(value: string) {
    this._scadenza = value;
  }

  public get proposta(): string {
    return this._proposta;
  }
  public set proposta(value: string) {
    this._proposta = value;
  }

  public get soggetto(): string {
    return this._soggetto;
  }
  public set soggetto(value: string) {
    this._soggetto = value;
  }

  public get modello(): string {
    return this._modello;
  }
  public set modello(value: string) {
    this._modello = value;
  }
}