/**
 * Classe di utility per Reflection & introspection e medoti messi a fattore comune di tutte le classi typescript
 */
export class Generics<T> {

    introspectField2Str(g: T): string[] {
        let keys = this._introspectField(g);
        return (keys)?keys.map(key => key.toString().substring(1)):[];
    }

    private _introspectField(g: T): (keyof T)[] {
        return Object.keys(g) as Array<keyof T>;
    }


}