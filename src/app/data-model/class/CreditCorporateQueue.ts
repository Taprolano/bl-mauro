export class CreditCorporateQueue {

  private _arrivo?: string;
  private _richiedente?: string;
  private _esposizione?: string;
  private _numBlocchi?: number;
  private _tipoProposta?: string;
  private _prodotto?: string;
  private _proposta?: number;
  
  constructor(arrivo?: string,
    richiedente?: string,
    esposizione?: string,
    numBlocchi?: number,
    tipoProposta?: string,
    prodotto?: string,
    proposta?: number) {
      this._arrivo = arrivo;
      this._richiedente = richiedente;
      this._esposizione = esposizione;
      this._numBlocchi = numBlocchi;
      this._prodotto = prodotto;
      this._tipoProposta = tipoProposta;
      this._proposta = proposta; 
    }

    /**@ignore nomi delle chiavi del backend, per tab relativo - devono corrispondere ai getter (autogenerare dalle var). Da aggiornare se variano. */
    static readonly keys = ["proposta", "arrivo", "richiedente", "tipoProposta", "prodotto", "esposizione"];


  /**
   * BINDING BE
   */

  get arrivo(): string {
    return this._arrivo;
  }
  set arrivo(value: string) {
    this._arrivo = value;
  }

  get richiedente(): string {
    return this._richiedente;
  }
  set richiedente(value: string) {
    this._richiedente = value;
  }

  get esposizione(): string {
    return this._esposizione;
  }
  set esposizione(value: string) {
    this._esposizione = value;
  }

  get numBlocchi(): number {
    return this._numBlocchi;
  }
  set numBlocchi(value: number) {
    this._numBlocchi = value;
  }
 
  get tipoProposta(): string {
    return this._tipoProposta;
  }
  set tipoProposta(value: string) {
    this._tipoProposta = value;
  }
  
  get proposta(): number {
    return this._proposta;
  }
  set proposta(value: number) {
    this._proposta = value;
  }

  get prodotto(): string {
    return this._prodotto;
  }
  set prodotto(value: string) {
    this._prodotto = value;
  }
}