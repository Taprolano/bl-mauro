
/* Struttura dei campi di input */

import {FireTheftCoverage} from '../class/FireTheftCoverage';
import {RtiCoverage} from '../class/RtiCoverage';

export interface FinancialInput{
    feelLife?: IFeelLifeServices,
    feelCareAxa?: IFeelCareServices,
    feelStar?: IFeelStarServices,
    feelNew?: IFeelNewServices,
    feelSale?: IFeelSafeService
}

/* 1) START FEEL LIFE  */
export interface IFeelLifeServices{
    feelLifeService?: IFeelLifeInput
}

export interface IFeelLifeInput{
    common: ICommon
}
/* END FEEL LIFE  */


/* 2) START FEEL LIFE CARE  */
export interface IFeelCareServices{
    feelLifeCareService?: IFeelCareInput
}

export interface IFeelCareInput{
    common: ICommon,
}
/* END FEEL LIFE CARE  */


/* 3) START FEEL STAR   */
export interface IFeelStarServices{
    base?: IBaseInput,
    baseLight?: IBaseLightInput,
    premiumNordOvest?: IPremiumNordOvest,
    premiumNordEst?: IPremiumNordEst,
    premiumCentro?: IPremiumCentro,
    premiumSud?: IPremiumSud
}

export interface IBaseInput{
    quattroZampe?: string,
    autoCortesia?: boolean,
    infortuniConducente?: boolean,
    commonFeelStar ?: ICommonFeelStarInput,
    common?: ICommon,
}

export interface IBaseLightInput{
    quattroZampe?: string,
    autoCortesia?: boolean,
    infortuniConducente?: boolean,
    commonFeelStar ?: ICommonFeelStarInput,
    common?: ICommon,
}

export interface IPremiumNordOvest{
    commonFeelStar ?: ICommonFeelStarInput,
    common?: ICommon,
}

export interface IPremiumNordEst{
    commonFeelStar ?: ICommonFeelStarInput,
    common?: ICommon,
}

export interface IPremiumCentro{
    commonFeelStar ?: ICommonFeelStarInput,
    common?: ICommon,
}

export interface IPremiumSud{
    commonFeelStar ?: ICommonFeelStarInput,
    common?: ICommon,
}
/* END FEEL STAR */


/* 4 START FEEL NEW */
export interface IFeelNewServices{
    rtiService?: IRtiInput,
}

export interface IRtiInput{
    polizFurtoIncendio?: boolean,
    compagniaDiAssicurzionFI?: string,
    provincia?: string,
    satellitare?: boolean,
    riepilogoTotale?: number,
    durataFeelNew?: RtiCoverage,
    durataVNFurtoIncendio?: FireTheftCoverage,
    common?: ICommon,
}
/* END FEEL NEW */

/* 5 STAR FEEL SALE */
export interface IFeelSafeService{
    feelSafeService?: IFeelSafeInput,
}

export interface IFeelSafeInput{
    common?: ICommon,
}
/* END STAR FEEL SALE */

/* COMMON */
export interface ICommonFeelStarInput{
    provincia?: string,
    satellitare?: boolean,
    privacy?: boolean
}

export interface ICommon{
    noleggioOtaxi: boolean,
    recuperoIva: boolean,
    valoreAssicurativo: number
    deroga: boolean
}