import {IFilterChoice} from "./IFilterChoice";

export interface IFilterOption {
    key: string;
    possibleChoices : IFilterChoice[];
}
