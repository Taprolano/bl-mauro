export interface IBrand {
    key:string;
    value:string;
    defaultValue?:boolean;
}
