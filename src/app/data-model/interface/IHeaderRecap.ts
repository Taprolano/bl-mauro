export interface IHeaderRecap {
  proposalsInWork: number;
  proposalsInAnalysis: number;
  proposalsInError: number;
  proposalsAccepted: number;
}