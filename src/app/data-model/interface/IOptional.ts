export interface IOptional {
    code: string,
    description: string,
    price: number,
    priceWithIVA: number,
    discount: boolean
    selected: boolean
    premiumAdjustment?:number
}
