export interface IIva {
    key?: string,
    label: string,
    value: number,
    default?: boolean
    locked?: boolean
}
