/**
 * @ignore
 * Converte le chiavi con l'equivalente backend
 */
export const ListOrderKeys = {
    PROPOSAL:'proposta',
    CONTRACT:'contratto',
    PETITIONER:'soggetto',
    STATE:'stato',
    DATE:'data',
    DEADLINE:'scadenza',
    SELLER:'venditore'
};