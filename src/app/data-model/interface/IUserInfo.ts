import {IDealershipInfo} from "./IDealershipInfo";

export interface IUserInfo {
    name: string,
    surname: string,
    jobRole: string,
    dealerships: IDealershipInfo[]
    defolutDealership: string
}
