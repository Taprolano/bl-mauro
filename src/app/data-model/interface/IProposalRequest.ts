export interface IProposalRequest {
    activationDateTo: string;
    activationDateFrom: string;
    contractNumber: string;
    proposalNumber: string,
    licensePlate: string,
    frameNumber: string,
    applicant: string,
    proposalState: number[];
}
