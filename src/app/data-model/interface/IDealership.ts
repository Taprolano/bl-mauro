import { IDealershipInfo } from "./IDealershipInfo";

export interface IDealership {

  info: IDealershipInfo;
  dealerAuth: string[];
  
  // proposalsList : {
  //   selectableFilters : string[]
  // };
  // product: {
  //   versionFilters: string[]
  // },
  // commercial: {
  //   newUsedTabs:{
  //     new: boolean,
  //     used: boolean
  //   },
  //   accessoriesAccordion: {
  //     presence: boolean,
  //     accessoriesTabs: {
  //       insert: boolean,
  //       find: boolean
  //     }
  //   }              
  //   thirdParts: {
  //     presence: boolean
  //   },
  //   suppliers: string[]
  // }
}