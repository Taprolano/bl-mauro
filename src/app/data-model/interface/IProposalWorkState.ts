export interface IProposalWorkState {
    label: string;
    desc: string;
    id: number
}
