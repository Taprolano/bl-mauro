import {IFilterChoice} from "./IFilterChoice";

export interface ISearch {
    field: string,
    filters: IFilterChoice[]
}