import {IModel} from "./IModel";
import {IVersion} from "./IVersion";

export interface IProduct {
    model: IModel,
    version: IVersion,
    productCode: string
}