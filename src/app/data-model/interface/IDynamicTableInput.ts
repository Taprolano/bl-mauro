/**
 * @ignore
 */
export interface IDynamicTableInput{
    headers: string[];
    data: string[][];
}