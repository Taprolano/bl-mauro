export interface IServiceCost {
    customerServiceCost: number,
    supplyAmount: number,
}
