export interface ISupplier {
    label: string,
    code: string
}
