import {IModel} from "./IModel";

export interface IModels {

    Altri: IModel[],
    Smart: IModel[],
    Truck: IModel[],
    Van: IModel[],
    Vetture: IModel[]

}