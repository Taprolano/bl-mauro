import {ISupplier} from './ISupplier';
import {IIva} from './IIva';
// import {IOptional} from "./IOptional";
import {IThirdPartOptional} from './IThirdPartOptional';
import { OptionalElement } from '../class/AssetOptionals';

export interface ICommercial {

    price: number;
    entry: number;
    supplier: ISupplier;
    iva: IIva;
    ipt: number;
    marketValue: number;
    exemptAmount: number;
    chassis?: string;
    brand: string;
    description?: string;
    netCost?: number;
    totalAmmount?: number;
    isNew: boolean;

    optionals: OptionalElement[];
    tpOptionals: IThirdPartOptional[];

    secondhandData: ISecondhandData;

    otherBrandOptional?: number;
}



export interface ISecondhandData {
    licensePlate: string;
    km: number;
    frame?: string;
    registrationYear: number;
    registrationMonth: number;
    chassis?: string;
}

