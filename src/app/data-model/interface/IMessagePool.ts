import {NotificationMessage} from "../../shared/components/notification/NotificationMessage";

export interface IMessagePool {
    type: string;
    size: number;
    mainMessage: string;
    messages: NotificationMessage[];
    category?: string;
}
