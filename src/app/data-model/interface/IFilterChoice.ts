export interface IFilterChoice {
    value: string;
    label: string;
    selected: boolean;
}
