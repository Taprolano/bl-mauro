export interface IDealershipInfo {
  name: string;
  id: string;
}