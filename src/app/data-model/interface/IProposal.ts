import {IProduct} from "./IProduct";
import {ICommercial} from "./ICommercial";
import {IFinancial} from "./IFinancial";
import {IRegistry} from "./IRegistry";
import {IPreliminary} from "./IPreliminary";
import {IContract} from "./IContract";
import {IFinalize} from "./IFinalize";
import {IProposalSummary} from "./IProposalSummary";

export interface IProposal {
    product: IProduct,
    commercial: ICommercial,
    financial: IFinancial,
    registry: IRegistry,
    preliminary: IPreliminary,
    contact: IContract,
    finalize: IFinalize,
    summary: IProposalSummary,
    hash: string,
    state: string
}