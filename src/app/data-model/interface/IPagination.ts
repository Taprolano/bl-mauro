export interface IPagination {
    keys: string[],
    numberOfResults: number,
    recordPerPage: number,
    page: number,
    nPage: number,
    size: number
}
