import {IProfile} from "./IProfile";

export interface IAuthResponse {
    authorities: Authority[],
    details: string,
    authenticated: boolean,
    principal: IPrincipal
    authorizedClientRegistrationId: string,
    credentials: string,
    name: string
}

export interface IPrincipal {
    name: string,
    firstName: string,
    codTer: number,
    bpName: string,
    surname: string,
    email: string,
    phone: string,
    username: string,
    profiles: IProfile[],
    authorities: Authority[],
    attributes: IAttributes
}

export interface Authority {
    authority: string,
    source?: IPrincipalImpersonate
}


export interface IPrincipalImpersonate {
    authorities: string[],
    details: string,
    authenticated: boolean,
    principal: IPrincipal,
    authorizedClientRegistrationId: string,
    credentials: string,
    name: string
}

export interface IAttributes {
    phone: string,
    user_name: string,
    surname: string,
    scope: string[],
    name: string,
    exp: string,
    iat: string,
    jti: string,
    client_id: string,
    email:string,
    username: string
}
