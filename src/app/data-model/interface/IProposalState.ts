export interface IProposalState {
    label: string,
    id: number,
    route: string,
    dataModelKey: string,
    state: string
}
