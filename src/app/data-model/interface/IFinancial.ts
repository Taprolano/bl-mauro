import {FinancialPlans} from '../class/FinancialPlans';
import {IIva} from './IIva';
import {FinancialCampaign} from '../class/FinancialCampaign';
import {FinancialValue} from '../class/FinancialValue';

export interface IFinancial {

    iva: IIva,
    selectedCampaign: FinancialCampaign,
    financialPlans: FinancialPlans,
    selectedServicesList: FinancialValue[],

}