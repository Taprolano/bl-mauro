import {IOptional} from './IOptional';
import {ISupplier} from './ISupplier';
import {IIva} from './IIva';

export interface IThirdPartOptional {
    optional: IOptional;
    supplier: ISupplier;
    iva: IIva;
}
