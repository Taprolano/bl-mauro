import {IFunctionality} from "./IFunctionality";

export interface IProfile {
    "code": string,
    "description": string,
    "app": string,
    "functions": IFunctionality[]
}

