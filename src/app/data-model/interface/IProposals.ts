export interface IProposals {
    asset?: number,
    proposal: string,
    contract: string,
    subject: string,
    state: string,
    date: string,
    deadline: string,
    priority?: string,
    seller: string
}



// ASSET	PROPOSTA	CONTRATTO	iSOGGETTO STATO DATA	SCADENZA	PRIORITÀ	VENDITORE