export interface IProposalSummary {
    model: string,
    version: string,
    productCode:string,
    financial:string,
    registry: {
        accountHolder: string,
        type:string
    },
    preliminary: string,
    contract: string

}
