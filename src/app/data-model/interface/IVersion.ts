export interface IVersion {
    productCode: string,
    version: string,
    fuel: string,
    catalog:string,
    price: number,
    forceSecondHand: boolean
}
