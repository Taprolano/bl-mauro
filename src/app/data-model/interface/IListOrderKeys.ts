/**
 * @ignore
 * Converte le chiavi con l'equivalente backend
 * @type {{PROPOSAL: string; CONTRACT: string; PETITIONER: string; STATE: string; DATE: string; DEADLINE: string; SELLER: string}}
 */
export const ListOrderKeys = {
    PROPOSAL:'proposta',
    CONTRACT:'contratto',
    SUBJECT:'soggetto',
    STATE:'stato',
    DATE:'data',
    DEADLINE:'scadenza',
    SELLER:'venditore'
};