import { IDealershipInfo } from "./IDealershipInfo";

export interface IDealer {
	id: string;
  name: string;
	dealerType: string;
	dealershipsAvailable: IDealershipInfo[];
	dealershipSelected: string
}