export interface IFunctionality {
    code: string,
    description: string
    attributes: string[]
}
