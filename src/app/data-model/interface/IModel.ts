export interface IModel {
  imagePath: string;
  assetTypeDesc: string;
  makeDesc: string;
  brandId ?: string;
  orderNumber: number;
  visibleAsNew: string;
  makeCode: string;
  assetType: string;
  assetClassCode: string;
  linkPromotion?: string;
  hovered?: boolean;
}

// "imagePath": "/media/images/assets/MBA.jpg",
//     "assetTypeDesc": "Classe A",
//     "makeDesc": "Mercedes-Benz",
//     "orderNumber": 100,
//     "visibleAsNew": "1",
//     "makeCode": "MB",
//     "assetType": "MBA",
//     "assetClassCode": "10"