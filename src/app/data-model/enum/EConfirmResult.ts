/**
 * Risultato di una richiesta di conferma tramite modale
 */
export const enum ConfirmResult{
    CONFIRM = 1,
    UNDO = 2
}