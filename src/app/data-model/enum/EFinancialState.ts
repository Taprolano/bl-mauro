
export  enum FINANCIAL_SERVICES{
    FEEL_LIFE = '44',
    FEEL_CARE_AXA = '71',
    FEEL_STAR ='38',
    FEEL_NEW = '39',
    FEEL_SAFE = '43',
    ASSISTENZA = '60', // ipotizzo
}

/* 44 */
export  enum FINANCIAL_FEEL_LIFE{
    FEEL_LIFE = '440'
}

/* 71 */
export  enum FINANCIAL_FEEL_CARE_AXA{
    FEEL_CARE_AXA = '710'
}

/* 38 */
export  enum FINANCIAL_FEEL_STAR{
    BASE = '381',
    BASE_LIGHT = '3810',
    PREMIUM_NORD_OVEST = '382',
    PREMIUM_NORD_EST = '383',
    PREMIUM_CENTRO = '384',
    PREMIUM_SUD = '385'
}

/* 39 */
export  enum FINANCIAL_FEEL_NEW{
    FEEL_NEW = '390'
}

/* 43 */
export  enum FINANCIAL_FEEL_SAFE{
    FEEL_SAFE = '430'
}

/* 60 */
export  enum ASSISTENZA{
    SERVICE_PLUS_EXCELLENT = '600',
    SERVICE_PLUS_COMPACT = '602',
    SERVICE_PLUS_CONFORT = '605',
    SERVICE_PLUS_ADVANCE = '609'
}
