import { CommonModule } from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SharedModule } from  './shared/shared.module'
import {ProposalService} from "./shared/services/proposal.service";
import {RouterModule} from "@angular/router";
import {NgxSpinnerModule} from "ngx-spinner";
import {MbzDashboardModule} from "./mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "./proposals/proposals.module";
import {DirectAccessGuard} from "./shared/guard/direct-access/direct-access.guard";
import {DEFAULT_TIMEOUT, HttpConfigInterceptor} from "./shared/interceptor/http.config.interceptor";
import {AppSettings} from "../AppSettings";


// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        TranslateModule,
        HttpClientModule,
        MbzDashboardModule,
        ProposalsModule,
        NgxSpinnerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    declarations: [AppComponent],
    providers: [
        AuthGuard,
        DirectAccessGuard,
        [{ provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }],
        [{ provide: DEFAULT_TIMEOUT, useValue: AppSettings.HTTP_TIMEOUT_CALLS }]    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
