import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import {ProposalsModule} from "./proposals/proposals.module";
import {AppRoutingConstant} from "./appRoutingConstat";
import {ListTestComponent} from "./proposals/components/list-test/list-test.component";
import {MbzDashboardComponent} from "./mbz-dashboard/mbz-dashboard/mbz-dashboard.component";


const path = AppRoutingConstant.path;

const routes: Routes = [
    { path: '', redirectTo: 'layout', pathMatch: 'prefix' },
    { path: 'layout', loadChildren: './layout/layout.module#LayoutModule'},
    
    //{ path: path.proposals, loadChildren: './layout/proposals/proposals.module#ProposalsModule', canActivate: [AuthGuard] },
    //{ path: path.login, loadChildren: './login/login.module#LoginModule' },
    { path: path.signup, loadChildren: './signup/signup.module#SignupModule' },
    { path: path.error, loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: path.accessDenied, loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: path.notFound, loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: 'mbz-dashboard' , component:MbzDashboardComponent },
    { path: '**', redirectTo: 'not-found'},

];

@NgModule({
    imports: [RouterModule.forRoot((routes), { initialNavigation: true, useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
