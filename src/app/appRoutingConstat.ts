export class AppRoutingConstant {
    public static PROPOSALS = 'proposals';
    public static LOGIN = 'login';
    public static SIGNUP = 'signup';
    public static ERROR = 'error';
    public static ACCESS_DENIED = 'access-denied';
    public static NOT_FOUND = 'not-found';

    public static path = {
        proposals: 'proposals',
        login: 'login',
        signup: 'signup',
        error:'error',
        accessDenied: 'access-denied',
        notFound:'not-found',
        home:'home',
    };

    public static fullPath(key: string){
        return '/layout/'+this.path[key];
    };
}