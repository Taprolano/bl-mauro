import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppSettings } from 'src/AppSettings';
import { WorkInProgressComponent } from 'src/app/shared/components/work-in-progress/work-in-progress.component';
import { AbstractModalService } from 'src/app/shared/components/abstract-modal/abstract-modal.service';
import {takeUntil} from "rxjs/internal/operators";
import { Subject, Subscription } from 'rxjs';
import {TranslateService} from "@ngx-translate/core";
import {LoggerService} from "../services/logger.service";
import {Order} from "../../data-model/interface/EOrder";

/**
 * AbstractListMng: Contiene i metodi comuni alle liste
 */

export abstract class AbstractListMng implements OnInit {

    /**@ignore*/
    @Input() id: string = 'mbz-components-list__id'; //default

    /** Headers lista */
    abstract readonly headers:string[];
    /** @ignore */
    abstract readonly objectKeys:string[];
    /**@ignore dimensione colonne in %, per allineamento header fisso*/
    abstract readonly __widths: string[];
    /**@ignore altezza della lista*/
    abstract readonly __wrapperHeight: string;



    /**@ignore*/
    @Input() list;

    /**@ignore*/
    @Output() scrollEvent = new EventEmitter();

    /**@ignore*/
    @Output() sortEvent = new EventEmitter();

    /**@ignore*/
    callBack: Function;

    /**@ignore*/
    unsubscribe = new Subject<any>();


    /**
     * @ignore
     * tiene traccia dell'header selezionato per il sorting (per css)
     */
    protected sorted;


    /**
     * @ignore
     * stringa per requestParameters
     */
    protected sortString;

    /**@ignore*/
    constructor(
        protected _abstractModalService: AbstractModalService,
        protected translate: TranslateService,
        protected _logger: LoggerService
    ) { }

    /**
     * Emette l'evento di scroll
     */
    emitterFunc() { this.scrollEvent.emit(); }

    /**
     * Invoca le primitive per inizializzare il comportamento della tabella delle proposte
     */
    ngOnInit() {
        this.callBack = this.emitterFunc.bind(this);
        // Opzioni e aggiunge callback per emettere l'evento input
        $(this.id).mCustomScrollbar({
                theme: AppSettings.scrollStyle,
                callbacks: {
                    onTotalScrollOffset: 400,
                    alwaysTriggerOffsets: true,
                    onTotalScroll: () => { this.emitterFunc(); }
                }
            }
        );
    }


    /**
     * Mostra le informazioni aggiuntive presenti per ciascuna proposta
     * @param {number} index
     */
    openAccordion(index: number) {
        // aggiunge o rimuove la classe css che nasconde l'elemento
        this._logger.logInfo('AccordionListComponent', 'openAccordion', 'opening accordion' + index)
        //console.log('opening accordion' + index);
        if (document.getElementById('accordion' + index).classList.contains('hidden')) {
            // accordion styling
            document.getElementById('accordion' + index).classList.remove('hidden');
            // tr row styling
            document.getElementById('row' + index).classList.add('active');
            //trigger styling
            document.getElementById('trigger' + index).getElementsByTagName('svg')[0].style.transform="rotate(180deg)";
        }
        else {
            // accordion styling
            document.getElementById('accordion' + index).classList.add('hidden');
            // tr row styling
            document.getElementById('row' + index).classList.remove('active');
            //trigger styling
            // document.getElementById('trigger' + index).style.transform = "";
            document.getElementById('trigger' + index).getElementsByTagName('svg')[0].style.transform="";

        }
    }

    /**
     * @ignore
     * mostra modale di WIP
     */
    openUnavailableContent() {
        let customObj = {
            component: WorkInProgressComponent,
            parameters: {title: "", message: ""}
        };

        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
    }

    /**
     * Restituisce il tipo di ordinamento richiesto eg: (asc, desc o "")
     * @param {string} id
     * @returns {string}
     */
    sortTable(id:string):string {
        /* gestisce i css per mostrare le icone di ordinamento */
        let target = document.getElementById(id);
        let targetClassList = target.classList;
        let order="";
        // imposta la classe che mostra o nasconde la relativa icona di ordinamento
        if(targetClassList.contains('order-none')){
            targetClassList.remove('order-none');
            targetClassList.add('order-desc'); //none->desc
            order = Order.DESC;
        }
        else if(targetClassList.contains('order-desc')){
            targetClassList.remove('order-desc');
            targetClassList.add('order-asc'); //desc->asc
            order = Order.ASC;
        }
        else {
            targetClassList.remove('order-asc');
            targetClassList.add('order-none'); //asc->none
        }
        return order;
    }

    /**
     * Re-inizializza graficamente le icone che visualizzano il tipo di ordinamento richiesto
     * @param {string} id
     */
    resetSort(id:string){
        /* resetta la classe css che mostre le icone ordinamento */
        let target = document.getElementById(id).classList;
        target.remove('order-asc');
        target.remove('order-desc');
        target.add('order-none');
    }

    /**
     * Innesca la chiamata per ottenere la lista ordinata per id
     * @param {string} id
     */
    sortByField(id:string){
        //resetta eventuali altre colonne con icone ordinamento
        let old = this.sorted;
        if(old && (id != old) ) this.resetSort(old);
        this.sorted = id;
        let order = this.sortTable(id);
        //scarica nuova lista
        let keyIndex = id.substring(1);
        let key = this.objectKeys[keyIndex];
        if(!order)this.sortString="";             //elimina ordinamento
        else this.sortString=key+','+order;  //imposta ordinamento
        //resetta scroll
        this.goToTop();
        //scarica nuove proposte (ordinate come richiesto)
        this.sortEvent.emit(this.sortString);
    }

    /**
     * Sposta il focus dell'utente all'inizio della pagina
     */
    goToTop(){
        /*resetta scroll lista e pagina*/
        let elements = document.getElementsByClassName("scrollable__element");
        for(let i = 0; i < elements.length; i++) {
            $(elements[i]).mCustomScrollbar("scrollTo", "top", {
                scrollInertia: 0 //altrimenti viene interrotto lo scroll se ancora in corso all'arrivo della lista
            });
            // console.log("scrollable__element class:", elements[i]);
            // console.log("scrollable__element id:", document.getElementById("mbz-components-scroll__id"));
        }
    }

}
