
export class ModalEvent {
  private _event: string;


  constructor(event: string) {
      this._event = event;
  }


  get event(): string {
      return this._event;
  }


}
