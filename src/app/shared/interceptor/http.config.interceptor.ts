import {Inject, Injectable, InjectionToken} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse, HttpResponseBase
} from '@angular/common/http';

import {Observable, ObservableInput, throwError} from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {LoggerService} from "../services/logger.service";
import {AppSettings} from "../../../AppSettings";
import {PERSISTENCE_KEY, PersistenceService} from "../services/persistence.service";
import {UtilsService} from "../services/utils.service";
import {environment} from "../../../environments/environment";
import {timeout} from "rxjs/internal/operators";


export const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');


@Injectable()
/**
 * Intecetta tutte le chiamate/risposte effettuate verso i servizi di BE e in caso di errore di autenticazione
 * rimuove i dati applicativi e reindirizza alla login erogata dall'identity server (OAUTH2)
 */
export class HttpConfigInterceptor implements HttpInterceptor {


    constructor(
                @Inject(DEFAULT_TIMEOUT) protected defaultTimeout: number,
                private _logger: LoggerService,
                private _utils: UtilsService,
                private _persistenceService: PersistenceService) { }


    /**
     * Gestisce tutti gli errori di auteticazione
     * @param {HttpRequest<any>} request
     * @param {HttpHandler} next
     * @returns {Observable<HttpEvent<any>>}
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this._logger.logInfo(this.constructor.name, 'intercept', request);

        const timeoutValue = Number(request.headers.get('timeout')) || this.defaultTimeout;

        return next.handle(request).pipe(timeout(timeoutValue)).pipe(
            map((event: HttpEvent<any>) => {

                this._logger.logInfo(this.constructor.name, 'intercept', 'handle-event', event);

                if (event instanceof HttpResponse) {
                    this._logger.logInfo(this.constructor.name, 'intercept', 'handle-http-response', event);
                    // this.errorDialogService.openDialog(event);

                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {


                /*                    if (error.error instanceof ErrorEvent) {
                                        // A client-side or network error occurred. Handle it accordingly.
                                        this._logger.logError(this.constructor.name, 'intercept', 'catchError',
                                            `Backend returned code ${error.status} ` );

                                    } else {
                                        // The backend returned an unsuccessful response code.
                                        // The response body may contain clues as to what went wrong,
                                        this._logger.logError(this.constructor.name, 'intercept', 'catchError',
                                            `Backend returned code ${error.status}, ` +
                                            `body was: ${error.error}`);
                                    }*/

                // issue angular nella gestione degli errori 302 e redirect viene inserito come status 0 e codice unknown
                // per gestire questo problema si esegue la check su questo codice e messaggio specifico
                if((error.status===AppSettings.HTTP_BROWSER_NONE
                        && error.message == AppSettings.HTTP_FOUND_MESSAGE)
                    &&  !this._utils.isVoid(this._persistenceService.get(PERSISTENCE_KEY.CHECK_INTERCEPTOR, true))) {

                    this._logger.logWarn(this.constructor.name, 'intercept', 'mng auth redirect on 302');
                    this._persistenceService.removeAll();
                    this._persistenceService.remove(PERSISTENCE_KEY.CHECK_INTERCEPTOR, true);

                    // manage loop on local architecture
                    if(!environment.local) {
                        document.location.reload(true);
                    }


                } else if((error.status===AppSettings.HTTP_BROWSER_NONE
                        && error.message == AppSettings.HTTP_FOUND_MESSAGE)
                    &&  this._utils.isVoid(this._persistenceService.get(PERSISTENCE_KEY.CHECK_INTERCEPTOR, true))) {

                    this._persistenceService.save(PERSISTENCE_KEY.CHECK_INTERCEPTOR, 'force-logout', true);

                }


                this._logger.logInfo(this.constructor.name, 'intercept', 'catchError', error);
                return throwError(error);
            }));
    }
}