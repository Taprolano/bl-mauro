import { Pipe, PipeTransform } from '@angular/core';
import {ProposalsConstant} from "../../proposals/proposalsConstant";

@Pipe({
  name: 'stateFromCode'
})
export class ProposalStateFromCodePipe implements PipeTransform {

  transform(value: number, args?: any): any {
    let statesList = ProposalsConstant.PROPOSAL_STATES_4_FILTER_BAR

    for(let state of statesList){
      if(state.id ===  value) return state.label
    }

    return value;
  }

}
