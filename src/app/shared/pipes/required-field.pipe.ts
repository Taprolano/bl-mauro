import {Pipe, PipeTransform} from '@angular/core';
const obbligatoryField = '*';

@Pipe({
    name: 'required'
})

export class RequiredFieldPipe implements PipeTransform {


    transform(value: any, args?: any): any {
        return value + obbligatoryField;
    }
}