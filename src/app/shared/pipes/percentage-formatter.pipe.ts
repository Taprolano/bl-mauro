import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'percentageFormatter'
})
export class PercentageFormatterPipe implements PipeTransform {

    transform(value: number, args?: any): string {

        let number = 0;
        if(!!value){
            number = value
        }
        let decimal = 2;

        if (!!args) {
            decimal = +args;
        }

        return number.toFixed(decimal).replace(/\./,",") + " %";
    }

}
