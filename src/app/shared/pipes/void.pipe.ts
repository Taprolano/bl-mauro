import { Pipe, PipeTransform } from '@angular/core';
import {AppSettings} from "../../../AppSettings";

@Pipe({
  name: 'void'
})
export class VoidPipe implements PipeTransform {


  transform(value: any, args?: any): any {

      if(args)
          return (this.isVoid(value))?args:value;
      else
          return (this.isVoid(value))?AppSettings.ND_FIELDS_VALUE:value;
  }


  isVoid(input) {
      return input==='undefined' || input===undefined || input===null || input==='null' || input==='';
  }


}
