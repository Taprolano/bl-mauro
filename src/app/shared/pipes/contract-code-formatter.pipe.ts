import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contractCodeFormatter'
})
export class ContractCodeFormatterPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    let ret= value;
    return !!ret ? ret.replace(/0*/g, ""): ret;
  }

}
