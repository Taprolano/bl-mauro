import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'currencyFormatter'
})
export class CurrencyFormatter implements PipeTransform {

    transform(val: number): string {
        if (val !== undefined && val !== null) {
            // here we just remove the commas from value

            let ret: string = val.toString().replace(/,/g, "*");
            ret = ret.replace(/\./g, ",");
            ret = ret.replace(/\*/g, ".");
//            ret = ret.replace(/€/g, "€ ");
            ret = ret.replace(/€/g, "")+" €";

            return ret;
        } else {
            return "";
        }
    }
}
