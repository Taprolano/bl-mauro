import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormatter'
})
export class DateFormatterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      let day = value.split('-')[0];
      let month = value.split('-')[1];
      let year = value.split('-')[2];
    return `${day}/${month}/${year}`;
  }

}
