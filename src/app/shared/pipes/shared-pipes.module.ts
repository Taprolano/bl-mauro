import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CurrencyFormatter} from './currency-formatter.pipe';
import {VoidPipe} from './void.pipe';
import {ContractCodeFormatterPipe} from './contract-code-formatter.pipe';
import {DateFormatterPipe} from './date-formatter.pipe';
import {ProposalStateFromCodePipe} from './proposal-state-from-code.pipe';
import {PercentageFormatterPipe} from './percentage-formatter.pipe';
import {RequiredFieldPipe} from "./required-field.pipe";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [CurrencyFormatter, VoidPipe, ContractCodeFormatterPipe, DateFormatterPipe, ProposalStateFromCodePipe, PercentageFormatterPipe, RequiredFieldPipe],
    exports: [CurrencyFormatter, VoidPipe, ContractCodeFormatterPipe, PercentageFormatterPipe, RequiredFieldPipe]
})
export class SharedPipesModule {
}
