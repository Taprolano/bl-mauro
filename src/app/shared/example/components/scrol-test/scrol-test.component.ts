import { Component, OnInit, HostListener } from '@angular/core';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Component({
  selector: 'app-scrol-test',
  templateUrl: './scrol-test.component.html',
  styleUrls: ['./scrol-test.component.scss']
})
export class ScrolTestComponent implements OnInit {

  elements = [];
  length = 42;

  scrolling = 0;
  constructor(private _logger: LoggerService) { }

  ngOnInit() {
    this.append(50);
  }

  append(n){
    let current = this.elements.length
      for(let i of Array.from({length: (n)}, (v, k) => k+1)){
          let number = i + current;
          this.elements.push("elemento "+number);
      }
  };

    @HostListener("window:scroll", [])
    onScroll(): void {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {

            this._logger.logInfo('ScrolTestComponent','onScroll', 'SEI ARRIVATO IN FONDO');
            // console.log("SEI ARRIVATO IN FONDO");
            this.append(50);
        }
    }
}
