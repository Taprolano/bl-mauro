import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrolTestComponent } from './scrol-test.component';

describe('ScrolTestComponent', () => {
  let component: ScrolTestComponent;
  let fixture: ComponentFixture<ScrolTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrolTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrolTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
