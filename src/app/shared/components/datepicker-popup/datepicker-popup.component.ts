import {Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges} from '@angular/core';
import * as moment from 'moment';
import 'jquery';
import {DateTimePickerDirective} from "ng2-eonasdan-datetimepicker";

import {Moment} from "moment";
import { LoggerService } from '../../services/logger.service';

/**
 * @ignore
 */
declare var $: any;

/**
 * DatepickerPopupComponent: Componente che incapsula il comportamento del date-picker
 */
@Component({
    selector: 'app-datepicker-popup',
    templateUrl: './datepicker-popup.component.html',
    styleUrls: ['./datepicker-popup.component.scss'],
})
export class DatepickerPopupComponent implements OnInit, OnChanges {


    /**
     * @ignore
     * @type {string}
     */
    @Input() label = 'Data';

    /**
     * @ignore
     * @type {string}
     */
    @Input() placeholder = 'Selezionare una data';

    /**
     * @ignore
     * @type {string}
     */
    @Input() strDate = '';

    /**
     * @ignore
     * @type {EventEmitter<any>}
     */
    @Output() strDateChange = new EventEmitter();

    /**
     * @ignore
     * @type {null}
     */
    @Input() minDate = null;

    /**
     * @ignore
     * @type {null}
     */
    @Input() maxDate = null;

    /**
     * @ignore
     * @type {false}
     */
    dateOpen = false;

    /**
     * @ignore
     */
    date;

    /**
     * @ignore
     */
    @ViewChild(DateTimePickerDirective) dp: DateTimePickerDirective;

    /**
     * @ignore
     * @type {{format: string; showClose: boolean; showClear: boolean; useCurrent: boolean; tooltips: {today: string; clear: string; close: string; selectMonth: string; prevMonth: string; nextMonth: string; selectYear: string; prevYear: string; nextYear: string}; icons: {time: string; date: string; up: string; down: string; previous: string; next: string; today: string; close: string; clear: string}}}
     */
    options: any = {
        format: 'DD/MM/YYYY', showClose: true, showClear: true, useCurrent: false,
        tooltips: {
            today: 'Oggi',
            clear: 'Reset Data',
            close: 'Chiudi',
            selectMonth: 'Seleziona il Mese',
            prevMonth: 'Mese Precedente',
            nextMonth: 'Mese Prossimo',
            selectYear: 'Seleziona Anno',
            prevYear: 'Anno Precedente',
            nextYear: 'Anno Prossimo',
        },
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-arrow-up',
            down: 'fa fa-arrow-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-calendar-check-o',
            close: 'fa fa-times',
            clear: 'fa fa-trash'
        }
    };

    /**
     * @ignore
     */
    constructor(private _logger: LoggerService) {
    }


    /**
     * Inizializza il formato delle date
     */
    ngOnInit() {
        this.date = this.getMomentFromBEStringFormat(this.strDate);
        if (!!this.minDate) this.options.minDate = this.minDate;
        if (!!this.maxDate) this.options.maxDate = this.maxDate;
        
        this._logger.logInfo('DatepickerPopupComponent','ngOnInit', "moment date: ", this.date);
        // console.log("[Datepicker] moment date: ", this.date);
    }

    /**
     * Invoca la primitiva per aprire il datepicker
     */
    open() {
        this.dp.datepicker.show();
    }

    /**
     * @ignore
     */
    ngOnChanges() {
        if (!!this.minDate) this.options.minDate = this.minDate;
        if (!!this.maxDate) this.options.maxDate = this.maxDate;
    }

    /**
     * Aggiorna i dati selezionati attraverso il date-picker ed emette l'evento che veicola la data inserita
     */
    update() {
        this.strDate = (!!this.date && this.date.isValid()) ? moment(this.date).format(this.options.format) : '';
        this.strDateChange.emit(this.formatDate4BE(this.strDate));
        if (!!this.minDate) this.options.minDate = this.minDate;
        if (!!this.maxDate) this.options.maxDate = this.maxDate;
    }

    /**
     * Data la stringa passata come parametro esegue la conversione in oggetto moment con il
     * formato anno-mese-giorno
     * @param {string} strDate
     * @returns {any}
     */
    private getMomentFromBEStringFormat(strDate: string) {
        this._logger.logInfo('DatepickerPopupComponent','getMomentFromBEStringFormat', "start", strDate);
        // console.log("[DATEPIKER] start",strDate);
        if (!!strDate) {
            let day = strDate.split('-')[0];
            let month = strDate.split('-')[1];
            let year = strDate.split('-')[2];
            this._logger.logInfo('DatepickerPopupComponent','getMomentFromBEStringFormat', "end", `${year}-${month}-${day}`);
            // console.log("[DATEPIKER] end",`${year}-${month}-${day}`);

            return moment(`${year}-${month}-${day}`);
        }

        return null;
    }

    /**
     * Esegue la conversione per il formato richiesto dai servizi di backend
     * @param feStringDate
     * @returns {string}
     */
    formatDate4BE(feStringDate): string{
        if (!!feStringDate) {
            let day = feStringDate.split('/')[0];
            let month = feStringDate.split('/')[1];
            let year = feStringDate.split('/')[2];
            return `${day}-${month}-${year}`;
        }
        return null;
    }
}

