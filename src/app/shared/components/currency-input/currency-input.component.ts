import {
  Component, EventEmitter, forwardRef, HostListener, Input, OnInit, Output, Renderer2,
  ViewChild
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {ControlValueAccessor} from '../control-value-accessor';

@Component({
  selector: 'app-currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CurrencyInputComponent),
      multi: true
    }
  ]
})
export class CurrencyInputComponent implements ControlValueAccessor, OnInit {

  @ViewChild('target') innerInput;

  @Input() label = '';

  @Output()
  blurEvent = new EventEmitter();

  @Output()
  focusEvent = new EventEmitter();

  @Input() containerId;

  // stringhe di classi separate da spazi

  @Input() containerCss = 'a-input a-input--text';
  @Input() inputCss = 'inputCurrency';
  @Input() labelCss = 'a-input__label';

  decimalID: string;
  _currencyValue: number;
  stringValue = '0,00 €';

  numberToReturn = 0.00;

  propagateChange = (_: any) => {
  }


  constructor(private translate: TranslateService,
              private _renderer: Renderer2) {
  }

  ngOnInit() {
    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
    this.translate.setDefaultLang('it');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

    this.decimalID = this.label + '-decimal';
  }

  /**
   * Metodo ControlValueAccessor del form, i.e. chiamato da form control per settare il valore del form
   * @param value
   */
  writeValue(value: any) {
    if (value !== undefined) {
      this._currencyValue = value;
    }
    this.createOutputString(value);
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  /**
   * Ritorna il valore numerico del form
   * @returns {any}
   */
  get currencyValue() {
    return this._currencyValue;
  }

  /**
   * Imposta il valore "output" del form, ovvero letto da eventuali form control
   * @param val
   */
  set currencyValue(val) {
    if (isNaN(val)) { return; }
    this._currencyValue = val;
    this.propagateChange(this._currencyValue);
  }

  /**
   * Processa input per ottenere il valore del campo.
   */
  updateOutput() {

    // let startValue = this.stringValue;
    if (this.stringValue.length > 0) {// console.log("START FROM: " + this.stringValue);
      // console.log("NUMERIC: " + (+this.stringValue));
      // let value = this.stringValue.replace(/[^\d,]/g, '');
      // console.log("value: " + value);
      // value = value.replace(/,/g, '.');
      // console.log("value: " + value);
      // let numValue = +value;
      // console.log("numValue: " + numValue);
      let value = this.stringValue.replace(/[^\d,]/g, '');
      value = value.replace(/,/g, '.');
      const numValue = +value;
      this.createOutputString(numValue);
      this.currencyValue = numValue;
      // +" €"
      /*console.log("value: "+value);
      let integerPart = value.split(',')[0];
      console.log("integerPart: "+integerPart);
      let suffix = !!value.split(',')[1]
          ?","+value.split(',')[1]+" €"
          :",00 €";
      console.log("suffix: "+suffix);

      let dotNumber =  Math.ceil(integerPart.length / 3);
      console.log("dotNumber: "+dotNumber);
      let startFrom = integerPart.length % 3;
      console.log("startFrom: "+startFrom);

      let counter = 3 - integerPart.length % 3;
      console.log("counter: "+counter);
      let final = "";
      let dn = 0;
      for(let char of integerPart){

          if (counter == 3 && dn != 0 ){
              final = final+"."+char;
              counter = 1 ;
              dn++;
          }else{
              final = final+char;
              counter++;
          }
          console.log("final: "+final);
      }

      this.stringValue = final+suffix;
      console.log("stringValue: "+this.stringValue);*/
    } else {
      this.stringValue = '0,00 €';
      // console.log("stringValue: " + this.stringValue);
      this.currencyValue = 0;
    }
    this.blurEvent.next();
    //       this.currencyValue = total;
  }


  /**
   * Formatta e assegna la stringa di output del form in base al valore numerico fornito
   * @param numValue
   */
  createOutputString(numValue) {
    if (!isNaN(numValue)) {
      this.numberToReturn = Math.round(numValue * 100) / 100;
//            let integer =parseInt(numValue);
      const integer = Math.floor(numValue);
      const integerStr = integer.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');

      const decimal = Math.round((numValue % 1) * 100);
      const decimalStr = decimal > 10 ? decimal.toString() : '0' + decimal;

      this.stringValue = integerStr + ',' + decimalStr + ' €';
    } else {
      this.stringValue = this.numberToReturn.toString().replace(/\./g, ',') + ' €';
    }
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this._renderer.setAttribute(this.innerInput.nativeElement, 'disabled', String(isDisabled)) :
        this._renderer.removeAttribute(this.innerInput.nativeElement, 'disabled');

  }

}
