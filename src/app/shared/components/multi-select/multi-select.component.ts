import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {Observable} from "rxjs";

/**
 *  Multi select component per la selezione di una o piu' opzioni
 */

@Component({
    selector: 'app-multi-select',
    templateUrl: './multi-select.component.html',
    styleUrls: ['./multi-select.component.scss'],
})
export class MultiSelectComponent implements OnInit {

    /**
     * Lista delle opzioni selezionabili nella select
     */
    @Input()
    listItems: any[];
    /**
     * Parametro per rendere la select a scelta multipla
     */
    @Input()
    multiple: boolean;
    /**
     * Label della select
     */
    @Input()
    label: string;
    /**
     * Placeholder che verra' visualizzato all'interno della select in caso di nessuna opzione selezionata
     */
    @Input()
    placeholder: string;
    /**
     * Parametro per il reset esterno della multi select
     */
    @Input()
    reset: Observable<boolean>;

    /**
     * Lista degli elementi selezionati nella multi select
     */
    @Output()
    private _itemSelected = new EventEmitter();

    /**
     * @ignore parametro che determina lo stato aperto o chiuso della tendina
     */
    listOpen: boolean = false;
    /**
     * @ignore parametro che determina se il selettore ALL e' selezionato o meno
     */
    allSelected: boolean = false;
    /**
     * @ignore
     */
    private _selectedItems: any[] = [];
    /**
     * @ignore label dell'opzione selezionata in caso di select singola
     */
    selectedItemsLabel: string;

    /**
     * @ignore
     */
    icon = 'arrow-down';
    /**
     * @ignore
     */
    private _id = Math.floor(Math.random() * 1000) + 1;
    /**
     * @ignore
     */
    id = `multi-select-${this._id}`;


    constructor() {
    }

    ngOnInit() {
    }

    /**
     * Funzione che switcha lo stato della tendina tra aperto e chiuso
     */
    toggleList() {
        this.listOpen = !this.listOpen;
    }

    /**
     * Funzione che aggiorna la lista degli elementi selezionati
     */
    selectItem(item) {
        if (this.multiple) {
            let index = this._selectedItems.findIndex(x => x === item);
            if (index > -1) {
                this._selectedItems.splice(index, 1);
            } else {
                this._selectedItems.push(item);
            }
        } else {
            this._selectedItems = [item];
            this.selectedItemsLabel = item.value;
            this.listOpen = false;
        }

        if (this.selectedItems.filter(function(e) { return e.selected }).length < this.listItems.length) {
            this.allSelected = false;
        } else if (this.selectedItems.filter(function(e) { return e.selected }).length === this.listItems.length){
            this.allSelected = true;
        }

        this.selectedItemsEmit(this._selectedItems);
    }

    /**
     * Funzione che aggiorna la lista nel caso in cui l'opzione ALL viene selezionata
     */
    selectAllItem() {
        if (!this.allSelected) {
            this._selectedItems = [];
            this.listItems.forEach(obj => {
                obj.selected = false;
            });
        } else {
            this._selectedItems = Object.assign([], this.listItems);
            this.listItems.forEach(obj => {
                obj.selected = true;
            });
        }

        this.selectedItemsEmit(this._selectedItems);
    }

    /**
     * Funzione che viene lanciata dopo che gli elementi della lista vengono selezionati
     */
    selectedItemsEmit(items: any): void {
        this._itemSelected.emit(items);
    }
    /**
     * @ignore
     */
    get itemSelected(): EventEmitter<any> {
        return this._itemSelected;
    }
    /**
     * @ignore
     */
    get selectedItems(): any[] {
        return this._selectedItems;
    }

    /**
     * Funzione che rimuove i badge delle opzioni selezionate
     */
    removeBadge(item) {
        let index = this.selectedItems.findIndex(x => x === item);
        if (index > -1) {
            this.selectedItems.splice(index, 1);
            item.selected = false;
            if (this.selectedItems.filter(function(e) { return e.selected }).length < this.listItems.length) {
                this.allSelected = false;
            }
        }
    }


    /**
     * @ignore
     */
    ngOnChanges(changes: SimpleChanges) {
        console.log(changes);
        if (changes.reset && changes.reset.previousValue) {
            this._selectedItems = [];
            this.listItems.forEach(obj => {
                obj.selected = false;
            });
            this.listOpen = false;
            this.allSelected = false;
            this.selectedItemsEmit(this._selectedItems);
        }
    }
}
