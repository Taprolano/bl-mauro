import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractModalService} from "../abstract-modal/abstract-modal.service";
import {Router} from "@angular/router";
import {LoggerService} from "../../services/logger.service";
import {TranslateService} from "@ngx-translate/core";
import {AbstractListMng} from "../../classes/abstract-list-mng.abstract";
import {ImpUser} from "../../../data-model/class/ImpUser";
import {UtilsService} from "../../services/utils.service";

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent extends AbstractListMng implements OnInit, AfterViewInit  {

    /** Parametri per tab ALL*/

    /**@ignore*/
    // private _genericsUtils =  new Generics<CreditWorkQueue>();
    /**@ignore*/
    // private _reflectFields = this._genericsUtils
    //     .introspectField2Str(
    //         new CreditWorkQueue('','','',
    //             '','',false,0,
    //             0,'',0,0));

    /**@ignore*/
    readonly __widths = ["0%", "0%", "0%", "0%", "0%", "0%", "0%", "0%", "%0"];
    /**@ignore*/
    readonly headers = ImpUser.keys;


    /**@ignore*/
    readonly objectKeys = ImpUser.keys;

    @Output() selected = new EventEmitter();



    selectedUser(row: any) : void { this.selected.emit(row)}


    /**@ignore*/
    readonly sortKeys = ['proposta','tipo di soggetto','arrivo','istruttoria','prodotto','assegnatario','richiedente', 'score', 'esposizione'];
    /**@ignore*/
    readonly __wrapperHeight = "300px";

    /**@ignore*/
    constructor(
        _abstractModalService: AbstractModalService,
        translate: TranslateService,
        _logger: LoggerService,
        private _utils: UtilsService,
        private _router: Router
    ) {
        super(_abstractModalService,translate,_logger);
        // this._logger.logInfo(CreditListAllComponent.name, 'constructor', this._reflectFields);

    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void{
        let elements = document.getElementsByClassName('mCSB_inside');
        if(!this._utils.isVoid(elements)){
            let element = elements.item(0);
            element.classList.remove('mCSB_inside');
            if(element.classList.contains('mCSB_inside')){
                element.classList.remove('mCSB_inside');
            }
        }
    }

    //fornire ordinamento di default?
    // ngOnInit(){
    //     super.ngOnInit();
    //     this.sortEvent.emit('')
    // }



}
