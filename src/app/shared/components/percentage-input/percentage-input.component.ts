import {Component, EventEmitter, forwardRef, Input, OnInit, Output, Renderer2, ViewChild} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ControlValueAccessor} from '../control-value-accessor';

const __defValue = '0,00 %';

@Component({
    selector: 'app-percentage-input',
    templateUrl: './percentage-input.component.html',
    styleUrls: ['./percentage-input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PercentageInputComponent),
            multi: true
        }
    ]
})
export class PercentageInputComponent implements ControlValueAccessor, OnInit {

    @ViewChild('target') innerInput;

    @Input() label = '';

    @Input() decimalDigit = 2;

    @Output()
    blurEvent = new EventEmitter();
    decimalID: string;

    private _digitMoltiplicator: number;
    _percentageValue = null;
    stringValue = '';

    numberToReturn: 1;

    propagateChange = (_: any) => {
    }


    constructor(private translate: TranslateService,
                private _renderer: Renderer2) {
    }

    ngOnInit() {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('it');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this._digitMoltiplicator = Math.pow(10, this.decimalDigit);
    }

    /**
     * Metodo ControlValueAccessor del form, i.e. chiamato da form control per settare il valore del form
     * @param value
     */
    writeValue(value: any) {
        console.log('percentage-inpu value:', value);
        if (value !== undefined) {
            this._percentageValue = value;
        }
        this.createOutputString(value);
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched() {
    }

    /**
     * Ritorna il valore numerico del form
     * @returns {any}
     */
    get percentageValue() {
        return this._percentageValue;
    }

    /**
     * Imposta il valore "output" del form, ovvero letto da eventuali form control
     * @param val
     */
    set percentageValue(val) {
        if (isNaN(val)) { return; }
        this._percentageValue = val;
        this.propagateChange(this._percentageValue);
    }

    /**
     * Processa input per ottenere il valore del campo.
     */
    updateOutput() {
        if (this.stringValue.length > 0) {// console.log("START FROM: " + this.stringValue);
            let value = this.stringValue.replace(/[^\d,]/g, '');
            value = value.replace(/,/g, '.');
            const numValue = +value;
            this.createOutputString(numValue);
            this.percentageValue = numValue;
        } else {
            this.stringValue = __defValue;
            this.percentageValue = 0;
        }
        this.blurEvent.next();
    }


    /**
     * Formatta e assegna la stringa di output del form in base al valore numerico fornito
     * @param numValue
     */
    createOutputString(numValue) {
        if (!isNaN(numValue)) {

            const value = Math.round(numValue * this._digitMoltiplicator) / this._digitMoltiplicator;

            if (value) {
                this.stringValue = value.toFixed(this.decimalDigit).replace(/\./, ',') + ' %';
            }
        } else {
            this.stringValue = !!this.numberToReturn ? this.numberToReturn.toFixed(this.decimalDigit).replace(/\./g, ',') + ' %' : __defValue;
        }

    }

    setDisabledState(isDisabled: boolean) {
        isDisabled ? this._renderer.setAttribute(this.innerInput.nativeElement, 'disabled', String(isDisabled)) :
            this._renderer.removeAttribute(this.innerInput.nativeElement, 'disabled');
    }

}

