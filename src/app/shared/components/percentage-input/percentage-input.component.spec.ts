import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PercentageInputComponent} from './percentage-input.component';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {HttpClient, HttpClientModule} from '../../../../../node_modules/@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader} from '../../../app.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('PercentageInputComponent', () => {
    let component: PercentageInputComponent;
    let fixture: ComponentFixture<PercentageInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PercentageInputComponent],
            imports: [
                MalihuScrollbarModule,
                HttpClientModule,
                FormsModule,
                ReactiveFormsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PercentageInputComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('writeValue test', () => {
        spyOn(component, 'createOutputString').and.returnValue(false);
        component.writeValue('test');
    });

    it('registerOnChange test', () => {
        component.registerOnChange('test');
    });

    it('getpercentageValue test', () => {
        let res = component.percentageValue;
    });

    it('setpercentageValue test', () => {
        component.percentageValue = 'test';
    });

    it('updateOutput test', () => {
        component.stringValue = 'test test';
        spyOn(component, 'createOutputString').and.returnValue(false);
        component.updateOutput();
    });

    it('updateOutput stringNull test', () => {
        component.stringValue = '';
        spyOn(component, 'createOutputString').and.returnValue(false);
        component.updateOutput();
    });

    it('createOutputString test', () => {
        component.createOutputString(0);
    });

    it('createOutputString nan test', () => {
        component.createOutputString('test');
    });

    it('setDisabledState test', () => {
        component.setDisabledState(false);
    });

});
