///<reference path="../../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import { Component, OnInit } from '@angular/core';
import {ISearch} from "../../../data-model/interface/ISearch";
import {AppSettings} from "../../../../AppSettings";
import {IPagination} from "../../../data-model/interface/IPagination";
import {UtilsService} from "../../services/utils.service";
import {RequestManagerConstant} from "../../../proposals/requestManagerConstant";
import {REQUEST_PRIORITY, RequestManagerService} from "../../services/request-manager.service";
import {takeUntil} from "rxjs/operators";
import {AuthConstant} from "../../../../assets/authConstant";
import {LoggerService} from "../../services/logger.service";
import {BeUserService} from "../../services/be-user.service";
import {CreditQueueAllRequestParameters} from "../../../data-model/class/ProposalListAllRequestParameters";
import {Subject} from "rxjs/index";
import {ImpUserRequestParams} from "../../../data-model/class/ImpUserRequestParams";
import {ImpUser} from "../../../data-model/class/ImpUser";
import {PersistenceService} from "../../services/persistence.service";
import {LocationStrategy, PathLocationStrategy} from "@angular/common";

const __categoryNotification = 'MODAL';

const __searchInt = {
    field: null,
    filters: []
};

const __pageUsers: IPagination = {
    keys: null,
    numberOfResults: null,
    recordPerPage: AppSettings.ELEMENT2PAGE,
    page: AppSettings.START2PAGE,
    nPage: 0,
    size: AppSettings.PROPOSALS2PAGE
};

/**
 * Implementa la logica di impersonificazione
 */
@Component({
    selector: 'app-impersonate',
    templateUrl: './impersonate.component.html',
    styleUrls: ['./impersonate.component.scss']
})
export class ImpersonateComponent implements OnInit {

    /** @ignore */
    private ngUnsubscribe = new Subject<void>();
    /** @ignore */
    searchObj: ISearch;
    /** @ignore */
    printList: ImpUser[];
    /** @ignore */
    userList: ImpUser[];
    /** @ignore */
    pageUsers: IPagination;
    /** @ignore */
    sortString;
    /** @ignore */
    httpTest: boolean = true;


    /**
     * Parametri da fornire per la richiesta GET, inclusi filtri
     */
    requestParams: ImpUserRequestParams;
    sem: boolean = false;

    constructor(private _utils: UtilsService,
                private _logger: LoggerService,
                private _requestManagerService: RequestManagerService,
                private _beUserService: BeUserService,
                private _persistenceService: PersistenceService) { }

    /**
     * Inizializza le variabili di ricerca ed i parametri per effettuare la richiesta verso i servizi di backend
     */
    ngOnInit() {
        this.searchObj = __searchInt;
        this.pageUsers = this._utils.assign(__pageUsers);
    }

    /**
     * Metodo che filtra in base all'input ricevuto nella lista di utenti da impersonificare
     */
    search(): void {

        if(this._utils.isVoid(this.searchObj.field)){
            this.getUserList();
        }else{
            this.getUserList();
            this.printList = this.printList.filter((ele) => {
                return ele.username == this.searchObj.field;
            });

        }

    }

    /**
     * Effettua la ricerca degli utenti da impersonare
     */
    getUserList() {
        //spinner e logger
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_IMP_USER_LIST, REQUEST_PRIORITY.HIGH);
        this._logger.logInfo(this.constructor.name, 'getUserList', "Request parameters: ", this.requestParams);
        // set parametri richiesta
        // set parametri richiesta
        let req = new ImpUserRequestParams();
        //sottoscrizione
        this._beUserService.getUser2Impersonate(req, this.httpTest).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    /*
                                        this.userList = !!response.object ? response.object : [];
                                        this.printList = this.printList.concat(this.userList);
                    */
                    this.userList = !!response.object ? response.object.object : [];
                    this._logger.logInfo(this.constructor.name, 'getUserList', "allList: ", this.userList);
                    // console.log("[List][getAllProposalsList] userList: ", this.userList);
                    if (this.pageUsers.page > AppSettings.START2PAGE) {
                        this.printList = this.printList.concat(this.userList);
                    } else {
                        this.printList = this.userList;
                    }

                } else {
                    this._logger.logError(this.constructor.name, 'getUserList', "ERRORE NON IN FALLBACK", response.code);
                }
                //spinner e semaforo
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_IMP_USER_LIST, response.code, __categoryNotification);
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            }, resp => {
                this._logger.logError(this.constructor.name, 'getUserList', resp);
                this.userList = [];
                this.printList = this.userList;
                let error = this._utils.getErrorResponse(resp);
                this._logger.logError(this.constructor.name, 'getUserList', "error", error);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_IMP_USER_LIST, error.code, error.message, __categoryNotification);
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            });
    }


    /**
     * Scarica la nuova pagina dopo lo scroll relativa alla tab attiva
     */
    getNextPage() {
        if (!this.sem) {
            this.sem = true;
            this.pageUsers.page += 1;
            this.getUserList();
        }
    }

    /**
     * Effettua la chiamata di impersonificazione, rimuove i dati associati all'utente che sta impersonificando
     * ed effettua
     */
    selectUser2Imp(row: ImpUser): void {

        this._logger.logInfo(this.constructor.name, 'selectUser2Imp', row);

        //spinner e logger
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_IMP_USER, REQUEST_PRIORITY.HIGH);
        // set parametri richiesta
        let req = new ImpUserRequestParams();
        req.username = row.username;

        this._logger.logInfo(this.constructor.name, 'selectUser2Imp', "Request parameters: ", req);
        // req.setParamsFromFilter(this.filterParamsAll);

        //sottoscrizione
        this._beUserService.impersonateUser(req, false).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {

                    this._logger.logInfo(this.constructor.name, 'selectUser2Imp', response);
                    this._persistenceService.removeAll();
                    // this._location.reload(true);
                    document.location.reload(true);
                } else {
                    this._logger.logError(this.constructor.name, 'selectUser2Imp', "ERRORE NON IN FALLBACK", response.code);
                }
                //spinner e semaforo
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_IMP_USER, response.code, __categoryNotification);
                }, AppSettings.TIMEOUT_SPINNER);
            }, resp => {
                this._logger.logError(this.constructor.name, 'selectUser2Imp', resp);
                let error = this._utils.getErrorResponse(resp);
                this._logger.logError(this.constructor.name, 'selectUser2Imp', "error", error);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_IMP_USER, error.code, error.message, __categoryNotification);
                }, AppSettings.TIMEOUT_SPINNER);
            });




    }


    /**
     *  callback per event emitter del sort della lista degli utenti da impersonare
     */
    onSort(sortString: string) {
        this.sortString = sortString;
        // this.resetPagination();
        this.getUserList();
    }

}
