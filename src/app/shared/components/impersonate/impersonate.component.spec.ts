import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpersonateComponent } from './impersonate.component';
import {AuthGuard} from "../../index";
import {ServiceWorkerModule} from "@angular/service-worker";
import {SharedModule} from "../../shared.module";
import {AppRoutingModule} from "../../../app-routing.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MbzDashboardModule} from "../../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../../proposals/proposals.module";
import {AppComponent} from "../../../app.component";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {CommonModule} from "@angular/common";
import {NgxSpinnerModule} from "ngx-spinner";
import {createTranslateLoader} from "../../../app.module";
import {environment} from "../../../../environments/environment";
import {DirectAccessGuard} from "../../guard/direct-access/direct-access.guard";

describe('ImpersonateComponent', () => {
  let component: ImpersonateComponent;
  let fixture: ComponentFixture<ImpersonateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
            CommonModule,
            BrowserModule,
            BrowserAnimationsModule,
            SharedModule,
            TranslateModule,
            HttpClientModule,
            MbzDashboardModule,
            ProposalsModule,
            NgxSpinnerModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: createTranslateLoader,
                    deps: [HttpClient]
                }
            }),
            AppRoutingModule,
            ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        ],
        declarations: [AppComponent],
        providers: [AuthGuard, DirectAccessGuard]    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpersonateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
