import {AfterViewInit, Component, OnInit, Input} from '@angular/core';
import 'jquery';
import 'bootstrap';
import {NotificationService} from "./notification.service";
import {NOTIFICATION_TYPE, NotificationMessage} from "./NotificationMessage";
import {UtilsService} from "../../services/utils.service";
import {TranslateService} from "@ngx-translate/core";
import {IMessagePool} from "../../../data-model/interface/IMessagePool";
import { LoggerService } from '../../services/logger.service';

declare var $: any;


/**
 * Configurazione componente
 * @type {{ID: string; STYLE: {WARN: string; SUCCESS: string; ERROR: string};
 *         ICON: {WARN: string; SUCCESS: string; ERROR: string}; ID2INJECT: string}}
 */
const CONF_NOTIF = {
    ID: 'main-notifify-wrapper__id',
    STYLE: {
        WARN: 'm-notification  m-notification--alert',
        SUCCESS: 'm-notification  m-notification--success',
        ERROR: 'm-notification  m-notification--error'
    },
    BOX_STYLE: {
        WARN: 'c-box--alert',
        SUCCESS: 'c-box--success',
        ERROR: 'c-box--error'
    },

    ICON: {
        WARN: 'alert',
        SUCCESS: 'success',
        ERROR: 'error'
    },
    ID2INJECT: 'mbz-notify-inject__id'
};

/**
 * Componente che gestisce le notifiche da mostrare all'utente, inclusi messaggi di errore.
 */
@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, AfterViewInit {
    /**stile css */
    styleType: string;

    /**stile css */
    boxStyle: string;

    /** visibilità */
    show: boolean;

    /** messaggio contenuto nella notifica */
    mainMessage: string;

    messages: string[] = [];

    /** messaggio contenuto nella notifica */
    message: string;

    /** id elemento */
    id: string;

    /** icona di notifica */
    icon: string;

    /* parametro che controlla lo show dei messaggi */
    showMore = false;

    /**id sul quale appendere il notification */
    @Input() idToAppend: string = CONF_NOTIF.ID2INJECT;

    /**categoria componente */
    @Input() category: string;

    /**
     *
     * @param {TranslateService} translate
     * @param {NotificationService} _notificationService
     * @param {UtilsService} _utils
     */
    constructor(private translate: TranslateService,
                private _notificationService: NotificationService,
                private _utils: UtilsService,
                private _logger: LoggerService) {
        this.id = CONF_NOTIF.ID;

    }

    /**
     * Attiva sottoscrizione al service e setta traduzione
     */
    ngOnInit() {
        this._logger.logInfo('NotificationComponent','ngOnInit', 'init');
        // console.log('NotificationComponent init');

        this._notificationService.sub.subscribe(messagePool => {
            this._logger.logInfo('NotificationComponent','ngOnInit', 'Recived message pool', messagePool);
            // console.log('[NotificationComponent][subscribe] Recived message pool', messagePool);
            this.cleanAll();
            this._manageMessages(messagePool);

        });
    }

    /**
     * Gestisce un messaggio ricevuto dal service
     * @param {IMessagePool} messagePool
     * @private
     */
    _manageMessages(messagePool: IMessagePool): void {

        if (!this._utils.isVoid(messagePool) && !this._utils.isVoid(messagePool.type)) {
            switch (messagePool.type.toString()) {
                case NOTIFICATION_TYPE.ERROR.toString(): {
                    this.styleType = CONF_NOTIF.STYLE.ERROR;
                    this.icon = CONF_NOTIF.ICON.ERROR;
                    this.boxStyle = CONF_NOTIF.BOX_STYLE.ERROR;
                    break;
                }
                case NOTIFICATION_TYPE.WARN.toString(): {
                    this.styleType = CONF_NOTIF.STYLE.WARN;
                    this.icon = CONF_NOTIF.ICON.WARN;
                    this.boxStyle = CONF_NOTIF.BOX_STYLE.WARN;
                    break;
                }
                case NOTIFICATION_TYPE.SUCCESS.toString(): {
                    this.styleType = CONF_NOTIF.STYLE.SUCCESS;
                    this.icon = CONF_NOTIF.ICON.SUCCESS;
                    this.boxStyle = CONF_NOTIF.BOX_STYLE.SUCCESS;
                    break;
                }
                default:
                    this._logger.logInfo('NotificationComponent','_manageMessages', 'not matched messagePool received m: ' + JSON.stringify(messagePool));
                    // console.log(`[${NotificationComponent.name}][_manageMessage] not matched messagePool received m: ${JSON.stringify(messagePool)}`)
            }

            let msg: string;
            for (let m of messagePool.messages) {
                msg = !!m.beMessage
                    ? this.translate.instant(m.message) + ": " + m.beMessage
                    : this.translate.instant(m.message)+ ".";
                this.messages.push(msg);
            }

            if (this.messageListSize > 1) {
                this.mainMessage = this.translate.instant("MULTIPLE_ERROR_NOTIFICATION");
                this._logger.logInfo('NotificationComponent','_manageMessages', 'mainMessage ' ,this.mainMessage);
                // console.log("[NotificationComponent][menageMessage] mainMessage: ",this.mainMessage);
                this.mainMessage = this.mainMessage.replace("%N%", this.messageListSize.toString());
            } else {
                this.mainMessage = this.messages[0];
            }

            this._logger.logInfo('NotificationComponent','_manageMessages', `${!this._utils.isVoid(messagePool)} && ${!this._utils.isVoid(messagePool.type)} message pool: `,messagePool);
            // console.log(`${!this._utils.isVoid(messagePool)} && ${!this._utils.isVoid(messagePool.type)} message pool: `,messagePool);
            if(!this._utils.isVoid(this.category)) {
                this.show = this.category == messagePool.category;
            }
            else if(this._utils.isVoid(messagePool.category)) {
                this.show = true;
            }

        }
    }


    /**
     * Aggiunge l'area di notifica alla pagina web
     */
    ngAfterViewInit(): void {
        $('#' + this.id).appendTo('#' + this.idToAppend);  
    }

    /**
     * Resetta i campi ai valori di partenza
     */
    cleanAll(){
        this.show = false;
        this.showMore = false;
        this.mainMessage = null;
        this.messages = [];
    }

    /**
     * Mostra l'area di notifica
     */
    hide2show(): void {
        this.show = !this.show;
    }


    get messageListSize(): number {
        return this.messages.length;
    }


}
