import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs/index";
import {NotificationMessage} from "./NotificationMessage";
import {IMessagePool} from "../../../data-model/interface/IMessagePool";
import { LoggerService } from '../../services/logger.service';

/**
 * Servizio per l'invio di notifiche all'utente
 */
@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private _id = Math.floor(Math.random() * 1000) + 1

    /**
     * Componenti sottoscritte al servizio
     * @type {Subject<any>}
     * @private
     */
    private _notifySub = new Subject<any>();

    /**@ignore*/
    constructor(private _logger: LoggerService) { }

    /**
     * Invia una notifica
     * @param {NotificationMessage} message Messaggio di notifica
     */
    sendNotification(message:NotificationMessage): void {
        this._logger.logInfo('NotificationService','sendNotification', 'obj' + message);
        // console.log(`[${NotificationService.name}][sendNotification] obj:`, message);
        let pool: IMessagePool = {type: message.type, messages:[message], size:1,mainMessage: message.message};
        this._notifySub.next(pool);
    }

    /**
     *
     * @param {NotificationMessage[]} messages
     * @param {string} type
     */
    sendNotificationPool(messages: NotificationMessage[], type: string, mainMessage: string, category?: string): void{

        let pool: IMessagePool  = null;
        if(!!messages && messages.length>0){
            pool= {type: type, messages:messages,size: messages.length, mainMessage: mainMessage};
            pool.category = category? category : null;
            this._logger.logInfo('NotificationService','sendNotificationPool',this._id, 'Pool di Messaggi', pool);
            // console.log("[notification-service][sendNotificationPool]["+this._id+"] Pool di Messaggi", pool);

        }

        this._notifySub.next(pool);
    }

    /**@ignore*/
    subscription(): Observable<any>  {
        return this._notifySub.asObservable();
    }

    /**@ignore*/
    get sub(): Observable<any>  {
        return this._notifySub.asObservable();
    }


}
