/**
 * Tipi di notifiche
 */
export enum NOTIFICATION_TYPE {
    ERROR = 'error',
    SUCCESS = 'success',
    WARN ='warn'
}

/**
 * Componente usata per i messaggi di notifica
 */
export class NotificationMessage {

    /**
     * messaggio
     */
    private _message:string;

    /**
     * messaggio del BE
     */
    private _beMessage:string;

    /**
     * tipo
     */
    private _type:string;

    /**
     *
     * @param {string} message  Messaggio notifica
     * @param type  Tipo notifica
     * @param {string} beMessage  Messaggio notifica del BE
     */
    constructor(message: string, type: any, beMessage?: string) {
        this._message = message;
        this._type = type;
        this._beMessage = !!beMessage ? beMessage : null;
    }

    /** */
    get message(): string {
        return this._message;
    }

    /** */

    set message(value: string) {
        this._message = value;
    }

    /** */
    get beMessage(): string {
        return this._beMessage;
    }

    /** */

    set beMessage(value: string) {
        this._beMessage = value;
    }


    /** */
    get type(): string {
        return this._type;
    }

    /** */
    set type(value: string) {
        this._type = value;
    }

}
