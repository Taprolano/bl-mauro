import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoggerService } from '../../services/logger.service';

@Component({
  selector: 'app-message-modal',
  templateUrl: './message-modal.component.html',
  styleUrls: ['./message-modal.component.scss']
})
export class MessageModalComponent implements OnInit {

  constructor() { }

  modalMessage: string ;

  /**
   * Inizializza componente 
   */
  ngOnInit() {
    this.modalMessage = 'MODAL_REDIRECT';

    $("#theModal").on("hidden.bs.modal", function () {
      window.location.href = environment.redirectToNoAuth;
    });
  }

  /**
   * Conferma la navigazione verso l'esterno
   */
  clickConfirmToGoOut(): void {
    window.location.href = environment.redirectToNoAuth;
  }

}
