import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';

/**
 * @ignore
 */
const emptyList_path = "assets/icons/threePoints.svg";
// const emptyList_path = environment.config.basepath + "assets/icons/threePoints";

/**
 * @ignore
 */
@Component({
  selector: 'app-empty-list',
  templateUrl: './empty-list.component.html',
  styleUrls: ['./empty-list.component.scss']
})
export class EmptyListComponent implements OnInit {

  /**
   * @ignore
   */
  constructor() { }

  /**
   * @ignore
   */
  imgPath: string = emptyList_path;

  /**
   * @ignore
   */
  @Input() emptyList: boolean;

  /**
   * @ignore
   */
  ngOnInit() {
  }

}
