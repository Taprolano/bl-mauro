import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { IFilterOption } from "../../../data-model/interface/IFilterOption";
import {FormBuilder, FormGroup} from "@angular/forms";
import {IFilterChoice} from "../../../data-model/interface/IFilterChoice";
import {AppSettings} from "../../../../AppSettings";
import { LoggerService } from '../../services/logger.service';

/**
 * FilterBarComponent: Componente che gestisce i filtri
 */
@Component({
    selector: 'app-filter-bar',
    templateUrl: './filter-bar.component.html',
    styleUrls: ['./filter-bar.component.scss']
})
export class FilterBarComponent implements OnInit {


    @Input()
    list: object[];

    @Input()
    placeholder: string;

    @Input()
    filterKeys: string[];

    @Input()
    filterOption: IFilterOption;

    @Input()
    returningList: object[];

    @Output()
    returningListChange = new EventEmitter();

    filterForm: FormGroup;
    show = false;

    constructor(
        private _formBuilder: FormBuilder,
        private _logger: LoggerService
    ) { }

    ngOnInit() {
        this.returningList = this.list;

        this.filterForm = this._formBuilder.group({
            pattern: this._formBuilder.control("")
        });
    }


    filter(): void{
        let pattern = this.filterForm.value.pattern;

        this._logger.logInfo('FilterBarComponent','filter', this.filterForm.value.pattern);
        // console.log(this.filterForm.value.pattern);
        this.returningList =[];
        for (let item of this.list){
/*let test: string;
test.match("123")*/
            let match = false;
            for (let key of this.filterKeys){
                this._logger.logInfo('FilterBarComponent','filter', item[key].match(pattern));
                // console.log(item[key].match(pattern));
                if ( !!item[key].match(pattern) ){
                    match = true;
                    break;
                }

            }

            let choice = !this.filterOption;
            if(!!this.filterOption){
                choice = !this.selectedFilter.length;
                for (let fc of this.selectedFilter){
                    if(item[this.filterOption.key] === fc.value){
                        choice = true;
                        break
                    }
                }
            }

            this._logger.logInfo('FilterBarComponent','filter', "match: " + match + " choice: " + choice);
            // console.log("match: "+match+" choice: "+choice);
            if(match && choice) this.returningList.push(item);

        }
        this.returningListChange.emit(this.returningList);
    }


    get selectedFilter() : IFilterChoice[] {
        let retList =[];
        if (!!this.filterOption.possibleChoices){
            for (let fc of this.filterOption.possibleChoices){
                if (fc.selected) retList.push(fc);
            }
        }
        return retList;
    }



}
