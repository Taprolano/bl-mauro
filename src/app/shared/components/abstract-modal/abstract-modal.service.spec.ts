import { TestBed } from '@angular/core/testing';

import { AbstractModalService } from './abstract-modal.service';

describe('AbstractModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbstractModalService = TestBed.get(AbstractModalService);
    expect(service).toBeTruthy();
  });
});
