import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LoggerService } from '../../services/logger.service';
import { WorkInProgressComponent } from '../work-in-progress/work-in-progress.component';

/**
 * AbstractModalService: Servizio che incapsula gli eventi che servono al componente AbstractModalComponent
 * per la visualizzazione
 */
@Injectable({
    providedIn: 'root'
})
export class AbstractModalService {

    /**
     * @ignore
     * @type {Subject<any>}
     * @private
     */
    private _guardEvent = new Subject<any>();


    /**
     * @ignore
     * @type {Observable<boolean>}
     */
    guardEventSub = this._guardEvent.asObservable();


    /**
     * @ignore
     * @type {Subject<any>}
     */
    hideModal:Subject<any> = new Subject();

    /**
     * @ignore
     * @type {Subject<any>}
     */
    private unSub:Subject<any> = new Subject();

    closeModal(){this.hideModal.next(true);}

    /**
     * @ignore
     * @type {Subject<any>}
     */
    showModal:Subject<any> = new Subject();

    /*    private errorEmitter = new BehaviorSubject({tipo:'',testo:''});
        ricezioneErrore = this.errorEmitter.asObservable();*/

    /**
     * @ignore
     * @type {BehaviorSubject<any>}
     * @private
     */
    private _modalEventSource = new Subject<any>();//new Subject<any>();

    /**
     * @ignore
     * @type {BehaviorSubject<any>}
     * @private
     */
    public modalEvent = this._modalEventSource.asObservable();

    /**
     * @ignore
     * @clazz {BehaviorSubject<any>}
     * @private
     */
    private _captureOutcomeSub = new Subject<any>();//new Subject<any>();

    /**
     * @ignore
     * @clazz {BehaviorSubject<any>}
     * @private
     */
    public captureOutcome = this._captureOutcomeSub.asObservable();

    /**
     * @ignore
     */
    constructor(private _logger: LoggerService) {
    }
    //
    // sendMessage(message: ModalEvent) {
    //     this._modalEventSource.next(message);
    // }


    get guardEvent(): Subject<any> {
        return this._guardEvent;
    }

    /**
     * Emette l'evento propagando l'oggetto passato come parametro
     * @param obj
     */
    sendOutput(obj: any) {
        // console.log("[abstract-modal-service][sendOutput] obj: ",obj);
        this._modalEventSource.next(obj);
    }

    /**
     *
     * @param onSubmit
     * @param unsubscribe
     * @param component
     * Funzione da richiamare per mostrare la modale a cui passare il componente
     * che si vuole mostrare all'interno
     */
    showModalCallback(onSubmit: boolean, unsubscribe: Subject<any>, component: any) {
        this.modalEvent.pipe(takeUntil(unsubscribe)).subscribe((resp) =>{
            if(!onSubmit) {
                unsubscribe.next();
            }
        });
        this.showModal.next(component);

        /*        openDialog(): Observable<boolean> {
                    const dialogRef = this.dialog.open(DialogWindowComponent, {
                        width: '250px',
                    });

                return dialogRef.afterClosed().pipe(map(result => {
                    // can manipulate result here as needed, if needed
                    // may need to coerce into an actual boolean depending
                    return result;
                }));
            }*/

        /*        return this.guardSubject.pipe(map(result => {
                    // can manipulate result here as needed, if needed
                    // may need to coerce into an actual boolean depending
                    return result;
                }));*/

    }

    /**
     * Apre una modale con la componente in input.
     * @param destroyChild  Subject che indica quando la componente chiamante sta venendo distrutta
     * @param component
     */
    openWithOutcome(destroyChild:Subject<any>,component: any): void {
        this.showModal.next(component);
        this._logger.logInfo(this.constructor.name, 'openWithOutcome', `START`);

        //quando la componente richiedente è in clean-up, distrugge anche la componente modale da essa richiesta.
        destroyChild.asObservable().subscribe(destroy=>{
            this.closeModal();
        })
    }



    get unsubscribe(){
        return this.unSub.asObservable()
    }

    emitUnsubscribe(){
        this.unSub.next(true);
    }

    /**@ignore Apre popup WIP */
    openUnavailableContent(unsubscribe$:Subject<boolean>){
        let customObj = {
            component: WorkInProgressComponent,
            parameters: {title: "", message: ""}
        };
        this.showModalCallback(true,unsubscribe$, customObj);
    }
}
