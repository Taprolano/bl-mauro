import {
    Component,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ComponentRef,
    ComponentFactoryResolver,
    Injector, Input
} from '@angular/core';
import {Subscription} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractModalService} from './abstract-modal.service';
// import * as $ from 'jquery';
import 'jquery';
import 'bootstrap';

/**
 * @ignore
 */
declare var $: any;

/**
 * AbstractModalComponent: Componente che visualizza come modale il contenuto
 * passato come "parametro"
 */
@Component({
    selector: 'app-abstract-modal',
    templateUrl: './abstract-modal.component.html',
    styleUrls: ['./abstract-modal.component.scss']
})
export class AbstractModalComponent implements OnInit {

    /**
     * passato come "parametro"
     */
    @ViewChild('theBody', {read: ViewContainerRef}) theBody;
    cmpRef: ComponentRef<any>;

    @Input()
    props: any;

    /**
     * @ignore
     */
    private _titleEventSubscripion: Subscription;

    /**
     * Costruttore AbstractModalComponent
     * @param {AbstractModalService} _abstractModalService
     * @param {ComponentFactoryResolver} componentFactoryResolver
     * @param {Injector} injector
     * @param {NgbModal} modalService
     */
    constructor(
        private _abstractModalService: AbstractModalService,
        private componentFactoryResolver: ComponentFactoryResolver,
        injector: Injector,
        private modalService: NgbModal) {

        _abstractModalService.showModal.subscribe(type => {

            if (this.cmpRef) {
                this.cmpRef.destroy();
            }
            let factory = this.componentFactoryResolver.resolveComponentFactory(type.component);


            this.cmpRef = this.theBody.createComponent(factory);


            this.cmpRef.instance.modalParameters = {param: type.parameters};

            $('#theModal').modal(this.props).modal('show');

//      console.log("theModal", $('#theModal').modal())

        });

        _abstractModalService.hideModal.subscribe(out => {
                if (!!out) {
                    _abstractModalService.sendOutput(out);
                }
                // out = null;
                $('#theModal').modal('hide');
            }
        );
    }

    /**
     * @ignore
     */
    ngOnInit() {
    }

    /**
     * In fase di chiusura della modale invoca la primitiva di destroy
     */
    close() {
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }
        this.cmpRef = null;
    }

}
