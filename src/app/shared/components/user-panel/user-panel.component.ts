import {Component, OnInit} from '@angular/core';
import {HeaderService} from "../../services/header.service";
import {UserProfileService} from '../../services/user-profile.service';
import {AuthConstant} from 'src/assets/authConstant';
import {IDealershipInfo} from "../../../data-model/interface/IDealershipInfo";
import {AppSettings} from "../../../../AppSettings";
import {IPrincipal} from 'src/app/data-model/interface/IAuthResponse';
import {environment} from 'src/environments/environment';
import {PERSISTENCE_KEY, PersistenceService} from '../../services/persistence.service';
import {LoggerService} from '../../services/logger.service';
import {UtilsService} from "../../services/utils.service";
import {IProfile} from "../../../data-model/interface/IProfile";
import {Authority, IAttributes, IAuthResponse} from '../../../data-model/interface/IAuthResponse';


/**
 * funzionalità di default da associare nel caso non siano associati funzionalità
 * @type {{code: string; description: string; attributes: any[]}}
 * @private
 */
const __iFunctionality = {
    code: '',
    description: '',
    attributes: []
};

/**
 * profilo di default da associare nel caso non siano associati profili
 * @type {{code: string; description: string; app: string; functions}}
 * @private
 */
const __iProfileDefault = {
    "code": '',
    "description": '',
    "app": '',
    "functions": [__iFunctionality]
};


/**
 * Componente che contiene il pannello utente
 */
@Component({
    selector: 'app-user-panel',
    templateUrl: './user-panel.component.html',
    styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {

    /**
     * informazioni sull'utente
     */
    userInfo: IPrincipal; //IAttributes; //IUserInfo;

    /**
     * ruolo dell'utente
     */
    jobRole: string;

    /**@ignore*/
    debugProfileInfo: any;

    /**@ignore*/
    showDebugProfileInfo: boolean;

    /** visibilità corrente del pannello */
    show = false;

    /**@ignore*/
    public locations: IDealershipInfo[] =[] ;

    /**
     * id selezionata
     */
    selectedId: string;

    /**
     * Opzioni per scrollbar
     * @type {{axis: string; theme: string; setHeight: string}}
     */
    scrollbarOptions = { axis: 'y', theme: AppSettings.scrollStyle, setHeight: '30%'};

    /**
     *
     * @param {HeaderService} _headerService
     * @param {UserProfileService} _userProfileService
     * @param {PersistenceService} _persistenceService
     * @param _logger
     * @param _utils
     */
    constructor(public _headerService: HeaderService,
                private _userProfileService: UserProfileService,
                private _persistenceService: PersistenceService,
                private _logger: LoggerService,
                private _utils: UtilsService) {
    }

    /**
     * Richiede attributi e descrizione profilo all'userProfileService
     */
    ngOnInit() {
        this.showDebugProfileInfo = environment.debuggingProfileInfo;
        this._headerService.getVariable().subscribe(() => {
            this.show = !this.show;
            let userHashed = this._persistenceService.get('userProfile');
            let userProfile: IAuthResponse;
            if(!this._utils.isVoid(userHashed)) {
                userProfile = userHashed;
            }
            this.userInfo = userProfile.principal || {
                name: '',
                firstName: '',
                codTer: null,
                bpName: '',
                surname: '',
                email: '',
                phone: '',
                username: '',
                profiles: [],
                authorities: [],
                attributes: null,
            };

            let profile = this.getProfileForApp();
            this.jobRole = (!this._utils.isVoid(profile)) ? profile.description : '';
            //this._userProfileService.setLocalFunctionalities(this._userProfileService.userInfo.profiles[0].functions, false);
            // this.locations = this.userInfo.dealerships;
            // this.selectedId = this.userInfo.defolutDealership;
            if(this.showDebugProfileInfo){
                this.debugProfileInfo = (!this._utils.isVoid(userProfile) && !this._utils.isVoid(userProfile.principal.profiles)
                    && !this._utils.isVoid(profile))? profile : '';
                //TODO: modificare il corrispettivo id quando i profili saranno definitivi
                this.locations = [
                    {name: "Mercedes - CREDITO RETAIL", id: '0'},
                    {name: "Mercedes - CREDITO CORPORATE", id: '1'},
                    {name: "Mercedes - DEALER CONTACT", id: '2'},
                    {name: "Mercedes - PAYOUT", id: '3'}
                ];
            }

            this._logger.logInfo('UserPanelComponent','ngOnInit', "userInfo: ", this.userInfo );
            // console.log("[UserPanelComponent][ngOnInit] userInfo: ", this.userInfo);
        });
    }

    /**
     * 
     */
    getProfileForApp(): IProfile {
        if(!this._utils.isVoid(this.userInfo.profiles)){
            if(this._userProfileService.checkAppProfile(this.userInfo.profiles, AppSettings.AUTH_BL)) {
                for(let profile of this.userInfo.profiles){
                    if(profile.app == AppSettings.AUTH_BL){
                        return profile;
                    }
                }
            }
            else return null;
        }    
        else return null;
    }

    /**
     * Salva id dell'elemento selezionato
     * @param {string} id
     */
    saveId(id: string) {
        this.selectedId = id;
        // this._userProfileService.dealerId = this.selectedId;
        this._userProfileService.loadFunctionalities(this.selectedId, true);
    }

    /**
     * Ritorna dimensioni dell'header
     * @returns {number | number} dimensioni (in pixel)
     */
    get HeaderDimension() {
        let header = document.getElementsByClassName('m-header')[0];
        //let header=document.getElementById('bigHeader');
        let type = header.id;
        return type == 'bigHeader' ? 97 : 62;
        /*let bigHeight= header.style.height.toString();
        console.log(bigHeight);*/
    }

    /**
     * Controlla se l'utente corrente ha il permesso di accedere alle preferenze
     * @param {string} locId id elemento
     * @returns {boolean}
     */
    isAuthorizedToPreferences(locId: string): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.DEALER_PREFERENCE) &&
            locId === this.selectedId;
    }


    /**
     * Cancella il contenuto della sessione
     */
    logout() {
        this._logger.logWarn(this.constructor.name,"logout");
        this._persistenceService.removeAll('logout');
        this._persistenceService.save(PERSISTENCE_KEY.CHECK_INTERCEPTOR, 'force-logout', true);
    }

    /**
     * Restituisce il path presente nell'enviroment
     * @returns {string}
     */
    get logoutPath(){
        return environment.api.logout;
    }

}
