import { Component, Input } from '@angular/core';

/**
 * Wrapper per icone svg
 */
@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent {

  /**
   * stringa che identifica l'icona
   */
  @Input() icon: string;

    /**
     * classi css da assegnare all'icona
     */
  @Input() class: string;

  /**@ignore*/
  constructor() { }

    /**
     * Ritorna le classi css fornite come input del wrapper
     * @returns {string}
     */
  get additionalClasses(): string {
    return this.class != null ? this.class : `icon icon--${this.icon}`;
  }

    /**
     * Ritorna il path dell'icona
     * @returns {string}
     */
  get iconPath(){
    return `assets/icons/svg-defs.svg#${this.icon}`;
  }
}
