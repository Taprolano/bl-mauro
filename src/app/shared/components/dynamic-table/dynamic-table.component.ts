import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IDynamicTableInput} from "../../../data-model/interface/IDynamicTableInput";
import {AbstractModalService} from "../abstract-modal/abstract-modal.service";
import {AppSettings} from "../../../../AppSettings";

/**
 * Tabella creata in base all'input (iniettabile come modale).
 */
@Component({
    selector: 'app-dynamic-table',
    templateUrl: './dynamic-table.component.html',
    styleUrls: ['./dynamic-table.scss']
})
export class DynamicTableComponent implements OnInit {
    /* Inizializzabile sia tramite input che tramite AbstractModalService */

    /**@ignore
     * Parametri componente passati dal costruttore modale
     */
    @Input() modalParameters: any;

    /** Dati input */
    @Input() inputData: IDynamicTableInput; //se se non creata tramite modale

    /** Id elemento */
    @Input() id: string = 'dynamic-scroll-table';

    /**
     * Altezza massima tabella
     * @type {string}
     */
    @Input() maxHeight: string = "400px";

    /**
     * Larghezza massima tabella
     * @type {string}
     */
    @Input() maxWidth: string = "600px";


    /**@ignore per chiamata tramite modale*/
    cssClasses:string[];
    /**Per chiamata tramite modale*/
    modal = false;
    /**@ignore*/
    headers: string[];
    /**@ignore*/
    data: string[][];


    /**@ignore*/
    constructor(private _abstractModalService: AbstractModalService) {}

    /**@ignore*/
    ngOnInit() {
        //spacchetta parametri modale
        if(this.modalParameters && this.modalParameters.param)
            for(let key in this.modalParameters.param)
                this[key] = this.modalParameters.param[key];
        //imposta classi css
        if(this.cssClasses)
            for(let cls of this.cssClasses)
                document.getElementById("dynamic-table-main").classList.add(cls);
        //shortcut
        if(this.inputData){
            this.headers = this.inputData.headers;
            this.data = this.inputData.data;
        }
        //per modale
        if(this.modal){
            //rimuove default padding modale
            document.getElementById('modalBodyId').style.padding='5px';
            //dimensioni modale
            document.getElementById('modalContentId').style.maxWidth=this.maxWidth;
            document.getElementById('modalContentId').style.margin="auto";
        }
        //scrollbar
        $("dynamic-table-scrollable").mCustomScrollbar({
            theme: AppSettings.scrollStyle,
        });
    }

    /**
     * Chiude modale
     */
    closeModal(){
        this._abstractModalService.hideModal.next();
    }

}
