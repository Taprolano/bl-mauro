import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicTableComponent } from './dynamic-table.component';
import {SharedModule} from "../../shared.module";
import {AppComponent} from "../../../app.component";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {BrowserModule} from "@angular/platform-browser";
import {createTranslateLoader} from "../../../app.module";
import {ProposalsModule} from "../../../proposals/proposals.module";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CommonModule} from "@angular/common";
import {NgxSpinnerModule} from "ngx-spinner";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {environment} from "../../../../environments/environment";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {MbzDashboardModule} from "../../../mbz-dashboard/mbz-dashboard.module";
import {AuthGuard} from "../../index";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AppRoutingModule} from "../../../app-routing.module";
import {DirectAccessGuard} from "../../guard/direct-access/direct-access.guard";

xdescribe('DynamicTableComponent', () => {
  let component: DynamicTableComponent;
  let fixture: ComponentFixture<DynamicTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
            CommonModule,
            BrowserModule,
            BrowserAnimationsModule,
            SharedModule,
            TranslateModule,
            HttpClientModule,
            MbzDashboardModule,
            ProposalsModule,
            NgxSpinnerModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: createTranslateLoader,
                    deps: [HttpClient]
                }
            }),
            AppRoutingModule,
            ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        ],
        declarations: [AppComponent],
        providers: [AuthGuard, DirectAccessGuard],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
