import {Component, OnInit, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

const wip_path = "assets/images/mechanical.png";

/**
 * WorkInProgressComponent: Componente dedito alla visualizzazione dei lavori in corso
 */
@Component({
    selector: 'app-work-in-progress',
    templateUrl: './work-in-progress.component.html',
    styleUrls: ['./work-in-progress.component.scss']
})
export class WorkInProgressComponent implements OnInit {

    /**
     * @ignore
     * @type {string}
     */
    imgpath: string = wip_path;

    /**
     * @ignore
     * @type {string}
     */
    @Input() title: string;

    /**
     * @ignore
     * @type {string}
     */
    @Input() message: string;


    /**
     * @ignore
     * @type {string}
     */
    constructor(private translate: TranslateService) {
    }

    /**
     * Inizializza l'immagine ed il messaggio da visualizzare
     */
    ngOnInit() {
        if (!this.title) {
            this.title = this.translate.instant('CONTENT_IN_DEVELOPMENT');
        }
        else this.title = this.translate.instant(this.title);

        if (!this.message) {
            this.message = this.translate.instant('CONTENT_IN_DEVELOPMENT');
        }
        else this.message = this.translate.instant(this.message);

    }

}
