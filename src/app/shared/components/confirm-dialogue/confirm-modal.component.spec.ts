import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDialogue } from './confirm-dialogue.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {AppComponent} from "../../../app.component";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {createTranslateLoader} from "../../../app.module";
import {environment} from "../../../../environments/environment";
import {AuthGuard} from "../../index";
import {BrowserModule} from "@angular/platform-browser";
import {ServiceWorkerModule} from "@angular/service-worker";
import {ProposalsModule} from "../../../proposals/proposals.module";
import {SharedModule} from "../../shared.module";
import {MbzDashboardModule} from "../../../mbz-dashboard/mbz-dashboard.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {DirectAccessGuard} from "../../guard/direct-access/direct-access.guard";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppRoutingModule} from "../../../app-routing.module";

xdescribe('ConfirmModalComponent', () => {
  let component: ConfirmDialogue;
  let fixture: ComponentFixture<ConfirmDialogue>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDialogue);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
