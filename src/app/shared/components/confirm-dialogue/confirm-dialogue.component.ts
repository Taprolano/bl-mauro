import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {ConfirmResult} from "../../../data-model/enum/EConfirmResult";
import {AbstractModalService} from "../abstract-modal/abstract-modal.service";

@Component({
    selector: 'app-confirm-modal',
    templateUrl: './confirm-modal.component.html',
    styleUrls: ['./confirm-modal.component.scss']
})
/**
 * Componente per richiede conferma all'esecuzione di azioni. Invia output tramite servizio modali.
 */
export class ConfirmDialogue implements OnInit {

    /**@ignore
     * Parametri componente passati dal costruttore modale
     */
    @Input() modalParameters: any;

    /**
     * Messaggio mostrato dalla modale
     * @type {string}
     */
    message='';
    /**@ignore classi css assegnate alla modale*/
    cssClasses:string[]=[];

    /**
     *
     * @param {TranslateService} translate
     * @param modalService
     */
    constructor(
        private translate:TranslateService,
        private modalService:AbstractModalService
    ) { }

    /** Inizializza modale */
    ngOnInit() {
        this.cssClasses= this.modalParameters.param.cssClasses.length>0? this.modalParameters.param.cssClasses : ['default'];
        this.message = this.modalParameters.param.message? this.modalParameters.param.message : '';
    }

    /** Propaga l'evento di conferma */
    confirm(){this.modalService.sendOutput(ConfirmResult.CONFIRM);}

    /** Propoaga evento di annulamento */
    undo(){this.modalService.sendOutput(ConfirmResult.UNDO);}

}
