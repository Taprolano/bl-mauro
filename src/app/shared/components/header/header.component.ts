import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {HeaderService} from '../../services/header.service';
import {ProposalsRoutingConstant} from "../../../proposals/proposalsRoutingConstant";
import {AppRoutingConstant} from "../../../appRoutingConstat";
import {ShadowService} from "../../services/shadow.service";
import {UtilsService} from "../../services/utils.service";
import {UserProfileService} from "../../services/user-profile.service";
import {AuthConstant} from "../../../../assets/authConstant";
import {ProposalWorkflowService} from "../../../proposals/services/proposal-workflow.service";
import {WorkInProgressComponent} from 'src/app/shared/components/work-in-progress/work-in-progress.component';
import {AbstractModalService} from 'src/app/shared/components/abstract-modal/abstract-modal.service';
import {Observable, Subject} from 'rxjs';
import {NotificationService} from '../notification/notification.service';
import {REQUEST_PRIORITY, RequestManagerService} from "../../services/request-manager.service";
import {RequestManagerConstant} from "../../../proposals/requestManagerConstant";
import {LoggerService} from '../../services/logger.service';
import {BeWorkQueue} from '../../../proposals/services/be-work-queue.service';
import {NumberProposalHeader} from 'src/app/data-model/class/NumberProposalHeader';
import {ImpersonateComponent} from "../impersonate/impersonate.component";
import {IAuthResponse} from '../../../data-model/interface/IAuthResponse';
import {PersistenceService} from '../../services/persistence.service';
import {environment} from "../../../../environments/environment";

/**
 * @ignore
 * @type {string[]}
 * @private
 */
const __listRouter = [ProposalsRoutingConstant.fullPath('list'),
    ProposalsRoutingConstant.fullPath('workQueueCreditRetail'),
    // ProposalsRoutingConstant.fullPath('workQueueDealerContact'),
    ProposalsRoutingConstant.fullPath('workQueuePayout'),

    ProposalsRoutingConstant.fullPath('workQueue'), AppRoutingConstant.fullPath('home')];
// const __headerLabel2Links = {
//     'Proposals':1,
//     'Refundings':2,
//     'Reworks':3,
//     'MyMobilityPass':4
// };

/**
 * Labels dell'header
 */
export enum __headerLabel2Links {
    Home = "Home",
    Statistics = "Statistics",
    Tools = "Tools",
    UserManual = "UserManual",
    WorkQueue = "WorkQueue"
}

/**
 * Labels dell'header che compare al mouse over su 'strumenti'
 */
export enum __headerLabelTools2Links {
    Subjects = "Subjects",
    ConsultProps = "ConsultProps",
    CreditScreen = "CreditScreen",
    Cas = "Cas"
}

export enum __headerLabelQueue2Links {
    Retail = "Retail",
    Corporate = "Corporate"
}

/**
 * Header del portale web
 */
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    /** */
    pushRightClass: string;
    /** */
    headerRecap: NumberProposalHeader;

    // notifications: number;

    /** link alla pagina home */
    homeLink = ProposalsRoutingConstant.fullPath(ProposalsRoutingConstant.path.workQueue);
    /** */
    unsubscribe = new Subject<any>();
    /** */
    label2link = __headerLabel2Links;

    /** */
    labelToolsTolink = __headerLabelTools2Links;

    /** */
    labelQueueToLink = __headerLabelQueue2Links;

    //public isBig = false;
    /** */
    cHeaderLink: string;

    /** */
    onOverTools: boolean;

    /** */
    onOverQueue: boolean;

    /** */
    toolSubLinkClicked: string;

    /** */
    queueSubLinkClicked: string;

    /** */
    isEditing: boolean = false;

    /** */
    private _id = Math.ceil(Math.random() * 1000);

    isImpersonating: boolean;

    /**
     *
     * @param {Router} router
     * @param {HeaderService} headerService
     * @param _beWorkQueue
     * @param {Router} _router
     * @param {ShadowService} _shadowService
     * @param _requestManagerService
     * @param {UtilsService} _utils
     * @param {UserProfileService} _userProfileService
     * @param {ProposalWorkflowService} _proposalWfService
     * @param {AbstractModalService} _abstractModalService
     * @param _notificationService
     * @param _logger
     * @param _persistenceService
     */
    constructor(public router: Router,
                private headerService: HeaderService,
                private _beWorkQueue: BeWorkQueue,
                private _router: Router,
                private _shadowService: ShadowService,
                private _requestManagerService: RequestManagerService,
                private _utils: UtilsService,
                private _userProfileService: UserProfileService,
                private _proposalWfService: ProposalWorkflowService,
                private _abstractModalService: AbstractModalService,
                private _notificationService: NotificationService,
                private _logger: LoggerService,
                private _persistenceService: PersistenceService,
    ) {

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    /**
     * Impostazioni inizializzazione
     */
    ngOnInit() {
        //this.headerService.getVariable().subscribe(state => this.isBig = state )
        let fromMock: boolean = true;
        this.isImpersonating = false;
        this.pushRightClass = 'push-right';
        // this.notifications = 8;
        this.cHeaderLink = this.label2link.Home;
        this.onOverTools = false;
        this.onOverQueue = false;
        this.getHeaderData(fromMock);
        this.checkImpersonate();
    }

    /**
     * Controlla se si sta impersonificando un altro profilo
     */
    checkImpersonate(): void {
        let userProfile = this._persistenceService.get('userProfile');
        if (!this._utils.isVoid(userProfile)) {
            for (let auth of userProfile.authorities) {
                if (!this._utils.isVoid(auth.source)) {
                    this.isImpersonating = true;
                }
            }
        }
    }

    /**
     * @ignore
     */
    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    /**
     * @ignore
     */
    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    /**
     * Naviga alla home
     */
    goToFromLogo() {
        this._router.navigate([this.homeLink])
    }

    /*rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }*/

    /**
     * Prende dati dall'header service
     * @param {boolean} fromMock
     */
    getHeaderData(fromMock: boolean) {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_COUNTER_VALUE, REQUEST_PRIORITY.LOW);
        let tipoCoda: string;
        if (this._router.url.indexOf(ProposalsRoutingConstant.fullPath('workQueueCreditRetail')) > -1) {
            tipoCoda = AuthConstant.CODA_CREDITO_RETAIL;
        } else if (this._router.url.indexOf(ProposalsRoutingConstant.fullPath('workQueueCreditCorporate')) > -1) {
            tipoCoda = AuthConstant.CODA_CREDITO_CORPORATE;
        } else if (this._router.url.indexOf(ProposalsRoutingConstant.fullPath('workQueuePayout')) > -1) {
            tipoCoda = AuthConstant.CODA_PAYOUT;
        } else if (this._router.url.indexOf(ProposalsRoutingConstant.fullPath('workQueueDealerContact')) > -1) {
            tipoCoda = AuthConstant.CODA_DEALER_CONTACT;
        }

        this._beWorkQueue.getNumbersProposalHeader(tipoCoda).subscribe(data => {
                // this.headerRecap = data;

                this._logger.logInfo('HeaderComponent', 'getHeaderData', data);
                // console.log('getHeaderData', data);
                this.headerRecap = data.object;
                this._requestManagerService.handleRequest(RequestManagerConstant.GET_COUNTER_VALUE, data.code)

            },
            resp => {
                this.headerRecap = new NumberProposalHeader(null,null,null,null,null,null,null,null);
                let error = this._utils.getErrorResponse(resp);
                this._logger.logInfo('HeaderComponent', 'getHeaderData', error, this._id);
                // console.error(`[${HeaderComponent.name}][getHeaderData][${this._id}]:`, error);
//                this._notificationService.sendNotification(ProposalsMessages.VERSION_REQUEST_ERROR);
                this._requestManagerService.handleRequest(RequestManagerConstant.GET_COUNTER_VALUE, error.code, error.message)
            });
    }


    /** Naviga al link cliccato */
    go2(section: string) {
        this.onOverTools = false;
        this.onOverQueue = false;
        this.toolSubLinkClicked = '';
        this.queueSubLinkClicked = '';
        this.cHeaderLink = section;
        if (section == this.label2link.Home) {
            this._proposalWfService.exitToList();
        }
        //TODO add navigation
        else {
            this.openUnavailableContent();
        }
    }

    /**Click sulla voce del menù strumenti: mostra il sottomenù strumenti e
     *  disabilita l'altro sottomenù delle code*/
    clickOnToolsLink() {
        this.onOverQueue = false;
        this.cHeaderLink = this.label2link.Tools;
        this.onOverTools = !this.onOverTools;
    }

    /**Naviga al link cliccato tra gli strumenti */
    goToTool(section: string) {
        this.toolSubLinkClicked = section;
        //TODO add navigation
        if(section == this.labelToolsTolink.ConsultProps) {
            this._router.navigate([ProposalsRoutingConstant.fullPath('workQueuePropsHistory')]);
        }
        else this.openUnavailableContent();
    }

    /**Click sulla voce del menù code: mostra il sottomenù code e
     *  disabilita l'altro sottomenù degli strumenti*/
    clickOnQueueLink(): void {
        this.onOverTools = false;
        this.cHeaderLink = this.label2link.WorkQueue;
        this.onOverQueue = !this.onOverQueue;

    }

    /**Naviga sulla view relativa alla coda richiesta */
    goToQueue(section: string) {
        this.queueSubLinkClicked = section;
        this.onOverQueue = false;
        switch (section) {
            case __headerLabelQueue2Links.Corporate: {
                this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueCreditCorporate')]);
                break;
            }
            case __headerLabelQueue2Links.Retail: {
                this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueCreditRetail')]);
                break;
            }
        }
    }

    /**
     * Decide dimensioni header a seconda dell'URL
     * @returns {boolean}
     */
    get headerCondition(): boolean {
        let url = this._router.url;
        //console.log("[header] url: "+ url +" condition; "+ (__listRouter.indexOf(url) >= 0));
        return __listRouter.indexOf(url) >= 0;
    }

    /** Apre User Panel e attiva shadow */
    variableToUserAndLayout() {
        this.headerService.changeUserPanel();
        // this._shadowService.updateShadow();
    }

    /** Ritorna la data */
    get currentDateString(): string {
        let currentDate = new Date();
        return this._utils.monthsList[currentDate.getMonth()].toLocaleLowerCase() + " " + currentDate.getFullYear();
    }

    /** Controlla se l'utente ha il permesso di accedere alla funzionalità PROP_RIL_CONF_COMMERCIALE */
    get rilAuth(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_RIL_CONF_COMMERCIALE)
    }

    /** Controlla se l'utente ha il permesso di alla funzionalità PROP_RIF_CONF_COMMERCIALE */
    get rifAuth(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_RIF_CONF_COMMERCIALE)
    }

    /**
     * Avvia il processo per l'impersonificazione
     */
    openImpersonateContent(): void {
        let customObj = {
            component: ImpersonateComponent,
            parameters: {title: "", message: ""}
        };

        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
    }

    /**@ignore*/
    openUnavailableContent(): void {
        let customObj = {
            component: WorkInProgressComponent,
            parameters: {title: "", message: ""}
        };

        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
    }

    /**
     * Funzione per determinare se il dealer è autorizzato al mobility pass
     */
    isAuthForMobilityPass(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_NOLEGGIO);
    }


    /**
     * Controlla se l'utente corrente ha il permesso per accedere alle funzionalità per la visualizzazione
     * @returns {boolean}
     */
    hasCorporateAndRetailFunc(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.CODA_CREDITO_RETAIL)
            && this._userProfileService.haveAbilitation(AuthConstant.CODA_CREDITO_CORPORATE);
    }

    /**
     * Controlla se l'utente corrente ha il permesso per accedere alla funzionalità di impersonificazione
     * @returns {boolean}
     */
    isAuth2Impersonate(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.FUNC_IMPERSONATE) && !environment.production;
    }

    /**
     * Controlla se l'utente corrente ha il permesso per accedere alla funzionalità di consulta storico proposte
     * @returns {boolean}
     */
    isAuth2ConsultProposals(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.FUNC_IMPERSONATE);
    }

    /**
     * Switcha in modalita' editing
     */
    toggleEditConsultation(): void {
        this.isEditing = !this.isEditing;
        //TODO: aggiungere servizio per controllo lock
        this._proposalWfService.toggleEditingMode(this.isEditing);
    }

    /**
     * controlla se ci si trova all'interno della rotta di consultazione della proposta
     * per visualizzare il sub-header con la possibilità di editare la proposta
     */
    isInsideConsultation(): boolean {
        return this._router.url.indexOf(ProposalsRoutingConstant.path.workQueueConsultation) > -1;
    }

    /**
     * Controlla se il counter esiste
     */

    checkIfExist(item): boolean {
        return typeof (item) !== "undefined";
    }
}
