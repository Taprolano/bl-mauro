import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs/index";
import {UserProfileService} from "../services/user-profile.service";
import {IPrincipal} from "../../data-model/interface/IAuthResponse";
import {BLAuthGuard} from "./bl-auth/bl-auth.guard";
import {LoggerService} from "../services/logger.service";
import {MessageModalComponent} from "../components/message-modal/message-modal.component";
import {AbstractModalService} from "../components/abstract-modal/abstract-modal.service";
import {ProposalWorkflowService} from "../../proposals/services/proposal-workflow.service";
import {UtilsService} from "../services/utils.service";
import {PersistenceService} from "../services/persistence.service";
import {AppSettings} from "../../../AppSettings";
import {AuthConstant} from "../../../assets/authConstant";
import {ProposalsRoutingConstant} from "../../proposals/proposalsRoutingConstant";


@Injectable()
export class QueueDispatcherGuard implements CanActivate {


    constructor(private _userProfileService: UserProfileService,
                private _proposalWorkflowService: ProposalWorkflowService,
                private _persistenceService: PersistenceService,
                private _utilsService: UtilsService,
                private _abstractModalService: AbstractModalService,
                private _logger: LoggerService,
                private _router: Router
    ) {
    }


    unsubscribe = new Subject<any>();

    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        //TODO: inserire tutte le rotte per code lavoro

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_CREDITO_RETAIL)) {
            this._logger.logInfo(QueueDispatcherGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueueCreditRetail'));
            this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueCreditRetail')]);
            return true;

        }

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_CREDITO_CORPORATE)) {
            this._logger.logInfo(QueueDispatcherGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueueCreditCorporate'));
            this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueCreditCorporate')]);
            return true;

        }

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_DEALER_CONTACT)) {
            this._logger.logInfo(QueueDispatcherGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueueDealerContact'));
            this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueDealerContact')]);
            return true;

        }

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_PAYOUT)) {
            this._logger.logInfo(QueueDispatcherGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueuePayout'));
            this._router.navigate([ProposalsRoutingConstant.fullPath('workQueuePayout')]);
            return true;

        }

        this._logger.logDebug(QueueDispatcherGuard.name, 'canActivate', 'redirectToExternal');
        this.redirectToExternal();
        return false;

/*        if (this._utilsService.isVoid(this.userProfile)) {
            return false
        }
        else {
            this.app = this.userProfile.profiles[0].app;
            this._logger.logDebug(QueueDispatcherGuard.name, 'canActivate', this.app);
            this._userProfileService.setLocalFunctionalities(this.userProfile.profiles[0].functions, false);
            if (this.app == AppSettings.AUTH_BL) {
                return true;
            }
            else {
                this._logger.logDebug(QueueDispatcherGuard.name, 'canActivate', 'redirectToExternal');
                this.redirectToExternal();
                return false;
            }
        }*/

    }

    /**
     * Funzione per dirottare l'utente verso l'esterno se non abilitato a navigare l'applicativo
     */
    redirectToExternal(): void {
        this._logger.logDebug(QueueDispatcherGuard.name, 'redirectToExternal', 'START');
        let customObj = {
            component: MessageModalComponent,
            parameters: {modalMessage: ""}
        };

        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);

    }

}
