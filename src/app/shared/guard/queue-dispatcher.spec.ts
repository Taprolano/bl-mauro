import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { QueueDispatcherGuard } from "./queue-dispatcher";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from '../../app.module';
import { SharedModule } from '../shared.module';
import {CookieService} from "ngx-cookie-service";
import {AbstractModalComponent} from "../components/abstract-modal/abstract-modal.component";
import {UserPanelComponent} from "../components/user-panel/user-panel.component";
import {ScrolTestComponent} from "../example/components/scrol-test/scrol-test.component";
import {EmptyListComponent} from "../components/empty-list/empty-list.component";
import {ImpersonateComponent} from "../components/impersonate/impersonate.component";
import {WorkInProgressComponent} from "../components/work-in-progress/work-in-progress.component";
import {SharedPipesModule} from "../pipes/shared-pipes.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DynamicTableComponent} from "../components/dynamic-table/dynamic-table.component";
import {OptionalListComponent} from "../../proposals/components/optional-list/optional-list.component";
import {PersistenceService} from "../services/persistence.service";
import {CommonModule} from "@angular/common";
import {NotificationComponent} from "../components/notification/notification.component";
import {SvgIconComponent} from "../components/svg-icon/svg-icon.component";
import {ThirdPartOptionalUpdateComponent} from "../../proposals/components/third-part-optional-update/third-part-optional-update.component";
import {MessageModalComponent} from "../components/message-modal/message-modal.component";
import {MalihuScrollbarModule} from "ngx-malihu-scrollbar";
import {FilterBarComponent} from "../components/filter-bar/filter-bar.component";
import {ListUsersComponent} from "../components/list-users/list-users.component";
import {AuthGuard} from "../index";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AppRoutingModule} from "../../app-routing.module";
import {MbzDashboardModule} from "../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../proposals/proposals.module";
import {AppComponent} from "../../app.component";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgxSpinnerModule} from "ngx-spinner";
import {environment} from "../../../environments/environment";
import {DirectAccessGuard} from "./direct-access/direct-access.guard";


describe('QueueDispatcherGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
        });
    });

    it('should ...', inject([QueueDispatcherGuard], (guard: QueueDispatcherGuard) => {
        expect(guard).toBeTruthy();
    }));
});
