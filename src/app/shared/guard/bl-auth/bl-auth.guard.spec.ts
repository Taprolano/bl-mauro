import { TestBed, async, inject } from '@angular/core/testing';

import {BLAuthGuard} from './bl-auth.guard';

xdescribe('BLAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BLAuthGuard]
    });
  });

  it('should ...', inject([BLAuthGuard], (guard: BLAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
