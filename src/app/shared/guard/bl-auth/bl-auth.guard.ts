import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanActivate} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {ProposalWorkflowService} from 'src/app/proposals/services/proposal-workflow.service';
import {IPrincipal} from 'src/app/data-model/interface/IAuthResponse';
import {AppSettings} from 'src/AppSettings';
import {takeUntil} from "rxjs/internal/operators";
import {UserProfileService} from "../../services/user-profile.service";
import {PersistenceService} from "../../services/persistence.service";
import {UtilsService} from "../../services/utils.service";
import {AbstractModalService} from "../../components/abstract-modal/abstract-modal.service";
import {LoggerService} from "../../services/logger.service";
import {MessageModalComponent} from "../../components/message-modal/message-modal.component";
import {IAuthResponse} from "../../../data-model/interface/IAuthResponse";


@Injectable({
    providedIn: 'root'
})
/**
 * Verifica l'autorizzazione applicativa, abilitando l'utente alle funzionalità applicative
 */
export class BLAuthGuard implements CanActivate {

    constructor(private _userProfileService: UserProfileService,
                private _proposalWorkflowService: ProposalWorkflowService,
                private _persistenceService: PersistenceService,
                private _utilsService: UtilsService,
                private _abstractModalService: AbstractModalService,
                private _logger: LoggerService) {
    }

    private userProfile: IAuthResponse;
    private isAuth: boolean;
    unsubscribe = new Subject<any>();

    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        let userHashed = this._persistenceService.get('userProfile');
        
        this._logger.logDebug(BLAuthGuard.name, 'canActivate', userHashed);
        this.isAuth = false;
        if (this._utilsService.isVoid(userHashed)) {
            this.retrieveProfile();

        }
        else {
            this.userProfile = userHashed;
            this.isAuth = this._userProfileService.checkAppProfile(this.userProfile.principal.profiles, AppSettings.AUTH_BL);
            this._logger.logDebug(BLAuthGuard.name, 'canActivate', this.isAuth);
            this._userProfileService.setFunctionalities2Profile(this.userProfile.principal.profiles, AppSettings.AUTH_BL, false);
            //gestisce la funzionalità di impersonate
            this._userProfileService.updateImpersonateFunction(this.userProfile.authorities);


            if (this.isAuth) {
                return true;
            }
            else {
                this._logger.logDebug(BLAuthGuard.name, 'canActivate', 'redirectToExternal');
                this.redirectToExternal();
                return false;
            }
        }

    }

    /**
     * Funzione per effettuare l'autenticazione e stabilire se si è abilitati a navigare nell'applicativo
     */
    retrieveProfile() {

        this._logger.logDebug(BLAuthGuard.name, 'retrieveProfile', 'START');

        this._userProfileService.getUserProfile(false).subscribe( resp => {
                this._logger.logDebug(BLAuthGuard.name, 'retrieveProfile', 'inside subscribe');

                this.userProfile = resp.object;
                this._logger.logDebug(BLAuthGuard.name, 'retrieveProfile', this.userProfile);

                this.isAuth = this._userProfileService.checkAppProfile(this.userProfile.principal.profiles, AppSettings.AUTH_BL);
                this._logger.logDebug(BLAuthGuard.name, 'retrieveProfile', this.isAuth);

                this._userProfileService.setFunctionalities2Profile(this.userProfile.principal.profiles, AppSettings.AUTH_BL, false);
                //gestisce la funzionalità di impersonate
                this._userProfileService.updateImpersonateFunction(this.userProfile.authorities);

                if (this.isAuth) {
                    this._persistenceService.save('userProfile', this.userProfile);
                    this._proposalWorkflowService.exitToList();
                    return true;
                }
                else {
                    this._logger.logDebug(BLAuthGuard.name, 'retrieveProfile', 'redirectToExternal');
                    this.redirectToExternal();
                    return false;
                }
            },
            error => {
                console.log("[AuthGuard][retrieveProfile] error", error);
                this._logger.logError(BLAuthGuard.name, 'retrieveProfile', error);

                //TODO cambiare path a oauth2
                this.redirectToExternal();
            });

    }

    /**
     * Funzione per dirottare l'utente verso l'esterno se non abilitato a navigare l'applicativo
     */
    redirectToExternal(): void {
        this._logger.logDebug(BLAuthGuard.name, 'redirectToExternal', 'START');
        let customObj = {
            component: MessageModalComponent,
            parameters: {modalMessage: ""}
        };

        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);

    }

}
