import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs/index";
import {UserProfileService} from "../services/user-profile.service";
import {IPrincipal} from "../../data-model/interface/IAuthResponse";
import {BLAuthGuard} from "./bl-auth/bl-auth.guard";
import {LoggerService} from "../services/logger.service";
import {MessageModalComponent} from "../components/message-modal/message-modal.component";
import {AbstractModalService} from "../components/abstract-modal/abstract-modal.service";
import {ProposalWorkflowService} from "../../proposals/services/proposal-workflow.service";
import {UtilsService} from "../services/utils.service";
import {PersistenceService} from "../services/persistence.service";
import {AppSettings} from "../../../AppSettings";
import {AuthConstant} from "../../../assets/authConstant";
import {ProposalsRoutingConstant} from "../../proposals/proposalsRoutingConstant";


@Injectable()
export class CheckQueueGuard implements CanActivate {


    constructor(private _userProfileService: UserProfileService,
                private _proposalWorkflowService: ProposalWorkflowService,
                private _persistenceService: PersistenceService,
                private _utilsService: UtilsService,
                private _abstractModalService: AbstractModalService,
                private _logger: LoggerService,
                private _router: Router
    ) {
    }


    unsubscribe = new Subject<any>();

    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        this._logger.logInfo(CheckQueueGuard.name,'canActivate', "previous url:", this._router.url);
        // console.log("previous url:", this.router.url);
        this._logger.logInfo(CheckQueueGuard.name,'canActivate', "next url:", state.url);

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_CREDITO_RETAIL)
            && state.url.includes(ProposalsRoutingConstant.fullPath('workQueueCreditRetail'))) {
            this._logger.logInfo(CheckQueueGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueueCreditRetail'));
            return true;
        }

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_CREDITO_CORPORATE)
            && state.url.includes(ProposalsRoutingConstant.fullPath('workQueueCreditCorporate'))) {
            this._logger.logInfo(CheckQueueGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueueCreditCorporate'));
            return true;
        }

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_PAYOUT)
            && state.url.includes(ProposalsRoutingConstant.fullPath('workQueuePayout'))) {
            this._logger.logInfo(CheckQueueGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueuePayout'));
            return true;
        }

        if(this._userProfileService.haveAbilitation(AuthConstant.CODA_DEALER_CONTACT)
            && state.url.includes(ProposalsRoutingConstant.fullPath('workQueueDealerContact'))) {
            this._logger.logInfo(CheckQueueGuard.name,
                'canActivate',
                ProposalsRoutingConstant.fullPath('workQueueDealerContact'));
            return true;
        }

        this._logger.logDebug(CheckQueueGuard.name, 'canActivate', 'redirectToExternal');
        this._router.navigate([ProposalsRoutingConstant.basePath()]);

        return false;

/*        if (this._utilsService.isVoid(this.userProfile)) {
            return false
        }
        else {
            this.app = this.userProfile.profiles[0].app;
            this._logger.logDebug(QueueDispatcherGuard.name, 'canActivate', this.app);
            this._userProfileService.setLocalFunctionalities(this.userProfile.profiles[0].functions, false);
            if (this.app == AppSettings.AUTH_BL) {
                return true;
            }
            else {
                this._logger.logDebug(QueueDispatcherGuard.name, 'canActivate', 'redirectToExternal');
                this.redirectToExternal();
                return false;
            }
        }*/

    }

    /**
     * Funzione per dirottare l'utente verso l'esterno se non abilitato a navigare l'applicativo
     */
    redirectToExternal(): void {
        this._logger.logDebug(CheckQueueGuard.name, 'redirectToExternal', 'START');
        let customObj = {
            component: MessageModalComponent,
            parameters: {modalMessage: ""}
        };

        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);

    }

}
