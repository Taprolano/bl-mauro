import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { QueueDispatcherGuard } from "./queue-dispatcher";
import {CheckQueueGuard} from "./check-queue";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { createTranslateLoader } from '../../app.module';
import { SharedModule } from '../shared.module';
import {AuthGuard} from "../index";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AppRoutingModule} from "../../app-routing.module";
import {MbzDashboardModule} from "../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../proposals/proposals.module";
import {AppComponent} from "../../app.component";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CommonModule} from "@angular/common";
import {NgxSpinnerModule} from "ngx-spinner";
import {environment} from "../../../environments/environment";
import {DirectAccessGuard} from "./direct-access/direct-access.guard";


describe('CheckQueueGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard]
        });
    });

    it('should ...', inject([QueueDispatcherGuard], (guard: QueueDispatcherGuard) => {
        expect(guard).toBeTruthy();
    }));
});
