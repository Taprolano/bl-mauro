import { Injectable } from '@angular/core';
import {
    CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanDeactivate,
    NavigationStart, NavigationEnd
} from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ProposalWorkflowService } from 'src/app/proposals/services/proposal-workflow.service';
import { LoggerService } from '../../services/logger.service';
import {ConfirmDialogue} from "../../components/confirm-dialogue/confirm-dialogue.component";
import {ConfirmResult} from "../../../data-model/enum/EConfirmResult";
import {AbstractModalService} from "../../components/abstract-modal/abstract-modal.service";
import {BehaviorSubject, ReplaySubject, Subject} from "rxjs/index";
import {ProposalsRoutingConstant} from "../../../proposals/proposalsRoutingConstant";
import {AppComponent} from "../../../app.component";
import {AbstractModalComponent} from "../../components/abstract-modal/abstract-modal.component";
import {VersionComponent} from "../../../proposals/proposal/product/version/version.component";
import {takeUntil} from "rxjs/internal/operators";
import {UtilsService} from "../../services/utils.service";

/**
 * Guardia che inibisce l'utilizzo dell'uri per pilotare il routing dell'applicativo
 */
@Injectable({
    providedIn: 'root'
})
export class WarnExitGuard implements CanActivate, CanDeactivate<AppComponent> {


    /**
     * Identificativo utile per verificare che la guardia sia definita come singleton
     * @type {number}
     * @private
     */
    private _id = Math.floor(Math.random() * 1000) + 1;

    /**
     * Utilizzata per legare tutte le desottoscrizioni al ciclo di vita della guardia
     * @type {Subject<any>}
     */
    private guardUnsub$: Subject<any>;
    private guard: BehaviorSubject<any>;


    /**
     * Incapsula l'evento di popState utilizzato per gestire la modifica dell'uri
     */
    private _imperativeStateEvent: BehaviorSubject<boolean>;

    /**
     * @ignore
     */
    private _popStateObservable: Observable<boolean>;

    /**
     * Evento che cattura le operazioni effettuate sulla modale
     */
    private _modalEvent: BehaviorSubject<boolean>;

    /**
     * @ignore
     */
    private _modalObservable: Observable<boolean>;

    /**
     * Mantiene lo stato di inizializazione dei vari subscribers presenti all'interno della guardia
     * @type {boolean}
     * @private
     */
    private _ready = false;

    private _nextState?: RouterStateSnapshot;


    /**
     *
     * @param {ProposalWorkflowService} _workflowService
     * @param {AbstractModalService} _abstractModalService
     * @param {LoggerService} _logger
     * @param _utils
     * @param {Router} _router
     */
    constructor(private _workflowService: ProposalWorkflowService,
                private _abstractModalService: AbstractModalService,
                private _logger: LoggerService,
                private _utils: UtilsService,
                private _router: Router) {



    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if(!this._ready) {
            this._logger.logInfo(this.constructor.name+this._id, 'canActivate', 'init', this._ready);
            this.init();
        }

        return true;

    }


    /**
     * Restituscire il valore  innescato dall'evento di popState e gestito attraverso il servizio
     * associato alla modale
     * @param {AppComponent} component
     * @param {ActivatedRouteSnapshot} currentRoute
     * @param {RouterStateSnapshot} currentState
     * @param {RouterStateSnapshot} nextState
     * @returns {Observable<boolean> | Promise<boolean> | boolean}
     */
    canDeactivate(component: AppComponent, currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        this._logger.logInfo(this.constructor.name+this._id, 'canDeactivate', 'nextState', nextState);

        let skipManualFlow: boolean = this.verify2PropFlow(nextState);
        if (skipManualFlow) {
            // this.throwEvent(this.guard, true);
            this.destroy();
            return skipManualFlow;
        } else {
            if(this._modalEvent && !this._modalEvent.value)
                this.addressAppFlow();
        }

        this._nextState = nextState;


        /*        if(!this._ready) {
                    this._logger.logInfo(this.constructor.name+this._id, 'canDeactivate', 'init', this._ready);
                    this.init();
                }*/

        /*        this._router.events.pipe(takeUntil(this.guardUnsub$)).subscribe((event) => {
                    if(event instanceof NavigationStart) {

                        if (event.navigationTrigger === 'imperative') {*/



        /*                } else {

                            this._modalEvent.next(true);

                        }
                    }
                });*/


        return this._modalEvent;
    }


    init(): void {
        this._nextState = null;
        this._logger.logInfo(this.constructor.name+this._id, 'init', "INIT OF NEW GUARD");
        this._ready = true;
        this.guardUnsub$ = new Subject<any>();
        this.guard = new BehaviorSubject<any>(null);

        this._modalEvent = new BehaviorSubject<boolean>(null);
        this._modalObservable = this._modalEvent.asObservable();


        try {
            this.subscribeModalService();
            // this.subscribeImperativeState();
            this.subscribe2GuardFlow();

            this._ready = true;
        } catch(error) {
            this._logger.logError(this.constructor.name+this._id, 'init', error);
        }

        /*        this._modalObservable.pipe(takeUntil(this.guardUnsub$)).subscribe((value) => {
                    this._logger.logInfo(this.constructor.name + this._id, 'canDeactivate', `result`, value);
                    // this._modalEvent.next(value);
        /!*            if(value) {
                        this.destroy();
                    }*!/
                });*/

    }

    subscribe2GuardFlow(): void {
        this.guard.asObservable().pipe(takeUntil(this.guardUnsub$)).subscribe((value)=> {

            if(value!=null) {
                this._logger.logInfo(this.constructor.name+this._id, 'subscribe2GuardFlow', `result`, value);

                this.throwEvent(this._modalEvent, value);
                this._abstractModalService.closeModal();

                // this._modalEvent.next(value);
                if(value){
                    this._logger.logWarn(this.constructor.name+this._id, 'subscribe2GuardFlow', `exitToList`);
                    setTimeout(() => {
                        if(this._nextState) {
                            this._router.navigate([this._nextState.url]);
                            this.destroy();
                        }
                    },250);
                }
            }

        });
    }


    /*    subscribeImperativeState(): void {
            this._router.events.pipe(takeUntil(this.guardUnsub$)).subscribe((evt) => {
                if(evt instanceof NavigationStart) {

                    if (evt.navigationTrigger === 'imperative') {



                        this._logger.logInfo(this.constructor.name+this._id,
                            'subscribeImperativeState', 'imperative');
                        // this._modalEvent.next(true);

                    } else if (evt.navigationTrigger === 'popstate' || evt.navigationTrigger === 'hashchange') {
                        this.throwEvent(this.guard, true);
                    }

                }
            });
        }*/



    verify2PropFlow(nextState: any): boolean {


        if (!this._utils.isVoid(nextState) && !this._utils.isVoid(nextState.url)) {
            this._logger.logInfo(this.constructor.name+this._id, 'canDeactivate', 'nextState.url', nextState);
            let outcome = this._workflowService.getStateFromPath(nextState.url);
            this._logger.logInfo(this.constructor.name+this._id, 'canDeactivate', 'nextState.url', nextState, 'outcome', outcome);

            return !!outcome;
        }
    }


    /**
     * Sottoscrizione al servizio della modale ed ottenimento dell'esito
     */
    subscribeModalService(): void {

        //gestione output modale
        this._abstractModalService.guardEventSub.pipe(takeUntil(this.guardUnsub$)).subscribe(
            (result) => {

                this._logger.logInfo(this.constructor.name+this._id, 'subscribeModalService',
                    `inside modalEvent: ${result}  `);

                if (result) {
                    if (result == ConfirmResult.CONFIRM) {
                        this.guard.next(true);

                    }


                    if (result == ConfirmResult.UNDO) {
                        this.guard.next(false);

                    }

                    this._abstractModalService.closeModal();


                } else {
                    this._logger.logDebug(this.constructor.name+this._id, 'subscribeModalService','flow non gestito');
                    this._abstractModalService.closeModal();

                }
            },
            err => {

                this._logger.logError(this.constructor.name+this._id, 'preparePrevetion',
                    `Errore in output modale`);
                this._abstractModalService.closeModal();

            }
        );
    }


    /**
     * Inizializza il componente da mostrare all'interno della modale e restitusce l'observable
     * su cui sottoscriversi per ottenere le informazioni selezionate dall'utente
     * @returns {Observable<boolean>}
     */
    addressAppFlow(): void{

        this._logger.logInfo(this.constructor.name+this._id, 'addressAppFlow', 'START');

        //campi modale
        let id = "confirm-message-exit";
        let cssClasses = [];
        let message = 'GUARD_CONFIRM_EXIT';
        let guard = true;


        //crea modale
        let popupComponent = {
            component: ConfirmDialogue,
            parameters: {id, cssClasses, message, guard}
        };

        this._abstractModalService.openWithOutcome(this.guardUnsub$, popupComponent);
    }


    /**
     * Distrugge il subject associato al ciclo di vita della guardia ed in cascata di tutte
     * le sottoscrizioni effettuate
     */
    destroy(): void {
        this.guardUnsub$.next();
        this.guardUnsub$.complete();
        this._ready = false;
    }

    throwEvent(evt:Subject<boolean>, value:boolean): void {

        /*        setTimeout(() => {
                    if(evt instanceof Subject) {*/
        evt.next(value);
        /*            }
                },100);*/
    }




}
