import { TestBed, async, inject } from '@angular/core/testing';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MbzDashboardModule } from 'src/app/mbz-dashboard/mbz-dashboard.module';
import { ProposalsModule } from 'src/app/proposals/proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { createTranslateLoader } from 'src/app/app.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { AppComponent } from 'src/app/app.component';
import { AuthGuard } from '../../index';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {WarnExitGuard} from "./warn-exit.guard";
import {DirectAccessGuard} from "../direct-access/direct-access.guard";

describe('WarnExitGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        TranslateModule,
        HttpClientModule,
        MbzDashboardModule,
        ProposalsModule,
        NgxSpinnerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, DirectAccessGuard, WarnExitGuard],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
  });

  it('should ...', inject([WarnExitGuard], (guard: WarnExitGuard) => {
    expect(guard).toBeTruthy();
  }));
});
