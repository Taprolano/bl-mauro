///<reference path="../../components/abstract-modal/abstract-modal.service.ts"/>
import { Injectable } from '@angular/core';
import {
    CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanDeactivate,
    NavigationStart, NavigationEnd
} from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ProposalWorkflowService } from 'src/app/proposals/services/proposal-workflow.service';
import { LoggerService } from '../../services/logger.service';
import {ConfirmDialogue} from "../../components/confirm-dialogue/confirm-dialogue.component";
import {ConfirmResult} from "../../../data-model/enum/EConfirmResult";
import {AbstractModalService} from "../../components/abstract-modal/abstract-modal.service";
import {BehaviorSubject, ReplaySubject, Subject} from "rxjs/index";
import {ProposalsRoutingConstant} from "../../../proposals/proposalsRoutingConstant";
import {AppComponent} from "../../../app.component";
import {AbstractModalComponent} from "../../components/abstract-modal/abstract-modal.component";
import {VersionComponent} from "../../../proposals/proposal/product/version/version.component";
import {takeUntil} from "rxjs/internal/operators";


/**
 * Interfaccia definita per tutti quei componenti che posso disattivare la guardia
 */
export interface CanComponentDeactivate {
    canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

/**
 * Guardia che inibisce l'utilizzo dell'uri per pilotare il routing dell'applicativo
 */
@Injectable({
    providedIn: 'root'
})
export class DirectAccessGuard implements CanActivate, CanDeactivate<AppComponent> {

    /**
     * Identificativo utile per verificare che la guardia sia definita come singleton
     * @type {number}
     * @private
     */
    private _id = Math.floor(Math.random() * 1000) + 1;

    /**
     * Utilizzata per legare tutte le desottoscrizioni al ciclo di vita della guardia
     * @type {Subject<any>}
     */
    private guardUnsub$: Subject<any>;

    private guard: ReplaySubject<any>;


    /**
     * Incapsula l'evento di popState utilizzato per gestire la modifica dell'uri
     */
    private _popStateEvent: BehaviorSubject<boolean>;

    /**
     * @ignore
     */
    private _popStateObservable: Observable<boolean>;

    /**
     * Evento che cattura le operazioni effettuate sulla modale
     */
    private _modalEvent: ReplaySubject<boolean>;

    /**
     * @ignore
     */
    private _modalObservable: Observable<boolean>;

    /**
     * Mantiene lo stato di inizializazione dei vari subscribers presenti all'interno della guardia
     * @type {boolean}
     * @private
     */
    private _ready = false;


    /**
     *
     * @param {ProposalWorkflowService} _workflowService
     * @param {AbstractModalService} _abstractModalService
     * @param {LoggerService} _logger
     * @param {Router} _router
     */
    constructor(private _workflowService: ProposalWorkflowService,
                private _abstractModalService: AbstractModalService,
                private _logger: LoggerService,
                private _router: Router) {

    }

    /**
     * Metodo richiamato prima di attivare il routing verso il componente richiesto.
     * L'unico obiettivo è quello di inizializzare lo stato affiche si gestisca nel canDeactivate il flusso di verifica
     * @param {ActivatedRouteSnapshot} next
     * @param {RouterStateSnapshot} state
     * @returns {Observable<boolean> | Promise<boolean> | boolean}
     */
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean {
        this._logger.logDebug(this.constructor.name+this._id,'canActivate', "previous url:", this._router.url);
        this._logger.logDebug(this.constructor.name+this._id,'canActivate', "CURRENT STATE:", window.history.state);

        if(!this._ready)
            this.init();
        return true;

    }

    /**
     * Restituscire il valore  innescato dall'evento di popState e gestito attraverso il servizio
     * associato alla modale
     * @param {AppComponent} component
     * @param {ActivatedRouteSnapshot} currentRoute
     * @param {RouterStateSnapshot} currentState
     * @param {RouterStateSnapshot} nextState
     * @returns {Observable<boolean> | Promise<boolean> | boolean}
     */
    canDeactivate(component: AppComponent, currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        return this._modalEvent;
    }


    /**
     * Inizializza lo stato
     */
    init(): void {
        this._logger.logInfo(this.constructor.name+this._id, 'init', "INIT OF NEW GUARD");
        this.guardUnsub$ = new Subject<any>();
        this.guard = new ReplaySubject<any>(0);
        this._popStateEvent = new BehaviorSubject<boolean>(null);
        this._popStateObservable = this._popStateEvent.asObservable();

        this._modalEvent = new ReplaySubject<boolean>(1);
        this._modalObservable = this._popStateEvent.asObservable();


        try {
            this.subscribe2GuardFlow();
            this.subscribeRouterPopState();
            this.subscribeModalService();
            this.triggerConsensusOrDefaultRouting();

            this._ready = true;
        } catch(error) {
            this._logger.logError(this.constructor.name+this._id, 'init', error);
        }
    }

    /**
     * Esegue la sottoscrizione agli eventi di routing applicativi e incapsula nel popStateEvent i tipi eventi da
     * gestire applicativamente. Per quest'ultimi ci si sottoscrive agli eventi innescati dal servizio della modale
     */
    subscribeRouterPopState(): void {

        this._router.events.pipe(takeUntil(this.guardUnsub$)).subscribe((evt) => {
            if(evt instanceof NavigationStart) {
                this._logger.logInfo(this.constructor.name+this._id, 'subscribeRouterPopState', evt.navigationTrigger);

                if ((evt.navigationTrigger === 'popstate') || (evt.navigationTrigger === 'hashchange')) {

                    this._logger.logInfo(this.constructor.name+this._id,
                        'subscribeRouterPopState', 'popstate');

                    this._popStateEvent.next(true);

                } else {

                    this._popStateEvent.next(false);
                    // this._extModal.next(true);

                }

            }
        });

        /*        this._router.events.pipe(takeUntil(this.guardUnsub$)).subscribe((evt) => {
                    if(evt instanceof NavigationEnd) {
                        this._logger.logInfo(this.constructor.name+this._id, 'subscribeRouterPopState', 'NavigationEnd', event);
                        // this.destroy();

                    }
                });*/
    }


    /**
     * Gestisce l'evento di popState ritornando true nel caso non si tratti di popstate oppure hashchange.
     * In caso contrario si innesca il flusso manuale gestito attraverso la modale
     */
    triggerConsensusOrDefaultRouting(): void {
        this._popStateObservable.pipe(takeUntil(this.guardUnsub$)).subscribe((popEvent) => {

            if(popEvent!=null) {

                if(!popEvent) {
                    // this._modalEvent.next(true);
                    this.throwEvent(this._modalEvent, true);
//                    this.destroy();
                    return;
                }

                // let result = this.addressAppFlow();
                // this._logger.logInfo(this.constructor.name+this._id, 'triggerConsensusOrDefaultRouting', result);

                this.addressAppFlow();
                this.throwEvent(this._modalEvent, false);
                // this._modalEvent.next(false);
            }
        });
    }

    subscribe2GuardFlow(): void {
        this.guard.asObservable().pipe(takeUntil(this.guardUnsub$)).subscribe((value)=> {

                if(value!=null) {
                    this._logger.logInfo(this.constructor.name + this._id, 'subscribe2GuardFlow', `result`, value);
                    this._modalEvent.next(value);

                    setTimeout(() => {

                        if (value) {
                            this._logger.logWarn(this.constructor.name + this._id, 'subscribe2GuardFlow', `exitToList`);
                            this._workflowService.exitToList();
                            this.destroy();
                        }
                    }, 250);
                }
            }
        );
    }

    /**
     * Sottoscrizione al servizio della modale ed ottenimento dell'esito
     */
    subscribeModalService(): void {

        //gestione output modale
        this._abstractModalService.guardEventSub.pipe(takeUntil(this.guardUnsub$)).subscribe(
            (result) => {

                this._logger.logInfo(this.constructor.name+this._id, 'subscribeModalService',
                    `inside modalEvent: ${result}  `);

                if (result) {
                    if (result == ConfirmResult.CONFIRM) {
                        this.guard.next(true);

                    }

                    if (result == ConfirmResult.UNDO) {
                        this.guard.next(false);

                    }

                    this._abstractModalService.closeModal();

                } else {
                    this._logger.logDebug(this.constructor.name, 'subscribeModalService','flow non gestito');
                    this._abstractModalService.closeModal();

                }
            },
            err => {

                this._logger.logError(this.constructor.name+this._id, 'subscribeModalService',
                    `Errore in output modale`);
                this._abstractModalService.closeModal();

            }
        );
    }


    /**
     * Inizializza il componente da mostrare all'interno della modale e restitusce l'observable
     * su cui sottoscriversi per ottenere le informazioni selezionate dall'utente
     * @returns {Observable<boolean>}
     */
    addressAppFlow(): void {

        this._logger.logInfo(this.constructor.name+this._id, 'addressAppFlow', 'START');

        //campi modale
        let id = "confirm-message";
        let cssClasses = [];
        let message = 'GUARD_CONFIRM_REDIRECT';
        let guard = true;

        //crea modale
        let popupComponent = {
            component: ConfirmDialogue,
            parameters: {id, cssClasses, message, guard}
        };


        this._abstractModalService.openWithOutcome(this.guardUnsub$, popupComponent);
    }

    addressAppFlowConfirm(): boolean {
        return confirm(`sei sicuro SI NO`);
    }

    /**
     * Distrugge il subject associato al ciclo di vita della guardia ed in cascata di tutte
     * le sottoscrizioni effettuate
     */
    destroy(): void {
        /*
                setTimeout(() => {
        */
        this.guardUnsub$.next();
        this.guardUnsub$.complete();
        this._ready = false;
        /*        },150);*/
    }

    throwEvent(evt:Subject<boolean>, value:boolean): void {

        /*        setTimeout(() => {
                    if(event instanceof Subject) {*/
        evt.next(value);
        /*            }
                },100);*/
    }



}
