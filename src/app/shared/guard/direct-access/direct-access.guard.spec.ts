import { TestBed, async, inject } from '@angular/core/testing';

import { DirectAccessGuard } from './direct-access.guard';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MbzDashboardModule } from 'src/app/mbz-dashboard/mbz-dashboard.module';
import { ProposalsModule } from 'src/app/proposals/proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { createTranslateLoader } from 'src/app/app.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { AppComponent } from 'src/app/app.component';
import { AuthGuard } from '../../index';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('DirectAccessGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        TranslateModule,
        HttpClientModule,
        MbzDashboardModule,
        ProposalsModule,
        NgxSpinnerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, DirectAccessGuard],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
  });

  it('should ...', inject([DirectAccessGuard], (guard: DirectAccessGuard) => {
    expect(guard).toBeTruthy();
  }));
});
