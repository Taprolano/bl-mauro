import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BeResponse } from '../../data-model/class/Response';
import { ListValuesReqParams } from '../../data-model/class/ListValuesReqParams';
import { ListValues, ListValuesFactory } from 'src/app/data-model/class/ListValues';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class BeSelectBoxService {

  constructor(private _http: HttpClient, private _logger: LoggerService) { }

  /**
   * @param params
   * API che restituisce le liste di valori richieste, per popolare le selectbox
   */
  getSelectListOfValues(params: ListValuesReqParams): Observable<BeResponse<ListValues>> {
    return this._http.get(environment.api.listOfValues, {observe: 'response', params: params.requestParameters()})
    .pipe(map((resp: HttpResponse<any>) => {
      
      let factory = new ListValuesFactory();
      this._logger.logInfo(BeSelectBoxService.name, 'getSelectListOfValues', resp.body)
      let rawlist: any = resp.body.body;

      return new BeResponse<any>(resp.status, null, factory.getInstance(rawlist));
    }));
  }
}
