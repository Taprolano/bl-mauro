import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  constructor() { }

  /**
   *
   * @param className
   * @param methodName
   * @param optionalDataToLog
   * Metodo per loggare a livello di log INFO
   */
  logInfo(className: string, methodName: string, ...args: any[]): void {
    let callbackLog = console.info;
    this.logData(callbackLog, className, methodName, args);
  }

  /**
   *
   * @param className
   * @param methodName
   * @param optionalDataToLog
   * Metodo per loggare a livello di log DEBUG
   */
  logDebug(className: string, methodName: string, ...args: any[]): void {
    let callbackLog = console.debug;
    this.logData(callbackLog, className, methodName, args);
  }

  /**
   *
   * @param className
   * @param methodName
   * @param optionalDataToLog
   * Metodo per loggare a livello di log WARN
   */
  logWarn(className: string, methodName: string, ...args: any[]): void {
    let callbackLog = console.warn;
    this.logData(callbackLog, className, methodName, args);
  }

  /**
   *
   * @param className
   * @param methodName
   * @param optionalDataToLog
   * Metodo per loggare a livello di log ERROR
   */
  logError(className: string, methodName: string, ...args: any[]): void {
    let callbackLog = console.error;
    this.logData(callbackLog, className, methodName, args);
  }

  /**
   * @param callbackLog
   * @param className
   * @param methodName
   * @param optionalDataToLog
   * Metodo per loggare informazioni di differente tipo di log a seconda della callbackLog passata
   */
  private logData(callbackLog, className: string, methodName: string, ...args: any[]): void {
    if(environment.loggerServiceActive) {
      let message: string = `[Classe: ` + className + `] - [Metodo: ` + methodName + `]`;
      message += !this.isConsistent(args) ? ` - Dato loggato: ` : ``
      callbackLog(message, ...args);
    }
  }

  /**
   *
   * @param input
   * Metodo che controlla che il dato sia consistente
   */
  private isConsistent(input) {
    return input==='undefined' || input===undefined || input===null || input==='null' || input==='';
  }
}
