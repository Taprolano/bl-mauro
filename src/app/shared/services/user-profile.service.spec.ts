import {TestBed, inject} from '@angular/core/testing';

import {UserProfileService} from './user-profile.service';
import {MockDataService} from "./mock-data.service";
import {AuthConstant} from "../../../assets/authConstant";
import {SharedModule} from "../shared.module";
import {ProposalsModule} from "../../proposals/proposals.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";


const __userAuthorities = [
    {
        code: AuthConstant.DEALER_PREFERENCE,
        description: "DEALER_PREFERENCE",
        attributes: ['abilitaEstrattoConto=SI'] //['abilitaEstrattoConto=NO']
    },
    {
        code: AuthConstant.PROP_CONSULTAZIONE,
        description: "PROP_CONSULTAZIONE",
        attributes: ['scope=PERSONALI']
    },
    {
        code: AuthConstant.PROP_CONF_COMMERCIALE,
        description: "PROP_CONF_COMMERCIALE",
        attributes: ['abilitaFornitoreEsterno=SI', 'abilitaAllestimentoTerzeParti=NO'] //['abilitaFornitoreEsterno=SI', 'abilitaAllestimentoTerzeParti=SI'] ['abilitaFornitoreEsterno=NO', 'abilitaAllestimentoTerzeParti=SI'] ['abilitaFornitoreEsterno=NO', 'abilitaAllestimentoTerzeParti=NO']
    },
    {
        code: AuthConstant.PROP_CONF_FINANZIARIA,
        description: "PROP_CONF_FINANZIARIA",
        attributes: []
    },
    {
        code: AuthConstant.PROP_CONF_FIN_DEROGA_SERVIZI,
        description: "PROP_CONF_FIN_DEROGA_SERVIZI",
        attributes: []
    },
    {
        code: AuthConstant.PROP_CONF_FIN_DEROGA_COMMERCIALE,
        description: "PROP_CONF_FIN_DEROGA_COMMERCIALE",
        attributes: []
    },
    {
        code: AuthConstant.PROP_CONF_FIN_PROVVIGIONI,
        description: "PROP_CONF_FIN_PROVVIGIONI",
        attributes: []
    },
    {
        code: AuthConstant.PROP_ANAGRAFICA,
        description: "PROP_ANAGRAFICA",
        attributes: ['abilitaOmocodia=SI'] //['abilitaOmocodia=NO']
    },
    {
        code: AuthConstant.PROP_ATTIVAZIONE,
        description: "PROP_ATTIVAZIONE",
        attributes: ['abilitaAttivazione=SI'] //['abilitaAttivazione=NO']
    },
    {
        code: AuthConstant.PROP_RIL_CONF_COMMERCIALE,
        description: "PROP_RIL_CONF_COMMERCIALE",
        attributes: []
    },
    {
        code: AuthConstant.PROP_NOLEGGIO,
        description: "PROP_NOLEGGIO",
        attributes: []
    },
    {
        code: AuthConstant.PROP_NOTE_GESTIONE,
        description: "PROP_NOTE_GESTIONE",
        attributes: []
    }
];

describe('UserProfileService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            SharedModule,
            ProposalsModule,
            HttpClientModule
        ],
        providers: [
            UserProfileService,
            HttpClient
        ]
    }));

    it('should be created', () => {
        const service: UserProfileService = TestBed.get(UserProfileService);
        expect(service).toBeTruthy();
    });


    it('should correctly crate the map', inject(
        [UserProfileService], (ups) => {


/*            let epectedMap = AuthConstant.FUNCTIONALITY_STARTER_MAP;
            epectedMap.set(AuthConstant.DEALER_PREFERENCE, true);
            epectedMap.set(AuthConstant.DEALER_PREFERENCE_ESTRATTO_CONTO, true);
            epectedMap.set(AuthConstant.PROP_CONSULTAZIONE, true);
            epectedMap.set(AuthConstant.PROP_CONSULTAZIONE_PERSONALI, true);
            epectedMap.set(AuthConstant.PROP_CONF_FINANZIARIA, true);
            epectedMap.set(AuthConstant.PROP_CONF_FIN_DEROGA_SERVIZI, true);
            epectedMap.set(AuthConstant.PROP_CONF_FIN_DEROGA_COMMERCIALE, true);
            epectedMap.set(AuthConstant.PROP_ANAGRAFICA, true);
            epectedMap.set(AuthConstant.PROP_ANAGRAFICA_OMOCODIA, true);
            epectedMap.set(AuthConstant.PROP_ATTIVAZIONE, true);
            epectedMap.set(AuthConstant.PROP_RIL_CONF_COMMERCIALE, true);
            epectedMap.set(AuthConstant.PROP_NOLEGGIO, true);
            epectedMap.set(AuthConstant.PROP_NOTE_GESTIONE, true);
            epectedMap.set(AuthConstant.PROP_CONF_COMMERCIALE, true);
            epectedMap.set(AuthConstant.PROP_CONF_COMMERCIALE_TERZE_PARTI, true);

            expect(ups.formatLocalFunctionalities(__userAuthorities )
            ).toEqual(__userAuthorities);*/

        })
    );

    it('should correctly check te auotorization', inject(
        [UserProfileService], (ups) => {


/*           ups.setLocalFunctionalities(__userAuthorities);

            expect(ups.haveAbilitation( AuthConstant.DEALER_PREFERENCE )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.DEALER_PREFERENCE_ESTRATTO_CONTO )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_CONSULTAZIONE_PERSONALI )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_CONF_FINANZIARIA )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_CONF_FIN_DEROGA_SERVIZI )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_CONF_FIN_DEROGA_COMMERCIALE )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_ANAGRAFICA )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_ANAGRAFICA_OMOCODIA )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_ATTIVAZIONE )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_RIL_CONF_COMMERCIALE )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_NOLEGGIO )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_NOTE_GESTIONE )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_CONF_COMMERCIALE )
            ).toBeTruthy();
            expect(ups.haveAbilitation( AuthConstant.PROP_CONF_COMMERCIALE_TERZE_PARTI )
            ).toBeTruthy();*/
        })
    );
});
