import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BeResponse } from 'src/app/data-model/class/Response';
import { IDealer } from 'src/app/data-model/interface/IDealer';
import { MockDataService } from './mock-data.service';
import { HttpClient } from '@angular/common/http';
import { IDealership } from 'src/app/data-model/interface/IDealership';

@Injectable({
  providedIn: 'root'
})
export class DealerService {

  constructor(private mockDataService: MockDataService, private http: HttpClient) { }

  getDealerInfo(mock = false): Observable<BeResponse<IDealer>> {
    if(mock) {
      return this.mockDataService.getDealerInfo();
    }
    else {
      //BE service
    }
  }

  getDealershipDetails(mock = false, dealer: string, dealership: string): 
    Observable<BeResponse<IDealership>> {
    
    if(mock) {
      return this.mockDataService.getDealershipDetails();
    }
    else {
      //BE service
    }
  }
}
