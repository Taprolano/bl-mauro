import { TestBed } from '@angular/core/testing';

import { NewProposalPresenceService } from './new-proposal-presence.service';

describe('NewProposalPresenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewProposalPresenceService = TestBed.get(NewProposalPresenceService);
    expect(service).toBeTruthy();
  });
});
