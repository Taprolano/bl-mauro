import { Injectable } from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {UtilsService} from "./utils.service";
import { LoggerService } from './logger.service';

const __appPath = '/proposals/';

export enum PERSISTENCE_KEY {
    USER_PROFILE = 'userProfile',
    CHECK_INTERCEPTOR = 'checkInterceptor'
}

@Injectable({
    providedIn: 'root'
})
/**
 * Servizio che si occupa della permanenza dei dati.
 */
export class PersistenceService {




    constructor(private _cookieService: CookieService,
                private _utilService: UtilsService,
                private _logger: LoggerService) { }


    save(key: string, object: any, lstorage:boolean = false) {

        let jObj = JSON.stringify(object);
        this._logger.logInfo('PersistenceService','save', jObj);
        let hashObj = this._utilService.hash(jObj);
        this._logger.logInfo('PersistenceService','save','hashObj', jObj);


        if(!this._utilService.isVoid(sessionStorage) && !lstorage)
            sessionStorage.setItem(key, hashObj);
        else if(!this._utilService.isVoid(localStorage) && lstorage){
            localStorage.setItem(key, hashObj);
        }
        this._cookieService.set(key, hashObj, null, __appPath);

        this._logger.logInfo('PersistenceService','save', '_cookieService.getByKey', this._cookieService.get(key));
        // console.log('[PersistenceService][save] _cookieService.getByKey', this._cookieService.get(key));
    }

    remove(key:string, lstorage:boolean = false) {
        this._logger.logWarn(this.constructor.name, 'remove', key);
        if(!this._utilService.isVoid(sessionStorage) && !lstorage)
            sessionStorage.removeItem(key);
        else if(!this._utilService.isVoid(localStorage) && lstorage)
            localStorage.removeItem(key);

        this._cookieService.delete(key, __appPath);
    }

    get(key:string, lstorage:boolean = false):any {

        let obj;

        try {
            if(!this._utilService.isVoid(sessionStorage) && !lstorage
                && !this._utilService.isVoid(sessionStorage.getItem(key))){
                obj = sessionStorage.getItem(key);
                this._logger.logInfo('PersistenceService','get', 'return from sessionStorage', obj);
                // console.log('[PersistenceService][get] return from localstorage', obj);

            } else if(!this._utilService.isVoid(localStorage)  && lstorage
                && !this._utilService.isVoid(localStorage.getItem(key))) {
                obj = localStorage.getItem(key);
                this._logger.logInfo('PersistenceService','get','return from localStorage', obj);
                // console.log('[PersistenceService][get] return from _cookieService', obj);


            } else if(!this._utilService.isVoid(this._cookieService)
                && !this._utilService.isVoid(this._cookieService.get(key))) {
                obj = this._cookieService.get(key);
                this._logger.logInfo('PersistenceService','get','return from _cookieService', obj);
                // console.log('[PersistenceService][get] return from _cookieService', obj);

            }
            obj = JSON.parse(this._utilService.unhash(obj));

        }catch(error) {
            this._logger.logWarn('PersistenceService','get', error);
            //  console.warn('[PersistenceService][get]:', error);
            obj = undefined;
        }
        return obj;
    }

    removeAll(caller:string = 'default') {
        this._logger.logWarn(this.constructor.name, 'removeAll', caller);
        if(!this._utilService.isVoid(sessionStorage))
            sessionStorage.clear();
        this._cookieService.deleteAll();
    }

    dumpAllMemory() {
        this._logger.logInfo('PersistenceService','dumpMemory','_cookieService: ' + this._cookieService.getAll());
        // console.log(`[${PersistenceService.name}][dumpMemory][_cookieService: ${this._cookieService.getAll()}]`);

        this._logger.logInfo('PersistenceService','dumpMemory','sessionStorage: ' + sessionStorage);
        // console.log(`[${PersistenceService.name}][dumpMemory][sessionStorage: ${sessionStorage}]`);

    }

    dumpMemoryByKey(key: string) {
        this._logger.logInfo('PersistenceService','dumpMemory','_cookieService: ' + this._cookieService.get(key));
        // console.log(`[${PersistenceService.name}][dumpMemory][_cookieService: ${this._cookieService.get(key)}]`);

        this._logger.logInfo('PersistenceService','dumpMemory','sessionStorage: ' + sessionStorage.getItem(key));
        // console.log(`[${PersistenceService.name}][dumpMemory][sessionStorage: ${sessionStorage.getItem(key)}]`);

    }




}
