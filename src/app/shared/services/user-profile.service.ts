import {Injectable} from '@angular/core';
import {IFunctionality} from "../../data-model/interface/IFunctionality";
import {Observable, Subject} from "rxjs/index";
import {BeResponse} from "../../data-model/class/Response";
import {MockDataService} from "./mock-data.service";
import {AuthConstant} from "../../../assets/authConstant";
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {map} from 'rxjs/operators';
import {Authority, IAuthResponse, IPrincipal} from 'src/app/data-model/interface/IAuthResponse';
import {UtilsService} from './utils.service';
import {LoggerService} from './logger.service';
import {PersistenceService} from "./persistence.service";
import {AppSettings} from "../../../AppSettings";
import {IProfile} from "../../data-model/interface/IProfile";

@Injectable({
    providedIn: 'root'
})
export class UserProfileService {

    userProfile: IPrincipal; //IUserInfo
    randId = Math.floor(Math.random() * 1000);
    functionalities: string[] = [];
    functionMap: Map<string,boolean> = new Map();

    private _dev: boolean = false;
    private _activeSupplierId: number;
    private _activeSupplierCode: string;
    private _syncSessions = new Subject<any>();


    constructor(
        private mockDataService: MockDataService,
        private http: HttpClient,
        private _utilsService: UtilsService,
        private _logger: LoggerService,
        private _persistenceService: PersistenceService

    ) {
    }


    // loadUserProfile() {
    //     this.getUserProfile( false).subscribe(resp => {
    //         this.userProfile = resp.object;
    //         this._persistenceService.save('userProfile', this.userProfile);
    //         console.log("["+this.randId+"][UserProfileService][loadUserProfile] userInfo: ", this.userProfile);
    //     },
    //     error => {
    //         this.getUserProfile( true).subscribe(resp => {
    //             this.userProfile = resp.object;
    //             this._persistenceService.save('userProfile', this.userProfile);
    //             console.log("["+this.randId+"][UserProfileService][loadUserProfile] userInfo: ", this.userProfile);
    //             this.loadFunctionalities('012345678', true);
    //         });
    //     });
    // }

    /**
     * Restituisce la lista delle funzionalità a cui l'utente risulta abilitato
     * @param {boolean} mock
     * @returns {any}
     */
    getUserProfile(mock = false): any {
        if (mock) return this.mockDataService.getUserInfo();
        else {
            console.log(`[${UserProfileService.name}][getUserProfile] START:`);
            return this.http.get(environment.api.me, {observe: 'response'})
                .pipe(map((resp: HttpResponse<any>) => {
                        this._logger.logInfo('UserProfileService','getUserProfile',"RESPONSE:", resp);

                        let rawList: IAuthResponse  = resp.body;
/*                        this.setFunctionalities2Profile(rawList.principal.profiles, AppSettings.AUTH_BL, false);
                        //gestisce la funzionalità di impersonate
                        this.updateImpersonateFunction(rawList.authorities);*/

                        this._activeSupplierCode = !!rawList.principal.codTer ? rawList.principal.codTer.toString():'';
                        // this.setLocalFunctionalities(rawList.principal.profiles[0].functions, false);
                        //propagate event to sync session problem
                        // setTimeout(() => {
                        this.syncSessions();
                        // },1000);
                        return new BeResponse(resp.status, null, rawList);
                    })
                );
        }
    }

    /**
     * Restituisce le funzionalità associate al profilo legato all'app BDW o BL
     * @param {IProfile[]} profiles
     * @param app
     * @returns {IFunctionality[]}
     */
    getAppProfile2Principal(profiles: IProfile[], app: string): IFunctionality[] {

        let functions = [];
        if(!this._utilsService.isVoid(profiles) && !this._utilsService.isVoid(app)) {

            profiles.forEach(function(profile) {
                if(profile.app.toUpperCase() == app) {
                    functions = profile.functions;
                }
            });
        }
        return functions;
    }


    /**
     * Verifica che il principal corrente abbia il nome dell'applicativo e che l'utente loggato sia autorizzato
     * @param {IProfile[]} profiles
     * @param {string} app
     * @returns {boolean}
     */
    checkAppProfile(profiles: IProfile[], app: string): boolean {
        let isAuth = false;
        if(!this._utilsService.isVoid(profiles) && !this._utilsService.isVoid(app)) {
            profiles.forEach(function(profile) {
                if(profile.app.toUpperCase() == app) {
                    isAuth = true;
                    return;
                }
            });
        }
        return isAuth;
    }

    /**
     * Esegue l'update della funzionalità di impersonificazione
     * @param {Authority[]} authorities
     */
    updateImpersonateFunction(authorities: Authority[]) {

        let principalImp:Authority = this.getSourceAuthority(authorities);
        if(!this._utilsService.isVoid(principalImp)
            && !this.functionMap.get(AuthConstant.FUNC_IMPERSONATE)) {
            this.functionMap.set(AuthConstant.FUNC_IMPERSONATE,
                this.checkProfileForImpersonate(principalImp, AuthConstant.FUNC_IMPERSONATE));
            // this._persistenceService.save(AppSettings.P_FUNCTIONALY, this.functionMap);


        }
    }

    /**
     * Restituisce l'autority che detiene i dati sorgente per l'impersonificazione
     * @param {Authority[]} authorities
     * @returns {Authority}
     */
    getSourceAuthority(authorities: Authority[]): Authority {
        let authority = null;
        if(!this._utilsService.isVoid(authorities)) {
            authorities.forEach( (auth) =>  {
                if(!this._utilsService.isVoid(auth.source)) {
                    authority = auth;
                    return;
                }
            })
        }
        return authority;
    }

    /**
     * Esegue un check per capire se nel principal specifico dell'impersonate è presente
     * la funzionalità richiesta
     * @param {Authority} authority
     * @param {string} functionality
     * @returns {boolean}
     */
    checkProfileForImpersonate(authority: Authority, functionality: string): boolean {
        let impersonate = false;

        if(!this._utilsService.isVoid(authority)
            && !this._utilsService.isVoid(authority.source)
            && !this._utilsService.isVoid(authority.source.principal)
            && !this._utilsService.isVoid(authority.source.principal.profiles))  {

            authority.source.principal.profiles.forEach((profile) => {
                if(!this._utilsService.isVoid(profile) && !this._utilsService.isVoid(profile.functions)) {
                    profile.functions.forEach((fun) => {


                        if(!this._utilsService.isVoid(fun) && (fun.code.toUpperCase() == functionality)) {
                            impersonate = true;
                            return;
                        }
                    });
                    if(impersonate)
                        return;
                }
            })
        }
        return impersonate;
    }

    // get userInfo(): IPrincipal{
    //     if(!this.userProfile){
    //         this.loadUserProfile();
    //     }
    //     return this.userProfile;
    // }

    public loadFunctionalities(dealershipId: string, fromMock: boolean): void {
        this.getFunctionalities(dealershipId, true).subscribe(resp => {
            let functionalityList: IFunctionality[] = resp.object;
            this._logger.logInfo('UserProfileService','loadFunctionalities', "functionalityList", functionalityList, this.randId);
            this.setLocalFunctionalities(functionalityList, fromMock);
            this.printFunctionalities();
        });
    }

    getFunctionalities(dealershipId: string, mock = false): Observable<BeResponse<IFunctionality[]>> {
        if (mock) return this.mockDataService.getAuthoritiesForUser(dealershipId);
    }

    /**
     * Imposta le funzionalità associate ai profili passati come parametro
     * @param {IProfile[]} profiles
     * @param {string} app
     * @param {boolean} fromMock
     */
    setFunctionalities2Profile(profiles: IProfile[], app: string, fromMock: boolean): void {
        this.setLocalFunctionalities(this.getAppProfile2Principal(profiles, app), fromMock);
    }

    setLocalFunctionalities(functionalities: IFunctionality[], fromMock: boolean) {
        this.functionMap = this.formatLocalFunctionalities(functionalities, fromMock);
        this.printFunctionalities();
    }

    formatLocalFunctionalities(functionalities: IFunctionality[], fromMock: boolean): Map<string, boolean> {

        let fMap = new Map(AuthConstant.FUNCTIONALITY_STARTER_MAP);


        if(!this._utilsService.isVoid(functionalities))
            this._persistenceService.save(AppSettings.P_FUNCTIONALY, functionalities);

        for (let functionality of functionalities) {
            let macroFunctionality = functionality.code;
            if(fMap.has((macroFunctionality))) fMap.set(macroFunctionality, !!macroFunctionality);
            macroFunctionality = !this._utilsService.isVoid(macroFunctionality) ? macroFunctionality.toLowerCase() : '';
            if (!!functionality.attributes) {
                for (let attribute of functionality.attributes) {
                    let microFunctionality = fromMock ? attribute.split('=')[0] : !this._utilsService.isVoid(attribute['code'])? attribute['code']:'';
                    let value = fromMock? attribute.split('=')[1] : !this._utilsService.isVoid(attribute['value'])? attribute['value'].toLowerCase():'';
                    let func;
                    if (!this._utilsService.isVoid(microFunctionality) && !this._utilsService.isVoid(value)) {
                        if( !!AuthConstant.authConstantKeys[macroFunctionality] &&
                            !!AuthConstant.authConstantKeys[macroFunctionality][microFunctionality]
                        )func = AuthConstant.authConstantKeys[macroFunctionality][microFunctionality][value];
                   }
                    if (!!func) fMap.set(func, !!func);
                }
            }
        }

        this._logger.logInfo('UserProfileService','formatLocalFunctionalities', "fMap: ", fMap);
        return fMap
    }

    printFunctionalities() {
//      console.log("[UserProfileService][loadFunctionalities] functionalities: ", this.functionalities);
        this._logger.logInfo('UserProfileService','printFunctionalities', 'functionMap: ', this.functionMap, this.randId);
        // console.log("["+this.randId+"]UserProfileService][printFunctionalities] functionMap: ", this.functionMap);
        }

    haveAbilitation(key: string) {
//        return this.functionalities.lastIndexOf(key) > -1;

        if(this.functionMap.size === 0) {
            let cFun = this._persistenceService.get(AppSettings.P_FUNCTIONALY);
            if(!this._utilsService.isVoid(cFun)) {
                this.setLocalFunctionalities(cFun,false);
            }
        }
        return this.functionMap.get(key);
    }

    public syncSessions() {
        this._syncSessions.next();
    }

    getSyncSessions(): Observable<any> {
        return this._syncSessions.asObservable();
    }



    /**
     * invoca il servizio BE di logout
     */
    logout(){
        this._logger.logInfo('UserProfileService','logout',"START");
        return this.http.get(environment.api.logout, {observe: 'response'})
            .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('UserProfileService','logout',"LOGOUT:",resp);
                })
            );
    }

    // set dealerId(id: string){
    //     this.userProfile.defolutDealership = id;
    // }

    get activeSupplierCode(): string {
        return this._activeSupplierCode;
    }

    set activeSupplierCode(value: string) {
        this._activeSupplierCode = value;
    }

    /** @ignore */
    get activeSupplierId(): number {
        return this._activeSupplierId;
    }


    /** @ignore */
    set activeSupplierId(value: number) {
        this._activeSupplierId = value;
    }

    /** @ignore */
    get dev(): boolean {
        console.warn("get");
        return this._dev;
    }

    /** @ignore */
    set dev(value: boolean) {
        console.warn("set");
        this._dev = value;
    }
}
