import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})


export class ShadowService {

  private eventState = new Subject<any>();


  constructor() {
  }


  updateShadow() {
    this.eventState.next();

  }

  getStatus (): Observable<any> {
    return this.eventState.asObservable();
  }

}