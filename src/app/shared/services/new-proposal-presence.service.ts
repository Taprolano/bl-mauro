import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewProposalPresenceService {

  constructor() { }

  getNewProposalPresence(): boolean {
    if(window.location.href.indexOf('proposals/list') !== -1){
      return true;
    }
    return false;
  }

}
