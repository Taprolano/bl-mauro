import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {IProposal} from "../../data-model/interface/IProposal";
import {IProduct} from "../../data-model/interface/IProduct";
import {AppSettings} from "../../../AppSettings";
import {Observable, Subject} from "rxjs/index";
import {ProposalsConstant} from "../../proposals/proposalsConstant";
import {PersistenceService} from "./persistence.service";
import {UtilsService} from "./utils.service";
import {IProposalSummary} from "../../data-model/interface/IProposalSummary";
import { LoggerService } from './logger.service';
import {FinancialValue} from "../../data-model/class/FinancialValue";


const ERROR_MISSING_STATE = "lo stato inserito non è censito nella lista degli stati della proposta";

/**
 * Service dedicated to managing the states of the proposal
 * and managing the data model and cache
 */
@Injectable({
    providedIn: 'root'
})
export class ProposalService {

    private _proposal: IProposal;
    private _states: Map<string, string>;
    private _currentState: string;
    private _startDate: number;
    private _eventState = new Subject<any>();
    private _startingState: string;
    private _lastState: string;
    private _keyStore = 'PROP';
    private _id = Math.floor(Math.random() * 1000) + 1;
    private _initiated = false;
    private _foundedAmmount: number;


    constructor(private _persistence: PersistenceService,
                private _utilService: UtilsService,
                private _logger: LoggerService) {

        this._proposal = this._initProp();
        this._proposal.state = this._currentState;
        this._startDate = (new Date()).getTime();
        this._currentState = ProposalsConstant.stateProduct2Model;
        this._lastState = ProposalsConstant.stateFinalize2Payments;
        this._startingState = ProposalsConstant.stateProduct2Model;
        this._states = new Map<string, any>();
        this._populateAppStates();

    }

    printMsg(msg: string) {
        this._logger.logInfo('ProposalService','printMsg', 'currentstate ' + this._currentState + msg, this._id);
        // console.log(`[ProposalService (${this._id})][printMsg] currentstate ${this._currentState} {${msg}}`)
    }


    init(): void {
        this._logger.logInfo('ProposalService','init','PROP', JSON.stringify(this._persistence.get(this._keyStore)));
        // console.log('[ProposalService][ngOnInit] PROP', JSON.stringify(this._persistence.get(this._keyStore)));
        this._initiated = true;
        if (!this._utilService.isVoid(this._persistence.get(this._keyStore))) {
            // console.log('[ProposalService][ngOnInit] PROP', this._persistence.get(this._keyStore));
            this._proposal = this._persistence.get(this._keyStore);
            this._currentState = (!this._utilService.isVoid(this._proposal)
                && !this._utilService.isVoid(this._proposal.state)) ? this._proposal.state : undefined;
        }

    }


    destroy(): void {

        this._persistence.save(this._proposal.hash, this._proposal);
        // console.log('[ProposalService][ngOnDestroy] PROP _persistence, hash, proposal',
        //     this._persistence.get(this._keyStore), this._proposal.hash, this._proposal);

    }

    clear() {
        this._persistence.remove(this._keyStore);
        this._proposal = this._initProp();
        this._startDate = (new Date()).getTime();
        this.updateStatus(null); //TODO: capire se serve il refresh dello stato nella clear

    }

    public getCurrentState(): string {
        return this._currentState;
    }


    public updateStatus(state: string) {
        this._logger.logInfo('ProposalService','updateStatus', state);
        // console.log("update status", state);

        /*    if(this._states.get(state) == undefined) {
              // console.log("state", this._states.get(state));
              throw Error(ERROR_MISSING_STATE);
            }*/
        this._currentState = state;
        if (!this._utilService.isVoid(this._proposal))
            this._proposal.state = this._currentState;
        this._eventState.next(state);

    }

    public getNextState(): string {

        let nextState: string = undefined;
        let found: boolean = false;
        let BreakException = {};

        try {
            this._states.forEach((value: string, key: string) => {

                if (found) {
                    nextState = key;
                    throw BreakException;
                }
                if (key === this._currentState)
                    found = true;
            });

        } catch (e) {
            if (e !== BreakException) throw e;
        }
        return nextState;
    }


    public getPrevState(): string {

        let prevState: string = undefined;
        let found: boolean = false;
        let BreakException = {};
        try {
            this._states.forEach((value: string, key: string) => {
                if (key === this._currentState) {
                    found = true;
                    throw BreakException;
                }
                if (key !== this._lastState)
                    prevState = key;
            });

        } catch (e) {
            if (e !== BreakException) throw e;
        }
        return prevState;
    }

    getMessage(): Observable<any> {
        return this._eventState.asObservable();
    }

    public getObj2CurrentState(): any {
        this._logger.logInfo('ProposalService','getObj2CurrentState', "fetch: ", this._proposal );
        // console.log("[getObj2CurrentState] fetch: ", this._proposal);

        if (this._verifyTTL()) {
            this.delete(this._currentState);
        }
        
        this._logger.logInfo('ProposalService','getObj2CurrentState', "CurrentState: ", this._currentState);
        // console.log("CurrentState: ", this._currentState);
        return this._proposal[this._states.get(this._currentState)];
    }

    public getSummary(): IProposalSummary {
        if (!this._initiated) this.init();
        this._logger.logInfo('ProposalService','getSummary', "fetch: ", this._proposal);
        // console.log("[getSummary] fetch: ", this._proposal);
        return this._proposal.summary;
    }

    public getAllDataModel(): IProposal {
        return this._proposal;
    }

    /**
     *
     * @param {string} state
     * @param model
     */
    public save(state: string, model: any) {
        this._logger.logInfo('ProposalService','save', 'STORAGING');
        // console.log("[ProposalService][save] STORAGING");

        this._logger.logInfo('ProposalService','save', model, "in", state, this._states.get(state));
        // console.log("[ProposalService][save] ", model, "in", state, this._states.get(state));
        
        this._proposal[this._states.get(state)] = model;
        this._logger.logInfo('ProposalService','save', "proposal: ", this._proposal);
        // console.log("[ProposalService][save] proposal: ", this._proposal);

        // console.log('[ProposalService][save] state, model, dataModelKey, _proposal',
        //     state, model, this._states.get(state), this._proposal);

    }

    /**
     *
     * @param {string} state
     */
    public delete(state: string) {
        delete this._proposal[this._states.get(state)];
    }

    private _verifyTTL(): boolean {
        return (((new Date()).getTime()) - this._startDate) > AppSettings.TTL_PROPOSAL;
    }

    private _populateAppStates() {

        let entries = Object.keys(this._proposal);
        this._states.set(this._startingState, entries[0]);
        this._states.set(ProposalsConstant
            .stateProduct2Version, entries[0]);
        this._states.set(ProposalsConstant
            .stateCommercial, entries[1]);
        this._states.set(ProposalsConstant
            .stateFinancial, entries[2]);
        this._states.set(ProposalsConstant
            .stateRegistry2Profiles, entries[3]);
        this._states.set(ProposalsConstant
            .stateRegistry2Documents, entries[3]);
        this._states.set(ProposalsConstant
            .statePreliminary, entries[4]);
        this._states.set(ProposalsConstant
            .statePreliminary2Privacy, entries[4]);
        this._states.set(ProposalsConstant
            .stateContract2Frame, entries[5]);
        this._states.set(ProposalsConstant
            .stateContract2Print, entries[5]);
        this._states.set(ProposalsConstant
            .stateFinalize, entries[6]);
        this._states.set(this._lastState, entries[6]);
        this._states.set(ProposalsConstant.summary, entries[7]);

        this._logger.logInfo('ProposalService','ProposalService', 'entries, _states  ', entries, this._states);
        // console.log('[ProposalService][ProposalService] entries, _states  ', entries, this._states);

        this._logger.logInfo('ProposalService','_populateAppStates','key, value', ProposalsConstant.summary, entries[7]);
        // console.log('[ProposalService][_populateAppStates] key, value', ProposalsConstant.summary, entries[7]);

    }

    private _initProp(): IProposal {

        return {
            product: {
                model: null,
                version: null,
                productCode: null
            },
            commercial: null,
            financial: null,
            registry: null,
            preliminary: null,
            contact: null,
            finalize: null,
            // hash: this._utilService.getRandHash(),
            summary: null,
            hash: this._keyStore,
            state: null
        }
    }

    get states(): Map<string, string> {
        return this._states;
    }

    public printMemory() {
        this._persistence.dumpMemoryByKey(this._keyStore);
    }

    get financialServicesList() {
        return !!this._proposal && !!this._proposal.financial ? this._proposal.financial.selectedServicesList : [];
    }

    get financialServicesAmmount() {
        let ammount = 0;

        if (this.financialServicesList) {
            for (let fs of this.financialServicesList) {
                ammount += fs.price;
            }
        }
        return ammount;
    }

    get financialServicesAmmountInsurance() {
        let ammount = 0;

        if (this.financialServicesList) {
            for (let fs of this.financialServicesList) {
                if (!fs.isAssistence) ammount += fs.price;
            }
        }
        return ammount;
    }

    get financialManagementFees() {
        let ammount = 0;

        if (this.financialServicesList) {
            for (let fs of this.financialServicesList) {
                ammount += fs.customerServiceCost;
            }
        }
        return ammount;
    }

    get financialServicesAmmountAssistance() {
        let ammount = 0;

        if (this.financialServicesList) {
            for (let fs of this.financialServicesList) {
                if (fs.isAssistence) ammount += fs.price;
            }
        }
        return ammount;
    }

    get foundedAmmount(): number {
        return this._foundedAmmount;
    }


    set foundedAmmount(value: number) {
        this._foundedAmmount = value;
    }

    set financialServicesList(value: FinancialValue[]) {
        if (this._proposal) {
            if (!!this._proposal.financial) {
                this._proposal.financial.selectedServicesList = value;
            }
        }
        // this.save(ProposalsConstant.stateFinancial, this._proposal.financial);
    }

}
