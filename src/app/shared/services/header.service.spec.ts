import { TestBed } from '@angular/core/testing';

import { HeaderService } from './header.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { createTranslateLoader } from 'src/app/app.module';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MbzDashboardModule } from 'src/app/mbz-dashboard/mbz-dashboard.module';
import { ProposalsModule } from 'src/app/proposals/proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { AppComponent } from 'src/app/app.component';
import { AuthGuard } from '..';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {DirectAccessGuard} from "../guard/direct-access/direct-access.guard";

describe('HeaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      CommonModule,
      BrowserModule,
      BrowserAnimationsModule,
      SharedModule,
      TranslateModule,
      HttpClientModule,
      MbzDashboardModule,
      ProposalsModule,
      NgxSpinnerModule,
      TranslateModule.forRoot({
          loader: {
              provide: TranslateLoader,
              useFactory: createTranslateLoader,
              deps: [HttpClient]
          }
      }),
      AppRoutingModule,
      ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  declarations: [AppComponent],
  providers: [AuthGuard, DirectAccessGuard],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
  }));

  it('should be created', () => {
    const service: HeaderService = TestBed.get(HeaderService);
    expect(service).toBeTruthy();
  });
});
