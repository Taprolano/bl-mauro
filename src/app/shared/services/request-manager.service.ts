import {Injectable} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {IServiceRequest} from "../../data-model/interface/IServiceRequest";
import {NotificationMessage, NOTIFICATION_TYPE} from "../components/notification/NotificationMessage";
import {NotificationService} from "../components/notification/notification.service";
import { LoggerService } from './logger.service';

/**
 *
 * @type {number}
 * @private
 *
 * Soglia per priorità HIGH
 */
const __highTrashold = 10;

/**
 *
 * @type {number}
 * @private
 *
 * Soglia per priorità LOW
 */
const __mediumTrashold = 5;

/**
 *
 * @type {number}
 * @private
 *
 * Soglia per priorità LOW
 */
const __lowTrashold = 1;

export enum REQUEST_PRIORITY {
    HIGH = "HIGH",
    NORMAL = "NORMAL",
    LOW = "LOW"
}

@Injectable({
    providedIn: 'root'
})
export class RequestManagerService {

    //coda dei servizi esetrni chiamati da FE non ancora evasi
    private _workingQueue: IServiceRequest[] = [];

    //coda dei servizi esetrni chiamati da FE evasi
    private _endedQueue: IServiceRequest[] = [];


    constructor(
        private _spinner: NgxSpinnerService,
        private _notificationService: NotificationService,
        private _logger: LoggerService
    ) {
    }

    /**
     *
     * @param {IServiceRequest} request
     *
     * Il metodo aggiunge una nuova chiamata nella coda.
     */
    pushNewRequest(requestTag: string, priority: string): void {
        if (this._workingQueue.length === 0) this._spinner.show();

        let pr = this.getPriorityFromString(priority, this._workingQueue);
        let request: IServiceRequest = {requestTag: requestTag, result: null, priority: pr, beMessage: null};

        let index = this.getIndexOfPriority(request.priority, this._workingQueue);

        this._logger.logInfo('RequestManagerService','pushNewRequest', "ADDED REQUEST: ", request);
        // console.log("ADDED REQUEST: ", request);

        this._workingQueue.splice(index, 0, request);
    }


    /**
     *
     * @param {string} requestTag
     *
     * Metodo che rimuove una richiesta dalla coda di quelle attive e la inserisce in quella delle richieste evase.
     *
     */
    handleRequest(requestTag: string, result: number, beMessage?: string, category?: string): void {

        let index = this.getIndexByTag(requestTag, this._workingQueue);

        let requestHandled: IServiceRequest = this._workingQueue.splice(index, 1)[0];

        requestHandled.result = result;
        requestHandled.beMessage = !!beMessage ? beMessage : null;

        index = this.getIndexOfPriority(requestHandled.priority, this._endedQueue);

        this._endedQueue.splice(index, 0, requestHandled);
        this._logger.logInfo('RequestManagerService','handleRequest',"HANDED REQUEST: ", requestHandled);
        // console.log("HANDED REQUEST: ", requestHandled);

        if (this._workingQueue.length === 0) {
            if(category) this.publishErrorMessage(category);
            else this.publishErrorMessage();
        }



    }

    /**
     *
     * @param {string} priority
     * @returns {number}
     *
     * Il metodo espone un valore numerico per la priorità in funzione di una descrizione qualitativa ["LOW","MEDIUM","HIGH"] passata in input come stringa.
     * Il alore viene calcolato in funzione delle chiamate gia presenti nella coda.
     */
    private getPriorityFromString(priority: string, queue: IServiceRequest[]): number {
        let numericalPriority;
        let maxPriority = queue.length > 0
            ? queue[0].priority
            : __highTrashold;

        let minPriority = queue.length > 0
            ? queue[queue.length - 1].priority
            : __lowTrashold;

        switch (priority) {
            case REQUEST_PRIORITY.HIGH : {
                return Math.max(__highTrashold, maxPriority + 1);
            }

            case REQUEST_PRIORITY.NORMAL: {
                return Math.max(__mediumTrashold, this.avaragePriority(queue))
            }
            case REQUEST_PRIORITY.LOW: {
                return Math.max(__lowTrashold, minPriority - 1)
            }
            default: {
                return __lowTrashold;
            }
        }
    }

    /**
     * Metodo che comunica al servizio di notifca la lista dei messaggi sugli errori;
     */
    private publishErrorMessage(category?: string): void {

        let errorMessageList: NotificationMessage[] = [];

        for (let response of this._endedQueue) {
            if (response.result >= 400) {
                let newMsg = new NotificationMessage(response.requestTag + "-" + response.result , NOTIFICATION_TYPE.ERROR, response.beMessage);
                errorMessageList.push(newMsg);
                this._logger.logInfo('RequestManagerService','publishErrorMessage','NotificationMessage ', newMsg);
                // console.log('NotificationMessage ', newMsg)
            }else{
                this._logger.logInfo('RequestManagerService','publishErrorMessage','NotificationMessage response ', response);
                // console.log('NotificationMessage response ', response)
            }
        }

        this._endedQueue = [];

        this._notificationService.sendNotificationPool(errorMessageList,NOTIFICATION_TYPE.ERROR,"MULTIPLE_ERROR_NOTIFICATION", category);

        this._spinner.hide()
    }


    /**
     *
     * @param {IServiceRequest[]} queue
     * @returns {number}
     *
     * Metodo che restituisce la priorità media delle chiamate presenti una coda. Restituisce sempre un itnero.
     *
     */
    private avaragePriority(queue: IServiceRequest[]): number {
        let average = 0;
        for (let elem of queue) {
            average += elem.priority;
        }
        return Math.ceil(average / queue.length);
    }

    /**
     *
     * @param {number} priority
     * @param {IServiceRequest[]} queue
     * @returns {number}
     *
     * Questo metodo ritorna l'ultimo indice di posizione al interno della queue (passata come argomento) con una data priorità
     */
    private getIndexOfPriority(priority: number, queue: IServiceRequest[]): number {
        let index = 0;

        for (let r of queue) {
            if (priority > r.priority) break;
            index++;
        }

        return index;

    }

    /**
     *
     * @param {string} tag
     * @param {IServiceRequest[]} queue
     * @returns {number}
     *
     * Questo metodo ritorna il primo indice di posizione al interno della queue (passata come argomento) con una data tag
     */
    private getIndexByTag(tag: string, queue: IServiceRequest[]): number {
        let index = 0;

        for (let r of queue) {
            if (tag === r.requestTag) break;
            index++;
        }

        return index;
    }

}
