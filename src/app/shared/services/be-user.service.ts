import {Injectable} from '@angular/core';
import {Observable} from "rxjs/index";
import {MockDataService} from "../../shared/services/mock-data.service";
import {BeResponse} from "../../data-model/class/Response";
import {PaginatedObject} from "../../data-model/class/PaginatedObject";
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {LoggerService} from '../../shared/services/logger.service';
import {ImpUserRequestParams} from "../../data-model/class/ImpUserRequestParams";
import {PaginationData} from "../../data-model/class/PaginationData";
import {ImpUser} from "../../data-model/class/ImpUser";
import {IAuthResponse} from "../../data-model/interface/IAuthResponse";

@Injectable({
  providedIn: 'root'
})
export class BeUserService {

  constructor(
      private _mockDataService: MockDataService,
      private http: HttpClient,
      private _logger: LoggerService
  ) { }


    /**
     * servizio per popolare la lista degli utenti
     * @param params
     * @param mock
     */
    getUser2Impersonate(params: ImpUserRequestParams, mock = false): Observable<BeResponse<PaginatedObject<ImpUser[]>>> {
        if (mock)
            return this._mockDataService.getImpUserList();
        else {
            return this.http.get(environment.api.user.list, {
                observe: 'response',
                params: params.requestParameters()
            })
                .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo(this.constructor.name, 'getUser2Impersonate', resp.body);
                    //   console.log(resp.body);
                    // console.log(resp.body.body);
                    let rawlist: any = resp.body.body.content;
                    this._logger.logInfo(this.constructor.name, 'getUser2Impersonate', rawlist);

                    let pagData = new PaginationData(resp.body.body.totalElements, resp.body.body.totalPages, resp.body.body.pageable.pageNumber);
                    let respObj = new PaginatedObject(pagData, rawlist);

                    return new BeResponse<PaginatedObject<ImpUser[]>>(resp.status, null, respObj);
                }))
            // }
        }
    }

    /**
     * servizio per impersonare un utente
     * @param params
     * @param mock
     */
    impersonateUser(params: ImpUserRequestParams, mock = false): Observable<BeResponse<PaginatedObject<IAuthResponse>>> {
        if (mock)
            return this._mockDataService.impersonateUser();
        else {
            return this.http.get(environment.api.user.impersonate, {
                observe: 'response',
                params: params.requestParameters()
            })
                .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo(this.constructor.name, 'impersonateUser', resp.body);
                    //   console.log(resp.body);
                    // console.log(resp.body.body);
                    let rawlist: any = resp.body;
                    this._logger.logInfo(this.constructor.name, 'impersonateUser', rawlist);
                    let respObj = new PaginatedObject(null, rawlist);

                    return new BeResponse<PaginatedObject<IAuthResponse>>(resp.status, null, respObj);
                }))
            // }
        }
    }






}
