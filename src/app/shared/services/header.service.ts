import {Injectable} from '@angular/core';
import {IHeaderRecap} from 'src/app/data-model/interface/IHeaderRecap';
import {MockDataService} from './mock-data.service';
import {Observable, Subject} from "rxjs/index";
import {Proposal} from "../../data-model/class/Proposal";
import {IProposals} from "../../data-model/interface/IProposals";
import {BeResponse} from "../../data-model/class/Response";
import {ProposalListRequestParameters} from "../../data-model/class/ProposalListRequestParameters";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {ProposalCounter, ProposalCounterFactory} from "../../data-model/class/ProposalCounter";
import {LoggerService} from './logger.service';

@Injectable({
    providedIn: 'root'
})
export class HeaderService {

    eventState = new Subject<any>();

    constructor(private mockService: MockDataService,
                private _http: HttpClient,
                private _logger: LoggerService) {
    }

    getHeaderRecap(fromMock: boolean = false): Observable<BeResponse<ProposalCounter>> {
        let headerRecap: IHeaderRecap;
        if (fromMock) {
            return this.mockService.getHeaderProposalsRecap();
        }
        else {
            return this._http.get(environment.api.proposals.counters)
                .pipe(map((resp: HttpResponse<any>) => {

                    let fac = new ProposalCounterFactory();
                    this._logger.logInfo('HeaderService', 'getHeaderRecap', resp.body);
                    // console.log('getHeaderRecap', resp.body);
                    // console.log(resp.body.body);
                    let rawlist: ProposalCounter = resp.body.counters;

                    this._logger.logInfo('HeaderService', 'getHeaderRecap', rawlist);
                    // console.log('getHeaderRecap-rawlist', rawlist);


                    return new BeResponse<ProposalCounter>(resp.status, null, fac.getInstance(rawlist));
                }))
        }
    }

    public changeUserPanel() {
        this.eventState.next();

    }

    getVariable(): Observable<any> {
        return this.eventState.asObservable();
    }


}
