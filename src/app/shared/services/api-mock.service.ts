import { Injectable } from '@angular/core';
import { TdProposalEntry } from 'src/data_mock/TdProposalEntry';
import { promise } from 'protractor';
import { GdAutoModel } from '../../../data_mock/GdAutoModel';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class ApiMockService {

  constructor(private _logger: LoggerService) { }

  public getProposalEntry(cardinality: number): Promise<any []> {

    let dataRetrieved = new TdProposalEntry(cardinality, this._logger);

    return Promise.resolve(dataRetrieved.fillContent());
  } 

  public getAutoModel(cardinality: number): Promise<any []> {
    let dataRetrieved = new GdAutoModel(cardinality, this._logger);

    return Promise.resolve(dataRetrieved.fillContent());
  }
}
