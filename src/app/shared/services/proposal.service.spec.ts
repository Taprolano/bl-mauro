import { async, TestBed } from '@angular/core/testing';
import { ProposalService } from './proposal.service';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AuthGuard } from '..';
import { AppComponent } from 'src/app/app.component';
import { environment } from 'src/environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { createTranslateLoader } from 'src/app/app.module';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared.module';
import { MbzDashboardModule } from 'src/app/mbz-dashboard/mbz-dashboard.module';
import { ProposalsModule } from 'src/app/proposals/proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import {DirectAccessGuard} from "../guard/direct-access/direct-access.guard";

xdescribe('BeWorkQueue', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));
  it('should be created', () => {
    const service: ProposalService = TestBed.get(ProposalService);
    expect(service).toBeTruthy();
  });
});
