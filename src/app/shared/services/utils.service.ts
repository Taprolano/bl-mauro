import { Injectable } from '@angular/core';
import * as _ from "lodash";
import {IErrorResponse} from "../../data-model/interface/IErrorResponse";
import { LoggerService } from './logger.service';
import { TranslateService } from '@ngx-translate/core';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {
    
    private _logger: LoggerService = new LoggerService();
    constructor() { }

    static translationOptions(translate: TranslateService): void {

        translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        translate.setDefaultLang('it');
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }

    get monthsList(): string[]{
        return ['Gennaio', 'Febbraio', 'Marzo', 'Aprile','Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Novembre', 'Dicembre' ]
    }

    isVoid(input) {
      return input==='undefined' || input===undefined || input===null || input==='null' || input==='';
    }

    getRandHash():string {
      return '_' + Math.random().toString(36).substr(2, 9);
    }

    removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }

    assign(object: any): any {
        return _.cloneDeep(object);
    }

    getErrorResponse(response: any): IErrorResponse{
        let er: IErrorResponse = {code: response.status, message:"" };

        this._logger.logInfo('UtilsService','getErrorResponse', "response:", response);
        // console.log("[UtilsService][getErrorResponse] response:", response);

        this._logger.logInfo('UtilsService','getErrorResponse', "response.error:", response.error);
        // console.log("[UtilsService][getErrorResponse] response.error:", response.error);

        if(response.error) {
            this._logger.logInfo('UtilsService','getErrorResponse', "response.error.message:", response.error.message);
        // console.log("[UtilsService][getErrorResponse] response.error.message:", response.error.message);

        er.message = !!response.error.message
                        ? response.error.message
                        : !!response.message
                            ? response.message
                            : null;

        }

        this._logger.logInfo('UtilsService','getErrorResponse',"er:", er);
        // console.log("[UtilsService][getErrorResponse] er:", er);

        return er;
    }

    /**Funzione per ottenere hashcode di una variabile */
    hash(stringToHash): string {
        let hash = JSON.stringify(stringToHash);
        return btoa(hash);
    }

    /**Funzione per fare unhash di una variabile */
    unhash(stringToUnhash): any {
        return JSON.parse(atob(stringToUnhash));
    }

    /**Funzione per fare unhash di una variabile */
    formatOBject4Output = function (obj: any): any {


        switch (typeof obj) {

            //Deep compare objects
            case 'object':

                if (Array.isArray(obj)) {
                    let newArray = [];
                    for (let elem of obj) {
                        newArray.push(this.formatOBject4Output(elem));
                    }

                    return newArray;
                }
                if (obj === null) {
                    return null;
                }

                let newObj = {};
                for (let k in obj) {
                    let newK = k.startsWith("_") ? k.substring(1) : k;
                    if(!!obj[k]){
                        newObj[newK] = this.formatOBject4Output(obj[k]);
                    }
                }
                return newObj;
            default:
                return obj;
        }

    };

    isUndefided = function (input) {
        return input === 'undefined';
    };

    /** @ignore*/
    addCssClassByIds(cssClass: string, ...ids: string[]) {
        ids.forEach(id => {
            let target = document.querySelector('#' + id);
            if (target) target.classList.add(cssClass);
        });
    }

    /** @ignore */
    removeCssClassByIds(cssClass: string, ...ids: string[]) {
        ids.forEach(id => {
            let target = document.querySelector('#' + id);
            if (target) target.classList.remove(cssClass);
        });
    }
}
