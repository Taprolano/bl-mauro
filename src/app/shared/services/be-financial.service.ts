import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {LoggerService} from "./logger.service";
import {MockDataService} from "./mock-data.service";
import {BeResponse} from "../../data-model/class/Response";
import {Observable} from 'rxjs';
import {FinancialProducts} from "../../data-model/class/FinancialProducts";
import {environment} from '../../../environments/environment.svil';
import {map} from 'rxjs/internal/operators';
import {CampaignDetail, FinancialCampaign} from '../../data-model/class/FinancialCampaign';
import {FinancialCampaignReqParams} from '../../data-model/class/FinancialCampaignReqParams';
import {FinancialServicesReqParams} from "../../data-model/class/FinancialServicesReqParams";
import {FinancialServices, FinancialServicesFactory} from '../../data-model/class/FinancialServices';
import {checkCompatibility, CheckCompatibilityReqParamas} from "../../data-model/class/CheckCompatibilityReqParamas";
import {FinancialPlans, FinancialPlansFactory} from '../../data-model/class/FinancialPlans';
import {FinancialConfigServiceBodyParameters} from "../../data-model/class/FinancialConfigServiceBodyParameters";
import {ListValuesEnhancedReqParams} from "../../data-model/class/ListValuesEnhancedReqParams";
import {ListValuesEnhanced} from '../../data-model/class/ListValuesEnhanced';
import {FinancialConfigServiceResponse, FinancialConfigServiceResponseFactory} from "../../data-model/class/FinancialConfigServiceResponse";
import {FinancialServiceBodyParameters} from "../../data-model/class/FinancialServiceBodyParameters";
import * as FileSaver from 'file-saver';
import {IFinancialPlans} from "../../data-model/interface/IFinancialPlans";
import {ISavedProposal} from "../../data-model/class/ISavedProposal";
import {
    BeSaveSimulationOfferReqParams,
    BeSaveSimulationOfferReqParamsFactory
} from '../../data-model/class/save-simulation-offer/BeSaveSimulationOfferReqParams';
import {Attachment} from '../../data-model/class/Attachment';
import {
    SaveDealerOfferProposalFactory,
    SaveSimulationOfferProposal
} from '../../data-model/class/save-simulation-offer/SaveSimulationOfferProposal';

const ID_MAIL = 'M013';

/**@ignore Piano Finanziario mock */
const MOCK_PLAN = `{
            "allestimenti": [ ],
            "anticipoPercentuale": 22,
            "assetClassCode": "10",
            "assetModel": "A 160 CDI Classi autom.",
            "assetType": "MBA",
            "balloon": true,
            "balloonPercentuale": 5,
            "baumuster": "16800612001",
            "brandId": "MBF",
            "codeIpoteca": " ",
            "codiceTipoInteresse": " ",
            "contractEnd": "2019-06-30",
            "contractLength": 36,
            "contributoCampagna": 0,
            "contributoConcessionario": 0,
            "currentMileAge": 0,
            "customerFullName": "Ayna Kebayeva",
            "decorrenza": "2019-06-28",
            "distance": 30,
            "financedAmount":  15286.00,
            "financialProductCode": "FB",
            "frequency": "30",
            "idProposal": "1002009503",
            "importoAnticipo":  3821.49,
            "importoBalloonNetto": 1252.17,
            "importoFornitura": 0,
            "importoListino": 14875.00,
            "importoRataAnticipoServizi": 0,
            "importoRataBalloon": 1252.17,
            "importoRataBalloonServizi": 0.00,
            "importoRataServizi": 0.00,
            "incentivoVendita": 0,
            "ipt": 0,
            "kilometraggio": 0,
            "marca": "Mercedes-Benz",
            "netCost":  15661.88,
            "optionals": [
                {
                    "extraDescription":"STRADA",
                    "extraNetCost":  786.88,
                    "extravatamt":  786.88,
                    "extrarvadjust": 0
                }
            ],
            "percentualeDiListino": 5.26,
            "prodottoFinanziario": "Finanziamento Balloon",
            "proposalStatus": "NEW",
            "rata": 452.77,
            "rataNetta":452.77,
            "rataPercentuale": 0,
            "rental": false,
            "speseAnticipo": 0,
            "speseDiIstruttoria": 300.00,
            "speseRataCanone": 0,
            "rate": [
                {
                    "bloccato": true,
                    "dataPagamentoRata": "20/06/2019",
                    "cfAmount": 3821.49,
                    "cfAmountPercentage": 20,
                    "molteplicita": 1,
                    "distanza": 0,
                    "anticipo": true,
                    "balloon": false
                },
                {
                    "bloccato": false,
                    "dataPagamentoRata": "20/07/2019",
                    "cfAmount": 452.77,
                    "cfAmountPercentage": 1.56,
                    "molteplicita": 35,
                    "distanza": 1,
                    "anticipo": false,
                    "balloon": false
                },
                {
                    "bloccato": true,
                    "dataPagamentoRata": "20/06/2023",
                    "cfAmount": 1252.17,
                    "cfAmountPercentage": 5,
                    "molteplicita": 1,
                    "distanza": 48,
                    "anticipo": false,
                    "balloon": true
                }
            ],
            "taeg": 9.33,
            "tan": 7.11,
            "tipoInteresse": "Fisso",
            "totalAmount": 19107.49,
            "used": false,
            "vatPercentage": 22.0,
            "versionum": 0
            }`;

const BE_FIN_PLAN = `{  
      "campiBloccati":{  
         "anticipo":true
      },
      "rataPercentuale":1.57,
      "rata":393.54,
      "balloonPercentuale":37.6,
      "contributoConcessionario":0.0,
      "codiceTipoInteresse":"FX",
      "dealerBuyBack":false,
      "contributoCampagna":0.0,
      "incentivoVendita":80.19,
      "speseAnticipo":0.0,
      "speseRataCanone":0.0,
      "importoListino":19754.1,
      "importoFornitura":0.0,
      "ipt":0.0,
      "percentualeDiListino":0.0,
      "totalCreditAmount":20048.0,
      "totalCustomerAmount":29011.99,
      "frequency":1,
      "selFixComAmount":0.0,
      "commCamp":false,
      "adminFeeShareOverride":false,
      "mustUseSpIstr":false,
      "dealManuContrComm":false,
      "manuPercContrComm":0.0,
      "dealPercManuContrComm":0.0,
      "flagShare":false,
      "mfr":2.988,
      "tan":7.05199999999999960209606797434389591217041015625,
      "taeg":8.41499999999999914734871708787977695465087890625,
      "manuFixContrComm":0.0,
      "dealPercCom":0.0,
      "brandId":"MBF",
      "assetClassCode":"10",
      "assetType":"MBA",
      "financialProductCode":"FB",
      "contractLength":36,
      "optionals":[  
         {  
            "extraDescription":"STRADA",
            "extraNetCost":786.8799999999999954525264911353588104248046875,
            "extraVatAmount":173.113599999999998999555828049778938293457031250
         }
      ],
      "speseDiIstruttoria":300.0,
      "netCost":20540.98,
      "vatPercentage":22,
      "used":false,
      "baumuster":"17604110-IT0DL1",
      "distance":30,
      "totalAmount":25060,
      "importoAnticipo":5012,
      "anticipoPercentuale":20.0,
      "importoRataBalloon":9423.100000000000363797880709171295166015625,
      "codeIpoteca":"00",
      "rate":[  
         {  
            "bloccato":true,
            "dataPagamentoRata":"24/07/2019",
            "cfAmount":5012.0,
            "cfAmountPercentage":20.0,
            "molteplicita":1,
            "distanza":0,
            "anticipo":true,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/08/2019",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":1,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/09/2019",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":2,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/10/2019",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":3,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/11/2019",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":4,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/12/2019",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":5,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/01/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":6,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/02/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":7,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/03/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":8,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/04/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":9,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/05/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":10,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/06/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":11,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/07/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":12,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/08/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":13,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/09/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":14,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/10/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":15,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/11/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":16,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/12/2020",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":17,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/01/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":18,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/02/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":19,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/03/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":20,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/04/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":21,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/05/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":22,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/06/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":23,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/07/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":24,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/08/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":25,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/09/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":26,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/10/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":27,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/11/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":28,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/12/2021",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":29,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/01/2022",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":30,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/02/2022",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":31,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/03/2022",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":32,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/04/2022",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":33,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/05/2022",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":34,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/06/2022",
            "cfAmount":393.54,
            "cfAmountPercentage":1.57,
            "molteplicita":1,
            "distanza":35,
            "anticipo":false,
            "balloon":false
         },
         {  
            "bloccato":false,
            "dataPagamentoRata":"24/07/2022",
            "cfAmount":9423.1,
            "cfAmountPercentage":37.6,
            "molteplicita":1,
            "distanza":36,
            "anticipo":false,
            "balloon":true
         }
      ],
      "financedAmount":20048,
      "leasing":false,
      "rental":false,
      "dealerCDTer":390038773,
      "balloon":false,
      "sellerCommission":0.0,
      "percentAtDealer":0.0,
      "percentAtSales":0.0,
      "mss":786.88,
      "spread":2.0,
      "dealFixCom":0.0,
      "selFixComPerc":0.0,
      "provImportoVendita":80.19,
      "provImportoVenditaPerc":0.0,
      "provImportoSpeseIstruttoria":0.0,
      "provImportoSpeseIstruttoriaPerc":0.0,
      "provImportoServizi":0.0,
      "provImportoServiziPerc":0.0,
      "provImportoDeltaTasso":646.44,
      "tassoBase":4.988,
      "sellerSIpercentage":0.0,
      "dealerSIpercentage":0.0,
      "valoreCommissione":0.0,
      "provImportoDeltaTassoPerc":0.0,
      "provImportoTotale":726.6300000000001
   }`;


@Injectable({
    providedIn: 'root'
})
export class BeFinancialService {

    /**
     *
     * @param _mockDataService
     * @param _http
     * @param _logger
     */
    constructor(private _mockDataService: MockDataService,
                private _http: HttpClient,
                private _logger: LoggerService) {
    }

    /**
     *
     * @param {boolean} mock
     * @param {FinancialServicesReqParams} params
     * @returns {Observable<BeResponse<FinancialServices[]>>}
     */
    getFinancialInsuranceList(params: FinancialServicesReqParams, mock = false): Observable<BeResponse<FinancialServices[]>> {

        if (mock) {
            return null;
        } else {
            return this._http.get(environment.api.financial.insurance,
                {observe: 'response', params: params.requestParameters()}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('Financial Service', 'getFinancialInsuranceList', resp.body.body.serviceEntities);
                    let rawlist: any = resp.body.body.serviceEntities;

                    let financialInsuranceList: FinancialServices[] = FinancialServicesFactory.getInstanceList(rawlist);


                    return new BeResponse<any>(resp.status, resp.body.message, financialInsuranceList);
                })
            );

        }
    }

    /**
     * Method in which financial/configService service is called
     * @param {boolean} mock
     * @param {FinancialConfigServiceBodyParameters} params
     * @returns {Observable<BeResponse<FinancialConfigServiceBodyParameters>>}
     */
    getFinancialConfigService(params: FinancialConfigServiceBodyParameters, mock = false): Observable<BeResponse<FinancialConfigServiceResponse>> {
        let jsonObjectT = <JSON> params.requestParameters();

        if (mock) {
            return null;
        } else {
            return this._http.post(environment.api.financial.configService, jsonObjectT, {observe: 'response'}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('Financial Service', 'getFinancialConfigService ', resp.body);
                    const rawlist: any = resp.body.body;

                    let finConfig = FinancialConfigServiceResponseFactory.getInstance(rawlist);

                    this._logger.logInfo('Financial Service', 'getFinancialConfigService', rawlist);
                    return new BeResponse<any>(resp.status, resp.body.message, finConfig);
                })
            );
        }

    }

    /**
     * Method in which financial/service service is called
     * @param {boolean} mock
     * @param {FinancialServiceBodyParameters} params
     * @returns {Observable<BeResponse<any>>}
     */
    getFinancialService(params: FinancialServiceBodyParameters, mock = false): Observable<BeResponse<any>> {
        let jsonObjectT = <JSON> params.requestParameters();
        if (mock) {
            return null;
        } else {
            return this._http.post(environment.api.financial.service, jsonObjectT, {observe: 'response'}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('Financial Service', 'getFinancialServiceX', 'resp.body', resp.body);
                    return new BeResponse<number>(resp.status, resp.body.message, resp.body.body);
                })
            );
        }
    }

    /**
     *
     * @param {boolean} mock
     * @param {CheckCompatibilityReqParamas} params
     * @returns {Observable<BeResponse<CheckCompatibilityReqParamas>>}
     */
    getCheckCompatibility(params: CheckCompatibilityReqParamas, mock = false): Observable<BeResponse<checkCompatibility>> {
        if (mock) {
            return null;
        } else {
            return this._http.get(environment.api.financial.checkCompatibility,
                {observe: 'response', params: params.requestParameters()}).pipe(
                map((resp: HttpResponse<any>) => {
                    const rawlist: any = resp.body;

                    this._logger.logInfo('Financial Service', 'getCheckCompatibility', resp.body);
                    return new BeResponse<any>(resp.status, resp.body.message, rawlist);
                })
            );
        }

    }

    /**
     * Return financialCampaign list after financial/campaigns service call
     * @param mock
     * @param params
     */
    getCampaignList(params: FinancialCampaignReqParams, mock = false): Observable<BeResponse<FinancialCampaign[]>> {

        if (mock) {
            if (this._mockDataService.getCampaignList()) {
                return this._mockDataService.getCampaignList();
            }
        } else {
            return this._http.get(environment.api.financial.campaigns,
                {observe: 'response', params: params.requestParameters()}).pipe(//  , params: params.requestParameters()}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('Financial Service', 'getCampaignList', resp.body);
                    const rawlist: any = resp.body.body;

                    const financialCampaignsList: FinancialCampaign[] = [];
                    let campDetail/* = new CampaignDetail()*/;
                    let campaignDetailList: CampaignDetail[] = [];

                    for (const item of rawlist) {

                        campaignDetailList = [];

                        for (let camp of item.campaigns) {
                            campDetail = camp;
                            campaignDetailList.push(campDetail);
                        }

                        const financialCampaign: FinancialCampaign = new FinancialCampaign(
                            item.fpCode, item.fpDescription, false, '', campaignDetailList, campDetail.showVoucher
                        );

                        financialCampaignsList.push(financialCampaign);
                    }

                    this._logger.logInfo('Financial Service', 'getCampaignList', rawlist);

                    return new BeResponse<any>(resp.status, resp.body.message, financialCampaignsList);
                })
            );

        }
    }


    //TODO Da ultimare
    postProductList(mock = false): Observable<BeResponse<FinancialProducts>> {
        if (mock)
            return this._mockDataService.getProductList();
        else {
            return this._http.post(environment.api.financial.products,
                {observe: 'response'}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo(BeFinancialService.name, 'postProductList', 'resp.body', resp.body);
                    let rawlist: any = resp.body[0];

                    let respObj = new FinancialProducts(rawlist.fpCode, rawlist.fpDescription, rawlist.financeCode);
                    this._logger.logWarn(BeFinancialService.name, 'postProductList', respObj);


                    return new BeResponse<FinancialProducts>(resp.status, resp.body.message, respObj);
                })
            );

        }
    }

    /**
     * Return financial plans after financial/plans service call
     * @param fp
     */
    getFinancialPlans(fp: FinancialPlans): Observable<BeResponse<FinancialPlans>> {

        // let jsonObject = <JSON> params.requestParameters();
        this._logger.logInfo('Financial Service', 'getFinancialPlans requestParameters', fp.requestParameters());

        return this._http.post(environment.api.financial.plans, fp.requestParameters(),{observe: 'response'}).pipe(
            map((resp: HttpResponse<any>) => {
                this._logger.logInfo('Financial Service', 'getFinancialPlans TEST', resp);
                const rawlist: any = resp.body.body;
                this._logger.logInfo('Financial Service', 'getFinancialPlans', rawlist);

                let financialPlan = !!rawlist ? FinancialPlansFactory.getInstance(rawlist) : null;

                return new BeResponse<any>(resp.status, resp.body.message, financialPlan );
            })
        );

    }

    /**
     * Return list values list from backend or mock
     * @param mock
     * @param params
     */
    getAllFinancialViewListValuesEnhanced(params: ListValuesEnhancedReqParams, mock = false): Observable<BeResponse<ListValuesEnhanced[]>> {

        if (mock) {
            if (this._mockDataService.getFinancialListValuesEnhanced()) {
                return this._mockDataService.getFinancialListValuesEnhanced();
            }
        } else {

            return this._http.get(environment.api.listOfValuesEnhanced,
                {observe: 'response', params: params.requestParameters()}).pipe(//  , params: params.requestParameters()}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('Financial Service', 'getListValuesEnhancedReqParams', resp.body);
                    const rawlist: any = resp.body.body;

                    let resultList: ListValuesEnhanced[] = rawlist;

                    this._logger.logInfo('Financial Service', 'getListValuesEnhancedReqParams', rawlist);

                    return new BeResponse<any>(resp.status, resp.body.message, resultList);
                })
            );

        }
    }

    /**
     * Return pdf file after /document service call
     * @param attachment
     */
    getPdfFile(attachment: Attachment): void {
        let paramsHR = new HttpParams();
        /** Pdf params get from selected detail campaign */
        if (attachment) {
            paramsHR = paramsHR.set('contentType', attachment.contentType);
            paramsHR = paramsHR.set('name', attachment.name);
            paramsHR = paramsHR.set('url', attachment.url);
            this._logger.logInfo('Financial Service', 'getPdfFile', 'paramsHR real', paramsHR);
        } else {
            this._logger.logInfo('Financial Service', 'getPdfFile', 'Attachment not found in Campaign Detail');
        }

        /** Mocked Values for /document http request */
        let mockedReqPdfParam = new HttpParams();
        mockedReqPdfParam = mockedReqPdfParam.set('contentType', 'application/pdf');
        mockedReqPdfParam = mockedReqPdfParam.set('name', 'test');
        mockedReqPdfParam = mockedReqPdfParam.set('url', '/attachments/test.pdf');

        this._http.post(environment.api.document, paramsHR, {responseType: 'blob'}).subscribe(
            resp => {

                /** Save pdf file gets from BE and automatically download start */
                FileSaver.saveAs(resp, paramsHR.get('name') + '.pdf');
                this._logger.logInfo('Financial Service', 'getPdfFile', 'Pdf downloaded');

            },
            err => {
                this._logger.logInfo('Financial Service', 'getPdfFile', 'Pdf download error');
            });

    }

    /**
     * Scarica il pdf generato dal BE tramite i dati del preventivo forniti
     * @param mock
     * @param {FinancialPlans} plan
     */
    getPriceQuotePdf(plan: FinancialPlans, mock: boolean = false): void {

        let reqParams = new HttpParams().set('customerPrivacy', 'true').set('tipoStampa', 'offerta');

        console.log(JSON.stringify(plan.requestParameters()));


        const mockedPlan = JSON.parse(MOCK_PLAN);

        const planParams = mock ? mockedPlan : plan.requestParameters();

        this._http.post(environment.api.proposals.priceQuote, planParams, {responseType: 'blob', params: reqParams}).subscribe(
            resp => {
                this._logger.logInfo(this.constructor.name, 'getPriceQuotePdf', resp);
                FileSaver.saveAs(resp, 'prospect.pdf');
            },
            err => {
                this._logger.logInfo(this.constructor.name, 'getPriceQuotePdf', 'Pdf download error');
            });
    }

    /**
     * BE call that save the dealer offer
     *
     * @param params
     * @param mock
     */
    saveDealerOffer(params: BeSaveSimulationOfferReqParams, mock = false): Observable<BeResponse<any>> {

        // let jsonObject = <JSON> params.requestParameters();
        this._logger.logInfo('Financial Service', 'Save Dealer Offer requestParameters', params.requestParameters());

        return this._http.post('URLDAINSERIRE', params.requestParameters(), {observe: 'response'}).pipe(
            map((resp: HttpResponse<any>) => {

                this._logger.logInfo('Financial Service', 'saveDealerOffer', resp);
                const rawlist: any = resp.body.body;
                this._logger.logInfo('Financial Service', 'saveDealerOffer', rawlist);

                // TODO Restituire la proposta come response
                // let response = !!rawlist ? BeSaveSimulationOfferReqParamsFactory.getInstance(rawlist) : null;
                // console.log('responseSaveDealer -> ' + JSON.stringify(response));

                return new BeResponse<any>(resp.status, resp.body.message, 'test');
            })
        );
    }

    /**
     * BE call that prints the simulation
     *
     * @param plan
     * @param mock
     */
    simulationPrint(plan: FinancialPlans, mock: boolean = false): void {

        // TODO verificare se presenti parametri in query string
        let reqParams = new HttpParams().set('customerPrivacy', 'true').set('tipoStampa', 'offerta');

        console.log(JSON.stringify(plan.requestParameters()));

        const mockedPlan = JSON.parse(MOCK_PLAN);

        const planParams = mock ? mockedPlan : plan.requestParameters();

        // TODO cambiare endpoint ed eventuali parametri di request con il servizio BE per la stampa della simulazione (quando pronto)
        this._http.post(environment.api.proposals.priceQuote, planParams, {responseType: 'blob', params: reqParams}).subscribe(
            resp => {
                this._logger.logInfo(this.constructor.name, 'simulationPrint', resp);
                // TODO da verificare il nome del pdf da salvare
                FileSaver.saveAs(resp, 'simulation.pdf');
            },
            err => {
                this._logger.logInfo(this.constructor.name, 'simulationPrint', 'Pdf download error');
            });
    }

    /**
     * BE call that prints the cover
     *
     * @param plan
     * @param mock
     */
    frontCoverPrint(plan: FinancialPlans, mock: boolean = false): void {

        // TODO verificare se presenti parametri in query string
        let reqParams = new HttpParams().set('customerPrivacy', 'true').set('tipoStampa', 'offerta');

        console.log(JSON.stringify(plan.requestParameters()));

        const mockedPlan = JSON.parse(MOCK_PLAN);

        const planParams = mock ? mockedPlan : plan.requestParameters();

        // TODO cambiare endpoint ed eventuali parametri di request con il servizio BE per la stampa del frontalino (quando pronto)
        this._http.post(environment.api.proposals.priceQuote, planParams, {responseType: 'blob', params: reqParams}).subscribe(
            resp => {
                this._logger.logInfo(this.constructor.name, 'frontCoverPrint', resp);
                // TODO da verificare il nome del pdf da salvare
                FileSaver.saveAs(resp, 'frontCover.pdf');
            },
            err => {
                this._logger.logInfo(this.constructor.name, 'frontCoverPrint', 'Pdf download error');
            });

    }

    /**
     * BE call that restore the proposal
     *
     * @param proposalID
     * @param version
     * @param mock
     */
    getProposalContext(proposalID: string, version: string, mock = false): Observable<BeResponse<SaveSimulationOfferProposal>> {
        return this._http.get(environment.api.proposals.proposalContext + '/' + proposalID, {
            observe: 'response',
            params: {'version': version}
        })
            .pipe(map((resp: HttpResponse<any>) => {

                let rawlist: any = resp.body;

                this._logger.logInfo('be-proposal.service', 'getProposalContext', rawlist);

                let response: SaveSimulationOfferProposal = !!rawlist ? SaveDealerOfferProposalFactory.getInstance(rawlist) : null;

                return new BeResponse<SaveSimulationOfferProposal>(resp.status, null, response);
            }));
    }

    /** END FINANCIAL ACTIONS SERVICES */

    /** Salva la proposta lato BE e ritorna numero e versione associati */
    saveProposal(plan: IFinancialPlans, mock = false): Observable<BeResponse<ISavedProposal>> {
        const mockPlan = JSON.parse(BE_FIN_PLAN);

        const financialPlan = mock? mockPlan : plan;

        return this._http.post(environment.api.proposals.save, financialPlan,{observe: 'response'}).pipe(map((resp:any)=>{
            const prop:ISavedProposal = {PROPNUM: resp.body.body.PROPNUM,VERSIONNUM:resp.body.body.VERSIONNUM};
            return new BeResponse<ISavedProposal>(resp.body.code,resp.body.message,prop);
        }));
    }

    sendProposalByEmail(idProposal, addrTo){
        const params = new HttpParams().set('to',addrTo).set('idProposal',idProposal).set('idMail',ID_MAIL);
        this._http.post(environment.api.email.priceQuote,null,{observe:'response', params}).subscribe(resp=>{
            console.log('PROP MAILED:',resp);
        }, err =>{
            this._logger.logError(this.constructor.name,'sendProposalByEmail',err);
        });
    }
}