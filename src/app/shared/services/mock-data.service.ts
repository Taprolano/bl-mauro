import {Injectable} from '@angular/core';
import {ISupplier} from '../../data-model/interface/ISupplier';
import {BeResponse} from '../../data-model/class/Response';
import {of} from 'rxjs';
import {Observable, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {IOptional} from '../../data-model/interface/IOptional';
import {IProposals} from 'src/app/data-model/interface/IProposals';
import {PaginatedObject} from '../../data-model/class/PaginatedObject';
import {IVersion} from '../../data-model/interface/IVersion';
import {NOTIFICATION_TYPE, NotificationMessage} from '../components/notification/NotificationMessage';
import {IDealer} from 'src/app/data-model/interface/IDealer';
import {IDealership} from 'src/app/data-model/interface/IDealership';
import {IFunctionality} from '../../data-model/interface/IFunctionality';
import {IUserInfo} from '../../data-model/interface/IUserInfo';
import {IDealershipInfo} from '../../data-model/interface/IDealershipInfo';
import {AuthConstant} from '../../../assets/authConstant';
import {HttpErrorResponse} from '@angular/common/http';
import {ProposalCounter} from '../../data-model/class/ProposalCounter';
import {IModels} from '../../data-model/interface/IModels';
import {CreditWorkQueue} from '../../data-model/class/CreditWorkQueue';
import { CreditCorporateQueue } from '../../data-model/class/CreditCorporateQueue';
import { PaginationData } from '../../data-model/class/PaginationData';
import {ImpUser} from '../../data-model/class/ImpUser';
import {IAuthResponse, IPrincipal} from '../../data-model/interface/IAuthResponse';
import {DealerContactQueue} from '../../data-model/class/DealerContactQueue';
import {CampaignDetail, FinancialCampaign} from "../../data-model/class/FinancialCampaign";
import {Attachment} from "../../data-model/class/Attachment";
import {FinancialProducts} from "../../data-model/class/FinancialProducts";
import {ListValuesEnhanced, ValueListEnhanced} from "../../data-model/class/ListValuesEnhanced";

const __constOptionalList: IOptional[] = [
    {
        code: '531-dF',
        description: 'Sist multimed Tablet integrato Comand Online(DL3)',
        price: 2960.00,
        priceWithIVA: 3611.20,
        discount: false,
        selected: true
    },
    {
        code: 'AC2F',
        description: 'Rapporto al ponte i=4,111',
        price: 2200.00,
        priceWithIVA: 2684.00,
        discount: false,
        selected: true
    },
    {
        code: 'PEAF',
        description: 'Motorsport pack (cn 191o650o662o696o761o787)',
        price: 2160.00,
        priceWithIVA: 2635.20,
        discount: false,
        selected: true
    },
    {
        code: 'AC9F',
        description: 'Corso NEVE: 2 giorni in montagna',
        price: 2000.00,
        priceWithIVA: 2440.00,
        discount: false,
        selected: true
    },
    {
        code: '662C',
        description: 'Grigio Montagna magno designo',
        price: 1795.00,
        priceWithIVA: 2189.90,
        discount: false,
        selected: true
    },
    {
        code: '531-aF',
        description: 'Sist multimed Tablet integrato Comand Online(DL1)',
        price: 1700.00,
        priceWithIVA: 2074.00,
        discount: false,
        selected: true
    },
    {
        code: '531-bF',
        description: 'Sist multimed Tablet integrato Comand Online(DL2)',
        price: 1700.00,
        priceWithIVA: 2074.00,
        discount: false,
        selected: true
    },
    {
        code: '531-cF',
        description: 'Sist multimed Tablet integrato Comand Online(DL5)',
        price: 1700.00,
        priceWithIVA: 2074.00,
        discount: false,
        selected: true
    },
    {
        code: 'DN1-cP',
        description: 'Pacchetto NEXT (DL3)',
        price: 1557.38,
        priceWithIVA: 1900.00,
        discount: false,
        selected: true
    },
    {
        code: '855-bT',
        description: 'Pelle nappa porcellana/plancia nera(cnDL1,955,P34)',
        price: 1500.00,
        priceWithIVA: 1830.00,
        discount: false,
        selected: true
    },
    {
        code: '855-cT',
        description: 'Pelle nappa porcellana/plancia nera(cnDL3,P34)',
        price: 1500.00,
        priceWithIVA: 1830.00,
        discount: false,
        selected: true
    },
    {
        code: '804-aT',
        description: 'Pelle nocciola (con DL1)',
        price: 1500.00,
        priceWithIVA: 1830.00,
        discount: false,
        selected: true
    },
    {
        code: '804-bT',
        description: 'Pelle nocciola (con DL2)',
        price: 1500.00,
        priceWithIVA: 1830.00,
        discount: false,
        selected: true
    },
    {code: '801-aT', description: 'Pelle Nera', price: 1500.00, priceWithIVA: 1830.00, discount: false, selected: true},
    {
        code: '801-bT',
        description: 'Pelle passion nera (solo con 950/P24)',
        price: 1500.00,
        priceWithIVA: 1830.00,
        discount: false,
        selected: true
    },
    {
        code: 'B28F',
        description: 'Pacchetto carbonio AMG II per gli esterni',
        price: 1300.00,
        priceWithIVA: 1586.00,
        discount: false,
        selected: true
    },
    {
        code: 'DF2F',
        description: 'Pacchetto Comfort',
        price: 1200.00,
        priceWithIVA: 1464.00,
        discount: false,
        selected: true
    },
    {code: 'DF2P', description: 'PLUS pack', price: 1200.00, priceWithIVA: 1464.00, discount: false, selected: true},
    {
        code: 'AC1F',
        description: 'Corso BASE: (AC1) 1 giorno su circuito',
        price: 1100.00,
        priceWithIVA: 1342.00,
        discount: false,
        selected: true
    },
    {
        code: '855-aT',
        description: 'Pelle nappa porcellana/plancia nera	                              ',
        price: 1100.00,
        priceWithIVA: 1342.00,
        discount: false,
        selected: true
    },
    {
        code: '804-cT',
        description: 'Pelle nocciola (con DL3)',
        price: 1100.00,
        priceWithIVA: 1342.00,
        discount: false,
        selected: true
    },
    {
        code: '[ N.D. ]',
        description: 'Pelle Nappa nera cuciture a contrasto (con DEX)',
        price: 1100.00,
        priceWithIVA: 1342.00,
        discount: false,
        selected: true
    },
    {
        code: '228F',
        description: 'Riscaldamento supplementare',
        price: 1100.00,
        priceWithIVA: 1342.00,
        discount: false,
        selected: true
    },
    {
        code: '811T',
        description: 'Rivestimenti in pelle Nappa black con cielo nero',
        price: 1100.00,
        priceWithIVA: 1342.00,
        discount: false,
        selected: true
    },
    {
        code: '646-cF',
        description: 'Cerchi lega AMG BLACK Performance 18"(cnP55/DL1/DL2)',
        price: 1057.02,
        priceWithIVA: 1289.56,
        discount: false,
        selected: true
    },
    {
        code: '[ N.D. ]',
        description: 'Pacchetto sportivo (inc R19,V26 con DL1)',
        price: 975.00,
        priceWithIVA: 1189.50,
        discount: false,
        selected: true
    },
    {
        code: 'R69-cF',
        description: 'Cerchi in lega da 17" a 5 razze in nero torniti a specchio (DL6)',
        price: 950.00,
        priceWithIVA: 1159.00,
        discount: false,
        selected: true
    },
    {
        code: '522-dF',
        description: 'Audio 20 CD sistema multimediale Tablet (DL0)',
        price: 900.00,
        priceWithIVA: 1098.00,
        discount: false,
        selected: true
    },
    {
        code: '632-bF',
        description: 'Fari a LED (cn DL2 )',
        price: 875.00,
        priceWithIVA: 1067.50,
        discount: false,
        selected: true
    },
    {
        code: '632-dF',
        description: 'Fari anteriori LED e fari posteriori full LED (DL3)',
        price: 875.00,
        priceWithIVA: 1067.50,
        discount: false,
        selected: true
    },
    {
        code: '632-cF',
        description: 'Fari anteriori LED e fari posteriori full LED (DL5)',
        price: 875.00,
        priceWithIVA: 1067.50,
        discount: false,
        selected: true
    },
    {
        code: '239F',
        description: 'Distronic Plus',
        price: 865.29,
        priceWithIVA: 1055.65,
        discount: false,
        selected: true
    },
    {
        code: '612-bF',
        description: 'Cerchi in lega AMG da 18" a 5 doppie razze in grigio titanio (DL2)',
        price: 830.00,
        priceWithIVA: 1012.60,
        discount: false,
        selected: true
    },
    {
        code: '791-cF',
        description: 'Cerchi in lega da 18" AMG multirazze silver (DL3)',
        price: 830.00,
        priceWithIVA: 1012.60,
        discount: false,
        selected: true
    },
    {code: 'DF3P', description: 'ACTIVE pack', price: 820.00, priceWithIVA: 1000.40, discount: false, selected: true},
    {
        code: 'DF3-cF',
        description: 'ACTIVE pack (cn DL2 comprende 632,P49)',
        price: 820.00,
        priceWithIVA: 1000.40,
        discount: false,
        selected: true
    },
    {
        code: 'DF3-bF',
        description: 'Pacchetto Lounge (DL0comp P14,P65,730,301)',
        price: 820.00,
        priceWithIVA: 1000.40,
        discount: false,
        selected: true
    },
    {
        code: 'DF3-aF',
        description: 'Pacchetto Lounge (DL1comp P14,P65,730,301)',
        price: 820.00,
        priceWithIVA: 1000.40,
        discount: false,
        selected: true
    },
    {
        code: 'P66F',
        description: 'SPORT STAR EDITION. Serie speciale limitata.',
        price: 819.67,
        priceWithIVA: 1000.00,
        discount: false,
        selected: true
    },
    {
        code: '550-aF',
        description: 'Gancio traino',
        price: 782.64,
        priceWithIVA: 954.82,
        discount: false,
        selected: true
    },
    {code: '550F', description: 'Gancio traino', price: 782.64, priceWithIVA: 954.82, discount: false, selected: true},
    {
        code: '550-bF',
        description: 'Gancio traino estraibile (solo con P54',
        price: 782.64,
        priceWithIVA: 954.82,
        discount: false,
        selected: true
    },
    {
        code: '550-cF',
        description: 'Gancio traino estraibile(cn DL6)',
        price: 782.64,
        priceWithIVA: 954.82,
        discount: false,
        selected: true
    }
];


@Injectable({
    providedIn: 'root'
})
export class MockDataService {

    constructor() {
    }

    getOptionalList(optCode: string,
                    optDescription: string,
                    pagination: PaginationData): Observable<BeResponse<PaginatedObject<IOptional[]>>> {


        const optionalList: IOptional[] = []; // = __constOptionalList;
        let code = 200;
        if (pagination == null || (pagination.pageSize == null || pagination.currentPage == null )) {
            code = 409;
        }


        for (const opt of __constOptionalList) {
            if (
                ( !optDescription || opt.description.toLowerCase().match(optDescription.toLowerCase()) ) &&
                ( !optCode || opt.code.toLowerCase().match(optCode.toLowerCase()))) { optionalList.push(opt); }
        }

        pagination.totalElements = optionalList.length;

        const start = (pagination.currentPage - 1) * pagination.pageSize;
        const end = start + pagination.pageSize;

        const paginatedOptionalList = new PaginatedObject<IOptional[]>(pagination, optionalList.slice(start, end));

        pagination.totalElements = optionalList.length;

        return of(paginatedOptionalList).pipe(map(resp => {

            const response: BeResponse<PaginatedObject<IOptional[]>> =
                new BeResponse<PaginatedObject<IOptional[]>>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 409: {
                    response.message = new NotificationMessage('Bad Request', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;

        }));
    }

    getSuppliersList(): Observable<BeResponse<ISupplier[]>> {

        const suppliersList: ISupplier[] = [
            {label: 'Pardiso SRL', code: 'paradisoSRL'},
            {label: 'Urban Mobility Store', code: 'ums'},
            {label: 'Roma Mobility Store', code: 'rms'}
        ];
        const code = 200;

        return of(suppliersList).pipe(map(resp => {

            const response: BeResponse<ISupplier[]> = new BeResponse<ISupplier[]>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;

        }));
    }

    getHeaderProposalsRecap(): Observable<BeResponse<ProposalCounter>> {

        const headerRecap: ProposalCounter = new ProposalCounter(
            43,
            23,
            74,
            12
        );

        const code = 200;


        return of(headerRecap).pipe(map(resp => {

            const response: BeResponse<ProposalCounter> = new BeResponse<ProposalCounter>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('contatori non trovati', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;

        }));


        // Promise.resolve(headerRecap);
    }

    getModelCars(): Observable<BeResponse<IModels>> {
        // imagePath: string;
        // assetTypeDesc: string;
        // makeDesc: string;
        // orderNumber: number;
        // visibleAsNew: string;
        // makeCode: string;
        // assetType: string;
        // assetClassCode: string;
        // linkPromotion?: string;
        // hovered?: boolean;



        const listCars: IModels = {
            Vetture: [
                            {
                                imagePath: '/media/images/assets/MBA.jpg',
                                assetTypeDesc: 'Classe A',
                                makeDesc: 'Mercedes-Benz',
                                orderNumber: 100,
                                visibleAsNew: '1',
                                makeCode: 'MB',
                                assetType: 'MBA',
                                assetClassCode: '10',
                                linkPromotion: '',
                                hovered: false
                            },
                            {
                                imagePath: '/media/images/assets/MBB.jpg',
                                assetTypeDesc: 'Classe B',
                                makeDesc: 'Mercedes-Benz',
                                orderNumber: 105,
                                visibleAsNew: '1',
                                makeCode: 'MB',
                                assetType: 'MBB',
                                assetClassCode: '10',
                                linkPromotion: '',
                                hovered: false
                            },
                            {
                                imagePath: '/media/images/assets/MBC.jpg',
                                assetTypeDesc: 'Classe C',
                                makeDesc: 'Mercedes-Benz',
                                orderNumber: 110,
                                visibleAsNew: '1',
                                makeCode: 'MB',
                                assetType: 'MBC',
                                assetClassCode: '10',
                                linkPromotion: '',
                                hovered: false
                            }
        ],
        Altri: [],
        Smart: [],
        Truck: [],
        Van: []
        };
        // codice risposta mock http
        const code = 404;

        switch (code) {
            case 404: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
                break;
            }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            // case 200: {break; }// non è errore
            // // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna risultati mock
        return of(listCars).pipe(map(resp => {
            const response: BeResponse<IModels> = new BeResponse<IModels>(code, null, null);
            response.object = resp;
            response.message = null;
            return response;
        }));
    }

    getCreditQueueList(): Observable<BeResponse<PaginatedObject<CreditWorkQueue[]>>> {
        const creditQueueList: CreditWorkQueue[] = [{
            'lastNote': 'Passaggio dalla PreIstruttoria all\'Istruttoria [09:45]',
                'processInstanceId': 254,
                'proposta': '1000902366',
                'tipoDiSoggetto': 'Persona fisica',
                'numDocMancanti': 0,
                'score': 'null',
                'numBlocchi': 1,
                'blocked': false,
                'arrivo': '17:01:2012 11:00',
                'istruttoria': 'null',
                'urgente': false,
                'prodotto': 'Vettura',
                'assegnatario': 'null',
                'richiedente': 'ZAZZARELLI ROSANNA',
                'esposizione': 102927.7,
                'riproposta': false,
                'numNote': 1,
                'campagna': 'NAF',
                'descrizioneCampagna': 'Campagna Test'
        }];


        const code = 200; // 400
        // ritorna Observable<never> contenente errori http simulati
        switch (code) {
            // case 404: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
            //     // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
            //     break;
            // }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            case 200: {break; }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna lista mock
        return of(creditQueueList).pipe(map(resp => {
            const response: BeResponse<PaginatedObject<CreditWorkQueue[]>> =
                new BeResponse<PaginatedObject<CreditWorkQueue[]>>(code, null, null);
            response.object = new PaginatedObject(null, resp);
            response.message = null;
            return response;
        }));
    }

    getCreditCorporateQueueList(): Observable<BeResponse<PaginatedObject<CreditCorporateQueue[]>>> {
        const creditQueueList: CreditCorporateQueue[] = [{
            'arrivo': '17:01:2012 11:00',
            'tipoProposta': 'Finanziamento',
            'prodotto': '',
            'richiedente': 'FORDENT FORNITURE DENTALI SRL',
            'esposizione': '98453.61',
            'numBlocchi': 1,
            'proposta': 1000900097
        }];


        const code = 404; // 400
        // ritorna Observable<never> contenente errori http simulati
        switch (code) {
            case 404: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
                // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
                break;
            }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            // case 200: {break; }// non è errore
            // // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna lista mock
        return of(creditQueueList).pipe(map(resp => {
            const response: BeResponse<PaginatedObject<CreditCorporateQueue[]>> =
                new BeResponse<PaginatedObject<CreditCorporateQueue[]>>(code, null, null);
            response.object = new PaginatedObject(null, resp);
            response.message = null;
            return response;
        }));
    }

    getProposalsList(): Observable<BeResponse<IProposals[]>> {
        const proposalsList: IProposals[] = [
            {
                asset: 1,
                proposal: '100200746223',
                contract: '--',
                subject: 'Henry Richardson Henry Richardson',
                state: '0',
                date: '01-12-2020',
                deadline: '01-12-2020',
                priority: 'C',
                seller: 'Alfie Wood'
            },
            {
                asset: 3,
                proposal: '100200746462',
                contract: '2880242',
                subject: 'Alicia Puma',
                state: '10',
                date: '01-12-2019',
                deadline: '01-12-2020',
                priority: 'C',
                seller: 'Veerie de Bree'
            },
            {
                asset: 3,
                proposal: '100200746983',
                contract: '--',
                subject: 'Irene Sotelo',
                state: '15',
                date: '01-12-2019',
                deadline: '01-12-2020',
                priority: 'C',
                seller: 'Matilda Evans'
            }
        ];

        const code = 200; // 400
        // ritorna Observable<never> contenente errori http simulati
        switch (code) {
        //     case 404: {
        //         return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
        //         // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
        //         break;
        //     }
        //     case 500: {
        //         return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
        //         break;
        //     }
            case 200: {break; }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna lista mock
        return of(proposalsList).pipe(map(resp => {
            const response: BeResponse<IProposals[]> = new BeResponse<IProposals[]>(code, null, null);
            response.object = resp;
            response.message = null;
            return response;
        }));
    }

    getCarVersions(): Observable<BeResponse<IVersion[]>> {
        const listVersions: IVersion[] = [
            {
                productCode: '17600209',
                version: 'A200d Automatic 4 MATIC EXECUTIVE\n',
                fuel: 'D',
                catalog: '01-12-2020',
                price: 112358,
                forceSecondHand: false
            },
            {
                productCode: '17600000',
                version: 'TESTER DRIVE\n',
                fuel: 'D',
                catalog: '01-12-2020',
                price: 1000000,
                forceSecondHand: false
            },
            {
                productCode: '42000',
                version: 'A200j Automatic 5 MATIC EXECUTIVE\n',
                fuel: 'GPL',
                catalog: '01-12-2020',
                price: 42000,
                forceSecondHand: false
            }
        ];

        const code = 200;

        switch (code) {
            // case 404: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
            //     // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
            //     break;
            // }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            case 200: {break; }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }

        return of(listVersions).pipe(map(resp => {
            const response: BeResponse<IVersion[]> = new BeResponse<IVersion[]>(code, null, null);
            response.object = resp;
            response.message = null;
            return response;
        }));
    }

    getDealerInfo(): Observable<BeResponse<IDealer>> {
        const currentDealer: IDealer = {
            id: '753698',
            name: 'Marcello Rossini',
            dealerType: 'Venditore senior',
            dealershipsAvailable: [
                {
                    name: 'Mercedes-Benz Roma',
                    id: '63548'
                },
                {
                    name: 'Mercedes-Benz Milano',
                    id: '63532'
                }
            ],
            dealershipSelected: 'Mercedes-Benz Roma'
        };
        const code = 200;

        return of(currentDealer).pipe(map(resp => {
            const response: BeResponse<IDealer> = new BeResponse<IDealer>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Utente dealer non trovato', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;
        }));
    }

    getDealershipDetails(): Observable<BeResponse<IDealership>> {

        const dealershipDetails: IDealership = {
            info: {
                name: 'Mercedes-Benz Roma',
                id: '63548'
            },
            dealerAuth: ['all', 'partial', 'simmulation', 'mbcpos', 'att', 'n_tb', 'u_tb', '3part', 'acccs']

            // proposalsList : {
            //     selectableFilters : ["Tutte", "Offerte Concessionario", "Documentazione Incompleta", "Simulazioni"]
            // },
            // product: {
            //     versionFilters: ["Codice prodotto", "Versione"]
            // },
            // commercial: {
            //     newUsedTabs: {
            //         new: true,
            //         used: true
            //     },
            //     accessoriesAccordion: {
            //         presence: true,
            //         accessoriesTabs: {
            //             insert: true,
            //             find: true
            //         }
            //     },
            //     thirdParts: {
            //         presence: true
            //     },
            //     suppliers: ["Paradiso SRL", "Roma Mobility Store"]
            // }
        };

        const code = 200;

        return of(dealershipDetails).pipe(map(resp => {
            const response: BeResponse<IDealership> = new BeResponse<IDealership>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Concessionaria non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;
        }));
    }

    getAuthoritiesForUser(dealershipId: string): Observable<BeResponse<IFunctionality[]>> {
        // let id: number = +dealershipId
        const userAuthorities = [];
        if ('458'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.DEALER_PREFERENCE,
            description: 'DEALER_PREFERENCE',
            attributes: ['abilitaEstrattoConto=SI']
        });
        }

        if ('012345678'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONSULTAZIONE,
            description: 'PROP_CONSULTAZIONE',
            attributes: ['scope=PERSONALI']
        });
        }

        if ('12345678'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONF_COMMERCIALE,
            description: 'PROP_CONF_COMMERCIALE',
            attributes: ['abilitaFornitoreEsterno=SI', 'abilitaAllestimentoTerzeParti=SI']
        });
        }

        if ('78'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONF_COMMERCIALE_VAL_USATO,
            description: 'PROP_CONF_COMMERCIALE_VAL_USATO',
            attributes: []
        });
        }

        if ('012345678'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONF_FIN_DEROGA_SERVIZI,
            description: 'PROP_CONF_FIN_DEROGA_SERVIZI',
            attributes: []
        });
        }

        if ('234578'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONF_FIN_DEROGA_COMMERCIALE,
            description: 'PROP_CONF_FIN_DEROGA_COMMERCIALE',
            attributes: []
        });
        }
        if ('358'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONF_FIN_PROVVIGIONI,
            description: 'PROP_CONF_FIN_PROVVIGIONI',
            attributes: []
        });
        }
        if ('34578'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_CONF_FIN_PATTO_RIACQUISTO,
            description: 'PROP_CONF_FIN_PATTO_RIACQUISTO',
            attributes: []
        });
        }
        if ('458'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_STAMPA_FRONTALINO,
            description: 'PROP_STAMPA_FRONTALINO',
            attributes: []
        });
        }
        if ('12345678'.indexOf(dealershipId) > -1) {
            userAuthorities.push({
                code: AuthConstant.PROP_CONF_FINANZIARIA,
                description: 'PROP_CONF_FINANZIARIA',
                attributes: []});
            userAuthorities.push({
                code: AuthConstant.PROP_ANAGRAFICA,
                description: 'PROP_ANAGRAFICA',
                attributes: ['abilitaOmocodia=SI'] // ['abilitaOmocodia=NO']
            });
            userAuthorities.push({
                code: AuthConstant.PROP_ISTRUTTORIA,
                description: 'PROP_ISTRUTTORIA',
                attributes: ['abilitaInvioIstruttoria=NO'] // ['abilitaInvioIstruttoria=SI']
            });
            userAuthorities.push({
                code: AuthConstant.PROP_NUMERAZIONE,
                description: 'PROP_NUMERAZIONE',
                attributes: ['abilitaRichiestaNumerazione=SI'] // ['abilitaRichiestaNumerazione=NO']
            });
            userAuthorities.push({
                code: AuthConstant.PROP_FIRMA_CONTRATTO,
                description: 'PROP_FIRMA_CONTRATTO',
                attributes: ['abilitaStampaContratto=SI'] // ['abilitaStampaContratto=NO']
            });
            userAuthorities.push({
                code: AuthConstant.PROP_ATTIVAZIONE,
                description: 'PROP_ATTIVAZIONE',
                attributes: ['abilitaAttivazione=SI'] // ['abilitaAttivazione=NO']
            });
            userAuthorities.push({
                code: AuthConstant.PROP_RIF_CONF_COMMERCIALE,
                description: 'PROP_RIF_CONF_COMMERCIALE',
                attributes: []
            });
            userAuthorities.push({
                code: AuthConstant.PROP_NOTE_GESTIONE,
                description: 'PROP_NOTE_GESTIONE',
                attributes: []
            });
            userAuthorities.push({
                code: AuthConstant.PROP_DOC_GESTIONE,
                description: 'PROP_DOC_GESTIONE',
                attributes: []
            });
            userAuthorities.push({
                code: AuthConstant.PROP_CANCELLA,
                description: 'PROP_CANCELLA',
                attributes: []
            });
            userAuthorities.push({
                code: AuthConstant.PROP_DELTA_MOD_ISTR,
                description: 'PROP_DELTA_MOD_ISTR',
                attributes: []
            });
            userAuthorities.push({
                code: AuthConstant.PROP_RESET,
                description: 'PROP_RESET',
                attributes: []
            });

        }
        if ('678'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_RIL_CONF_COMMERCIALE,
            description: 'PROP_RIL_CONF_COMMERCIALE',
            attributes: []
        });
        }
        if ('1234578'.indexOf(dealershipId) > -1) { userAuthorities.push({
            code: AuthConstant.PROP_NOLEGGIO,
            description: 'PROP_NOLEGGIO',
            attributes: []
        });
        }
        // ['abilitaEstrattoConto=NO']

        /*
            {
                code: "DEALER_PREFERENCE",
                description: "DEALER_PREFERENCE",
                attributes: ['abilitaEstrattoConto=SI'] //['abilitaEstrattoConto=NO']
            },
            {

            },
            {
                code: "PROP_CONF_COMMERCIALE",
                description: "PROP_CONF_COMMERCIALE",
                attributes: ['abilitaFornitoreEsterno=NO', 'abilitaAllestimentoTerzeParti=NO'] //['abilitaFornitoreEsterno=SI', 'abilitaAllestimentoTerzeParti=SI'] ['abilitaFornitoreEsterno=NO', 'abilitaAllestimentoTerzeParti=SI'] ['abilitaFornitoreEsterno=NO', 'abilitaAllestimentoTerzeParti=NO']
            },
            {
                code: "PROP_CONF_COMMERCIALE_VAL_USATO",
                description: "PROP_CONF_COMMERCIALE_VAL_USATO",
                attributes: []
            },
            {
                code: "PROP_CONF_FINANZIARIA",
                description: "PROP_CONF_FINANZIARIA",
                attributes: []
            },
            {
                code: "PROP_CONF_FIN_DEROGA_SERVIZI",
                description: "PROP_CONF_FIN_DEROGA_SERVIZI",
                attributes: []
            },
            {
                code: "PROP_CONF_FIN_DEROGA_COMMERCIALE",
                description: "PROP_CONF_FIN_DEROGA_COMMERCIALE",
                attributes: []
            },
            {
                code: "PROP_CONF_FIN_PROVVIGIONI",
                description: "PROP_CONF_FIN_PROVVIGIONI",
                attributes: []
            },
            {
                code: "PROP_CONF_FIN_PATTO_RIACQUISTO",
                description: "PROP_CONF_FIN_PATTO_RIACQUISTO",
                attributes: []
            },
            {
                code: "PROP_STAMPA_FRONTALINO",
                description: "PROP_STAMPA_FRONTALINO",
                attributes: []
            },
            {
                code: "PROP_STAMPA_SIMULAZIONE",
                description: "PROP_STAMPA_SIMULAZIONE",
                attributes: []
            },
            {
                code: "PROP_OFFERTA_DEALER",
                description: "PROP_OFFERTA_DEALER",
                attributes: []
            },
            {
                code: "PROP_STAMPA PROPOSTA",
                description: "PROP_STAMPA PROPOSTA",
                attributes: []
            },
            {
                code: "PROP_ANAGRAFICA",
                description: "PROP_ANAGRAFICA",
                attributes: ['abilitaOmocodia=SI'] //['abilitaOmocodia=NO']
            },
            {
                code: "PROP_ISTRUTTORIA",
                description: "PROP_ISTRUTTORIA",
                attributes: ['abilitaInvioIstruttoria=NO'] //['abilitaInvioIstruttoria=SI']
            },
            {
                code: "PROP_NUMERAZIONE",
                description: "PROP_NUMERAZIONE",
                attributes: ['abilitaRichiestaNumerazione=SI'] //['abilitaRichiestaNumerazione=NO']
            },
            {
                code: "PROP_FIRMA_CONTRATTO",
                description: "PROP_FIRMA_CONTRATTO",
                attributes: ['abilitaStampaContratto=SI'] //['abilitaStampaContratto=NO']
            },
            {
                code: "PROP_ATTIVAZIONE",
                description: "PROP_ATTIVAZIONE",
                attributes: ['abilitaAttivazione=SI'] //['abilitaAttivazione=NO']
            },
            {
                code: "PROP_RIF_CONF_COMMERCIALE",
                description: "PROP_RIF_CONF_COMMERCIALE",
                attributes: []
            },
            {
                code: "PROP_RIL_CONF_COMMERCIALE",
                description: "PROP_RIL_CONF_COMMERCIALE",
                attributes: []
            },
            {
                code: "PROP_NOLEGGIO",
                description: "PROP_NOLEGGIO",
                attributes: []
            },
            {
                code: "PROP_NOTE_GESTIONE",
                description: "PROP_NOTE_GESTIONE",
                attributes: []
            },
            {
                code: "PROP_DOC_GESTIONE",
                description: "PROP_DOC_GESTIONE",
                attributes: []
            },
            {
                code: "PROP_CANCELLA",
                description: "PROP_CANCELLA",
                attributes: []
            },
            {
                code: "PROP_DELTA_MOD_ISTR",
                description: "PROP_DELTA_MOD_ISTR",
                attributes: []
            },
            {
                code: "PROP_RESET",
                description: "PROP_RESET",
                attributes: []
            }

    ]*/

        const code = 200;

        return of(userAuthorities).pipe(map(resp => {

            const response: BeResponse<any> = new BeResponse<any>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Lista Authorities non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;
        }));
    }

    getUserInfo(): Observable<BeResponse<IUserInfo>> {
        const dealerships: IDealershipInfo[] = [
            {name: 'Mercedes - CONSULTAZIONE', id: '0'},
            {name: 'Mercedes - VENDITORE JUNIOR', id: '1'},
            {name: 'Mercedes - VENDITORE', id: '2'},
            {name: 'Mercedes - VENDITORE SENIOR', id: '3'},
            {name: 'Mercedes - BACKOFFICE', id: '4'},
            {name: 'Mercedes - UTENTE AGITA', id: '5'},
            {name: 'Mercedes - AMMINISTRATORE DEALER', id: '6'},
            {name: 'Mercedes - UTENTE MBFSI', id: '7'},
            {name: 'Mercedes - SUPER USER', id: '8'}
        ];
        const user: IUserInfo = {
            name: 'Paolo',
            surname: 'Rossi',
            jobRole: 'Junior Dealer',
            dealerships: dealerships,
            defolutDealership: '8'
        };

        const code = 200;

        return of(user).pipe(map(userResp => {

            const response: BeResponse<any> = new BeResponse<any>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = userResp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Lista Authorities non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;
        }));
    }


    getImpUserList(): Observable<BeResponse<PaginatedObject<ImpUser[]>>> {
        const userList: ImpUser[] = [

            new ImpUser(
            'credit_check_retail',
             'user_su@nttdata.com',
            'credit_retail',
            'Lattazzi',
            'Luigi'),
            new ImpUser(
                'test',
                'test@nttdata.com',
                'test',
                'Test',
                'Impersonate'),
            new ImpUser(
                'direttore_credit_operations',
                'direttore_credit_operations@nttdata.com',
                'direttore_credit_operations',
                'Isidori',
                'Davide')

        ];
        const code = 200; // 400
        // ritorna Observable<never> contenente errori http simulati
        switch (code) {
            // case 404: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
            //     // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
            //     break;
            // }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            case 200: {break; }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna lista mock
        return of(userList).pipe(map(resp => {
            const response: BeResponse<PaginatedObject<ImpUser[]>> = new BeResponse<PaginatedObject<ImpUser[]>>(code, null, null);
            response.object = new PaginatedObject(null, resp);
            response.message = null;
            return response;
        }));
    }

    getDealerContactQueueList(): Observable<BeResponse<PaginatedObject<DealerContactQueue[]>>> {
        const userList: DealerContactQueue[] = [

            new DealerContactQueue(
                'ora',
                'asset',
                'richiedente',
                'sospesaDa',
                1,
                1,
                'prodotto',
                1,
                2,
                'causaleSospensione',
                'campagna',
                'descrizioneCampagna',
                'propStatus'
            )

        ];
        const code = 200; // 400
        // ritorna Observable<never> contenente errori http simulati
        switch (code) {
            // case 404: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
            //     // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
            //     break;
            // }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            case 200: {break; }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna lista mock
        return of(userList).pipe(map(resp => {
            const response: BeResponse<PaginatedObject<DealerContactQueue[]>> = new BeResponse<PaginatedObject<DealerContactQueue[]>>(code, null, null);
            response.object = new PaginatedObject(null, resp);
            response.message = null;
            return response;
        }));
    }

    impersonateUser(): Observable<BeResponse<PaginatedObject<IAuthResponse>>> {
        const auth: IAuthResponse = {
            authorities: [],
            details: '',
            authenticated: true,
            principal: null,
            authorizedClientRegistrationId: '',
            credentials: '',
            name: ''
        };


        const code = 200; // 400
        // ritorna Observable<never> contenente errori http simulati
        switch (code) {
            // case 404: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
            //     // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
            //     break;
            // }
            // case 500: {
            //     return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
            //     break;
            // }
            case 200: {break; }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
            }
        }
        // ritorna lista mock
        return of(auth).pipe(map(resp => {
            const response: BeResponse<PaginatedObject<IAuthResponse>> = new BeResponse<PaginatedObject<IAuthResponse>>(code, null, null);
            response.object = new PaginatedObject(null, resp);
            response.message = null;
            return response;
        }));
    }


    /*MOCK FINANCIAL*/


    /**
     * Mocked list of Financial Campaigns
     */
    getCampaignList(): Observable<BeResponse<FinancialCampaign[]>> {

        let mockAttachment = new Attachment('test-contType', 'test/url', 'test-name');

        const campaignDetailsLeasingMock: CampaignDetail = {
            internalProposal: false,
            code: 'CODE1',
            expiryDate: '01/01/1900',
            finalExpiryDate: '',
            name: 'My Drive Pass Classe A',
            description: 'Offerta valida sulla gamma nuova Classe A, escluse le motorizzazioni AMG',
            tan: '3,90%',
            taeg: '4,88%',
            advance: '6500 €',
            defaultValue: true,
            mustUseSpIstr: false,
            speseIstruttoria: 0,
            adminFeeShareOverride: false,
            commCamp: false,
            dealManuContrComm: false,
            flagShare: false,
            selFixComAmount: 0,
            selFixComPerc: 0,
            manuFixContrComm: 0,
            manuPercContrComm: 0,
            dealPercManuContrComm: 0,
            dealFixCom: 0,
            dealPercCom: 0,
            attachment: mockAttachment,
            tattica: false
        };

        const campaignDetailsFinanziamentoMock: CampaignDetail = {
            internalProposal: false,
            code: 'CODE2',
            expiryDate: '01/01/1901',
            finalExpiryDate: '',
            name: 'My Drive Pass Classe B',
            description: 'Offerta valida sulla gamma nuova Classe B, escluse le motorizzazioni AMG',
            tan: '3,50%',
            taeg: '4,70%',
            advance: '6000 €',
            defaultValue: false,
            mustUseSpIstr: false,
            speseIstruttoria: 0,
            adminFeeShareOverride: false,
            commCamp: false,
            dealManuContrComm: false,
            flagShare: false,
            selFixComAmount: 0,
            selFixComPerc: 0,
            manuFixContrComm: 0,
            manuPercContrComm: 0,
            dealPercManuContrComm: 0,
            dealFixCom: 0,
            dealPercCom: 0,
            attachment: mockAttachment,
            tattica: false
        };

        const campaignDetailsFinanziamentoMock2: CampaignDetail = {
            internalProposal: false,
            code: 'CODE3',
            expiryDate: '01/01/1902',
            finalExpiryDate: '',
            name: 'My Drive Pass Classe C',
            description: 'Offerta valida sulla gamma nuova Classe C, escluse le motorizzazioni AMG',
            tan: '4,00%',
            taeg: '4,50%',
            advance: '5300 €',
            defaultValue: false,
            mustUseSpIstr: false,
            speseIstruttoria: 0,
            adminFeeShareOverride: false,
            commCamp: false,
            dealManuContrComm: false,
            flagShare: false,
            selFixComAmount: 0,
            selFixComPerc: 0,
            manuFixContrComm: 0,
            manuPercContrComm: 0,
            dealPercManuContrComm: 0,
            dealFixCom: 0,
            dealPercCom: 0,
            attachment: mockAttachment,
            tattica: false
        };

        const campaignDetailsArrayLeasing: CampaignDetail[] = [
            campaignDetailsLeasingMock,
            campaignDetailsFinanziamentoMock,
            campaignDetailsLeasingMock,
            campaignDetailsFinanziamentoMock
        ];

        const campaignDetailsArrayFinanziamento: CampaignDetail[] = [
            campaignDetailsFinanziamentoMock,
            campaignDetailsFinanziamentoMock2
        ];

        const financialCampaignLeasing: FinancialCampaign = new FinancialCampaign('LP', 'Leasing', true, '', campaignDetailsArrayLeasing,false,true);
        const financialCampaignFinanziamento: FinancialCampaign = new FinancialCampaign('FN', 'Finanziamento', false, '', campaignDetailsArrayFinanziamento,false,true);

        const campaignsList: FinancialCampaign[] = [
            financialCampaignLeasing,
            financialCampaignFinanziamento
        ];

        const code = 200;

        return of(campaignsList).pipe(map(resp => {

            const response: BeResponse<FinancialCampaign[]> = new BeResponse<FinancialCampaign[]>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('Lista Campagne non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;

        }));
    }

    /**
     *
     * @returns {Observable<BeResponse<FinancialProducts>>}
     */
    getProductList(): Observable<BeResponse<FinancialProducts>> {
        const finProd = {
            fpCode: 'FB',
            fpDescription: 'Finanziamento Balloon',
            financeCode: 'F'
        };

        const code = 200; // 400
        const response: BeResponse<FinancialProducts> = new BeResponse<FinancialProducts>(code, null, null);
        // ritorna Observable<never> contenente errori http simulati
        switch (response.code) {
            case 404: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Not Found'}));
                // response.message = new NotificationMessage('Lista Proposte non trovata', NOTIFICATION_TYPE.ERROR);
                break;
            }
            case 500: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Internal Error'}));
                break;
            }
            case 200: {
                break;
            }// non è errore
            // altri errori
            default: {
                return throwError(new HttpErrorResponse({status: code, statusText: 'Mock Error: Generic Error'}));
                break;
            }
        }
        // ritorna lista mock
        return of(finProd).pipe(map(resp => {
            response.object = new FinancialProducts(resp.fpCode, resp.fpDescription, resp.financeCode);
            return response;
        }));
    }

    /**
     * Mocked list of Financial ListValues
     */
    getFinancialListValuesEnhanced(): Observable<BeResponse<ListValuesEnhanced[]>> {

        const mockValues: ValueListEnhanced [] = [
            {
                key: 'CHEQ',
                value: 'Rimessa diretta',
                defaultValue: false,
                additionalProperties: [{
                    key: '',
                    value: ''
                }]
            },
            {
                key: 'DDEB',
                value: 'RID',
                defaultValue: true,
                additionalProperties: [{
                    key: '',
                    value: ''
                }]
            },
            {
                key: 'DLRCT',
                value: 'Consegna Concessionario',
                defaultValue: false,
                additionalProperties: [{
                    key: '',
                    value: ''
                }]
            },
            {
                key: 'TRATT',
                value: 'Trattenuta Stipendio',
                defaultValue: false,
                additionalProperties: [{
                    key: '',
                    value: ''
                }]
            }
        ];

        let mockListValuesEnhanced = new ListValuesEnhanced('PAYMENT_METHODS_RATE', mockValues);
        let mockListValuesEnhancedArray: ListValuesEnhanced[] = [];
        mockListValuesEnhancedArray.push(mockListValuesEnhanced);

        const code = 200;

        return of(mockListValuesEnhancedArray).pipe(map(resp => {

            const response: BeResponse<ListValuesEnhanced[]> = new BeResponse<ListValuesEnhanced[]>(code, null, null);

            switch (response.code) {
                case 200: {
                    response.object = resp;
                    response.message = null;
                    break;
                }

                case 404: {
                    response.message = new NotificationMessage('ListValuesEnhanced non trovata', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                case 500: {
                    response.message = new NotificationMessage('Errore interno.', NOTIFICATION_TYPE.ERROR);
                    break;
                }

                default: {
                    response.message = new NotificationMessage('Errore generico', NOTIFICATION_TYPE.ERROR);
                }
            }

            return response;

        }));
    }

}
