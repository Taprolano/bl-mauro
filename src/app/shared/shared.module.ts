import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterBarComponent} from './components/filter-bar/filter-bar.component';
import {FormsModule} from '@angular/forms';
import {SvgIconComponent} from './components/svg-icon/svg-icon.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedPipesModule} from './pipes/shared-pipes.module';
import {AbstractModalComponent} from "./components/abstract-modal/abstract-modal.component";
import {ThirdPartOptionalUpdateComponent} from "../proposals/components/third-part-optional-update/third-part-optional-update.component";
import {OptionalListComponent} from "../proposals/components/optional-list/optional-list.component";
import {CookieService} from "ngx-cookie-service";
import {NotificationComponent} from './components/notification/notification.component';
import {PersistenceService} from "./services/persistence.service";
import {UserPanelComponent} from './components/user-panel/user-panel.component';
import {ScrolTestComponent} from './example/components/scrol-test/scrol-test.component';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {WorkInProgressComponent} from './components/work-in-progress/work-in-progress.component';
import {TranslateModule} from "@ngx-translate/core";
import {DynamicTableComponent} from "./components/dynamic-table/dynamic-table.component";
import {MessageModalComponent} from './components/message-modal/message-modal.component';
import {ImpersonateComponent} from './components/impersonate/impersonate.component';
import {ListUsersComponent} from './components/list-users/list-users.component';
import {EmptyListComponent} from "./components/empty-list/empty-list.component";
import {ConfirmDialogue} from './components/confirm-dialogue/confirm-dialogue.component';
import {MultiSelectComponent} from './components/multi-select/multi-select.component';
import { HeaderComponent } from './components/header/header.component';
import { CurrencyInputComponent } from './components/currency-input/currency-input.component';
import {PercentageInputComponent} from './components/percentage-input/percentage-input.component';
import {CurrencyInputDirective} from './directives/CurrencyInputDirective';

@NgModule({
    declarations: [
        FilterBarComponent,
        SvgIconComponent,
        AbstractModalComponent,
        ThirdPartOptionalUpdateComponent,
        OptionalListComponent,
        NotificationComponent,
        UserPanelComponent,
        HeaderComponent,
        ScrolTestComponent,
        WorkInProgressComponent,
        DynamicTableComponent,
        MessageModalComponent,
        ImpersonateComponent,
        ListUsersComponent,
        ConfirmDialogue,
        MultiSelectComponent,
        CurrencyInputComponent,
        PercentageInputComponent,
        CurrencyInputDirective,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedPipesModule,
        MalihuScrollbarModule.forRoot(),
        TranslateModule,
    ],
    exports: [
        HeaderComponent,
        FilterBarComponent,
        SvgIconComponent,
        SharedPipesModule,
        AbstractModalComponent,
        NotificationComponent,
        MalihuScrollbarModule,
        UserPanelComponent,
        WorkInProgressComponent,
        DynamicTableComponent,
        ConfirmDialogue,
        MultiSelectComponent,
        CurrencyInputComponent,
        CurrencyInputDirective,
        PercentageInputComponent
    ],
    entryComponents: [SvgIconComponent, ThirdPartOptionalUpdateComponent, OptionalListComponent, WorkInProgressComponent,
        DynamicTableComponent, MessageModalComponent, ImpersonateComponent, EmptyListComponent, ConfirmDialogue],
    providers: [ // singleton services
        CookieService, // TODO: verificare perche inserito come provider qui funziona
//        NotificationService,
        PersistenceService
    ]

})
export class SharedModule {
}
