import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ProposalsRoutingConstant} from "./proposalsRoutingConstant";
import {HeaderService} from "../shared/services/header.service";
import {AppSettings} from "../../AppSettings";
import {MalihuScrollbarService} from "ngx-malihu-scrollbar";

@Component({
  selector: 'app-proposals',
  templateUrl: './proposals.component.html',
  styleUrls: ['./proposals.component.scss']
})
export class ProposalsComponent implements OnInit {

    public showEffect = false;

    constructor(private _router: Router,
                private _headerService: HeaderService,
                private _mScrollbarService: MalihuScrollbarService,) {
    }

    ngOnInit() {
        this._headerService.getVariable().subscribe(() => {
            this.showEffect = !this.showEffect;
            this.toggleScroll();
        });

        this._mScrollbarService.initScrollbar(document.body, {axis: 'y', theme: AppSettings.scrollStyle});

    }

    /*
      goToTop() {
        window.scrollTo(0,0);
        let elements = document.getElementsByClassName("scrollable__element");
        for(let i = 0; i < elements.length; i++) {
          elements[i].scrollTop = 0;
          console.log("scrollable__element class:", elements[i]);
          console.log("scrollable__element id:", document.getElementById("mbz-components-scroll__id"));
        }
      }*/

    toggleScroll() {
        document.body.style.overflowY = this.showEffect ? 'hidden' : 'auto';
        if (this.showEffect) {
            this._mScrollbarService.destroy(document.body)
        } else {
            this._mScrollbarService.initScrollbar(document.body, {axis: 'y', theme: AppSettings.scrollStyle});
        }
    }

}




