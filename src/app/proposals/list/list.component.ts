import {Component, OnInit, OnDestroy} from '@angular/core';
import {IVersion} from "../../data-model/interface/IVersion";
import {IProposals} from "../../data-model/interface/IProposals";
import {IFilterChoice} from "../../data-model/interface/IFilterChoice";
import {AppSettings} from "../../../AppSettings";
import {IFilterOption} from "../../data-model/interface/IFilterOption";
import {ProposalService} from "../../shared/services/proposal.service";
import {ProposalComponent} from "../proposal/proposal.component";
import {ProposalsConstant} from "../proposalsConstant";
import {PRIMARY_OUTLET, Router} from "@angular/router";
import {BeWorkQueue} from '../services/be-work-queue.service';
import {Subscription} from 'rxjs';
import {HeaderService} from '../../shared/services/header.service';
import {ProposalWorkflowService} from "../services/proposal-workflow.service";
import {ProposalListRequestParameters} from 'src/app/data-model/class/ProposalListRequestParameters';
import {NgxSpinnerService} from "ngx-spinner";
import {IPagination} from 'src/app/data-model/interface/IPagination';
import {NotificationService} from "../../shared/components/notification/notification.service";
import {ProposalsMessages} from "../proposalsMessages";
import {UserProfileService} from "../../shared/services/user-profile.service";
import {AuthConstant} from 'src/assets/authConstant';
import {IProposalRequest} from "../../data-model/interface/IProposalRequest";
import {ListOrderKeys} from "../../data-model/interface/IListOrderKeys";
import {NotificationMessage} from "../../shared/components/notification/NotificationMessage";
import {Order} from "../../data-model/interface/EOrder";
import {UtilsService} from "../../shared/services/utils.service";
import {RequestManagerService, REQUEST_PRIORITY} from "../../shared/services/request-manager.service";
import {RequestManagerConstant} from "../requestManagerConstant";
import { LoggerService } from 'src/app/shared/services/logger.service';
import {ListTabs} from "./EListTabs";
import {CreditQueueAllRequestParameters} from "../../data-model/class/ProposalListAllRequestParameters";

const __filterPlaceholder = 'Cerca per proposta, contratto o soggetto.';
const __startPage = 0;


const __keysChoice: IFilterChoice[] = [
    {value: '0', label: 'Nuova proposta', selected: false},
    {value: 'modifyProposal', label: 'Proposta in modifica', selected: false},
    {value: 'benRequest', label: 'Richiesta benestare', selected: false},
    {value: 'failedRequest', label: 'Richiesta numerazione fallita', selected: false},

    {value: 'notAcptProp', label: 'Proposta non accettata', selected: false},
    {value: 'acptProp', label: 'Proposta accettata', selected: false},
    {value: 'acptModProp', label: 'Proposta accettata con modifiche', selected: false},
    {value: 'numRequest', label: 'Richiesta numerazione', selected: false},


    {value: 'refusedNum', label: 'Numerazione rifiutata', selected: false},
    {value: 'execNum', label: 'Numerazione eseguita', selected: false},
    {value: 'printContract', label: 'Contratto stampato', selected: false},
    {value: 'actRequest', label: 'Richiesta attivazione', selected: false},

    {value: 'execActiv', label: 'Attivazione eseguita', selected: false},
    {value: 'refusedActiv', label: 'Attivazione rifiutata', selected: false},
    {value: 'failCancRequest', label: 'Richiesta cancellazione fallita', selected: false},
    {value: 'modifyProp', label: 'Proposta in modifica', selected: false}
];
const __filterOption: IFilterOption = {
    key: 'state',
    possibleChoices: __keysChoice
};

const __pageProposals: IPagination = {
    keys: null,
    numberOfResults: null,
    recordPerPage: AppSettings.ELEMENT2PAGE,
    page: __startPage,
    nPage: 0,
    size: AppSettings.PROPOSALS2PAGE
};

/**
 * ListComponent: Componente che incapsula la pagina della lista proposte
 */

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit, OnDestroy {


    private listSubscribe: Subscription;
    proposalsList: IProposals[] = [];
    printList: IProposals[] = [];
    requestParameters: CreditQueueAllRequestParameters;

    filterParams: IProposalRequest = {
        activationDateTo: null,
        activationDateFrom: null,
        contractNumber: null,
        proposalNumber: null,
        licensePlate: null,
        frameNumber: null,
        applicant: null,
        proposalState: null
    };

    /* semaforo per bloccare eventi multipli di scrollEnd che richiedono di scaricare nuove proposte */
    sem: boolean = false;



    newProposalButtonPosition: string;
    spaceFromBar: string;

    /* dimensioni (in %) colonne tabella, per sincronizzare header fisso con accordion list */
    __widths = ["14%", "12%", "15%", "9%", "13%", "13%"];

    // /* tiene traccia dell'header selezionato per il sorting (per css) */
    // private sorted;
    // /* stringa per requestParameters */
    // private sortString;

    /**
     * @ignore
     * 'true' per usare il mock service
     */
    private httpTest = false;

    //notifica per mostrare componente "lista vuota"
    isEmptyList: boolean = false;

    //notifica per mostrare temporaneamente Work in progress se tab selezionato != 'tutte'
    showWIP: boolean = false;

    /* stringa di ordinamento corrente */
    private sortString:string = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC
    private pageProposals;


    /**@ignore Per utilizzare enum nel template*/
    ListTabs = ListTabs;
    /** Tab correntemente attiva */
    activeTab: ListTabs = ListTabs.ALL;


    constructor(
        private _proposalService: ProposalService,
        private _proposalWorkflowService: ProposalWorkflowService,
        private beProposalService: BeWorkQueue,
        private _headerService: HeaderService,
//        private _spinner: NgxSpinnerService,
        private _requestManagerService: RequestManagerService,
        private _router: Router,
        private _userProfileService: UserProfileService,
        private _notification: NotificationService,
        private _utils: UtilsService,
        private _logger: LoggerService
    ) {}

    ngOnInit() {
/*        console.log("INIT LIST")
        this._spinner.show();*/
        //assegna una copia per evitare problema di modifica della "costante" per riferimento
        this.pageProposals = this._utils.assign(__pageProposals);

        this._proposalService.clear();

        this._proposalService.updateStatus(null);
        //this._userProfileService.loadUserProfile();

        //this.variableToHeader();
        /*
                console.log('-----------------------------------------------------------');
                this._proposalService.printMemory();
                console.log('-----------------------------------------------------------');
                this._proposalService.clear();
                console.log('-----------------------------------------------------------');
                this._proposalService.printMemory();
                console.log('-----------------------------------------------------------');
        */

        this._proposalService.getMessage().subscribe(
            message => this._logger.logInfo('ListComponent','ngOnInit', "sentMessage - testMessage", message)
            // console.log("sentMessage - testMessage", message)
        );

        // this._proposalService.updateStatus(ProposalsConstant.stateProduct2Model);

        this.requestParameters = new CreditQueueAllRequestParameters();

        this.getProposalsList();
        //rimuove sottoscrizione
        window.onbeforeunload = () => this.ngOnDestroy();

    }

    /* resetta la lista di proposte alla prima pagina */
    resetPagination(){
        this.pageProposals.page = __startPage;
    }

    /* chiede al backend la lista proposte */
    getProposalsList() {

//        this._spinner.show();
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSALS_LIST, REQUEST_PRIORITY.HIGH) ;
        this._logger.logInfo('ListComponent','getProposalsList', "filterParams: ", this.filterParams);
        // console.log("[List][getProposalsList] filterParams: ", this.filterParams);
        let req = new CreditQueueAllRequestParameters();
        // req.setRequestParametersFromInterface(this.filterParams);
        this.requestParameters = req;
        this.requestParameters.page = this.pageProposals.page;
        this.requestParameters.size = this.pageProposals.size;
        // console.log("[List][getProposalsList] requestParameters: ", req);
        this.requestParameters.sort = this.sortString;
        // console.log("[List][getProposalsList] requestParameters: ", this.requestParameters);

        this.listSubscribe = this.beProposalService.getProposalsList(this.requestParameters, this.httpTest).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    /*
                                        this.proposalsList = !!response.object ? response.object : [];
                                        this.printList = this.printList.concat(this.proposalsList);
                    */
                    this.proposalsList = !!response.object ? response.object : [];
                    this._logger.logInfo('ListComponent','getProposalsList', "proposalsList: ", this.proposalsList);
                    // console.log("[List][getProposalsList] proposalsList: ", this.proposalsList);
                    if (this.pageProposals.page > __startPage) {
                        this.printList = this.printList.concat(this.proposalsList);
                    } else {
                        this.printList = this.proposalsList;
                    }

                } else {
                    this._logger.logError('ListComponent','getProposalsList', "ERRORE NON IN FALLBACK", response.code);
                    // console.error("ERRORE NON IN FALLBACK",response.code);
                }
                //spinner e semaforo
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSALS_LIST,response.code);
                    }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            }, resp => {
                this._logger.logError('ListComponent','getProposalsList', resp);
                // console.error(`[${ListComponent.name}][getProposalsList]`, resp);
                this.proposalsList = [];
                this.printList = this.proposalsList;
                let error = this._utils.getErrorResponse(resp);
                this._logger.logError('ListComponent','getProposalsList', "error", error);
                // console.error(`[${ListComponent.name}][getProposalsList] error`, error);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSALS_LIST,error.code,error.message);
                    }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            });
    }

    /* scarica la pagina successiva di proposte (attivata tramite eventEmitter su fine scroll accordion) */
    getNextProposalsList() {
//        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_MODEL_LIST, REQUEST_PRIORITY.HIGH);
        if (!this.sem) {
            // Emit the event
            // this.scrollEvent.emit();
            this.sem = true;
//            this._spinner.show();
            this._logger.logInfo('ListComponent','getNextProposalsList');
            // console.log(`[${ListComponent.name}][getNextProposalsList]`);
            this.pageProposals.page += 1;
            this.getProposalsList();
        }else{
//            this._requestManagerService.handleRequest(RequestManagerConstant.GET_MODEL_LIST, response.code)
        }
    }

    /* ngOnInit() {
         this.variableToHeader();
         this._proposalService.getMessage().subscribe(message => console.log('sentMessage - testMessage', message));

         this._proposalService.save(ProposalsConstant.stateProduct2Model, {
             cmp1: ''
         });
         console.log(this._proposalService.getObj2CurrentState());
 
 
         this.proposalsList.push({
             asset: 1,
             proposal: '100200746223',
             contract: '--',
             subject: 'Henry Richardson Henry Richardson',
             state: '0',
             date: '01-12-2020',
             deadline: '01-12-2020',
             priority: 'C',
             seller: 'Alfie Wood'
         });
 
         this.printList = this.proposalsList;
     }*/

    public get filterPlaceholder(): string {
        return __filterPlaceholder;
    }
    //
    // public get listKeys(): string[] {
    //     return LIST_KEYS;
    // }
    /* ritorna lista chiavi con prima lettera in Uppercase */
    // public get listKeysU(){
    //     let keys = LIST_KEYS;
    //     let keysU = [];
    //     keys.forEach(
    //         (value => {
    //             keysU.push(value.charAt(0).toUpperCase()+value.substring(1));
    //         })
    //     )
    //     return keysU;
    // }

    public get filterOption(): IFilterOption {
        return __filterOption;
    }


    /*goToNewProposal() {
        this._proposalWorkflowService.startNewProposal();
    }*/

    /* public variableToHeader() {
         this._headerService.setVariable(true);
     }*/

    ngAfterContentChecked() {
        if (!!window.outerHeight) {
            this.newProposalButtonPosition = (window.outerHeight / 2).toString() + "px";
        }
        this.spaceFromBar = "20px";
    }
    /* rimuove sottoscrizione alle proposte */
    ngOnDestroy() {
        this.listSubscribe.unsubscribe();
    }
    //non fa niente (al momento)
    // goToTest() {
    //     this._router.navigate(['../proposals/test/list'])
    // }
    //
    isAuthorizedToSearch() {
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_CONSULTAZIONE);
    }

    /* richiede lista aggiornata, innescata da accordion-list.component tramite eventEmitter */
    onSort(sortString:string){
        this._logger.logWarn('ListComponent', 'onSort', this.sortString);
        // console.warn(this.sortString);
//        this._spinner.show();
        this.sortString = sortString;
        this.resetPagination();
        this.getProposalsList();
    }

    /**
     * Mostra il tab selezionato
     * @param {string} tabKey
     */
    onTabKeyChange(tabKey: ListTabs): void {
        //resetta ordinamento default
        this.sortString = ListOrderKeys.DATE + ',' + Order.DESC;
        //imposta tab
        this.activeTab = tabKey;
        //per tab in sviluppo
        this.showWIP = !([ListTabs.ALL,ListTabs.MBCPOS,ListTabs.VDZ].includes(tabKey));
    }


/*    testNotification() {
        this._requestManagerService.pushNewRequest("NORMAL1", "NORMAL");
        this._requestManagerService.pushNewRequest("LOW4", "LOW");
        this._requestManagerService.pushNewRequest("HIGH1", "HIGH");
        this._requestManagerService.pushNewRequest("LOW2", "LOW");
        this._requestManagerService.pushNewRequest("HIGH2", "HIGH");
        this._requestManagerService.pushNewRequest("LOW3", "LOW");
        this._requestManagerService.pushNewRequest("LOW1", "LOW");
        this._requestManagerService.pushNewRequest("NORMAL2", "NORMAL");

        setTimeout(() => {
            this._requestManagerService.handleRequest("HIGH1", 404, null);
        }, 1000);
        setTimeout(() => {
            this._requestManagerService.handleRequest("HIGH2", 404, null);
        }, 5000);
        setTimeout(() => {
            this._requestManagerService.handleRequest("LOW1", 404, "ERRORE BE");
        }, 1500);
        setTimeout(() => {
            this._requestManagerService.handleRequest("LOW2", 404, "ERRORE BE");
        }, 500);
        setTimeout(() => {
            this._requestManagerService.handleRequest("LOW3", 404, null);
        }, 2000);
        setTimeout(() => {
            this._requestManagerService.handleRequest("LOW4", 404, "ERRORE BE");
        }, 6000);
        setTimeout(() => {
            this._requestManagerService.handleRequest("NORMAL1", 404, null);
        }, 2000);
        setTimeout(() => {
            this._requestManagerService.handleRequest("NORMAL2", 404, "ERRORE BE");
        }, 8000);
    }*/
}

