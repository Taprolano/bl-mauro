import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabVdzComponent } from './tab-vdz.component';

describe('TabVdzComponent', () => {
  let component: TabVdzComponent;
  let fixture: ComponentFixture<TabVdzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabVdzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabVdzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
