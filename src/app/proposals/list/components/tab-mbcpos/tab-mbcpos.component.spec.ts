import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabMbcposComponent } from './tab-mbcpos.component';

describe('TabMbcposComponent', () => {
  let component: TabMbcposComponent;
  let fixture: ComponentFixture<TabMbcposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabMbcposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabMbcposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
