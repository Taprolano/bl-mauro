/**
 * Tab selezionabili per la lista proposte
 */
export enum ListTabs {
    ALL="all",
    PARTIAL="partial",
    OFFER="offer",
    SIMULATION="simulation",
    MBCPOS="mbcpos",
    VDZ="vdz"
}