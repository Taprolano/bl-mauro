import {Injectable} from '@angular/core';
import {Observable, forkJoin} from "rxjs/index";
import {ISupplier} from "../../data-model/interface/ISupplier";
import {MockDataService} from "../../shared/services/mock-data.service";
import {BeResponse} from "../../data-model/class/Response";
import {IOptional} from "../../data-model/interface/IOptional";
import {IProposals} from '../../data-model/interface/IProposals';
import {PaginatedObject} from "../../data-model/class/PaginatedObject";
import {IPagination} from "../../data-model/interface/IPagination";
import {ProposalListRequestParameters} from '../../data-model/class/ProposalListRequestParameters';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Proposal} from 'src/app/data-model/class/Proposal';
import {LoggerService} from '../../shared/services/logger.service';
import {CreditQueueAllRequestParameters} from "../../data-model/class/ProposalListAllRequestParameters";
import {CreditWorkQueue} from "../../data-model/class/CreditWorkQueue";
import {CreditCorporateQueue} from 'src/app/data-model/class/CreditCorporateQueue';
import {ValueList} from '../../data-model/class/ListValues';
import {NumberProposalTab} from '../../data-model/class/NumberProposalTab';
import {NumberProposalHeader} from 'src/app/data-model/class/NumberProposalHeader';
import {PaginationData} from 'src/app/data-model/class/PaginationData';
import {DealerContactQueue} from '../../data-model/class/DealerContactQueue';
import {PayoutQueue} from '../../data-model/class/PayoutQueue';
import {ProposalConsultationRequestParameters} from "../../data-model/class/ProposalConsultationRequestParameters";
import {ConsultationQueue} from "../../data-model/class/Consultation/ConsultationQueue";
import { ConsultationSubjects } from '../../data-model/class/Consultation/Subjects/ConsultationSubjects';

@Injectable({
    providedIn: 'root'
})
export class BeWorkQueue {

    constructor(private _mockDataService: MockDataService,
                private http: HttpClient,
                private _logger: LoggerService) {
    }


    getSuppliersList(mock = false): Observable<BeResponse<ISupplier[]>> {
        if (mock)
            return this._mockDataService.getSuppliersList();
        // else{

        // }
    }

    getOptionalList(optCode: string, optDescription: string, pagination: PaginationData, mock = false): Observable<BeResponse<PaginatedObject<IOptional[]>>> {
        if (mock)
            return this._mockDataService.getOptionalList(optCode, optDescription, pagination);
        // else{

        // }
    }

    getProposalsList(params: CreditQueueAllRequestParameters, mock = false): Observable<BeResponse<IProposals[]>> {
        if (mock)
            return this._mockDataService.getProposalsList();
        else {
            return this.http.get(environment.api.proposals.proposal, {
                observe: 'response',
                params: params.requestParameters()
            })
                .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('BeWorkQueue', 'getProposalsList', resp.body);
                    //   console.log(resp.body);
                    // console.log(resp.body.body);
                    let rawlist: any = resp.body.body.content;

                    return new BeResponse<IProposals[]>(resp.status, null, Proposal.getListProposalNormalized(rawlist));
                }))
        }
    }

    /**
     * servizio per popolare coda lavoro retail
     * @param params
     * @param mock
     */
    getCreditQueueList(params: CreditQueueAllRequestParameters, mock = false): Observable<BeResponse<PaginatedObject<CreditWorkQueue[]>>> {
        // if(mock)
        //     return this._mockDataService.getCreditQueueList();
        // else{
        return this.http.get(environment.api.proposals.queue.credit, {
            observe: 'response',
            params: params.requestParameters()
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo('BeWorkQueue', 'getProposalsList', resp.body);
                //   console.log(resp.body);
                // console.log(resp.body.body);
                let rawlist: any = resp.body.body.content;
                this._logger.logInfo(BeWorkQueue.name, 'getCreditQueueList', rawlist);

                let pagData = new PaginationData(resp.body.body.totalElements, resp.body.body.totalPages, resp.body.body.pageable.pageNumber);
                let respObj = new PaginatedObject(pagData, rawlist);

                return new BeResponse<PaginatedObject<CreditWorkQueue[]>>(resp.status, null, respObj);
            }))
        // }
    }

    /**
     * servizio per popolare coda lavoro corporate
     * @param params
     * @param mock
     */
    getCreditCorporateQueueList(params: CreditQueueAllRequestParameters, mock = false): Observable<BeResponse<PaginatedObject<CreditCorporateQueue[]>>> {
        // if(mock)
        //     return this._mockDataService.getCreditCorporateQueueList();
        // else{
        return this.http.get(environment.api.proposals.queue.credit, {
            observe: 'response',
            params: params.requestParameters()
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getCreditCorporateQueueList', resp.body);
                //   console.log(resp.body);
                // console.log(resp.body.body);
                let rawlist: any = resp.body.body.content;
                this._logger.logInfo(BeWorkQueue.name, 'getCreditCorporateQueueList', rawlist);

                let pagData = new PaginationData(resp.body.body.totalElements, resp.body.body.totalPages, resp.body.body.pageable.pageNumber);
                let respObj = new PaginatedObject(pagData, rawlist);

                return new BeResponse<PaginatedObject<CreditCorporateQueue[]>>(resp.status, null, respObj);

            }))
        // }
    }

    /**
     * servizio per popolare coda lavoro corporate
     * @param params
     * @param mock
     */
    getDealerContactQueueList(params: CreditQueueAllRequestParameters): Observable<BeResponse<PaginatedObject<DealerContactQueue[]>>> {
        return this.http.get(environment.api.proposals.queue.credit, {
            observe: 'response',
            params: params.requestParameters()
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getDealerContactQueueList', resp.body);
                //   console.log(resp.body);
                // console.log(resp.body.body);
                let rawlist: any = resp.body.body.content;
                this._logger.logInfo(BeWorkQueue.name, 'getDealerContactQueueList', rawlist);

                let pagData = new PaginationData(resp.body.body.totalElements, resp.body.body.totalPages, resp.body.body.pageable.pageNumber);
                let respObj = new PaginatedObject(pagData, rawlist);

                return new BeResponse<PaginatedObject<DealerContactQueue[]>>(resp.status, null, respObj);
            }))
    }

    getPayoutQueueList(params: CreditQueueAllRequestParameters): Observable<BeResponse<PaginatedObject<PayoutQueue[]>>> {
        return this.http.get(environment.api.proposals.queue.credit, {
            observe: 'response',
            params: params.requestParameters()
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getPayoutQueueList', resp.body);
                //   console.log(resp.body);
                // console.log(resp.body.body);
                let rawlist: any = resp.body.body.content;
                this._logger.logInfo(BeWorkQueue.name, 'getPayoutQueueList', rawlist);

                let pagData = new PaginationData(resp.body.body.totalElements, resp.body.body.totalPages, resp.body.body.pageable.pageNumber);
                let respObj = new PaginatedObject(pagData, rawlist);

                return new BeResponse<PaginatedObject<PayoutQueue[]>>(resp.status, null, respObj);
            }))
    }

    /**
     * servizio per popolare determinate select
     */
    getSelectBoxValues(listCode: string): Observable<BeResponse<ValueList[]>> {
        return this.http.get(environment.api.proposals.queue.wfListOfValues, {
            observe: 'response',
            params: {listCode: listCode}
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getSelectBoxValue', resp.body);
                let rawlist: any = resp.body.body[0].values;
                this._logger.logInfo(BeWorkQueue.name, 'getSelectBoxValue', rawlist);

                return new BeResponse<ValueList[]>(resp.status, null, rawlist);
            }));
    }

    /**
     * Servizio per ricevere i contatori delle varie tab di proposte
     * @param listCode
     */
    getNumbersProposalTab(tipoCoda: string): Observable<BeResponse<NumberProposalTab>> {
        return this.http.get(environment.api.proposals.queue.numProposalTab, {
            observe: 'response',
            params: {tipoCoda: tipoCoda}
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getNumbersProposalTab', resp.body);
                let rawlist: any = resp.body.body;
                this._logger.logInfo(BeWorkQueue.name, 'getNumbersProposalTab', rawlist);

                return new BeResponse<NumberProposalTab>(resp.status, null, rawlist);
            }));
    }

    /**
     * Servizio per ricevere i contatori dellìheader per il BL
     */
    getNumbersProposalHeader(tipoCoda: string): Observable<BeResponse<NumberProposalHeader>> {
        return this.http.get(environment.api.proposals.queue.numProposalHeader, {
            observe: 'response',
            params: {tipoCoda: tipoCoda}
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getNumbersProposalTab', resp.body);
                let rawlist: any = resp.body.body.counters;
                this._logger.logInfo(BeWorkQueue.name, 'getNumbersProposalTab', rawlist);

                return new BeResponse<NumberProposalHeader>(resp.status, null, rawlist);
            }));

    }


    // todo: rimuovre la parte mockata
    mock = false;

    /**
     * Servizio per recuperare sezioni specifiche (accordion o tab) della parte di consultazione
     * @param params 
     */
    getConsultazioneProposal(params: ProposalConsultationRequestParameters): Observable<BeResponse<any>> {
        this._logger.logInfo(BeWorkQueue.name, 'getConsultazioneProposal', 'params', params);
        if (this.mock) {
            return this.http.get('/bl2/0000300033/consultaTest', {
                observe: 'response',
            })
                .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo(BeWorkQueue.name, 'getConsultazioneProposal', 'resp.body', resp.body);
                    return new BeResponse<any>(resp.status, null, resp.body);
                }));
        } else {
            return this.http.get(environment.api.proposals.queue.consultazioneProposta, {
                observe: 'response',
                params: params.requestParameters()
            })
                .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo(BeWorkQueue.name, 'getConsultazioneProposal', 'resp.body', resp.body);
                    return new BeResponse<any>(resp.status, null, resp.body.body);
                }));
        }
    }

    getConsulazioneSubjects(params: ProposalConsultationRequestParameters): Observable<BeResponse<ConsultationSubjects[]>> {
        this._logger.logInfo(BeWorkQueue.name, 'getConsulazioneSubjects', 'params', params);

        return this.http.get(environment.api.proposals.queue.consultazioneSoggetti, {
            observe: 'response',
                params: params.requestParameters()
        })
            .pipe(map((resp: HttpResponse<any>) => {
                this._logger.logInfo(BeWorkQueue.name, 'getConsulazioneSubjects', 'resp.body', resp.body);
                return new BeResponse<any>(resp.status, null, resp.body.body);
            }));
    }
}
