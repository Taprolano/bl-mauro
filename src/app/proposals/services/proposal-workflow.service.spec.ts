import {async, TestBed, inject} from '@angular/core/testing';

import {ProposalWorkflowService} from './proposal-workflow.service';
import {RouterTestingModule} from "@angular/router/testing";
import {SharedModule} from "../../shared/shared.module";
import {ProposalService} from "../../shared/services/proposal.service";
import {ProposalsModule} from "../proposals.module";
import {ProposalsRoutingConstant} from "../proposalsRoutingConstant";
import {ProposalsConstant} from "../proposalsConstant";

describe('ProposalWorkflowService', () => {

    let pws: ProposalWorkflowService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                ProposalsModule,
                RouterTestingModule],
            providers: [
                ProposalWorkflowService
            ]

        });
//        pws = TestBed.get('ProposalWorkflowService');
    }));

    it('The service ProposalWorkflowService work ', inject(
        [ProposalWorkflowService], (proposalWorkflowService) => {
            expect(proposalWorkflowService.thisServiceWork()).toBeTruthy();
        }));

    it('The founded state is correct ', inject(
        [ProposalWorkflowService], (pws) => {
            expect(pws.getStateFromPath(ProposalsRoutingConstant.path.model)
            ).toEqual(ProposalsConstant.PROPOSAL_STATE_PRODUCT_MODEL);
            expect(pws.getStateFromPath(ProposalsRoutingConstant.path.version)
            ).toEqual(ProposalsConstant.PROPOSAL_STATE_PRODUCT_VERSION);
            expect(pws.getStateFromPath(ProposalsRoutingConstant.path.commercial)
            ).toEqual(ProposalsConstant.PROPOSAL_STATE_COMMERCIAL_DATA);
        })
    );

    it('Return null for a non existing state ', inject(
        [ProposalWorkflowService], (pws) => {
            expect(pws.getStateFromPath(ProposalsRoutingConstant.path.model + "xxx")
            ).toEqual(null);
            expect(pws.getStateFromPath("xxx")
            ).toEqual(null);
            expect(pws.getStateFromPath(ProposalsRoutingConstant.path.list)
            ).toEqual(null);
        }));


});

/*

fdescribe('ProposalWorkflowService', () => {

  beforeEach( async( () => {
      TestBed.configureTestingModule({
          declarations: [
              ProposalService,
              Router,
              PlatformLocation],
          imports: []
      })
          .compileComponents();
  }));
  /!*

  beforeEach(() => TestBed.configureTestingModule({
      declarations: [
          ProposalService,
          Router,
          PlatformLocation],
      imports: []
  }));
*!/
/!*  const pws = new ProposalWorkflowService(
      _prop = new ProposalService(),
      _router = new Router,
      _location = new PlatformLocation()
  );*!/

  it('should be created', () => {
    const service: ProposalWorkflowService = TestBed.get(ProposalWorkflowService);
    expect(service).toBeTruthy();
  });

  it('should be select the correct state', () => {
/!*
    expect(pws.getStateFromPath(ProposalsRoutingConstat.path.model)
        ).toEqual(ProposalsConstant.PROPOSAL_STATE_PRODUCT_MODEL);*!/
  });
});
*/
