import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProposalsRoutingModule} from './proposals-routing.module';
import {ListComponent} from './list/list.component';
import {ProposalComponent} from './proposal/proposal.component';
import {ProductComponent} from './proposal/product/product.component';
import {ModelComponent} from './proposal/product/model/model.component';
import {VersionComponent} from './proposal/product/version/version.component';
import {CommercialComponent} from './proposal/commercial/commercial.component';
import {FinancialComponent} from './proposal/financial/financial.component';
import {RegistryComponent} from './proposal/registry/registry.component';
import {ProfilesComponent} from './proposal/registry/profiles/profiles.component';
import {DocumentsComponent} from './proposal/registry/documents/documents.component';
import {PreliminaryComponent} from './proposal/preliminary/preliminary.component';
import {PrivacyComponent} from './proposal/preliminary/privacy/privacy.component';
import {ContractComponent} from './proposal/contract/contract.component';
import {FrameComponent} from './proposal/contract/frame/frame.component';
import {PrintComponent} from './proposal/contract/print/print.component';
import {FinalizeComponent} from './proposal/finalize/finalize.component';
import {PaymentsComponent} from './proposal/finalize/payments/payments.component';
import {ProposalsComponent} from './proposals.component';
import {SharedModule} from '../shared/shared.module';
import {StepperComponent} from './components/stepper/stepper.component';
import {GoTopNewProposalComponent} from './components/go-top-new-proposal/go-top-new-proposal.component';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxCurrencyModule} from "ngx-currency";
import {ProposalService} from "../shared/services/proposal.service";
import {ProductSummaryComponent} from './components/proposal-summary/proposal-summary.component';
import {ProposalWorkflowService} from "./services/proposal-workflow.service";
import {InfiniteScrollComponent} from './components/infinite-scroll/infinite-scroll.component';
import {ProposalFilterBarComponent} from './components/proposal-filter-bar/proposal-filter-bar.component';
import {CommercialOptionalComponent} from './proposal/commercial/components/commercial-optional/commercial-optional.component';
import {CommercialThirdPartComponent} from './proposal/commercial/components/commercial-third-part/commercial-third-part.component';
import {SearchFilterComponent} from './components/search-filter/search-filter.component';
import {ProposalFooterComponent} from './components/proposal-footer/proposal-footer.component';
import {ListTestComponent} from './components/list-test/list-test.component';
import {DatepickerPopupComponent} from 'src/app/shared/components/datepicker-popup/datepicker-popup.component';
import {A2Edatetimepicker} from 'ng2-eonasdan-datetimepicker';
import {AccordionListComponent} from './components/accordion-list/accordion-list.component';
import {VersionListComponent} from './components/version-list/version-list.component';
import {EmptyListComponent} from '../shared/components/empty-list/empty-list.component';
import {TabMbcposComponent} from './list/components/tab-mbcpos/tab-mbcpos.component';
import {TabVdzComponent} from './list/components/tab-vdz/tab-vdz.component';
import {WorkQueueComponent} from './work-queue/work-queue.component';
import {CreditRetailComponent} from './work-queue/credit-retail/credit-retail.component';
import {CreditCorporateComponent} from './work-queue/credit-corporate/credit-corporate.component';
import {DealerContactComponent} from './work-queue/dealer-contact/dealer-contact.component';
import {PayoutComponent} from './work-queue/payout/payout.component';
import {CreditListAllComponent} from './work-queue/credit-retail/components/credit-list-all/credit-list-all.component';
import {CorporateListAllComponent} from './work-queue/credit-corporate/components/corporate-list-all/corporate-list-all.component';
import {WorkQueueConsultationComponent} from './work-queue-consultation/work-queue-consultation.component';
import {ConsultationHeaderComponent} from './work-queue-consultation/components/consultation-header/consultation-header.component';
import {DealerListAllComponent} from './work-queue/dealer-contact/components/dealer-list-all/dealer-list-all.component';
import {PayoutListAllComponent} from './work-queue/payout/components/payout-list-all/payout-list-all.component';
import {ConsultationLeftSectionComponent} from './work-queue-consultation/components/consultation-left-section/consultation-left-section.component';
import {ConsultationRightSectionComponent} from './work-queue-consultation/components/consultation-right-section/consultation-right-section.component';
import {ContractsListComponent} from './work-queue-consultation/components/consultation-left-section/components/contracts-list/contracts-list.component';
import {ConsultationSubjectsListComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-subjects-list/consultation-subjects-list.component';
import {ConsultationProposalComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/consultation-proposal.component';
import {ConsultationDataBanksComponent} from './work-queue-consultation/components/consultation-right-section/components/consultation-data-banks/consultation-data-banks.component';
import {ConsultationDocumentsComponent} from './work-queue-consultation/components/consultation-right-section/components/consultation-documents/consultation-documents.component';
import {ConsultationNotesComponent} from './work-queue-consultation/components/consultation-right-section/components/consultation-notes/consultation-notes.component';
import {ConfigPropServicesComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/config-prop-services.component';
import {PetitionerProposalComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/petitioner-proposal/petitioner-proposal.component';
import {ProductProposalComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/product-proposal/product-proposal.component';
import {ProposalCommercialComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/proposal-commercial/proposal-commercial.component';
import {ProposalSaleDetailsComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/proposal-sale-details/proposal-sale-details.component';
import {ExposureOutstandingComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/exposure-outstanding/exposure-outstanding.component';
import {HeaderTabsComponent} from './work-queue-consultation/components/header-tabs/header-tabs.component';
import {FinancialCampaignComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-campaign/financial-campaign.component';
import {FinancialServicesComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/financial-services.component';
import {FinancialConfigurationComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-configuration/financial-configuration.component';
import {FinancialRateComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-rate/financial-rate.component';
import {FinancialPaymentMethodComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-payment-method/financial-payment-method.component';
import {FinancialDetailsComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-details/financial-details.component';
import {RiskSharingComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/risk-sharing/risk-sharing.component';
import { RiskPhenomensComponent } from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/risk-sharing/components/risk-phenomens/risk-phenomens.component';
import { PhenomenDetailComponent } from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/risk-sharing/components/phenomen-detail/phenomen-detail.component';
import { RiskSummaryDetailsComponent } from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/risk-sharing/components/risk-summary-details/risk-summary-details.component';
import { ConsultHistoryProposalsComponent } from './work-queue/consult-history-proposals/consult-history-proposals.component';
import { ConsultAdvancedFiltersComponent } from './work-queue/consult-history-proposals/components/consult-advanced-filters/consult-advanced-filters.component';
import { ConsultFilterDateSelectComponent } from './work-queue/consult-history-proposals/components/consult-filter-date-select/consult-filter-date-select.component';
import { ConsultFilterDateTextComponent } from './work-queue/consult-history-proposals/components/consult-filter-date-text/consult-filter-date-text.component';
import { ConsultFilterTextComponent } from './work-queue/consult-history-proposals/components/consult-filter-text/consult-filter-text.component';
import { ConsultFilterWorkQueueComponent } from './work-queue/consult-history-proposals/components/consult-filter-work-queue/consult-filter-work-queue.component';
import {PaymentDetailComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-rate/subcomponents/payment-detail/payment-detail.component';
import {PaymentYearComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-rate/subcomponents/payment-year/payment-year.component';
import {PaymentMonthComponent} from './work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-rate/subcomponents/payment-month/payment-month.component';
import {FinancialInsuranceElemComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-elem/financial-insurance-elem.component";
import {FinancialInsuranceTypeComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-type/financial-insurance-type.component";
import {CommonInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/common-input/common-input.component";
import {FeelCareBaseInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-care-base-input/feel-care-base-input.component";
import {FeelLifeInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-life-input/feel-life-input.component";
import {FeelNewInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-new-input/feel-new-input.component";
import {FeelSafeInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-safe-input/feel-safe-input.component";
import {FeelStarPremiumCentroInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-star-premium-centro-input/feel-star-premium-centro-input.component";
import {FeelStarBaseLightInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-star-base-light-input/feel-star-base-light-input.component";
import {FeelStarBaseInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-star-base-input/feel-star-base-input.component";
import {FeelStarPremiumNordEstInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-star-premium-nord-est-input/feel-star-premium-nord-est-input.component";
import {FeelStarPremiumNordOvestInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-star-premium-nord-ovest-input/feel-star-premium-nord-ovest-input.component";
import {FeelStarPremiumSudInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/feel-star-premium-sud-input/feel-star-premium-sud-input.component";
import {FinanciaInsuranceDocumentComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/financial-insurance-document/financial-insurance-document.component";
import {ServicePlusAdvanceInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/service-plus-advance-input/service-plus-advance-input.component";
import {ServicePlusCompactInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/service-plus-compact-input/service-plus-compact-input.component";
import {ServicePlusConfortInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/service-plus-confort-input/service-plus-confort-input.component";
import {ServicePlusExcellentInputComponent} from "./work-queue-consultation/components/consultation-left-section/components/consultation-proposal/components/config-prop-services/components/financial-services/components/financial-insurance-input/components/service-plus-excellent-input/service-plus-excellent-input.component";

export const customCurrencyMaskConfig = {
    align: "left",
    allowNegative: false,
    allowZero: true,
    decimal: ",",
    precision: 2,
    prefix: '',
    suffix: "€",
    thousands: ".",
    nullable: true
};

@NgModule({
    declarations: [
        ListComponent,
        ProposalComponent,
        ProductComponent,
        ModelComponent,
        VersionComponent,
        CommercialComponent,
        FinancialComponent,
        RegistryComponent,
        ProfilesComponent,
        DocumentsComponent,
        PreliminaryComponent,
        PrivacyComponent,
        ContractComponent,
        FrameComponent,
        PrintComponent,
        FinalizeComponent,
        PaymentsComponent,
        ProposalsComponent,
        StepperComponent,
        GoTopNewProposalComponent,
        ProductSummaryComponent,
        InfiniteScrollComponent,
        ProposalFilterBarComponent,
        CommercialOptionalComponent,
        CommercialThirdPartComponent,
        SearchFilterComponent,
        ProposalFooterComponent,
        ListTestComponent,
        DatepickerPopupComponent,
        AccordionListComponent,
        EmptyListComponent,
        VersionListComponent,
        TabMbcposComponent,
        TabVdzComponent,
        WorkQueueComponent,
        CreditRetailComponent,
        CreditCorporateComponent,
        DealerContactComponent,
        PayoutComponent,
        CreditListAllComponent,
        CorporateListAllComponent,
        WorkQueueConsultationComponent,
        ConsultationHeaderComponent,
        ConsultationSubjectsListComponent,
        ConsultationDataBanksComponent,
        DealerListAllComponent,
        PayoutListAllComponent,
        ConsultationLeftSectionComponent,
        ConsultationRightSectionComponent,
        ContractsListComponent,
        ConsultationProposalComponent,
        ConsultationDocumentsComponent,
        ConsultationNotesComponent,
        ConfigPropServicesComponent,
        PetitionerProposalComponent,
        ProductProposalComponent,
        ProposalCommercialComponent,
        ProposalSaleDetailsComponent,
        ExposureOutstandingComponent,
        HeaderTabsComponent,
        FinancialCampaignComponent,
        FinancialServicesComponent,
        FinancialConfigurationComponent,
        FinancialRateComponent,
        FinancialPaymentMethodComponent,
        FinancialDetailsComponent,
        RiskSharingComponent,
        RiskPhenomensComponent,
        PhenomenDetailComponent,
        RiskSummaryDetailsComponent,
        ConsultHistoryProposalsComponent,
        ConsultAdvancedFiltersComponent,
        ConsultFilterDateSelectComponent,
        ConsultFilterDateTextComponent,
        ConsultFilterTextComponent,
        ConsultFilterWorkQueueComponent,
        PaymentDetailComponent,
        PaymentYearComponent,
        PaymentMonthComponent,
        FinancialInsuranceElemComponent,
        FinancialInsuranceTypeComponent,
        CommonInputComponent,
        FeelCareBaseInputComponent,
        FeelLifeInputComponent,
        FeelNewInputComponent,
        FeelSafeInputComponent,
        FeelStarBaseInputComponent,
        FeelStarBaseLightInputComponent,
        FeelStarPremiumCentroInputComponent,
        FeelStarPremiumNordEstInputComponent,
        FeelStarPremiumNordOvestInputComponent,
        FeelStarPremiumSudInputComponent,
        FinanciaInsuranceDocumentComponent,
        ServicePlusAdvanceInputComponent,
        ServicePlusCompactInputComponent,
        ServicePlusConfortInputComponent,
        ServicePlusExcellentInputComponent
        //OptionalListComponent,
        //ThirdPartOptionalUpdateComponent,
    ],
    imports: [
        CommonModule,
        ProposalsRoutingModule,
        // LayoutModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
        TranslateModule,
        A2Edatetimepicker
    ],
    exports: [
        ProposalsComponent
    ],
    providers: [ // singleton services
        ProposalService,
        // ProposalWorkflowService,
//      UserProfileService
    ]
})
export class ProposalsModule {
}
