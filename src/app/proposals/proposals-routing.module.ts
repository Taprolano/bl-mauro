import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProposalComponent} from "./proposal/proposal.component";
import {ListComponent} from "./list/list.component";
import {VersionComponent} from "./proposal/product/version/version.component";
import {ProposalsComponent} from "./proposals.component";
import {ModelComponent} from "./proposal/product/model/model.component";
import {CommercialComponent} from "./proposal/commercial/commercial.component";
import {ProposalsRoutingConstant} from "./proposalsRoutingConstant";
import { FinancialComponent } from './proposal/financial/financial.component';
import {ListTestComponent} from "./components/list-test/list-test.component";
import {WorkQueueComponent} from "./work-queue/work-queue.component";
import {CreditCorporateComponent} from "./work-queue/credit-corporate/credit-corporate.component";
import {CreditRetailComponent} from "./work-queue/credit-retail/credit-retail.component";
import {DealerContactComponent} from "./work-queue/dealer-contact/dealer-contact.component";
import {PayoutComponent} from "./work-queue/payout/payout.component";
import {BLAuthGuard} from "../shared/guard/bl-auth/bl-auth.guard";
import {QueueDispatcherGuard} from "../shared/guard/queue-dispatcher";
import {CheckQueueGuard} from "../shared/guard/check-queue";
import { WorkQueueConsultationComponent } from './work-queue-consultation/work-queue-consultation.component';
import {DirectAccessGuard} from "../shared/guard/direct-access/direct-access.guard";
import { ConsultHistoryProposalsComponent } from './work-queue/consult-history-proposals/consult-history-proposals.component';

const routes: Routes = [
    {

        path: '',
        component: ProposalsComponent,
        //TODO: nel momento in cui sarà disponibile la login e sarà spostata la chiamata di autenticazione
        //che attualmente viene chiamata nel Listcomponent
        //canActivateChild: [BdwebAuthGuard],
        children: [
            // { path: '', component: ProposalComponent },
            { path: '', component: WorkQueueComponent, canActivate: [BLAuthGuard, QueueDispatcherGuard]},
            { path: ProposalsRoutingConstant.path.workQueueCreditCorporate, component: CreditCorporateComponent, canActivate: [BLAuthGuard, CheckQueueGuard] },
            { path: ProposalsRoutingConstant.path.workQueueCreditRetail, component: CreditRetailComponent, canActivate: [BLAuthGuard, CheckQueueGuard] },
            { path: ProposalsRoutingConstant.path.workQueueDealerContact, component: DealerContactComponent, canActivate: [BLAuthGuard, CheckQueueGuard] },
            { path: ProposalsRoutingConstant.path.workQueuePayout, component: PayoutComponent, canActivate: [BLAuthGuard, CheckQueueGuard] },
            { path: ProposalsRoutingConstant.path.workQueueConsultation, component: WorkQueueConsultationComponent, canActivate: [BLAuthGuard] },
            { path: ProposalsRoutingConstant.path.workQueuePropsHistory, component: ConsultHistoryProposalsComponent, canActivate: [BLAuthGuard] },



/*            { path: ProposalsRoutingConstant.path.list, component: ListComponent, /!*canActivate: [BLAuthGuard]*!/ },
            { path: ProposalsRoutingConstant.path.model, component: ModelComponent, canActivate: [DirectAccessGuard, BLAuthGuard] },
            { path: ProposalsRoutingConstant.path.version, component: VersionComponent, canActivate: [DirectAccessGuard, BLAuthGuard]} ,
            { path: ProposalsRoutingConstant.path.commercial, component: CommercialComponent, canActivate: [DirectAccessGuard, BLAuthGuard]},
            { path: ProposalsRoutingConstant.path.financial, component: FinancialComponent, canActivate: [DirectAccessGuard, BLAuthGuard]},
            { path: 'test/list', component: ListTestComponent},*/
// { path: ProposalsRoutingConstat.path.financial, component: FinancialComponent}
]
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [BLAuthGuard, QueueDispatcherGuard, CheckQueueGuard]
})
export class ProposalsRoutingModule {}

