import {NotificationMessage, NOTIFICATION_TYPE} from "../shared/components/notification/NotificationMessage";



export class RequestManagerConstant {


//    const data = await import('../data.json')

    public static GET_IMP_USER_LIST = "GET_IMP_USER_LIST";
    public static GET_IMP_USER = "GET_IMP_USER";

    public static GET_PROPOSALS_LIST = "GET_PROPOSALS_LIST";
    public static GET_MODEL_VERSION_LIST = "GET_MODEL_VERSION_LIST";
    public static GET_MODEL_LIST = "GET_MODEL_LIST";
    public static GET_COUNTER_VALUE = "GET_COUNTER_VALUE";
    public static GET_SELECT_BOX_VALUES = "GET_SELECT_BOX_VALUES";
    public static GET_NUMBER_PROPOSAL_TAB = "GET_NUMBER_PROPOSAL_TAB";

    public static GET_PROPOSAL_CONSULTATION = "GET_PROPOSAL_CONSULTATION";
    public static GET_PROPOSAL_CONSULTATION_PROD = "GET_PROPOSAL_CONSULTATION_PROD";
    public static GET_PROPOSAL_CONSULTATION_COMM = "GET_PROPOSAL_CONSULTATION_COMM";
    public static GET_PROPOSAL_CONSULTATION_FIN = "GET_PROPOSAL_CONSULTATION_FIN";
    public static GET_PROPOSAL_CONSULTATION_HEADER = "GET_PROPOSAL_CONSULTATION_HEADER";
    public static GET_PROPOSAL_CONSULTATION_PETITIONER = "GET_PROPOSAL_CONSULTATION_PETITIONER";
    public static GET_PROPOSAL_CONSULTATION_SUBJECTS = "GET_PROPOSAL_CONSULTATION_SUBJECTS";

    public static GET_FINANCIAL_LIST = "GET_FINANCIAL_LIST";
    public static GET_FINANCIAL_SERVICE = "GET_FINANCIAL_SERVICE";
    public static GET_FINANCIAL_CONFIG_SERVICE = "GET_FINANCIAL_CONFIG_SERVICE";
    public static GET_FINANCIAL_CHECK_COMPATIBILITY = "GET_FINANCIAL_CHECK_COMPATIBILITY";


/*
    public static SERVICE_MANAGER_MESSAGE_MAP: Map<string, NotificationMessage> = new Map([
        [RequestManagerConstant.GET_PROPOSALS_LIST + "-404",
            new NotificationMessage(msg.proposalsList., NOTIFICATION_TYPE.ERROR)],
        [RequestManagerConstant.GET_PROPOSALS_LIST + "-403",
            new NotificationMessage("Non disponi delle autorizzazioni necessarie.", NOTIFICATION_TYPE.ERROR)],
        [RequestManagerConstant.GET_PROPOSALS_LIST + "-401",
            new NotificationMessage("Non disponi delle autorizzazioni necessarie.", NOTIFICATION_TYPE.ERROR)]
    ]);
*/

}