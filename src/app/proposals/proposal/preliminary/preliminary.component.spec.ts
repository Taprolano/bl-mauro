import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreliminaryComponent } from './preliminary.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {AuthGuard} from "../../../shared";
import {createTranslateLoader} from "../../../app.module";
import {ProposalsModule} from "../../proposals.module";
import {AppComponent} from "../../../app.component";
import {BrowserModule} from "@angular/platform-browser";
import {SharedModule} from "../../../shared/shared.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {MbzDashboardModule} from "../../../mbz-dashboard/mbz-dashboard.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {AppRoutingModule} from "../../../app-routing.module";
import {DirectAccessGuard} from "../../../shared/guard/direct-access/direct-access.guard";

describe('PreliminaryComponent', () => {
  let component: PreliminaryComponent;
  let fixture: ComponentFixture<PreliminaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({

        imports: [
            CommonModule,
            BrowserModule,
            BrowserAnimationsModule,
            SharedModule,
            TranslateModule,
            HttpClientModule,
            MbzDashboardModule,
            ProposalsModule,
            NgxSpinnerModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: createTranslateLoader,
                    deps: [HttpClient]
                }
            }),
            AppRoutingModule,
            ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        ],
        declarations: [AppComponent],
        providers: [AuthGuard, DirectAccessGuard],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]


    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreliminaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
