import {Component, DoCheck, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {ProposalService} from "../../../../shared/services/proposal.service";
import {ProposalsConstant} from "../../../proposalsConstant";
import {BeModelService} from './services/be-model.service';
import {Subscription} from 'rxjs';
import {IModel} from 'src/app/data-model/interface/IModel';
import {Router} from '@angular/router';
import {HeaderService} from "../../../../shared/services/header.service";
import {ProposalWorkflowService} from "../../../services/proposal-workflow.service";
import {NotificationService} from "../../../../shared/components/notification/notification.service";
import {NgxSpinnerService} from "ngx-spinner";
import {PlatformLocation} from "@angular/common";
import {environment} from "../../../../../environments/environment";
import {AppSettings} from "../../../../../AppSettings";
import {ProposalsMessages} from "../../../proposalsMessages";
import {ListComponent} from "../../../list/list.component";
import {RequestManagerService, REQUEST_PRIORITY} from "../../../../shared/services/request-manager.service";
import {RequestManagerConstant} from "../../../requestManagerConstant";
import {IModels} from "../../../../data-model/interface/IModels";
import {UtilsService} from "../../../../shared/services/utils.service";
import { LoggerService } from '../../../../shared/services/logger.service';
const not_found_path = "/assets/images/img_not_found.png";
const __models:IModels = {
    Altri: [],
    Smart: [],
    Truck: [],
    Van: [],
    Vetture: []
};


/**
 * View per selezionare il modello della proposta
 */
@Component({
    selector: 'app-model',
    templateUrl: './model.component.html',
    styleUrls: ['./model.component.scss']
})
export class ModelComponent implements OnInit, OnDestroy {

    /**
     * @ignore2
     */
    modelType = ProposalsConstant.modelEnum;

    /**@ignore*/
    public selectedType: string;

    /**@ignore*/
    public modelsList: IModel[] = [];

    /**@ignore*/
    public completeModelsList: IModels;

    /**@ignore*/
    private carsSubscription: Subscription;

    /**@ignore*/
    basePath;

    /**@ignore*/
    locationPath;

    /**@ignore*/
    private httpTest= false; //TODO settare meccanismo dinamico


    /**@ignore*/
    constructor(
        private _proposalService: ProposalService,
        private beModel: BeModelService,
        private _headerService: HeaderService,
        private _proposalWorkflowService: ProposalWorkflowService,
//        private _spinner: NgxSpinnerService,
        private _requestManagerService: RequestManagerService,
        private _location: PlatformLocation,
        private _elRef: ElementRef,
        private _notification: NotificationService,
        private _utils: UtilsService,
        private _logger: LoggerService
    ) {}

    /**
     * Inizializza componente: servizi, gestione lingua, e scarica lista iniziale
     */
    ngOnInit() {
        /*
           this._proposalService.updateStatus(ProposalsConstant.stateProduct2Model);
       */
        //this._spinner.show();

        /*    setTimeout(() => {
              /!** spinner ends after 5 seconds *!/
              this._spinner.hide();
            }, 5000);*/
        this.selectedType = this.modelType.car;
        this.basePath = environment.config.basepath.substring(0, environment.config.basepath.length - 1);
        this.locationPath = this._location.pathname;

        this.completeModelsList = this._utils.assign(__models);

        this._proposalService.clear();
        this._proposalService.updateStatus(ProposalsConstant.stateProduct2Model);
        // console.log("BEFORE INIT");
        this._proposalService.init();
        // console.log("AFTER INIT");

        //this._headerService.setVariable(false);
        this._logger.logInfo('ModelComponent', 'ngOnInit', this._proposalService.getAllDataModel());
        // console.log('[ModelComponent][_proposal]', this._proposalService.getAllDataModel());

        this.getVehiclesList();

        window.onbeforeunload = () => this.ngOnDestroy();

        /**
         * test linking states
         */
        /*    this._proposalService.states.forEach((value: string, key: string) => {
                this._proposalService.updateStatus(key);
                console.log(`TEST LINKING KEY:${key} PREV:${this._proposalService.getPrevState()} NEXT:${this._proposalService.getNextState()}`);

            });*/

    }

    /**
     * Scarica lista di veicoli e relative immagini
     */
    getVehiclesList() {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_MODEL_LIST,REQUEST_PRIORITY.HIGH);
        this.carsSubscription = this.beModel.getModelCars(/*this.selectedType,*/ this.httpTest).subscribe(response => {
            this._logger.logInfo('ModelComponent', 'getVehiclesList', "[response]", response);
            // console.log("[response]", response);

            if (response.code == AppSettings.HTTP_OK) {

                this.completeModelsList = response.object;
                this.modelsList = !this._utils.isVoid(this.completeModelsList)
                    ?this.completeModelsList[this.selectedType]:[];
                this.patchBeImgs(this.modelsList);


            }
            else {
                this._logger.logError('ModelComponent', 'getVehiclesList', "ERRORE NON IN FALLBACK");
            //  console.error("ERRORE NON IN FALLBACK");
            }
            setTimeout(() => {
                this._requestManagerService.handleRequest(RequestManagerConstant.GET_MODEL_LIST,response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            },
            resp =>{
                this._logger.logError('ModelComponent', 'getVehiclesList', resp);
                // console.error(`[${ModelComponent.name}][getVehiclesList]:`,resp);
                let error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_MODEL_LIST,error.code,error.message);
                }, AppSettings.TIMEOUT_SPINNER);
        });
    }

    /**
     *
     * @param {IModels[]} modelList
     */
    patchBeImgs(modelList:IModel[]):void {

        this._logger.logInfo('ModelComponent', 'patchBeImgs', modelList);
        // console.dir(modelList);
        modelList.forEach((elem)=>{elem['imagePath']= elem['imagePath'].replace('.jpg','.png')});
        this._logger.logInfo('ModelComponent', 'patchBeImgs', 'after mod:', modelList);
        // console.log('after mod:');
        // console.dir((modelList));

    }

    /**
     * Seleziona un tipo di veicolo
     * @param value tipo cliccato
     */
    setNavClicked(value) {
        this.selectedType = value;
        this.modelsList = !this._utils.isVoid(this.completeModelsList)
            ?this.completeModelsList[this.selectedType]:[];
        this.patchBeImgs(this.modelsList);
        // this.getVehiclesList();
    }

    /**
     * Avanza al passo successivo di creazione proposta
     */
    goNext() {
        this._proposalWorkflowService.onward()
//      this._router.navigate(['../proposals/product/model/version']);
    }

    /**
     * Avanza al passo successivo di creazione proposta con selezionato un modello
     * @param {IVersion} version modello selezionato per la proposta
     */
    selectModel(model: any) {
        // test passing data with ProposalService
        this._proposalWorkflowService.onward(model);
    }

    /**
     *  Torna al passo precedente di creazione proposta
     */
    goPrev() {
        this._proposalWorkflowService.backward()
    }

    /**
     * @ignore
     */
    ngOnDestroy() {
        this.carsSubscription.unsubscribe();
        this._proposalService.destroy();
    }

    /**
     * Ritorna path dell'immagine
     * @param {string} imgPath path di base
     * @returns {string} path dell'immagine
     */
    getImgPathWithBaseUrl(imgPath: string) {
        let path: string;
        // console.log("imgPath: "+imgPath);
        path = this.locationPath.startsWith(this.basePath)? this.basePath + imgPath : imgPath;
        // if (this.locationPath.startsWith(this.basePath)) {
        //     path = this.basePath + imgPath;
        // } else {
        //     path = imgPath;
        // }

        // console.log("path: "+path);
        return path
    }

    /**
     * Mostra un messaggio di lista vuota se non sono presenti record restituiti dal backend
     * @param {string} view
     * @returns {boolean}
     */
    showEmpty2View(view:string):boolean {
        return !this._utils.isVoid(this.completeModelsList)?this.completeModelsList[view].length<=0:true;
    }

    /**
     * Nasconde i tabs se non è presente nessuna tipologia di modello a catalogo
     * @returns {boolean}
     */
    hideTabs():boolean {
        return !(this.showEmpty2View(this.modelType.van) &&
            this.showEmpty2View(this.modelType.car) &&
            this.showEmpty2View(this.modelType.other) &&
            this.showEmpty2View(this.modelType.smart) &&
            this.showEmpty2View(this.modelType.truck))
    }

    /**
     * @ignore
     *  TODO: prendere il basepath
     */
    showPlaceholderImg(event, model: IModel): void {
        this._logger.logInfo('ModelComponent', 'showPlaceholderImg', not_found_path);
        // console.log(not_found_path);
        model.imagePath = not_found_path;
    }
}
