import { Injectable } from '@angular/core';
import { MockDataService } from '../../../../../shared/services/mock-data.service';
import { Observable } from 'rxjs';
import { BeResponse } from 'src/app/data-model/class/Response';
import { IModel } from 'src/app/data-model/interface/IModel';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {environment} from "../../../../../../environments/environment";
import {UtilsService} from "../../../../../shared/services/utils.service";
import {IVersion} from "../../../../../data-model/interface/IVersion";
import {VersionRequestParameters} from "../../../../../data-model/class/VersionRequestParameters";
import {IModels} from "../../../../../data-model/interface/IModels";
import { LoggerService } from '../../../../../shared/services/logger.service';

/**
 * Servizio per lo scaricamento dei modelli
 */
@Injectable({
  providedIn: 'root'
})
export class BeModelService {

    /**@ignore*/
    constructor(private mockDataService: MockDataService,
              private http: HttpClient,
              private _utils: UtilsService,
              private _logger: LoggerService
              ) { }

    /**
     * Richiede i modelli di un determinato tipo
     * @param {string} type tipo modello
     * @param {boolean} mock true per versione mock del service
     * @returns {Observable<BeResponse<IModel[]>>}
     */
  getModelCars(mock = false): Observable<BeResponse<IModels>> {
    if(mock) {
      return this.mockDataService.getModelCars();
    }
    else {
        return this.http.get(environment.api.proposals.models,{observe: 'response'})
            .pipe(map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('BeModelService', 'getModelCars', "RESPONSE:", resp);
                    // console.log("[BeModelService][getModelCars] RESPONSE:", resp);
                    // console.dir(resp);
                
                    let rawList  = resp.body.body;

/*
                    let modelList: IModel[] = [];
                    for( let rawModel of rawList){
                        let model = {
                            imagePath: rawModel.imagePath,
                            assetTypeDesc: rawModel.assetTypeDesc,
                            makeDesc: rawModel.makeDesc,
                            orderNumber: rawModel.orderNumber,
                            visibleAsNew: rawModel.visibleAsNew,
                            makeCode: rawModel.makeCode,
                            assetType: rawModel.assetType,
                            assetClassCode: rawModel.assetClassCode,
                            linkPromotion: !!rawModel.linkPromotion ? rawModel.linkPromotion : '',
                            hovered: false
                        }
                        modelList.push(model);
                    }
*/

                    return new BeResponse<IModels>(resp.status, null, rawList);
                })
            )
    }
  }

}
