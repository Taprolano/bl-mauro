import {ComponentFixture, TestBed} from '@angular/core/testing';
import {Injectable, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule, isPlatformBrowser} from '@angular/common';
import {BrowserModule, By} from '@angular/platform-browser';

import {ModelComponent} from './model.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '../../../../shared/shared.module';
import {ProposalsModule} from '../../../proposals.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {createTranslateLoader} from '../../../../app.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../../../../../environments/environment';
import {AuthGuard} from '../../../../shared';
import {AppRoutingModule} from '../../../../app-routing.module';
import {AppComponent} from '../../../../app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {MbzDashboardModule} from '../../../../mbz-dashboard/mbz-dashboard.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {DirectAccessGuard} from '../../../../shared/guard/direct-access/direct-access.guard';
import {MockDataService} from '../../../../shared/services/mock-data.service';
import {BeModelService} from './services/be-model.service';
import {IModel} from '../../../../data-model/interface/IModel';
import {IModels} from '../../../../data-model/interface/IModels';
import {ProposalsConstant} from '../../../proposalsConstant';
import {IProduct} from '../../../../data-model/interface/IProduct';
import {IVersion} from '../../../../data-model/interface/IVersion';
import {ProposalWorkflowService} from '../../../services/proposal-workflow.service';

@Injectable()
class MockProposalService {
}

@Injectable()
class MockBeModelService {

    constructor(private mockDataService: MockDataService) {
    }

    getModelCars() {
        return this.mockDataService.getModelCars();
    }

}

@Injectable()
class MockHeaderService {
}

@Injectable()
class MockProposalWorkflowService {

    onward(data: any = null) {
    }

    backward() {
    }

}

@Injectable()
class MockRequestManagerService {
}

@Injectable()
class MockTranslateService {
}

@Injectable()
class MockElementRef {
    // constructor() { super(undefined); }
    nativeElement = {};
}

@Injectable()
class MockNotificationService {
}

@Injectable()
class MockUtilsService {
}

describe('ModelComponent', () => {

    let component: ModelComponent;
    let fixture: ComponentFixture<ModelComponent>;


    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard,
                {provide: BeModelService, useClass: MockBeModelService},
                {provide: ProposalWorkflowService, useClass: MockProposalWorkflowService}],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
        fixture = TestBed.createComponent(ModelComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create a component', async () => {
        expect(component).toBeTruthy();
    });

    it('getVehiclesList test', async () => {
        component.getVehiclesList();
    });

    it('patchBeImgs test', async () => {

        let mockIModel: IModel = {
            imagePath: ' ',
            assetTypeDesc: ' ',
            makeDesc: ' ',
            orderNumber: 0,
            visibleAsNew: ' ',
            makeCode: ' ',
            assetType: ' ',
            assetClassCode: ' ',
            linkPromotion: ' ',
            hovered: false,
        };
        let mockIModelList: IModel[] = [];
        mockIModelList.push(mockIModel);

        component.patchBeImgs(mockIModelList);

    });

    it('setNavClicked test', async () => {

        let mockIModel: IModel = {
            imagePath: ' ',
            assetTypeDesc: ' ',
            makeDesc: ' ',
            orderNumber: 0,
            visibleAsNew: ' ',
            makeCode: ' ',
            assetType: ' ',
            assetClassCode: ' ',
            linkPromotion: ' ',
            hovered: false,
        };
        let mockIModelList: IModel[] = [];
        mockIModelList.push(mockIModel);

        let mockModels: IModels = {
            Altri: mockIModelList,
            Smart: mockIModelList,
            Truck: mockIModelList,
            Van: mockIModelList,
            Vetture: mockIModelList
        };
        component.completeModelsList = mockModels;

        spyOn(component, 'patchBeImgs').and.returnValue(false);
        component.setNavClicked('Altri');
    });

    it('goNext test', async () => {
        component.goNext();
    });

    it('selectModel test', async () => {
        component.selectModel('test');
    });

    it('goPrev test', async () => {
        component.goPrev();
    });

    it('getImgPathWithBaseUrl test', async () => {
        component.getImgPathWithBaseUrl('test');
    });

});
