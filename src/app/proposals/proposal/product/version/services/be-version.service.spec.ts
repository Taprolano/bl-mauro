import { TestBed } from '@angular/core/testing';

import { BeVersionService } from './be-version.service';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../../../../shared/shared.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {BrowserModule} from "@angular/platform-browser";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {MbzDashboardModule} from "../../../../../mbz-dashboard/mbz-dashboard.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AuthGuard} from "../../../../../shared";
import {createTranslateLoader} from "../../../../../app.module";
import {AppComponent} from "../../../../../app.component";
import {ProposalsModule} from "../../../../proposals.module";
import {AppRoutingModule} from "../../../../../app-routing.module";
import {Version} from "../../../../../data-model/class/Version";
import {DirectAccessGuard} from "../../../../../shared/guard/direct-access/direct-access.guard";

describe('BeVersionService', () => {
  beforeEach(() => TestBed.configureTestingModule({

      imports: [
          CommonModule,
          BrowserModule,
          BrowserAnimationsModule,
          SharedModule,
          TranslateModule,
          HttpClientModule,
          MbzDashboardModule,
          ProposalsModule,
          NgxSpinnerModule,
          TranslateModule.forRoot({
              loader: {
                  provide: TranslateLoader,
                  useFactory: createTranslateLoader,
                  deps: [HttpClient]
              }
          }),
          AppRoutingModule,
          ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
      ],
      declarations: [AppComponent],
      providers: [AuthGuard, DirectAccessGuard],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

  }));

  it('should be created', () => {
    const service: BeVersionService = TestBed.get(BeVersionService);
    expect(service).toBeTruthy();
  });

    it('should be normalized', () => {
        const __vers: Version = new Version('','');
        expect(__vers).toBeTruthy();
    });
});
