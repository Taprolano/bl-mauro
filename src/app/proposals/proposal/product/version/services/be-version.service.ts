import { Injectable } from '@angular/core';
import { MockDataService } from '../../../../../shared/services/mock-data.service';
import { Observable } from 'rxjs';
import { BeResponse } from 'src/app/data-model/class/Response';
import { IVersion } from 'src/app/data-model/interface/IVersion';
import {VersionRequestParameters} from "../../../../../data-model/class/VersionRequestParameters";
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../../../../environments/environment';
import { map } from 'rxjs/operators';
import {Version} from "../../../../../data-model/class/Version";
import {Normalizer} from "../../../../../data-model/class/Normalizer";
import { LoggerService } from '../../../../../shared/services/logger.service';

/**
 * Servizio per lo scaricamento della descrizione delle versioni
 */
@Injectable({
    providedIn: 'root'
})
export class BeVersionService {

    /**@ignore*/
    constructor(private mockDataService: MockDataService, private http: HttpClient,
        private _logger: LoggerService) { }

    /**
     * Richiede la descrizione delle versioni
     * @param {VersionRequestParameters} par parametri della richiesta
     * @param {boolean} mock true per usare versione mock del service
     * @returns {Observable<BeResponse<IVersion[]>>}
     */
    getCarVersions(par: VersionRequestParameters, mock = false): Observable<BeResponse<IVersion[]>> {
        if(mock) { return this.mockDataService.getCarVersions(); }
        return this.http.get(environment.api.proposals.version,
            {observe: 'response', params:par.requestParameters()}).pipe(
                map((resp: HttpResponse<any>) => {
                    this._logger.logInfo('BeVersionService', 'getCarVersions', 'RESPONSE', resp);
                    // console.log(`[${BeVersionService.name}][getCarVersions] RESPONSE:`, resp);
                    let rawList: any  = resp.body.body.content;
                    return new BeResponse<IVersion[]>(resp.status, null, Version.getListNormalizedData(rawList));
                })
            )
    }
}
