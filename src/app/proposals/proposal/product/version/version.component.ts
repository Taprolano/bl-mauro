import {AfterViewChecked, AfterViewInit, Component, DoCheck, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {IVersion} from "../../../../data-model/interface/IVersion";
import {AppSettings} from "../../../../../AppSettings";
import {IFilterChoice} from "../../../../data-model/interface/IFilterChoice";
import {IFilterOption} from "../../../../data-model/interface/IFilterOption";
import {ProposalService} from "../../../../shared/services/proposal.service";
import {ProposalsConstant} from "../../../proposalsConstant";
import {PRIMARY_OUTLET, Router} from "@angular/router";
import {Subscription} from 'rxjs';
import {BeVersionService} from './services/be-version.service';
import {ProposalWorkflowService} from "../../../services/proposal-workflow.service";
import {NotificationService} from "../../../../shared/components/notification/notification.service";
import {NOTIFICATION_TYPE, NotificationMessage} from "../../../../shared/components/notification/NotificationMessage";
import {VersionRequestParameters} from "../../../../data-model/class/VersionRequestParameters";
import {translateExpression} from "@angular/compiler-cli/src/ngtsc/transform/src/translator";
import {NgxSpinnerService} from "ngx-spinner";
import {IPagination} from "../../../../data-model/interface/IPagination";
import {InfiniteScrollComponent} from "../../../components/infinite-scroll/infinite-scroll.component";


import 'jquery';
import 'bootstrap';
import {UtilsService} from "../../../../shared/services/utils.service";
import {Version} from "../../../../data-model/class/Version";
import {ProposalsMessages} from "../../../proposalsMessages";
import {MalihuScrollbarService} from "ngx-malihu-scrollbar";
import {RequestManagerService, REQUEST_PRIORITY} from "../../../../shared/services/request-manager.service";
import {RequestManagerConstant} from "../../../requestManagerConstant";
import { LoggerService } from '../../../../shared/services/logger.service';

/** @ignore */
const __filterPlaceholder = "Cerca baumuster o per versione.";

/** @ignore */
const __keysChoice: IFilterChoice[] = [
    {value: "productCode", label: "Baumuster", selected: true},
    {value: "version", label: "Versione", selected: false},
];

/** @ignore */
const __filterOption: IFilterOption = {
    key: "fuel",
    possibleChoices: __keysChoice
};

/** @ignore */
const __keys: string[] = ["productCode", "version"];

/** @ignore */
const __initVersion = {
    productCode: null,
    version: null,
    fuel: null,
    catalog: null,
    price: null
};

/** @ignore */
const __startPage = 0;
const __record2Page = 20;


/** @ignore */
const __scrollId = '#malihu-scrollbar-versionList';

/** @ignore */
const __pageVersions: IPagination = {
    keys: null,
    numberOfResults: null,
    recordPerPage: AppSettings.ELEMENT2PAGE,
    page: __startPage,
    nPage: 0,
    size: null
};

/*[
{key:"productCode", matchRule: AppSettings.FILTER_KEY_MATCH_EXACT, possibleChoices: null},
{key:"version", matchRule: AppSettings.FILTER_KEY_MATCH_LIKE, possibleChoices: null},
{key:"example", matchRule: AppSettings.FILTER_KEY_MATCH_CHOICE, possibleChoices: __keysChoice},
];*/

//'version', 'fuel'];

/**
 * View per selezionare la versione del veicolo per la proposta
 */
@Component({
    selector: 'app-version',
    templateUrl: './version.component.html',
    styleUrls: ['./version.component.scss']
})
export class VersionComponent implements OnInit, OnDestroy {

    /** @ignore */
    private listVersionsSubscribe: Subscription;

    /** @ignore */
    versionsList: IVersion[] = [];

    /** @ignore */
    printList: IVersion[] = [];

    /** @ignore */
    requestParameters: VersionRequestParameters;

    /** @ignore */
    percentValue: number = 0;

    /** @ignore */
    pageVersions;

    /** @ignore */
    relHeight: any;

    /** @ignore */
    windowsScrollTop: any;

    /**
     * semaforo per spinner
     * @type {boolean}
     */
    sem: boolean = false;

    /** @ignore */
    onScrollCallback: Function;


    // public opt= {
    //     axis: 'y', theme: AppSettings.scrollStyle, setHeight: __height , callbacks:{
    //         onTotalScroll :function(){
    //         }}
    // };


    /**
     * dimensioni per allineare fixed header
     */
    _widths = ['18%', '31%', '13%', '18%', '20%'];


    /**
     * per usare mock service
     */
    httpTest = false;

    /**
     * notifica per mostrare componente "lista vuota"
     */
    isEmptyList: boolean = false;

    /**
     *  stringa di ordinamento corrente
     */
    private sortString: string;

    /** @ignore */
    constructor(private _proposalService: ProposalService,
                private _proposalWorkflowService: ProposalWorkflowService,
                private _beVersionService: BeVersionService,
                private _router: Router,
                private _notificationService: NotificationService,
                //                private _spinner: NgxSpinnerService,
                private _requestManagerService: RequestManagerService,
                public _wrapper: ElementRef,
                private _utils: UtilsService,
                private _logger: LoggerService) {
    }

    /**@ignore*/
    ngOnInit() {
        /*
              this._proposalService.updateStatus(ProposalsConstant.stateProduct2Version);
              this._proposalService.ngOnInit();
              this._proposalService.printMsg(" printing on init")

        */
        /*
              this._proposalService.updateStatus(ProposalsConstant.stateProduct2Version);
              this._proposalService.init();
        */

        // console.warn('ngOnInit-VersionComponent', __pageVersions, this.pageVersions);

//        this._spinner.show();

        //assegna una copia per evitare problema di modifica della "costante" per riferimento
        this.pageVersions = this._utils.assign(__pageVersions);

        this._proposalService.updateStatus(ProposalsConstant.stateProduct2Version);
        this._proposalService.init();

        this._logger.logInfo('VersionComponent', 'ngOnInit', '[_proposal]', this._proposalService.getAllDataModel());
        // console.log('[VersionComponent][_proposal]', this._proposalService.getAllDataModel());
        let product = this._proposalService.getAllDataModel().product;

        this._logger.logInfo('VersionComponent', 'ngOnInit', '[product]', product);
        // console.log('[VersionComponent][product]', product);

        if (!this._utils.isVoid(product) && !this._utils.isVoid(product.model))
            this.requestParameters = new VersionRequestParameters(
                product.model.assetClassCode,
                product.model.assetType,
                product.model.makeCode,
                this._logger
            );

        this.getVersionList();
        window.onbeforeunload = () => this.ngOnDestroy();
    }

    /**
     * Resetta la lista di versioni alla prima pagina
     */
    resetPagination() {

        if(!this._utils.isVoid(this.pageVersions))
            this.pageVersions.page = __startPage;
    }

    /**
     * Scarica lista di versioni, eventualmente ordinata tramite sortString
     * @param {string} sortString stringa di ordinamento
     */
    getVersionList() {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_MODEL_VERSION_LIST, REQUEST_PRIORITY.HIGH);

        if(!this._utils.isVoid(this.requestParameters)) {

            this.requestParameters.page = !this._utils.isVoid(this.pageVersions.page)?this.pageVersions.page:__startPage;
            this.requestParameters.size = this.pageVersions.size;
            this.requestParameters.sort = this.sortString ? this.sortString : null;

        }

        this._logger.logInfo('VersionComponent', 'getVersionList', this.requestParameters);
        // console.log('getVersionList', this.requestParameters);
        this.listVersionsSubscribe = this._beVersionService.getCarVersions(this.requestParameters, this.httpTest).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    this.versionsList = !!response.object ? response.object : [];
                    // this.printList = this.printList.concat(this.versionsList);

                    if (this.pageVersions.page > __startPage) {
                        this.printList = this.printList.concat(this.versionsList);
                    } else {
                        this.printList = this.versionsList;
                    }

                } else {
                    this._logger.logError('VersionComponent', 'getVersionList', "ERRORE NON IN FALLBACK");
                    // console.error("ERRORE NON IN FALLBACK");
                }
                //spinner
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_MODEL_VERSION_LIST, response.code)
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            }, resp => {
                let error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_MODEL_VERSION_LIST, error.code, error.message)
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            }
        )
    }

    /**
     * Incrementa la pagina corrente e richiama getVersionsList
     */
    getNextVersionList() {
        this._logger.logInfo('VersionComponent', 'getNextVersionList');
        // console.log(`[${VersionComponent.name}][getNextVersionList]`);
        // Compare the new with old and only raise the event if values change
        if (!this.sem) {
            this.sem = true;
            this._logger.logInfo('VersionComponent', 'getNextVersionList');
            // console.log(`[${VersionComponent.name}][getNextVersionList]`);
            this.pageVersions.page += 1;
            this.getVersionList();
        }
    }

    /**
     * Setta il parametro di ricerca (bottone radio)
     * @param $event evento di selezione parametri di ricerca
     */
    setSearchParams($event): void {
        if (!this._utils.isVoid($event) && !this._utils.isVoid($event.filters)) {

            if (!this._utils.isVoid($event.filters[0]) && $event.filters[0].value == 'productCode') {

                if (this.requestParameters.baumuster != $event.field)
                    this.resetPagination();

                this.requestParameters.baumuster = $event.field;
                this.requestParameters.modelDescription = null;


            } else if (!this._utils.isVoid($event.filters[0]) && $event.filters[0].value == 'version') {

                if (this.requestParameters.modelDescription != $event.field)
                    this.resetPagination();

                this.requestParameters.modelDescription = $event.field;
                this.requestParameters.baumuster = null;

            }
            this.printList = [];
            this.getVersionList();
        }
    }

    /**
     *  callback per event emitter del sort della lista versioni
     */
    onSort(sortString: string) {
        this.sortString = sortString;
        this.resetPagination();
        this.getVersionList();
    }

    /**
     * Avanza al passo successivo di creazione proposta
     * @param {IVersion} version modello selezionato per la proposta
     */
    goNext(version: IVersion) {
        this._proposalService.printMsg(" goNext");
        this._proposalWorkflowService.onward(version);
//        this._router.navigate(['../proposals/commercial', {navigationByAngular: "true"}]);
    }

    /**
     *  Torna al passo precedente di creazione proposta
     */
    goPrev() {
        this._notificationService.sendNotification(
            new NotificationMessage(`È obbligatorio consegnare al cliente, insieme al contratto, il documento
                di sintesi aggiornato. Eventuali altri documenti di sintesi consegnati in giorni precedenti a quello
                di stipula non sono da ritenersi più validi.`, NOTIFICATION_TYPE.ERROR));
        this._proposalWorkflowService.backward();
    }

    /**@ignore*/
    public get filterPlaceholder(): string {
        return __filterPlaceholder;
    }

    /**@ignore*/
    public get filterKeys(): string[] {
        return __keys;
    }

    /**@ignore*/
    public get filterOption(): IFilterOption {
        return __filterOption;
    }


    /**@ignore*/
    ngOnDestroy() {
        // console.warn('ngOnDestroy-VersionComponent');
        this.listVersionsSubscribe.unsubscribe();
        this._proposalService.destroy();

    }
}
