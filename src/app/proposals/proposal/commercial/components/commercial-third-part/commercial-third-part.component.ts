import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IThirdPartOptional} from "../../../../../data-model/interface/IThirdPartOptional";
import {FormBuilder, FormGroup} from "@angular/forms";
import {IOptional} from "../../../../../data-model/interface/IOptional";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-commercial-third-part',
    templateUrl: './commercial-third-part.component.html',
    styleUrls: ['./commercial-third-part.component.scss']
})
export class CommercialThirdPartComponent implements OnInit {

    @Input()
    isEditing = false;

    tpEditMode = false;

    tpOptionalForm: FormGroup;

    thirdPardAccordionOpen = false;

    @Input()
    thirdPartList: IThirdPartOptional[];

    @Output()
    thirdPartListChange = new EventEmitter();

    tpOptionalIndex: number;

    suppliersList;
    ivaList;

    constructor(
        private _formBuilder: FormBuilder,
        private translate: TranslateService,

    ) {
    }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.tpOptionalForm = this._formBuilder.group({
            supplier: this._formBuilder.control(null),
            iva: this._formBuilder.control(null),
            description: this._formBuilder.control(null),
            code: this._formBuilder.control(null),
            price: this._formBuilder.control(null),
            priceWithIVA: this._formBuilder.control(null),
        });
    }


    get totalTpPriceWithoutIVA() {
        let price = 0;
        for (let tp of this.thirdPartList) {
            if (tp.optional.selected) price += tp.optional.price
        }
        return price
    }

    get totalTpPriceWithIVA(): number {
        let price = 0;
        for (let tp of this.thirdPartList) {
            if (tp.optional.selected) price += tp.optional.priceWithIVA
        }

        return price;
    }


    removeTPOptional(index: number): void {
        if (index > -1) {
            this.thirdPartList.splice(index, 1);
        }
    }

    addTpOptional(): void {
//        if(this.customOptionalForm.value.description != null && this.customOptionalForm.value.price != null ){

        let priceWithIVA = this.tpOptionalForm.get('price').value * this.tpOptionalForm.get('iva').value.value;
        let newOptional: IOptional = {
            code: this.tpOptionalForm.get('code').value,
            description: this.tpOptionalForm.get('description').value,
            price: this.tpOptionalForm.get('price').value,
            priceWithIVA: priceWithIVA,
            discount: false,
            selected: true
        };
        let newTpOptional: IThirdPartOptional = {
            supplier: this.tpOptionalForm.get('supplier').value,
            iva: this.tpOptionalForm.get('iva').value,
            optional: newOptional,
        };

        this.thirdPartList.push(newTpOptional);
        this.tpOptionalForm.reset();
        //       }
    }

    printTpOptional() {
        console.log(
            this.tpOptionalForm.value.code,
            this.tpOptionalForm.value.description,
            this.tpOptionalForm.value.price,
            this.tpOptionalForm.value.supplier,
            this.tpOptionalForm.value.iva)
    }

    editTpOptional(index: number, tpOptional: IThirdPartOptional) {

        this.tpEditMode = true;

        this.tpOptionalForm = this._formBuilder.group({
            supplier: this._formBuilder.control(tpOptional.supplier),
            iva: this._formBuilder.control(tpOptional.iva),
            description: this._formBuilder.control(tpOptional.optional.description),
            code: this._formBuilder.control(tpOptional.optional.code),
            price: this._formBuilder.control(tpOptional.optional.price),
            priceWithIVA: this._formBuilder.control(tpOptional.optional.priceWithIVA)
        });

        this.tpOptionalIndex = index


        /*        var customObj = {
                    component: ThirdPartOptionalUpdateComponent,
                    parameters: {ivaList: this.ivaList, suppliersList: this.suppliersList, tpOptional: tpOptional}
                };

                this.tpUpdateSubscribe = this._abstractModalService.modalEvent.pipe(takeUntil(this.unsubscribe)).subscribe(resp =>{
                    console.log("RUN");
                    if(!!resp){
                        this.thirdPartList[index] = resp;
                        this.unsubscribe.next();
                    }
                });

                this._abstractModalService.showModal.next(customObj);*/
    }

    saveTpChange() {

        let priceWithIVA = this.tpOptionalForm.get('price').value * this.tpOptionalForm.get('iva').value.value;

        if (!!this.tpOptionalIndex) {
            let newOptional: IOptional = {
                code: this.tpOptionalForm.get('code').value,
                description: this.tpOptionalForm.get('description').value,
                price: this.tpOptionalForm.get('price').value,
                priceWithIVA: priceWithIVA,
                discount: false,
                selected: true
            };
            let newTpOptional: IThirdPartOptional = {
                supplier: this.tpOptionalForm.get('supplier').value,
                iva: this.tpOptionalForm.get('iva').value,
                optional: newOptional,
            };

            this.thirdPartList[this.tpOptionalIndex] = newTpOptional;
            this.tpOptionalForm.reset();
            this.tpEditMode = false;
        }
    }

    undoTpChange() {
        this.tpOptionalIndex = null;
        this.tpOptionalForm.reset();
        this.tpEditMode = false;
    }


}
