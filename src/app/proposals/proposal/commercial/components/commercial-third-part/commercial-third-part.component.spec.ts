import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CommercialThirdPartComponent} from './commercial-third-part.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../../../../../../environments/environment';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {AuthGuard} from '../../../../../shared';
import {AppRoutingModule} from '../../../../../app-routing.module';
import {createTranslateLoader} from '../../../../../app.module';
import {ProposalsModule} from '../../../../proposals.module';
import {AppComponent} from '../../../../../app.component';
import {BrowserModule} from '@angular/platform-browser';
import {SharedModule} from '../../../../../shared/shared.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {MbzDashboardModule} from '../../../../../mbz-dashboard/mbz-dashboard.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {DirectAccessGuard} from '../../../../../shared/guard/direct-access/direct-access.guard';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IThirdPartOptional} from '../../../../../data-model/interface/IThirdPartOptional';

describe('CommercialThirdPartComponent', () => {
    let component: CommercialThirdPartComponent;
    let fixture: ComponentFixture<CommercialThirdPartComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({

            imports: [
                FormsModule,
                ReactiveFormsModule,
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]


        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CommercialThirdPartComponent);
        component = fixture.componentInstance;

        let mockIThirdPartOptional: IThirdPartOptional = {
            optional: {
                code: ' ',
                description: ' ',
                price: 0,
                priceWithIVA: 0,
                discount: false,
                selected: true,
            },
            supplier: {
                label: ' ',
                code: ' ',
            },
            iva: {
                label: ' ',
                value: 0,
            }
        };

        let mockThPartList: IThirdPartOptional[] = [];
        mockThPartList.push(mockIThirdPartOptional);
        component.thirdPartList = mockThPartList;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            supplier: builder.control(' '),
            iva: builder.control(' ').setValue('test'),
            description: builder.control(' '),
            code: builder.control(' '),
            price: builder.control(0),
            priceWithIVA: builder.control(0),
        });
        component.tpOptionalForm = mockFormGroup;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        spyOn(component, 'createForm').and.returnValue(false);
        component.ngOnInit();
    });

    it('createForm test', () => {
        component.createForm();
    });

    it('getTotalTpPriceWithoutIVA test', () => {
        let res = component.totalTpPriceWithoutIVA;
    });

    it('getTotalTpPriceWithIVA test', () => {
        let res = component.totalTpPriceWithIVA;
    });

    it('removeTPOptional test', () => {
        component.removeTPOptional(0);
    });

    it('addTpOptional test', () => {
        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            supplier: builder.control(' '),
            iva: builder.control({test: 'test'}),
            description: builder.control(' '),
            code: builder.control(' '),
            price: builder.control(0),
            priceWithIVA: builder.control(0),
        });
        component.tpOptionalForm = mockFormGroup;
        component.addTpOptional();
    });

    it('printTpOptional test', () => {
        component.printTpOptional();
    });

    it('undoTpChange test', () => {
        component.undoTpChange();
    });

    it('editTpOptional test', () => {
        let mockOpt = component.thirdPartList;
        component.editTpOptional(0, mockOpt[0]);
    });

    it('saveTpChange test', () => {
        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            supplier: builder.control(' '),
            iva: builder.control({test: 'test'}),
            description: builder.control(' '),
            code: builder.control(' '),
            price: builder.control(0),
            priceWithIVA: builder.control(0),
        });
        component.tpOptionalForm = mockFormGroup;

        component.saveTpChange();
    });

});
