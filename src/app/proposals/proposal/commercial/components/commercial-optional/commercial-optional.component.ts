import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IOptional} from '../../../../../data-model/interface/IOptional';
import {Form, FormBuilder, FormGroup} from '@angular/forms';
import {BeWorkQueue} from '../../../../services/be-work-queue.service';
import {UtilsService} from '../../../../../shared/services/utils.service';
import {LoggerService} from '../../../../../shared/services/logger.service';
import {PaginationData} from '../../../../../data-model/class/PaginationData';

@Component({
    selector: 'app-commercial-optional',
    templateUrl: './commercial-optional.component.html',
    styleUrls: ['./commercial-optional.component.scss']
})
export class CommercialOptionalComponent implements OnInit {

    @Input()
    isEditing = false;

    @Input()
    optionalList: IOptional[];
    @Output()
    optionalListChange = new EventEmitter();

    @Input()
    iva: number;

    customOptionalForm: FormGroup;
    searchOptionalForm: FormGroup;
    foundOptionalList: IOptional[];

    accordionOpen = false;
    newOptional = false;
    search = false;


    pagination: PaginationData = new PaginationData(null, 1, 0, 8, null);

    constructor(
        private _formBuilder: FormBuilder,
        private _beProposal: BeWorkQueue,
        private _util: UtilsService,
        private _logger: LoggerService
    ) {
    }

    ngOnInit() {

        this.createForms();

    }


    createForms() {
        this.customOptionalForm = this._formBuilder.group({
            description: this._formBuilder.control(null),
            price: this._formBuilder.control(null),
            discount: this._formBuilder.control(false),
        });
        this.searchOptionalForm = this._formBuilder.group({
            description: this._formBuilder.control(null),
            code: this._formBuilder.control(null),
        });
    }

    priceWithIVA(price: number): number {
        return price * (1 + this.iva);
    }

    addOptional(optional = null): void {
        this._logger.logInfo('CommercialOptionalComponent', 'addOptional', 'INIT DISCOUNT' + this.customOptionalForm.get('discount').value);
        //   console.log("[CommercialOptionalComponent][addOptional] INIT DISCOUNT"+this.customOptionalForm.get('discount').value);

        if (!this._util.isVoid(optional)) {
            this.optionalList.push(optional);
            this.searchOptionalForm.reset();

        }

        if (this._util.isVoid(optional) && this.customOptionalForm.value.description != null && this.customOptionalForm.value.price != null) {

            const newOptional: IOptional = {
                code: '[N.D.]',
                description: this.customOptionalForm.get('description').value,
                price: this.customOptionalForm.get('price').value,
                priceWithIVA: this.priceWithIVA(this.customOptionalForm.get('price').value),
                discount: this.customOptionalForm.get('discount').value,
                selected: true
            };
            this._logger.logInfo('CommercialOptionalComponent', 'addOptional', 'newOptional: ' + newOptional.price.toString());
            //   console.log("newOptional: "+newOptional.price.toString());
            this.optionalList.push(newOptional);
            this.customOptionalForm.reset();
        }

        this._logger.logInfo('CommercialOptionalComponent', 'addOptional', 'FINAL DISCOUNT' + this.customOptionalForm.get('discount').value);
        //   console.log("[CommercialOptionalComponent][addOptional] FINAL DISCOUNT"+this.customOptionalForm.get('discount').value);

    }

    removeOptional(index: number): void {
        if (index > -1) {
            this.optionalList.splice(index, 1);
        }
    }

    findOptionalList() {
        this.pagination.currentPage = 1;
        this.foundOptionalList = [];
        this._beProposal.getOptionalList(this.searchOptionalForm.value.code, this.searchOptionalForm.value.description, this.pagination, true).subscribe(response => {
            if (response.code == 200) {
                for (const optional of response.object.object) {
                    this.foundOptionalList.push(optional);
                }
                // = response.object.object;
            } else {
                // toDo Gestione Errore
                this._logger.logError('CommercialOptionalComponent', 'findOptionalList', 'ERRORE: ' + response.message);
                //   console.log("ERRORE: "+response.message );

            }
        });
    }

    findNextOptionalList() {
        this.pagination.currentPage++;
        this.findOptionalList();
    }

    searchOptional() {
        this.search = true;
        this.foundOptionalList = [];
        this.findOptionalList();
        //
        // let customObj = {
        //     component: OptionalListComponent,
        //     parameters: {code: this.searchOptionalForm.get('code').value, description: this.searchOptionalForm.get('description').value}
        // };
        //
        // let onSubmit = true;
        //
        // this._abstractModalService.modalEvent.pipe(takeUntil(this.unsubscribe)).subscribe(resp =>{
        //     if(!onSubmit){
        //         this.optionalList.push(resp);
        //         resp = null;
        //         this.unsubscribe.next();
        //     }else{
        //         onSubmit=false;
        //     }
        // });
        //
        // console.log(this._abstractModalService);
        // this._abstractModalService.showModal.next(customObj);
    }

}
