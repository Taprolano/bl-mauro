import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CommercialOptionalComponent} from './commercial-optional.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../../../../../../environments/environment';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {AuthGuard} from '../../../../../shared';
import {createTranslateLoader} from '../../../../../app.module';
import {ProposalsModule} from '../../../../proposals.module';
import {AppComponent} from '../../../../../app.component';
import {BrowserModule} from '@angular/platform-browser';
import {SharedModule} from '../../../../../shared/shared.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {MbzDashboardModule} from '../../../../../mbz-dashboard/mbz-dashboard.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AppRoutingModule} from '../../../../../app-routing.module';
import {DirectAccessGuard} from '../../../../../shared/guard/direct-access/direct-access.guard';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IOptional} from '../../../../../data-model/interface/IOptional';
import {PaginationData} from '../../../../../data-model/class/PaginationData';

describe('CommercialOptionalComponent', () => {
    let component: CommercialOptionalComponent;
    let fixture: ComponentFixture<CommercialOptionalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({

            imports: [
                FormsModule,
                ReactiveFormsModule,
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]

        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CommercialOptionalComponent);
        component = fixture.componentInstance;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            description: builder.control('test'),
            price: builder.control(0),
            discount: builder.control(false),
        });
        component.customOptionalForm = mockFormGroup;

        mockFormGroup = builder.group({
            description: builder.control('test'),
            code: builder.control(0)
        });
        component.searchOptionalForm = mockFormGroup;

        component.iva = 0;

        let mockOptional: IOptional = {
            code: ' ',
            description: ' ',
            price: 0,
            priceWithIVA: 0,
            discount: false,
            selected: true
        };
        let mockOptList: IOptional[] = [];
        mockOptList.push(mockOptional);
        component.optionalList = mockOptList;

        let mockPagdata: PaginationData = new PaginationData(1, 2, 1, 1, 0);
        component.pagination = mockPagdata;

        component.search = true;
        component.foundOptionalList = [];

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        spyOn(component, 'ngOnInit').and.returnValue(false);
        component.ngOnInit();
    });

    it('createForms test', () => {
        component.createForms();
    });

    it('priceWithIVA test', () => {
        component.priceWithIVA(0);
    });

    it('addOptional test', () => {

        let mockOptional: IOptional = {
            code: ' ',
            description: ' ',
            price: 0,
            priceWithIVA: 0,
            discount: false,
            selected: true
        };

        component.addOptional(mockOptional);
    });

    it('removeOptional test', () => {
        component.removeOptional(0);
    });

    it('findOptionalList test', () => {
        component.findOptionalList();
    });

    it('findNextOptionalList test', () => {
        spyOn(component, 'findOptionalList').and.returnValue(false);
        component.findNextOptionalList();
    });

    it('searchOptional test', () => {
        spyOn(component, 'findOptionalList').and.returnValue(false);
        component.searchOptional();
    });

});
