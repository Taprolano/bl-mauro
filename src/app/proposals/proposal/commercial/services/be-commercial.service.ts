import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BeResponse } from 'src/app/data-model/class/Response';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { CommercialAmountsReqParams } from '../../../../data-model/class/CommercialAmountsReqParams';
import { map } from 'rxjs/operators';
import { LoggerService } from '../../../../shared/services/logger.service';
import { CommercialAmounts, CommercialAmountsFactory } from 'src/app/data-model/class/CommercialAmounts';
import { AssetOptionalsReqParams } from '../../../../data-model/class/AssetOptionalsReqParams';
import { AssetOptionals, AssetOptionalsFactory } from 'src/app/data-model/class/AssetOptionals';
import { AssetsChassisReqParams } from '../../../../data-model/class/AssetsChassisReqParams';
import { AssetsChassis, AssetsChassisFactory } from 'src/app/data-model/class/AssetsChassis';

@Injectable({
  providedIn: 'root'
})
export class BeCommercialService {

  constructor(private http: HttpClient, private _logger: LoggerService) { }

  /**
   * 
   * @param params
   * API che restituisce gli importi commerciali dato un baumuster e il flag nuovo/usato
   */
  getCommercialAmounts(params: CommercialAmountsReqParams): Observable<BeResponse<CommercialAmounts>> {
    return this.http.get(environment.api.commercial.amounts, {observe: 'response', params: params.requestParameters()})
    .pipe(map((resp: HttpResponse<any>) => {

      let factory = new CommercialAmountsFactory();
      this._logger.logInfo(BeCommercialService.name, 'getCommercialAmounts', resp.body)
      let rawlist: any = resp.body.body;

      return new BeResponse<any>(resp.status, null, factory.getInstance(rawlist));

    }));
  }

  /**
   * 
   * @param params 
   * API che restituisce gli optional dato un baumuster
   */
  getAssetsOptionals(params: AssetOptionalsReqParams): Observable<BeResponse<AssetOptionals>> {
    return this.http.get(environment.api.commercial.optionals, {observe: 'response', params: params.requestParameters()})
    .pipe(map((resp: HttpResponse<any>) => {

      let factory = new AssetOptionalsFactory();
      this._logger.logInfo(BeCommercialService.name, 'getAssetsOptionals', resp.body)
      let rawlist: any = resp.body.body;

      return new BeResponse<any>(resp.status, null, factory.getInstance(rawlist));

    }));
  }

  /**
   * 
   * @param params
   * API che effettua il check telaio dato telaio, baumuster e makecode
   */
  getAssetsChassis(params: AssetsChassisReqParams): Observable<BeResponse<AssetsChassis>> {
    return this.http.get(environment.api.commercial.chassis, {observe: 'response', params: params.requestParameters()})
    .pipe(map((resp: HttpResponse<any>) => {
      let factory = new AssetsChassisFactory();
      this._logger.logInfo(BeCommercialService.name, 'getAssetsOptionals', resp.body)
      let rawlist: any = resp.body.body;

      return new BeResponse<any>(resp.status, null, factory.getInstance(rawlist));
    }))
  }

}
