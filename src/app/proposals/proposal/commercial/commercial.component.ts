import {Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {IOptional} from '../../../data-model/interface/IOptional';
import {FormBuilder, FormGroup} from '@angular/forms';
import {IThirdPartOptional} from '../../../data-model/interface/IThirdPartOptional';
import {ISupplier} from '../../../data-model/interface/ISupplier';
import {IIva} from '../../../data-model/interface/IIva';
import {ProposalsConstant} from '../../proposalsConstant';
import {ProposalService} from '../../../shared/services/proposal.service';
import {AbstractModalComponent} from '../../../shared/components/abstract-modal/abstract-modal.component';
import {AbstractModalService} from '../../../shared/components/abstract-modal/abstract-modal.service';
import {SvgIconComponent} from '../../../shared/components/svg-icon/svg-icon.component';

import {Subject, Subscription} from 'rxjs/index';
import {BeWorkQueue} from '../../services/be-work-queue.service';
import {ThirdPartOptionalUpdateComponent} from '../../components/third-part-optional-update/third-part-optional-update.component';
import {takeUntil, takeWhile} from 'rxjs/internal/operators';
import {OptionalListComponent} from '../../components/optional-list/optional-list.component';
import {Router} from '@angular/router';
import {UtilsService} from '../../../shared/services/utils.service';
import {ICommandName} from 'selenium-webdriver';
import {ICommercial, ISecondhandData} from '../../../data-model/interface/ICommercial';
import {ProposalWorkflowService} from '../../services/proposal-workflow.service';
import {IPagination} from '../../../data-model/interface/IPagination';
import {DealerService} from 'src/app/shared/services/dealer.service';
import {map, flatMap} from 'rxjs/operators';
import {AuthConstant} from 'src/assets/authConstant';
import {UserProfileService} from 'src/app/shared/services/user-profile.service';
import {IProduct} from '../../../data-model/interface/IProduct';
import { LoggerService } from '../../../shared/services/logger.service';

const __priceConst = 42000.80;
const __optionalConst = 24000.80;

const __months = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Novembre', 'Dicembre'];
const __years = [2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2012, 2011, 2010, 2009];

@Component({
    selector: 'app-commercial',
    templateUrl: './commercial.component.html',
    styleUrls: ['./commercial.component.scss']

})
export class CommercialComponent implements OnInit, OnDestroy, DoCheck {


    secondHandCondition = false;
    tpEditMode = false;

    supplierSubscribe: Subscription;
    unsubscribe = new Subject<any>();
    search = false;

    commercialDataForm: FormGroup;

/*
    tpOptionalForm: FormGroup;
*/
    secondhandProductForm: FormGroup;

    thirdPardAccordionOpen = false;

    isNew = true;

    productInfo: IProduct;

/*
    optionalAccordionOpen = true;
    customOptionalForm: FormGroup;
    searchOptionalForm: FormGroup;
    foundOptionalList: IOptional[] = [];
    tpUpdateSubscribe: Subscription;
    newOptional = true;
*/

    discount = 0;

    ivaList: IIva[] = [
        {label: 'IVA 22% vendite', value: 0.22},
        {label: 'IVA 10% vendite', value: 0.1},
    ];

    suppliersList: ISupplier[] = [];

    selectedOptionalList: IOptional[] = [];
    thirdPartList: IThirdPartOptional[] = [];

    tpOptionalIndex: number;


    pagination: IPagination = {keys: null, numberOfResults: null, recordPerPage: 8, page: 1, nPage: 0, size: null};
    authorizations: string[];

    constructor(private _formBuilder: FormBuilder,
                private _proposalService: ProposalService,
                private _beProposal: BeWorkQueue,
                private _proposalWorkflowService: ProposalWorkflowService,
                private _abstractModalService: AbstractModalService,
                private _utils: UtilsService,
                private _dealerService: DealerService,
                private _userProfileService: UserProfileService,
                private _logger: LoggerService) {

    }

    /*findOptionalList(){
       this._beProposal.getOptionalList(this.searchOptionalForm.value.code,this.searchOptionalForm.value.description, this.pagination, true).subscribe(response =>{
            if (response.code == 200) {
                for(let optional of response.object.object){
                    this.foundOptionalList.push(optional);
                }
                //= response.object.object;
            } else {
                //toDo Gestione Errore
                console.log("ERRORE: "+response.message );

            }
        })
    }*/


    ngOnInit() {
//        this._proposalService.updateStatus(ProposalsConstant.stateCommercial);


        this._proposalService.init();
        this._logger.logInfo('CommercialComponent', 'ngOnInit', this._proposalService.getAllDataModel());
        // console.log('[CommercialComponent][_proposal]', this._proposalService.getAllDataModel());

        this.productInfo = this._proposalService.getAllDataModel().product;

        this._proposalWorkflowService.onwardSub.subscribe(() => this.save());

        this.supplierSubscribe = this._beProposal.getSuppliersList(true).subscribe(
            response => {
                this._logger.logInfo('CommercialComponent', 'ngOnInit', 'response', response);
                // console.log("[CommercilComponent][OnInit] response", response);

                if (response.code == 200) {
                    this.suppliersList = response.object;
                } else {
                    // toDo Gestione Errore
                }
            }
        );

        this.thirdPartList.push({
                optional: {
                    code: '123',
                    description: 'abc',
                    price: 100,
                    priceWithIVA: 120,
                    discount: false,
                    selected: true
                },
                supplier: {
                    label: 'TEST',
                    code: '12345678'
                },
                iva: {
                    label: '20%',
                    value: 0.2
                }
            }
        );
        if (!this.isAuthorizedToOtherSuppliers()) {
            document.getElementById('fornitore').setAttribute('disabled', '');
        }
        this.createForm();
        if (!this.authorizations) {
            return this._dealerService.getDealerInfo(true).pipe(flatMap((response) => {
                const dealer = response.object;
                return this._dealerService.getDealershipDetails(true, dealer.id, dealer.dealershipSelected)
                    .pipe(map((response) => {
                        this.authorizations = response.object.dealerAuth;
                    }));
            })).subscribe(() => {

            });
        }
        window.onbeforeunload = () => this.ngOnDestroy();
    }


    ngOnDestroy() {
        if (!!this.supplierSubscribe) { this.supplierSubscribe.unsubscribe(); }
        this._proposalService.destroy();
    }

    createForm() {

        // let supplierCode = !!this.suppliersList[0] ? this.suppliersList[0].code : null;
        // let ivaValue = !!this.ivaList[0] ? this.ivaList[0].value : null;

        this._logger.logInfo('CommercialComponent', 'createForm', 'getObj2CurrentState', this._proposalService.getObj2CurrentState());
        // console.log('[createForm] getObj2CurrentState', this._proposalService.getObj2CurrentState());

        const ipt = this._utils.isVoid(this._proposalService.getObj2CurrentState()) ? 0 : this._proposalService.getObj2CurrentState();

        this._logger.logInfo('CommercialComponent', 'createForm', 'ipt value: ' + ipt + ' type: ' + typeof ipt);
        // console.log("ipt value: " + ipt + " type: " + typeof ipt);
        this.commercialDataForm = this._formBuilder.group({
            price: this._formBuilder.control(this.productInfo.version.price),
            entry: this._formBuilder.control(0),
            supplier: this._formBuilder.control(this.suppliersList[0]),
            iva: this._formBuilder.control(this.ivaList[0]),
            ipt: this._formBuilder.control(ipt),
            marketValue: this._formBuilder.control(0),
            exemptAmount: this._formBuilder.control(0)
        });

        this.secondhandProductForm = this._formBuilder.group({
            licensePlate: this._formBuilder.control(null),
            km: this._formBuilder.control(null),
            registrationYear: this._formBuilder.control(null),
            registrationMonth: this._formBuilder.control(null),
            assetConfirm: this._formBuilder.control(false),
        });

    }

    ngDoCheck(): void {

        /*
                if(this._proposalService.getCurrentState()!==ProposalsConstant.stateCommercial) {
                    this._proposalService.updateStatus(ProposalsConstant.stateCommercial);
                }
        */
    }

    changeType(type: boolean) {

        if (this.isNew != type) {
            this.isNew = type;

            if (!!this.secondhandProductForm) { this.secondhandProductForm.reset(); }


            if (!!this.commercialDataForm) { this.commercialDataForm.patchValue({
                entry: 0,
                ipt: 0,
                marketValue: 0,
                exemptAmount: 0
            });
            }


            /*
                        this.commercialDataForm.value.entry = 0;
                        this.commercialDataForm.value.ipt = 0;
                        this.commercialDataForm.value.marketValue = 0;
                        this.commercialDataForm.value.exemptAmount = 0;
            */

        }

    }

    get partialAmmount(): number {

        const partialAmmount =
            this.commercialDataForm.get('price').value +
            this.totalTpPriceWithoutIVA +
            this.commercialDataForm.get('entry').value +
            this.commercialDataForm.get('ipt').value;

        return partialAmmount;
    }

    get iva(): number {
        const iva = (
            this.partialAmmount - this.commercialDataForm.get('ipt').value
        ) * this.commercialDataForm.get('iva').value.value;
        const tpIva = this.totalTpPriceWithIVA - this.totalTpPriceWithoutIVA;
        return iva + tpIva;
    }


    get ivaValue(): number {
        return this.commercialDataForm.get('iva').value.value;
    }

    get totalAmmount(): number {
        const totalAmmount: number = +this.partialAmmount + this.iva - this.discount;

        return totalAmmount;
    }

    get totalAmmountWithoutDiscount(): number {
        const totalAmmount: number = +this.partialAmmount + this.iva;

        return totalAmmount;
    }


    get totalTpPriceWithoutIVA() {
        let price = 0;
        for (const tp of this.thirdPartList) {
            if (tp.optional.selected) { price += tp.optional.price; }
        }
        return price;
    }

    get totalTpPriceWithIVA(): number {
        let price = 0;
        for (const tp of this.thirdPartList) {
            if (tp.optional.selected) { price += tp.optional.priceWithIVA; }
        }

        return price;
    }

    get selectedOptionalPrice(): number {
        let price = 0;
        for (const optional of this.selectedOptionalList) {
            if (optional.selected) { price += optional.price; }
        }
        return price;
    }

    /*get selectedOptionalPrice() : number{
        let price = 0;
        for ( let optional of this.selectedOptionalList){
            if(optional.selected) price += optional.price
        }
        return price
    }*/

    get tpPriceWithIVA(): number {
/*
        let iva = !!this.tpOptionalForm.value.iva
            ? this.tpOptionalForm.value.iva.value
            : 0;
        let price = !!this.tpOptionalForm.value.price
            ? this.tpOptionalForm.value.price
            : 0;
*/
        return 10 * (1 );
    }

    get tpIVA() {
        const iva = 0;
        const price = 0;
        return price * (iva);
    }

/*
        removeOptional(index: number): void {
            if (index > -1) {
                this.selectedOptionalList.splice(index, 1);
            }
        }

    removeTPOptional(index: number): void {
        if (index > -1) {
            this.thirdPartList.splice(index, 1);
        }
    }

        addOptional(): void{
            if(this.customOptionalForm.value.description != null && this.customOptionalForm.value.price != null ){

                let priceWithIVA = this.customOptionalForm.get('price').value*1.22;

                let newOptional: IOptional = {
                    code: "[N.D.]",
                    description: this.customOptionalForm.get('description').value,
                    price: this.customOptionalForm.get('price').value,
                    priceWithIVA: priceWithIVA,
                    discount: this.customOptionalForm.get('discount').value,
                    selected: true
                };

                console.log("newOptional: "+newOptional.price.toString());
                this.selectedOptionalList.push(newOptional);
                this.customOptionalForm.reset();
            }
        }


    addTpOptional(): void {
//        if(this.customOptionalForm.value.description != null && this.customOptionalForm.value.price != null ){

        let newOptional: IOptional = {
            code: this.tpOptionalForm.get('code').value,
            description: this.tpOptionalForm.get('description').value,
            price: this.tpOptionalForm.get('price').value,
            priceWithIVA: this.tpPriceWithIVA,
            discount: false,
            selected: true
        };
        let newTpOptional: IThirdPartOptional = {
            supplier: this.tpOptionalForm.get('supplier').value,
            iva: this.tpOptionalForm.get('iva').value,
            optional: newOptional,
        };

        this.thirdPartList.push(newTpOptional);
        this.tpOptionalForm.reset();
        //       }
    }

        searchOptional(){

            this.search = true;
            this.foundOptionalList = [];
            this.findOptionalList();
            //
            // let customObj = {
            //     component: OptionalListComponent,
            //     parameters: {code: this.searchOptionalForm.get('code').value, description: this.searchOptionalForm.get('description').value}
            // };
            //
            // let onSubmit = true;
            //
            // this._abstractModalService.modalEvent.pipe(takeUntil(this.unsubscribe)).subscribe(resp =>{
            //     if(!onSubmit){
            //         this.selectedOptionalList.push(resp);
            //         resp = null;
            //         this.unsubscribe.next();
            //     }else{
            //         onSubmit=false;
            //     }
            // });
            //
            // console.log(this._abstractModalService);
            // this._abstractModalService.showModal.next(customObj);

        }

    printTpOptional() {
        this._logger.logInfo(
            'CommercialComponent', 'printTpOptional', this.tpOptionalForm.value.code,
            this.tpOptionalForm.value.description,
            this.tpOptionalForm.value.price,
            this.tpOptionalForm.value.supplier,
            this.tpOptionalForm.value.iva)
        // console.log(
        //     this.tpOptionalForm.value.code,
        //     this.tpOptionalForm.value.description,
        //     this.tpOptionalForm.value.price,
        //     this.tpOptionalForm.value.supplier,
        //     this.tpOptionalForm.value.iva)
    }

    editTpOptional(index: number, tpOptional: IThirdPartOptional) {

        this.tpEditMode = true;

        this.tpOptionalForm = this._formBuilder.group({
            supplier: this._formBuilder.control(tpOptional.supplier),
            iva: this._formBuilder.control(tpOptional.iva),
            description: this._formBuilder.control(tpOptional.optional.description),
            code: this._formBuilder.control(tpOptional.optional.code),
            price: this._formBuilder.control(tpOptional.optional.price),
            priceWithIVA: this._formBuilder.control(tpOptional.optional.priceWithIVA)
        });

        this.tpOptionalIndex = index


        /!*        var customObj = {
                    component: ThirdPartOptionalUpdateComponent,
                    parameters: {ivaList: this.ivaList, suppliersList: this.suppliersList, tpOptional: tpOptional}
                };

                this.tpUpdateSubscribe = this._abstractModalService.modalEvent.pipe(takeUntil(this.unsubscribe)).subscribe(resp =>{
                    console.log("RUN");
                    if(!!resp){
                        this.thirdPartList[index] = resp;
                        this.unsubscribe.next();
                    }
                });

                this._abstractModalService.showModal.next(customObj);*!/
    }

    saveTpChange() {
        if (!!this.tpOptionalIndex) {
            let newOptional: IOptional = {
                code: this.tpOptionalForm.get('code').value,
                description: this.tpOptionalForm.get('description').value,
                price: this.tpOptionalForm.get('price').value,
                priceWithIVA: this.tpPriceWithIVA,
                discount: false,
                selected: true
            };
            let newTpOptional: IThirdPartOptional = {
                supplier: this.tpOptionalForm.get('supplier').value,
                iva: this.tpOptionalForm.get('iva').value,
                optional: newOptional,
            };

            this.thirdPartList[this.tpOptionalIndex] = newTpOptional;
            this.tpOptionalForm.reset();
            this.tpEditMode = false;
        }
    }

    undoTpChange() {
        this.tpOptionalIndex = null;
        this.tpOptionalForm.reset();
        this.tpEditMode = false;
    }
*/

    goNext() {
//        this._router.navigate(['../proposals/financial']);
    }

    goPrev() {
        this._proposalWorkflowService.backward();
    }

    get months() {
        return __months;
    }

    get years() {
        return __years;
    }

    save() {
        const commercilaData = this.createDataModel();
        this._logger.logInfo('CommercialComponent', 'save', 'commercilaData', commercilaData);
        // console.log("commercilaData", commercilaData);

        this._proposalService.save(ProposalsConstant.stateCommercial, this.commercialDataForm.value.ipt);
        this._logger.logInfo('CommercialComponent', 'save', 'saving', this.commercialDataForm.value.ipt, this._proposalService.getObj2CurrentState(), this._proposalService.getCurrentState());
        // console.log("saving", this.commercialDataForm.value.ipt, this._proposalService.getObj2CurrentState(), this._proposalService.getCurrentState());
        this._proposalWorkflowService.onward(commercilaData);
    }

    createDataModel(): ICommercial {

        let secondhandData: ISecondhandData;

        if (this.isNew) {
            secondhandData = null;
        } else {
            secondhandData = {
                licensePlate: this.secondhandProductForm.get('licensePlate').value.toUpperCase(),
                km: this.secondhandProductForm.get('km').value,
                registrationYear: this.secondhandProductForm.get('registrationYear').value,
                registrationMonth: this.secondhandProductForm.get('registrationMonth').value,
            };
        }

        const commercilaData: ICommercial = null;
        //     {
        //     price: this.commercialDataForm.get('price').value,
        //     entry: this.commercialDataForm.get('entry').value,
        //     supplier: this.commercialDataForm.get('supplier').value,
        //     iva: this.commercialDataForm.get('iva').value,
        //     ipt: this.commercialDataForm.get('ipt').value,
        //     marketValue: this.commercialDataForm.get('marketValue').value,
        //     exemptAmount: this.commercialDataForm.get('exemptAmount').value,
        //
        //     brand: 'brand',
        //     optionals: this.selectedOptionalList,
        //     tpOptionals: this.thirdPartList,
        //     secondhandData: secondhandData
        // };

        return commercilaData;
    }

    isAuthorizedToThirdParts(): boolean {
        // return this.authorizations.indexOf("3part") > -1;
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_COMMERCIALE_TERZE_PARTI);
    }

    showAccessories(): boolean {
        return this.authorizations.indexOf('acccs') > -1;
    }

    showNewTab(): boolean {
        if (this.authorizations.indexOf('n_tb') > -1) {
            return true;
        } else {
            this.isNew = false;
            return false;
        }
    }

    isAuthorizedToUsedTab(): boolean {
        // console.log("usato disponibile", this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_COMMERCIALE_VAL_USATO));
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_COMMERCIALE_VAL_USATO);
    }

    isAuthorizedToOtherSuppliers(): boolean {
        // console.log("fornitori esterni", this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_COMMERCIALE_FORNITORE_ESTERNO));
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_COMMERCIALE_FORNITORE_ESTERNO);
    }
}
