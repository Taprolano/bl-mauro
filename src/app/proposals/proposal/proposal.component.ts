import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ProposalsRoutingConstant} from "../proposalsRoutingConstant";
import {ProposalWorkflowService} from "../services/proposal-workflow.service";
import { LoggerService } from '../../shared/services/logger.service';

@Component({
    selector: 'app-proposal',
    templateUrl: './proposal.component.html',
    styleUrls: ['./proposal.component.scss']
})
export class ProposalComponent implements OnInit {

    constructor(private _router: Router,
                private _proposalWorkflowService: ProposalWorkflowService,
                private _logger: LoggerService) {
    }

    ngOnInit() {
        // console.log("ROUTING TO: " + ProposalsRoutingConstant.fullPath('list'));
        this._router.navigate([ProposalsRoutingConstant.fullPath('')])
//        this._proposalWorkflowService.exitToList();
    }

}
