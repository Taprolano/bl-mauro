import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IFilterTab} from "../../../data-model/interface/IFilterTab";
import {ProposalWorkflowService} from "../../services/proposal-workflow.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ProposalListRequestParameters} from "../../../data-model/class/ProposalListRequestParameters";
import {IProposalRequest} from "../../../data-model/interface/IProposalRequest";
import * as moment from 'moment';
import 'jquery';
import {ProposalsConstant} from "../../proposalsConstant";
import {UtilsService} from "../../../shared/services/utils.service";
import { AuthConstant } from 'src/assets/authConstant';
import { UserProfileService } from 'src/app/shared/services/user-profile.service';
import { LoggerService } from '../../../shared/services/logger.service';
import {ListTabs} from "../../list/EListTabs";

/**
 * @ignore
 */
declare var $: any;

/**
 * @ignore
 */
const __maxNumberOfTabs = 4;

/**
 * @ignore
 */
const __proposalStatePlaceholder = {desc: "Seleziona uno stato", id: null};

/**
 * ProposalFilterBarComponent: Componente che incapsula i filtri e le tab presenti all'interno
 * della lista delle proposte
 */
@Component({
    selector: 'app-proposal-filter-bar',
    templateUrl: './proposal-filter-bar.component.html',
    styleUrls: ['./proposal-filter-bar.component.scss']
})
export class ProposalFilterBarComponent implements OnInit {

    /**
     * @ignore
     */
    @Input()
    tabKey: string;

    /**
     * @ignore
     */
    @Output()
    tabKeyChange = new EventEmitter();

    /**
     * @ignore
     */
    @Input()
    versionFilter: IProposalRequest;

    /**
     * @ignore
     */
    @Output()
    versionFilterChange = new EventEmitter();

    /**
     * @ignore
     */
    @Output()
    doResearch = new EventEmitter();

    /**
     * @ignore
     */
    from: string;

    /**
     * @ignore
     */
    to: string;

    /**
     * @ignore
     */
    startDate =  "";

    /**
     * @ignore
     */
    endDate = "";

    /**
     * @ignore
     */
    filterAccordion = false;

    /**
     * @ignore
     */
    addingTab = false;

    /**
     * @ignore
     */
    possibleTabs: IFilterTab[] = [
        {label: "Tutte", key: ListTabs.ALL, selected: true},
        {label: "Documentazione Incompleta", key: ListTabs.PARTIAL, selected: false},
        {label: "Offerte Concessionario", key: ListTabs.OFFER, selected: false},
        {label: "Simulazioni", key: ListTabs.SIMULATION, selected: false},
        {label: "MBCPOS", key: ListTabs.MBCPOS, selected: false},
        {label: "VDZ", key: ListTabs.VDZ, selected: false}
    ];


    /**
     * @ignore
     */
    possibleStates = ProposalsConstant.PROPOSAL_STATES_4_FILTER_BAR;

    /**
     * @ignore
     */
    showProposalStatesFilter: boolean;

    /**
     * @ignore
     */
    filterOptionalForm: FormGroup;

    /**
     * Lista di tab attivi
     */
    tabs = [];

    /**
     * @ignore
     */
    authorizations: string[];

    /**
     * @ignore
     */
    selectableTabs: IFilterTab[] = [];

    /**
     * @ignore
     */
    stateSelected: string;

    /**
     * @ignore
     */
    constructor(private _proposalWorkflowService: ProposalWorkflowService,
                private _formBuilder: FormBuilder,
                private _utilsService: UtilsService,
                private _userProfileService: UserProfileService,
                private _logger: LoggerService) {
    }

    /**
     * invoca le primitive per inizializzare il comportamento dei tab e dei campi di ricerca presenti nei form
     */
    ngOnInit() {

        this.showProposalStatesFilter = false;
        this.tabKey = 'all';
        this.setTabs();
        this.createForms();
    }

    /**
     * primitiva per la creazione del form relativa ai campi di ricerca
     */
    createForms() {
        this.filterOptionalForm = this._formBuilder.group({
            contractNumber: this._formBuilder.control(null),
            proposalNumber: this._formBuilder.control(null),
            proposalState: this._formBuilder.control(null),
            licensePlate: this._formBuilder.control(null),
            frameNumber: this._formBuilder.control(null),
            applicant: this._formBuilder.control(null)
        });
    }

    /**
     * primitiva per la creazione dei tab
     */
    setTabs() {
        this.tabs = [];
        for (let tab of this.possibleTabs) {
            if (tab.selected) this.tabs.push(tab);
        }
        this.addingTab = false;
    }

    /**
     * attiva il processo di creazione di una nuova proposta
     */
    goToNewProposal() {
        this._proposalWorkflowService.startNewProposal();
    }

    /**
     * restituisce il numero di tab attivi
     * @returns {number}
     */
    get numberOfTabs(): number {
        let counter = 0;
        for (let tab of this.possibleTabs) {
            if (tab.selected) counter++;
        }

        return counter;
    }

    /**
     * restituisce il numero massimo di tab che è possibile attivare
     * @returns {number}
     */
    get maxNumberOfTabs(): number {
        return __maxNumberOfTabs;
    }

    /**
     * esegue il salvataggio dei campi presenti nel form all'interno dell'oggetto IProposalRequest che
     * verrà utilizzato per effettuare la ricerca
     */
    saveFilter() {
        let newVerionFilter: IProposalRequest = {
            activationDateTo: this.endDate,
            activationDateFrom: this.startDate,
            contractNumber: this.filterOptionalForm.get('contractNumber').value,
            proposalNumber: this.filterOptionalForm.get('proposalNumber').value,
            licensePlate: this.filterOptionalForm.get('licensePlate').value,
            frameNumber: this.filterOptionalForm.get('frameNumber').value,
            applicant: this.filterOptionalForm.get('applicant').value,
            proposalState: [this.filterOptionalForm.get('proposalState').value]
        };

        this.versionFilter = this._utilsService.assign(newVerionFilter);
        this.versionFilterChange.emit(this.versionFilter);
        this._logger.logInfo('ProposalFilterBarComponent', 'saveFilter', "versionFilter: ", this.versionFilter);
        // console.log("[List][getProposalsList] versionFilter: ", this.versionFilter);
        this.doResearch.next();
    }

    /**
     * @ignore
     * @returns {boolean}
     */
    // get tabs(): any[]{
    //     let retList = [];
    //     for( let tab of this.possibleTabs ){
    //         if(tab.selected) retList.push(tab);
    //     }
    //
    //     return retList;
    // }


    /**
     * @ignore
     * @returns {boolean}
     */
    checkStateActiveSelected(): boolean {
        for (let state of this.possibleStates) {
            if (state.id == 6) {
                return true;
            }
        }
        return false;
    }

    /**
     * @ignore
     */
    deselectAllProposalType(): void {
        for (let state of this.possibleStates) {
            /*state.selected = false;*/
        }
    }

    /**
     * pulisce i campi presenti nel form
     */
    clear(){
        this.filterOptionalForm.reset();
        this.startDate = '';
        this.endDate = '';
    }

    /**
     * ottiene lo stato selezionato nella select-list delle proposte
     * @returns {any}
     */
    get proposalState() {
        // console.log('proposalState', this.filterOptionalForm.get('proposalState').value, this.possibleStates[3].id);
        return this.filterOptionalForm.get('proposalState').value;
    }

    /**
     * @ignore
     * @returns {boolean}
     */
    public get proposalStateSelected() {
        return !!this.filterOptionalForm.get('proposalState').value;
    };


    /**
     * seleziona il tab passato come parametro
     * @param {string} key
     */
    setTab(key: ListTabs): void {
        this.tabKey = key;
        this.tabKeyChange.emit(key);
    }

    /*
    *  verifica se il bottone + è visible in base alle funzionalità del profilo
    */
    showNewProposal(): boolean {
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_COMMERCIALE) && 
        this._userProfileService.haveAbilitation(AuthConstant.PROP_CONF_FINANZIARIA);
    }
}
