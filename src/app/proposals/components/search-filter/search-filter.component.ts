import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {IFilterOption} from "../../../data-model/interface/IFilterOption";
import {IFilterChoice} from "../../../data-model/interface/IFilterChoice";
import {ISearch} from "../../../data-model/interface/ISearch";
import { LoggerService } from '../../../shared/services/logger.service';

const __searchInt = {
  field: null,
  filters: []
};

const __possiblefield =['productCode','version'];

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss']
})
export class SearchFilterComponent implements OnInit, OnDestroy {



    @Input()
    placeholder: string;

    @Input()
    filterKeys: string[];

    @Input()
    filterOption: IFilterOption;

    searchObj: ISearch;

    selectedId = 0;

    field: string;

    @Output()
    keySearched = new EventEmitter();
    show = false;

    constructor(private _logger: LoggerService) { }

    ngOnInit() {
        // console.log(`[${SearchFilterComponent.name}][onInit]`);
        this.searchObj = __searchInt;

    }

    ngOnDestroy(): void {
        // console.log(`[${SearchFilterComponent.name}][onDestroy]`);
        this.searchObj =  __searchInt;
        this.searchObj.field =  null; //force clear of search field
    }


    search(): void{
        this._logger.logInfo('SearchFilterComponent', 'search', this.searchObj);
        // console.log('search', this.searchObj);
        this.searchObj.filters = this.selectedFilter;
        this.keySearched.emit(this.searchObj);
    }

    selectField(value){
        this.selectedId = value;
        this.filterOption.possibleChoices[value].selected = true;
        this.filterOption.possibleChoices[(value+1)%2].selected = false;

        /*
                for (let fc of this.filterOption.possibleChoices){
                    if (fc.value != value) {
                        console.log("removing "+fc.value)
                        fc.selected = false;
                    }else {
                        fc.selected = !fc.selected;
                        console.log(fc.value+" = "+fc.selected )
                    }
                }
        */
    }
    changeField(){

    }

    get selectedFilter() : IFilterChoice[] {
        let retList =[];
        if (!!this.filterOption.possibleChoices){
            for (let fc of this.filterOption.possibleChoices){
                if (fc.selected) retList.push(fc);
            }
        }
        return retList;
    }


}
