import {Component, DoCheck, OnInit} from '@angular/core';
import {IProposalSummary} from "../../../data-model/interface/IProposalSummary";
import {Subscription} from "rxjs/index";
import {ProposalWorkflowService} from "../../services/proposal-workflow.service";
import {ProposalService} from "../../../shared/services/proposal.service";
import {ProposalsConstant} from "../../proposalsConstant";
import {UtilsService} from "../../../shared/services/utils.service";
import { LoggerService } from '../../../shared/services/logger.service';

const __voidSummary = {
    model: '',
    version: '',
    productCode:'',
    financial: '',
    registry: {
        accountHolder: '',
        type:''
    },
    preliminary: '',
    contract: ''
};

@Component({
  selector: 'app-product-summary',
  templateUrl: './proposal-summary.component.html',
  styleUrls: ['./proposal-summary.component.scss']
})
export class ProductSummaryComponent implements OnInit, DoCheck {

  completeStatesList = ProposalsConstant.PROPOSAL_STATE_LIST;
  summarySubscription: Subscription;
  activeState: string;

  productSummary: IProposalSummary = {
    model: '',
    version: '',
    productCode:'',
    financial: '',
    registry: {
        accountHolder: '',
        type:''
    },
    preliminary: '',
    contract: ''
  };

  constructor(
      private _proposalWorkflowService: ProposalWorkflowService,
      private _proposalService :ProposalService,
      private _utils: UtilsService,
      private _logger: LoggerService
  ) {
      this.summarySubscription = this._proposalWorkflowService.summarySub.subscribe(
          summary => this.updateSummary(summary)
      );

      this._proposalService.getMessage().subscribe(
          message => {
              this.updateState(message);
              // console.log("sentMessage - testMessage", message)
          });
  }

  ngOnInit() {
      this._logger.logInfo('ProductSummaryComponent', 'ngOnInit', "Starting");
    //   console.log("[ProductSummaryComponent][onInit] Starting");
      this._logger.logInfo('ProductSummaryComponent', 'ngOnInit', "fetch: "+this._proposalService.getSummary());
    //   console.log("[ProductSummaryComponent][onInit] fetch: "+this._proposalService.getSummary());
      this.productSummary = this._utils.isVoid(this._proposalService.getSummary())?__voidSummary:this._proposalService.getSummary();
  }


  ngDoCheck(){
/*
      console.log("[ProductSummaryComponent][onInit] Starting");
      console.log("[ProductSummaryComponent][onInit] fetch: "+this._proposalService.getSummary());
      this.productSummary = this._utils.isVoid(this._proposalService.getSummary())?__voidSummary:this._proposalService.getSummary();
*/

  }

    updateSummary(update: any): void{
        let updateKeys: string[] = Object.keys(update);
        let summaryKeys: string[] = Object.keys(this.productSummary);
        this._logger.logInfo('ProductSummaryComponent', 'updateSummary', "[update] save update", update);
        // console.log("[ProductSummaryComponent][updateSummary][update] save update", update);

        for ( let k of updateKeys ){
            if(summaryKeys.lastIndexOf(k)>=0) {
                this._logger.logInfo('ProductSummaryComponent', 'updateSummary', `[update] insert ${update[k]} in ${k} index: ${summaryKeys.lastIndexOf(k)}`);
                // console.log(`[ProductSummaryComponent][updateSummary][update] insert ${update[k]} in ${k} index: ${summaryKeys.lastIndexOf(k)}`);
                this.productSummary[k] = update[k];
            }
        }
        this._logger.logInfo('ProductSummaryComponent', 'updateSummary', "[update] save update", update);
        // console.log("[ProductSummaryComponent][updateSummary] save productSummary", this.productSummary);
        this._logger.logInfo('ProductSummaryComponent', 'updateSummary', "in", ProposalsConstant.summary);
        // console.log("[ProductSummaryComponent][updateSummary] in", ProposalsConstant.summary);
        this._proposalService.save(ProposalsConstant.summary, this.productSummary);
    }


    updateState(state:string) {

        let fstate;
        if(!this._utils.isVoid(state)) {
            fstate = this.completeStatesList.filter(item =>
                Object.keys(item).some(k => !this._utils.isVoid(item[k]) &&
                    item[k].toString().toLowerCase()
                        .indexOf(state.toLowerCase())>=0)
            );
        }
        this.activeState = (!this._utils.isVoid(fstate) && fstate.length>0)? (fstate)[0].id: undefined;
        if (!this.activeState){
            this.productSummary = __voidSummary;
        }

    }

    showSummary(): boolean {
        let currentState = this._proposalService.getCurrentState();
        return !!currentState && currentState == ProposalsConstant.stateProduct2Version;
    }

    showVersionAndProductCode(): boolean {
        let currentState = this._proposalService.getCurrentState();
        return this.showSummary() && currentState != ProposalsConstant.stateProduct2Version;
    }

}
