import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppSettings} from "../../../../AppSettings";
import {ListOrderKeys} from "../../../data-model/interface/IListOrderKeys";
import {Order} from "../../../data-model/interface/EOrder";
import { LoggerService } from '../../../shared/services/logger.service';

const __height:number = 450; //altezza lista
const  LIST_KEYS = ['productCode','version','fuel','catalog','price'];
const  LIST_LABELS = ['PRODUCT_CODE','VERSION','FUEL','CATALOG','PRICE'];

/* per richieste al backend */
const ORDER_KEYS = ['baumuster','modelDescription','fuelTypeDescr','pricelistDeadline','netCost'];

@Component({
    selector: 'app-version-list',
    templateUrl: './version-list.component.html',
    styleUrls: ['./version-list.component.scss']
})


export class VersionListComponent implements OnInit {

    @Input() id: string = 'version-scrollable_table'; //default
    // @Input() wrapperHeight: string;
    @Input() inputList;
    @Input() _widths; //per allineamento con header

    @Output() onClickEvent = new EventEmitter();
    @Output() onScrollEndEvent = new EventEmitter();
    @Output() sortEvent = new EventEmitter();

    defalutValueDate = AppSettings.ND_VERSION_DATE_VALUE;

    /**
     *     tiene traccia dell'header selezionato per il sorting (per css)
     */
    private sorted;

    // public opt= {
    //     axis:"y", theme: AppSettings.scrollStyle, setHeight: __height, callbacks: {
    //         onTotalScrollOffset: 300,
    //         alwaysTriggerOffsets: true,
    //         onTotalScroll: () => { this.emitterFunc(); }
    //     }
    // };


    callBack: Function;

    constructor(private _logger: LoggerService) { }

    emitterFunc() { this.onScrollEndEvent.emit(); 
        this._logger.logWarn('VersionListComponent', 'emitterFunc', "SCROLL") 
        // console.warn("SCROLL"); 
    }

    /**
     * callback per click
     * @param v : elemento della lista cliccato
     */
    onClick(v){
        this.onClickEvent.emit(v);
    }; // goNext(v) del padre

    ngOnInit() {
        //scoping della callback
        this.callBack = this.emitterFunc.bind(this);
        // Aggiunge callback per emettere l'evento input
        $(this.id).mCustomScrollbar({
            axis:'y', theme: AppSettings.scrollStyle, setHeight: __height, callbacks: {
                onTotalScrollOffset: 500,
                alwaysTriggerOffsets: true,
                onTotalScroll: () => { this.emitterFunc(); }
            }
        });
        // scrollbar inertia
/*        $(this.id).mCustomScrollbar("scrollTo", "bottom", {
            scrollInertia: 1000
        });*/
    }

    //SORTING COLONNE
    /**
     * Gestisce le icone css per il caricamento
     * @param {string} id : elemento contenente gli span con le icone
     * @returns {string} order : asc/desc
     */
    sortTable(id:string):string {
        let target = document.getElementById(id);
        let targetClassList = target.classList;
        let order="";
        // imposta la classe che mostra o nasconde la relativa icona di ordinamento
        if(targetClassList.contains('order-none')){
            targetClassList.remove('order-none');
            targetClassList.add('order-desc'); //none->desc
            order = Order.DESC;
        }
        else if(targetClassList.contains('order-desc')){
            targetClassList.remove('order-desc');
            targetClassList.add('order-asc'); //desc->asc
            order = Order.ASC;
        }
        else {
            targetClassList.remove('order-asc');
            targetClassList.add('order-none'); //asc->none
        }
        return order;
    }

    /**
     * Resetta la classe css che mostre le icone ordinamento
     * @param {string} id : elemento cui applicare i css
     */
    resetSort(id:string){
        let target = document.getElementById(id).classList;
        target.remove('order-asc');
        target.remove('order-desc');
        target.add('order-none');
    }

    /**
     * Memorizza la colonna correntemente ordinata e chiama nuove proposte
     * @param {string} id : elemento cliccato
     */
    sortByField(id:string){
        //resetta eventuali altre colonne con icone ordinamento
        let old = this.sorted;
        if(old && (id != old) ) this.resetSort(old);
        this.sorted = id;
        let order = this.sortTable(id);
        //scarica nuova lista
        let keyIndex = id.substring(1);
        // let key = LIST_KEYS[keyIndex];
        // let orderKey = ListOrderKeys[key];
        let orderKey = ORDER_KEYS[keyIndex];
        this.getSortedVersionList(orderKey,order);
    }

    /**
     * Richiede (tramite event emitter) proposte ordinate
     * @param {string} key : chiave del parametro
     * @param {string} order : asc/desc
     */
    getSortedVersionList(key:string,order:string){
        let sortString = order? `${key},${order}` : null;
        //scarica nuove proposte (ordinate come richiesto)
        this.sortEvent.emit(sortString);
        //resetta scroll
        this.goToTop();
    }

    /**
     * Resetta scroll lista e pagina
     */
    goToTop(){
        let elements = document.getElementsByClassName("scrollable__element");
        for(let i = 0; i < elements.length; i++) {
            $(elements[i]).mCustomScrollbar("scrollTo", "top", {
                scrollInertia: 0
            });
        }
    }

    public get LIST_LABELS(){
        return LIST_LABELS;
    }
}
