import {Component, Input, OnInit} from '@angular/core';
import {IOptional} from "../../../data-model/interface/IOptional";
import {Subscription} from "rxjs/index";
import {BeWorkQueue} from "../../services/be-work-queue.service";
import {AbstractModalService} from "../../../shared/components/abstract-modal/abstract-modal.service";
import { LoggerService } from '../../../shared/services/logger.service';
import { UtilsService } from '../../../shared/services/utils.service';
import { PaginationData } from '../../../data-model/class/PaginationData';

@Component({
  selector: 'app-optional-list',
  templateUrl: './optional-list.component.html',
  styleUrls: ['./optional-list.component.scss']
})
export class OptionalListComponent implements OnInit {


  @Input() modalParameters: any;

  code: string;
  description: string;

  optionalList: IOptional[];

  optionalSubscription: Subscription;

  pagination: PaginationData = new PaginationData(null,1,0,8,null);

  getOptionalListCallBack: Function;

  constructor(
      private _beProposalService: BeWorkQueue,
      private _abstractModalService: AbstractModalService,
      private _logger: LoggerService, 
      private _utilsService: UtilsService
  ) {}

  ngOnInit() {
      if(!this._utilsService.isVoid(this.modalParameters)){
        this.code = this.modalParameters.param.code;
        this.description = this.modalParameters.param.description;
      }

      this.getOptionalListCallBack = this.findOptionalList.bind(this);
      this.findOptionalList();
  }

  findOptionalList(){
      this.optionalSubscription = this._beProposalService.getOptionalList(this.code,this.description, this.pagination, true).subscribe(response =>{
          if (response.code == 200) {
              this.optionalList = response.object.object;
              this._logger.logInfo('OptionalListComponent', 'findOptionalList', "optionalList" , this.optionalList);
              //   console.log("optionalList", this.optionalList )
          } else {
              //toDo Gestione Errore
              this._logger.logError('OptionalListComponent', 'findOptionalList',"ERRORE: "+response.message);
            //   console.log("ERRORE: "+response.message );

          }
      })
  }

  addOptional( opt:  IOptional ){
      this._logger.logInfo('OptionalListComponent', 'addOptional', opt)
    //   console.log(opt);
      this._logger.logInfo('OptionalListComponent', 'addOptional', this._abstractModalService)
    //   console.log(this._abstractModalService);
      this._abstractModalService.hideModal.next(opt);
  }

}
