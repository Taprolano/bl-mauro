import { Component, OnInit, Input } from '@angular/core';
import { NewProposalPresenceService } from '../../../shared/services/new-proposal-presence.service';
import { Router } from '@angular/router';
import { ProposalWorkflowService } from '../../services/proposal-workflow.service';
import { ProposalsConstant } from '../../proposalsConstant';
import { DomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';

@Component({
  selector: 'app-go-top-new-proposal',
  templateUrl: './go-top-new-proposal.component.html',
  styleUrls: ['./go-top-new-proposal.component.scss']
})
export class GoTopNewProposalComponent implements OnInit {


  newProposalButtonPosition: string;
  spaceFromBar: string;

  constructor(private newProposalPresenceService: NewProposalPresenceService,
              private router: Router,
              private _proposalWorkflow: ProposalWorkflowService) { }


  ngOnInit() {
  }

  goToTop() {
    window.scrollTo(0,0);
    // document.getElementById()
  }

  goToNewProposal() {
    this._proposalWorkflow.goTo(ProposalsConstant.stateProduct2Model);
  }

  isNewProposalPresent(): boolean {
    return this.newProposalPresenceService.getNewProposalPresence();
  }

  ngAfterViewInit() {
    this.newProposalButtonPosition = (window.outerHeight/2).toString() + "px";
    this.spaceFromBar = "20px";
    // this.newProposalBottonPosition = (window.innerHeight + 
    // document.getElementById('container').offsetTop).toString() + 'px';
    // this.newProposalBottonPosition = 
    // (window.innerHeight - (document.getElementById('container').offsetTop
    //  + document.getElementById('container').offsetHeight)).toString() + 'px';
  }
}
