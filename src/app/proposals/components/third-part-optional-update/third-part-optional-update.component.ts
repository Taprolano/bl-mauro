import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ISupplier} from "../../../data-model/interface/ISupplier";
import {IIva} from "../../../data-model/interface/IIva";
import {IThirdPartOptional} from "../../../data-model/interface/IThirdPartOptional";
import {AbstractModalService} from "../../../shared/components/abstract-modal/abstract-modal.service";

@Component({
  selector: 'app-third-part-optional-update',
  templateUrl: './third-part-optional-update.component.html',
  styleUrls: ['./third-part-optional-update.component.scss']
})
export class ThirdPartOptionalUpdateComponent implements OnInit {

    tpOptionalForm: FormGroup;
    suppliersList: ISupplier[];
    ivaList: IIva[];


    @Input() modalParameters: any;

    constructor(private _formBuilder: FormBuilder,
        private _abstractModalService: AbstractModalService
    ) { }

  ngOnInit() {
    this.suppliersList = this.modalParameters.param.suppliersList;
    this.ivaList = this.modalParameters.param.ivaList;

    let tpOptionalOld: IThirdPartOptional = this.modalParameters.param.tpOptional;

    this.tpOptionalForm = this._formBuilder.group({
          supplier: this._formBuilder.control(tpOptionalOld.supplier),
          iva: this._formBuilder.control(tpOptionalOld.iva),
          description: this._formBuilder.control(tpOptionalOld.optional.description),
          code: this._formBuilder.control(tpOptionalOld.optional.code),
          price: this._formBuilder.control(tpOptionalOld.optional.price),
          priceWithIVA: this._formBuilder.control(tpOptionalOld.optional.priceWithIVA),

      });
  }

  updateTpOptional(){
        let priceWithIVA = this.tpOptionalForm.get('price').value*this.tpOptionalForm.get('iva').value.value;
        let newTpOptional: IThirdPartOptional = {
            supplier: this.tpOptionalForm.get('supplier').value,
            iva: this.tpOptionalForm.get('iva').value,
            optional:{
                description:this.tpOptionalForm.get('description').value,
                code:this.tpOptionalForm.get('code').value,
                price:this.tpOptionalForm.get('price').value,
                priceWithIVA: priceWithIVA,
                selected: true,
                discount: false
            }
        };

        this._abstractModalService.hideModal.next(newTpOptional);
    }
}
