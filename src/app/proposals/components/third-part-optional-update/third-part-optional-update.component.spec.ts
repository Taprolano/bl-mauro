import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartOptionalUpdateComponent } from './third-part-optional-update.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MbzDashboardModule } from 'src/app/mbz-dashboard/mbz-dashboard.module';
import { ProposalsModule } from '../../proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { createTranslateLoader } from 'src/app/app.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';
import { AppComponent } from 'src/app/app.component';
import { AuthGuard } from 'src/app/shared';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {DirectAccessGuard} from "../../../shared/guard/direct-access/direct-access.guard";

describe('ThirdPartOptionalUpdateComponent', () => {
    let component: ThirdPartOptionalUpdateComponent;
    let fixture: ComponentFixture<ThirdPartOptionalUpdateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

    }));

    it('should create component', async () => {
        fixture = TestBed.createComponent(ThirdPartOptionalUpdateComponent);
        component = fixture.componentInstance;
        component.modalParameters = {
            param: {
                code: '',
                description: '',
                tpOptional: {
                    optional: {
                        code: '',
                        description: '',
                        price: 0,
                        priceWithIVA: 0,
                        discount: false,
                        selected: true
                    },
                    supplier: {
                        lable: '',
                        code: ''
                    },
                    iva: {
                        lable: '',
                        value: 0
                    }
                }
            }
        };
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });
});
