import {Component, OnInit, Output} from '@angular/core';
import {ProposalWorkflowService} from "../../services/proposal-workflow.service";
import {Subject} from "rxjs/index";
import {ProposalsConstant} from "../../proposalsConstant";
import {ProposalService} from "../../../shared/services/proposal.service"
import {RequestManagerService} from "../../../shared/services/request-manager.service";
import { LoggerService } from '../../../shared/services/logger.service';

@Component({
    selector: 'app-proposal-footer',
    templateUrl: './proposal-footer.component.html',
    styleUrls: ['./proposal-footer.component.scss']
})
export class ProposalFooterComponent implements OnInit {


    constructor(private _proposalWorkflowService: ProposalWorkflowService,
                private _proposalService: ProposalService,
                private _logger: LoggerService) {
    }

    ngOnInit() {
    }

    goPrev() {
        this._proposalWorkflowService.backward();
    }

    goNext() {
        this._proposalWorkflowService.goNextThroughButton()
    }

    get showBackwardButton() {
        let currentState = this._proposalService.getCurrentState();
        let stateAlowed: string[] = ProposalsConstant.PROPOSAL_BACKWARD_BUTTON_STATE_LIST;
        return stateAlowed.indexOf(currentState) > -1
    }

    get showOnwardButton() {
        let currentState = this._proposalService.getCurrentState();
        let stateAlowed: string[] = ProposalsConstant.PROPOSAL_ONWARD_BUTTON_STATE_LIST;
        return stateAlowed.indexOf(currentState) > -1
    }

    get showExitButton() {
        return this.showOnwardButton;
        // let currentState = this._proposalService.getCurrentState();
        // let stateAlowed: string[] = ProposalsConstant.PROPOSAL_ONWARD_BUTTON_STATE_LIST;
        // return stateAlowed.indexOf(currentState) > -1
    }

    exit() {
        this._proposalWorkflowService.exitToList();
    }


    goToTop() {
        $(document.body).mCustomScrollbar("scrollTo", "top", {
            scrollInertia: 1500
        }) //window.scrollTo(0,0);
        let elements = document.getElementsByClassName("scrollable__element");
        for (let i = 0; i < elements.length; i++) {
            $(elements[i]).mCustomScrollbar("scrollTo", "top", {
                scrollInertia: 1500
            }) //window.scrollTo(0,0);

            //elements[i].scrollTop = 0;
            this._logger.logInfo('ProposalFooterComponent', 'goToTop', "scrollable__element class:", elements[i]);
            // console.log("scrollable__element class:", elements[i]);
            this._logger.logInfo('ProposalFooterComponent', 'goToTop', "scrollable__element id:", document.getElementById("mbz-components-scroll__id"));
            // console.log("scrollable__element id:", document.getElementById("mbz-components-scroll__id"));
        }
    }

}
