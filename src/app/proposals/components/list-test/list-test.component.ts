import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Subscription } from 'rxjs';
import { ProposalListRequestParameters } from 'src/app/data-model/class/ProposalListRequestParameters';
import {NgxSpinnerService} from "ngx-spinner";
import { IPagination } from 'src/app/data-model/interface/IPagination';
import {IProposals} from "../../../data-model/interface/IProposals";
import {AppSettings} from "../../../../AppSettings";
import {ProposalService} from "../../../shared/services/proposal.service";
import {ProposalWorkflowService} from "../../services/proposal-workflow.service";
import {BeWorkQueue} from "../../services/be-work-queue.service";
import {HeaderService} from "../../../shared/services/header.service";
import {IFilterOption} from "../../../data-model/interface/IFilterOption";
import {IFilterChoice} from "../../../data-model/interface/IFilterChoice";
import {NotificationService} from "../../../shared/components/notification/notification.service";
import {ProposalsMessages} from "../../proposalsMessages";
import {RequestManagerService} from "../../../shared/services/request-manager.service";
import { LoggerService } from '../../../shared/services/logger.service';
import {CreditQueueAllRequestParameters} from "../../../data-model/class/ProposalListAllRequestParameters";



const  __filterPlaceholder = 'Cerca per proposta, contratto o soggetto.';


const __keysChoice: IFilterChoice[] = [
    {value: '0', label: 'Nuova proposta', selected: false },
    {value: 'modifyProposal', label: 'Proposta in modifica', selected: false},
    {value: 'benRequest', label: 'Richiesta benestare', selected: false},
    {value: 'failedRequest', label: 'Richiesta numerazione fallita', selected: false},

    {value: 'notAcptProp', label: 'Proposta non accettata', selected: false },
    {value: 'acptProp', label: 'Proposta accettata', selected: false},
    {value: 'acptModProp', label: 'Proposta accettata con modifiche', selected: false},
    {value: 'numRequest', label: 'Richiesta numerazione', selected: false},


    {value: 'refusedNum', label: 'Numerazione rifiutata', selected: false },
    {value: 'execNum', label: 'Numerazione eseguita', selected: false},
    {value: 'printContract', label: 'Contratto stampato', selected: false},
    {value: 'actRequest', label: 'Richiesta attivazione', selected: false},

    {value: 'execActiv', label: 'Attivazione eseguita', selected: false },
    {value: 'refusedActiv', label: 'Attivazione rifiutata', selected: false},
    {value: 'failCancRequest', label: 'Richiesta cancellazione fallita', selected: false},
    {value: 'modifyProp', label: 'Proposta in modifica', selected: false}
];
const __filterOption: IFilterOption = {
    key: 'state',
    possibleChoices: __keysChoice
};

const  __keys: string[] = [ 'proposal', 'contract', 'subject' ];
@Component({
  selector: 'app-list-test',
  templateUrl: './list-test.component.html',
  styleUrls: ['./list-test.component.scss']
})
export class ListTestComponent implements OnInit {


    private listSubscribe: Subscription;
    proposalsList: IProposals[] = [];
    printList: IProposals[] = [];
    requestParameters: CreditQueueAllRequestParameters;

    sem:boolean = false;

    pageProposals: IPagination = {keys: null, numberOfResults: null, recordPerPage: AppSettings.ELEMENT2PAGE, page: 1, nPage:0, size: AppSettings.PROPOSALS2PAGE}

    newProposalButtonPosition: string;
    spaceFromBar: string;

    constructor(private _proposalService: ProposalService,
                private _proposalWorkflowService: ProposalWorkflowService,
                private beProposalService: BeWorkQueue,
                private _headerService: HeaderService,
                private _serviceManager: RequestManagerService,
                private _spinner: NgxSpinnerService,
                private _notification: NotificationService,
                private _logger: LoggerService
    ) {
    }

    ngOnInit() {
        this._spinner.show();
        //this.variableToHeader();
        this._logger.logInfo('ListTestComponent', 'ngOnInit', '-----------------------------------------------------------');
        // console.log('-----------------------------------------------------------');
        this._proposalService.printMemory();
        this._logger.logInfo('ListTestComponent', 'ngOnInit', '-----------------------------------------------------------');
        // console.log('-----------------------------------------------------------');
        this._proposalService.clear();
        this._logger.logInfo('ListTestComponent', 'ngOnInit', '-----------------------------------------------------------');
        // console.log('-----------------------------------------------------------');
        this._proposalService.printMemory();
        this._logger.logInfo('ListTestComponent', 'ngOnInit', '-----------------------------------------------------------');
        // console.log('-----------------------------------------------------------');

        this._proposalService.getMessage().subscribe(message =>
             this._logger.logInfo('ListTestComponent', 'ngOnInit', "sentMessage - testMessage", message)
             );

        // this._proposalService.updateStatus(ProposalsConstant.stateProduct2Model);

        this.requestParameters = new CreditQueueAllRequestParameters();

        this.getProposalsList();

        window.onbeforeunload = () => this.ngOnDestroy();

    }

    getProposalsList() {

        this.requestParameters.page = this.pageProposals.page;
        this.requestParameters.size = this.pageProposals.size;
        this.listSubscribe = this.beProposalService.getProposalsList(this.requestParameters, false).subscribe(
            response => {
                if (response.code == 200) {
                    this.proposalsList = response.object;
                    this.printList = this.printList.concat(this.proposalsList);
                }
                setTimeout(() => {
                    this._spinner.hide();
                }, 250);
                this.sem = false;
            }, error => {
//                this._notification.sendNotification(ProposalsMessages.LIST_REQUEST_ERROR);
                setTimeout(() => {
                    this._spinner.hide();
                }, 250);

                this.sem = false;
            }

        );
    }

    getNextProposalsList() {

        if(!this.sem){

            // Emit the event
            // this.scrollEvent.emit();
            this.sem = true;

            this._spinner.show();
            this._logger.logInfo('ListTestComponent', 'getNextProposalsList');
            //console.log(`[${ListTestComponent.name}][getNextProposalsList]`);
            this.pageProposals.page +=1;
            this.getProposalsList();

        }
    }

    /* ngOnInit() {
         this.variableToHeader();
         this._proposalService.getMessage().subscribe(message => console.log('sentMessage - testMessage', message));

         this._proposalService.save(ProposalsConstant.stateProduct2Model, {
             cmp1: ''
         });
         console.log(this._proposalService.getObj2CurrentState());


         this.proposalsList.push({
             asset: 1,
             proposal: '100200746223',
             contract: '--',
             subject: 'Henry Richardson Henry Richardson',
             state: '0',
             date: '01-12-2020',
             deadline: '01-12-2020',
             priority: 'C',
             seller: 'Alfie Wood'
         });

         this.printList = this.proposalsList;
     }*/

    public get filterPlaceholder(): string {
        return __filterPlaceholder;
    }

    public get filterKeys(): string[] {
        return __keys;
    }

    public get filterOption(): IFilterOption {
        return __filterOption;
    }


    goToNewProposal() {
        this._proposalWorkflowService.startNewProposal();
    }

    /* public variableToHeader() {
         this._headerService.setVariable(true);
     }*/

    ngAfterContentChecked() {
        if(!!window.outerHeight){
            this.newProposalButtonPosition = (window.outerHeight/2).toString() + "px";
        }
        this.spaceFromBar = "20px";
    }

    ngOnDestroy() {
        this.listSubscribe.unsubscribe();
    }

}
