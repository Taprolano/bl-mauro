import {Component, ElementRef, EventEmitter, HostListener, OnInit, Output, Renderer2, Input} from '@angular/core';
import 'jquery';
import 'bootstrap';
import { LoggerService } from '../../../shared/services/logger.service';
declare var $:any;

@Component({
  selector: 'app-infinite-scroll',
  templateUrl: './infinite-scroll.component.html',
  styleUrls: ['./infinite-scroll.component.scss']
})
export class InfiniteScrollComponent implements OnInit {


  @Input() id:string = 'mbz-components-scroll__id'; //default
  @Input() wrapperHeight: string;

    @Output() throwEvent = new EventEmitter();

  scrollHeight: number;
  pointerScroll: number;


  constructor(private _logger: LoggerService) { }

  ngOnInit() {

  }

/*  @HostListener("window:scroll", [])
  onScroll(): void {

      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          this.scrollEvent.emit();
      }
  }*/

  onScroll(): void {
      this._logger.logInfo('InfiniteScrollComponent', 'onScroll', 'scrollTop id:[' + this.id + ']');
      //console.log(`[${InfiniteScrollComponent.name}][onScroll] scrollTop id:[${this.id}]`);
      const elHeight = $('#' + this.id);
      this.scrollHeight = elHeight.prop('scrollHeight');
      const maxScrollHeight = this.scrollHeight - elHeight.innerHeight();
      this.pointerScroll = ((elHeight.scrollTop()) % maxScrollHeight);
      const abs2con = (maxScrollHeight-(maxScrollHeight*10/100));

      // console.log(`[${InfiniteScrollComponent.name}][onScroll] scrollTop, relHeight, abs2con, maxScrollHeight `, elHeight.scrollTop(), this.pointerScroll, abs2con, maxScrollHeight);

      // Compare the new with old and only raise the event if values change
      if( this.pointerScroll >= abs2con){
          // Emit the event
          this.throwEvent.emit();
      }
  }



}
