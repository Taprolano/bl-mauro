import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProposalsConstant} from "../../proposalsConstant";
import {ProposalService} from "../../../shared/services/proposal.service";
import {UtilsService} from "../../../shared/services/utils.service";

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, OnDestroy {

  completeStatesList = ProposalsConstant.PROPOSAL_STATE_LIST;
  uniqueStateList;
  activeState;

  constructor(private _proposalService: ProposalService,
              private _utils: UtilsService) {

  }

  ngOnInit() {

    this.uniqueStateList = this._utils.removeDuplicates(this.completeStatesList, 'id');
    this._proposalService.getMessage().subscribe(
      message => {
        this.updateStepperState(message);
        // console.log("sentMessage - testMessage", message)
    });


  }


  updateStepperState(state:string) {
      // let fstate = this.statesList.find(cstate => cstate.state[0] == state );
      let fstate;
      if(!this._utils.isVoid(state)) {
          fstate = this.completeStatesList.filter(item =>
              Object.keys(item).some(k => !this._utils.isVoid(item[k]) &&
                  item[k].toString().toLowerCase()
                      .indexOf(state.toLowerCase())>= 0)
          );
      }
      // console.log("activeState, state, fstate", this.activeState, state, fstate);
      this.activeState = (!this._utils.isVoid(fstate) && fstate.length>0)? (fstate)[0].id: undefined;

  }


  ngOnDestroy(): void {
  }



}
