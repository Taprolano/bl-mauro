import {NotificationMessage, NOTIFICATION_TYPE} from "../shared/components/notification/NotificationMessage";

export class ProposalsMessages{

    public static LIST_REQUEST_ERROR= new NotificationMessage(
        "Si è verificato un errore nella ricerca della lista delle proposte. Ricaricare la pagina.",
        NOTIFICATION_TYPE.ERROR
    )

    public static MODEL_REQUEST_ERROR= new NotificationMessage(
        "Si è verificato un errore nella ricerca dei modelli. Ricaricare la pagina.",
        NOTIFICATION_TYPE.ERROR
    )

    public static VERSION_REQUEST_ERROR= new NotificationMessage(
        "Si è verificato un errore nella ricerca delle versioni. Ricaricare la pagina.",
        NOTIFICATION_TYPE.ERROR
    )

    public static HEADER_REQUEST_ERROR = new NotificationMessage(
        "Si è verificato un errore nel caricamento dei contatori delle proposte. Ricaricare la pagina.",
        NOTIFICATION_TYPE.ERROR
    )
}