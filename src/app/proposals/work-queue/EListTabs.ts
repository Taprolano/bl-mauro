/**
 * Tab selezionabili per la lista proposte
 */
export enum ListTabs {
    ALL="all",
    PARTIAL="partial",
    OFFER="offer",
    SIMULATIONS="simulations",
    MBCPOS="mbcpos",
    VDZ="vdz",
    INERROR="inerror",
    REPROPOSED="reproposed",
    URGENT="urgent",
    ASSIGNED_TO_ME="assingnedToME",
    DOCUMENT_CHECK="documentCheck",
    BLOCKED="blocked",
    DOCUMENT_INCOMPLETE="documentIncomplete",
    SECOND_APPROVATION="secondApprovation",
    IN_INVESTIGATION="inInvestigation",
    SUSPENDED="suspended",
    WORKABLE="workable",
    TO_ACTIVATE= "toActivate",
    GRAPHOMETRIC_SIGN="graphometricSignature",
    DDM="ddm",
    RIF_RIL="rifRil",
    EXCLUDED_RIF_RIL="excludedRifRil",
    PAPER_SIGN="paperSign"
}
