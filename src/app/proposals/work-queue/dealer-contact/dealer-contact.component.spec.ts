import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DealerContactComponent} from './dealer-contact.component';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from '../../../shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MbzDashboardModule} from '../../../mbz-dashboard/mbz-dashboard.module';
import {ProposalsModule} from '../../proposals.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {createTranslateLoader} from '../../../app.module';
import {environment} from '../../../../environments/environment';
import {AppRoutingModule} from '../../../app-routing.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {AppComponent} from '../../../app.component';
import {IFilterTab} from '../../../data-model/interface/IFilterTab';
import {PaginationData} from '../../../data-model/class/PaginationData';
import {ListTabs} from '../EListTabs';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('DealerContactComponent', () => {
    let component: DealerContactComponent;
    let fixture: ComponentFixture<DealerContactComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DealerContactComponent);
        component = fixture.componentInstance;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            subjectName: builder.control(' '),
            subjectType: builder.control(' '),
            propNumber: builder.control(' '),
            proposalType: builder.control(' '),
            chassis: builder.control(' '),
            assetType: builder.control(' ')
        });
        component.advancedFilters = mockFormGroup;

        let mockFilterTab: IFilterTab = {
            label: ' ',
            key: ' ',
            selected: true
        };
        let mockFilterTabList: IFilterTab[] = [];
        mockFilterTabList.push(mockFilterTab);
        component.possibleTabs = mockFilterTabList;

        let mockPageProposals: PaginationData = new PaginationData(0, 0, 0, )

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('checkSelectableTabs test', () => {
        component.checkSelectableTabs();
    });

    it('get numberOfTabs test', () => {
        let res = component.numberOfTabs;
    });

    it('get maxNumberOfTabs test', () => {
        let res = component.maxNumberOfTabs;
    });

    it('setTabs test', () => {
        spyOn(component, 'manageSelectedTabs').and.returnValue(false);
        component.setTabs();
    });

    it('resetPagination test', () => {
        component.resetPagination();
    });

    it('getNextPage test', () => {
        component.sem = false;
        component.getNextPage();
    });

    it('get filterPlaceholder test', () => {
        let res = component.filterPlaceholder;
    });

    it('get filterOption test', () => {
        let res = component.filterOption;
    });

    it('onSort test', () => {
        spyOn(component, 'resetPagination').and.returnValue(false);
        spyOn(component, 'getAllList').and.returnValue(false);
        component.onSort('test');
    });

    it('onTabKeyChange test', () => {
        spyOn(component, 'cleanTabData').and.returnValue(false);
        spyOn(component, 'getAllList').and.returnValue(false);
        component.onTabKeyChange('test');
    });

    it('cleanTabData test', () => {
        spyOn(component, 'resetPagination').and.returnValue(false);
        component.cleanTabData();
    });

    it('setTab test', () => {
        component.addingTab = true;
        component.tabKey = 'test1';
        spyOn(component, 'onTabKeyChange').and.returnValue(false);
        component.setTab('test');
    });

    it('onFilterClick test', () => {
        component.addingTab = true;
        component.onFilterClick();
    });

    it('onTabSelectorClick test', () => {
        component.addingTab = true;
        component.onTabSelectorClick();
    });

    it('clearForm test', () => {
        component.clearForm();
    });

    it('saveAdvancedFilter test', () => {
        spyOn(component, 'resetPagination').and.returnValue(false);
        spyOn(component, 'getAllList').and.returnValue(false);
        component.saveAdvancedFilter();
    });

});
