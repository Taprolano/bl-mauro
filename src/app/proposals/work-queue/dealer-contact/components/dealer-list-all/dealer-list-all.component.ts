import {Component, OnInit} from '@angular/core';
import {AccordionListAbstract} from '../../../../components/accordion-list/accordion-list.abstract';
import {AbstractModalService} from '../../../../../shared/components/abstract-modal/abstract-modal.service';
import {TranslateService} from '@ngx-translate/core';
import {LoggerService} from '../../../../../shared/services/logger.service';
import {Router} from '@angular/router';
import {ProposalsRoutingConstant} from '../../../../proposalsRoutingConstant';
import {UtilsService} from '../../../../../shared/services/utils.service';
import {PayoutQueue} from '../../../../../data-model/class/PayoutQueue';
import {PersistenceService} from "../../../../../shared/services/persistence.service";
import {DealerContactQueue} from "../../../../../data-model/class/DealerContactQueue";

@Component({
    selector: 'app-dealer-list-all',
    templateUrl: './dealer-list-all.component.html',
    styleUrls: ['./dealer-list-all.component.scss']
})
export class DealerListAllComponent extends AccordionListAbstract implements OnInit {

    /**@ignore*/
    readonly __widths = ["0%", "0%", "0%", "0%", "0%", "0%", "0%", "0%"];
    /**@ignore*/
    readonly headers = ["PROPOSAL", "ARRIVAL", "FINANCIAL_PRODUCT", "APPLICANT", /*"SUSPENDED_BY",*/ "CAUSAL_SUSPENSION", /* "CAMP","ASSET",*/ ""];

    /**@ignore*/
    readonly objectKeys = DealerContactQueue.keys;

    /**@ignore*/
    readonly __wrapperHeight = "300px";

    constructor(_abstractModalService: AbstractModalService,
                translate: TranslateService,
                _logger: LoggerService,
                private _router: Router,
                private _persistence: PersistenceService,
                private _utils: UtilsService) {
        super(_abstractModalService, translate, _logger);
    }

    ngOnInit() {
    }

    /**
     * metodo per rimuovere una classe che "restringe" il contenuto dell'accordion una volta aperto
     */
    checkContent() {
        let elements = document.getElementsByClassName('mCSB_inside');
        if (!this._utils.isVoid(elements)) {
            let element = elements.item(0);
            element.classList.remove('mCSB_inside');
            if (element.classList.contains('mCSB_inside')) {
                element.classList.remove('mCSB_inside');
            }
        }
    }

    /**
     * Naviga verso la pagina di consultazione
     */
    goToConsultation(idProposal: string, processInstanceID: number): void {

        let obj = {
            idProposal: idProposal,
            processInstanceID: processInstanceID
        };

        this._persistence.save("SELECT_PROPOSAL", obj);

        this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueConsultation')]);
    }

}
