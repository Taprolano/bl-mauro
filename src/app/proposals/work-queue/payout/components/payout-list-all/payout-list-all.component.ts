import {Component, OnInit} from '@angular/core';
import {AccordionListAbstract} from '../../../../components/accordion-list/accordion-list.abstract';
import {PayoutQueue} from '../../../../../data-model/class/PayoutQueue';
import {AbstractModalService} from '../../../../../shared/components/abstract-modal/abstract-modal.service';
import {TranslateService} from '@ngx-translate/core';
import {LoggerService} from '../../../../../shared/services/logger.service';
import {Router} from '@angular/router';
import {UtilsService} from '../../../../../shared/services/utils.service';
import {ProposalsRoutingConstant} from '../../../../proposalsRoutingConstant';
import {PersistenceService} from "../../../../../shared/services/persistence.service";

@Component({
    selector: 'app-payout-list-all',
    templateUrl: './payout-list-all.component.html',
    styleUrls: ['./payout-list-all.component.scss']
})
export class PayoutListAllComponent extends AccordionListAbstract implements OnInit {

    /**@ignore*/
    readonly __widths = ["0%", "0%", "0%", "0%", "0%", "0%", "0%", "0%"];
    /**@ignore*/
    readonly headers = ["CONTRACT", "ARRIVAL", "FINANCIAL_PRODUCT", "APPLICANT", "SUSPENDED_BY", "CAUSAL_SUSPENSION", "CAMP", ""];

    /**@ignore*/
    readonly objectKeys = PayoutQueue.keys;

    /**@ignore*/
    readonly __wrapperHeight = "300px";

    constructor(_abstractModalService: AbstractModalService,
                translate: TranslateService,
                _logger: LoggerService,
                private _persistence: PersistenceService,
                private _router: Router,
                private _utils: UtilsService) {
        super(_abstractModalService, translate, _logger);
    }

    ngOnInit() {
    }

    /**
     * metodo per rimuovere una classe che "restringe" il contenuto dell'accordion una volta aperto
     */
    checkContent() {
        let elements = document.getElementsByClassName('mCSB_inside');
        if (!this._utils.isVoid(elements)) {
            let element = elements.item(0);
            if(element && element.classList &&
                element.classList.contains('mCSB_inside')) {
                element.classList.remove('mCSB_inside');
            }
        }
    }

    /**
     * Naviga verso la pagina di consultazione
     */
    goToConsultation(idProposal: string, processInstanceID: number): void {

        let obj = {
            idProposal: idProposal,
            processInstanceID: processInstanceID
        };

        this._persistence.save("SELECT_PROPOSAL", obj);

        this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueConsultation')]);
    }

    getWidth(key) : string{
        let app : string = '';
        switch (key) {
            case 'CONTRACT':
                app = '152px';
                break;
            case 'contratto':
                app = '152px';
                break;
            case 'ARRIVAL':
                app = '135px';
                break;
            case 'arrivo':
                app = '135px';
                break;
            case 'CAMP':
                app = '80px';
                break;
            case 'campagna':
                app = '80px';
                break;
            case 'FINANCIAL_PRODUCT':
                app = '180px';
                break;
            case 'tipoProposta':
                app = '180px';
                break;
            case 'SUSPENDED_BY':
                app = '160px';
                break;
            case 'sospesaDa':
                app = '160px';
                break;
            case 'APPLICANT':
                app = '160px';
                break;
            case 'richiedente':
                app = '160px';
                break;
            case 'CAUSAL_SUSPENSION':
                app = '180px';
                break;
            case 'causaleSospensione':
                app = '180px';
                break;
            default:
                app = '';
                break;
        }
        return app;
    }

}
