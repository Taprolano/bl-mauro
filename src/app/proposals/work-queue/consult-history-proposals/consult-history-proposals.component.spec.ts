import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultHistoryProposalsComponent } from './consult-history-proposals.component';

describe('ConsultHistoryProposalsComponent', () => {
  let component: ConsultHistoryProposalsComponent;
  let fixture: ComponentFixture<ConsultHistoryProposalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultHistoryProposalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultHistoryProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
