import {Component, Input, OnInit} from '@angular/core';
import {ListTabsConsult} from '../../EListTabsConsult';
import {ValueList} from '../../../../../data-model/class/ListValues';

@Component({
  selector: 'app-consult-advanced-filters',
  templateUrl: './consult-advanced-filters.component.html',
  styleUrls: ['./consult-advanced-filters.component.scss']
})
export class ConsultAdvancedFiltersComponent implements OnInit {
  @Input() listLabel: string;

  filterList: ValueList[];
  cdsFilters: ValueList[];
  derogaFilters: ValueList[];
  documentazioneFilters: ValueList[];
  lineaProdottoFilters: ValueList[];
  retailFilters: ValueList[];
  soggettoFilters: ValueList[];

  childSelectLabel: string;

  childFirstTextLabel: string;
  childSecondTextLabel: string;
  tabs = ListTabsConsult;

  @Input() firstLabel: string;
  @Input() firstValue: string;
  @Input() secondLabel: string;
  @Input() secondValue: string;

  _activeTab: string;
  get activeTab(): string {
    return this._activeTab;
  }

  @Input('activeTab')
  set allowDay(value: string) {
    this._activeTab = value;
    this.childSelectLabelInit();
    this.childTextLabelInit();
  }

  constructor() { }

  ngOnInit() {
    this.childSelectLabelInit();
    this.childTextLabelInit();
    this.filterListsInit();
  }

  /**
   * pulisce il form
   */
  clearForm(): void {
    // this.resetDerogation = !this.resetDerogation;
    // this.resetSubjectTypes = !this.resetSubjectTypes;
    // this.resetCDS = !this.resetCDS;
    // this.advancedFilters.reset();
  }

  /**
   * esegue il salvataggio dei campi presenti nel form all'interno dell'oggetto request che
   * verrà utilizzato per effettuare la ricerca
   */
  saveAdvancedFilter(value: string): void {
    console.info('SavingValue:', value);
    // this.requestParamiters.deroga = this.selectedDerogation;
    // this.requestParamiters.errorBancaDati = this.advancedFilters.get('dataBankInError').value;
    // this.requestParamiters.nomeSogg = this.advancedFilters.get('subjectName').value;
    // this.requestParamiters.tipoProp = this.advancedFilters.get('proposalType').value;
    // this.requestParamiters.tipoSogg = this.selectedSubjectTypes;
    // this.requestParamiters.k2k = this.advancedFilters.get('k2k').value;
    // // this.requestParamiters.clickAndGo = this.advancedFilters.get('clickAndGo').value;
    // this.requestParamiters.cds = this.selectedCDS;
    // this.requestParamiters.asset = this.advancedFilters.get('assetType').value;
    // this.resetPagination();
    // this.getAllList();
    // this.showCorporateFilter = false;
  }

  childSelectLabelInit() {
    switch (this.activeTab) {
      case ListTabsConsult.NOLEGGIOMBR:
      case ListTabsConsult.NOLEGGIOMBCHW:
      case ListTabsConsult.OPDIRETTE:
      case ListTabsConsult.SUBENTRI:
        this.childSelectLabel = 'STATES';
        break;
      case ListTabsConsult.DATAMODELLO:
        this.childSelectLabel = 'MODEL';
        break;
      case ListTabsConsult.DATAWQSOSPESECONCESSIONARIO:
        this.childSelectLabel = 'DEALER';
        break;
      case ListTabsConsult.PROPESITATEFINAL:
        this.childSelectLabel = 'ASSET';
        break;
      case ListTabsConsult.DATAVENDITORE:
        this.childSelectLabel = 'SELLER';
        break;
      case ListTabsConsult.DATASTATO:
        this.childSelectLabel = 'STATE';
        break;
    }
  }

  childTextLabelInit() {
    switch (this.activeTab) {
      case ListTabsConsult.NUMPROP:
        this.childFirstTextLabel = 'NUMERO PROPOSTA';
        this.childSecondTextLabel = null;
        break;
      case ListTabsConsult.DATACLIENTE:
        this.childFirstTextLabel = 'CLIENTE';
        this.childSecondTextLabel = null;
        break;
      case ListTabsConsult.CFPIVA:
        this.childFirstTextLabel = 'PARTITA IVA';
        this.childSecondTextLabel = 'CODICE FISCALE';
        break;
      case ListTabsConsult.TARGATELAIO:
        this.childFirstTextLabel = 'TARGA';
        this.childSecondTextLabel = 'TELAIO';
        break;
      case ListTabsConsult.NUMCONTRATTO:
        this.childFirstTextLabel = 'NUMERO CONTRATTO';
        this.childSecondTextLabel = null;
        break;
    }
  }

  filterListsInit() {
    this.cdsFilters = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
    this.derogaFilters = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
    this.documentazioneFilters = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
    this.lineaProdottoFilters = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
    this.retailFilters = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
    this.soggettoFilters = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
    this.filterList = [
      {key: 'dal servizio', value: 'dal servizio'},
      {key: 'quale servizio?', value: 'quale servizio'},
      {key: 'questo sconosciuto', value: 'questo sconosciuto'}
    ];
  }
}
