import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultAdvancedFiltersComponent } from './consult-advanced-filters.component';

describe('ConsultAdvancedFiltersComponent', () => {
  let component: ConsultAdvancedFiltersComponent;
  let fixture: ComponentFixture<ConsultAdvancedFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultAdvancedFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultAdvancedFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
