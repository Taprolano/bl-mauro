import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-consult-filter-date-text',
  templateUrl: './consult-filter-date-text.component.html',
  styleUrls: ['./consult-filter-date-text.component.scss']
})
export class ConsultFilterDateTextComponent implements OnInit {

  endDate = '';
  inputText = '';
  @Input() textLabel;
  @Output() textValue = new EventEmitter<string>();
  @Input() subtitleLabel;
  @Output() dateValue = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

}
