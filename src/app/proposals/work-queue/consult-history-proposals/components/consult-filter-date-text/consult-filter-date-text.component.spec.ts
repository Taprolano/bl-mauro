import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultFilterDateTextComponent } from './consult-filter-date-text.component';

describe('ConsultFilterDateTextComponent', () => {
  let component: ConsultFilterDateTextComponent;
  let fixture: ComponentFixture<ConsultFilterDateTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultFilterDateTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultFilterDateTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
