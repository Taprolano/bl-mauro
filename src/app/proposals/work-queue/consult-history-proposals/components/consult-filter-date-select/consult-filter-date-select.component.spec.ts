import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultFilterDateSelectComponent } from './consult-filter-date-select.component';

describe('ConsultFilterDateSelectComponent', () => {
  let component: ConsultFilterDateSelectComponent;
  let fixture: ComponentFixture<ConsultFilterDateSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultFilterDateSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultFilterDateSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
