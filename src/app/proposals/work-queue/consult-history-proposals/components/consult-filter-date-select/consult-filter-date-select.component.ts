import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ListValues} from '../../../../../data-model/class/ListValues';

@Component({
    selector: 'app-consult-filter-date-select',
    templateUrl: './consult-filter-date-select.component.html',
    styleUrls: ['./consult-filter-date-select.component.scss']
})
export class ConsultFilterDateSelectComponent implements OnInit {

    @Input() reset = false;
    @Input() subtitleLabel;
    @Input() listLabel;
    @Input() filterList = [
        {key: 'giorgio', value: 'giorgio'},
        {key: 'giorgio', value: 'giorgio'},
    ];
    endDate = '';

    @Output() dateValue = new EventEmitter<string>();
    @Output() selectValues = new EventEmitter<ListValues[]>();

    constructor() {
    }

    ngOnInit() {
    }


}
