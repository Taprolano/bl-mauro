import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultFilterWorkQueueComponent } from './consult-filter-work-queue.component';

describe('ConsultFilterWorkQueueComponent', () => {
  let component: ConsultFilterWorkQueueComponent;
  let fixture: ComponentFixture<ConsultFilterWorkQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultFilterWorkQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultFilterWorkQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
