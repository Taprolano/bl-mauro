import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ValueList} from '../../../../../data-model/class/ListValues';

@Component({
    selector: 'app-consult-filter-work-queue',
    templateUrl: './consult-filter-work-queue.component.html',
    styleUrls: ['./consult-filter-work-queue.component.scss']
})
export class ConsultFilterWorkQueueComponent implements OnInit {

    @Input() lineaProdottoFilters: ValueList[];
    @Output() lineaProdottoValue = new EventEmitter();

    @Input() cdsFilters: ValueList[];
        // = [
        // {
        //     value: 'Banche dati in errore',
        //     key: 'Banche dati in errore'
        // },
        // {
        //     value: 'CDS Automatiche',
        //     key: 'CDS Automatiche'
        // },
        // {
        //     value: 'CDS Manuali',
        //     key: 'CDS Manuali'
        // },
        // {
        //     value: 'Senza CDS o errore',
        //     key: 'Senza CDS o errore'
        // }];
    @Output() cdsValue = new EventEmitter();

    @Input() retailFilters: ValueList[];
        // = [
        // {
        //     value: 'Retail',
        //     key: 'Retail'
        // },
        // {
        //     value: 'K2K',
        //     key: 'K2K'
        // },
        // {
        //     value: 'Click & Go OK',
        //     key: 'Click & Go OK'
        // }];
    @Output() retailValue = new EventEmitter();

    @Input() documentazioneFilters: ValueList[];
        // = [
        // {
        //     value: 'Documenti non Recapitati',
        //     key: 'Documenti non Recapitati'
        // },
        // {
        //     value: 'Documentazione Completa',
        //     key: 'Documentazione Completa'
        // },
        // {
        //     value: 'Documentazione da Controllare',
        //     key: 'Documentazione da Controllare'
        // },
        // {
        //     value: 'Documenti Firmati',
        //     key: 'Documenti Firmati'
        // }];
    @Output() documentazioneValue = new EventEmitter();

    @Input() derogaFilters: ValueList[];
        // = [
        // {
        //     value: 'Senza deroga comm',
        //     key: 'Senza deroga comm'
        // },
        // {
        //     value: 'Deroga comm. valutata',
        //     key: 'Deroga comm. valutata'
        // },
        // {
        //     value: 'Deroga comm. non valutata',
        //     key: 'Deroga comm. non valutata'
        // }];
    @Output() derogaValue = new EventEmitter();

    @Input() soggettoFilters: ValueList[];
        // = [
        // {
        //     value: 'Persona Fisica',
        //     key: 'Persona Fisica'
        // },
        // {
        //     value: 'Libero Professionista',
        //     key: 'Libero Professionista'
        // },
        // {
        //     value: 'Ditta Individuale',
        //     key: 'Ditta Individuale'
        // },
        // {
        //     value: 'Societa',
        //     key: 'Societa'
        // }];
    @Output() soggettoValue = new EventEmitter();


    @Input() reset = false;

    constructor() {
    }

    ngOnInit() {
    }

}
