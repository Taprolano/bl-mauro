import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultFilterTextComponent } from './consult-filter-text.component';

describe('ConsultFilterTextComponent', () => {
  let component: ConsultFilterTextComponent;
  let fixture: ComponentFixture<ConsultFilterTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultFilterTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultFilterTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
