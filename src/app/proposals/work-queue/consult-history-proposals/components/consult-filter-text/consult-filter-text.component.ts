import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-consult-filter-text',
    templateUrl: './consult-filter-text.component.html',
    styleUrls: ['./consult-filter-text.component.scss']
})
export class ConsultFilterTextComponent implements OnInit {

    firstTextValue = '';
    secondTextValue = '';

    @Input() subtitleLabel;
    @Input() firstLabel;
    @Output() firstValue = new EventEmitter<string>();
    @Input() secondLabel;
    @Output() secondValue = new EventEmitter<string>();

    constructor() {
    }


    ngOnInit() {

    }

}
