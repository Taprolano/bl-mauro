import {Component, OnInit} from '@angular/core';
import {AccordionListAbstract} from '../../components/accordion-list/accordion-list.abstract';
import {AbstractModalService} from 'src/app/shared/components/abstract-modal/abstract-modal.service';
import {LoggerService} from '../../../shared/services/logger.service';
import {TranslateService} from '@ngx-translate/core';
import {IFilterTab} from '../../../data-model/interface/IFilterTab';
import {NOTIFICATION_TYPE, NotificationMessage} from '../../../shared/components/notification/NotificationMessage';
import {NotificationService} from '../../../shared/components/notification/notification.service';
import {RequestManagerService} from '../../../shared/services/request-manager.service';
import {ListTabsConsult} from './EListTabsConsult';
import {UtilsService} from '../../../shared/services/utils.service';

const __startPage = 0;

/**
 * @ignore
 */
const __maxNumberOfTabs = 4;

const __retailTabs: IFilterTab[] = [
    {label: 'Tutte', key: ListTabsConsult.ALL, selected: true},
    {label: 'Proposte in preistruttoria/istruttoria', key: ListTabsConsult.PROPIP, selected: false},
    {label: 'Proposte in attesa di esito CAS', key: ListTabsConsult.PROPCAS, selected: false},
    {label: 'Proposte K2K', key: ListTabsConsult.PROPK2K, selected: false},
    {label: 'Data + Cliente', key: ListTabsConsult.DATACLIENTE, selected: false},
    {label: 'Data', key: ListTabsConsult.DATA, selected: false},
    {label: 'Numero Proposta', key: ListTabsConsult.NUMPROP, selected: false},
    {label: 'Codice Fiscale e Partita Iva', key: ListTabsConsult.CFPIVA, selected: false},
    {label: 'Numero Contratto', key: ListTabsConsult.NUMCONTRATTO, selected: false},
    {label: 'Targa e Telaio', key: ListTabsConsult.TARGATELAIO, selected: false},
    {label: 'Noleggio MBR', key: ListTabsConsult.NOLEGGIOMBR, selected: false},
    {label: 'Noleggio MBCHW', key: ListTabsConsult.NOLEGGIOMBCHW, selected: false},
    {label: 'Operazioni Dirette', key: ListTabsConsult.OPDIRETTE, selected: false},
    {label: 'Subentri', key: ListTabsConsult.SUBENTRI, selected: false},
    {label: 'Elenco Proposte in Work Queue', key: ListTabsConsult.PROPWORKQUEUE, selected: false},
    {label: 'Data + Modello', key: ListTabsConsult.DATAMODELLO, selected: false},
    {label: 'Concessionario + Data WQ + Sospese', key: ListTabsConsult.DATAWQSOSPESECONCESSIONARIO, selected: false},
    {label: 'Elenco Proposte esitate e finalizzate', key: ListTabsConsult.PROPESITATEFINAL, selected: false},
    {label: 'Data + Venditore', key: ListTabsConsult.DATAVENDITORE, selected: false},
    {label: 'Data + Stato', key: ListTabsConsult.DATASTATO, selected: false},
];

@Component({
    selector: 'app-consult-history-proposals',
    templateUrl: './consult-history-proposals.component.html',
    styleUrls: ['./consult-history-proposals.component.scss']
})
export class ConsultHistoryProposalsComponent extends AccordionListAbstract implements OnInit {

    /**@ignore*/
    readonly __widths = ['0%', '0%', '0%', '0%', '0%', '0%', '0%', '0%', '%0', '%0',
        //  "0%", "0%", "0%", "0%", "0%", "0%", "0%", "0%", "%0", "%0",
        //  "0%", "0%", "0%", "0%", "0%", "0%", "0%", "0%", "%0", "%0"
    ];

    /**@ignore*/
    readonly headers = ['PROPOSAL', 'STATE', 'CONTRACT', 'ASSET', 'CLIENT_EXT', 'DATA', 'DATA_BENESTARE', 'PF', 'CAMPAIGN', '' // ,"IP", "DOC", "URG",  "BRAND"
        // , "CLS", "ST", "ASSIGNEE", "APPLICANT", "USER", "ANALIST", "RAGGR", "CS", "SUSPENSION",
        //  "SD", "DDM", "SND_FIRMATARY", , "EXPOSURE", "ISTR", "CAMPAIGN",
        // "EXPOSURE", "CAMP"
    ];
    /**@ignore*/
    readonly __wrapperHeight = '400px';

    /**@ignore*/
    readonly objectKeys = ['proposta', 'stato', 'cli', 'ip', 'doc', 'urgent', 'data', 'brand', 'pf',
        'contract', 'cls', 'st', 'assegnee', 'user', 'analist', 'raggr', 'cs', 'suspension', 'sd', 'ddm',
        'secondFirmatary', 'asset', 'esposizione', 'istruttoria', 'campagna'];

    /**
     * numero proposte per tab
     */
    numberAllProposals: number;
    numberIstruttoriaProposals: number;
    numberK2KProposals: number;
    numberCASProposals: number;
    numberDataCliente: number;
    numberData: number;
    numberNumProposta: number;
    numberCodiceFiscalePartitaIva: number;
    numberNumContratto: number;
    numberTargaTelaio: number;
    numberNoleggioMBR: number;
    numberNoleggioMBCHW: number;
    numberOperazioniDirette: number;
    numberSubentri: number;
    numberWorkQueueProposals: number;
    numberDataModello: number;
    numberDataWorkQueueSospeseConcessionario: number;
    numberEsitateFinalProposals: number;
    numberDataVenditore: number;
    numberDataStato: number;

    /**
     * Lista di oggetti che vengono mostrati in tabella
     */
    printList = [];

    /**
     * @ignore
     */
    tabKey: string;

    /**
     * lista delle tab selezionabili per corporate
     */
    tabs: IFilterTab[] = [];

    /** Tab correntemente attiva */
    activeTab: string = ListTabsConsult.ALL;

    tabsEnum = ListTabsConsult;
    /**
     * lista delle tab possibili per corporate
     */
    possibleTabs: IFilterTab[] = [];

    /**
     * flag per mostrare i filtri avanzati
     */
    showCorporateFilter: boolean;

    /**
     * flag per mostrare la view per aggiungere i tab
     */
    addingTab: boolean;

    /**
     * restituisce il numero di tab attivi
     * @returns {number}
     */
    get numberOfTabs(): number {
        let counter = 0;
        for (const tab of this.possibleTabs) {
            if (tab.selected) {
                counter++;
            }
        }

        return counter;
    }

    /**
     * restituisce il numero massimo di tab che è possibile attivare
     * @returns {number}
     */
    get maxNumberOfTabs(): number {
        return __maxNumberOfTabs;
    }


    constructor(
        private _requestManagerService: RequestManagerService,
        private _notification: NotificationService,
        private _utils: UtilsService,
        _abstractModalService: AbstractModalService,
        translate: TranslateService,
        _logger: LoggerService) {
        super(_abstractModalService, translate, _logger);
    }

    ngOnInit() {
        this.numberAllProposals = 0;
        this.numberIstruttoriaProposals = 0;
        this.numberK2KProposals = 0;
        this.numberCASProposals = 0;
        this.numberDataCliente = 0;
        this.numberData = 0;
        this.numberNumProposta = 0;
        this.numberCodiceFiscalePartitaIva = 0;
        this.numberNumContratto = 0;
        this.numberTargaTelaio = 0;
        this.numberNoleggioMBR = 0;
        this.numberNoleggioMBCHW = 0;
        this.numberOperazioniDirette = 0;
        this.numberSubentri = 0;
        this.numberWorkQueueProposals = 0;
        this.numberDataModello = 0;
        this.numberDataWorkQueueSospeseConcessionario = 0;
        this.numberEsitateFinalProposals = 0;
        this.numberDataVenditore = 0;
        this.numberDataStato = 0;
        this.possibleTabs = __retailTabs;
        this.tabKey = ListTabsConsult.ALL;
        this.getAllList();
        this.getNumProposalTabs();
    }

    getWidth(key): string {
        return '';
    }

    /**
     * seleziona il tab passato come parametro
     * @param {string} key
     */
    setTab(key: string): void {
        if (this.addingTab) {
            this._notification.sendNotification(
                new NotificationMessage(this.translate.instant('ADDING_TAB'), NOTIFICATION_TYPE.WARN)
            );
            return;
        }
        if (this.tabKey != key) {
            this.tabKey = key;
            this.onTabKeyChange(key);
        }
    }

    /**
     * imposta le tab per i filtri base
     */
    setTabs() {
        this.tabs = [];
        for (const tab of this.possibleTabs) {
            const tabToAdd: IFilterTab = this._utils.assign(tab);
            if (tab.selected) {
                this.tabs.push(tabToAdd);
            }
        }
        this.addingTab = false;
        this.manageSelectedTabs(this.tabs);
    }

    manageSelectedTabs(tabs: IFilterTab[]): void {
        for (const tab of tabs) {
            if (tab.label.indexOf('(') <= 0) {
                switch (tab.key) {
                    case ListTabsConsult.ALL: {
                        tab.label += `    (${this.numberAllProposals})`;
                        break;
                    }
                    case ListTabsConsult.PROPIP: {
                        tab.label += `    (${this.numberIstruttoriaProposals})`;
                        break;
                    }
                    case ListTabsConsult.PROPESITATEFINAL: {
                        tab.label += `    (${this.numberEsitateFinalProposals})`;
                        break;
                    }
                    case ListTabsConsult.PROPWORKQUEUE: {
                        tab.label += `    (${this.numberWorkQueueProposals})`;
                        break;
                    }
                    case ListTabsConsult.PROPCAS: {
                        tab.label += `    (${this.numberCASProposals})`;
                        break;
                    }
                    case ListTabsConsult.PROPK2K: {
                        tab.label += `    (${this.numberK2KProposals})`;
                        break;
                    }
                    case ListTabsConsult.DATA: {
                        tab.label += `    (${this.numberData})`;
                        break;
                    }
                    case ListTabsConsult.DATASTATO: {
                        tab.label += `    (${this.numberDataStato})`;
                        break;
                    }
                    case ListTabsConsult.DATAVENDITORE: {
                        tab.label += `    (${this.numberDataVenditore})`;
                        break;
                    }
                    case ListTabsConsult.DATAWQSOSPESECONCESSIONARIO: {
                        tab.label += `    (${this.numberDataWorkQueueSospeseConcessionario})`;
                        break;
                    }
                    case ListTabsConsult.DATACLIENTE: {
                        tab.label += `    (${this.numberDataCliente})`;
                        break;
                    }
                    case ListTabsConsult.DATAMODELLO: {
                        tab.label += `    (${this.numberDataModello})`;
                        break;
                    }
                    case ListTabsConsult.NUMPROP: {
                        tab.label += `    (${this.numberNumProposta})`;
                        break;
                    }
                    case ListTabsConsult.NUMCONTRATTO: {
                        tab.label += `    (${this.numberNumContratto})`;
                        break;
                    }
                    case ListTabsConsult.SUBENTRI: {
                        tab.label += `    (${this.numberSubentri})`;
                        break;
                    }
                    case ListTabsConsult.OPDIRETTE: {
                        tab.label += `    (${this.numberOperazioniDirette})`;
                        break;
                    }
                    case ListTabsConsult.NOLEGGIOMBCHW: {
                        tab.label += `    (${this.numberNoleggioMBCHW})`;
                        break;
                    }
                    case ListTabsConsult.NOLEGGIOMBR: {
                        tab.label += `    (${this.numberNoleggioMBR})`;
                        break;
                    }
                    case ListTabsConsult.CFPIVA: {
                        tab.label += `    (${this.numberCodiceFiscalePartitaIva})`;
                        break;
                    }
                    case ListTabsConsult.TARGATELAIO: {
                        tab.label += `    (${this.numberTargaTelaio})`;
                        break;
                    }
                }
            }
        }
        // se deselezionato il tab corrente
        let changeTab = true;
        for (const selectedTab of this.tabs) {
            if (selectedTab.key == this.tabKey) {
                changeTab = false;
                break;
            }
        }
        if (changeTab) {
            this.setTab(this.tabs[0].key);
        }
    }

    /**
     * Apre/chiude pannello dei filtri (se non è aperto il pannello dei tab)
     */
    onFilterClick() {
        if (this.addingTab) {
            this._notification.sendNotification(
                new NotificationMessage(this.translate.instant('ADDING_TAB'), NOTIFICATION_TYPE.WARN)
            );
            return;
        }
        this.showCorporateFilter = !this.showCorporateFilter;
        // this.addingTab = false;
    }

    /**
     * Apre/chiude pannello di selezione dei tab visibili. All'apertura, chiude e disabilita temporaneamente
     * il pannello dei filtri.
     */
    onTabSelectorClick() {
        this.addingTab = !this.addingTab;
        this.showCorporateFilter = false;
    }


    /* chiede al backend la lista proposte */
    getAllList() {
        // spinner e logger
        // this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSALS_LIST, REQUEST_PRIORITY.HIGH);
        // set parametri richiesta
        // let req = this.requestParamiters;
        // req.setParamsFromFilter(this.filterParamsAll);
        // req.sort = this.sortString;
        // req.tipoCoda = AuthConstant.CODA_CREDITO_RETAIL;
        // req.page = this.pageProposals.page;
        // req.size = AppSettings.PROPOSALS2PAGE;
    }

    /* resetta la lista di proposte alla prima pagina */
    resetPagination() {
        // this.pageProposals.page = __startPage;
    }

    cleanTabData() {
        // resetta ordinamento default
        this.sortString = '';
        // svuota dati non in uso
        this.printList = [];
        this.proposalsList = [];
        this.resetPagination();
    }

    /**
     * popola i contatori dei tab delle proposte
     */
    getNumProposalTabs() {
        this.checkSelectableTabs();
        // this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, REQUEST_PRIORITY.HIGH);
        // this.beProposalService.getNumbersProposalTab(AuthConstant.CODA_CREDITO_RETAIL).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
        //     response => {
        //         if (response.code == AppSettings.HTTP_OK) {
        //             this.numberBlockedPropoposals = response.object.numBloccate;
        //             this.numberReproposedProposals = response.object.numRiproposta;
        //             this.numberAllProposals = response.object.numTotali;
        //             this.numberAssignedToMeProposals = response.object.numAssegnateAMe;
        //             this.numberUrgentProposals = response.object.numUrgenti;
        //             this.numberInCheckDocProposals = response.object.numInControlloDocumentale;
        //             this.numberInInvestigationProposals = response.object.numInInstruttoria;
        //             this.numberSecondApprPropoposals = response.object.numSecondaApprovazione;
        //             this.numberIncompleteDocPropoposals = response.object.numDocumentazionIncompleta;
        //             this.numberSuspendeProposals = response.object.numSospese;
        //
        //         } else {
        //             this._logger.logError(CreditRetailComponent.name, 'getSelectBoxValues', "ERRORE NON IN FALLBACK", response.code);
        //         }
        //         this.checkSelectableTabs();
        //         setTimeout(() => {
        //             this._requestManagerService.handleRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, response.code);
        //         }, AppSettings.TIMEOUT_SPINNER);
        //     },
        //     error => {
        //         this._logger.logError(CreditRetailComponent.name, 'getNumProposalTabs', error);
        //         let errResp = this._utils.getErrorResponse(error);
        //         this._logger.logError(CreditRetailComponent.name, 'getNumProposalTabs', "error", errResp);
        //         setTimeout(() => {
        //             this._requestManagerService.handleRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, error.code, error.message);
        //         }, AppSettings.TIMEOUT_SPINNER);
        //     }
        // );
    }

    /**
     * popola le tab selezionabili
     */
    checkSelectableTabs() {
        for (let tab of this.possibleTabs) {
            if (tab.selected) {
                let tabToAdd: IFilterTab = this._utils.assign(tab);
                this.tabs.push(tabToAdd);
                this.manageSelectedTabs(this.tabs);
            }
        }
    }

    /**
     * Mostra il tab selezionato
     * @param {string} tabKey
     */
    onTabKeyChange(tabKey: string): void {
        this.cleanTabData();
        // imposta tab
        this.activeTab = tabKey;
        // this.requestParamiters = new CreditQueueAllRequestParameters();
        // switch (tabKey) {
        //     case ListTabs.ALL: {
        //         this.requestParamiters.sort = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC;
        //         break;
        //     }
        //     case ListTabs.URGENT: {
        //         this.requestParamiters.vista = WorkQueueFilterView.URGENTI;
        //         break;
        //     }
        //     case ListTabs.ASSIGNED_TO_ME: {
        //         this.requestParamiters.vista = WorkQueueFilterView.ASSEGNATE_A_ME;
        //         break;
        //     }
        //     case ListTabs.DOCUMENT_CHECK: {
        //         this.requestParamiters.vista = WorkQueueFilterView.IN_CONTROLLO_DOCUMENTALE;
        //         break;
        //     }
        //     case ListTabs.BLOCKED: {
        //         this.requestParamiters.vista = WorkQueueFilterView.BLOCCATE;
        //         break;
        //     }
        //     case ListTabs.DOCUMENT_INCOMPLETE: {
        //         this.requestParamiters.vista = WorkQueueFilterView.DOCUMENTAZIONE_INCOMPLETA;
        //         break;
        //     }
        //     case ListTabs.SUSPENDED: {
        //         this.requestParamiters.vista = WorkQueueFilterView.SOSPESE;
        //         break;
        //     }
        //     case ListTabs.SECOND_APPROVATION: {
        //         this.requestParamiters.vista = WorkQueueFilterView.SECONDA_APPROVAZIONE;
        //         break;
        //     }
        //     case ListTabs.REPROPOSED: {
        //         this.requestParamiters.vista = WorkQueueFilterView.RIPROPOSTA;
        //         break;
        //     }
        //     case ListTabs.IN_INVESTIGATION: {
        //         this.requestParamiters.vista = WorkQueueFilterView.IN_ISTRUTTORIA;
        //         break;
        //     }
        //     // case ListTabs.INERROR: {
        //     //     this.requestParamiters.vista = WorkQueueFilterView.BLOCCATE;
        //     //     break;
        //     // }
        // }
        // imposto pagina e size a tutti TODO valutare se settarlo di default nel classe astratta
        // this.requestParamiters.size = AppSettings.PROPOSALS2PAGE;
        // this.requestParamiters.page = __startPage;

        // Chiama la lista di proposte del tab selezionato
        this.getAllList();

        // per tab in sviluppo
        // this.showWIP = !([ListTabs.ALL, ListTabs.MBCPOS, ListTabs.VDZ, ListTabs.SIMULATIONS].includes(tabKey));
    }
}
