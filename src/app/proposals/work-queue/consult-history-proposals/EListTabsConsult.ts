/**
 * Tab selezionabili per la consultazione delle proposte
 */
export enum ListTabsConsult {
    ALL = 'all',
    PROPIP = 'propIstruttoria',
    PROPCAS = 'propAttesaCAS',
    PROPK2K = 'propK2K',
    DATACLIENTE = 'dataCliente',
    DATA = 'data',
    NUMPROP = 'numeroProposte',
    CFPIVA = 'codiceFiscalePartitaIva',
    NUMCONTRATTO = 'numeroContratto',
    TARGATELAIO = 'targaTelaio',
    NOLEGGIOMBR = 'noleggioMBR',
    NOLEGGIOMBCHW = 'noleggioMBCHW',
    OPDIRETTE = 'operazioniDirette',
    SUBENTRI = 'subentri',
    PROPWORKQUEUE = 'proposteWorkQueue',
    DATAMODELLO = 'dataModello',
    DATAWQSOSPESECONCESSIONARIO = 'datawqSospeseConcessionario',
    PROPESITATEFINAL = 'proposteEsisateFinalizzate',
    DATAVENDITORE = 'dataVenditore',
    DATASTATO = 'dataStato',
}
