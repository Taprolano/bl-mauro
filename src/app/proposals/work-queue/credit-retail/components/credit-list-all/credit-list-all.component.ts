import {Component, OnInit} from '@angular/core';
import {LoggerService} from "../../../../../shared/services/logger.service";
import {TranslateService} from "@ngx-translate/core";
import {ProposalsConstant} from "../../../../proposalsConstant";
import {AbstractModalService} from "../../../../../shared/components/abstract-modal/abstract-modal.service";
import {IDynamicTableInput} from "../../../../../data-model/interface/IDynamicTableInput";
import {DynamicTableComponent} from "../../../../../shared/components/dynamic-table/dynamic-table.component";
import {AccordionListAbstract} from "../../../../components/accordion-list/accordion-list.abstract";
import {CreditWorkQueue} from "../../../../../data-model/class/CreditWorkQueue";
import {ProposalsRoutingConstant} from '../../../../proposalsRoutingConstant';
import {Router} from '@angular/router';
import {PersistenceService} from "../../../../../shared/services/persistence.service";

@Component({
    selector: 'app-credit-list-all',
    templateUrl: './credit-list-all.component.html',
    styleUrls: ['./credit-list-all.component.scss']
})
export class CreditListAllComponent extends AccordionListAbstract implements OnInit {

    /**@ignore*/
    readonly __widths = ["0%", "0%", "0%", "0%", "0%", "0%", "0%", "0%", "%0", "%0"];
    /**@ignore*/
    readonly headers = ["PROPOSAL", "APPLICANT", "CLI", "ARRIVAL", "PRELIMINARY", "CATEGORY_ASSET", "USER", "SCORE", "EXPOSURE", "CAMP", "" /*,"REVIVED", "ACTION"*/];

    //TODO: verificare i campi mancati rispetto all'invision -> ISTRUTTORIA, PRODOTTO

    /**@ignore*/
    readonly objectKeys = CreditWorkQueue.keys;

    /**@ignore*/
    readonly __wrapperHeight = "400px";

    /**@ignore*/
    readonly sortKeys = ['proposta', 'tipo di soggetto', 'arrivo', 'istruttoria', 'prodotto', 'assegnatario', 'richiedente', 'score', 'esposizione'];

    /**@ignore*/
    constructor(_abstractModalService: AbstractModalService,
                translate: TranslateService,
                _logger: LoggerService,
                private _persistence: PersistenceService,
                private _router: Router) {
        super(_abstractModalService, translate, _logger);
    }

    //fornire ordinamento di default?
    // ngOnInit(){
    //     super.ngOnInit();
    //     this.sortEvent.emit('')
    // }

    /**
     * Apre modale con descrizione stati proposta
     */
    openStatePopup() {
        //disabilita scroll
        //input per dynamic table
        let id = "stateTypesModal";
        let headers = ["Stato", "Descrizione"];
        let data = [];
        ProposalsConstant.PROPOSAL_STATES_4_FILTER_BAR.slice(1)
            .sort((a, b) => ProposalsConstant.stateSortAlg(a, b))
            .forEach(elem => {
                data.push([elem.id, this.translate.instant(elem.label)]);
            });
        let inputData: IDynamicTableInput = {headers, data};
        let cssClasses = ['propStatesList'];
        let modal = true;
        //crea modale
        let popupComponent = {
            component: DynamicTableComponent,
            parameters: {id, inputData, cssClasses, modal}
        };
        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, popupComponent);
    };

    /**
     * @param {number} code
     * @returns {any}
     * Funzione che riporta la stringa tradotta corrispondente al codice dello stato della proposta
     */
    getDescriptionFromCode(code: number) {
        let statesList = ProposalsConstant.PROPOSAL_STATES_4_FILTER_BAR;

        for (let state of statesList) {
            if (state.id == code) {
                return this.translate.instant(state.label);
            }
        }

        return "STR" + code;
    }

    /**
     * Naviga verso la pagina di consultazione
     */
    goToConsultation(idProposal: string, processInstanceID: number): void {

        let obj = {
            idProposal: idProposal,
            processInstanceID: processInstanceID
        };

        this._persistence.save("SELECT_PROPOSAL", obj);
        this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueConsultation')]);
    }

    mapTipoDiSoggetto(subject) : string{
        let app : string;
        switch (subject.toLowerCase()) {
            case 'persona fisica':
                app = 'PF';
                break;
            case 'libero professionista':
                app = 'LIB';
                break;
            case 'ditta individuale':
                app = 'DI';
                break;
            case "societa'":
                app = 'SOC';
                break;
            default:
                app = 'N/D';
                break;
        }
        return app;
    }

    getWidth(key) : string{
        let app : string = '';
        switch (key) {
            case 'CAMP':
                app = '80px';
                break;
            case 'campagna':
                app = '80px';
                break;
            case 'CLI':
                app = '70px';
                break;
            case 'tipoDiSoggetto':
                app = '70px';
                break;
            case 'SCORE':
                app = '80px';
                break;
            case 'score':
                app = '80px';
                break;
            default:
                app = '';
                break;
        }
        return app;
    }
}
