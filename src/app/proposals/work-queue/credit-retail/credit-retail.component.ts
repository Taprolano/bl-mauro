import {Component, OnDestroy, OnInit} from '@angular/core';
import {IPagination} from "../../../data-model/interface/IPagination";
import {IFilterOption} from "../../../data-model/interface/IFilterOption";
import {AppSettings} from "../../../../AppSettings";
import {IFilterChoice} from "../../../data-model/interface/IFilterChoice";
import {Subject} from 'rxjs/index';
import {Order} from "../../../data-model/interface/EOrder";
import {ListOrderKeys} from "../../../data-model/interface/ListOrderKeys";
import {ProposalService} from "../../../shared/services/proposal.service";
import {ProposalWorkflowService} from "../../services/proposal-workflow.service";
import {BeWorkQueue} from "../../services/be-work-queue.service";
import {TranslateService} from "@ngx-translate/core";
import {HeaderService} from "../../../shared/services/header.service";
import {REQUEST_PRIORITY, RequestManagerService} from "../../../shared/services/request-manager.service";
import {Router} from "@angular/router";
import {UserProfileService} from "../../../shared/services/user-profile.service";
import {NotificationService} from "../../../shared/components/notification/notification.service";
import {UtilsService} from "../../../shared/services/utils.service";
import {LoggerService} from "../../../shared/services/logger.service";
import {RequestManagerConstant} from "../../requestManagerConstant";
import {CreditQueueAllRequestParameters} from "../../../data-model/class/ProposalListAllRequestParameters";
import {AuthConstant} from "../../../../assets/authConstant";
import {CreditWorkQueue} from "../../../data-model/class/CreditWorkQueue";
import {IFilterTab} from '../../../data-model/interface/IFilterTab';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ValueList} from '../../../data-model/class/ListValues';
import {WorkQueueFilterView} from '../EWorkQueueFilterView';
import {takeUntil} from 'rxjs/operators';
import {ListTabs} from '../EListTabs';
import { NotificationMessage, NOTIFICATION_TYPE } from '../../../shared/components/notification/NotificationMessage';

const __filterPlaceholder = 'Cerca per proposta, contratto o soggetto.';
const __startPage = 0;


const __keysChoice: IFilterChoice[] = [
    { value: '0', label: 'Nuova proposta', selected: false },
    { value: 'modifyProposal', label: 'Proposta in modifica', selected: false },
    { value: 'benRequest', label: 'Richiesta benestare', selected: false },
    { value: 'failedRequest', label: 'Richiesta numerazione fallita', selected: false },

    { value: 'notAcptProp', label: 'Proposta non accettata', selected: false },
    { value: 'acptProp', label: 'Proposta accettata', selected: false },
    { value: 'acptModProp', label: 'Proposta accettata con modifiche', selected: false },
    { value: 'numRequest', label: 'Richiesta numerazione', selected: false },


    { value: 'refusedNum', label: 'Numerazione rifiutata', selected: false },
    { value: 'execNum', label: 'Numerazione eseguita', selected: false },
    { value: 'printContract', label: 'Contratto stampato', selected: false },
    { value: 'actRequest', label: 'Richiesta attivazione', selected: false },

    { value: 'execActiv', label: 'Attivazione eseguita', selected: false },
    { value: 'refusedActiv', label: 'Attivazione rifiutata', selected: false },
    { value: 'failCancRequest', label: 'Richiesta cancellazione fallita', selected: false },
    { value: 'modifyProp', label: 'Proposta in modifica', selected: false }
];
const __filterOption: IFilterOption = {
    key: 'state',
    possibleChoices: __keysChoice
};
const __pageProposals: IPagination = {
    keys: null,
    numberOfResults: null,
    recordPerPage: AppSettings.ELEMENT2PAGE,
    page: __startPage,
    nPage: 0,
    size: AppSettings.PROPOSALS2PAGE
};

const __retailTabs: IFilterTab[] = [
    { label: "Tutte", key: ListTabs.ALL, selected: true },
    { label: "Urgenti", key: ListTabs.URGENT, selected: false },
    { label: "Assegnate a me", key: ListTabs.ASSIGNED_TO_ME, selected: false },
    { label: "In controllo documentale", key: ListTabs.DOCUMENT_CHECK, selected: false },
    { label: "Bloccate", key: ListTabs.BLOCKED, selected: false },
    { label: "Sospese", key: ListTabs.SUSPENDED, selected: false },
    { label: "Documentazione incompleta", key: ListTabs.DOCUMENT_INCOMPLETE, selected: false },
    { label: "Seconda Approvazione", key: ListTabs.SECOND_APPROVATION, selected: false },
    { label: "Riproposte", key: ListTabs.REPROPOSED, selected: false },
    { label: "In Istruttoria", key: ListTabs.IN_INVESTIGATION, selected: false }
];

/**
 * @ignore
 */
const __maxNumberOfTabs = 4;


const __subjectListCode: string = 'TipoSogg';
const __proposalTypeListCode: string = 'TipoProposta';
const __proposalDerogationListCode: string = 'TipoDeroga';
const __assetListCode: string = 'TipoAsset';
const __cdsListCode: string = 'TipoCds';
// const __clickAndGoListCode: string = 'TipoClickAndGo';
const __productTypeListCode: string = 'TipoProdotto';

@Component({
    selector: 'app-credit-retail',
    templateUrl: './credit-retail.component.html',
    styleUrls: ['./credit-retail.component.scss']
})
export class CreditRetailComponent implements OnInit, OnDestroy {

    private ngUnsubscribe = new Subject<void>();

    /*Oggetti risposta*/
    proposalsList: CreditWorkQueue[] = [];
    printList: CreditWorkQueue[] = [];


    /**
     * Parametri da fornire per la richiesta GET, inclusi filtri
     */
    requestParamiters: CreditQueueAllRequestParameters;

    /**
     * Maschera del filtro per lista proposto tab ALL
     * @type {{activationDateTo: null; activationDateFrom: null; contractNumber: null; proposalNumber: null; licensePlate: null; frameNumber: null; applicant: null; proposalState: null}}
     */
    /*filterParamsAll: IProposalAllRequest = {
        activationDateTo: null,
        activationDateFrom: null,
        contractNumber: null,
        proposalNumber: null,
        licensePlate: null,
        frameNumber: null,
        applicant: null,
        proposalState: null
    };*/



    /**@ignore semaforo per bloccare eventi multipli di scrollEnd che richiedono di scaricare nuove proposte */
    sem: boolean = false;

    newProposalButtonPosition: string;
    spaceFromBar: string;

    /**
     * @ignore
     * 'true' per usare il mock service
     */
    private httpTest = false;

    //notifica per mostrare componente "lista vuota"
    isEmptyList: boolean = false;

    //notifica per mostrare temporaneamente Work in progress se tab selezionato != 'tutte'
    showWIP: boolean = false;

    /* stringa di ordinamento corrente */
    private sortString: string = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC

    private pageProposals;


    /**@ignore Per utilizzare enum nel template*/
    readonly ListTabs = ListTabs;

    /** Tab correntemente attiva */
    activeTab: string = ListTabs.ALL;

    /**
   * @ignore
   */
    tabKey: string;

    /**
     * lista delle tab selezionabili per corporate
     */
    tabs: IFilterTab[] = [];

    /**
     * lista delle tab possibili per corporate
     */
    possibleTabs: IFilterTab[] = [];

    /**
     * flag per mostrare i filtri avanzati
     */
    showCorporateFilter: boolean;

    /**
     * flag per mostrare la view per aggiungere i tab
     */
    addingTab: boolean;

    /**
     * form filtri avanzati
     */
    advancedFilters: FormGroup;
    selectedSubjectTypes: any[];
    selectedDerogation: any[];
    selectedCDS: any[];

    /**
     * select box da popolare
     */
    subjectTypes: ValueList[] = [];
    proposalTypes: ValueList[] = [];
    proposalDerogations: ValueList[] = [];
    cds: ValueList[] = [];
    // clickAndGo: ValueList[] = [];

    /**
     * numero proposte per tab
     */
    numberAllProposals: number;
    numberUrgentProposals: number;
    numberAssignedToMeProposals: number;
    numberInCheckDocProposals: number;
    numberBlockedPropoposals: number;
    numberIncompleteDocPropoposals: number;
    numberSecondApprPropoposals: number;
    numberReproposedProposals: number;
    numberInInvestigationProposals: number;
    numberSuspendeProposals: number;

    numDocMancanti: number;

    /**
     *  campi per reset multi select
     */
    resetDerogation: boolean = false;
    resetSubjectTypes: boolean = false;
    resetCDS: boolean = false;


    constructor(
        private _proposalService: ProposalService,
        private _proposalWorkflowService: ProposalWorkflowService,
        private beProposalService: BeWorkQueue,
        private translate: TranslateService,
        private _headerService: HeaderService,
        private _requestManagerService: RequestManagerService,
        private _router: Router,
        private _userProfileService: UserProfileService,
        private _notification: NotificationService,
        private _utils: UtilsService,
        private _logger: LoggerService,
        private _formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.numberAllProposals = 0;
        this.numberUrgentProposals = 0;
        this.numberAssignedToMeProposals = 0;
        this.numberInCheckDocProposals = 0;
        this.numberBlockedPropoposals = 0;
        this.numberIncompleteDocPropoposals = 0;
        this.numberSecondApprPropoposals = 0;
        this.numberReproposedProposals = 0;
        this.numberInInvestigationProposals = 0;
        this.numDocMancanti = 0;
        this.possibleTabs = __retailTabs;
        this.tabKey = ListTabs.ALL;
        //assegna una copia per evitare problema di modifica della "costante" per riferimento
        this.pageProposals = this._utils.assign(__pageProposals);
        this._proposalService.clear();
        this._proposalService.updateStatus(null);
        //this._userProfileService.loadUserProfile();

        //this.variableToHeader();
        /*
                console.log('-----------------------------------------------------------');
                this._proposalService.printMemory();
                console.log('-----------------------------------------------------------');
                this._proposalService.clear();
                console.log('-----------------------------------------------------------');
                this._proposalService.printMemory();
                console.log('-----------------------------------------------------------');
        */

        this._proposalService.getMessage().subscribe(
            message => this._logger.logInfo(CreditRetailComponent.name, 'ngOnInit', "sentMessage - testMessage", message)
        );

        // this._proposalService.updateStatus(ProposalsConstant.stateProduct2Model);
        this.requestParamiters = new CreditQueueAllRequestParameters();
        this.requestParamiters.sort = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC;

        this.getAllList();

        this.getNumProposalTabs();

        this.getSelectBoxValues(__subjectListCode);
        this.getSelectBoxValues(__proposalTypeListCode);
        this.getSelectBoxValues(__proposalDerogationListCode);
        this.getSelectBoxValues(__cdsListCode);
        // this.getSelectBoxValues(__clickAndGoListCode);
        this.createForms();
        //rimuove sottoscrizione
        window.onbeforeunload = () => this.ngOnDestroy();
    }

    /**
    * popola le tab selezionabili
    */
    checkSelectableTabs() {
        for (let tab of this.possibleTabs) {
            if (tab.selected) {
                let tabToAdd: IFilterTab = this._utils.assign(tab);
                this.tabs.push(tabToAdd);
                this.manageSelectedTabs(this.tabs);
            }
        }
    }

    /**
     * restituisce il numero di tab attivi
     * @returns {number}
     */
    get numberOfTabs(): number {
        let counter = 0;
        for (let tab of this.possibleTabs) {
        if (tab.selected) counter++;
        }

        return counter;
    }

    /**
     * restituisce il numero massimo di tab che è possibile attivare
     * @returns {number}
     */
    get maxNumberOfTabs(): number {
        return __maxNumberOfTabs;
    }

    /**
     * imposta le tab per i filtri base
     */
    setTabs() {
        this.tabs = [];
        for (let tab of this.possibleTabs) {
        let tabToAdd: IFilterTab = this._utils.assign(tab);
        if (tab.selected) this.tabs.push(tabToAdd);
        }
        this.addingTab = false;
        this.manageSelectedTabs(this.tabs);
    }

    /* resetta la lista di proposte alla prima pagina */
    resetPagination() {
        this.pageProposals.page = __startPage;
    }

    /* chiede al backend la lista proposte */
    getAllList() {
        //spinner e logger
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSALS_LIST, REQUEST_PRIORITY.HIGH);
        this._logger.logInfo(CreditRetailComponent.name, 'getAllList', "Request parameters: ", this.requestParamiters);
        // set parametri richiesta
        let req = this.requestParamiters;
        // req.setParamsFromFilter(this.filterParamsAll);
        req.sort = this.sortString;
        req.tipoCoda = AuthConstant.CODA_CREDITO_RETAIL;
        req.page = this.pageProposals.page;
        req.size = AppSettings.PROPOSALS2PAGE;
        //sottoscrizione
        this.beProposalService.getCreditQueueList(req, this.httpTest).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    /*
                                        this.proposalsList = !!response.object ? response.object : [];
                                        this.printList = this.printList.concat(this.proposalsList);
                    */
                    let respObject = !!response.object ? response.object : null;
                    this.proposalsList = (!!respObject && !!respObject.object) ?respObject.object : [];
                    if(!!respObject && !!respObject.pageData){
                        this.pageProposals.totalPages = respObject.pageData.totalPages;
                        this.pageProposals.totalElements = respObject.pageData.totalElements;
                    }
                    this._logger.logInfo(CreditRetailComponent.name, 'getAllList', "allList: ", this.proposalsList);
                    // console.log("[List][getAllProposalsList] proposalsList: ", this.proposalsList);
                    if (this.pageProposals.page > __startPage) {
                        this.printList = this.printList.concat(this.proposalsList);
                    } else {
                        this.printList = this.proposalsList;
                    }
                    this.getNumDocmancanti();

                } else {
                    this._logger.logError(CreditRetailComponent.name, 'getAllList', "ERRORE NON IN FALLBACK", response.code);
                }
                //spinner e semaforo
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSALS_LIST, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            }, resp => {
                this._logger.logError(CreditRetailComponent.name, 'getAllProposalsList', resp);
                this.proposalsList = [];
                this.printList = this.proposalsList;
                let error = this._utils.getErrorResponse(resp);
                this._logger.logError(CreditRetailComponent.name, 'getAllProposalsList', "error", error);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSALS_LIST, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            });
    }

    /**
     * Scarica la nuova pagina dopo lo scroll relativa alla tab attiva
     */
    getNextPage() {
        if (!this.sem) {
            this.sem = true;
            this.pageProposals.page += 1;
            if(this.pageProposals.page < this.pageProposals.totalPages) {
                this.getAllList();
            }
        }
    }


    /* ngOnInit() {
         this.variableToHeader();
         this._proposalService.getMessage().subscribe(message => console.log('sentMessage - testMessage', message));

         this._proposalService.save(ProposalsConstant.stateProduct2Model, {
             cmp1: ''
         });
         console.log(this._proposalService.getObj2CurrentState());


         this.proposalsList.push({
             asset: 1,
             proposal: '100200746223',
             contract: '--',
             subject: 'Henry Richardson Henry Richardson',
             state: '0',
             date: '01-12-2020',
             deadline: '01-12-2020',
             priority: 'C',
             seller: 'Alfie Wood'
         });

         this.printList = this.proposalsList;
     }*/

    public get filterPlaceholder(): string {
        return __filterPlaceholder;
    }

    public get filterOption(): IFilterOption {
        return __filterOption;
    }


    /*goToNewProposal() {
        this._proposalWorkflowService.startNewProposal();
    }*/

    /* public variableToHeader() {
         this._headerService.setVariable(true);
     }*/

    ngAfterContentChecked() {
        if (!!window.outerHeight) {
            this.newProposalButtonPosition = (window.outerHeight / 2).toString() + "px";
        }
        this.spaceFromBar = "20px";
    }

    /*
        non fa niente (al momento)
        goToTest() {
            this._router.navigate(['../proposals/test/list'])
        }
    */

    isAuthorizedToSearch() {
        return this._userProfileService.haveAbilitation(AuthConstant.PROP_CONSULTAZIONE);
    }

    /* richiede lista aggiornata, innescata da accordion-list.component tramite eventEmitter */
    onSort(sortString: string) {
        this.sortString = sortString;
        this.resetPagination();
        this.getAllList();
    }

    /**
     * Mostra il tab selezionato
     * @param {string} tabKey
     */
    onTabKeyChange(tabKey: string): void {
        this.cleanTabData();
        //imposta tab
        this.activeTab = tabKey;
        this.requestParamiters = new CreditQueueAllRequestParameters();
        switch (tabKey) {
            case ListTabs.ALL: {
                this.requestParamiters.sort = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC;
                break;
            }
            case ListTabs.URGENT: {
                this.requestParamiters.vista = WorkQueueFilterView.URGENTI;
                break;
            }
            case ListTabs.ASSIGNED_TO_ME: {
                this.requestParamiters.vista = WorkQueueFilterView.ASSEGNATE_A_ME;
                break;
            }
            case ListTabs.DOCUMENT_CHECK: {
                this.requestParamiters.vista = WorkQueueFilterView.IN_CONTROLLO_DOCUMENTALE;
                break;
            }
            case ListTabs.BLOCKED: {
                this.requestParamiters.vista = WorkQueueFilterView.BLOCCATE;
                break;
            }
            case ListTabs.DOCUMENT_INCOMPLETE: {
                this.requestParamiters.vista = WorkQueueFilterView.DOCUMENTAZIONE_INCOMPLETA;
                break;
            }
            case ListTabs.SUSPENDED: {
                this.requestParamiters.vista = WorkQueueFilterView.SOSPESE;
                break;
            }
            case ListTabs.SECOND_APPROVATION: {
                this.requestParamiters.vista = WorkQueueFilterView.SECONDA_APPROVAZIONE;
                break;
            }
            case ListTabs.REPROPOSED: {
                this.requestParamiters.vista = WorkQueueFilterView.RIPROPOSTA;
                break;
            }
            case ListTabs.IN_INVESTIGATION: {
                this.requestParamiters.vista = WorkQueueFilterView.IN_ISTRUTTORIA;
                break;
            }
            // case ListTabs.INERROR: {
            //     this.requestParamiters.vista = WorkQueueFilterView.BLOCCATE;
            //     break;
            // }
        }
        // imposto pagina e size a tutti TODO valutare se settarlo di default nel classe astratta
        this.requestParamiters.size = AppSettings.PROPOSALS2PAGE;
        this.requestParamiters.page = __startPage;

        // Chiama la lista di proposte del tab selezionato
        this.getAllList();

        // per tab in sviluppo
        // this.showWIP = !([ListTabs.ALL, ListTabs.MBCPOS, ListTabs.VDZ, ListTabs.SIMULATIONS].includes(tabKey));
    }

    cleanTabData() {
        // resetta ordinamento default
        this.sortString = "";
        //svuota dati non in uso
        this.printList = [];
        this.proposalsList = [];
        this.resetPagination();
    }

    /**
     *  Metodo che effettua la chiamata per la lista delle proposte collegata al tab attivo.
     */
    // getProposalList() {
    //     switch (this.activeTab) {
    //         case ListTabs.ALL: {
    //             this.getAllList();
    //             break;
    //         }
    //     }
    // }

    /**
     * Funziona che applica il filtro all'oggetto RequestParameters
     * @param event
     */
    onFilterChange(event) {
        this.requestParamiters.setParamsFromFilter(event);
        this._logger.logWarn(CreditRetailComponent.name, 'onFilterChange', this.requestParamiters);
    }

        /**
     * seleziona il tab passato come parametro
     * @param {string} key
         */
    setTab(key: string): void {
        if (this.addingTab) {
            this._notification.sendNotification(
                new NotificationMessage(this.translate.instant('ADDING_TAB'), NOTIFICATION_TYPE.WARN)
            );
            return;
        }
        if (this.tabKey != key) {
            this.tabKey = key;
            this.onTabKeyChange(key);
        }
    }

    /**
   * Apre/chiude pannello dei filtri (se non è aperto il pannello dei tab)
   */
  onFilterClick() {
    if(this.addingTab) {
      this._notification.sendNotification(
          new NotificationMessage(this.translate.instant('ADDING_TAB'),NOTIFICATION_TYPE.WARN)
      );
      return;
    }
    this.showCorporateFilter = !this.showCorporateFilter; 
    // this.addingTab = false;
  }

  /**
     * Apre/chiude pannello di selezione dei tab visibili. All'apertura, chiude e disabilita temporaneamente
     * il pannello dei filtri.
     */
    onTabSelectorClick(){
      this.addingTab = !this.addingTab;
      this.showCorporateFilter = false;
  }

        /**
     * popola la selectbox relativa al listcode passato
     * @param listCode
     */
    getSelectBoxValues(listCode: string): void {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_SELECT_BOX_VALUES, REQUEST_PRIORITY.HIGH);
        let valueList: ValueList[] = [];
        this.beProposalService.getSelectBoxValues(listCode).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
        response => {
            if (response.code == AppSettings.HTTP_OK) {
            valueList = response.object;
            } else {
            this._logger.logError(CreditRetailComponent.name, 'getSelectBoxValues', "ERRORE NON IN FALLBACK", response.code);
            }
            switch (listCode) {
                case __subjectListCode: {
                    this.subjectTypes = valueList;
                    break;
                }
                case __proposalTypeListCode: {
                    this.proposalTypes = valueList;
                    break;
                }
                case __proposalDerogationListCode: {
                    this.proposalDerogations = valueList;
                    break;
                }
                case __cdsListCode: {
                    this.cds = valueList;
                    break;
                }
                // case __clickAndGoListCode: {
                //     this.clickAndGo = valueList;
                //     break;
                // }
            }
            setTimeout(() => {
            this._requestManagerService.handleRequest(RequestManagerConstant.GET_SELECT_BOX_VALUES, response.code);
            }, AppSettings.TIMEOUT_SPINNER);
        },
        error => {
            this._logger.logError(CreditRetailComponent.name, 'getSelectBoxValues', error);
            let errorResp = this._utils.getErrorResponse(error);
            this._logger.logError(CreditRetailComponent.name, 'getSelectBoxValues', "error", errorResp);
            setTimeout(() => {
            this._requestManagerService.handleRequest(RequestManagerConstant.GET_SELECT_BOX_VALUES, error.code, error.message);
            }, AppSettings.TIMEOUT_SPINNER);
        }
        );
    }

    /**
   * popola i contatori dei tab delle proposte
   */
    getNumProposalTabs() {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, REQUEST_PRIORITY.HIGH);
        this.beProposalService.getNumbersProposalTab(AuthConstant.CODA_CREDITO_RETAIL).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
        response => {
            if (response.code == AppSettings.HTTP_OK) {
                this.numberBlockedPropoposals = response.object.numBloccate;
                this.numberReproposedProposals = response.object.numRiproposta;
                this.numberAllProposals = response.object.numTotali;
                this.numberAssignedToMeProposals = response.object.numAssegnateAMe;
                this.numberUrgentProposals = response.object.numUrgenti;
                this.numberInCheckDocProposals = response.object.numInControlloDocumentale;
                this.numberInInvestigationProposals = response.object.numInInstruttoria;
                this.numberSecondApprPropoposals = response.object.numSecondaApprovazione;
                this.numberIncompleteDocPropoposals = response.object.numDocumentazionIncompleta;
                this.numberSuspendeProposals = response.object.numSospese;

            } else {
                this._logger.logError(CreditRetailComponent.name, 'getSelectBoxValues', "ERRORE NON IN FALLBACK", response.code);
            }
            this.checkSelectableTabs();
            setTimeout(() => {
            this._requestManagerService.handleRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, response.code);
            }, AppSettings.TIMEOUT_SPINNER);
        },
        error => {
            this._logger.logError(CreditRetailComponent.name, 'getNumProposalTabs', error);
            let errResp = this._utils.getErrorResponse(error);
            this._logger.logError(CreditRetailComponent.name, 'getNumProposalTabs', "error", errResp);
            setTimeout(() => {
            this._requestManagerService.handleRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, error.code, error.message);
            }, AppSettings.TIMEOUT_SPINNER);
        }
        );
    }

    manageSelectedTabs(tabs: IFilterTab[]): void {
        for (let tab of tabs) {
            if(tab.label.indexOf('(') <= 0){
                switch(tab.key) {
                case ListTabs.ALL: {
                    tab.label+= '    (' + this.numberAllProposals + ')';
                    break;
                }
                case ListTabs.BLOCKED: {
                    tab.label += '    (' + this.numberBlockedPropoposals + ')';
                    break;
                }
                case ListTabs.URGENT: {
                    tab.label += '    (' + this.numberUrgentProposals + ')';
                    break;
                }
                case ListTabs.ASSIGNED_TO_ME: {
                    tab.label += '    (' + this.numberAssignedToMeProposals + ')';
                    break;
                }
                case ListTabs.DOCUMENT_CHECK: {
                    tab.label += '    (' + this.numberInCheckDocProposals + ')';
                    break;
                }
                case ListTabs.DOCUMENT_INCOMPLETE: {
                    tab.label += '    (' + this.numberIncompleteDocPropoposals + ')';
                    break;
                }
                case ListTabs.SUSPENDED: {
                    tab.label += '    (' + this.numberSuspendeProposals + ')';
                    break;
                }
                case ListTabs.SECOND_APPROVATION: {
                    tab.label += '    (' + this.numberSecondApprPropoposals + ')';
                    break;
                }
                case ListTabs.REPROPOSED: {
                    tab.label += '    (' + this.numberReproposedProposals + ')';
                    break;
                }
                case ListTabs.IN_INVESTIGATION: {
                    tab.label += '    (' + this.numberInInvestigationProposals + ')';
                    break;
                }
                }
            }
        }
        //se deselezionato il tab corrente
        let changeTab = true;
        for(let selectedTab of this.tabs) {
            if(selectedTab.key==this.tabKey) {
                changeTab = false; 
                break;
            }
        }
        if(changeTab) this.setTab(this.tabs[0].key);
    }

        /**
         * primitiva per la creazione del form relativa ai campi di ricerca
         */
    createForms() {
        this.advancedFilters = this._formBuilder.group({
            subjectName: this._formBuilder.control(null),
            subjectType: this._formBuilder.control(null),
            proposalType: this._formBuilder.control(null),
            proposalDerogation: this._formBuilder.control(null),
            dataBankInError: this._formBuilder.control(null),
            k2k: this._formBuilder.control(null),
            // clickAndGo: this._formBuilder.control(null),
            cds: this._formBuilder.control(null),
            assetType: this._formBuilder.control(null)
        });
    }

    /**
     * pulisce il form
     */
    clearForm(): void {
        this.resetDerogation = !this.resetDerogation;
        this.resetSubjectTypes = !this.resetSubjectTypes;
        this.resetCDS = !this.resetCDS;
        this.advancedFilters.reset();
    }

    /**
     * esegue il salvataggio dei campi presenti nel form all'interno dell'oggetto request che
     * verrà utilizzato per effettuare la ricerca
     */
    saveAdvancedFilter(): void {
        this.requestParamiters.deroga = this.selectedDerogation;
        this.requestParamiters.errorBancaDati = this.advancedFilters.get('dataBankInError').value;
        this.requestParamiters.nomeSogg = this.advancedFilters.get('subjectName').value;
        this.requestParamiters.tipoProp = this.advancedFilters.get('proposalType').value;
        this.requestParamiters.tipoSogg = this.selectedSubjectTypes;
        this.requestParamiters.k2k = this.advancedFilters.get('k2k').value;
        // this.requestParamiters.clickAndGo = this.advancedFilters.get('clickAndGo').value;
        this.requestParamiters.cds = this.selectedCDS;
        this.requestParamiters.asset = this.advancedFilters.get('assetType').value;

        this.resetPagination();

        this.getAllList();

        this.showCorporateFilter = false;
    }

    /**
     * calcola il numero di documenti mancanti
     */
    getNumDocmancanti(): void {
        if(this.numDocMancanti == 0){
            for(let prop of this.proposalsList) {
                this.numDocMancanti += prop.numDocMancanti;
            }
        }
    }

    /**
     * verifica se l'icona di documentazione incompleta deve essere presente
     */
    docMancantiIconPresence(key): boolean {
        return this.numDocMancanti > 0 && key == ListTabs.DOCUMENT_INCOMPLETE;
    }

    /* rimuove sottoscrizione alle proposte */
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    /*    testNotification() {
            this._requestManagerService.pushNewRequest("NORMAL1", "NORMAL");
            this._requestManagerService.pushNewRequest("LOW4", "LOW");
            this._requestManagerService.pushNewRequest("HIGH1", "HIGH");
            this._requestManagerService.pushNewRequest("LOW2", "LOW");
            this._requestManagerService.pushNewRequest("HIGH2", "HIGH");
            this._requestManagerService.pushNewRequest("LOW3", "LOW");
            this._requestManagerService.pushNewRequest("LOW1", "LOW");
            this._requestManagerService.pushNewRequest("NORMAL2", "NORMAL");

            setTimeout(() => {
                this._requestManagerService.handleRequest("HIGH1", 404, null);
            }, 1000);
            setTimeout(() => {
                this._requestManagerService.handleRequest("HIGH2", 404, null);
            }, 5000);
            setTimeout(() => {
                this._requestManagerService.handleRequest("LOW1", 404, "ERRORE BE");
            }, 1500);
            setTimeout(() => {
                this._requestManagerService.handleRequest("LOW2", 404, "ERRORE BE");
            }, 500);
            setTimeout(() => {
                this._requestManagerService.handleRequest("LOW3", 404, null);
            }, 2000);
            setTimeout(() => {
                this._requestManagerService.handleRequest("LOW4", 404, "ERRORE BE");
            }, 6000);
            setTimeout(() => {
                this._requestManagerService.handleRequest("NORMAL1", 404, null);
            }, 2000);
            setTimeout(() => {
                this._requestManagerService.handleRequest("NORMAL2", 404, "ERRORE BE");
            }, 8000);
        }*/

    getSelectedSubjectType(items) {
        let values = items.map((item) => {
            return item['key'];
        });
        this.selectedSubjectTypes = values;
    }

    getSelectedDerogation(items) {
        let values = items.map((item) => {
            return item['key'];
        });
        this.selectedDerogation = values;
    }

    getSelectedCDS(items) {
        let values = items.map((item) => {
            return item['key'];
        });
        this.selectedCDS = values;
    }
}
