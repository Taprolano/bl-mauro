import {Component, OnInit} from '@angular/core';
import {LoggerService} from "../../../../../shared/services/logger.service";
import {TranslateService} from "@ngx-translate/core";
import {AbstractModalService} from "../../../../../shared/components/abstract-modal/abstract-modal.service";
import {AccordionListAbstract} from "../../../../components/accordion-list/accordion-list.abstract";
import {ProposalsRoutingConstant} from '../../../../proposalsRoutingConstant';
import {Router} from '@angular/router';
import {CreditCorporateQueue} from "../../../../../data-model/class/CreditCorporateQueue";
import {PersistenceService} from "../../../../../shared/services/persistence.service";

@Component({
    selector: 'app-corporate-list-all',
    templateUrl: './corporate-list-all.component.html',
    styleUrls: ['./corporate-list-all.component.scss']
})
export class CorporateListAllComponent extends AccordionListAbstract implements OnInit {

    /**@ignore*/
    readonly __widths = ["0%", "0%", "0%", "0%", "0%", "0%", "0%"];
    /**@ignore*/
    readonly headers = ["PROPOSAL", "ARRIVAL", "APPLICANT", "FINANCIAL_PRODUCT", "CATEGORY_ASSET", "EXPOSURE", ""];

    //TODO: verificare i campi mancati rispetto all'invision -> ISTRUTTORIA, PRODOTTO

    /**@ignore*/
    readonly objectKeys = CreditCorporateQueue.keys;

    /**@ignore*/
    readonly __wrapperHeight = "400px";

    /**@ignore*/
    trigger = 'trigger';

    //readonly sortKeys = ['proposta','tipo di soggetto','arrivo','istruttoria','prodotto','assegnatario','richiedente', 'score', 'esposizione'];

    /**@ignore*/
    constructor(_abstractModalService: AbstractModalService,
                translate: TranslateService,
                _logger: LoggerService,
                private _persistence: PersistenceService,
                private _router: Router) {
        super(_abstractModalService, translate, _logger);
    }

    /**
     * Naviga verso la pagina di consultazione
     */
    goToConsultation(idProposal: string, processInstanceID: number): void {

        let obj = {
            idProposal: idProposal,
            processInstanceID: processInstanceID
        };

        this._persistence.save("SELECT_PROPOSAL", obj);
        this._router.navigate([ProposalsRoutingConstant.fullPath('workQueueConsultation')]);
    }
}
