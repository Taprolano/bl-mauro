import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CreditCorporateQueue } from 'src/app/data-model/class/CreditCorporateQueue';
import { CreditQueueAllRequestParameters } from '../../../data-model/class/ProposalListAllRequestParameters';
import { ProposalService } from '../../../shared/services/proposal.service';
import { LoggerService } from '../../../shared/services/logger.service';
import { ListOrderKeys } from '../../../data-model/interface/ListOrderKeys';
import { Order } from 'src/app/data-model/interface/EOrder';
import { REQUEST_PRIORITY, RequestManagerService } from '../../../shared/services/request-manager.service';
import { RequestManagerConstant } from '../../requestManagerConstant';
import { AppSettings } from '../../../../AppSettings';
import { BeWorkQueue } from '../../services/be-work-queue.service';
import { UtilsService } from '../../../shared/services/utils.service';
import { IPagination } from '../../../data-model/interface/IPagination';
import { IFilterOption } from '../../../data-model/interface/IFilterOption';
import { IFilterChoice } from '../../../data-model/interface/IFilterChoice';
import { AuthConstant } from 'src/assets/authConstant';
import { IFilterTab } from '../../../data-model/interface/IFilterTab';
import { WorkQueueFilterView } from '../EWorkQueueFilterView';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ValueList } from '../../../data-model/class/ListValues';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ListTabs } from '../EListTabs';
import { NotificationService } from '../../../shared/components/notification/notification.service';
import { NotificationMessage, NOTIFICATION_TYPE } from '../../../shared/components/notification/NotificationMessage';


const __filterPlaceholder = 'Cerca per proposta, contratto o soggetto.';
const __startPage = 0;

const __keysChoice: IFilterChoice[] = [
    { value: '0', label: 'Nuova proposta', selected: false },
    { value: 'modifyProposal', label: 'Proposta in modifica', selected: false },
    { value: 'benRequest', label: 'Richiesta benestare', selected: false },
    { value: 'failedRequest', label: 'Richiesta numerazione fallita', selected: false },

    { value: 'notAcptProp', label: 'Proposta non accettata', selected: false },
    { value: 'acptProp', label: 'Proposta accettata', selected: false },
    { value: 'acptModProp', label: 'Proposta accettata con modifiche', selected: false },
    { value: 'numRequest', label: 'Richiesta numerazione', selected: false },


    { value: 'refusedNum', label: 'Numerazione rifiutata', selected: false },
    { value: 'execNum', label: 'Numerazione eseguita', selected: false },
    { value: 'printContract', label: 'Contratto stampato', selected: false },
    { value: 'actRequest', label: 'Richiesta attivazione', selected: false },

    { value: 'execActiv', label: 'Attivazione eseguita', selected: false },
    { value: 'refusedActiv', label: 'Attivazione rifiutata', selected: false },
    { value: 'failCancRequest', label: 'Richiesta cancellazione fallita', selected: false },
    { value: 'modifyProp', label: 'Proposta in modifica', selected: false }
];

const __filterOption: IFilterOption = {
    key: 'state',
    possibleChoices: __keysChoice
};

const __pageProposals: IPagination = {
    keys: null,
    numberOfResults: null,
    recordPerPage: AppSettings.ELEMENT2PAGE,
    page: __startPage,
    nPage: 0,
    size: AppSettings.PROPOSALS2PAGE
};

const __corporateTabs: IFilterTab[] = [
    { label: "Tutte", key: ListTabs.ALL, selected: true },
    { label: "In errore", key: ListTabs.INERROR, selected: false },
    { label: "Riproposte", key: ListTabs.REPROPOSED, selected: false }
];

/**
 * @ignore
 */
const __maxNumberOfTabs = 4;

const __subjectListCode: string = 'TipoSogg';
const __proposalTypeListCode: string = 'TipoProposta';
const __proposalDerogationListCode: string = 'TipoDeroga';
const __cdsListCode: string = 'TipoCds';

@Component({
    selector: 'app-credit-corporate',
    templateUrl: './credit-corporate.component.html',
    styleUrls: ['./credit-corporate.component.scss']
})
export class CreditCorporateComponent implements OnInit, OnDestroy {

    constructor(private translate: TranslateService,
        private _proposalService: ProposalService,
        private _logger: LoggerService,
        private _requestManagerService: RequestManagerService,
        private _beProposalService: BeWorkQueue,
        private _utils: UtilsService,
        private _formBuilder: FormBuilder,
        private _notificationService: NotificationService) {
    }

    private ngUnsubscribe = new Subject<void>();

    private pageProposals;

    /*Oggetti risposta*/
    proposalsList: CreditCorporateQueue[] = [];
    printList: CreditCorporateQueue[] = [];

    /* stringa di ordinamento corrente */
    private sortString: string = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC

    /**
     * @ignore
     * 'true' per usare il mock service
     */
    private httpTest = false;

    /**@ignore Per utilizzare enum nel template*/
    readonly ListTabs = ListTabs;

    /**
     * Parametri da fornire per la richiesta GET, inclusi filtri
     */
    requestParameters: CreditQueueAllRequestParameters;


    /**@ignore semaforo per bloccare eventi multipli di scrollEnd che richiedono di scaricare nuove proposte */
    sem: boolean = false;

    /** Tab correntemente attiva */
    activeTab: string = ListTabs.ALL;

    //notifica per mostrare temporaneamente Work in progress se tab selezionato != 'tutte'
    showWIP: boolean = false;

    /**
     * @ignore
     */
    tabKey: string;

    /**
     * lista delle tab selezionabili per corporate
     */
    tabs: IFilterTab[] = [];

    /**
     * lista delle tab possibili per corporate
     */
    possibleTabs: IFilterTab[] = [];

    /**
     * flag per mostrare i filtri avanzati
     */
    showCorporateFilter: boolean;

    /**
     * flag per mostrare la view per aggiungere i tab
     */
    addingTab: boolean;

    /**
     * form filtri avanzati
     */
    advancedFilters: FormGroup;
    selectedDerogation: any[];
    selectedSubjectTypes: any[];

    /**
     * select box da popolare
     */
    subjectTypes: ValueList[] = [];
    proposalTypes: ValueList[] = [];
    proposalDerogations: ValueList[] = [];
    cds: ValueList[] = [];

    /**
     * numero proposte per tab
     */
    numberAllProposals: number;
    numberInErrorPropoposals: number;
    numberReproposedProposals: number;

    /**
     *  campi per reset multi select
     */
    resetDerogation: boolean = false;
    resetSubjectTypes: boolean = false;

    ngOnInit() {
        this.numberAllProposals = 0;
        this.numberInErrorPropoposals = 0;
        this.numberReproposedProposals = 0;
        this.possibleTabs = __corporateTabs;
        this.tabKey = ListTabs.ALL;
        this.pageProposals = this._utils.assign(__pageProposals);

        this._proposalService.getMessage().subscribe(
            message => this._logger.logInfo(CreditCorporateComponent.name, 'ngOnInit', "sentMessage - testMessage", message)
        );

        // this._proposalService.updateStatus(ProposalsConstant.stateProduct2Model);
        this.requestParameters = new CreditQueueAllRequestParameters();
        this.requestParameters.sort = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC;

        this.getAllList();

        this.getNumProposalTabs();

        this.getSelectBoxValues(__subjectListCode);
        this.getSelectBoxValues(__proposalTypeListCode);
        this.getSelectBoxValues(__proposalDerogationListCode);
        // this.getSelectBoxValues(__cdsListCode);
        this.createForms();

        window.onbeforeunload = () => this.ngOnDestroy();
    }

    /**
     * popola le tab selezionabili
     */
    checkSelectableTabs() {
        for (let tab of this.possibleTabs) {
            if (tab.selected) {
                let tabToAdd: IFilterTab = this._utils.assign(tab);
                this.tabs.push(tabToAdd);
                this.manageSelectedTabs(this.tabs);
            }
        }
    }

    /**
     * restituisce il numero di tab attivi
     * @returns {number}
     */
    get numberOfTabs(): number {
        let counter = 0;
        for (let tab of this.possibleTabs) {
            if (tab.selected) counter++;
        }

        return counter;
    }

    /**
     * restituisce il numero massimo di tab che è possibile attivare
     * @returns {number}
     */
    get maxNumberOfTabs(): number {
        return __maxNumberOfTabs;
    }

    /**
     * imposta le tab per i filtri base
     */
    setTabs() {
        this.tabs = [];
        for (let tab of this.possibleTabs) {
            let tabToAdd: IFilterTab = this._utils.assign(tab);
            if (tab.selected) this.tabs.push(tabToAdd);
        }
        this.addingTab = false;
        this.manageSelectedTabs(this.tabs);
    }

    /**
     * ritorna tutte le proposte per la vista corporate
     */
    getAllList() {
        //spinner e logger
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSALS_LIST, REQUEST_PRIORITY.HIGH);
        this._logger.logInfo(CreditCorporateComponent.name, 'getAllList', "Request parameters: ", this.requestParameters);
        // set parametri richiesta
        let req = this.requestParameters;
        // req.setParamsFromFilter(this.filterParamsAll);
        req.sort = this.sortString;
        req.tipoCoda = AuthConstant.CODA_CREDITO_CORPORATE;
        req.page = this.pageProposals.page;
        req.size = AppSettings.PROPOSALS2PAGE;
        //sottoscrizione
        this._beProposalService.getCreditCorporateQueueList(req, this.httpTest).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    /*
                                        this.proposalsList = !!response.object ? response.object : [];
                                        this.printList = this.printList.concat(this.proposalsList);
                    */
                    let respObject = !!response.object ? response.object : null;
                    this.proposalsList = (!!respObject && !!respObject.object) ? respObject.object : [];
                    if (!!respObject && !!respObject.pageData) {
                        this.pageProposals.totalPages = respObject.pageData.totalPages;
                        this.pageProposals.totalElements = respObject.pageData.totalElements;
                    }
                    this._logger.logInfo(CreditCorporateComponent.name, 'getAllList', "allList: ", this.proposalsList);
                    // console.log("[List][getAllProposalsList] proposalsList: ", this.proposalsList);
                    if (this.pageProposals.page > __startPage) {
                        this.printList = this.printList.concat(this.proposalsList);
                    } else {
                        this.printList = this.proposalsList;
                    }

                } else {
                    this._logger.logError(CreditCorporateComponent.name, 'getAllList', "ERRORE NON IN FALLBACK", response.code);
                }
                //spinner e semaforo
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSALS_LIST, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            }, resp => {
                this._logger.logError(CreditCorporateComponent.name, 'getAllProposalsList', resp);
                this.proposalsList = [];
                this.printList = this.proposalsList;
                let error = this._utils.getErrorResponse(resp);
                this._logger.logError(CreditCorporateComponent.name, 'getAllProposalsList', "error", error);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSALS_LIST, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
                this.sem = false;
            });
    }

    /* resetta la lista di proposte alla prima pagina */
    resetPagination() {
        this.pageProposals.page = __startPage;
    }

    /**
     * Scarica la nuova pagina dopo lo scroll relativa alla tab attiva
     */
    getNextPage() {
        if (!this.sem) {
            this.sem = true;
            this.pageProposals.page += 1;
            if (this.pageProposals.page < this.pageProposals.totalPages) {
                this.getAllList();
            }
        }
    }

    public get filterPlaceholder(): string {
        return __filterPlaceholder;
    }

    public get filterOption(): IFilterOption {
        return __filterOption;
    }

    /* richiede lista aggiornata, innescata da accordion-list.component tramite eventEmitter */
    onSort(sortString: string) {
        this.sortString = sortString;
        this.resetPagination();
        this.getAllList();
    }

    /**
     * Mostra il tab selezionato
     * @param {string} tabKey
     */
    onTabKeyChange(tabKey: string): void {
        this.cleanTabData();
        //imposta tab
        this.activeTab = tabKey;
        this.requestParameters = new CreditQueueAllRequestParameters();
        switch (tabKey) {
            case ListTabs.ALL: {
                this.requestParameters.sort = ListOrderKeys.DATE + ',' + Order.DESC; // ordinamento default DATA LISTINO DESC;
                break;
            }
            case ListTabs.REPROPOSED: {
                this.requestParameters.vista = WorkQueueFilterView.RIPROPOSTA;
                break;
            }
            case ListTabs.INERROR: {
                this.requestParameters.vista = WorkQueueFilterView.BLOCCATE;
                break;
            }
        }
        // imposto pagina e size a tutti TODO valutare se settarlo di default nel classe astratta
        this.requestParameters.size = AppSettings.PROPOSALS2PAGE;
        this.requestParameters.page = __startPage;

        // Chiama la lista di proposte del tab selezionato
        this.getAllList();

        //per tab in sviluppo
        //this.showWIP = !([ListTabs.ALL, ListTabs.MBCPOS, ListTabs.VDZ, ListTabs.SIMULATIONS].includes(tabKey));
    }

    cleanTabData() {
        //resetta ordinamento default
        this.sortString = "";
        //svuota dati non in uso
        this.printList = [];
        this.proposalsList = [];
        this.resetPagination();
    }

    /**
     *  Metodo che effettua la chiamata per la lista delle proposte collegata al tab attivo.
     */
    // getProposalList() {
    //   switch (this.activeTab) {
    //     case ListTabs.ALL: {
    //       this.getAllList();
    //       break;
    //     }
    //   }
    // }

    /**
     * Funziona che applica il filtro all'oggetto RequestParameters
     * @param event
     */
    onFilterChange(event) {
        this.requestParameters.setParamsFromFilter(event);
        this._logger.logWarn(CreditCorporateComponent.name, 'onFilterChange', this.requestParameters);
    }

    /**
     * seleziona il tab passato come parametro
     * @param {string} key
     */
    setTab(key: string): void {
        if (this.addingTab) {
            this._notificationService.sendNotification(
                new NotificationMessage(this.translate.instant('ADDING_TAB'), NOTIFICATION_TYPE.WARN)
            );
            return;
        }
        if (this.tabKey != key) {
            this.tabKey = key;
            this.onTabKeyChange(key);
        }
    }

    /**
    * Apre/chiude pannello dei filtri (se non è aperto il pannello dei tab)
    */
    onFilterClick() {
        if (this.addingTab) {
            this._notificationService.sendNotification(
                new NotificationMessage(this.translate.instant('ADDING_TAB'), NOTIFICATION_TYPE.WARN)
            );
            return;
        }
        this.showCorporateFilter = !this.showCorporateFilter;
        // this.addingTab = false;
    }

    /**
       * Apre/chiude pannello di selezione dei tab visibili. All'apertura, chiude e disabilita temporaneamente
       * il pannello dei filtri.
       */
    onTabSelectorClick() {
        this.addingTab = !this.addingTab;
        this.showCorporateFilter = false;
    }


    /**
     * popola la selectbox relativa al listcode passato
     * @param listCode
     */
    getSelectBoxValues(listCode: string): void {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_SELECT_BOX_VALUES, REQUEST_PRIORITY.HIGH);
        let valueList: ValueList[] = [];
        this._beProposalService.getSelectBoxValues(listCode).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    valueList = response.object;
                } else {
                    this._logger.logError(CreditCorporateComponent.name, 'getSelectBoxValues', "ERRORE NON IN FALLBACK", response.code);
                }
                switch (listCode) {
                    case __subjectListCode: {
                        this.subjectTypes = valueList;
                        break;
                    }
                    case __proposalTypeListCode: {
                        this.proposalTypes = valueList;
                        break;
                    }
                    case __cdsListCode: {
                        this.cds = valueList;
                        break;
                    }
                    case __proposalDerogationListCode: {
                        this.proposalDerogations = valueList;
                        break;
                    }
                }
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_SELECT_BOX_VALUES, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            },
            error => {
                this._logger.logError(CreditCorporateComponent.name, 'getSelectBoxValues', error);
                let errorResp = this._utils.getErrorResponse(error);
                this._logger.logError(CreditCorporateComponent.name, 'getSelectBoxValues', "error", errorResp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_SELECT_BOX_VALUES, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
            }
        );
    }

    /**
     * popola i contatori dei tab delle proposte
     */
    getNumProposalTabs() {
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, REQUEST_PRIORITY.HIGH);
        this._beProposalService.getNumbersProposalTab(AuthConstant.CODA_CREDITO_CORPORATE).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            response => {
                if (response.code == AppSettings.HTTP_OK) {
                    this.numberInErrorPropoposals = response.object.numBloccate;
                    this.numberReproposedProposals = response.object.numRiproposta;
                    this.numberAllProposals = response.object.numTotali;
                } else {
                    this._logger.logError(CreditCorporateComponent.name, 'getSelectBoxValues', "ERRORE NON IN FALLBACK", response.code);
                }
                this.checkSelectableTabs();
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            },
            error => {
                this._logger.logError(CreditCorporateComponent.name, 'getNumProposalTabs', error);
                let errResp = this._utils.getErrorResponse(error);
                this._logger.logError(CreditCorporateComponent.name, 'getNumProposalTabs', "error", errResp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_NUMBER_PROPOSAL_TAB, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
            }
        );
    }

    manageSelectedTabs(tabs: IFilterTab[]): void {
        for (let tab of tabs) {
            if (tab.label.indexOf('(') <= 0) {
                switch (tab.key) {
                    case ListTabs.ALL: {
                        tab.label += '    (' + this.numberAllProposals + ')';
                        break;
                    }
                    case ListTabs.INERROR: {
                        tab.label += '    (' + this.numberInErrorPropoposals + ')';
                        break;
                    }
                    case ListTabs.REPROPOSED: {
                        tab.label += '    (' + this.numberReproposedProposals + ')';
                        break;
                    }
                }
            }
        }
        //se deselezionato il tab corrente
        let changeTab = true;
        for (let selectedTab of this.tabs) {
            if (selectedTab.key == this.tabKey) {
                changeTab = false;
                break;
            }
        }
        if (changeTab) this.setTab(this.tabs[0].key);
    }

    /**
     * primitiva per la creazione del form relativa ai campi di ricerca
     */
    createForms() {
        this.advancedFilters = this._formBuilder.group({
            subjectName: this._formBuilder.control(null),
            subjectType: this._formBuilder.control(null),
            proposalType: this._formBuilder.control(null),
            proposalDerogation: this._formBuilder.control(null),
            // cds: this._formBuilder.control(null),
            dataBankInError: this._formBuilder.control(null)
        });
    }

    /**
     * pulisce il form
     */
    clearForm(): void {
        this.resetSubjectTypes = !this.resetSubjectTypes;
        this.resetDerogation = !this.resetDerogation;
        this.advancedFilters.reset();
    }

    /**
     * esegue il salvataggio dei campi presenti nel form all'interno dell'oggetto request che
     * verrà utilizzato per effettuare la ricerca
     */
    saveAdvancedFilter(): void {
        this.requestParameters.deroga = this.selectedDerogation;
        this.requestParameters.errorBancaDati = this.advancedFilters.get('dataBankInError').value;
        this.requestParameters.nomeSogg = this.advancedFilters.get('subjectName').value;
        this.requestParameters.tipoProp = this.advancedFilters.get('proposalType').value;
        this.requestParameters.tipoSogg = this.selectedSubjectTypes;
        // this.requestParameters.cds = this.advancedFilters.get('cds').value;

        this.resetPagination();

        this.getAllList();

        this.showCorporateFilter = false;
    }

    /* rimuove sottoscrizione alle proposte */
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    getSelectedSubjectType(items) {
        let values = items.map((item) => {
            return item['key'];
        });
        this.selectedSubjectTypes = values;
    }

    getSelectedDerogation(items) {
        let values = items.map((item) => {
            return item['key'];
        });
        this.selectedDerogation = values;
    }
}
