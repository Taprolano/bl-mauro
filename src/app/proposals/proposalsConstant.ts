/*
const routeProduct2Model =  '../proposals/product/model/';
const routeProduct2Version = '../proposals/product/version';
const routeCommercial= '../proposals/commercial';
const routeFinancial= '../proposals/financial';
const routeRegistry = '../proposals/registry';
const routeRegistry2Profiles = '../proposals/registry/profiles';
const routeRegistry2Documents = '../proposals/registry/documents';
const routePreliminary = '../proposals/preliminary';
const routePreliminary2Privacy = '../proposals/preliminary/privacy';
const routeContract = '../proposals/contract';
const routeContract2Frame = '../proposals/contract/frame';
const routeContract2Print = '../proposals/contract/print';
const routeFinalize  = '../proposals/finalize';
const routeFinalize2Payments = '../proposals/finalize/payments';*/

import {ProposalsRoutingConstant} from "./proposalsRoutingConstant";
import {IProposalState} from "../data-model/interface/IProposalState";
import {IProposalWorkState} from "../data-model/interface/IProposalWorkState";

const routeProduct2Model =  ProposalsRoutingConstant.fullPath('model');
const routeProduct2Version = ProposalsRoutingConstant.fullPath('version');
const routeCommercial= ProposalsRoutingConstant.fullPath('commercial');
const routeFinancial= '../proposals/financial';
const routeRegistry = '../proposals/registry';
const routeRegistry2Profiles = '../proposals/registry/profiles';
const routeRegistry2Documents = '../proposals/registry/documents';
const routePreliminary = '../proposals/preliminary';
const routePreliminary2Privacy = '../proposals/preliminary/privacy';
const routeContract = '../proposals/contract';
const routeContract2Frame = '../proposals/contract/frame';
const routeContract2Print = '../proposals/contract/print';
const routeFinalize  = '../proposals/finalize';
const routeFinalize2Payments = '../proposals/finalize/payments';

/**@ignore*/
enum modelType {
    car = 'Vetture',
    other = 'Altri',
    smart = 'Smart',
    truck= 'Truck',
    van = 'Van'
}

/**@ignore*/
export class ProposalsConstant {


    public static modelEnum = modelType;


    public static stateProduct: string = "PRODUCT";
    public static stateProduct2Model: string = "PRODUCT_MODEL"; //initial state
    public static stateProduct2Version: string = "PRODUCT_VERSION";
    public static stateCommercial: string = "COMMERCIAL";
    public static stateFinancial: string = "FINANCIAL";
    public static stateRegistry: string = "REGISTRY";
    public static stateRegistry2Profiles: string = "REGISTRY_PROFILES";
    public static stateRegistry2Documents: string = "REGISTRY_DOCUMENTS";
    public static statePreliminary: string = "PRELIMINARY";
    public static statePreliminary2Privacy: string = "PRELIMINARY_PRIVACY";
    public static stateContract: string = "CONTRACT";
    public static stateContract2Frame: string = "CONTRACT_FRAME";
    public static stateContract2Print: string = "CONTRACT_PRINT";
    public static stateFinalize: string = "FINALIZE";
    public static stateFinalize2Payments: string = "FINALIZE_PAYMENTS";
    public static summary: string = "SUMMARY";

    public static PROPOSAL_STATE_PRODUCT_MODEL: IProposalState = {
        label: 'Prodotto',
        id: 1,
        route: routeProduct2Model,
        dataModelKey: '',
        state: ProposalsConstant.stateProduct2Model
    };

    public static PROPOSAL_STATE_PRODUCT_VERSION: IProposalState = {
        label: 'Prodotto',
        id: 1,
        route: routeProduct2Version,
        dataModelKey: '',
        state: ProposalsConstant.stateProduct2Version
    };

    public static PROPOSAL_STATE_COMMERCIAL_DATA: IProposalState = {
        label: 'Dati Commerciali',
        id: 2,
        route: routeCommercial,
        dataModelKey: '',
        state: ProposalsConstant.stateCommercial
    };

    public static PROPOSAL_STATE_FINANCIAL_PLANE: IProposalState = {
        label: 'Piano Finanziario',
        id: 3,
        route: routeFinancial,
        dataModelKey: '',
        state: ProposalsConstant.stateFinancial
    };

    public static PROPOSAL_STATE_ANAGRAPHIC_PROFILES: IProposalState = {
        label: 'Anagrafica',
        id: 4,
        route: routeRegistry2Profiles,
        dataModelKey: '',
        state: ProposalsConstant.stateRegistry2Profiles
    };

    public static PROPOSAL_STATE_ANAGRAPHIC_DOCUMENTS: IProposalState = {
        label: 'Anagrafica',
        id: 4,
        route: routeRegistry2Documents,
        dataModelKey: '',
        state: ProposalsConstant.stateRegistry2Documents
    };

    public static PROPOSAL_STATE_PRELIMINARY: IProposalState = {
        label: 'Istruttoria',
        id: 5,
        route: routePreliminary,
        dataModelKey: '',
        state: ProposalsConstant.statePreliminary
    };

    public static PROPOSAL_STATE_PRELIMINARY_PRIVACY: IProposalState = {
        label: 'Istruttoria',
        id: 5,
        route: routePreliminary2Privacy,
        dataModelKey: '',
        state: ProposalsConstant.statePreliminary2Privacy
    };

    public static PROPOSAL_STATE_CONTRACT_FRAME: IProposalState = {
        label: 'Contratto',
        id: 6,
        route: routeContract2Frame,
        dataModelKey: '',
        state: ProposalsConstant.stateContract2Frame
    };

    public static PROPOSAL_STATE_CONTRACT_PRINT: IProposalState = {
        label: 'Contratto',
        id: 6,
        route: routeContract2Print,
        dataModelKey: '',
        state: ProposalsConstant.stateContract2Print
    };

    public static PROPOSAL_STATE_FINALIZZAZIONE: IProposalState = {
        label: 'Finalizzazione',
        id: 7,
        route: routeFinalize,
        dataModelKey: '',
        state: ProposalsConstant.stateFinalize
    };

    public static PROPOSAL_STATE_FINALIZZAZIONE_PAY: IProposalState = {
        label: 'Finalizzazione',
        id: 7,
        route: routeFinalize2Payments,
        dataModelKey: '',
        state: ProposalsConstant.stateFinalize2Payments
    };


    public static PROPOSAL_STATE_LIST = [
        ProposalsConstant.PROPOSAL_STATE_PRODUCT_MODEL,
        ProposalsConstant.PROPOSAL_STATE_PRODUCT_VERSION,
        ProposalsConstant.PROPOSAL_STATE_COMMERCIAL_DATA,
        ProposalsConstant.PROPOSAL_STATE_FINANCIAL_PLANE,
        ProposalsConstant.PROPOSAL_STATE_ANAGRAPHIC_PROFILES,
        ProposalsConstant.PROPOSAL_STATE_ANAGRAPHIC_DOCUMENTS,
        ProposalsConstant.PROPOSAL_STATE_PRELIMINARY,
        ProposalsConstant.PROPOSAL_STATE_PRELIMINARY_PRIVACY,
        ProposalsConstant.PROPOSAL_STATE_CONTRACT_FRAME,
        ProposalsConstant.PROPOSAL_STATE_CONTRACT_PRINT,
        ProposalsConstant.PROPOSAL_STATE_FINALIZZAZIONE,
        ProposalsConstant.PROPOSAL_STATE_FINALIZZAZIONE_PAY
    ];

    public static PROPOSAL_ONWARD_BUTTON_STATE_LIST = [
        ProposalsConstant.PROPOSAL_STATE_COMMERCIAL_DATA.state,
        ProposalsConstant.PROPOSAL_STATE_FINANCIAL_PLANE.state,
        ProposalsConstant.PROPOSAL_STATE_ANAGRAPHIC_PROFILES.state,
        ProposalsConstant.PROPOSAL_STATE_ANAGRAPHIC_DOCUMENTS.state,
        ProposalsConstant.PROPOSAL_STATE_PRELIMINARY.state,
        ProposalsConstant.PROPOSAL_STATE_PRELIMINARY_PRIVACY.state,
        ProposalsConstant.PROPOSAL_STATE_CONTRACT_FRAME.state,
        ProposalsConstant.PROPOSAL_STATE_CONTRACT_PRINT.state,
        ProposalsConstant.PROPOSAL_STATE_FINALIZZAZIONE.state,
        ProposalsConstant.PROPOSAL_STATE_FINALIZZAZIONE_PAY.state
    ];

    public static PROPOSAL_BACKWARD_BUTTON_STATE_LIST = [
        ProposalsConstant.PROPOSAL_STATE_COMMERCIAL_DATA.state,
        ProposalsConstant.PROPOSAL_STATE_FINANCIAL_PLANE.state,
        ProposalsConstant.PROPOSAL_STATE_ANAGRAPHIC_PROFILES.state,
        ProposalsConstant.PROPOSAL_STATE_ANAGRAPHIC_DOCUMENTS.state,
        ProposalsConstant.PROPOSAL_STATE_PRELIMINARY.state,
        ProposalsConstant.PROPOSAL_STATE_PRELIMINARY_PRIVACY.state,
        ProposalsConstant.PROPOSAL_STATE_CONTRACT_FRAME.state,
        ProposalsConstant.PROPOSAL_STATE_CONTRACT_PRINT.state,
        ProposalsConstant.PROPOSAL_STATE_FINALIZZAZIONE.state,
        ProposalsConstant.PROPOSAL_STATE_FINALIZZAZIONE_PAY.state
    ];


    public static getFullState(state: string): IProposalState{
        for(let propState of ProposalsConstant.PROPOSAL_STATE_LIST){
            if(propState.state === state) return propState;
        }
    }



    //---------------- STATI DELLA PROPOSTA ----------------//

    public static SDP_NEW: IProposalWorkState = {id:0, label:"NEW", desc:"Nuova Proposta"};
    public static SDP_IN_REVIEW: IProposalWorkState = {id:5,label:"IN_REVIEW",desc:"Proposta Resettata"};
    public static SDP_REJECTED: IProposalWorkState = {id:15,label:"REJECTED",desc:"Proposta Non Accettata"};
    public static SDP_ACCEPTED: IProposalWorkState = {id:20,label:"ACCEPTED",desc:"Proposta Accettata"};
    public static SDP_WAITING_FOR_APPROVAL: IProposalWorkState = {id:10,label:"WAITING_FOR_APPROVAL",desc:"Richiesta Benestare"};
    public static SDP_ACCEPTED_WITH_CONDITIONS: IProposalWorkState = {id:22,label:"ACCEPTED_WITH_CONDITIONS",desc:"Proposta Accettata con Modifiche"};
    public static SDP_WAITING_FOR_CONTRACT_NUMBER: IProposalWorkState = {id:24,label:"WAITING_FOR_CONTRACT_NUMBER",desc:"Richiesta Numerazione"};
    public static SDP_CONTRACT_NUMBER_ASSIGNED: IProposalWorkState = {id:40,label:"CONTRACT_NUMBER_ASSIGNED",desc:"Numerazione Eseguita"};
    public static SDP_CONTRACT_PRINTED: IProposalWorkState = {id:45,label:"CONTRACT_PRINTED",desc:"Contratto Stampato"};
    public static SDP_WAITING_FOR_ACTIVATION: IProposalWorkState = {id:68,label:"WAITING_FOR_ACTIVATION",desc:"Richiesta Attivazione"};

    //*************PMS*************
    public static SDP_PMS_PRE_CREDIT_CHECK: IProposalWorkState = {id:11,label:"Pre-Credit Check",desc:""};
    public static SDP_PMS_PRE_CREDIT_CHECK_EXECUTED: IProposalWorkState = {id:12,label:"Pre-Credit Check executed",desc:""};
    public static SDP_PMS_CREDIT_CHECK: IProposalWorkState = {id:13,label:"Credit Check",desc:""};
    public static SDP_PMS_ASSIGNING_CONTRACT_NUMBER: IProposalWorkState = {id:26,label:"Assigning Contract Number",desc:""};
    public static SDP_PMS_CONTRACT_ACTIVATION: IProposalWorkState = {id:70,label:"Contract Activation",desc:""};
    public static SDP_PMS_ANTI_MONEY_LAUNDERING: IProposalWorkState = {id:72,label:"Anti-Money Laundering",desc:""};
    public static SDP_PMS_SENDING_CONTRACT_TO_PHOENIX: IProposalWorkState = {id:75,label:"Sending contract to Phoenix",desc:""};
    public static SDP_PMS_SENDING_WORKFLOW_PHOENIX: IProposalWorkState = {id:77,label:"Starting WorkFlow Phoenix",desc:""};
//  public static SDP_PMS_CANCELLING_PROPOSAL: IProposalWorkState = {id:90,"Cancelling Proposal"), //TODO chiarire se questo stato Ã¨ lo stesso di
    //     PROPOS SDP_AL_CANCELLED
    //***************FINAL STATUS************
    public static SDP_FS_CONTRACT_NUMBER_REJECTED: IProposalWorkState = {id:35,label:"CONTRACT_NUMBER_REJECTED",desc:"Numerazione Rifiutata"};
    public static SDP_FS_ACTIVATION_REJECTED: IProposalWorkState = {id:93,label:"Actiovation rejected",desc:"Attivazione Rifiutata"};
    public static SDP_FS_CONTRACT_ACTIVATED: IProposalWorkState = {id:80,label:"CONTRACT_ACTIVATED",desc:"Attivazione Eseguita"};
    public static SDP_FS_PROPOSAL_CANCELLED: IProposalWorkState = {id:90,label:"Proposal cancelled",desc:"Proposta Cancellata"};

    public static SDP_FS_RICHIESTA_ATTIVAZIONE_RIFIUTATA: IProposalWorkState = {id:-68,label:"",desc:"Richiesta Attivazione Fallita"};
    public static SDP_FS_RICHIESTA_NUMERAZIONE_RIFIUTATA: IProposalWorkState = {id:-24,label:"RICHIESTA_NUMERAZIONE_RIFIUTATA",desc:"Richiesta Numerazione Fallita"};
    public static SDP_FS_RICHIESTA_BENESTARE_RIFIUTATA: IProposalWorkState = {id:-10,label:"",desc:"Richiesta Benestare Fallita"};
    public static SDP_FS_RICHIESTA_CANCELLAZIONE_RIFIUTATA: IProposalWorkState = {id:-90,label:"RICHIESTA_CANCELLAZIONE_RIFIUTATA",desc:"Richiesta canc. Fallita"};


    public static PROPOSAL_STATES_4_FILTER_BAR = [
        {id:null,label:"ALL",desc:"Tutti"},
        ProposalsConstant.SDP_NEW,
        ProposalsConstant.SDP_REJECTED,
        ProposalsConstant.SDP_FS_CONTRACT_NUMBER_REJECTED,
        ProposalsConstant.SDP_FS_CONTRACT_ACTIVATED,
        ProposalsConstant.SDP_IN_REVIEW,
        ProposalsConstant.SDP_ACCEPTED,
        ProposalsConstant.SDP_CONTRACT_NUMBER_ASSIGNED,
        ProposalsConstant.SDP_WAITING_FOR_APPROVAL,
        ProposalsConstant.SDP_ACCEPTED_WITH_CONDITIONS,
        ProposalsConstant.SDP_CONTRACT_PRINTED,
        ProposalsConstant.SDP_FS_RICHIESTA_NUMERAZIONE_RIFIUTATA,
        ProposalsConstant.SDP_FS_RICHIESTA_CANCELLAZIONE_RIFIUTATA,
        ProposalsConstant.SDP_WAITING_FOR_CONTRACT_NUMBER,
        ProposalsConstant.SDP_WAITING_FOR_ACTIVATION
    ];

    /**@ignore Maxlength dei campi in input, es. per il filtro */
    public static MAX_INPUT_SIZES = {
        proposalNumber: '10',
        contractNumber: '16',
        petitioner: '60',
        chassis: '17',
        licensePlate: '17',
        offerCode: '10',
        campaignCode: '5',
        asset: '',
        fuelType: '',
        simulationCode: '10',
        leadCode: '8',
        salesCode: '30',
        dspId: '50'
    };
/*
    public static PROPOSAL_STATES_4_POPUP = [
        {id:null,label:"ALL",desc:"Tutti"},
        ProposalsConstant.SDP_NEW,
        ProposalsConstant.SDP_REJECTED,
        ProposalsConstant.SDP_FS_CONTRACT_NUMBER_REJECTED,
        ProposalsConstant.SDP_FS_CONTRACT_ACTIVATED,
        ProposalsConstant.SDP_IN_REVIEW,
        ProposalsConstant.SDP_ACCEPTED,
        ProposalsConstant.SDP_CONTRACT_NUMBER_ASSIGNED,
        ProposalsConstant.SDP_WAITING_FOR_APPROVAL,
        ProposalsConstant.SDP_ACCEPTED_WITH_CONDITIONS,
        ProposalsConstant.SDP_CONTRACT_PRINTED,
        ProposalsConstant.SDP_FS_RICHIESTA_NUMERAZIONE_RIFIUTATA,
        ProposalsConstant.SDP_FS_RICHIESTA_CANCELLAZIONE_RIFIUTATA,
        ProposalsConstant.SDP_WAITING_FOR_CONTRACT_NUMBER,
        ProposalsConstant.SDP_WAITING_FOR_ACTIVATION
    ]
*/

    public static stateSortAlg(a,b){
        //ordinamento crescente, ma i negativi alla fine, crescenti sul valore assoluto
        let x = +(a.id), y= +(b.id);
        if(x<0) {
            if(y<0) return y-x;
            else return +1;
        }
        else if(y<0) return -1;
        return x-y;
    }



}
