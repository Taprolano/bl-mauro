export enum KeyLeftTabs {
  PROP = "prop",
  SUBJ = "subj",
  CONTRACTS = "contracts"
}

export enum KeyRightTabs { 
  DOCS = "docs",
  NOTES = "notes",
  BANKS_DATA = "banksData"
}

export enum KeyAllLeftTabs {
  PROP = "prop",
  SUBJ = "subj",
  CONTRACTS = "contracts",
  DOCS = "docs",
  NOTES = "notes",
  BANKS_DATA = "banksData"
}

export enum KeyAllRightTabs {
  DOCS = "docs",
  NOTES = "notes",
  BANKS_DATA = "banksData",
  PROP = "prop",
  SUBJ = "subj",
  CONTRACTS = "contracts"
}