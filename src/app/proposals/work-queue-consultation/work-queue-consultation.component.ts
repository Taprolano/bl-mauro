import {Component, OnDestroy, OnInit} from '@angular/core';
import {BeWorkQueue} from '../services/be-work-queue.service';
import {ProposalConsultationRequestParameters} from '../../data-model/class/ProposalConsultationRequestParameters';
import {REQUEST_PRIORITY, RequestManagerService} from '../../shared/services/request-manager.service';
import {RequestManagerConstant} from '../requestManagerConstant';
import {takeUntil} from 'rxjs/internal/operators';
import {AppSettings} from '../../../AppSettings';
import {UtilsService} from '../../shared/services/utils.service';
import {Subject, Subscription, forkJoin} from 'rxjs/index';
import {LoggerService} from '../../shared/services/logger.service';
// import {ConsultationQueue} from '../../data-model/class/Consultation/ConsultationQueue';
// import {
//     ConsultationHeaderQueue,
//     ConsultationHeaderQueueFactory
// } from '../../data-model/class/Consultation/ConsultationHeaderQueue';
// import {
//     ConsultationProposalProdQueueFactory,
//     ConsultationProposalProductQueue
// } from 'src/app/data-model/class/Consultation/ConsultationProposalProductQueue';
// import {
//     ConsultationFinancialQueueFactory,
//     ConsultationFinancialQueue
// } from '../../data-model/class/Consultation/ConsultationFinancialQueue';
// import {
//     ConsultationSubjectQueue,
//     ConsultationSubjectQueueFactory
// } from '../../data-model/class/Consultation/ConsultationSubjectQueue';
// import {
//     ConsultationCommercialQueue,
//     ConsultationCommercialQueueFactory
// } from '../../data-model/class/Consultation/ConsultationCommercialQueue';
import {PersistenceService} from '../../shared/services/persistence.service';
import {ProposalWorkflowService} from '../services/proposal-workflow.service';
import {NotificationService} from '../../shared/components/notification/notification.service';
import {NOTIFICATION_TYPE, NotificationMessage} from '../../shared/components/notification/NotificationMessage';
import {KeyAllLeftTabs, KeyAllRightTabs, KeyLeftTabs, KeyRightTabs} from './HeadersTabsConstant';
import {AbstractModalService} from '../../shared/components/abstract-modal/abstract-modal.service';
import {
    ConsultationSubjects,
    ConsultationSubjectsFactory
} from '../../data-model/class/Consultation/Subjects/ConsultationSubjects';

@Component({
    selector: 'app-work-queue-consultation',
    templateUrl: './work-queue-consultation.component.html',
    styleUrls: ['./work-queue-consultation.component.scss']
})
export class WorkQueueConsultationComponent implements OnInit, OnDestroy {

    rightTab: string;
    leftTab: string;
    rightHeaderTabs = [];
    leftHeaderTabs = [];

    isEditing = false;
    isEditingSubscribe: Subscription;

    /** @ignore */
    destroy$: Subject<boolean> = new Subject<boolean>();

    // consultationQueue: ConsultationQueue;
    // consHeaderQueue: ConsultationHeaderQueue;
    // consProposalProdQueue: ConsultationProposalProductQueue;
    // consSubjectQueue: ConsultationSubjectQueue;
    // consCommercialQueue: ConsultationCommercialQueue;
    // consFinancialQueue: ConsultationFinancialQueue;

    consultationSubjects: ConsultationSubjects[];
    commonQueue: any;

    constructor(private _workQueueService: BeWorkQueue,
                private _requestManagerService: RequestManagerService,
                private _utils: UtilsService,
                private _logger: LoggerService,
                private _proposalWfService: ProposalWorkflowService,
                private _persistence: PersistenceService,
                private _notificationService: NotificationService,
                private _modalService: AbstractModalService) {
    }

    ngOnInit() {
        this.getProposalCons();
        this.isEditingSubscribe = this._proposalWfService.getIsEditing().subscribe(isEditing => {
            this.isEditing = isEditing;
        });
    }

    /**
     * notifica del cambiamento di tab per la sezione di sinistra
     * @param tabKey
     */
    onTabLeftKeyChange(tabKey: string) {
        this.leftTab = tabKey;
    }

    /**
     * notifica del cambiamento di tab per la sezione di destra
     * @param tabKey
     */
    onTabRightKeyChange(tabKey: string) {
        // if (tabKey == KeyRightTabs.DOCS) {
        this.rightTab = tabKey;
        // } else {
        //    this.showWIP();
        // }
    }

    /**
     * notifica del cambiamento dell'header delle tabs di destra (allargamento o restringimento)
     * @param headerTabs
     */
    onHeaderRightChange(headerTabs) {
        // if (headerTabs == KeyAllLeftTabs.DOCS || headerTabs == KeyAllLeftTabs.PROP) {
        this.rightHeaderTabs = headerTabs;
        // } else {
        //    this.showWIP();
        // }
    }

    /**
     * notifica del cambiamento dell'header delle tabs di sinistra (allargamento o restringimento)
     * @param headerTabs
     */
    onHeaderLeftChange(headerTabs) {
        // if (headerTabs == KeyAllRightTabs.DOCS || headerTabs == KeyAllRightTabs.PROP) {
        this.leftHeaderTabs = headerTabs;
        // } else {
        //    this.showWIP();
        // }
    }

    /**
     * recupera la consultazione proposta
     */
    getProposalCons() {
        // let paramsMock = new ProposalConsultationRequestParameters('1000900143', 2605);
        const paramsMock = new ProposalConsultationRequestParameters('1002004188', 153);
        let obj = {
            idProposal: '1000900597',
            processInstanceID: 305
        };
        this._persistence.save('SELECT_PROPOSAL', obj);
        const objProposalSelect = this._persistence.get('SELECT_PROPOSAL');
        if (objProposalSelect) {
            const params = new ProposalConsultationRequestParameters(objProposalSelect.idProposal, objProposalSelect.processInstanceID);

            this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_SUBJECTS, REQUEST_PRIORITY.HIGH);
            // forkJoin(
            //     // TODO sostituire paramsMock con params quando saranno disponibili le chiamate
            //     // per tutti i vari idProposal e processInstanceId
            //     this._workQueueService.getConsultazioneProposal(params).pipe(takeUntil(this.destroy$)),
            //     this._workQueueService.getConsulazioneSubjects(params).pipe(takeUntil(this.destroy$))
            // )
            this._workQueueService.getConsulazioneSubjects(params).pipe(takeUntil(this.destroy$))
                .subscribe(
                    responseList => {
                        // this.consultationQueue = responseList[0].object;
                        // this.populateConsultationSections();
                        this.consultationSubjects = responseList.object;

                        setTimeout(() => {
                            this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_SUBJECTS, responseList.code);
                        }, AppSettings.TIMEOUT_SPINNER);
                    }, resp => {
                        this._notificationService.sendNotification(new NotificationMessage(`Non è stato possibile recuperare la lista dei soggetti`, NOTIFICATION_TYPE.ERROR));
                        this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'resp error', resp);
                        const error = this._utils.getErrorResponse(resp);
                        setTimeout(() => {
                            this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_SUBJECTS, error.code, error.message);
                        }, AppSettings.TIMEOUT_SPINNER);
                    }
                );
        }


        const tabHeader = 'common';
        const objProposalSelect2 = this._persistence.get('SELECT_PROPOSAL');
        const params = new ProposalConsultationRequestParameters(objProposalSelect2.idProposal, objProposalSelect2.processInstanceID, tabHeader);
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, REQUEST_PRIORITY.HIGH);
        this._workQueueService.getConsultazioneProposal(params).pipe(takeUntil(this.destroy$))
            .subscribe(response => {
                    this.commonQueue = response.object;
                    setTimeout(() => {
                        this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, response.code);
                    }, AppSettings.TIMEOUT_SPINNER);
                }, respError => {
                    this._notificationService.sendNotification(new NotificationMessage(`Non è stato possibile recuperare le informazioni dell'header della consultazione`, NOTIFICATION_TYPE.ERROR));
                    this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'resp error', respError);
                    const error = this._utils.getErrorResponse(respError);
                    setTimeout(() => {
                        this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, error.code, error.message);
                    }, AppSettings.TIMEOUT_SPINNER);
                }
            );

    }

    ngOnDestroy() {
        this.isEditingSubscribe.unsubscribe();
    }

    showWIP() {
        this._modalService.openUnavailableContent(this.destroy$);
    }

    /**
     * popola le sezioni della consultazione della proposta
     */
    // populateConsultationSections(): void {
    //     const consHeaderFactory = new ConsultationHeaderQueueFactory();
    //     const consSubjectFactory = new ConsultationSubjectQueueFactory();
    //     const consCommercialFactory = new ConsultationCommercialQueueFactory();
    //     const consPropProdFactory = new ConsultationProposalProdQueueFactory();
    //     const consFinancialFactory = new ConsultationFinancialQueueFactory();

    //     this.consHeaderQueue = consHeaderFactory.getInstance(this.consultationQueue.header);
    //     this.consCommercialQueue = consCommercialFactory.getInstance(this.consultationQueue.commercial);
    //     this.consSubjectQueue = consSubjectFactory.getInstance(this.consultationQueue.soggetto);
    //     this.consProposalProdQueue = consPropProdFactory.getInstance(this.consultationQueue.proposta);
    //     this.consFinancialQueue = consFinancialFactory.getInstance(this.consultationQueue.financial);

    //     this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'obj', this.consultationQueue);
    //     this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'consHeaderQueue', this.consHeaderQueue);
    //     this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'consCommercialQueue', this.consCommercialQueue);
    //     this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'consSubjectQueue', this.consSubjectQueue);
    //     this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'consProposalProdQueue', this.consProposalProdQueue);
    //     this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'consFinancialQueue', this.consFinancialQueue);

    // }

    // populateConsultationSubjectsTabs(): void {
    //     // let consSubjectsFactory = new ConsultationSubjectsFactory();
    // }
}
