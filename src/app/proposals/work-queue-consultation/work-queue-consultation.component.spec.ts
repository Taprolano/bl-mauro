import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkQueueConsultationComponent } from './work-queue-consultation.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MbzDashboardModule } from '../../mbz-dashboard/mbz-dashboard.module';
import { ProposalsModule } from '../proposals.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { createTranslateLoader } from '../../app.module';
import { AppRoutingModule } from '../../app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../../../environments/environment';
import { AppComponent } from '../../app.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {DirectAccessGuard} from "../../shared/guard/direct-access/direct-access.guard";
import {AuthGuard} from "../../shared/guard";

xdescribe('WorkQueueConsultationComponent', () => {
    let component: WorkQueueConsultationComponent;
    let fixture: ComponentFixture<WorkQueueConsultationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WorkQueueConsultationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
