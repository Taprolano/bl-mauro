import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {KeyLeftTabs, KeyRightTabs, KeyAllRightTabs, KeyAllLeftTabs} from '../../HeadersTabsConstant';
import {UtilsService} from 'src/app/shared/services/utils.service';
import {Subject} from "rxjs/index";
import {AbstractModalService} from "../../../../shared/components/abstract-modal/abstract-modal.service";

@Component({
    selector: 'app-header-tabs',
    templateUrl: './header-tabs.component.html',
    styleUrls: ['./header-tabs.component.scss']
})

export class HeaderTabsComponent implements OnInit {

    @Output() tabLeftKeyChange = new EventEmitter();
    @Output() tabRightKeyChange = new EventEmitter();
    @Output() headerLeftChange = new EventEmitter();
    @Output() headerRightChange = new EventEmitter();
    @Input() headerKey: string;
    @Input() selectedTabKey: string;
    tabLabel: string;
    isStrict: boolean;


    possibleTabs = [];
    initTabs = [];
    initRightTabKey: string;
    initLeftTabKey: string;

    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private _utils: UtilsService, private _modalService: AbstractModalService) {
    }

    /**
     * init componente
     */
    ngOnInit() {
        this.isStrict = true;
        switch (this.headerKey) {

            case 'left':
                this.possibleTabs = [
                    {label: "PROPOSAL", key: KeyLeftTabs.PROP},
                    {label: "SUBJECTS", key: KeyLeftTabs.SUBJ},
                    {label: "CONTRACT_DATA", key: KeyLeftTabs.CONTRACTS}
                ];
                this.selectedTabKey = KeyLeftTabs.PROP;
                this.setTab(this.selectedTabKey);
                this.headerLeftChange.emit(KeyLeftTabs);
                this.initLeftTabKey = this.selectedTabKey;
                break;

            case 'right':
                this.possibleTabs = [
                    {label: "DOCUMENTS", key: KeyRightTabs.DOCS},
                    {label: "NOTES", key: KeyRightTabs.NOTES},
                    {label: "BANKS_DATA", key: KeyRightTabs.BANKS_DATA}
                ];
                this.selectedTabKey = KeyRightTabs.DOCS;
                this.setTab(this.selectedTabKey);
                this.headerRightChange.emit(KeyRightTabs);
                this.initRightTabKey = this.selectedTabKey;
                break;

            default:

        }
        this.initTabs = this._utils.assign(this.possibleTabs);
        if(this.selectedTabKey == KeyLeftTabs.PROP){
            this.enlargeSection();
        }
    }

    /**
     * imposta la tab da visualizzare
     * @param key
     */
    private setTabToSee(key: string): void {
        switch (key) {

            case KeyLeftTabs.CONTRACTS :
                this.tabLabel = 'CONTRACT_DATA';
                this.initLeftTabKey = key;
                break;

            case KeyLeftTabs.PROP:
                this.tabLabel = 'PROPOSAL';
                this.initLeftTabKey = key;
                break;

            case KeyLeftTabs.SUBJ:
                this.tabLabel = 'SUBJECTS_LIST';
                this.initLeftTabKey = key;
                break;

            case KeyRightTabs.BANKS_DATA:
                this.tabLabel = 'BANKS_DATA';
                this.initRightTabKey = key;
                break;

            case KeyRightTabs.DOCS:
                this.tabLabel = 'DOCS_LIST';
                this.initRightTabKey = key;
                break;

            case KeyRightTabs.NOTES:
                this.tabLabel = 'NOTES';
                this.initRightTabKey = key;
                break;

            default:
        }

    }

    /**
     * imposta la tab
     * @param key
     */
    setTab(key: string): void {
        // TODO: Rimuovere il WIP
        if (key == KeyLeftTabs.PROP || key == KeyRightTabs.DOCS ||
            key == KeyAllLeftTabs.DOCS || key == KeyAllLeftTabs.PROP ||
            key == KeyAllLeftTabs.SUBJ) {
            this.selectedTabKey = key;
            this.setTabToSee(key);
            switch (this.headerKey) {

                case 'left':
                    this.tabLeftKeyChange.emit(key);
                    break;

                case 'right':
                    this.tabRightKeyChange.emit(key);
                    break;
                default:

            }
        } else {
            this.showWIP();
        }

    }

    /**
     * allarga la view della sezione
     */
    enlargeSection(): void {
        this.isStrict = false;
        //TODO fare enlarge o enstrict della view
        //TODO accorpare header
        this.possibleTabs = [
            {label: "PROPOSAL", key: KeyLeftTabs.PROP},
            {label: "SUBJECTS", key: KeyLeftTabs.SUBJ},
            {label: "CONTRACT_DATA", key: KeyLeftTabs.CONTRACTS},
            {label: "DOCUMENTS", key: KeyRightTabs.DOCS},
            {label: "NOTES", key: KeyRightTabs.NOTES},
            {label: "BANKS_DATA", key: KeyRightTabs.BANKS_DATA}
        ];
        let right = document.getElementById('rightView');
        let left = document.getElementById('leftView');
        switch (this.headerKey) {
            case 'right':
                right.classList.remove('col-6');
                right.classList.add('col-12');
                left.style.display = 'none';
                this.headerRightChange.emit(KeyAllRightTabs);
                break;
            case 'left':
                left.classList.remove('col-6');
                left.classList.add('col-12');
                right.style.display = 'none';
                this.headerLeftChange.emit(KeyAllLeftTabs);
                break;
        }
        // document.getElementById("strictFinanzialConf").style.display = "none";
        // document.getElementById("largeFinanzialConf").style.display = "block";
    }

    /**
     * restringe la view della sezione
     */
    restrictSection(): void {
        this.isStrict = true;
        this.headerRightChange.emit(KeyRightTabs);
        this.headerLeftChange.emit(KeyLeftTabs);
        this.possibleTabs = this.initTabs;
        let right = document.getElementById('rightView');
        let left = document.getElementById('leftView');
        switch (this.headerKey) {
            case 'right':
                right.classList.remove('col-12');
                right.classList.add('col-6');
                left.style.display = 'initial';
                this.setTab(this.initRightTabKey);
                this.tabLeftKeyChange.emit(KeyLeftTabs.PROP);
                break;
            case 'left':
                left.classList.remove('col-12');
                left.classList.add('col-6');
                right.style.display = 'initial';
                this.setTab(this.initLeftTabKey);
                this.tabRightKeyChange.emit(KeyRightTabs.DOCS);
                break;
        }
        // document.getElementById("strictFinanzialConf").style.display = "initial";
        // document.getElementById("largeFinanzialConf").style.display = "none";
    }

    isSubjectsTabSelected(): boolean {
        return this.selectedTabKey == KeyLeftTabs.SUBJ;
    }

    showWIP() {
        this._modalService.openUnavailableContent(this.destroy$);
    }
}
