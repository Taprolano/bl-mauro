import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-consultation-notes',
    templateUrl: './consultation-notes.component.html',
    styleUrls: ['./consultation-notes.component.scss']
})
export class ConsultationNotesComponent implements OnInit {

    showContent: boolean;

    constructor() {
    }

    ngOnInit() {
        this.showContent = false;
    }

}
