import {Component, OnInit} from '@angular/core';
import {AbstractModalService} from "../../../../../../shared/components/abstract-modal/abstract-modal.service";
import {Subject} from "rxjs/index";

@Component({
    selector: 'app-consultation-data-banks',
    templateUrl: './consultation-data-banks.component.html',
    styleUrls: ['./consultation-data-banks.component.scss']
})
export class ConsultationDataBanksComponent implements OnInit {

    showContent: boolean;
    showContent1: boolean;

    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private _modalService: AbstractModalService) {
    }

    ngOnInit() {
        this.showContent = false;
        this.showContent1 = false;
    }

    showWIP(){
        this._modalService.openUnavailableContent(this.destroy$);
    }

}
