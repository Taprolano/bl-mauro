import {Component, OnInit} from '@angular/core';
import {Subject} from "rxjs/index";
import {AbstractModalService} from "../../../../../../shared/components/abstract-modal/abstract-modal.service";

@Component({
    selector: 'app-consultation-documents',
    templateUrl: './consultation-documents.component.html',
    styleUrls: ['./consultation-documents.component.scss']
})
export class ConsultationDocumentsComponent implements OnInit {

    showContent: boolean;

    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private _modalService: AbstractModalService) {
    }

    ngOnInit() {
        this.showContent = false;
    }

    showWIP(){
        this._modalService.openUnavailableContent(this.destroy$);
    }

}
