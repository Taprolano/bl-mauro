import { Component, OnInit, Input } from '@angular/core';
// import { KeyRightTabs } from '../../HeadersTabsConstant';
// import { ConsultationProposalProductQueue } from 'src/app/data-model/class/Consultation/ConsultationProposalProductQueue';

@Component({
  selector: 'app-consultation-right-section',
  templateUrl: './consultation-right-section.component.html',
  styleUrls: ['./consultation-right-section.component.scss']
})

export class ConsultationRightSectionComponent implements OnInit {

  @Input() selectedTabKey: string;
  @Input() allRightTabs;
  // @Input() proposalProduct: ConsultationProposalProductQueue;

  constructor() { }

  ngOnInit() {}

}
