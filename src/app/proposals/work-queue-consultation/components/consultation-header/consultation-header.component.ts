import {Component, OnInit} from '@angular/core';
import {ConsultationHeaderQueue} from "../../../../data-model/class/Consultation/ConsultationHeaderQueue";
import { Subject } from 'rxjs/index';
import { BeWorkQueue } from '../../../services/be-work-queue.service';
import { PersistenceService } from '../../../../shared/services/persistence.service';
import { RequestManagerService, REQUEST_PRIORITY } from '../../../../shared/services/request-manager.service';
import { NotificationService } from '../../../../shared/components/notification/notification.service';
import { LoggerService } from '../../../../shared/services/logger.service';
import { UtilsService } from '../../../../shared/services/utils.service';
import { ProposalConsultationRequestParameters } from '../../../../data-model/class/ProposalConsultationRequestParameters';
import { RequestManagerConstant } from '../../../requestManagerConstant';
import { takeUntil } from 'rxjs/internal/operators';
import { NotificationMessage, NOTIFICATION_TYPE } from '../../../../shared/components/notification/NotificationMessage';
import { AppSettings } from '../../../../../AppSettings';

@Component({
    selector: 'app-consultation-header',
    templateUrl: './consultation-header.component.html',
    styleUrls: ['./consultation-header.component.scss']
})
export class ConsultationHeaderComponent implements OnInit {

    headerQueue: ConsultationHeaderQueue;

    /** @ignore */
    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private _workQueueService: BeWorkQueue,
        private _persistence: PersistenceService,
        private _requestManagerService: RequestManagerService,
        private _notificationService: NotificationService,
        private _logger: LoggerService,
        private _utils: UtilsService) {
    }

    ngOnInit() {
        const tabHeader = 'header';
        const objProposalSelect = this._persistence.get('SELECT_PROPOSAL');
        const params = new ProposalConsultationRequestParameters(objProposalSelect.idProposal, objProposalSelect.processInstanceID, tabHeader);
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, REQUEST_PRIORITY.HIGH);
        this._workQueueService.getConsultazioneProposal(params).pipe(takeUntil(this.destroy$))
        .subscribe( response => {
                this.headerQueue = response.object;
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            }, respError => {
                this._notificationService.sendNotification(new NotificationMessage(`Non è stato possibile recuperare le informazioni dell'header della consultazione`, NOTIFICATION_TYPE.ERROR));
                this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'resp error', respError);
                const error = this._utils.getErrorResponse(respError);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
            }
        );
    }


}
