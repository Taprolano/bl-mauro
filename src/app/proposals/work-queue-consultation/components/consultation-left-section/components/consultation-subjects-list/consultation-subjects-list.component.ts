import { Component, OnInit, Input } from '@angular/core';
import { ConsultationSubjects } from '../../../../../../data-model/class/Consultation/Subjects/ConsultationSubjects';

@Component({
  selector: 'app-consultation-subjects-list',
  templateUrl: './consultation-subjects-list.component.html',
  styleUrls: ['./consultation-subjects-list.component.scss']
})
export class ConsultationSubjectsListComponent implements OnInit {

  @Input() isEditing: boolean = false;
  @Input() subjectsList: ConsultationSubjects[];

  constructor() { }

  ngOnInit() {
    console.log("Edit active: ", this.isEditing);
    console.log("Subjects list:", this.subjectsList);
  }

  isMale(genderCode: string): boolean {
    return genderCode == 'M';
  }

  getSubjectRole(code: string): string {
    let role: string;
    switch (code) {
      case 'TE': { 
        role = "TITOLARE EFFETTIVO";
        break;
      }
      case '': {
        break;
      }
      default: {
        role = '';
      }
    }
    return role;
  }
}
