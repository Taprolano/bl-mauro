import {Component, Input, OnInit} from '@angular/core';
import {Subject} from "rxjs/index";
import {AbstractModalService} from "../../../../../../../../shared/components/abstract-modal/abstract-modal.service";

@Component({
    selector: 'app-risk-sharing',
    templateUrl: './risk-sharing.component.html',
    styleUrls: ['./risk-sharing.component.scss']
})
export class RiskSharingComponent implements OnInit {

    @Input() isEditing: boolean;

    showContent: boolean;

    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private _modalService: AbstractModalService) {
    }

    ngOnInit() {
        this.showContent = false;
    }

    showWIP() {
        this._modalService.openUnavailableContent(this.destroy$);
    }

}
