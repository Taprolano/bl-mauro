import {Component, Input, OnInit} from '@angular/core';
import {Subject} from "rxjs/index";
import {AbstractModalService} from "../../../../../../../../shared/components/abstract-modal/abstract-modal.service";

@Component({
    selector: 'app-proposal-sale-details',
    templateUrl: './proposal-sale-details.component.html',
    styleUrls: ['./proposal-sale-details.component.scss']
})
export class ProposalSaleDetailsComponent implements OnInit {

    @Input() isEditing: boolean;

    showContent: boolean;

    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private _modalService: AbstractModalService) {
    }

    ngOnInit() {
        this.showContent = false;
    }

    showWIP() {
        this._modalService.openUnavailableContent(this.destroy$);
    }

}
