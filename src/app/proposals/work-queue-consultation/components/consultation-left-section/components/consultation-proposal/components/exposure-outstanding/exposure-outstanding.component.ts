import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-exposure-outstanding',
    templateUrl: './exposure-outstanding.component.html',
    styleUrls: ['./exposure-outstanding.component.scss']
})
export class ExposureOutstandingComponent implements OnInit {

    @Input() isEditing: boolean;

    showContent: boolean;

    constructor() {
    }

    ngOnInit() {
        this.showContent = false;
    }
}
