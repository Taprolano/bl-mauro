import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IPaymentMonth} from "../../data-model/IPaymentYear";
import {AbstractModalService} from "../../../../../../../../../../../../shared/components/abstract-modal/abstract-modal.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ProposalService} from "../../../../../../../../../../../../shared/services/proposal.service";

@Component({
    selector: 'app-payment-detail',
    templateUrl: './payment-detail.component.html',
    styleUrls: ['./payment-detail.component.scss']
})
export class PaymentDetailComponent implements OnInit, OnDestroy {


    @Input() modalParameters: any;

    //TODO Serve ancora? @Pietro
    @ViewChild("pdAsset") assetField: ElementRef;

    paymentMonth: IPaymentMonth;

    /** */
    dataForm: FormGroup;

    closeCallBack: Function;

    constructor(private _formBuilder: FormBuilder,
                private _abstractModalService: AbstractModalService,
                private _proposalService: ProposalService) {
    }

    ngOnInit() {

        this.closeCallBack = close.bind(this);

        this.paymentMonth = this.modalParameters.param.paymentMonth;

        if (this.paymentMonth != null) {
            this.dataForm = this._formBuilder.group({
                asset: this._formBuilder.control(this.paymentMonth.asset),
                service: this._formBuilder.control(this.paymentMonth.service),
                assistance: this._formBuilder.control(this.paymentMonth.assistance),
                managementFees: this._formBuilder.control(this.paymentMonth.managementFees),
            });
            this.dataForm.get('service').disable();
            this.dataForm.get('assistance').disable();
            this.dataForm.get('managementFees').disable();

            // setTimeout(()=>{this.dataForm.get('service').enable();}, 3000,this); //TEST


        }
        document.getElementById('modalBodyId').style.padding = '5px';
        //dimensioni modale
        document.getElementById('modalContentId').style.maxWidth = "600px";
        document.getElementById('modalContentId').style.margin = "auto";
        /*
                let assetObj = document.getElementById("asset-input")
                console.log(assetObj);
                assetObj.focus();

                 window.onload = function() {
                document.getElementById("asset-input").focus();
            };
        */

        // TODO trova nativeElement  undefined
        if (this.assetField.nativeElement) this.assetField.nativeElement.focus();
    };

    ngOnDestroy() {
        this._abstractModalService.emitUnsubscribe();
        this._abstractModalService.closeModal();
//        this._abstractModalService.sendOutput(null);
    }

    closeModal() {
        this._abstractModalService.sendOutput(null);
        this._abstractModalService.closeModal();
    };

    save() {
        this._abstractModalService.sendOutput(this.getPaymentMonth());
        this._abstractModalService.closeModal();
    }



    get insuranceAmmount(){
        return this.dataForm.get('service').value
    }
    get assitanceAmmount(){
        return this.dataForm.get('assistance').value
    }
    get managmentFees(){
        return this.dataForm.get('managementFees').value
    }

    getPaymentMonth(): IPaymentMonth {
        return {
            locked: true,
            year: this.paymentMonth.year,
            month: this.paymentMonth.month,
            service: this.dataForm.get('service').value,
            asset: this.dataForm.get('asset').value,
            assetPercentage: this.assetPercentage,
            assistance: this._proposalService.financialServicesAmmount,
            managementFees: this._proposalService.financialManagementFees
        };
    }

    get assetPercentage(): number {
        return Math.round((this.dataForm.get('asset').value / this._proposalService.foundedAmmount) * 100) / 100;
    }

}
