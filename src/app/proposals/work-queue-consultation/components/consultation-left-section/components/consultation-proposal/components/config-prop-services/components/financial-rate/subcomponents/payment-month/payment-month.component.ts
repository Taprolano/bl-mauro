import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {IPaymentMonth} from "../../data-model/IPaymentYear";
import {FormGroup, FormBuilder} from "@angular/forms";
import {AbstractModalService} from "../../../../../../../../../../../../shared/components/abstract-modal/abstract-modal.service";
import {PaymentDetailComponent} from "../payment-detail/payment-detail.component";
import {ReplaySubject, Subject} from "rxjs/index";
import {TranslateService} from "@ngx-translate/core";
import {takeUntil} from "rxjs/internal/operators";

const ipm: IPaymentMonth = {
    locked:false,
    year: null,
    month: null,
    service: null,
    asset: null,
    assetPercentage:null,
    assistance: null,
    managementFees: null};
    
    @Component({
        selector: 'app-payment-month',
        templateUrl: './payment-month.component.html',
        styleUrls: ['./payment-month.component.scss']
    })
    export class PaymentMonthComponent implements OnInit{ 
        
        @Input()
        paymentMonth: IPaymentMonth = null;
        
        @Input()
        enableEditing: boolean;
        
        @Output()
        paymentMonthChange = new EventEmitter<IPaymentMonth>();
        
        @Output()
        changeTrigger = new EventEmitter();
        
        /** */
        unsubscribe = new Subject<any>();
        
        @Input()
        name: string = "";
        
        @Output()
        nameChange = new EventEmitter();
        
        /** */
        dataForm: FormGroup;
        /** */
        editing = false;
        
        constructor(private _abstractModalService: AbstractModalService,
            private translate: TranslateService) {
            }
            
            ngOnInit() {
                this.createForm();
                // console.log('PM - PY:',this.paymentMonth? this.paymentMonth.year : 'nullPM' );
            }
            
            blurf() {
                this.editing = false;
            }
            
            createForm() {
                
            }
            
            //TODO capire perché c'è un errore di template all'apertura della modale (ngOnInit)
            paymentDetail() {
                if(this.enableEditing){
                    let customObj = {
                        component: PaymentDetailComponent,
                        parameters: {paymentMonth: this.paymentMonth}
                    };
                    
                    let onSubmit = true;
                    //        this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
                    this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
                    this._abstractModalService.captureOutcome.pipe(takeUntil(this._abstractModalService.unsubscribe)).subscribe(resp => {
                        if (!!resp) {
                            this.paymentMonth.locked = this.paymentMonth.asset != resp.asset;
                            this.paymentMonth.asset = resp.asset;
                            this.changeTrigger.next();
                        }
                    })
                }
                
            }

            get fullPrice(){
                return this.paymentMonth.service+this.paymentMonth.asset+this.paymentMonth.assistance+this.paymentMonth.managementFees;
            }
            
            /*
            this.destroy$.unsubscribe();
            */
        }