import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskPhenomensComponent } from './risk-phenomens.component';

describe('RiskPhenomensComponent', () => {
  let component: RiskPhenomensComponent;
  let fixture: ComponentFixture<RiskPhenomensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskPhenomensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskPhenomensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
