import {Component, OnInit, Input} from '@angular/core';
// import {ConsultationProposalProductQueue} from '../../../../../../data-model/class/Consultation/ConsultationProposalProductQueue';
// import {ConsultationSubjectQueue} from "../../../../../../data-model/class/Consultation/ConsultationSubjectQueue";
// import {ConsultationCommercialQueue} from "../../../../../../data-model/class/Consultation/ConsultationCommercialQueue";
// import {ConsultationFinancialQueue} from '../../../../../../data-model/class/Consultation/ConsultationFinancialQueue';

@Component({
    selector: 'app-consultation-proposal',
    templateUrl: './consultation-proposal.component.html',
    styleUrls: ['./consultation-proposal.component.scss']
})
export class ConsultationProposalComponent implements OnInit {

    // @Input() proposalProduct: ConsultationProposalProductQueue;
    // @Input() subjectQueue: ConsultationSubjectQueue;
    // @Input() commQueue: ConsultationCommercialQueue;
    // @Input() financialQueue: ConsultationFinancialQueue;

    @Input() isEditing: boolean = false;

    constructor() {
    }

    ngOnInit() {
    }

}
