import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-feel-life-input',
    templateUrl: './feel-life-input.component.html',
    styleUrls: ['./feel-life-input.component.scss']
})


export class FeelLifeInputComponent implements OnInit {
    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelLifeForm: FormGroup;
    submitted = false;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.submitted = false;
    }

    get f() {
        return this.feelLifeForm.controls;
    }


    validateForm(): boolean {
        this.submitted = true;
        return this.feelLifeForm.valid;

    }

    getServiceData(): FinancialValue {

        if (this.validateForm) {
            // this.financialValue.code = 200;
            this.financialValue.message = '';
            this.financialValue.object = {
                common: null
            };
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }


}
