import {Component, OnInit, Input} from '@angular/core';
import {ConsultationFinancialQueue} from '../../../../../../../../data-model/class/Consultation/ConsultationFinancialQueue';
import {ProposalConsultationRequestParameters} from "../../../../../../../../data-model/class/ProposalConsultationRequestParameters";
import {RequestManagerConstant} from "../../../../../../../requestManagerConstant";
import {REQUEST_PRIORITY, RequestManagerService} from "../../../../../../../../shared/services/request-manager.service";
import {takeUntil} from "rxjs/operators";
import {AppSettings} from "../../../../../../../../../AppSettings";
import {
    NOTIFICATION_TYPE,
    NotificationMessage
} from "../../../../../../../../shared/components/notification/NotificationMessage";
import {BeWorkQueue} from "../../../../../../../services/be-work-queue.service";
import {PersistenceService} from "../../../../../../../../shared/services/persistence.service";
import {NotificationService} from "../../../../../../../../shared/components/notification/notification.service";
import {LoggerService} from "../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../shared/services/utils.service";
import {Subject} from "rxjs";
import {IPaymentMonth, IPaymentYear, Months} from "./components/financial-rate/data-model/IPaymentYear";
import {ProposalService} from "../../../../../../../../shared/services/proposal.service";
import {FinancialConstant} from "./FinancialConstant";
import {ListValuesEnhancedReqParams} from "../../../../../../../../data-model/class/ListValuesEnhancedReqParams";
import {ListValuesEnhanced, ValueListEnhanced} from "../../../../../../../../data-model/class/ListValuesEnhanced";
import {BeFinancialService} from "../../../../../../../../shared/services/be-financial.service";
import {IRange} from "../../../../../../../../data-model/class/IRange";
import {FormGroup} from "@angular/forms";

const monthNull: IPaymentMonth[] = [null, null, null, null, null, null, null, null, null, null, null, null];

/** @ignore listKey degli oggetti ritornati da listOfValuesEnhanced */ //TODO WIP
const enum SELECT_KEYS {
    financialProduct = 'financialProduct',
    frequency = 'FREQUENCY',
    mileage = 'KILOMETERS',
    financialDuration = 'RANGE_DURATION', //?
    effectiveDate = 'effectiveDate',
    interestType = 'INTEREST',
    rangeDownpayment = 'RANGE_DOWNPAYMENT',
    rangeBalloon = 'RANGE_RATE_BALLOON'
}

@Component({
    selector: 'app-config-prop-services',
    templateUrl: './config-prop-services.component.html',
    styleUrls: ['./config-prop-services.component.scss']
})
export class ConfigPropServicesComponent implements OnInit {

    financialQueue: ConsultationFinancialQueue;

    /** @ignore */
    destroy$: Subject<boolean> = new Subject<boolean>();

    /** lista dei pagamenti */
    paymentSchedule: IPaymentYear[];

    @Input() isEditing: boolean;

    showContent: boolean;

    /** Mappa delle info di enhancedValuesList */
    enhancedValuesMap: Map<string, ValueListEnhanced[]>;

    rangeBalloon: IRange;

    rangeDownpayment;

    /** Form contenente le select poste nel main body di financial */
    selectForm: FormGroup;

    /**
     * Financial view Values list
     */
    public enhancedValuesList: ListValuesEnhanced[];

    constructor(private _workQueueService: BeWorkQueue,
                private _persistence: PersistenceService,
                private _requestManagerService: RequestManagerService,
                private _notificationService: NotificationService,
                private _logger: LoggerService,
                private _utils: UtilsService,
                private _proposalService: ProposalService,
                private _beFinancialService: BeFinancialService) {
    }

    ngOnInit() {
        this.showContent = false;
    }

    initFinancialData(): void {
        const tabHeader = 'financial';
        const objProposalSelect = this._persistence.get('SELECT_PROPOSAL');
        const params = new ProposalConsultationRequestParameters(objProposalSelect.idProposal, objProposalSelect.processInstanceID, tabHeader);
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, REQUEST_PRIORITY.HIGH);
        this._workQueueService.getConsultazioneProposal(params).pipe(takeUntil(this.destroy$))
            .subscribe(response => {
                    this.financialQueue = response.object;
                    this.setSchedule();
                    setTimeout(() => {
                        this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, response.code);
                    }, AppSettings.TIMEOUT_SPINNER);
                    this.loadListOfValues();
                }, respError => {
                    this._notificationService.sendNotification(new NotificationMessage(`Non è stato possibile recuperare le informazioni financial della consultazione`, NOTIFICATION_TYPE.ERROR));
                    this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'resp error', respError);
                    const error = this._utils.getErrorResponse(respError);
                    setTimeout(() => {
                        this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_HEADER, error.code, error.message);
                    }, AppSettings.TIMEOUT_SPINNER);
                }
            );
    }

    toggleAccordionFinancial(): void {
        this.showContent = !this.showContent;
        if (this.showContent) {
            this.initFinancialData();
        }
    }

    setSchedule() {
        this.paymentSchedule = [];

        let dateArray: string[];
        let py: IPaymentYear;
        let paymentMonths: IPaymentMonth[];

        let year: number = null;
        let paymentYears: IPaymentYear[] = [];


        for (let r of this.financialQueue.rate) {
            if (!(r.balloon || r.anticipo)) {
                dateArray = r.dataPagamentoRata.split('/');
                let yearIndex = FinancialConstant.indexOfYear(+dateArray[2], paymentYears);
                let monthIndex = +dateArray[1] - 1;
                if (yearIndex < 0) {
                    let months = this._utils.assign(monthNull);
                    months[monthIndex] = {
                        locked: r.bloccato,
                        month: Months[monthIndex],
                        year: year,
                        service: this._proposalService.financialServicesAmmountInsurance,
                        asset: r.cfAmount,
                        assistance: this._proposalService.financialServicesAmmountAssistance,
                        managementFees: this._proposalService.financialManagementFees
                    };
                    paymentYears.push({
                        year: +dateArray[2],
                        paymentMonths: this._utils.assign(months)
                    });
                } else {
                    this._logger.logInfo('FinancialComponent', 'setSchedule', `set ${monthIndex + 1}/${+dateArray[2]}`);
                    paymentYears[yearIndex].paymentMonths[monthIndex] = {
                        locked: r.bloccato,
                        month: Months[monthIndex],
                        year: year,
                        service: this._proposalService.financialServicesAmmountInsurance,
                        asset: r.cfAmount,
                        assetPercentage: r.cfAmountPercentage,
                        assistance: this._proposalService.financialServicesAmmountAssistance,
                        managementFees: this._proposalService.financialManagementFees
                    };
                }


            }
        }

        paymentYears.sort(function (obj1, obj2) {
            return obj1.year - obj2.year;
        });

        this.paymentSchedule = paymentYears;

    }

    private loadListOfValues() {

        this._logger.logDebug('Financial Component', 'loadListOfValues', 'START');

        // TODO Da recuperare contractLenght, financedAmount e minimumKilometers
        let listValuesEnhancedReqParams: ListValuesEnhancedReqParams = new ListValuesEnhancedReqParams(
            this.financialQueue.assetClassCode,
            this.financialQueue.assetType,
            this.financialQueue.baumuster,
            this.financialQueue.brandId,
            this.financialQueue.campaignCode,
            0,
            this.financialQueue.financialProductCode,
            0,
            'PAYMENT_METHODS_DOWNPAYMENT,PAYMENT_METHODS_RATE,VAT,MORTAGAGE,FREQUENCY,KILOMETERS,INTEREST,RANGE_DURATION,RANGE_DOWNPAYMENT,RANGE_RATE_BALLOON',
            null);

        this._beFinancialService.getAllFinancialViewListValuesEnhanced(listValuesEnhancedReqParams, false)
            .pipe(takeUntil(this.destroy$)).subscribe(
            response => {
                this._logger.logInfo('FinancialComponent', 'loadListOfValues', 'load values list ', response);

                this.enhancedValuesList = response.object;
                //TODO: accedere direttamente alla map invece che iterare sull'array ogni volta che serve una info
                this.enhancedValuesMap = new Map<string, ValueListEnhanced[]>();

                this.enhancedValuesList.forEach(x => {
                    this.enhancedValuesMap.set(x.listCode, x.values);
                });

                /*this._logger.logInfo(this.constructor.name, 'enhancedValuesList', this.enhancedValuesList);
                this._logger.logInfo(this.constructor.name, 'enhancedValuesMap', this.enhancedValuesMap);
                this._logger.logInfo(this.constructor.name, 'enhancedValuesBool', this.enhancedValuesMap.get(SELECT_KEYS.frequency)[0].defaultValue);
*/
                //setta default a mano perché per qualche ragione [selected=x.isDefault] sull'html non va

                // this.setDefaultValue('frequency', SELECT_KEYS.frequency);
                // this.setDefaultValue('interestType', SELECT_KEYS.interestType);
                // this.setDefaultValue('financialDuration', SELECT_KEYS.financialDuration);
                // this.setDefaultValue('mileage', SELECT_KEYS.mileage);
                // this.setDefaultValue('interestType', SELECT_KEYS.interestType);

                //this._logger.logInfo(this.constructor.name, 'enhancedValuesList', 'mileage: ' + this.kilometraggio);
                //this.financialQueue.kilometraggio = this.selectForm.get('mileage').value;
                // this._logger.logInfo(this.constructor.name, 'enhancedValuesList', 'kilometraggio: ' + this.financialQueue.kilometraggio);
                /*
                this.selectForm.get('frequency').patchValue(
                    this.enhancedValuesMap.get(SELECT_KEYS.frequency).find(x => {
                        return x.defaultValue;
                    }).value
                );
                this.selectForm.get('interestType').patchValue(
                    this.enhancedValuesMap.get(SELECT_KEYS.interestType).find(x => {
                        return x.defaultValue;
                    }).value
                );                let defDuration = this.enhancedValuesMap.get(SELECT_KEYS.financialDuration).find(x => x.defaultValue);
                if(!!defDuration) this.selectForm.get('financialDuration').patchValue( defDuration.value );
                */

                // this.rangeBalloon = this.getRange(this.enhancedValuesMap.get(SELECT_KEYS.rangeBalloon)[0]);
                // this.rangeDownpayment = this.getRange(this.enhancedValuesMap.get(SELECT_KEYS.rangeDownpayment)[0]);

                //this.setEffectiveDateList();


                // console.log('frequency--->',this.enhancedValuesMap.get(SELECT_KEYS.frequency));

                /*
                                /!** Update financial view select *!/
                                this.populateAllFinancialViewUsingChildren();*/
                console.warn(this.enhancedValuesMap);
                setTimeout(() => {
                    // this._requestManagerService.handleRequest(RequestManagerConstant.GET_DEALERS, response.code);
                }, AppSettings.TIMEOUT_SPINNER);

            }, resp => {
                this._logger.logInfo('FinancialComponent', 'loadListOfValues', 'load values list error ', resp);
                // const error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    // this._requestManagerService.handleRequest(RequestManagerConstant.GET_DEALERS, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
            });

        this._logger.logDebug('Financial Component', 'loadListOfValues', 'END');

    }

    setDefaultValue(selectName: string, key: string) {
        let defaultValue = this.enhancedValuesMap.get(key).find(x => x.defaultValue);
        if (!!defaultValue) this.selectForm.get(selectName).patchValue(defaultValue.value);
    }


    getRange(obj: any): IRange {
        if (obj.hasOwnProperty('min') && obj.hasOwnProperty('max')) {
            return {
                min: +obj['min'],
                max: +obj['max']
            };
        }

        return null;
    }


}
