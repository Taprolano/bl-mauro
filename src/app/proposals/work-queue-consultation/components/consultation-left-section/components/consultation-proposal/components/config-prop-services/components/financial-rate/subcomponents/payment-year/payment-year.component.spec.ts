import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentYearComponent } from './payment-year.component';

describe('PaymentYearComponent', () => {
  let component: PaymentYearComponent;
  let fixture: ComponentFixture<PaymentYearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentYearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
