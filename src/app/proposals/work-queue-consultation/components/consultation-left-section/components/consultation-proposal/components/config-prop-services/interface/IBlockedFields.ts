export interface IBlockedFields {
    anticipo: boolean,
    matriceRate: boolean,
    importoRataBalloon: boolean,
    tan: boolean,
    rata: boolean,
    usePercentage: boolean
}
