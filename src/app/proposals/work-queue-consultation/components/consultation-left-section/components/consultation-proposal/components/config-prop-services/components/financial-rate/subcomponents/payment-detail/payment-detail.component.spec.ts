import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaymentDetailComponent} from './payment-detail.component';
import {HttpClient, HttpClientModule} from '../../../../../../../../../node_modules/@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader} from '../../../../../../../app.module';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA, Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {IPaymentMonth} from '../../data-model/IPaymentYear';
import any = jasmine.any;

describe('PaymentDetailComponent', () => {
    let component: PaymentDetailComponent;
    let fixture: ComponentFixture<PaymentDetailComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [PaymentDetailComponent],
            providers: [CookieService],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaymentDetailComponent);
        component = fixture.componentInstance;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            asset: builder.control(0),
            service: builder.control(0),
            nomeFormField: builder.control(true),
        });
        component.dataForm = mockFormGroup;

        let mockPaymMonth: IPaymentMonth = {
            locked: false,
            month: 'JAN',
            year: 0,
            service: 0,
            asset: 0,
            assetPercentage: 0,
        };

        let mockModalParameters = {
            param: {
                paymentMonth: mockPaymMonth
            }
        };
        component.modalParameters = mockModalParameters;

        spyOn(document, 'getElementById').and.callFake(function () {
            return {
                style: any
            };
        });

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it(' onDestroy test', () => {
        component.ngOnDestroy();
    });

    it(' closeModal test', () => {
        component.closeModal();
    });

    it(' save test', () => {
        component.save();
    });

    it(' getPaymentMonth test', () => {
        component.getPaymentMonth();
    });

});
