import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {IBaseLightInput} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-feel-star-base-light-input',
    templateUrl: './feel-star-base-light-input.component.html',
    styleUrls: ['./feel-star-base-light-input.component.scss']
})
export class FeelStarBaseLightInputComponent implements OnInit {
    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelStarBaseLightForm: FormGroup;
    submitted = false;
    feelStarBaseLightInput: IBaseLightInput;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.feelStarBaseLightForm = this._formBuilder.group({
            quattroZampe: [],
            autoCortesia: [],
            infortuniConducente: [],
            provincia: [],
            satellitare: [],
            privacy: []
        });
        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.quattroZampe)) this.feelStarBaseLightForm.controls['quattroZampe'].setValue(this.financialValue.object.quattroZampe);
            if (!this._utils.isVoid(this.financialValue.object.autoCortesia)) this.feelStarBaseLightForm.controls['autoCortesia'].setValue(this.financialValue.object.autoCortesia);
            if (!this._utils.isVoid(this.financialValue.object.infortuniConducente)) this.feelStarBaseLightForm.controls['infortuniConducente'].setValue(this.financialValue.object.infortuniConducente);
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelStarBaseLightForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelStarBaseLightForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.privacy)) this.feelStarBaseLightForm.controls['privacy'].setValue(this.financialValue.object.privacy);
        }
    }


    get f() {
        return this.feelStarBaseLightForm.controls;
    }


    valideteForm(): boolean {
        this.submitted = true;
        return this.feelStarBaseLightForm.valid;
    }


    getServiceData(): FinancialValue {

        if (this.valideteForm) {
            // this.financialValue.code = 200;

            this.feelStarBaseLightInput = {
                quattroZampe: this.feelStarBaseLightForm.value.quattroZampe,
                autoCortesia: this.feelStarBaseLightForm.value.autoCortesia,
                infortuniConducente: this.feelStarBaseLightForm.value.infortuniConducente,
                commonFeelStar: {
                    provincia: this.feelStarBaseLightForm.value.provincia,
                    satellitare: this.feelStarBaseLightForm.value.satellitare,
                    privacy: this.feelStarBaseLightForm.value.privacy
                },
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelStarBaseLightInput;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}
