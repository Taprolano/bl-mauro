import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ICommon} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-common-input',
    templateUrl: './common-input.component.html',
    styleUrls: ['./common-input.component.scss']
})
export class CommonInputComponent implements OnInit {

    commonForm: FormGroup;
    submitted = false;
    private isValid: boolean;

    @Output() isTaxi: EventEmitter<boolean>;

    @Input() commonInputValue: ICommon;

    flagIsTaxi: boolean = false;

    @Input()
    vatPercentage: number;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
        this.isTaxi = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.submitted = false;
        // Di default isTaxi è a false

        this.commonForm = this._formBuilder.group({
            noleggioOtaxi: [],
            valoreAssicurativo: [],
            recuperoIva: [],
            deroga: []
        });

        this.commonForm.get('valoreAssicurativo').disable();

        if (!this._utils.isVoid(this.commonInputValue.noleggioOtaxi)) this.commonForm.controls['noleggioOtaxi'].setValue(this.commonInputValue.noleggioOtaxi);
        if (!this._utils.isVoid(this.commonInputValue.recuperoIva)) this.commonForm.controls['recuperoIva'].setValue(this.commonInputValue.recuperoIva);
        if (!this._utils.isVoid(this.commonInputValue.deroga)) this.commonForm.controls['deroga'].setValue(this.commonInputValue.deroga);

        let toAdd = (this.vatPercentage * this.commonInputValue.valoreAssicurativo) / 100;

        this.commonForm.get('valoreAssicurativo').patchValue(Math.round(this.commonInputValue.valoreAssicurativo + toAdd));

    }

    validateForm(): boolean {
        this.submitted = true;

        if (this._utils.isVoid(this.commonForm.getRawValue().valoreAssicurativo)) {
            this._utils.addCssClassByIds('a-input--error', 'valoreAssicurativo');
            this.isValid = false;
        }
        if (this._utils.isVoid(this.commonForm.controls['noleggioOtaxi'].value)) {
            this._utils.addCssClassByIds('a-input--error', 'noleggioOtaxi');
        } else {
            this.isValid = true;
        }

        return this.isValid;
    }

    removeError(id: string) {
        this._utils.removeCssClassByIds('a-input--error', id);
    }

    handleChange(evt) {
        let temp = evt.target.value === 'true';
        this.isTaxi.emit(temp);
        this.flagIsTaxi = temp;
    }

    getServiceData(): ICommon {
        this.validateForm();
        if (this.isValid) {

            this.commonInputValue = {
                noleggioOtaxi: this.commonForm.value.noleggioOtaxi,
                valoreAssicurativo: this.commonForm.getRawValue().valoreAssicurativo,
                recuperoIva: !!this.commonForm.value.recuperoIva,
                deroga: !!this.commonForm.value.deroga
            };


        } else {
            this.commonInputValue = null;
        }
        return this.commonInputValue;

    }

    toggleVatFromTotal(): void {
        let check = this.commonForm.get('recuperoIva').value;
        if (check) {
            this.commonForm.get('valoreAssicurativo').patchValue(this.commonInputValue.valoreAssicurativo);
        } else {
            let toAdd = (this.vatPercentage * this.commonInputValue.valoreAssicurativo) / 100;
            this.commonForm.get('valoreAssicurativo').patchValue(
                Math.round(this.commonInputValue.valoreAssicurativo + toAdd)
            );
        }
    }
}
