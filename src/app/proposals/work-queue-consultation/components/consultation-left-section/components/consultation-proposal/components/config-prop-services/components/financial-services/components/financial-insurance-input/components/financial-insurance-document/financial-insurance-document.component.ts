import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractModalService} from "../../../../../../../../../../../../../../shared/components/abstract-modal/abstract-modal.service";
import {Attachment} from "../../../../../../../../../../../../../../data-model/class/Attachment";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {BeFinancialService} from "../../../../../../../../../../../../../../shared/services/be-financial.service";

@Component({
    selector: 'app-financia-insurance-document',
    templateUrl: './financial-insurance-document.component.html',
    styleUrls: ['./financial-insurance-document.component.scss']
})
export class FinanciaInsuranceDocumentComponent implements OnInit, OnDestroy {

    /**@ignore
     * Parametri componente passati dal costruttore modale
     */
    @Input() modalParameters: any;

    /**
     * Messaggio mostrato dalla modale
     * @clazz {string}
     */
    message = '';

    /** @ignore classi css assegnate alla modale */
    cssClasses: string[] = [];

    /** Lista di attachment con le url dei pdf */
    attachments: Attachment[];

    constructor(
        private _abstractModalService: AbstractModalService,
        private _financialService: BeFinancialService,
        private _logger: LoggerService) {
    }

    ngOnInit() {
        this.cssClasses = this.modalParameters.param.cssClasses.length > 0 ? this.modalParameters.param.cssClasses : ['default'];
        this.message = this.modalParameters.param.message ? this.modalParameters.param.message : '';
        this.attachments = this.modalParameters.param.attachments ? this.modalParameters.param.attachments : '';
    }

    /**
     * Chiude modale
     */
    closeModal() {
        this._abstractModalService.closeModal();
    }

    /**
     * Innesco per la chiamata al BE, con input l'oggetto attachment completo
     * @param {Attachment} elem
     */
    getDocument(elem: Attachment) {
        this._logger.logInfo(this.constructor.name, 'getDcoument', elem);
        this._financialService.getPdfFile(elem);
    }


    ngOnDestroy() {
        console.warn("destroy");
    }


}
