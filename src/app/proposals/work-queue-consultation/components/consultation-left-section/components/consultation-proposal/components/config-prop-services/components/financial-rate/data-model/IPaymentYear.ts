export interface IPaymentYear {
    year: number,
    paymentMonths: IPaymentMonth[];
}

export interface IPaymentMonth {
    locked: boolean,
    month: string,
    year: number,
    service: number,
    asset: number,
    assetPercentage: number,
    assistance: number,
    managementFees: number
}

export const Months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];