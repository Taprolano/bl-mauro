import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {IBaseInput} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-feel-star-base-input',
    templateUrl: './feel-star-base-input.component.html',
    styleUrls: ['./feel-star-base-input.component.scss']
})
export class FeelStarBaseInputComponent implements OnInit {
    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelStarBaseForm: FormGroup;
    submitted = false;
    feelStarBaseInput: IBaseInput;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.feelStarBaseForm = this._formBuilder.group({
            quattroZampe: [],
            autoCortesia: [],
            infortuniConducente: [],
            provincia: [],
            satellitare: [],
            privacy: []

        });

        // Recupero i datai dalla form se sono presenti cosi funziona ma non gli arriva l obj
        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.quattroZampe)) this.feelStarBaseForm.controls['quattroZampe'].setValue(this.financialValue.object.quattroZampe);
            if (!this._utils.isVoid(this.financialValue.object.autoCortesia)) this.feelStarBaseForm.controls['autoCortesia'].setValue(this.financialValue.object.autoCortesia);
            if (!this._utils.isVoid(this.financialValue.object.infortuniConducente)) this.feelStarBaseForm.controls['infortuniConducente'].setValue(this.financialValue.object.infortuniConducente);
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelStarBaseForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelStarBaseForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.privacy)) this.feelStarBaseForm.controls['privacy'].setValue(this.financialValue.object.privacy);
        }
    }

    get f() {
        return this.feelStarBaseForm.controls;
    }


    valideteForm(): boolean {
        this.submitted = true;
        return this.feelStarBaseForm.valid;
    }


    getServiceData(): FinancialValue {


        if (this.valideteForm) {
            // this.financialValue.code = 200;

            this.feelStarBaseInput = {
                quattroZampe: this.feelStarBaseForm.value.quattroZampe,
                autoCortesia: this.feelStarBaseForm.value.autoCortesia,
                infortuniConducente: this.feelStarBaseForm.value.infortuniConducente,
                commonFeelStar: {
                    provincia: this.feelStarBaseForm.value.provincia,
                    satellitare: this.feelStarBaseForm.value.satellitare,
                    privacy: this.feelStarBaseForm.value.privacy
                },
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelStarBaseInput;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }


}
