import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-service-plus-compact-input',
    templateUrl: './service-plus-compact-input.component.html',
    styleUrls: ['./service-plus-compact-input.component.scss']
})
export class ServicePlusCompactInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;
    serviceCompacttForm: FormGroup;
    serviceCompacttInput: any;
    private submitted = false;
    private isValid: boolean;

    annualDistance;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.submitted = false;
        this.serviceCompacttForm = this._formBuilder.group({
            annualDistance: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.annualDistance)) this.annualDistance = this.financialValue.object.annualDistance;
        } else {
            this.annualDistance = 'default';
        }

    }

    get f() {
        return this.serviceCompacttForm.controls;
    }

    validateForm(): boolean {
        this.submitted = true;

        if (this._utils.isVoid(this.serviceCompacttForm.controls['annualDistance'].value) || this.serviceCompacttForm.controls['annualDistance'].value === 'default') {
            this._utils.addCssClassByIds('a-input--error', 'annualDistance');
            this.isValid = false;
        } else {
            this.isValid = true;
        }

        return this.isValid;
    }

    getServiceData(): FinancialValue {
        this.validateForm();
        this.financialValue.code =  200;

        if (this.isValid === true) {

            this.serviceCompacttInput = {
                annualDistance: this.serviceCompacttForm.value.annualDistance,
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.serviceCompacttInput;

            this._logger.logInfo('Compact Input', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('Compact Input', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }

    removeError(id: string) {
        this._utils.removeCssClassByIds('a-input--error', id);
    }
}


