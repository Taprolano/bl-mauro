import {Component, Input, OnInit} from '@angular/core';
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";

@Component({
    selector: 'app-feel-safe-input',
    templateUrl: './feel-safe-input.component.html',
    styleUrls: ['./feel-safe-input.component.scss']
})
export class FeelSafeInputComponent implements OnInit {
    @Input()
    financialValue: FinancialValue;

    submitted = false;

    constructor(private _logger: LoggerService) {
    }

    ngOnInit() {
    }

    validateForm(): boolean {
        this.submitted = true;
        return true;
    }

    getServiceData(): FinancialValue {

        if (this.validateForm) {
            // this.financialValue.code = 200;
            this.financialValue.message = '';
            this.financialValue.object = {
                common: null
            };

            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}
