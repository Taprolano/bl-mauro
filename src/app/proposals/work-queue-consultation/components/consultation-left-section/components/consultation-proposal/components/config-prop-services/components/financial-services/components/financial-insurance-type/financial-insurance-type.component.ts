import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {FinanciaInsuranceDocumentComponent} from '../financial-insurance-input/components/financial-insurance-document/financial-insurance-document.component';
import {FinancialServices} from "../../../../../../../../../../../../data-model/class/FinancialServices";
import {FinancialValue} from "../../../../../../../../../../../../data-model/class/FinancialValue";
import {AbstractModalService} from "../../../../../../../../../../../../shared/components/abstract-modal/abstract-modal.service";
import {LoggerService} from "../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../shared/services/utils.service";



@Component({
    selector: 'app-financial-insurance-type',
    templateUrl: './financial-insurance-type.component.html',
    styleUrls: ['./financial-insurance-type.component.scss']
})
export class FinancialInsuranceTypeComponent implements OnInit {

    @Input()
    insuranceType: FinancialServices;

    @Input()
    id: any;

    @Input()
    typeIdActive: any;

    @Input()
    financialValueInputList: FinancialValue[];

    @Output()
    financialIdEvent: EventEmitter<FinancialValue> = new EventEmitter<FinancialValue>();

    @Output()
    deleteTypeEvent: EventEmitter<FinancialValue> = new EventEmitter<FinancialValue>();

    flag: boolean = true;

    price: number;

    financialValue: FinancialValue;

    /**@ignore*/
    private modalUnsub$: Subject<any> = new Subject<any>();

    constructor(private _abstractModalService: AbstractModalService, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
    }

    loadInsuranceType() {

        this.financialValue = new FinancialValue();
        this.financialValue.idParent = this.id;
        this.financialValue.supplyTypeCode = this.insuranceType.supplyTypeCode;
        this.financialValue.supplyTypeDesc = this.insuranceType.supplyTypeDesc;

        this.financialIdEvent.emit(this.financialValue);


        /*

            // Controlla se è gia presente un servizio con "code" nella lista di servizi selezionati
            for (let elem of this.financialValueList) {
                if (elem != undefined && elem.supplyTypeCode == this.financialElemSelected.supplyTypeCode) {
                    this._logger.logInfo('FinancialInsurance', 'loadDetail', ' Presente', elem);

                    // Setto tutti i valori del elem trovato nella lista di servizi già attivi alla nuova istanza del serivizio.
                    // Non si verificano problemi copiare l'istanza perchè all'aggiunta successiva ,
                    // aggiorna solo la form con i nuovi valori!

                    // TODO: qua non funziona se ho due elementi dello stesso padre, perche prendo sempre il primo.
                    this.financialValue.idParent = elem.idParent;
                    this.financialValue.idChild = elem.idChild;
                    this.financialValue.code = elem.code;
                    this.financialValue.message = elem.message;
                    this.financialValue.object = elem.object;
                    this.financialValue.supplyElemCode = elem.supplyElemCode;
                    this.financialValue.supplyElemDesc = elem.supplyElemDesc;
                    this.financialValue.supplyTypeCode = elem.supplyTypeCode;
                    this.financialValue.supplyTypeDesc = elem.supplyTypeDesc;
                    this.financialValue.clazz = elem.clazz;
                    this.financialValue.isAssistence = elem.isAssistence;
                    this.financialValue.isSelected = elem.isSelected;

                    // Chiamo la fuzione "loadInput" passandogli in input l'oggetto "this.financialValue"
                    // this.loadInput(this.financialValue);
                } else {
                    this._logger.logInfo('FinancialInsurance', 'loadDetail', 'idParent NON Presente');
                }
            }


         */

    }

    checkIfSelected(): boolean {
        this.price = null;

        for (let elem of this.financialValueInputList) {
            if (elem.supplyTypeCode === this.insuranceType.supplyTypeCode) {
                this.price += +elem.price;
            }
        }

        return !!this.price;
    }

    loadDocument() {

        //campi modale
        let id = 'finacial-insurance-tab-pdf';
        let cssClasses = [];
        let message = 'TAB_PDF';
        let attachments = this.insuranceType.attachments;
        //crea modale
        let popupComponent = {
            component: FinanciaInsuranceDocumentComponent,
            parameters: {id, cssClasses, message, attachments}
        };
        let onSubmit = true;
        this._abstractModalService.showModalCallback(onSubmit, this.modalUnsub$, popupComponent);
        //gestione output modale
        this._abstractModalService.captureOutcome.subscribe(
            result => {
                this._abstractModalService.closeModal();
            },
            err => {
                this._logger.logError(this.constructor.name, 'confirmTabSwitch', 'Errore in output modale');
                this._abstractModalService.closeModal();

            }
        );
    }

    // Creo un oggetto dello stesso tipo che prende in input la removeInsuranceService del padre ed invio l'evento al padre
    deleteType() {
        let obj = this.financialValueInputList.find(elem => {
            return elem.supplyTypeCode == this.insuranceType.supplyTypeCode;
        });

        let deleteItem: FinancialValue = new FinancialValue();
        deleteItem.supplyElemCode = obj.supplyElemCode;
        deleteItem.descr = obj.supplyElemDesc;

        this.deleteTypeEvent.emit(deleteItem);
    }




}
