import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhenomenDetailComponent } from './phenomen-detail.component';

describe('PhenomenDetailComponent', () => {
  let component: PhenomenDetailComponent;
  let fixture: ComponentFixture<PhenomenDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhenomenDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhenomenDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
