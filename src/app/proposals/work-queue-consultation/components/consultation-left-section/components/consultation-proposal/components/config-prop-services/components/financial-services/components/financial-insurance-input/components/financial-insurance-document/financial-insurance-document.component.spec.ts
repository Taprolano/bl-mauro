import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FinanciaInsuranceDocumentComponent} from './financial-insurance-document.component';
import {SvgIconComponent} from '../../../../../../../../../shared/components/svg-icon/svg-icon.component';
import {Attachment} from '../../../../../../../../../data-model/class/Attachment';

describe('FinanciaInsuranceDocumentComponent', () => {
    let component: FinanciaInsuranceDocumentComponent;
    let fixture: ComponentFixture<FinanciaInsuranceDocumentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FinanciaInsuranceDocumentComponent,
                SvgIconComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FinanciaInsuranceDocumentComponent);
        component = fixture.componentInstance;

        let mockCssClasses: string[] = ['test', 'test1'];

        let mockFinAtt: Attachment = new Attachment(' ', ' ', ' ');
        let mockAttachments: Attachment[] = [];
        mockAttachments.push(mockFinAtt);

        let mockModalParameter: Object = new Object();
        mockModalParameter = {
            param: {
                cssClasses: mockCssClasses,
                message: 'test',
                attachments: mockAttachments
            }
        };
        component.modalParameters = mockModalParameter;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('onDestroy test', () => {
        component.ngOnDestroy();
    });

    it('closeModal test', () => {
        component.closeModal();
    });

    it('getDocument test', () => {
        let mockFincialAttachment: Attachment = new Attachment('', '', '');
        component.getDocument(mockFincialAttachment);
    });

});
