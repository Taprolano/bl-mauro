import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FinancialServicesInner} from "../../../../../../../../../../../../data-model/class/Consultation/FinancialServiceInner";
import {FinancialValue} from "../../../../../../../../../../../../data-model/class/FinancialValue";
import {FINANCIAL_SERVICES} from "../../../../../../../../../../../../data-model/enum/EFinancialState";


@Component({
    selector: 'app-financial-insurance-elem',
    templateUrl: './financial-insurance-elem.component.html',
    styleUrls: ['./financial-insurance-elem.component.scss']
})
export class FinancialInsuranceElemComponent implements OnInit {

    /** Dettaglio del singolo servizio finanziario */
    @Input()
    insuranceElem: FinancialServicesInner;

    /** Id del tab corrente */
    @Input()
    id: any;

    /** Id del tab attivo */
    @Input()
    elemIdActive: any;

    @Input()
    financialValueList: FinancialValue[];

    /** Oggetto da prendere in input, aggiornare ed emettere */
    @Input()
    financialValue: FinancialValue;

    /** Emette l'oggetto con ide del tab corrente e il supplyCodeElem */
    @Output()
    finInputEvent: EventEmitter<FinancialValue> = new EventEmitter<FinancialValue>();


    /** @ignore */
    constructor() {
    }

    /** @ignore */
    ngOnInit() {
    }

    /**
     * Recupero al click di un dettaglio il supplyElementCode per "CheckCompatibily"
     */
    loadInsuranceInput() {

        console.warn("elem code ", this.insuranceElem.supplyElementCode);

        for (let elem of this.financialValueList) {
            if (elem.supplyTypeCode === this.insuranceElem.supplyTypeCode) {
                // è presente un oggetto con lo stesso typeCode (padre)
                this.financialValue.code = 210;
            } else {
                this.financialValue.code = 200;
            }

        }

        let tempFV = this.financialValueList.find(elem => {
            return this.insuranceElem.supplyElementCode === elem.supplyElemCode || null;
        });

        if(tempFV) {
            console.warn("tempFv FOUND",tempFV);
            this.financialValue = tempFV;
        } else {

            console.warn("tempFv NOT FOUND",tempFV);
            this.financialValue.idChild = this.id;
            this.financialValue.isAssistence = this.insuranceElem.supplyTypeCode.startsWith(FINANCIAL_SERVICES.ASSISTENZA);
            this.financialValue.supplyElemCode = this.insuranceElem.supplyElementCode;
            this.financialValue.supplyElemDesc = this.insuranceElem.supplyElementDesc;
            this.financialValue.supplyTypeCode = this.insuranceElem.supplyTypeCode;
            this.financialValue.supplyTypeDesc = this.insuranceElem.supplyTypeDesc;
            this.financialValue.clazz = this.insuranceElem.clazz;
        }

        this.finInputEvent.emit(this.financialValue);
    }

    get isSelected(): boolean {
        return this.financialValue.isSelected;
/*
        let test = this.financialValueList.find(elem => {
            return elem.supplyElemCode == this.insuranceElem.supplyElementCode;
        });

        return !!test;
*/
    }

}

