import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AppSettings} from "../../../../../../../../../../../../../../../AppSettings";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-service-plus-confort-input',
    templateUrl: './service-plus-confort-input.component.html',
    styleUrls: ['./service-plus-confort-input.component.scss']
})
export class ServicePlusConfortInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    serviceConfortForm: FormGroup;
    serviceConfortInput: any;
    private submitted = false;
    private isValid: boolean;

    annualDistance;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.submitted = false;
        this.serviceConfortForm = this._formBuilder.group({
            annualDistance: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.annualDistance)) this.annualDistance = this.financialValue.object.annualDistance;
        } else {
            this.annualDistance = 'default';
        }
    }

    get f() {
        return this.serviceConfortForm.controls;
    }

    validateForm(): boolean {
        this.submitted = true;
        if (this._utils.isVoid(this.serviceConfortForm.controls['annualDistance'].value) || this.serviceConfortForm.controls['annualDistance'].value === 'default') {
            this._utils.addCssClassByIds('a-input--error', 'annualDistance');
            this.isValid = false;
        } else {
            this.isValid = true;
        }

        return this.isValid;
    }

    getServiceData(): FinancialValue {
        this.validateForm();
        this.financialValue.code = AppSettings.HTTP_OK;

        if (this.isValid === true) {
            this.serviceConfortInput = {
                annualDistance: this.serviceConfortForm.value.annualDistance,
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.serviceConfortInput;

            this._logger.logInfo('ServicePlusConfortInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = AppSettings.HTTP_ERROR;
            this._logger.logInfo('ServicePlusConfortInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }

    removeError(id: string) {
        this._utils.removeCssClassByIds('a-input--error', id);
    }
}
