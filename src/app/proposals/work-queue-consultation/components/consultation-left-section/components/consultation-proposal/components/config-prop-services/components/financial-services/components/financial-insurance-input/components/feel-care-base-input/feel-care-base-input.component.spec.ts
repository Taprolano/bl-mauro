import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeelCareBaseInputComponent} from './feel-care-base-input.component';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";

describe('FeelCareBaseInputComponent', () => {
    let component: FeelCareBaseInputComponent;
    let fixture: ComponentFixture<FeelCareBaseInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FeelCareBaseInputComponent],
            imports: [
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeelCareBaseInputComponent);
        component = fixture.componentInstance;

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', {common: null}, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.validateForm();
    });

    it('getServiceData  valForm true test', () => {
        spyOn(component, 'validateForm').and.returnValue(true);
        component.getServiceData();
    });

});
