import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigPropServicesComponent } from './config-prop-services.component';
import {RegistryComponent} from "../../../../../../../proposal/registry/registry.component";
import {PreliminaryComponent} from "../../../../../../../proposal/preliminary/preliminary.component";
import {ModelComponent} from "../../../../../../../proposal/product/model/model.component";
import {ProposalWorkflowService} from "../../../../../../../services/proposal-workflow.service";
import {FinancialConfigurationComponent} from "./components/financial-configuration/financial-configuration.component";
import {CommercialComponent} from "../../../../../../../proposal/commercial/commercial.component";
import {ConsultationRightSectionComponent} from "../../../../../consultation-right-section/consultation-right-section.component";
import {ProposalFilterBarComponent} from "../../../../../../../components/proposal-filter-bar/proposal-filter-bar.component";
import {FinancialRateComponent} from "./components/financial-rate/financial-rate.component";
import {DealerListAllComponent} from "../../../../../../../work-queue/dealer-contact/components/dealer-list-all/dealer-list-all.component";
import {CorporateListAllComponent} from "../../../../../../../work-queue/credit-corporate/components/corporate-list-all/corporate-list-all.component";
import {FinalizeComponent} from "../../../../../../../proposal/finalize/finalize.component";
import {CreditRetailComponent} from "../../../../../../../work-queue/credit-retail/credit-retail.component";
import {
    MissingTranslationHandler,
    TranslateCompiler, TranslateLoader, TranslateModule, TranslateParser, TranslateService,
    TranslateStore
} from "@ngx-translate/core";
import {ExposureOutstandingComponent} from "../exposure-outstanding/exposure-outstanding.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommercialThirdPartComponent} from "../../../../../../../proposal/commercial/components/commercial-third-part/commercial-third-part.component";
import {SharedModule} from "../../../../../../../../shared/shared.module";
import {ProposalComponent} from "../../../../../../../proposal/proposal.component";
import {FinancialCampaignComponent} from "./components/financial-campaign/financial-campaign.component";
import {EmptyListComponent} from "../../../../../../../../shared/components/empty-list/empty-list.component";
import {ProductProposalComponent} from "../product-proposal/product-proposal.component";
import {CommercialOptionalComponent} from "../../../../../../../proposal/commercial/components/commercial-optional/commercial-optional.component";
import {CreditCorporateComponent} from "../../../../../../../work-queue/credit-corporate/credit-corporate.component";
import {ConsultationHeaderComponent} from "../../../../../consultation-header/consultation-header.component";
import {PayoutListAllComponent} from "../../../../../../../work-queue/payout/components/payout-list-all/payout-list-all.component";
import {TabMbcposComponent} from "../../../../../../../list/components/tab-mbcpos/tab-mbcpos.component";
import {ProfilesComponent} from "../../../../../../../proposal/registry/profiles/profiles.component";
import {ConsultationProposalComponent} from "../../consultation-proposal.component";
import {FinancialComponent} from "../../../../../../../proposal/financial/financial.component";
import {FrameComponent} from "../../../../../../../proposal/contract/frame/frame.component";
import {ContractsListComponent} from "../../../contracts-list/contracts-list.component";
import {ProposalsRoutingModule} from "../../../../../../../proposals-routing.module";
import {ProposalFooterComponent} from "../../../../../../../components/proposal-footer/proposal-footer.component";
import {HeaderTabsComponent} from "../../../../../header-tabs/header-tabs.component";
import {VersionListComponent} from "../../../../../../../components/version-list/version-list.component";
import {ProposalCommercialComponent} from "../proposal-commercial/proposal-commercial.component";
import {ConsultationLeftSectionComponent} from "../../../../consultation-left-section.component";
import {ProductComponent} from "../../../../../../../proposal/product/product.component";
import {WorkQueueConsultationComponent} from "../../../../../../work-queue-consultation.component";
import {HeaderComponent} from "../../../../../../../../shared/components/header/header.component";
import {FinancialPaymentMethodComponent} from "./components/financial-payment-method/financial-payment-method.component";
import {ContractComponent} from "../../../../../../../proposal/contract/contract.component";
import {ListTestComponent} from "../../../../../../../components/list-test/list-test.component";
import {PayoutComponent} from "../../../../../../../work-queue/payout/payout.component";
import {FinancialServicesComponent} from "./components/financial-services/financial-services.component";
import {ProposalService} from "../../../../../../../../shared/services/proposal.service";
import {ProposalsComponent} from "../../../../../../../proposals.component";
import {ConsultationDocumentsComponent} from "../../../../../consultation-right-section/components/consultation-documents/consultation-documents.component";
import {TabVdzComponent} from "../../../../../../../list/components/tab-vdz/tab-vdz.component";
import {CreditListAllComponent} from "../../../../../../../work-queue/credit-retail/components/credit-list-all/credit-list-all.component";
import {WorkQueueComponent} from "../../../../../../../work-queue/work-queue.component";
import {CommonModule} from "@angular/common";
import {ProductSummaryComponent} from "../../../../../../../components/proposal-summary/proposal-summary.component";
import {ConsultationNotesComponent} from "../../../../../consultation-right-section/components/consultation-notes/consultation-notes.component";
import {PrivacyComponent} from "../../../../../../../proposal/preliminary/privacy/privacy.component";
import {ConsultationDataBanksComponent} from "../../../../../consultation-right-section/components/consultation-data-banks/consultation-data-banks.component";
import {StepperComponent} from "../../../../../../../components/stepper/stepper.component";
import {GoTopNewProposalComponent} from "../../../../../../../components/go-top-new-proposal/go-top-new-proposal.component";
import {AccordionListComponent} from "../../../../../../../components/accordion-list/accordion-list.component";
import {PrintComponent} from "../../../../../../../proposal/contract/print/print.component";
import {InfiniteScrollComponent} from "../../../../../../../components/infinite-scroll/infinite-scroll.component";
import {DatepickerPopupComponent} from "../../../../../../../../shared/components/datepicker-popup/datepicker-popup.component";
import {ListComponent} from "../../../../../../../list/list.component";
import {PetitionerProposalComponent} from "../petitioner-proposal/petitioner-proposal.component";
import {RiskSharingComponent} from "../risk-sharing/risk-sharing.component";
import {customCurrencyMaskConfig, ProposalsModule} from "../../../../../../../proposals.module";
import {ConsultationSubjectsListComponent} from "../../../consultation-subjects-list/consultation-subjects-list.component";
import {A2Edatetimepicker} from "ng2-eonasdan-datetimepicker";
import {DealerContactComponent} from "../../../../../../../work-queue/dealer-contact/dealer-contact.component";
import {NgxCurrencyModule} from "ngx-currency";
import {DocumentsComponent} from "../../../../../../../proposal/registry/documents/documents.component";
import {FinancialDetailsComponent} from "./components/financial-details/financial-details.component";
import {SearchFilterComponent} from "../../../../../../../components/search-filter/search-filter.component";
import {VersionComponent} from "../../../../../../../proposal/product/version/version.component";
import {PaymentsComponent} from "../../../../../../../proposal/finalize/payments/payments.component";
import {ProposalSaleDetailsComponent} from "../proposal-sale-details/proposal-sale-details.component";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {AuthGuard} from "../../../../../../../../shared";
import {NgxSpinnerModule} from "ngx-spinner";
import {MbzDashboardModule} from "../../../../../../../../mbz-dashboard/mbz-dashboard.module";
import {createTranslateLoader} from "../../../../../../../../app.module";
import {DirectAccessGuard} from "../../../../../../../../shared/guard/direct-access/direct-access.guard";
import {environment} from "../../../../../../../../../environments/environment";
import {AppRoutingModule} from "../../../../../../../../app-routing.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AppComponent} from "../../../../../../../../app.component";

xdescribe('ConfigPropServicesComponent', () => {
  let component: ConfigPropServicesComponent;
  let fixture: ComponentFixture<ConfigPropServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
            CommonModule,
            BrowserModule,
            BrowserAnimationsModule,
            SharedModule,
            TranslateModule,
            HttpClientModule,
            MbzDashboardModule,
            ProposalsModule,
            NgxSpinnerModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: createTranslateLoader,
                    deps: [HttpClient]
                }
            }),
            AppRoutingModule,
            ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        ],
        declarations: [AppComponent],
        providers: [AuthGuard, DirectAccessGuard]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigPropServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
