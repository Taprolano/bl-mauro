import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {IRtiInput} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {RtiCoverage} from "../../../../../../../../../../../../../../data-model/class/RtiCoverage";
import {FireTheftCoverage} from "../../../../../../../../../../../../../../data-model/class/FireTheftCoverage";
import {AppSettings} from "../../../../../../../../../../../../../../../AppSettings";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";



@Component({
    selector: 'app-feel-new-input',
    templateUrl: './feel-new-input.component.html',
    styleUrls: ['./feel-new-input.component.scss']
})
export class FeelNewInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelNewForm: FormGroup;
    feelNewInput: IRtiInput;
    private submitted = false;
    isValid: boolean;

    private rti: RtiCoverage;
    private fi: FireTheftCoverage;
    riepilogoTotale: number;

    readonly defaultFireCoverage = AppSettings.DEFAULT_FIRE_COVERAGE;
    readonly defaultRtiCoverage = AppSettings.DEFAULT_RTI_COVERAGE;


    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.submitted = false;

        this.feelNewForm = this._formBuilder.group({
            polizFurtoIncendio: [],
            furtoIncendio: [],
            satellitare: [],
            provincia: [],
            compagniaDiAssicurzionFI: [],
            durataVNFurtoIncendio: [!!this.service ? this.service.fireTheftCoverage[0].code : null],
            durataFeelNew: [!!this.service ? this.service.rtiCoverage[0].code : null],
            riepilogoTotale: [this.riepilogoTotale]
        });


        if (this.financialValue.object != undefined) {
            if (!this._utils.isVoid(this.financialValue.object.durataVNFurtoIncendio)) this.feelNewForm.controls['durataVNFurtoIncendio'].setValue(this.financialValue.object.durataVNFurtoIncendio.code);
            if (!this._utils.isVoid(this.financialValue.object.durataFeelNew)) this.feelNewForm.controls['durataFeelNew'].setValue(this.financialValue.object.durataFeelNew.code);
            if (!this._utils.isVoid(this.financialValue.object.polizFurtoIncendio)) this.feelNewForm.controls['polizFurtoIncendio'].setValue(this.financialValue.object.polizFurtoIncendio);
            if (!this._utils.isVoid(this.financialValue.object.furtoIncendio)) this.feelNewForm.controls['furtoIncendio'].setValue(this.financialValue.object.furtoIncendio);
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelNewForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelNewForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.compagniaDiAssicurzionFI)) this.feelNewForm.controls['compagniaDiAssicurzionFI'].setValue(this.financialValue.object.compagniaDiAssicurzionFI);
            if (!this._utils.isVoid(this.financialValue.object.riepilogoTotale)) this.feelNewForm.controls['riepilogoTotale'].setValue(this.financialValue.object.riepilogoTotale);
        }

        this.updateriepilogoTotale();

    }

    get f() {
        return this.feelNewForm.controls;
    }

    validateForm() {
        this.submitted = true;
        this.isValid = true;

        if (this._utils.isVoid(this.feelNewForm.controls['provincia'].value)) {
            this._utils.addCssClassByIds('a-input--error', 'provincia');
            this.isValid = false;
        }

        if (this._utils.isVoid(this.feelNewForm.controls['compagniaDiAssicurzionFI'].value) && this.feelNewForm.value.polizFurtoIncendio) {
            this._utils.addCssClassByIds('a-input--error', 'compagniaDiAssicurzionFI');
            this.isValid = false;
        }

        if (this.feelNewForm.controls['durataFeelNew'].value === this.defaultRtiCoverage) {
            this._utils.addCssClassByIds('a-input--error', 'durataFeelNew');
            this.isValid = false;
        }

    }


    getServiceData(): FinancialValue {
        this.validateForm();
        this.financialValue.code = 200;

        this.getFi();
        this.getRti();

        if (this.isValid) {
            this.feelNewInput = {
                durataVNFurtoIncendio: this.fi,
                polizFurtoIncendio: this.feelNewForm.value.polizFurtoIncendio,
                durataFeelNew: this.rti,
                satellitare: this.feelNewForm.value.satellitare,
                provincia: this.feelNewForm.value.provincia,
                riepilogoTotale: this.riepilogoTotale,
                compagniaDiAssicurzionFI: this.feelNewForm.value.compagniaDiAssicurzionFI,
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelNewInput;


            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }

    getRti() {
        this.rti = this.service.rtiCoverage.find(elem => {
            return elem.code == this.feelNewForm.value.durataFeelNew;
        });
    }

    getFi() {
        this.fi = this.service.fireTheftCoverage.find(elem => {
            return elem.code == this.feelNewForm.value.durataVNFurtoIncendio;
        });
    }

    updateriepilogoTotale() {
        this.getRti();
        this.getFi();
        this.riepilogoTotale = +this.fi.coverageLength + +this.rti.coverageLength;
    }

    resetFieldExtra() {
        if (this.feelNewForm.get('polizFurtoIncendio').value === false) {
            this.feelNewForm.get('durataFeelNew').patchValue(this.service.rtiCoverage[0]);
            this.feelNewForm.get('compagniaDiAssicurzionFI').patchValue(null);
        }
        this.updateriepilogoTotale();
    }

    removeError(id: string) {
        this._utils.removeCssClassByIds('a-input--error', id);
    }
}

