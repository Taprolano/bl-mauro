import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";
import {IPremiumNordEst} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";

@Component({
    selector: 'app-feel-star-premium-nord-est-input',
    templateUrl: './feel-star-premium-nord-est-input.component.html',
    styleUrls: ['./feel-star-premium-nord-est-input.component.scss']
})
export class FeelStarPremiumNordEstInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelStarNordEstForm: FormGroup;
    submitted = false;
    feelStarNordEstInput: IPremiumNordEst;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.feelStarNordEstForm = this._formBuilder.group({
            provincia: [],
            satellitare: [],
            privacy: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelStarNordEstForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelStarNordEstForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.privacy)) this.feelStarNordEstForm.controls['privacy'].setValue(this.financialValue.object.privacy);
        }
    }

    valideteForm(): boolean {
        this.submitted = true;
        return this.feelStarNordEstForm.valid;
    }


    getServiceData(): FinancialValue {

        if (this.valideteForm) {
            // this.financialValue.code = 200;

            this.feelStarNordEstInput = {
                commonFeelStar: {
                    provincia: this.feelStarNordEstForm.value.provincia,
                    satellitare: this.feelStarNordEstForm.value.satellitare,
                    privacy: this.feelStarNordEstForm.value.privacy
                },
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelStarNordEstInput;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}
