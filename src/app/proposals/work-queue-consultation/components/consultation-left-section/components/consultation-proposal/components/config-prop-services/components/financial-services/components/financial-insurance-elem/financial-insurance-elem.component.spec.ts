import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FinancialInsuranceElemComponent} from './financial-insurance-elem.component';
import {SharedPipesModule} from "../../../../../../../../../../../../shared";
import {FinancialValue} from "../../../../../../../../../../../../data-model/class/FinancialValue";
import {FinancialServicesInner} from "../../../../../../../../../../../../data-model/class/Consultation/FinancialServiceInner";

describe('FinancialInsuranceElemComponent', () => {
    let component: FinancialInsuranceElemComponent;
    let fixture: ComponentFixture<FinancialInsuranceElemComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FinancialInsuranceElemComponent],
            imports: [SharedPipesModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FinancialInsuranceElemComponent);
        component = fixture.componentInstance;

        let mockFinancialValue: FinancialValue = new FinancialValue(' ', ' ', 0, ' ', null, ' ', ' ', ' ', ' ', ' ');
        let mockFinValList: FinancialValue[] = [];
        mockFinValList.push(mockFinancialValue);
        component.financialValueList = mockFinValList;
        component.financialValue = mockFinancialValue;

        let mockInsElem: FinancialServicesInner = new FinancialServicesInner(' ', ' ', ' ', ' ', ' ', ' ', ' ', false, false, false, false, ' ', ' ', ' ', ' ', false);
        component.insuranceElem = mockInsElem;

        component.id = 0;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('loadInsuranceInput supplyTypeCode == test', () => {
        spyOn(component.finInputEvent, 'emit').and.returnValue(false);
        component.loadInsuranceInput();
    });

    it('loadInsuranceInput supplyTypeCode != test', () => {
        spyOn(component.finInputEvent, 'emit').and.returnValue(false);
        component.insuranceElem.supplyTypeCode = 'test';
        component.loadInsuranceInput();
    });

    it('checkIfSelected true test', () => {
        component.checkIfSelected();
    });

    it('checkIfSelected false test', () => {
        component.insuranceElem.supplyElementCode = 'test';
        component.checkIfSelected();
    });

});
