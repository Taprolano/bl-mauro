import {Component, OnInit, Input} from '@angular/core';
import {ConsultationProposalProductQueue} from '../../../../../../../../data-model/class/Consultation/ConsultationProposalProductQueue';
import { BeWorkQueue } from '../../../../../../../services/be-work-queue.service';
import { PersistenceService } from '../../../../../../../../shared/services/persistence.service';
import { ProposalConsultationRequestParameters } from '../../../../../../../../data-model/class/ProposalConsultationRequestParameters';
import { RequestManagerConstant } from '../../../../../../../requestManagerConstant';
import { RequestManagerService, REQUEST_PRIORITY } from 'src/app/shared/services/request-manager.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { AppSettings } from '../../../../../../../../../AppSettings';
import { NOTIFICATION_TYPE, NotificationMessage } from '../../../../../../../../shared/components/notification/NotificationMessage';
import { NotificationService } from 'src/app/shared/components/notification/notification.service';
import { LoggerService } from '../../../../../../../../shared/services/logger.service';
import { UtilsService } from '../../../../../../../../shared/services/utils.service';

@Component({
    selector: 'app-product-proposal',
    templateUrl: './product-proposal.component.html',
    styleUrls: ['./product-proposal.component.scss']
})
export class ProductProposalComponent implements OnInit {

    proposalProduct: ConsultationProposalProductQueue;

     /** @ignore */
     destroy$: Subject<boolean> = new Subject<boolean>();

    @Input() isEditing: boolean;

    showContent: boolean;

    constructor(private _workQueueService: BeWorkQueue,
                private _persistence: PersistenceService,
                private _requestManagerService: RequestManagerService,
                private _notificationService: NotificationService,
                private _logger: LoggerService,
                private _utils: UtilsService) {
    }

    ngOnInit() {
        this.showContent = false;
        const tabProduct = 'proposta';
        const objProposalSelect = this._persistence.get('SELECT_PROPOSAL');
        const params = new ProposalConsultationRequestParameters(objProposalSelect.idProposal, objProposalSelect.processInstanceID, tabProduct);
        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_PROD, REQUEST_PRIORITY.HIGH);
        this._workQueueService.getConsultazioneProposal(params).pipe(takeUntil(this.destroy$))
        .subscribe( response => {
                this.proposalProduct = response.object;
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_PROD, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            }, respError => {
                this._notificationService.sendNotification(new NotificationMessage(`Non è stato possibile recuperare le informazioni sull'accordion prodotto`, NOTIFICATION_TYPE.ERROR));
                this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'resp error', respError);
                const error = this._utils.getErrorResponse(respError);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_PROD, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
            }
        );
    }

}
