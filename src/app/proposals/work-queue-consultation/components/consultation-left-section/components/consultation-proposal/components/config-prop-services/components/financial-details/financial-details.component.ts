import {Component, Input, OnInit} from '@angular/core';
import {Subject} from "rxjs/index";
import {AbstractModalService} from "../../../../../../../../../../shared/components/abstract-modal/abstract-modal.service";
import { ConsultationFinancialQueue } from '../../../../../../../../../../data-model/class/Consultation/ConsultationFinancialQueue';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ValueListEnhanced } from '../../../../../../../../../../data-model/class/ListValuesEnhanced';
import { LoggerService } from '../../../../../../../../../../shared/services/logger.service';

@Component({
    selector: 'app-financial-details',
    templateUrl: './financial-details.component.html',
    styleUrls: ['./financial-details.component.scss']
})
export class FinancialDetailsComponent implements OnInit {
    showContent: boolean;
    @Input() isEditing: boolean;

    financialQueue: ConsultationFinancialQueue = new ConsultationFinancialQueue();

    @Input('selectedFinancialQueue')
    set selectedFinancialQueue(value: ConsultationFinancialQueue) {
        if (value){
            this.financialQueue = value;
        }
    }

    detailDataForm: FormGroup;

    destroy$: Subject<boolean> = new Subject<boolean>();

/**
     * Payment rate select values
     */
    public paymentRateValuesList: ValueListEnhanced[];

    /**
     * Payment advance select values
     */
    public paymentAdvanceValuesList: ValueListEnhanced[];

    /**
     * Advance select values
     */
    public vatValuesList: ValueListEnhanced[];

    /**
     * Mortgage select values
     */
    public mortgageValuesList: ValueListEnhanced[];

    /**
     * Payment methods Rate selected
     */
    public advanceSelected: ValueListEnhanced;

    /**
     * Payment methods Advance selected
     */
    public rateSelected: ValueListEnhanced;

    /**
     * Vat selected
     */
    public vatSelected: ValueListEnhanced;

    /**
     * Mortgage selected
     */
    public mortgageSelected: ValueListEnhanced;


    constructor(private _modalService: AbstractModalService,
        private _formBuilder: FormBuilder,
        private _logger: LoggerService) {
    }

    ngOnInit() {
        this.showContent = false;
        this.createForm();
    }

    showWIP() {
        this._modalService.openUnavailableContent(this.destroy$);
    }

    public createForm() {

        this.detailDataForm = this._formBuilder.group({
            dealerContribution: this._formBuilder.control(0),
            vat: this._formBuilder.control(null),
            mortgage: this._formBuilder.control(null),
            invoiceCB: this._formBuilder.control(false),
            commercialDerogationCB: this._formBuilder.control(false),
            contributeDerogatioCB: this._formBuilder.control(false),
            repurchaseAgreementAcceptedCB: this._formBuilder.control(false),
            ratePaymentMethod: this._formBuilder.control(null),
            advacePaymentMethod: this._formBuilder.control(null),
            voucherCode: this._formBuilder.control(null),
            contributoRetention: this._formBuilder.control(null),
            k2k: this._formBuilder.control(null)
        });

        this.detailDataForm.get('contributoRetention').disable();
//        if (!!this.vatSelected) this.detailDataForm.get('vat').patchValue(this.vatSelected);
//        if (!!this.mortgageSelected) this.detailDataForm.get('mortgage').patchValue(this.mortgageSelected);
//        if (!!this.rateSelected) this.detailDataForm.get('ratePaymentMethod').patchValue(this.rateSelected);
//        if (!!this.advanceSelected) this.detailDataForm.get('advacePaymentMethod').patchValue(this.advanceSelected);


    }

    update() {

        let detail = {
            dealerContribution: this.detailDataForm.get('dealerContribution').value,
            vat: this.ivaValue,
            mortgage: this.detailDataForm.get('mortgage').value.key,
            invoice: this.detailDataForm.get('invoiceCB').value,
            commercialDerogation: this.detailDataForm.get('commercialDerogationCB').value,
            contributeDerogatio: this.detailDataForm.get('contributeDerogatioCB').value,
            repurchaseAgreementAccepted: this.detailDataForm.get('repurchaseAgreementAcceptedCB').value,
            ratePaymentMethod: this.detailDataForm.get('ratePaymentMethod').value.key,
            advacePaymentMethod: this.detailDataForm.get('advacePaymentMethod').value.key,
            voucherCode: this.detailDataForm.get('voucherCode').value,
            // showVoucher : this.showVoucher,
            contributoRetention :this.detailDataForm.get('contributoRetention').value,
            k2k: this.detailDataForm.get('k2k').value,
            vatCode: this.detailDataForm.get('vat').value.key,
    };

        this._logger.logWarn(this.constructor.name, "update", detail);

        // this.changeTrigger.next(detail);
    }

    get advancePaymentMethodPrice() {
        if (!!this.detailDataForm.get('advacePaymentMethod').value && !!this.detailDataForm.get('advacePaymentMethod').value.additionalProperties) {
            for (let prop of this.detailDataForm.get('advacePaymentMethod').value.additionalProperties) {
                if (prop.key === 'cost') return prop.value;
            }
        }
        return 0;
    }

    get ivaValue() {
        if (!!this.detailDataForm.get('vat').value && !!this.detailDataForm.get('vat').value.additionalProperties) {
            for (let prop of this.detailDataForm.get('vat').value.additionalProperties) {
                if (prop.key === 'PERC') return prop.value;
            }
        }
    }

    get ratePaymentMethodPrice() {
        if (!!this.detailDataForm.get('ratePaymentMethod').value && !!this.detailDataForm.get('ratePaymentMethod').value.additionalProperties) {
            for (let prop of this.detailDataForm.get('ratePaymentMethod').value.additionalProperties) {
                if (prop.key === 'cost') return prop.value;
            }
        }
        return 0;
    }
}
