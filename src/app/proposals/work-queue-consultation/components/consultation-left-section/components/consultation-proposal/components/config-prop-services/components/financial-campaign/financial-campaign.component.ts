import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CampaignDetail, FinancialCampaign} from '../../../../../../../../../../data-model/class/FinancialCampaign';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AppSettings} from '../../../../../../../../../../../AppSettings';
import {LoggerService} from '../../../../../../../../../../shared/services/logger.service';
import {UtilsService} from '../../../../../../../../../../shared/services/utils.service';
import {RequestManagerService} from '../../../../../../../../../../shared/services/request-manager.service';
import {IProposal} from '../../../../../../../../../../data-model/interface/IProposal';
import {FinancialCampaignReqParams} from '../../../../../../../../../../data-model/class/FinancialCampaignReqParams';
import {BeFinancialService} from '../../../../../../../../../../shared/services/be-financial.service';

@Component({
    selector: 'app-financial-campaign',
    templateUrl: './financial-campaign.component.html',
    styleUrls: ['./financial-campaign.component.scss']
})
export class FinancialCampaignComponent implements OnInit {

    showContent: boolean;

    /**
     * EventEmitter to send financial selectedCampaign selected to father
     */
    @Output() campaignEmitter: EventEmitter<FinancialCampaign>;

    /** Choosed proposal */
    proposal: IProposal;
    /*@Input()
    proposal: IProposal;*/
    @Input('selectedProposal')
    set selectedProposal(value: IProposal) {
        if (value) {
            this.proposal = value;
            // this.getCampaignsListFromBE();
        }
    }

    /**
     * Scrollbar options
     * @clazz {{axis: string; theme: string; setHeight: string}}
     */
    scrollbarOptions = {axis: 'y', theme: AppSettings.scrollStyle, setHeight: '100%'};

    /** Boolean that tells if the selectedCampaign accordion is open or not */
    campaignAccordionOpen = false;

    /**
     * @ignore
     */
    destroy$: Subject<boolean> = new Subject<boolean>();

    /** Enable second selectedCampaign column
     * @clazz boolean
     **/
    public secondColumnEnable: boolean;

    /** Campaign choosed by user
     * @clazz boolean
     **/
    public campaignChoosed: boolean;

    /** Enable third selectedCampaign column
     * @clazz boolean
     **/
    public thirdColumnEnable: boolean;

    /** Enable cancel and confirm button
     * @clazz boolean
     **/
    public buttonsEnabled: boolean;

    /** Campaign title
     * @clazz string */
    public campaignTitle: string;

    /**
     * Index of selected financialCampaign.
     */
    public indexSelectedFinancialCampaign: number;

    /**
     * Index of selected Campaign.
     */
    public indexSelectedCampaign: number;

    /**
     * Financial Campaigns list
     * @clazz Observable<BeResponse<FinancialCampaign[]>>
     */
    public financialCampaignList: FinancialCampaign[];

    /**
     * Financial Campaign selected by user
     * @clazz FinancialCampaign
     */
    public selectedFinancialCampaign: FinancialCampaign;

    /**
     * Campaign Detail selected by user
     * @clazz CampaignDetail
     */
    public selectedCampaignDetail: CampaignDetail;

    /**
     * Financial selectedCampaign selected clone to send to parent
     */
    public campaignSelectedClone: FinancialCampaign;

    /**
     * Default Campaign Detail
     */
    public defaultCampaignDetails: CampaignDetail;

    /**
     * Default Financial Campaign
     */
    public defaultFinancialCampaign: FinancialCampaign;

    /**
     * Default Financial Campaign index
     */
    public defFinCampaignIndex = -1;

    /**
     * Default Campaign Details Index
     */
    public defDetCampaignIndex = -1;

    /**
     * Default Campaign is setted or not
     */
    public defaultCampFind: boolean;

    /**
     *Financial selectedCampaign component constructor
     * @param _financialService
     * @param _logger
     * @param _utils
     * @param _requestManagerService
     */
    constructor(private _financialService: BeFinancialService,
                private _logger: LoggerService,
                private _utils: UtilsService,
                private _requestManagerService: RequestManagerService) {

        this.financialCampaignList = [];

        this.campaignTitle = '';

        this.campaignAccordionOpen = false;
        this.campaignChoosed = false;
        this.defaultCampFind = false;

        this.buttonsEnabled = false;

        this.indexSelectedFinancialCampaign = -1;
        this.indexSelectedCampaign = -1;

        this.secondColumnEnable = false;
        this.thirdColumnEnable = false;

        this.defaultCampaignDetails = new CampaignDetail();
        this.defaultFinancialCampaign = new FinancialCampaign('', '', false, '', null, false);
        this.defFinCampaignIndex = -1;
        this.defDetCampaignIndex = -1;

        this.campaignEmitter = new EventEmitter<FinancialCampaign>();
    }

    /**
     * On Init method financial selectedCampaign component. When the component is loaded, call financial/campaigns services
     */
    ngOnInit() {
         this.getCampaignsListFromBE();
    }

    /**
     * Reset accordion content after click on Cancel
     */
    public resetAccordion(): void {

        /** If financial selectedCampaign not choosed by user */
        if (!this.campaignChoosed) {
            this.financialCampaignList = [];
            this.indexSelectedFinancialCampaign = -1;
            this.indexSelectedCampaign = -1;
            this.buttonsEnabled = false;
            this.secondColumnEnable = false;
            this.thirdColumnEnable = false;
            this.getCampaignsListFromBE();
        } else if (!this.campaignChoosed && !this.defaultCampFind) {
            this.setDefaultCampaign();
        } else if (this.campaignChoosed && !this.defaultCampFind) {
            this.indexSelectedFinancialCampaign = this.defFinCampaignIndex;
            this.indexSelectedCampaign = this.defDetCampaignIndex;
            this.setDefaultCampaign();
        } else {
            this.indexSelectedFinancialCampaign = this.defFinCampaignIndex;
            this.indexSelectedCampaign = this.defDetCampaignIndex;
            this.selectedFinancialCampaign = this.financialCampaignList[this.indexSelectedFinancialCampaign];
            this.selectedCampaignDetail = this.financialCampaignList[this.indexSelectedFinancialCampaign]
                .campaignsDetailList[this.indexSelectedCampaign];
            this.buttonsEnabled = true;
            this.secondColumnEnable = true;
            this.thirdColumnEnable = true;
        }
    }

    /**
     * Load second column content
     * @param index
     * @param campaign
     */
    public loadSecondColumn(index: number, campaign: FinancialCampaign) {
        this.thirdColumnEnable = false;
        this.buttonsEnabled = false;
        this.indexSelectedCampaign = -1;
        this.indexSelectedFinancialCampaign = index;
        this.secondColumnEnable = true;
        this.campaignTitle = campaign.fpDescription;
        this.selectedFinancialCampaign = campaign;
        console.log('SET CAMP [loadSecondColumn]: ', campaign);
    }

    /**
     * Load third column content and enable buttons
     * @param index
     * @param campDetail
     */
    public loadThirdColumn(index: number, campDetail: CampaignDetail) {
        this.thirdColumnEnable = true;
        this.buttonsEnabled = true;
        this.indexSelectedCampaign = index;
        this.selectedCampaignDetail = campDetail;
    }

    /**
     * Set accordion open boolean
     * @param value
     */
    public setCampaignAccordionOpen(value: boolean): void {
        this.campaignAccordionOpen = value;
    }

    /**
     * Get FinancialCampaign list from backend
     */
    public getCampaignsListFromBE(): void {

        /** Check if data already loaded */
        if (this.financialCampaignList && this.financialCampaignList.length > 0) {
            return;
        }

        /** Mock params */
        const params = new FinancialCampaignReqParams(
         '90261112030',
         'MBF',
         '',
         '',
         false);

        /** Request params get from proposal */
        // let params = new FinancialCampaignReqParams(
        //     this.proposal.product.version.productCode,
        //     this.proposal.product.model.brandId,
        //     this.proposal.product.model.assetClassCode,
        //     this.proposal.product.model.assetType,
        //     false);

        this._financialService.getCampaignList(params, true).pipe(takeUntil(this.destroy$)).subscribe(
            response => {
                this._logger.logInfo('FinancialCampaignComponent', 'loadFinancialCampaignsList', 'load response ', response);
                this.financialCampaignList = response.object;

                /**Definisce le campagne strategiche per aggiugere il flag tattica */
                this.setStrategicCampaigns();

                /** Set default financial selectedCampaign */
                this.setDefaultCampaign();

                setTimeout(() => {
                    // this._requestManagerService.handleRequest(RequestManagerConstant.GET_DEALERS, response.code);
                }, AppSettings.TIMEOUT_SPINNER);

            }, resp => {
                this._logger.logInfo('FinancialCampaignComponent', 'loadFinancialCampaignsList', 'load error ', resp);
                // const error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    // this._requestManagerService.handleRequest(RequestManagerConstant.GET_DEALERS, error.code, error.message);
                }, AppSettings.TIMEOUT_SPINNER);
            });


    }

    /**
     * This method set the default financial selectedCampaign if exists
     */
    public setDefaultCampaign() {

        this.defDetCampaignIndex = -1;
        this.defFinCampaignIndex = -1;

        for (let financialCampaignIndex = 0; financialCampaignIndex < this.financialCampaignList.length; financialCampaignIndex++) {

            for (let detailsCampaignIndex = 0;
                 detailsCampaignIndex < this.financialCampaignList[financialCampaignIndex].campaignsDetailList.length;
                 detailsCampaignIndex++) {

                const detCamp = this.financialCampaignList[financialCampaignIndex].campaignsDetailList[detailsCampaignIndex];

                if (detCamp.defaultValue) {
                    this.defaultCampaignDetails = detCamp;
                    this.defDetCampaignIndex = detailsCampaignIndex;
                    this.defaultCampFind = true;
                    break;
                }
            }

            if (this.defaultCampFind) {
                this.defaultFinancialCampaign = this.financialCampaignList[financialCampaignIndex];
                this.defFinCampaignIndex = financialCampaignIndex;
                break;
            }
        }

        if (this.defaultCampFind) {
            this.indexSelectedFinancialCampaign = this.defFinCampaignIndex;
            this.indexSelectedCampaign = this.defDetCampaignIndex;

            this.loadSecondColumn(this.defFinCampaignIndex, this.defaultFinancialCampaign);
            this.loadThirdColumn(this.defDetCampaignIndex, this.defaultCampaignDetails);
        } else {
            this.secondColumnEnable = false;
            this.thirdColumnEnable = false;
            this.buttonsEnabled = false;
        }

        this.selectedFinancialCampaign = this.defaultFinancialCampaign;
        this.sendCampaign();

    }

    /**
     * Send financial selectedCampaign selected to parent with EventEmitter
     */
    public sendCampaign(): void {

        this.campaignSelectedClone = new FinancialCampaign(
            this.selectedFinancialCampaign.fpCode,
            this.selectedFinancialCampaign.fpDescription,
            this.selectedFinancialCampaign.isStrategic,
            this.selectedFinancialCampaign.urlPdf,
            [],
            !!this.selectedCampaignDetail ? this.selectedCampaignDetail.showVoucher : false,
            !this.selectedCampaignDetail.hasOwnProperty('code')
        );
        console.log('SET CAMP [sendCampaign]: ', this.selectedFinancialCampaign);
        this.campaignChoosed = true;
        this.campaignSelectedClone.campaignsDetailList.push(this.selectedCampaignDetail);
        this.campaignEmitter.emit(this.campaignSelectedClone);
        console.log('SET CAMP [sendCampaign CLONE]: ', this.campaignSelectedClone);
        console.log('SET CAMP [sendCampaign DETAIL]: ', this.selectedCampaignDetail);

    }

    /**
     * Definisce le campagne che devono avere il flag tattica
     */
    public setStrategicCampaigns(): void {
        for (const financialCampaign of this.financialCampaignList) {
            for (const financialDetail of financialCampaign.campaignsDetailList) {
                if (financialDetail.tattica) {
                    financialCampaign.isStrategic = true;
                    break;
                }
            }
        }
    }

    /**
     * Get pdf file from BE and download start
     * */
    public downloadPdf() {
        this.selectedCampaignDetail.attachment.contentType = 'test-contType';
        this.selectedCampaignDetail.attachment.url = 'test/url';
        this.selectedCampaignDetail.attachment.name = 'test-name';
        this._financialService.getPdfFile(this.selectedCampaignDetail.attachment);
    }
}

