import {Component, Input, OnInit} from '@angular/core';
import {ConsultationCommercialQueue} from '../../../../../../../../data-model/class/Consultation/ConsultationCommercialQueue';
import {LoggerService} from '../../../../../../../../shared/services/logger.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../../../../../../../../shared/services/utils.service';
import {IThirdPartOptional} from '../../../../../../../../data-model/interface/IThirdPartOptional';
import {IBrand} from '../../../../../../../../data-model/interface/IBrand';
import {ProposalsConstant} from '../../../../../../../proposalsConstant';
import {ICommercial} from '../../../../../../../../data-model/interface/ICommercial';
import {OptionalElement} from '../../../../../../../../data-model/class/AssetOptionals';
import {ISupplier} from '../../../../../../../../data-model/interface/ISupplier';

const __months =
    ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno',
        'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
const __yearRange = 10;

@Component({
    selector: 'app-proposal-commercial',
    templateUrl: './proposal-commercial.component.html',
    styleUrls: ['./proposal-commercial.component.scss']
})
export class ProposalCommercialComponent implements OnInit {

    isOtherBrand = false;
    isNew = true;

    /**@ignore per template*/
    readonly INPUT_SIZES = ProposalsConstant.MAX_INPUT_SIZES;


    showContent: boolean;

    selectedOptionalList: OptionalElement[] = [{
        code: '1',
        description: 'alfredo compreso nel pacchetto',
        netCost: 12,
        netCostVat: 15,
        discount: false,
        premium: true,
        premiumAdjustment: 1,
        selected: true
    }];

    thirdPartList: IThirdPartOptional[] = [{
        optional: {
            code: '1',
            description: 'alfredo compreso nel pacchetto',
            price: 123.00,
            priceWithIVA: 150.00,
            discount: false,
            selected: true
        },
        supplier: {
            label: 'giacomo',
            code: '123'
        },
        iva: {
            label: 'IVA Venditore',
            value: 0.1
        }
    }, {
        optional: {
            code: '1',
            description: 'alfredo compreso nel pacchetto',
            price: 123.00,
            priceWithIVA: 150.00,
            discount: false,
            selected: true
        },
        supplier: {
            label: 'giacomo',
            code: '123'
        },
        iva: {
            label: 'IVA Venditore',
            value: 0.1
        }
    }];

    commercialDataForm: FormGroup;
    secondhandProductForm: FormGroup;


    /**
     * Lista dei fornitori disponibili
     * @clazz {ISupplier[]}
     */
    suppliersList: ISupplier[] = [];

    /** Prezzo di un listino precedente (di data selezionata) per la versione */
    priceFromPriceList: number;

    isVisibleDiscount = false;
    /**
     * Valore numerico dello sconto applicato alla proposta (applicato alla fornitura)
     * @clazz {number}
     */
    discount = 0;
    /** Rapporto tra sconto complessivo e prezzo senza sconto */
    discountPercent = '';

    /** @ignore */
    secondHandAccordionOpen = true;
    /** @ignore */
    secondHandCondition = false;

    /** @ignore */
    years: string[] = [];
    /** @ignore */
    months: string[] = [];

    private _commQueue: ConsultationCommercialQueue;
    @Input() set commQueue(value: any) {
        this._commQueue = value;
        if (!this._utils.isVoid(value)) {
            this._logger.logInfo(this.constructor.name, 'createForm', '_commQueue: ', this._commQueue);
            this._logger.logInfo(this.constructor.name, 'createForm', 'value: ', value);
        }
    }

    private _isEditing: boolean;
    @Input() set isEditing(value: boolean) {
        this._isEditing = value;
        if (this.commercialDataForm !== undefined) {
            if (!value) {
                this.commercialDataForm.disable();
            } else {
                this.commercialDataForm.enable();
            }
        }
    }

    get isEditing(): boolean {
        return this._isEditing;
    }

    get ivaValue(): number {
        return 10;
        // return this.commercialDataForm.get('iva').value;
    }

    constructor(private _logger: LoggerService,
                private _formBuilder: FormBuilder,
                private _utils: UtilsService) {
        this.createForms();
    }

    ngOnInit() {
        this.showContent = false;
        this.loadYears();
        this.loadMonths();
    }


    /**
     * Form builder per creare i campi di modifica e visualizzazione dati della form con l'oggetto ConsultationCommercialQueue
     */
    // createForm(consCommQueue: ConsultationCommercialQueue = null) {
    //     this.commercialDataForm = this._formBuilder.group({
    //         ipt: this._formBuilder.control({value: !!consCommQueue ? consCommQueue.ipt : 0, disabled: !this.isEditing}),
    //         mss: this._formBuilder.control(!!consCommQueue ? consCommQueue.mss : null),
    //         netCost: this._formBuilder.control(!!consCommQueue ? consCommQueue.netCost : null),
    //         fornitoreAsset: this._formBuilder.control(!!consCommQueue ? consCommQueue.fornitoreAsset : null),
    //         vatCode: this._formBuilder.control(!!consCommQueue ? consCommQueue.vatCode : null),
    //         chassisNumber: this._formBuilder.control(!!consCommQueue ? consCommQueue.chassisNumber : null),
    //         plate: this._formBuilder.control(!!consCommQueue ? consCommQueue.plate : null),
    //         registration: this._formBuilder.control(!!consCommQueue ? consCommQueue.registration : null),
    //         mileage: this._formBuilder.control(!!consCommQueue ? consCommQueue.mileage : null),
    //         totalAmount: this._formBuilder.control(!!consCommQueue ? consCommQueue.totalAmount : 0),
    //         listingPrice: this._formBuilder.control(!!consCommQueue ? consCommQueue.listingPrice : 0),
    //         equipmentTotalAmount: this._formBuilder.control(!!consCommQueue ? consCommQueue.equipmentTotalAmount : 0),
    //         optionalsTotalAmount: this._formBuilder.control(!!consCommQueue ? consCommQueue.optionalsTotalAmount : 0),
    //         discountAmount: this._formBuilder.control(!!consCommQueue ? consCommQueue.discountAmount : 0),
    //         discountPerc: this._formBuilder.control(!!consCommQueue ? consCommQueue.discountPerc : 0),
    //         optionals: this._formBuilder.control(!!consCommQueue ? consCommQueue.optionals : null),
    //     });
    //     // Sezione per rendere non editabili i campi
    //     // this.commercialDataForm.get('optionals').disable();
    // }


    /**
     * Crea i campi di inserimento e visualizzazione dati
     */
    createForms(commercialPersistent: ICommercial = null) {

        this.selectedOptionalList = !!commercialPersistent
            ? commercialPersistent.optionals
            : [];

        this.thirdPartList = !!commercialPersistent
            ? commercialPersistent.tpOptionals
            : [];

        this._logger.logInfo(this.constructor.name, 'createForm', 'commercialPersistent: ', commercialPersistent);

        this.commercialDataForm = this._formBuilder.group({
            price: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.price : this.priceFromPriceList),
            entry: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.entry : 0),
            supplier: this._formBuilder.control(this.suppliersList[0]),
            iva: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.iva : null),
            ipt: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.ipt : 0),
            marketValue: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.marketValue : 0),
            exemptAmount: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.exemptAmount : 0),
            finalPrice: this._formBuilder.control(this.priceFromPriceList),
            chassis: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.chassis : null),
            description: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.description : null),
            brand: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.brand : null),
            otherBrandOptional: this._formBuilder.control(!!commercialPersistent ? commercialPersistent.otherBrandOptional : 0),
        });
        // non editabile a mano
        this.commercialDataForm.get('finalPrice').disable();
        // editabile solo per non franchised
        if (!this.isOtherBrand) {
            this.commercialDataForm.get('price').disable();
            this.commercialDataForm.get('brand').disable();
        }


        const secondHandPersistent = !!commercialPersistent ? commercialPersistent.secondhandData : null;
        this.secondhandProductForm = this._formBuilder.group({
            licensePlate: this._formBuilder.control(!!secondHandPersistent ? secondHandPersistent.licensePlate : null),
            km: this._formBuilder.control(!!secondHandPersistent ? secondHandPersistent.km : null),
            registrationYear: this._formBuilder.control(!!secondHandPersistent ? secondHandPersistent.registrationYear : null),
            registrationMonth: this._formBuilder.control(!!secondHandPersistent ? secondHandPersistent.registrationMonth : null),
            assetConfirm: this._formBuilder.control(false),
        });
    }
        /** Indica se il veicolo (franchised!) selezionato è mercedes o smart*/
    get franchisedBrand(): IBrand {
        // NOTA: assume che non ci siano altri marchi franchised
        // let brandString = this.productInfo.model.makeCode == CODE_SMART ? 'smart' : 'Mercedes';
        // let brand: IBrand = {key: '?', value: brandString};//TODO la chiave qual è?
        // return brand;

        return {
            key: 'key',
            value: 'brand.value',
            defaultValue: true
        };
    }

    /**@ignore*/
    loadYears(): void {

        const yearsList: string[] = [];

        let year = +(new Date()).getFullYear();
        const lastYear = year - __yearRange;

        while (year >= lastYear) {
            yearsList.push(year.toString());
            year--;
        }

        this.years = yearsList;
    }
    /**@ignore*/
    loadMonths(): void {
        this.months = __months;
    }
}
