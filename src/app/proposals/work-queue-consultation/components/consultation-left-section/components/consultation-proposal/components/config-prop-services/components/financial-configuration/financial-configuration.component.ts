import {Component, OnInit, Input} from '@angular/core';
import {ConsultationFinancialQueue} from '../../../../../../../../../../data-model/class/Consultation/ConsultationFinancialQueue';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FinancialProcuct} from '../../../../../../../../../../data-model/class/Consultation/FinancialProcuct';
import {ValueListEnhanced} from "../../../../../../../../../../data-model/class/ListValuesEnhanced";

@Component({
    selector: 'app-financial-configuration',
    templateUrl: './financial-configuration.component.html',
    styleUrls: ['./financial-configuration.component.scss']
})
export class FinancialConfigurationComponent implements OnInit {

    /** Choosed proposal */
    financialQueue: ConsultationFinancialQueue = new ConsultationFinancialQueue();
    /*        // MOCKED QUEUE
        financialQueue: ConsultationFinancialQueue = {
            financialProduct: [{
                fpCode: '',
                fpDescription: '',
                defaultValue: '',
                campaigns: [{
                    code: '',
                    description: '',
                    expiryDate: '',
                    finalExpiryDate: '',
                    internalProposal: true,
                    mustUseSpIstr: true,
                    speseIstruttoria: 58,
                    defaultValue: true,
                    adminFeeShareOverride: true,
                    commCamp: true,
                    dealManuContrComm: true,
                    flagShare: true,
                    selFixComAmount: 59,
                    selFixComPerc: 60,
                    manuFixContrComm: 61,
                    manuPercContrComm: 62,
                    dealPercManuContrComm: 63,
                    dealFixCom: 64,
                    dealPercCom: 65
                }]
            }],
            serviceEntity: [{
                type: '',
                supplyTypeCode: '',
                supplyTypeDesc: '',
                defaultElement: true,
                defaultSupply: true,
                updatable: true,
                retrieveVat: true,
                tassoProdotto: true,
                beneStrumentale: true,
                rate: null,
                attachments: null,
                opzioneServizio: null,
                serviceEntities: null,
            }],
            anticipoPercentuale: 11,
            assetClassCode: '',
            assetType: '',
            balloonPercentuale: 12,
            baumuster: '',
            brandId: '',
            campaignCode: '',
            campiBloccati: {
                tan: false,
                rata: false,
                balloon: false,
                anticipo: false
            },
            codAnticipo: '',
            codCanoneRata: '',
            codeIpoteca: '',
            codiceTipoInteresse: '',
            contractLength: 13,
            contributoCampagna: 14,
            contributoConcessionario: 15,
            derogaBlocchiServiziAssicurativi: true,
            derogaCommerciale: true,
            derogaContributo: true,
            distance: 17,
            fattura: true,
            financedAmount: 18,
            financialProductCode: '',
            frequency: 19,
            importoAnticipo: 20,
            importoRataAnticipoServizi: 21,
            importoRataBalloon: 22,
            importoRataBalloonServizi: 23,
            importoRataServizi: 24,
            incentivoVendita: 25,
            ipt: 26,
            mileage: 27,
            mfr: 28,
            mss: 29,
            netCost: 30,
            operazioneK2K: true,
            provImportoDeltaTasso: 31,
            provImportoServizi: 32,
            provImportoServiziPerc: 33,
            provImportoSpeseIstruttoria: 34,
            provImportoSpeseIstruttoriaPerc: 35,
            provImportoTotale: 36,
            provImportoVendita: 37,
            provImportoVenditaPerc: 38,
            provvigioniDTO: {
                provImportoDeltaTasso: 66,
                provImportoDeltaTassoPerc: 67,
                provImportoServizi: 68,
                provImportoServiziPerc: 69,
                provImportoSpeseIstruttoria: 70,
                provImportoSpeseIstruttoriaPerc: 71,
                provImportoTotale: 72,
                provImportoVendita: 73,
                provImportoVenditaPerc: 74,
            },
            rata: 39,
            rataPercentuale: 40,
            rate: [{
                anticipo: false,
                balloon: false,
                bloccato: false,
                cashFlowTypeCode: '',
                cfAmount: 75,
                cfAmountPercentage: 76,
                dataPagamentoRata: '',
                distanza: 77,
                molteplicita: 78,
                numberOfTimes: 79,
                tassoFisso: false,
                totalAmount: 80,
                vatAmount: 81,
                vatPercentage: 82,
            }],
            richiestaFattura: true,
            speseAnticipo: 41,
            speseDiIstruttoria: 42,
            speseRataCanone: 43,
            spread: 44,
            taeg: 45,
            tan: 46,
            tassBase: 47,
            tassoBase: 48,
            tipoInteresse: '',
            totalAmount: 49,
            used: true,
            vatPercentage: 50,
            importoServiziAssicurativiRata: 51,
            importoServiziAccordoAssisetenzaRata: 52,
            importoServiziAssicurativiBalloon: 53,
            importoServiziAccordoAssistenzaBalloon: 54,
            importoTotaleRata: 55,
            importoTotaleBalloon: 56,
            kilometraggio: 57,
        };*/
    public financialQueueForm: FormGroup;
    selectedFinancialProduct: any = '';

    @Input('selectedFinancialQueue')
    set selectedFinancialQueue(value: ConsultationFinancialQueue) {
        if (value) {
            this.financialQueue = value;
            //let app = this.financialQueue.financialProduct.filter(x => x.defaultValue);
            this.selectedFinancialProduct = '';
        }
    }

    @Input() isEditing: boolean;

    @Input('optionsValue')
    set in(value) {
        if (value) this.setSelectOptions(value)
    }


    isLeasing: boolean = false;
    showBaloon: boolean = false;
    blockedFields = {
        matriceRate: false
    };
    additionalCharges: number = 0;
    totalAmountAdvance: number = 0;
    totalAmountAdvanceWithiIva: number = 0;
    totalAmountRate: number = 0;
    totalAmountRateWithiIva: number = 0;
    totalAmountBalloon: number = 0;
    totalAmountBalloonWithiIva: number = 0;
    insuranceAmmount: number = 0;
    assitanceAmmount: number = 0;
    managmentFees: number = 0;


    showContent: boolean;

    enhancedValuesMap: Map<string, ValueListEnhanced[]> = new Map<string, ValueListEnhanced[]>();

    /**
     * @ignore
     */
    effectiveDateList: any[];

    constructor(
        private _formBuilder: FormBuilder) {
        this.createForm(this.financialQueue);
    }

    ngOnInit() {
        this.showContent = false;
    }

    get rateLabel(): string {
        return this.isLeasing ? 'LEASE' : 'RATE';
    }

    get balloonLabel(): string {
        return this.isLeasing ? 'RANSOM' : 'BALLOON';
    }

    get totalLabel(): string[] {
        return this.isLeasing ? ['TOTAL', 'TAXABLE'] : ['AMOUNT', 'TOTAL'];
    }

    get totalWithIaLabel(): string[] {
        return this.isLeasing ? ['TOTAL', 'IVA_INCLUDED'] : ['AMOUNT', 'TOTAL'];
    }

    updateSelectForm(flag?: boolean): boolean {
        return flag;
    }

    updetateOutInfo(): void {
        return;
    }

    updateValueFromPercentage(): void {
        return;
    }

    createForm(financialQueue: ConsultationFinancialQueue) {
        this.financialQueueForm = this._formBuilder.group({
            financialProduct: this._formBuilder.control(!!financialQueue.financialProduct ? financialQueue.financialProduct : null),
            serviceEntities: this._formBuilder.control(!!financialQueue.serviceEntity ? financialQueue.serviceEntity : null),
            anticipoPercentuale: this._formBuilder.control(!!financialQueue.anticipoPercentuale ? financialQueue.anticipoPercentuale : null),
            assetClassCode: this._formBuilder.control(!!financialQueue.assetClassCode ? financialQueue.assetClassCode : null),
            assetType: this._formBuilder.control(!!financialQueue.assetType ? financialQueue.assetType : null),
            balloonPercentuale: this._formBuilder.control(!!financialQueue.balloonPercentuale ? financialQueue.balloonPercentuale : null),
            baumuster: this._formBuilder.control(!!financialQueue.baumuster ? financialQueue.baumuster : null),
            brandId: this._formBuilder.control(!!financialQueue.brandId ? financialQueue.brandId : null),
            campaignCode: this._formBuilder.control(!!financialQueue.campaignCode ? financialQueue.campaignCode : null),
            campiBloccati: this._formBuilder.control(!!financialQueue.campiBloccati ? financialQueue.campiBloccati : null),
            codAnticipo: this._formBuilder.control(!!financialQueue.codAnticipo ? financialQueue.codAnticipo : null),
            codCanoneRata: this._formBuilder.control(!!financialQueue.codCanoneRata ? financialQueue.codCanoneRata : null),
            codeIpoteca: this._formBuilder.control(!!financialQueue.codeIpoteca ? financialQueue.codeIpoteca : null),
            codiceTipoInteresse: this._formBuilder.control(!!financialQueue.codiceTipoInteresse ? financialQueue.codiceTipoInteresse : null),
            contractLength: this._formBuilder.control(!!financialQueue.contractLength ? financialQueue.contractLength : null),
            contributoCampagna: this._formBuilder.control(!!financialQueue.contributoCampagna ? financialQueue.contributoCampagna : null),
            contributoConcessionario: this._formBuilder.control(!!financialQueue.anticipoPercentuale ? financialQueue.anticipoPercentuale : null),
            derogaBlocchiServiziAssicurativi: this._formBuilder.control(!!financialQueue.derogaBlocchiServiziAssicurativi ? financialQueue.derogaBlocchiServiziAssicurativi : null),
            derogaCommerciale: this._formBuilder.control(!!financialQueue.derogaCommerciale ? financialQueue.derogaCommerciale : null),
            derogaContributo: this._formBuilder.control(!!financialQueue.derogaContributo ? financialQueue.derogaContributo : null),
            distance: this._formBuilder.control(!!financialQueue.distance ? financialQueue.distance : null),
            fattura: this._formBuilder.control(!!financialQueue.fattura ? financialQueue.fattura : null),
            financedAmount: this._formBuilder.control(!!financialQueue.financedAmount ? financialQueue.financedAmount : null),
            financialProductCode: this._formBuilder.control(!!financialQueue.financialProductCode ? financialQueue.financialProductCode : null),
            frequency: this._formBuilder.control(!!financialQueue.frequency ? financialQueue.frequency : null),
            importoAnticipo: this._formBuilder.control(!!financialQueue.importoAnticipo ? financialQueue.importoAnticipo : null),
            importoRataAnticipoServizi: this._formBuilder.control(!!financialQueue.importoRataAnticipoServizi ? financialQueue.importoRataAnticipoServizi : null),
            importoRataBalloon: this._formBuilder.control(!!financialQueue.importoRataBalloon ? financialQueue.importoRataBalloon : null),
            importoRataBalloonServizi: this._formBuilder.control(!!financialQueue.importoRataBalloonServizi ? financialQueue.importoRataBalloonServizi : null),
            importoRataServizi: this._formBuilder.control(!!financialQueue.importoRataServizi ? financialQueue.importoRataServizi : null),
            incentivoVendita: this._formBuilder.control(!!financialQueue.incentivoVendita ? financialQueue.incentivoVendita : null),
            ipt: this._formBuilder.control(!!financialQueue.ipt ? financialQueue.ipt : null),
            mileage: this._formBuilder.control(!!financialQueue.mileage ? financialQueue.mileage : null),
            mfr: this._formBuilder.control(!!financialQueue.mfr ? financialQueue.mfr : null),
            mss: this._formBuilder.control(!!financialQueue.mss ? financialQueue.mss : null),
            netCost: this._formBuilder.control(!!financialQueue.netCost ? financialQueue.netCost : null),
            operazioneK2K: this._formBuilder.control(!!financialQueue.operazioneK2K ? financialQueue.operazioneK2K : null),
            provImportoDeltaTasso: this._formBuilder.control(!!financialQueue.provImportoDeltaTasso ? financialQueue.provImportoDeltaTasso : null),
            provImportoServizi: this._formBuilder.control(!!financialQueue.provImportoServizi ? financialQueue.provImportoServizi : null),
            provImportoServiziPerc: this._formBuilder.control(!!financialQueue.provImportoServiziPerc ? financialQueue.provImportoServiziPerc : null),
            provImportoSpeseIstruttoria: this._formBuilder.control(!!financialQueue.provImportoSpeseIstruttoria ? financialQueue.provImportoSpeseIstruttoria : null),
            provImportoSpeseIstruttoriaPerc: this._formBuilder.control(!!financialQueue.provImportoSpeseIstruttoriaPerc ? financialQueue.provImportoSpeseIstruttoriaPerc : null),
            provImportoTotale: this._formBuilder.control(!!financialQueue.provImportoTotale ? financialQueue.provImportoTotale : null),
            provImportoVendita: this._formBuilder.control(!!financialQueue.provImportoVendita ? financialQueue.provImportoVendita : null),
            provImportoVenditaPerc: this._formBuilder.control(!!financialQueue.provImportoVenditaPerc ? financialQueue.provImportoVenditaPerc : null),
            provvigioniDTO: this._formBuilder.control(!!financialQueue.provvigioniDTO ? financialQueue.provvigioniDTO : null),
            rata: this._formBuilder.control(!!financialQueue.rata ? financialQueue.rata : null),
            rataPercentuale: this._formBuilder.control(!!financialQueue.rataPercentuale ? financialQueue.rataPercentuale : null),
            rate: this._formBuilder.control(!!financialQueue.rate ? financialQueue.rate : null),
            richiestaFattura: this._formBuilder.control(!!financialQueue.richiestaFattura ? financialQueue.richiestaFattura : null),
            speseAnticipo: this._formBuilder.control(!!financialQueue.speseAnticipo ? financialQueue.speseAnticipo : null),
            speseDiIstruttoria: this._formBuilder.control(!!financialQueue.speseDiIstruttoria ? financialQueue.speseDiIstruttoria : null),
            speseRataCanone: this._formBuilder.control(!!financialQueue.speseRataCanone ? financialQueue.speseRataCanone : null),
            spread: this._formBuilder.control(!!financialQueue.spread ? financialQueue.spread : null),
            taeg: this._formBuilder.control(!!financialQueue.taeg ? financialQueue.taeg : null),
            tan: this._formBuilder.control(!!financialQueue.tan ? financialQueue.tan : null),
            tassBase: this._formBuilder.control(!!financialQueue.tassBase ? financialQueue.tassBase : null),
            tassoBase: this._formBuilder.control(!!financialQueue.tassoBase ? financialQueue.tassoBase : null),
            tipoInteresse: this._formBuilder.control(!!financialQueue.tipoInteresse ? financialQueue.tipoInteresse : null),
            totalAmount: this._formBuilder.control(!!financialQueue.totalAmount ? financialQueue.totalAmount : null),
            used: this._formBuilder.control(!!financialQueue.used ? financialQueue.used : null),
            vatPercentage: this._formBuilder.control(!!financialQueue.vatPercentage ? financialQueue.vatPercentage : null),
            importoServiziAssicurativiRata: this._formBuilder.control(!!financialQueue.importoServiziAssicurativiRata ? financialQueue.importoServiziAssicurativiRata : null),
            importoServiziAccordoAssisetenzaRata: this._formBuilder.control(!!financialQueue.importoServiziAccordoAssisetenzaRata ? financialQueue.importoServiziAccordoAssisetenzaRata : null),
            importoServiziAssicurativiBalloon: this._formBuilder.control(!!financialQueue.importoServiziAssicurativiBalloon ? financialQueue.importoServiziAssicurativiBalloon : null),
            importoServiziAccordoAssistenzaBalloon: this._formBuilder.control(!!financialQueue.importoServiziAccordoAssistenzaBalloon ? financialQueue.importoServiziAccordoAssistenzaBalloon : null),
            importoTotaleRata: this._formBuilder.control(!!financialQueue.importoTotaleRata ? financialQueue.importoTotaleRata : null),
            importoTotaleBalloon: this._formBuilder.control(!!financialQueue.importoTotaleBalloon ? financialQueue.importoTotaleBalloon : null),
            kilometraggio: this._formBuilder.control(!!financialQueue.kilometraggio ? financialQueue.kilometraggio : null),
        });

    }

    setSelectOptions(value): void {
        value.forEach(x => {
            this.enhancedValuesMap.set(x.listCode, x.values);
        });
    }

    returnList(value): any {
        if (this.enhancedValuesMap && this.enhancedValuesMap.size !== 0) {
            return this.enhancedValuesMap.get(value)
        } else {
            return []
        }
    }

    setEffectiveDateList(withReload: boolean = false) {
        console.log('enhanced DECORRENZA ', this.effectiveDateList);

        if (!this.enhancedValuesMap) this.effectiveDateList = [];

        let targetVal = this.financialQueue.frequency;

        //TODO: da capire perche la prima volta che esegue il form detiene il valore e non la chiave
        let target = this.enhancedValuesMap.get('FREQUENCY').find(x => {
            return (x.key == targetVal.toString());
        });


        this.effectiveDateList = target.additionalProperties;
        let defaultED = this.effectiveDateList.find(x => {
            return x.defaultValue;
        }).value;
        this.financialQueue.distance = defaultED;

        console.log('enhanced DECORRENZA ', this.effectiveDateList);

        if (withReload)
            this.updateSelectForm();
    }


}
