import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CommonInputComponent} from './common-input.component';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader} from '../../../../../../../../../app.module';
import {HttpClient, HttpClientModule} from '../../../../../../../../../../../node_modules/@angular/common/http';
import {CurrencyInputComponent} from '../../../../../../../../../shared/components/currency-input/currency-input.component';
import {ICommon} from '../../../../../../../../../data-model/interface/IFinancialInput';

describe('CommonInputComponent', () => {
    let component: CommonInputComponent;
    let fixture: ComponentFixture<CommonInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CommonInputComponent, CurrencyInputComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CommonInputComponent);
        component = fixture.componentInstance;

        let mockICommon: ICommon = {
            noleggioOtaxi: false,
            recuperoIva: false,
            valoreAssicurativo: 0,
            deroga: false
        };
        component.commonInputValue = mockICommon;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            noleggioOtaxi: builder.control([]),
            valoreAssicurativo: builder.control([]),
            recuperoIva: builder.control([]),
            deroga: builder.control([])
        });
        component.commonForm = mockFormGroup;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
