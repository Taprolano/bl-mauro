import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeelStarPremiumNordOvestInputComponent} from './feel-star-premium-nord-ovest-input.component';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {SvgIconComponent} from "../../../../../../../../../../../../../../shared/components/svg-icon/svg-icon.component";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {FireTheftCoverage} from "../../../../../../../../../../../../../../data-model/class/FireTheftCoverage";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('FeelStarPremiumNordOvestInputComponent', () => {
    let component: FeelStarPremiumNordOvestInputComponent;
    let fixture: ComponentFixture<FeelStarPremiumNordOvestInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FeelStarPremiumNordOvestInputComponent,
                SvgIconComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeelStarPremiumNordOvestInputComponent);
        component = fixture.componentInstance;

        let mockObj = new Object();
        mockObj = {
            provincia: ' ',
            satellitare: false,
            privacy: false
        };

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', '' , mockObj, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        let mockSerInner: FinancialConfigServiceResponseInner = new FinancialConfigServiceResponseInner();
        let mockFireTheftCoverage: FireTheftCoverage = new FireTheftCoverage(' ', '', 0);
        let mockFireThArray: FireTheftCoverage[] = [];
        mockFireThArray.push(mockFireTheftCoverage);
        mockSerInner.fireTheftCoverage = mockFireThArray;
        component.service = mockSerInner;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.valideteForm();
    });

    it('validateForm formFalse test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(true),
        });
        component.feelStarNordOvestForm = mockFormGroup;

        component.valideteForm();
    });

    it('getServiceData isValid true test', () => {
        spyOn(component, 'valideteForm').and.returnValue(true);
        component.getServiceData();
    });

    it('getServiceData isValid false test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(false),
        });
        component.feelStarNordOvestForm = mockFormGroup;

        component.getServiceData();
    });
});
