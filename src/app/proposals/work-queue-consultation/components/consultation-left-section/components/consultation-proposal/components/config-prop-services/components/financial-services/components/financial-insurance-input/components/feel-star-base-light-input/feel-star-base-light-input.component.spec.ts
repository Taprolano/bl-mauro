import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeelStarBaseLightInputComponent} from './feel-star-base-light-input.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProposalsModule} from "../../../../../../../../../../../../../proposals.module";
import {CommonModule} from "@angular/common";
import {MbzDashboardModule} from "../../../../../../../../../../../../../../mbz-dashboard/mbz-dashboard.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AuthGuard} from "../../../../../../../../../../../../../../shared/guard";
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "../../../../../../../../../../../../../../app-routing.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {SharedModule} from "../../../../../../../../../../../../../../shared/shared.module";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {AppComponent} from "../../../../../../../../../../../../../../app.component";
import {ServiceWorkerModule} from "@angular/service-worker";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {FireTheftCoverage} from "../../../../../../../../../../../../../../data-model/class/FireTheftCoverage";
import {DirectAccessGuard} from "../../../../../../../../../../../../../../shared/guard/direct-access/direct-access.guard";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {environment} from "../../../../../../../../../../../../../../../environments/environment";

describe('FeelStarBaseLightInputComponent', () => {
    let component: FeelStarBaseLightInputComponent;
    let fixture: ComponentFixture<FeelStarBaseLightInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeelStarBaseLightInputComponent);
        component = fixture.componentInstance;

        let mockObj = new Object();
        mockObj = {
            quattroZampe: false,
            autoCortesia: false,
            infortuniConducente: false,
            provincia: ' ',
            satellitare: false,
            privacy: false
        };

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', mockObj, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        let mockSerInner: FinancialConfigServiceResponseInner = new FinancialConfigServiceResponseInner();
        let mockFireTheftCoverage: FireTheftCoverage = new FireTheftCoverage(' ', '', 0);
        let mockFireThArray: FireTheftCoverage[] = [];
        mockFireThArray.push(mockFireTheftCoverage);
        mockSerInner.fireTheftCoverage = mockFireThArray;
        component.service = mockSerInner;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.valideteForm();
    });

    it('validateForm formFalse test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(true),
        });
        component.feelStarBaseLightForm = mockFormGroup;

        component.valideteForm();
    });

    it('getServiceData isValid true test', () => {
        spyOn(component, 'valideteForm').and.returnValue(true);
        component.getServiceData();
    });

    it('getServiceData isValid false test', () => {
        spyOn(component, 'valideteForm').and.returnValue(false);

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(false),
        });
        component.feelStarBaseLightForm = mockFormGroup;

        component.getServiceData();
    });

});
