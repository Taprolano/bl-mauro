import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-financial-payment-method',
  templateUrl: './financial-payment-method.component.html',
  styleUrls: ['./financial-payment-method.component.scss']
})
export class FinancialPaymentMethodComponent implements OnInit {

  @Input() isEditing : boolean;

  showContent: boolean;

  spese : number = 0.0;

  constructor() { }

  ngOnInit() {
  }

}
