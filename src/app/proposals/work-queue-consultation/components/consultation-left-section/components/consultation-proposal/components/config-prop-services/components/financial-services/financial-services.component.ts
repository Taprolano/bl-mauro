import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FinancialProcuct} from "../../../../../../../../../../data-model/class/Consultation/FinancialProcuct";
import {BeFinancialService} from "../../../../../../../../../../shared/services/be-financial.service";
import {LoggerService} from "../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../shared/services/utils.service";
import {NotificationService} from "../../../../../../../../../../shared/components/notification/notification.service";
import {
    REQUEST_PRIORITY,
    RequestManagerService
} from "../../../../../../../../../../shared/services/request-manager.service";
import {ProposalService} from "../../../../../../../../../../shared/services/proposal.service";
import {FinancialValue} from "../../../../../../../../../../data-model/class/FinancialValue";
import {TranslateService} from "@ngx-translate/core";
import {FinancialServices} from "../../../../../../../../../../data-model/class/FinancialServices";
import {CheckCompatibilityReqParamas} from "../../../../../../../../../../data-model/class/CheckCompatibilityReqParamas";
import {
    CodeObj,
    FinancialConfigServiceInner, FinancialConfigServiceInnerFactory
} from "../../../../../../../../../../data-model/class/FinancialConfigServiceInner";
import {FinancialServicesInner} from "../../../../../../../../../../data-model/class/Consultation/FinancialServiceInner";
import {FinancialConfigServiceResponse} from "../../../../../../../../../../data-model/class/FinancialConfigServiceResponse";
import {Subject} from "rxjs";
import {AppSettings} from "../../../../../../../../../../../AppSettings";
import {FinancialInsuranceTypeComponent} from "./components/financial-insurance-type/financial-insurance-type.component";
import {ServicePlusCompactInputComponent} from "./components/financial-insurance-input/components/service-plus-compact-input/service-plus-compact-input.component";
import {ServicePlusAdvanceInputComponent} from "./components/financial-insurance-input/components/service-plus-advance-input/service-plus-advance-input.component";
import {ServicePlusConfortInputComponent} from "./components/financial-insurance-input/components/service-plus-confort-input/service-plus-confort-input.component";
import {ServicePlusExcellentInputComponent} from "./components/financial-insurance-input/components/service-plus-excellent-input/service-plus-excellent-input.component";
import {FeelNewInputComponent} from "./components/financial-insurance-input/components/feel-new-input/feel-new-input.component";
import {FeelStarPremiumSudInputComponent} from "./components/financial-insurance-input/components/feel-star-premium-sud-input/feel-star-premium-sud-input.component";
import {FeelSafeInputComponent} from "./components/financial-insurance-input/components/feel-safe-input/feel-safe-input.component";
import {FeelStarPremiumCentroInputComponent} from "./components/financial-insurance-input/components/feel-star-premium-centro-input/feel-star-premium-centro-input.component";
import {FeelStarPremiumNordEstInputComponent} from "./components/financial-insurance-input/components/feel-star-premium-nord-est-input/feel-star-premium-nord-est-input.component";
import {FeelStarPremiumNordOvestInputComponent} from "./components/financial-insurance-input/components/feel-star-premium-nord-ovest-input/feel-star-premium-nord-ovest-input.component";
import {FeelStarBaseLightInputComponent} from "./components/financial-insurance-input/components/feel-star-base-light-input/feel-star-base-light-input.component";
import {FeelStarBaseInputComponent} from "./components/financial-insurance-input/components/feel-star-base-input/feel-star-base-input.component";
import {FeelCareBaseInputComponent} from "./components/financial-insurance-input/components/feel-care-base-input/feel-care-base-input.component";
import {FeelLifeInputComponent} from "./components/financial-insurance-input/components/feel-life-input/feel-life-input.component";
import {CommonInputComponent} from "./components/financial-insurance-input/components/common-input/common-input.component";
import {FinancialCampaign} from "../../../../../../../../../../data-model/class/FinancialCampaign";
import {IProposal} from "../../../../../../../../../../data-model/interface/IProposal";
import {
    IBaseInput, IBaseLightInput,
    ICommon,
    IFeelCareInput,
    IFeelLifeInput, IFeelSafeInput, IPremiumCentro, IPremiumNordEst, IPremiumNordOvest, IPremiumSud, IRtiInput
} from "../../../../../../../../../../data-model/interface/IFinancialInput";
import {
    ASSISTENZA,
    FINANCIAL_FEEL_CARE_AXA, FINANCIAL_FEEL_LIFE, FINANCIAL_FEEL_NEW, FINANCIAL_FEEL_SAFE, FINANCIAL_FEEL_STAR,
    FINANCIAL_SERVICES
} from "../../../../../../../../../../data-model/enum/EFinancialState";
import {IContractInfo} from "../../interface/IContractInfo";
import {
    NOTIFICATION_TYPE,
    NotificationMessage
} from "../../../../../../../../../../shared/components/notification/NotificationMessage";
import {IServiceCost} from "../../../../../../../../../../data-model/interface/IServiceCost";
import {FinancialConstant} from "../../FinancialConstant";
import {RequestManagerConstant} from "../../../../../../../../../requestManagerConstant";
import {takeUntil} from "rxjs/operators";
import {FinancialConfigServiceBodyParameters} from "../../../../../../../../../../data-model/class/FinancialConfigServiceBodyParameters";
import {FinancialServiceBodyParameters} from "../../../../../../../../../../data-model/class/FinancialServiceBodyParameters";
import {FinancialServicesReqParams} from "../../../../../../../../../../data-model/class/FinancialServicesReqParams";
import {ConsultationFinancialQueue} from "../../../../../../../../../../data-model/class/Consultation/ConsultationFinancialQueue";

@Component({
    selector: 'app-financial-services',
    templateUrl: './financial-services.component.html',
    styleUrls: ['./financial-services.component.scss']
})
export class FinancialServicesComponent implements OnInit {

    showContent: boolean;
    @Input() isEditing: boolean;
    @Input() selectedFinancialQueue: ConsultationFinancialQueue;

    /** @ignore */
    financialServices = FINANCIAL_SERVICES;

    /** @ignore */
    financialFeelLife = FINANCIAL_FEEL_LIFE;

    /** @ignore */
    financialFeelCareAxa = FINANCIAL_FEEL_CARE_AXA;

    /** @ignore */
    financialFeelStar = FINANCIAL_FEEL_STAR;

    /** @ignore */
    financialFeelNew = FINANCIAL_FEEL_NEW;

    /** @ignore */
    financialFeelSafe = FINANCIAL_FEEL_SAFE;

    /** @ignore */
    financialAssistenza = ASSISTENZA;

    /** Campagna selezionata */
    @Input()
    campaignSelected: FinancialCampaign;

    /** Proposta In Corso */
    @Input()
    proposal: IProposal;

    /** Oggetto con i dati contratto necessari */
    @Input()
    contractInfo: IContractInfo;

    /** Event to update selectedServicesList list in financial component */
    @Output() addService: EventEmitter<FinancialValue[]>;

    /** Evento per mandare al piano finanziario la lista completa di servizi finanziari */
    @Output() serviceEntitiesList: EventEmitter<FinancialServices[]>;

    /** Instanza del componente commonInput */
    @ViewChild(CommonInputComponent) commonInput: CommonInputComponent;

    /** Instanza del componente feelLifeInput */
    @ViewChild(FeelLifeInputComponent) feelLifeInput: FeelLifeInputComponent;

    /** Instanza del componente feelCareInput */
    @ViewChild(FeelCareBaseInputComponent) feelCareInput: FeelCareBaseInputComponent;

    /** Instanza del componente feelStarBaseForm */
    @ViewChild(FeelStarBaseInputComponent) feelStarBaseInput: FeelStarBaseInputComponent;

    /** Instanza del componente feelStarBaseLigth */
    @ViewChild(FeelStarBaseLightInputComponent) feelStarBaseLightInput: FeelStarBaseLightInputComponent;

    /** Instanza del componente feelStarPremiumNO */
    @ViewChild(FeelStarPremiumNordOvestInputComponent) feelStarPremiumNordOvestInput: FeelStarPremiumNordOvestInputComponent;

    /** Instanza del componente feelStarPremiumNE */
    @ViewChild(FeelStarPremiumNordEstInputComponent) feelStarPremiumNordEstInput: FeelStarPremiumNordEstInputComponent;

    /** Instanza del componente feelStarPremiumC */
    @ViewChild(FeelStarPremiumCentroInputComponent) feelStarPremiumCentro: FeelStarPremiumCentroInputComponent;

    /** Instanza del componente feelStarPremiumSud */
    @ViewChild(FeelStarPremiumSudInputComponent) feelStarPremiumSud: FeelStarPremiumSudInputComponent;

    /** Instanza del componente feelSafeInput */
    @ViewChild(FeelSafeInputComponent) feelSafeInput: FeelSafeInputComponent;

    /** Instanza del componente feelNewInput */
    @ViewChild(FeelNewInputComponent) feelNewInput: FeelNewInputComponent;

    /** Instanza del componente excellent input */
    @ViewChild(ServicePlusExcellentInputComponent) plusExcellentInput: ServicePlusExcellentInputComponent;

    /** Istanza del componente confort Input */
    @ViewChild(ServicePlusConfortInputComponent) plusConfortInput: ServicePlusConfortInputComponent;

    /** Istanza del componente advance Input */
    @ViewChild(ServicePlusAdvanceInputComponent) plusAdvanceInput: ServicePlusAdvanceInputComponent;

    /** Istanza del componente compact Input */
    @ViewChild(ServicePlusCompactInputComponent) plusCompactInput: ServicePlusCompactInputComponent;

    /** Instanza del componende FinancialInsuranceType */
    @ViewChild(FinancialInsuranceTypeComponent) financialInsuranceType: FinancialInsuranceTypeComponent;

    /**
     * Opzioni per scrollbar
     * @clazz {{axis: string; theme: string; setHeight: string}}
     */
    scrollbarOptions = {axis: 'y', theme: AppSettings.scrollStyle, setHeight: '100%'};

    /** Apertura Accordion Servizi Assicurativi
     * @clazz boolean
     **/
    insuranceAccordionOpen: boolean;

    /** Attiva seconda colonna Servizi Assicurativi
     * @clazz boolean
     **/
    secondColumnEnable: boolean;

    /** Attiva terza colonna Servizi Assicurativi
     * @clazz boolean
     **/
    thirdColumnEnable: boolean;

    /** Attiva Bottoni di conferma ed elimina
     * @clazz boolean
     **/
    buttonsEnabled: boolean;

    /** Flag per controllare se stiamo attraversando il flusso di rimozione
     * di un servizio finanziario
     * @type {boolean}
     */
    inDelete: boolean = false;

    /** @ignore */
    destroy$: Subject<boolean> = new Subject<boolean>();

    /** Response della chiamata FinancialConfigService */
    public financialConfigResponse: FinancialConfigServiceResponse;

    /** Id del tab di tipo Type attivo  */
    public tabTypeActiveId: string;

    /** Lista di Servizi Finanziari Disponibili dal BE */
    public financialTypeList: FinancialServices[];

    /** Lista di Servizi Finanziari Visiibili in base al valore di isTaxi */
    public financialTypeListValid: FinancialServices[] = [];

    /** Servizio Finanziario selezionato attualmente */
    public financialTypeSelected: FinancialServices;

    /** Lista di Elem del Servizio Finanziario selezionato */
    public financialElemList: FinancialServicesInner[];

    /** Singolo Dettaglio selezionato del Servizio Finanziario selezionato con campi aggiuntivi */
    public financialElemSelected: FinancialConfigServiceInner;

    /** Id del tab di tipo Elem attivo */
    public tabElemActiveId: string;

    /** Oggetto con i dati del servizio finanziario selezionado in corso */
    public financialValue: FinancialValue;

    /** Lista di oggetti contentente le form value */
    public financialValueList: FinancialValue[] = [];

    /** @ignore */
    private _mock = true;

    /**
     * Oggetto con i campi di input comuni a tutti i servizi finanziari
     * @clazz {{noleggioOtaxi: string; valoreAssicurativo: string; recuperoIva: boolean}}
     */
    commonInputValue: ICommon = {
        noleggioOtaxi: null,
        valoreAssicurativo: 0,
        recuperoIva: false,
        deroga: false
    };

    /** Requst Parameter per la chiamata "CheckCompatibility" */
    public checkCompatibilityReqParams = new CheckCompatibilityReqParamas('', [], '');


    constructor(
        private _financialService: BeFinancialService,
        private _logger: LoggerService,
        private _utils: UtilsService,
        private _notification: NotificationService,
        private _requestManagerService: RequestManagerService,
        private _proposalService: ProposalService,
        private translate: TranslateService) {

        this.addService = new EventEmitter<FinancialValue[]>();

        this.serviceEntitiesList = new EventEmitter<FinancialServices[]>();
    }

    ngOnInit() {
        this.showContent = false;
        this.secondColumnEnable = false;
        this.thirdColumnEnable = false;
        this.buttonsEnabled = false;
    }

    /**
     * Attiva o Disattiva l'accordion Financial Insurance se è presente un codice campagna
     */
    toggleInsuranceAccordion() {
        if (this._mock) {
            this.insuranceAccordionOpen = !this.insuranceAccordionOpen;
            if (this.insuranceAccordionOpen) this.getFinancialInsuranceList();
        } else {
            if (!this._utils.isVoid(this.campaignSelected.fpCode)) {
                this.insuranceAccordionOpen = !this.insuranceAccordionOpen;
                if (this.insuranceAccordionOpen) {
                    this.getFinancialInsuranceList();
                    this.commonInputValue.valoreAssicurativo = this.contractInfo.netCost;
                }
            } else {
                this._notification.sendNotification(new NotificationMessage(this.translate.instant('SELECT_A_CAMPAIGN_FIRST'), NOTIFICATION_TYPE.WARN));
            }
        }
    }

    /**
     * Carica la lista dei dettagli del servizio finanziario selezionato e
     * mostra la seconda colonna del tab Financial Insurance
     * @param obj
     */
    loadDetail(obj: FinancialValue) {
        this.financialValue = new FinancialValue();
        this.financialValue = obj;
        this.tabElemActiveId = null;
        this.thirdColumnEnable = false;
        this.buttonsEnabled = false;

        // Setto al servizio finanziario in corso, l'id del parent recuperato tramite componente figlio
        // Setto il tabType attivo al valore del id del parent
        this.tabTypeActiveId = this.financialValue.idParent;

        // Elemento della prima colonna selezionato tramite l'id del elem clickato
        this.financialTypeSelected = this.financialTypeListValid[this.financialValue.idParent];

        // Lista di sotto-servizi dell'elemento selezionato preso dalla lista di servizi validi ( flag taxi/noleggio )
        this.financialElemList = this.financialTypeListValid[this.financialValue.idParent].serviceEntities;

        // Attivo la seconda colonna
        this.secondColumnEnable = true;

        // Disattivo il bottone per salvare
        this.buttonsEnabled = false;
    }

    /**
     * Carica gli input del servizio e dettaglio finanziario selezionato e
     * mostra la terza colonna del tab Financial Insurance
     * @param obj
     */
    loadInput(obj: FinancialValue) {
        this._logger.logInfo('FinancialInsurance', 'loadInput', 'financialElem input', obj);

        this.financialValue = obj;

        // Setto alle variebili locali i valori che arrivano in input dall'oggetto obj
        this.tabTypeActiveId = obj.idParent;
        this.tabElemActiveId = obj.idChild;

        // Setto a financialElemSelected il sotto-servizo selezionato tramite l'id del sotto-servizio attivo
        this.financialElemSelected = this.financialTypeSelected.serviceEntities[this.tabElemActiveId];

        this._logger.logInfo('FinancialInsurance', 'loadInput', 'financialValue', this.financialValue);

        // Controllo la compatibilita tra i servizi attivi e quello selezionato, per ora c'e una pezza nella callback della chiamata
        this.getCheckCompatibility();

    }

    /** In caso il suppleElemCode è compatibile recupero o è gia selezionato recupero la lista in input */
    getInputField() {

        switch (this.financialValue.code) {
            case AppSettings.HTTP_OK:
                // Chiamo il servizio per recuperare i valori delle form dal BE
                this.getFinancialConfigService();
                this._logger.logInfo('FinancialInsurance', 'getInputField', 'Compatibile e/o presente nella lista');

                break;
            case AppSettings.HTTP_OK_BLOCK:
                this._notification.sendNotification(new NotificationMessage(
                    this.translate.instant('SERVICE_INSERT_ERROR'), NOTIFICATION_TYPE.WARN
                ));
                this.thirdColumnEnable = false;
                this.buttonsEnabled = false;
                this._logger.logInfo('FinancialInsurance', 'getInputField', 'Non Compatibile ');

                break;
            default:
                this._notification.sendNotification(new NotificationMessage(
                    this.translate.instant('ERROR'), NOTIFICATION_TYPE.ERROR
                ));
                this._logger.logInfo('FinancialInsurance', 'getInputField', 'Errore ');

                break;
        }
    }

    /**
     * Switch tra le varie valide form dei figli per scegliere
     * quale tra quella del Financial Insurance Input è attiva.
     * Usa il supplyElemCode per accedere a quella corrente
     */
    validateForm() {
        let serviceInn: FinancialConfigServiceInner = this.financialElemSelected;
        this.commonInputValue = this.commonInput.getServiceData();

        switch (this.financialValue.supplyElemCode) {
            case this.financialFeelLife.FEEL_LIFE: {
                this.financialValue = this.feelLifeInput.getServiceData();
                this.financialValue.object = <IFeelLifeInput>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_LIFE;
                this.financialValue.supplyTypeDesc = 'FEEL LIFE';
                this.financialValue.supplyElemCode = this.financialFeelLife.FEEL_LIFE;
                this.financialValue.supplyElemDesc = 'FEEL LIFE';
                break;
            }
            case this.financialFeelCareAxa.FEEL_CARE_AXA: {
                this.financialValue = this.feelCareInput.getServiceData();
                this.financialValue.object = <IFeelCareInput>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_CARE_AXA;
                this.financialValue.supplyTypeDesc = 'FEEL CARE AXA';
                this.financialValue.supplyElemDesc = this.financialFeelCareAxa.FEEL_CARE_AXA;
                this.financialValue.supplyElemDesc = 'FEEL CARE AXA';
                break;
            }
            case this.financialFeelStar.BASE: {
                this.financialValue = this.feelStarBaseInput.getServiceData();
                this.financialValue.object = <IBaseInput>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_STAR;
                this.financialValue.supplyTypeDesc = 'FEEL STAR';
                this.financialValue.supplyElemCode = this.financialFeelStar.BASE;
                this.financialValue.supplyElemDesc = 'BASE';
                break;
            }
            case this.financialFeelStar.BASE_LIGHT: {
                this.financialValue = this.feelStarBaseLightInput.getServiceData();
                this.financialValue.object = <IBaseLightInput>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_STAR;
                this.financialValue.supplyTypeDesc = 'FEEL STAR';
                this.financialValue.supplyElemCode = this.financialFeelStar.BASE_LIGHT;
                this.financialValue.supplyElemDesc = 'BASE LIGHT';
                break;
            }
            case this.financialFeelStar.PREMIUM_NORD_OVEST: {
                this.financialValue = this.feelStarPremiumNordOvestInput.getServiceData();
                this.financialValue.object = <IPremiumNordOvest>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_STAR;
                this.financialValue.supplyTypeDesc = 'FEEL STAR';
                this.financialValue.supplyElemCode = this.financialFeelStar.PREMIUM_NORD_OVEST;
                this.financialValue.supplyElemDesc = 'PREMIUM NORD OVEST';
                break;
            }
            case this.financialFeelStar.PREMIUM_NORD_EST: {
                this.financialValue = this.feelStarPremiumNordEstInput.getServiceData();
                this.financialValue.object = <IPremiumNordEst>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_STAR;
                this.financialValue.supplyTypeDesc = 'FEEL STAR';
                this.financialValue.supplyElemCode = this.financialFeelStar.PREMIUM_NORD_EST;
                this.financialValue.supplyElemDesc = 'PREMIUM NORD EST';
                break;
            }
            case this.financialFeelStar.PREMIUM_CENTRO: {
                this.financialValue = this.feelStarPremiumCentro.getServiceData();
                this.financialValue.object = <IPremiumCentro>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_STAR;
                this.financialValue.supplyTypeDesc = 'FEEL STAR';
                this.financialValue.supplyElemCode = this.financialFeelStar.PREMIUM_CENTRO;
                this.financialValue.supplyElemDesc = 'PREMIUM CENTRO';
                break;
            }
            case this.financialFeelStar.PREMIUM_SUD: {
                this.financialValue = this.feelStarPremiumSud.getServiceData();
                this.financialValue.object = <IPremiumSud>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_STAR;
                this.financialValue.supplyTypeDesc = 'FEEL STAR';
                this.financialValue.supplyElemCode = this.financialFeelStar.PREMIUM_SUD;
                this.financialValue.supplyElemDesc = 'PREMIUM SUD';
                break;
            }
            case this.financialFeelNew.FEEL_NEW: {
                this.financialValue = this.feelNewInput.getServiceData();
                this.financialValue.object = <IRtiInput>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_NEW;
                this.financialValue.supplyTypeDesc = 'FEEL NEW';
                this.financialValue.supplyElemCode = this.financialFeelNew.FEEL_NEW;
                this.financialValue.supplyElemDesc = 'FEEL NEW';
                break;
            }
            case this.financialFeelSafe.FEEL_SAFE: {
                this.financialValue = this.feelSafeInput.getServiceData();
                this.financialValue.object = <IFeelSafeInput>this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.FEEL_SAFE;
                this.financialValue.supplyTypeDesc = 'FEEL SAFE';
                this.financialValue.supplyElemCode = this.financialFeelSafe.FEEL_SAFE;
                this.financialValue.supplyElemDesc = 'FEEL SAFE';
                break;
            }
            case this.financialAssistenza.SERVICE_PLUS_EXCELLENT: {
                this.financialValue = this.plusExcellentInput.getServiceData();
                this.financialValue.object = /*<any>*/this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.ASSISTENZA;
                this.financialValue.supplyTypeDesc = 'SERVIZIO ASSISTENZA';
                this.financialValue.supplyElemCode = this.financialAssistenza.SERVICE_PLUS_EXCELLENT;
                this.financialValue.supplyElemDesc = 'SERVICE PLUS EXCELLENT';
                break;
            }
            case this.financialAssistenza.SERVICE_PLUS_CONFORT: {
                this.financialValue = this.plusConfortInput.getServiceData();
                this.financialValue.object = /*<any>*/this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.ASSISTENZA;
                this.financialValue.supplyTypeDesc = 'SERVIZIO ASSISTENZA';
                this.financialValue.supplyElemCode = this.financialAssistenza.SERVICE_PLUS_CONFORT;
                this.financialValue.supplyElemDesc = 'SERVICE PLUS CONFORT';
                break;
            }
            case this.financialAssistenza.SERVICE_PLUS_COMPACT: {
                this.financialValue = this.plusCompactInput.getServiceData();
                this.financialValue.object = /*<any>*/this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.ASSISTENZA;
                this.financialValue.supplyTypeDesc = 'SERVIZIO ASSISTENZA';
                this.financialValue.supplyElemCode = this.financialAssistenza.SERVICE_PLUS_COMPACT;
                this.financialValue.supplyElemDesc = 'SERVICE PLUS COMPACT';
                break;
            }

            case this.financialAssistenza.SERVICE_PLUS_ADVANCE: {
                this.financialValue = this.plusAdvanceInput.getServiceData();
                this.financialValue.object = /*<any>*/this.financialValue.object;
                this.financialValue.supplyTypeCode = this.financialServices.ASSISTENZA;
                this.financialValue.supplyTypeDesc = 'SERVIZIO ASSISTENZA';
                this.financialValue.supplyElemCode = this.financialAssistenza.SERVICE_PLUS_ADVANCE;
                this.financialValue.supplyElemDesc = 'SERVICE PLUS ADVANCE';
                break;
            }

            default: {
                this._logger.logWarn('FinancialInsurance', 'ValidateForm', 'TI TROVI IN DEFAULT');
                this.financialValue.code = 0;
                this.financialValue.message = 'DEFAULT CASE';
                break;
            }
        }

        switch (this.financialValue.code) {
            case AppSettings.HTTP_ERROR:
                // GESTIRE IL CASO IN CUI I DATI DELLA FORM DEL SINGOLO COMPONENT NON E' VALIDA
                this._notification.sendNotification(new NotificationMessage(
                    this.translate.instant('EMPTY'), NOTIFICATION_TYPE.WARN
                ));

                this._logger.logInfo('FinancialInsurance', 'ValidateForm', 'CASE 500');
                break;

            case AppSettings.HTTP_OK:
                if (this._utils.isVoid(this.commonInputValue)) {
                    // Caso in cui i campi di input common non sono stati riempiti
                    this._notification.sendNotification(new NotificationMessage(this.translate.instant('EMPTYS'), NOTIFICATION_TYPE.WARN));
                } else {
                    this.financialValue.object.common = {
                        noleggioOtaxi: this.commonInputValue.noleggioOtaxi,
                        valoreAssicurativo: this.commonInputValue.valoreAssicurativo,
                        recuperoIva: this.commonInputValue.recuperoIva,
                        deroga: this.commonInputValue.deroga
                    };

                    // Update dei campi comuni di tutti i financialValue presenti nella lista
                    for (let elem of this.financialValueList) elem.object.common = this.financialValue.object.common;

                    serviceInn.retrieveVat = !this._utils.isUndefided(this.financialValue.object.common.recuperoIva) ? this.financialValue.object.common.recuperoIva : false;
                    serviceInn.hasFireTheftInsurance = !this._utils.isUndefided(this.financialValue.object.hasFireTheftInsurance) ? this.financialValue.object.hasFireTheftInsurance : false;
                    serviceInn.hasSatellite = !this._utils.isUndefided(this.financialValue.object.satellitare) ? this.financialValue.object.satellitare : false;
                    serviceInn.calculationAllowed = !this._utils.isUndefided(this.financialValue.object.calculationAllowed) ? this.financialValue.object.calculationAllowed : false;
                    serviceInn.forcedToPC = !this._utils.isUndefided(this.financialValue.object.forcedToPC) ? this.financialValue.object.forcedToPC : false;
                    serviceInn.clazz = !this._utils.isUndefided(this.financialValue.clazz) ? this.financialValue.clazz : '';
                    serviceInn.annualDistance = !this._utils.isUndefided(this.financialValue.object.annualDistance) ? this.financialValue.object.annualDistance : '';
                    serviceInn.customerProvinceCode = !this._utils.isUndefided(this.financialValue.object.provincia) ? this.financialValue.object.provincia : '';

                    if (this.financialValue.object.durataFeelNew != undefined) {
                        let rtiC = new CodeObj(this.financialValue.object.durataFeelNew.code);
                        serviceInn.rtiCoverage = [];
                        if (rtiC.code != AppSettings.DEFAULT_FIRE_COVERAGE) {
                            serviceInn.rtiCoverage.push(rtiC);
                        }
                    }

                    if (this.financialValue.object.durataVNFurtoIncendio != undefined) {
                        let ftc = new CodeObj(this.financialValue.object.durataVNFurtoIncendio.code);
                        serviceInn.fireTheftCoverage = [];
                        if (ftc.code != AppSettings.DEFAULT_RTI_COVERAGE) {
                            serviceInn.fireTheftCoverage.push(ftc);
                        }
                    }

                    if (this.financialValue.object.estensioneGaranziaProposta != undefined) {
                        serviceInn.estensioneGaranziaProposta = [];
                        serviceInn.estensioneGaranziaProposta[0] = this.financialValue.object.estensioneGaranziaProposta;
                    }

                    // Chiamata al servizio BE che ritorna il prezzo del costo finanziario passato in input
                    this.getFinancialService(this._utils.formatOBject4Output(serviceInn));

                }

                this._logger.logInfo('FinancialInsurance', 'ValidateForm', 'CASE 200');
                break;
            default:
                // CASO DEFAULT
                this._logger.logInfo('FinancialInsurance', 'ValidateForm', 'DEFAULT CASE / codice non presente nella struttura nostra');
                break;
        }
    }

    /**
     * Cancella dalla lista dei serivizi assicurativi quello cliccato
     * @param item
     */
    removeInsuranceService(item: FinancialValue) {
        this.inDelete = true;

        let finElemRemoveIndex = this.financialValueList.findIndex(elem => {
            return elem.supplyElemCode == item.supplyElemCode;
        });


        this.setPrice(item.code, undefined);


        // Rimuove dalle liste l'oggetto selezionato
        if (finElemRemoveIndex != undefined) {
            this.financialValueList.splice(finElemRemoveIndex, 1);
            this.tabTypeActiveId = null;
            this._proposalService.financialServicesList = this.financialValueList;
            this.addService.emit(this.financialValueList);

            // Chiama la funzione del figlio per rimuovere il prezzo visualizzato vicino al nome
            this.financialInsuranceType.checkIfSelected();
            this.cleanFields();
            this._logger.logInfo('FinancialInsurance', 'removeInsuranceService', 'financialValueList', this.financialValueList);
        }
    }

    /**
     * Dato un codice SupplyItem e un prezzo, lo setta nella lista dei servizi finanziari
     * @param supplyCode
     * @param fullCost
     */
    public setPrice(supplyCode, fullCost: IServiceCost) {
        for (let y of this.financialTypeList) {
            for (let x of y.serviceEntities) {
                if (supplyCode === x.supplyElementCode) {
                    x.supplyAmount = !!fullCost ? fullCost.supplyAmount : null ;
                    x.customerServiceCost = !!fullCost ? fullCost.customerServiceCost : null;
                }
            }
        }

        this.serviceEntitiesList.emit(this.financialTypeList);
    }


    /**
     * Aggiorna la lista con i prezzi aggiornati
     * @param {FinancialServices[]} financialServices
     */
    public updateFinancialValueList(financialServices: FinancialServices[]) {
        // prendo il prezzo dal be dal piano finanziario e lo assegno alla lista mia. Se sto in DELETE non entro in questo flusso
        if (!this.inDelete) {
            if (financialServices != undefined) {
                for (let elem of financialServices) {
                    for (let x of elem.serviceEntities) {
                        if (x.supplyAmount > -1 && !!this.financialValue && this.financialValue.supplyElemCode === x.supplyElementCode) {
                            this.financialValue.price = x.supplyAmount;
                            this.financialValue.isSelected = true;
                        }
                    }
                }
            }

            let finValueEdit = null;
            if (this.financialValueList.length > 0 && !!this.financialValue) {
                finValueEdit = this.financialValueList.find(elem => {
                    return elem.supplyElemCode == this.financialValue.supplyElemCode;
                });
            }


            if (!!finValueEdit) {
                // FLUSSO EDIT
                finValueEdit.object = this.financialValue.object;
                finValueEdit.price = this.financialValue.price;
            } else if (this.financialValue != undefined) {
                // FLUSSO INSERT
                this.financialValueList.push(this.financialValue);
            }
            this._proposalService.financialServicesList = this.financialValueList;

        }
        this.inDelete = false;
        // Eseguo la clean dei dati
        this.cleanFields();

    }


    /**
     * Aggiunge alla lista financialTypeListValid gli elementi in base al valore di uso noleggio o taxi
     * @param {boolean} ev
     */
    isTaxi(ev: boolean = false) {
        this.financialTypeListValid = [];
        this.financialValueList = [];
        this.cleanFields();

        if (ev) {
            for (let elem of this.financialTypeList) {
                for (let code of FinancialConstant.LIST_OF_USE_TAXI_AVAILABLE) {
                    if (elem.supplyTypeCode == code) {
                        this.financialTypeListValid.push(elem);
                    }
                }
            }
        } else {
            this.financialTypeListValid = this.financialTypeList;
        }
    }

    public findAllFinancialValueByFather(supplyTypeCode: string, financialValueList: FinancialValue[]): FinancialValue[] {
        let financialValueOutput: FinancialValue[] = [];
        for (let obj of financialValueList) {
            if (obj.supplyTypeCode === supplyTypeCode) financialValueOutput.push(obj);
        }

        return financialValueOutput;
    }


    /**
     * Funzione per resettare i campi
     */
    cleanFields() {
        // TODO: Salvo i dati in proposal Service ma non nel persistence

        this._proposalService.financialServicesList = this.financialValueList;
        // this._proposalService.save(ProposalsConstant.stateCommercial, "");
        this.thirdColumnEnable = false;

        this.secondColumnEnable = false;
        this.buttonsEnabled = false;
        this.financialValue = null;
        this.financialTypeSelected = null;
        this.tabTypeActiveId = null;
        this.tabElemActiveId = null;

    }

    /**
     * Funzione per ressetare tutti i dati presenti nella sezione servizi
     * ATTENZIONE: Svuota anche le liste con i dati
     */
    cleanAllFields() {
        this.financialValueList = [];
        this.insuranceAccordionOpen = false;
        this.cleanFields();
    }

    /**
     * Funzione per controllare se il SupplyerCode è compatibile con la proposta
     * e con la lista degli altri supplyerCode gia controllati
     */
    public getCheckCompatibility(): void {
        // Recupero la lista di "code" già presenti da passare alla chiamata BE
        let supplyElemCodeList: string[] = [];
        for (let item of this.financialValueList) supplyElemCodeList.push(item.supplyElemCode);

        this.checkCompatibilityReqParams.supplyElemCodes = this.financialValue.supplyElemCode;
        this.checkCompatibilityReqParams.listSupplyElemCodes = supplyElemCodeList;
        this._logger.logInfo('FinancialInsurance', 'getCheckCompatibility', 'parmas', this.checkCompatibilityReqParams);

        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_FINANCIAL_CHECK_COMPATIBILITY, REQUEST_PRIORITY.HIGH);
        this._financialService.getCheckCompatibility(this.checkCompatibilityReqParams, false).pipe(takeUntil(this.destroy$)).subscribe(
            response => {
                this._logger.logInfo('FinancialCampaignComponent', 'getCheckCompatibility', 'load response ', response);
                this._logger.logInfo('FinancialCampaignComponent', 'getCheckCompatibility', '_msgCode ', response.object.messageType);

                switch (response.code) {
                    case AppSettings.HTTP_OK:
                        // Caso in cui è possibile aggiungere il servizio finanziario
                        this.financialValue.code = 200;
                        this._logger.logWarn('FinancialInsurance', 'getCheckCompatibility', 'CASE 200');
                        break;
                    case AppSettings.HTTP_OK_BLOCK:
                        // Caso modifica o bloccante
                        this.financialValue.code = supplyElemCodeList.indexOf(this.financialValue.supplyElemCode) > -1 ? 200 : 211;
                        this._logger.logWarn('FinancialInsurance', 'getCheckCompatibility', 'CASE 210');
                        break;
                    default:
                        let message = new NotificationMessage('ATTENTION', NOTIFICATION_TYPE.ERROR, '');
                        this._notification.sendNotification(message);

                        this._logger.logError(this.constructor.name, 'getCheckCompatibility', 'response code', response.code);
                        this._logger.logWarn('FinancialInsurance', 'getCheckCompatibility', 'CASE default');

                        break;
                }
                this.getInputField();
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_CHECK_COMPATIBILITY, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            }, resp => {
                this._logger.logError('FinancialInsuranceComponent', 'getCheckCompatibility', 'load error ', resp);
                let message = new NotificationMessage('ATTENTION', NOTIFICATION_TYPE.ERROR, 'GET_FINANCIAL_CHECK_COMPATIBILITY_500');
                this._notification.sendNotification(message);

                // const error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_CHECK_COMPATIBILITY, resp.code);
                }, AppSettings.TIMEOUT_SPINNER);
            });
    }

    /**
     * Funzione che ritorna nella variabile " financialConfigResponse " la lista di valori per popolare
     * la form relativa al sotto elemento selezionato
     */
    public getFinancialConfigService(): void {

        let params = new FinancialConfigServiceBodyParameters();

        params.assetType = this.proposal.product.model.assetType;
        params.brandId = this.proposal.product.model.brandId;
        params.contractLength = this.contractInfo.contractLength ? this.contractInfo.contractLength : 0;
        params.distance = this.contractInfo.distance ? this.contractInfo.distance : 0;
        params.financialProductCode = this.contractInfo.finProductCode ? this.contractInfo.finProductCode : '';
        params.financiedAmount = this.contractInfo.financiedAmount ? this.contractInfo.financiedAmount : 0;
        params.frequency = this.contractInfo.frequency ? this.contractInfo.frequency : 1;
        params.tan = this.contractInfo.tan ? this.contractInfo.tan : 0;
        params.netCost = this.contractInfo.netCost ? this.contractInfo.netCost : 0;
        params.vatPercentage = this.contractInfo.vatPercentage ? this.contractInfo.vatPercentage : 0;
        params.service = FinancialConfigServiceInnerFactory.getInstance(this.financialElemSelected);


        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_FINANCIAL_CONFIG_SERVICE, REQUEST_PRIORITY.HIGH);
        this._financialService.getFinancialConfigService(params, false).pipe(takeUntil(this.destroy$)).subscribe(
            response => {

                // Oggetto che passo ai componente figli come input per i valori della form
                this.financialConfigResponse = response.object;
                // Abilito la terza colonna e i bottoni
                this.thirdColumnEnable = true;
                this.buttonsEnabled = true;

                this._logger.logInfo('FinancialCampaignComponent', 'getFinancialConfigService', 'load finacialConfigResponse 1', this.financialConfigResponse);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_CONFIG_SERVICE, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            }, resp => {
                let message = new NotificationMessage('ATTENTION', NOTIFICATION_TYPE.ERROR, 'GET_FINANCIAL_CONFIG_SERVICE_500');
                this._notification.sendNotification(message);

                this._logger.logError('FinancialCampaignComponent', 'getFinancialConfigService', 'load error ', resp);
                // const error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_CONFIG_SERVICE, resp.code);
                }, AppSettings.TIMEOUT_SPINNER);
            });
    }

    /**
     * Recupera il prezzo del servizio finanziario
     * @param {FinancialConfigServiceInner} serviceInn
     */
    public getFinancialService(serviceInn: FinancialConfigServiceInner): void {

        let params = new FinancialServiceBodyParameters();
        let contractLength = this.contractInfo.contractLength ? this.contractInfo.contractLength : 0;
        let distance = this.contractInfo.distance ? this.contractInfo.distance : 0;
        let financiedAmount = this.contractInfo.financiedAmount ? this.contractInfo.financiedAmount : 0;
        let frequency = this.contractInfo.frequency ? this.contractInfo.frequency : 0;
        let tan = this.contractInfo.tan ? this.contractInfo.tan : 0;
        let finProductCode = this.contractInfo.finProductCode ? this.contractInfo.finProductCode : '';
        let vatPercentage = this.contractInfo.vatPercentage ? this.contractInfo.vatPercentage : 0;
        let netCost = this.contractInfo.netCost ? this.contractInfo.netCost : 0;

        params.assetType = this.proposal.product.model.assetType;
        params.brandId = this.proposal.product.model.brandId;
        params.service = serviceInn;
        params.contractLength = contractLength;
        params.distance = distance;
        params.financiedAmount = financiedAmount;
        params.frequency = frequency;
        params.tan = tan;
        params.finProductCode = finProductCode;
        params.vatPercentage = vatPercentage;
        params.netCost = netCost;

        this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_FINANCIAL_SERVICE, REQUEST_PRIORITY.HIGH);
        this._financialService.getFinancialService(this._utils.formatOBject4Output(params), false).pipe(takeUntil(this.destroy$)).subscribe(
            response => {

                let cost: IServiceCost = response.object;
                this._logger.logInfo('FinancialInsurance', 'getFinancialService', 'cost', cost);

                this._logger.logInfo('FinancialCampaignComponent', 'getFinancialService', 'load FinancialServiceR', response.object);
                if (cost == undefined) cost = null;
                this._logger.logInfo('FinancialCampaignComponent', 'getFinancialService', 'load FinancialServiceX', response.object);

                // Entro nel flusso di insert o modifica solo se torna il prezzo dalla chiamata getFinancialService
                if (!!cost) {

                    this.setPrice(this.financialValue.supplyElemCode, cost);
                    this.financialValue.price = cost.supplyAmount;
                    this.financialValue.customerServiceCost = cost.customerServiceCost;

                    this.addService.emit(this.financialValueList);

                    this._logger.logInfo('FinancialInsurance', 'getFinancialService', 'financialValueList', this.financialValueList);

                }


                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_SERVICE, response.code);
                }, AppSettings.TIMEOUT_SPINNER);
            }, resp => {
                let message = new NotificationMessage('ATTENTION', NOTIFICATION_TYPE.ERROR, 'GET_FINANCIAL_SERVICE_500');
                this._notification.sendNotification(message);
                this._logger.logError('FinancialCampaignComponent', 'getFinancialService', 'load error ', resp);
                // const error = this._utils.getErrorResponse(resp);
                setTimeout(() => {
                    this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_SERVICE, resp.code);
                }, AppSettings.TIMEOUT_SPINNER);
            });
    }


    /**
     * Funzione che assegna a "financialInsuranceList" una lista di Servizi finanziari
     */
    public getFinancialInsuranceList(): void {

        let campaignCode: string;
        let used = !this.selectedFinancialQueue.used;
        let contractLength: number;

        contractLength = !this._utils.isVoid(this.selectedFinancialQueue.contractLength) ? this.selectedFinancialQueue.contractLength : 0;

        //if (!this._utils.isVoid(this.campaignSelected.campaignsDetailList)) campaignCode = this.campaignSelected.campaignsDetailList[0].code;
        // if (!this._utils.isVoid(this.proposal.commercial.secondhandData)) used = true;


        let params = new FinancialServicesReqParams(
            this.selectedFinancialQueue.assetType,
            this.selectedFinancialQueue.financialProductCode,
            this.selectedFinancialQueue.brandId,
            contractLength,
            this.selectedFinancialQueue.financialProductCode,
            used,
            this.selectedFinancialQueue.campaignCode
        );

        this._logger.logInfo('FinancialInsurance', 'getFinancialInsuranceList', 'parametri reali', params);

        let paramsMock = new FinancialServicesReqParams('MBA', '16800612001', 'MBF', 36, 'FB', false);

        if (this._mock) {
            this._financialService.getFinancialInsuranceList(paramsMock, false).pipe(takeUntil(this.destroy$)).subscribe(
                response => {
                    this._logger.logInfo(this.constructor.name, 'getFinancialInsuranceList1', 'load response ', response);
                    this.financialTypeList = response.object;
                    // this.financialTypeListValid = this.financialTypeList;

                    setTimeout(() => {
                    }, AppSettings.TIMEOUT_SPINNER);
                }, resp => {
                    this._logger.logInfo('FinancialCampaignComponent', 'loadFinancialCampaignsList', 'load error ', resp);
                    // const error = this._utils.getErrorResponse(resp);
                    setTimeout(() => {
                    }, AppSettings.TIMEOUT_SPINNER);
                });
        } else {
            this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_FINANCIAL_LIST, REQUEST_PRIORITY.HIGH);
            this._financialService.getFinancialInsuranceList(params, false).pipe(takeUntil(this.destroy$)).subscribe(
                response => {
                    this._logger.logInfo('FinancialCampaignComponent', 'getFinancialInsuranceList1', 'load response ', response);
                    this.financialTypeList = response.object;


                    this.serviceEntitiesList.emit(response.object);

                    setTimeout(() => {
                        this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_LIST, response.code);
                    }, AppSettings.TIMEOUT_SPINNER);
                }, resp => {
                    let message = new NotificationMessage('ATTENTION', NOTIFICATION_TYPE.ERROR, 'GET_FINANCIAL_INSURANCE_LIST_500');
                    this._notification.sendNotification(message);
                    this._logger.logError('FinancialCampaignComponent', 'loadFinancialCampaignsList', 'load error ', resp);
                    // const error = this._utils.getErrorResponse(resp);
                    setTimeout(() => {
                        this._requestManagerService.handleRequest(RequestManagerConstant.GET_FINANCIAL_LIST, resp.code);
                    }, AppSettings.TIMEOUT_SPINNER);
                });
        }

    }


}
