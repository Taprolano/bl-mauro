import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeelSafeInputComponent} from './feel-safe-input.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";

describe('FeelSafeInputComponent', () => {
    let component: FeelSafeInputComponent;
    let fixture: ComponentFixture<FeelSafeInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FeelSafeInputComponent],
            imports: [
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeelSafeInputComponent);
        component = fixture.componentInstance;

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', {common: null}, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.validateForm();
    });

    it('getServiceData isValid true test', () => {
        spyOn(component, 'validateForm').and.returnValue(true);
        component.getServiceData();
    });

});
