import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FinancialInsuranceTypeComponent} from './financial-insurance-type.component';
import {CUSTOM_ELEMENTS_SCHEMA, Injectable} from '@angular/core';
import {SvgIconComponent} from '../../../../../../../shared/components/svg-icon/svg-icon.component';
import {FinancialValue} from '../../../../../../../data-model/class/FinancialValue';
import {FinancialServices} from '../../../../../../../data-model/class/FinancialServices';
import {Attachment} from '../../../../../../../data-model/class/Attachment';

@Injectable({
    providedIn: 'root',
})
export class AbstractModalServiceMock {

    constructor(private _abstractModalService: AbstractModalServiceMock) {
    }

}

describe('FinancialInsuranceTypeComponent', () => {
    let component: FinancialInsuranceTypeComponent;
    let fixture: ComponentFixture<FinancialInsuranceTypeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FinancialInsuranceTypeComponent, SvgIconComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FinancialInsuranceTypeComponent);
        component = fixture.componentInstance;

        let mockFinancialValue: FinancialValue = new FinancialValue(' ', ' ', 0, ' ', null, ' ', ' ', ' ', ' ', ' ');
        let mockFinValList: FinancialValue[] = [];
        mockFinValList.push(mockFinancialValue);
        component.financialValueInputList = mockFinValList;

        let mockFinServ: FinancialServices = new FinancialServices();
        let mockFinAtt: Attachment = new Attachment(' ', ' ', ' ');
        let mockAttachments: Attachment[] = [];
        mockAttachments.push(mockFinAtt);

        mockFinServ.supplyTypeCode = 'test';
        component.insuranceType = mockFinServ;
        component.insuranceType.attachments = mockAttachments;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('ngOnInit test', () => {
        component.ngOnInit();
    });

    it('loadInsuranceType test', () => {
        spyOn(component.financialIdEvent, 'emit').and.returnValue(false);
        component.loadInsuranceType();
    });


    it('checkIfSelected test', () => {
        let mockObj: Object = {
            supplyTypeCode: 'test'
        };

        spyOn(component.financialValueInputList, 'find').and.returnValue(mockObj);
        component.checkIfSelected();
    });

    it('loadDocument test', () => {
        component.loadDocument();
    });

    it('loadDocument subs test', () => {
        component.loadDocument();
    });

    it('deleteType test', () => {
        let mockObj: Object = {
            supplyTypeCode: 'test'
        };

        spyOn(component.financialValueInputList, 'find').and.returnValue(mockObj);
        spyOn(component.deleteTypeEvent, 'emit').and.returnValue(false);
        component.deleteType();
    });

});
