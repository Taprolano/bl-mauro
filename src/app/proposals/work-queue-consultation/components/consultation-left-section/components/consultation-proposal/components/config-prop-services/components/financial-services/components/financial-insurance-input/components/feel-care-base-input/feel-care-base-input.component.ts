import {Component, Input, OnInit} from '@angular/core';
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";

@Component({
    selector: 'app-feel-care-base-input',
    templateUrl: './feel-care-base-input.component.html',
    styleUrls: ['./feel-care-base-input.component.scss']
})
export class FeelCareBaseInputComponent implements OnInit {

    @Input()
    financialValue: FinancialValue;

    submitted = false;

    constructor(private _logger: LoggerService) {
    }

    ngOnInit() {
    }

    validateForm(): boolean {
        this.submitted = true;
        return true;
    }

    getServiceData(): FinancialValue {


        if (this.validateForm) {
            // this.financialValue.code = 200;
            this.financialValue.message = 'La form non ha campi richiesti';
            this.financialValue.object = {
                common: null
            };

            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}
