import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ServicePlusAdvanceInputComponent} from './service-plus-advance-input.component';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {SvgIconComponent} from "../../../../../../../../../../../../../../shared/components/svg-icon/svg-icon.component";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {FireTheftCoverage} from "../../../../../../../../../../../../../../data-model/class/FireTheftCoverage";
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('ServicePlusAdvanceInputComponent', () => {
    let component: ServicePlusAdvanceInputComponent;
    let fixture: ComponentFixture<ServicePlusAdvanceInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ServicePlusAdvanceInputComponent,
            SvgIconComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ServicePlusAdvanceInputComponent);
        component = fixture.componentInstance;

        let mockObj = new Object();
        mockObj = {
            kmTotali: [],
            estensione: []
        };

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', mockObj, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        let mockSerInner: FinancialConfigServiceResponseInner = new FinancialConfigServiceResponseInner();
        let mockFireTheftCoverage: FireTheftCoverage = new FireTheftCoverage(' ', '', 0);
        let mockFireThArray: FireTheftCoverage[] = [];
        mockFireThArray.push(mockFireTheftCoverage);
        mockSerInner.fireTheftCoverage = mockFireThArray;
        component.service = mockSerInner;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.validateForm();
    });

    it('validateForm formFalse test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(true),
        });
        component.serviceAdvanceForm = mockFormGroup;

        component.validateForm();
    });

    it('getServiceData isValid true test', () => {
        spyOn(component, 'validateForm').and.returnValue(true);
        component.getServiceData();
    });

    it('getServiceData isValid false test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(false),
        });
        component.serviceAdvanceForm = mockFormGroup;

        component.getServiceData();
    });

});
