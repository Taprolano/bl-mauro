import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeelLifeInputComponent} from './feel-life-input.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";

describe('FeelLifeInputComponent', () => {
    let component: FeelLifeInputComponent;
    let fixture: ComponentFixture<FeelLifeInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FeelLifeInputComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ],
            providers: [FormBuilder]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeelLifeInputComponent);
        component = fixture.componentInstance;

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', {common: null}, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(false),
        });
        component.feelLifeForm = mockFormGroup;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.validateForm();
    });

    it('getServiceData valid false test', () => {
        component.getServiceData();
    });

    it('getServiceData valid true test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            valid: builder.control(true),
        });
        component.feelLifeForm = mockFormGroup;

        component.getServiceData();
    });

});
