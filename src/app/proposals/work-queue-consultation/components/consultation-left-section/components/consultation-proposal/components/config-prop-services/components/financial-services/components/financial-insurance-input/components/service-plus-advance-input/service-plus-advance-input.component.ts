import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AppSettings} from "../../../../../../../../../../../../../../../AppSettings";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-service-plus-advance-input',
    templateUrl: './service-plus-advance-input.component.html',
    styleUrls: ['./service-plus-advance-input.component.scss']
})
export class ServicePlusAdvanceInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    serviceAdvanceForm: FormGroup;
    serviceAdvanceInput: any;
    private submitted = false;
    private isValid: boolean;

    estensioneGaranzia;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.submitted = false;
        this.serviceAdvanceForm = this._formBuilder.group({
            estensioneGaranziaProposta: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.estensioneGaranziaProposta)) this.estensioneGaranzia = this.financialValue.object.estensioneGaranziaProposta;
        } else {
            this.estensioneGaranzia = 'default';
        }

    }

    get f() {
        return this.serviceAdvanceForm.controls;
    }

    validateForm(): boolean {
        this.submitted = true;

        if (this._utils.isVoid(this.serviceAdvanceForm.controls['estensioneGaranziaProposta'].value) || this.serviceAdvanceForm.controls['estensioneGaranziaProposta'].value == 'default') {
            this._utils.addCssClassByIds('a-input--error', 'estensioneGaranziaProposta');
            this.isValid = false;
        } else {
            this.isValid = true;
        }

        return this.isValid;
    }

    getServiceData(): FinancialValue {
        this.validateForm();
        this.financialValue.code = AppSettings.HTTP_OK;

        if (this.isValid === true) {
            this.estensioneGaranzia = null;
            this.serviceAdvanceInput = {
                estensioneGaranziaProposta: this.serviceAdvanceForm.value.estensioneGaranziaProposta,
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.serviceAdvanceInput;

            this._logger.logInfo('ServicePlusAdvanceInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = AppSettings.HTTP_ERROR;
            this._logger.logInfo('ServicePlusAdvanceInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }

    removeError(id: string) {
        this._utils.removeCssClassByIds('a-input--error', id);
    }
}
