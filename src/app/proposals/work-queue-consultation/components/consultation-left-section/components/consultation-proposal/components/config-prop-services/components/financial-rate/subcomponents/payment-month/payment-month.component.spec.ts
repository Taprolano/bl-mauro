import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaymentMonthComponent} from './payment-month.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {AppComponent} from '../../../../../../../app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {createTranslateLoader} from '../../../../../../../app.module';
import {environment} from '../../../../../../../../environments/environment';
import {AuthGuard} from '../../../../../../../shared';
import {BrowserModule} from '@angular/platform-browser';
import {ServiceWorkerModule} from '@angular/service-worker';
import {ProposalsModule} from '../../../../../../proposals.module';
import {SharedModule} from '../../../../../../../shared/shared.module';
import {MbzDashboardModule} from '../../../../../../../mbz-dashboard/mbz-dashboard.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {DirectAccessGuard} from '../../../../../../../shared/guard/direct-access/direct-access.guard';
import {CUSTOM_ELEMENTS_SCHEMA, Injectable} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from '../../../../../../../app-routing.module';
import {IPaymentMonth} from '../../data-model/IPaymentYear';
import {MockDataService} from '../../../../../../../shared/services/mock-data.service';
import {Observable, of, Subject} from 'rxjs';
import {BeResponse} from '../../../../../../../data-model/class/BeResponse';
import {FinancialCampaign} from '../../../../../../../data-model/class/FinancialCampaign';
import {BeFinancialService} from '../../../../../../services/be-financial.service';
import {AbstractModalService} from '../../../../../../../shared/components/abstract-modal/abstract-modal.service';
import {map} from 'rxjs/operators';
import {PaginatedObject} from '../../../../../../../data-model/class/PaginatedObject';
import {IOptional} from '../../../../../../../data-model/interface/IOptional';
import {NOTIFICATION_TYPE, NotificationMessage} from '../../../../../../../shared/components/notification/NotificationMessage';

describe('PaymentMonthComponent', () => {
    let component: PaymentMonthComponent;
    let fixture: ComponentFixture<PaymentMonthComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaymentMonthComponent);
        component = fixture.componentInstance;

        let mockPaymMonth: IPaymentMonth = {
            locked: false,
            month: 'JAN',
            year: 0,
            service: 0,
            asset: 0,
            assetPercentage: 0,
        };

        component.paymentMonth = mockPaymMonth;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('blurf test', () => {
        component.blurf();
    });

    it('createForm test', () => {
        component.createForm();
    });

    it('paymentDetail test', () => {
        component.paymentDetail();
    });

});
