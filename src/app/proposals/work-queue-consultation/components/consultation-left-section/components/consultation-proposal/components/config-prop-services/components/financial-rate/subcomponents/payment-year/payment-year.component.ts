import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IPaymentMonth, IPaymentYear, Months} from "../../data-model/IPaymentYear";
import {PaymentDetailComponent} from "../payment-detail/payment-detail.component";
import {AbstractModalService} from "../../../../../../../../../../../../shared/components/abstract-modal/abstract-modal.service";
import {Subject} from "rxjs/index";
import {takeUntil} from "rxjs/internal/operators";
import {ProposalService} from "../../../../../../../../../../../../shared/services/proposal.service";


const months = [];
const ipy: IPaymentYear = {year: null, paymentMonths: []};

@Component({
    selector: 'app-payment-year',
    templateUrl: './payment-year.component.html',
    styleUrls: ['./payment-year.component.scss']
})
export class PaymentYearComponent implements OnInit {

    @Input()
    paymentYear: IPaymentYear = null;

    @Input()
    enableEditing: boolean = false;

    @Output()
    paymentYearChange = new EventEmitter();

    @Output()
    changeTrigger = new EventEmitter();

    @Output()
    massiveChangeByMonthTrigger = new EventEmitter();

    months = Months;

    name = "pipppo";

    unsubscribe = new Subject<any>();

    constructor(
        private _abstractModalService: AbstractModalService,
        private _proposalService: ProposalService
    ) {
    }

    ngOnInit() {
        //assegna il proprio anno ai pm "figli", che altrimenti è null
        //la prima riga non ha alcun py e dunque nessun pm perché contiene le label dei mesi
        if (this.paymentYear && this.paymentYear.paymentMonths) this.paymentYear.paymentMonths.forEach(m => {
            if (m) m.year = this.paymentYear.year;
        });
    }

    massiveChangeByYear() {

        if (this.enableEditing) {
            let newpaymentMont: IPaymentMonth = {
                locked: false,
                year: this.paymentYear.year,
                month: "",
                service: this._proposalService.financialServicesAmmountInsurance,
                asset: null,
                assetPercentage: null,
                assistance: this._proposalService.financialServicesAmmountAssistance,
                managementFees: this._proposalService.financialManagementFees
            };

            let customObj = {
                component: PaymentDetailComponent,
                parameters: {paymentMonth: newpaymentMont}
            };

            let onSubmit = true;
            this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
            this._abstractModalService.captureOutcome.pipe(takeUntil(this._abstractModalService.unsubscribe)).subscribe(resp => {
                if (!!resp) {
                    if (resp.asset != null) {

                        let newPaymentMonths: IPaymentMonth[] = [];
                        for (let month of this.paymentYear.paymentMonths) {
                            if (month != null) {
                                month.locked = true;
                                month.asset = resp.asset;
                            }
                            newPaymentMonths.push(month);

                        }
                        this.paymentYear.paymentMonths = newPaymentMonths;
                    }

                    this.changeTrigger.next();
                }

            })
        }

    }

    massiveChangeByMonth(i) {

        if (this.enableEditing) {
            let newpaymentMonth: IPaymentMonth = {
                locked: false,
                year: null,
                month: this.months[i],
                service: this._proposalService.financialServicesAmmountInsurance,
                asset: null,
                assetPercentage: null,
                assistance: this._proposalService.financialServicesAmmountAssistance,
                managementFees: this._proposalService.financialManagementFees
            };

            let customObj = {
                component: PaymentDetailComponent,
                parameters: {paymentMonth: newpaymentMonth}
            };

            let onSubmit = true;
            this._abstractModalService.showModalCallback(onSubmit, this.unsubscribe, customObj);
            this._abstractModalService.captureOutcome.pipe(takeUntil(this._abstractModalService.unsubscribe)).subscribe(resp => {
                if (!!resp) {
                    if (resp.asset != null) {
                        this.massiveChangeByMonthTrigger.next(resp);
                    }
                }
            })
        }

    }


    updateMonth(paymentMonth: IPaymentMonth) {
        for (let pm of this.paymentYear.paymentMonths) {
            if (paymentMonth.month === pm.month) {
                //pm = paymentMonth;
                break
            }
        }
    }

    propagateChanging(event) {
        this.changeTrigger.next(event)
    }
}
