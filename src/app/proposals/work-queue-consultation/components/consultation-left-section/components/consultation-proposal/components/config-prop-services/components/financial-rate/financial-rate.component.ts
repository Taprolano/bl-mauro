import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConsultationFinancialQueue} from "../../../../../../../../../../data-model/class/Consultation/ConsultationFinancialQueue";
import {Rate} from "../../../../../../../../../../data-model/class/Consultation/Rate";
import {IPaymentMonth, IPaymentYear, Months} from "./data-model/IPaymentYear";

@Component({
    selector: 'app-financial-rate',
    templateUrl: './financial-rate.component.html',
    styleUrls: ['./financial-rate.component.scss']
})
export class FinancialRateComponent implements OnInit {
    /*@Input() rate: Rate[];
    @Input() isEditing: boolean;

    wrapperHeight = "200px";

    constructor() {
    }

    ngOnInit() {
        this.showContent = false;
    }*/

    showContent: boolean;

    @Input()
    paymentSchedule: IPaymentYear[] = [];

    @Input()
    enableEditing: boolean;

    @Output()
    paymentScheduleChange = new EventEmitter();

    @Output()
    changeTrigger = new EventEmitter();

    constructor() {
        this.showContent = false;
    }

    ngOnInit() {
        console.log(this.paymentSchedule);
    }

    massiveChangeByMonth(paymentMonth){

        let month = paymentMonth.month;
        let index = Months.indexOf(month);
        for (let py of this.paymentSchedule){
            if (py.paymentMonths[index] != null) {
                py.paymentMonths[index] = paymentMonth;
            }
        }
        this.changeTrigger.next();
    }
    updateYear(paymentYear: IPaymentYear){
        for (let py of this.paymentSchedule){
            if (paymentYear.year=== py.year) {
                //py = paymentYear;
                break;
            }
        }
    }

    /**
     * Set boolean to open or close variable rates option accordion
     * @param value
     */
    public setVariableRatesAccordionOpen(value: boolean) {
        this.showContent = value;
    }

    propagateChanging(event){
        this.changeTrigger.next(event);
    }

    rollBack(){
        this.changeTrigger.next("RESET");
    }

}
