import {FINANCIAL_SERVICES} from "../../../../../../../../data-model/enum/EFinancialState";
import {IPaymentYear} from "./components/financial-rate/data-model/IPaymentYear";
import {IOptional} from "../../../../../../../../data-model/interface/IOptional";
import {IIva} from "../../../../../../../../data-model/interface/IIva";
import {FinancialOptionals} from "../../../../../../../../data-model/class/FinancialOptionals";


export class FinancialConstant {
//TODO refactor in un enum se non deve avere metodi

//    const data = await import('../data.json')
    public static PRODUCT_FINANZIAMENTO_BALLOON = "FB";
    public static PRODUCT_FINANZIAMENTO_NORMALE = "FN";
    public static PRODUCT_LEASING_ARTIGIANCASSA = "LA";
    public static PRODUCT_LEASING_C = "LC";
    public static PRODUCT_LEASING = "LP";
    public static PRODUCT_LEASING_RILOCAZIONE = "LR";

    public static LIST_OF_CAMPAINS_4_PRELIMINARY_CHARGES = [
        FinancialConstant.PRODUCT_FINANZIAMENTO_BALLOON,
        FinancialConstant.PRODUCT_FINANZIAMENTO_NORMALE
    ];

    public static LIST_OF_USE_TAXI_AVAILABLE = [
        FINANCIAL_SERVICES.ASSISTENZA,
        FINANCIAL_SERVICES.FEEL_CARE_AXA
    ];

    public static FINANCIAL_ACTIONS_BUTTON_TYPE_SAVE_DEALER_OFFER = 'saveDealerOffer';
    public static FINANCIAL_ACTIONS_BUTTON_TYPE_PRINT_FRONT_COVER = 'printFrontCover';
    public static FINANCIAL_ACTIONS_BUTTON_TYPE_PRINT_SIMULATION = 'printSimulation';
    public static FINANCIAL_ACTIONS_BUTTON_TYPE_RESTORE_PROPOSAL = 'restoreProposal';


    /* Metodi statici */

    public static indexOfYear(year: number, paymentSchedule: IPaymentYear[]): number {

        //if (this.paymentSchedule.length === 0) return -1

        let i = 0;
        for (let py of paymentSchedule) {
            if (year === py.year) return i;
            i++;
        }

        return -1;
    }

    public static convertOptional4BE(opt: IOptional, iva: IIva): FinancialOptionals {
        return new FinancialOptionals(
            '',
            opt.description,
            opt.price,
            0,
            !!opt.premiumAdjustment ? opt.premiumAdjustment : 0,
            opt.price * iva.value,
            iva.value,
            iva.key
        );
    }

    public static checkMonth(year: number, month: number, totalMonth: number, startingMonth: number) {
        return (year * 12 + month < totalMonth) && (year > 0 || month >= startingMonth);
    }
}

