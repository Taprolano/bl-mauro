import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskSummaryDetailsComponent } from './risk-summary-details.component';

describe('RiskSummaryDetailsComponent', () => {
  let component: RiskSummaryDetailsComponent;
  let fixture: ComponentFixture<RiskSummaryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskSummaryDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskSummaryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
