import {Component, Input, OnInit} from '@angular/core';
import {ConsultationSubjectQueue} from "../../../../../../../../data-model/class/Consultation/ConsultationSubjectQueue";
import { BeWorkQueue } from 'src/app/proposals/services/be-work-queue.service';
import { PersistenceService } from '../../../../../../../../shared/services/persistence.service';
import { RequestManagerService, REQUEST_PRIORITY } from '../../../../../../../../shared/services/request-manager.service';
import { NotificationService } from '../../../../../../../../shared/components/notification/notification.service';
import { LoggerService } from '../../../../../../../../shared/services/logger.service';
import { UtilsService } from '../../../../../../../../shared/services/utils.service';
import { ProposalConsultationRequestParameters } from '../../../../../../../../data-model/class/ProposalConsultationRequestParameters';
import { RequestManagerConstant } from '../../../../../../../requestManagerConstant';
import { takeUntil } from 'rxjs/internal/operators';
import { Subject } from 'rxjs/index';
import { AppSettings } from '../../../../../../../../../AppSettings';
import { NotificationMessage, NOTIFICATION_TYPE } from '../../../../../../../../shared/components/notification/NotificationMessage';

@Component({
  selector: 'app-petitioner-proposal',
  templateUrl: './petitioner-proposal.component.html',
  styleUrls: ['./petitioner-proposal.component.scss']
})
export class PetitionerProposalComponent implements OnInit {

  subjectQueue: ConsultationSubjectQueue;

  /** @ignore */
  destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() isEditing: boolean;

  showContent: boolean;

  constructor(private _workQueueService: BeWorkQueue,
    private _persistence: PersistenceService,
    private _requestManagerService: RequestManagerService,
    private _notificationService: NotificationService,
    private _logger: LoggerService,
    private _utils: UtilsService) { }

  ngOnInit() {
    this.showContent = false;
    const tabPetitioner = 'soggetto';
    const objProposalSelect = this._persistence.get('SELECT_PROPOSAL');
    const params = new ProposalConsultationRequestParameters(objProposalSelect.idProposal, objProposalSelect.processInstanceID, tabPetitioner);
    this._requestManagerService.pushNewRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_PETITIONER, REQUEST_PRIORITY.HIGH);
    this._workQueueService.getConsultazioneProposal(params).pipe(takeUntil(this.destroy$))
    .subscribe( response => {
        this.subjectQueue = response.object;
        setTimeout(() => {
            this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_PETITIONER, response.code);
        }, AppSettings.TIMEOUT_SPINNER);
      }, respError => {
        this._notificationService.sendNotification(new NotificationMessage(`Non è stato possibile recuperare le informazioni sull'accordion soggetto richiedente`, NOTIFICATION_TYPE.ERROR));
        this._logger.logInfo(BeWorkQueue.name, 'getProposalCons', 'resp error', respError);
        const error = this._utils.getErrorResponse(respError);
        setTimeout(() => {
            this._requestManagerService.handleRequest(RequestManagerConstant.GET_PROPOSAL_CONSULTATION_PETITIONER, error.code, error.message);
        }, AppSettings.TIMEOUT_SPINNER);
      }
    );
  }

}
