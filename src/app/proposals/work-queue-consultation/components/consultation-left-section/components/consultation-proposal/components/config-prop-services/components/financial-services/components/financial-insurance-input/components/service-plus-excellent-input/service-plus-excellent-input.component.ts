import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AppSettings} from "../../../../../../../../../../../../../../../AppSettings";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-service-plus-excellent-input',
    templateUrl: './service-plus-excellent-input.component.html',
    styleUrls: ['./service-plus-excellent-input.component.scss']
})
export class ServicePlusExcellentInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;
    serviceExcellentForm: FormGroup;
    serviceExcellentInput: any;
    private submitted = false;
    private isValid: boolean;

    annualDistance;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.submitted = false;
        this.serviceExcellentForm = this._formBuilder.group({
            annualDistance: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.annualDistance)) this.annualDistance = this.financialValue.object.annualDistance;
        } else {
            this.annualDistance = 'default';
        }
    }

    get f() {
        return this.serviceExcellentForm.controls;
    }

    validateForm(): boolean {
        this.submitted = true;


        if (this._utils.isVoid(this.serviceExcellentForm.controls['annualDistance'].value) || this.serviceExcellentForm.controls['annualDistance'].value === 'default') {
            this._utils.addCssClassByIds('a-input--error', 'annualDistance');
            this.isValid = false;
        } else {
            this.isValid = true;
        }

        return this.isValid;
    }

    getServiceData(): FinancialValue {
        this.validateForm();
        this.financialValue.code = AppSettings.HTTP_OK;

        if (this.isValid === true) {
            this.serviceExcellentInput = {
                annualDistance: this.serviceExcellentForm.value.annualDistance,
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.serviceExcellentInput;

            this._logger.logInfo('ServicePlusExcellentInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = AppSettings.HTTP_ERROR;
            this._logger.logInfo('ServicePlusExcellentInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }

    removeError(id: string) {
        this._utils.removeCssClassByIds('a-input--error', id);
    }
}


