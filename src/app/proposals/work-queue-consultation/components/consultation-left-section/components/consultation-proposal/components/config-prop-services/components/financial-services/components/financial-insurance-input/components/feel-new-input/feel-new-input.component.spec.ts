import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeelNewInputComponent} from './feel-new-input.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../../../../../../../../../../../../../shared/shared.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MbzDashboardModule} from "../../../../../../../../../../../../../../mbz-dashboard/mbz-dashboard.module";
import {ProposalsModule} from "../../../../../../../../../../../../../proposals.module";
import {NgxSpinnerModule} from "ngx-spinner";
import {createTranslateLoader} from "../../../../../../../../../../../../../../app.module";
import {AppRoutingModule} from "../../../../../../../../../../../../../../app-routing.module";
import {ServiceWorkerModule} from "@angular/service-worker";
import {environment} from '../../../../../../../../../../../../../../../environments/environment';
import {AppComponent} from "../../../../../../../../../../../../../../app.component";
import {AuthGuard} from "../../../../../../../../../../../../../../shared/guard";
import {DirectAccessGuard} from "../../../../../../../../../../../../../../shared/guard/direct-access/direct-access.guard";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FireTheftCoverage} from "../../../../../../../../../../../../../../data-model/class/FireTheftCoverage";
import {RtiCoverage} from "../../../../../../../../../../../../../../data-model/class/RtiCoverage";

describe('FeelNewInputComponent', () => {
    let component: FeelNewInputComponent;
    let fixture: ComponentFixture<FeelNewInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({

            imports: [
                CommonModule,
                BrowserModule,
                BrowserAnimationsModule,
                SharedModule,
                TranslateModule,
                HttpClientModule,
                MbzDashboardModule,
                ProposalsModule,
                NgxSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                AppRoutingModule,
                ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
            ],
            declarations: [AppComponent],
            providers: [AuthGuard, DirectAccessGuard],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]


        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeelNewInputComponent);
        component = fixture.componentInstance;

        spyOn(component, 'updateriepilogoTotale').and.returnValue(false);

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            polizFurtoIncendio: builder.control([]),
            furtoIncendio: builder.control([]),
            satellitare: builder.control([]),
            provincia: builder.control([]),
            compagniaDiAssicurzionFI: builder.control([]),
            durataVNFurtoIncendio: builder.control(' '),
            durataFeelNew: builder.control(' '),
            riepilogoTotale: builder.control(0)
        });
        component.feelNewForm = mockFormGroup;

        let mockObj = new Object();
        mockObj = {
            durataVNFurtoIncendio: 0,
            furtoIncendio: false,
            polizFurtoIncendio: false,
            provincia: ' ',
            satellitare: false,
            durataFeelNew: ' '
        };

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', mockObj, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        let mockSerInner: FinancialConfigServiceResponseInner = new FinancialConfigServiceResponseInner();
        let mockFireTheftCoverage: FireTheftCoverage = new FireTheftCoverage(' ', 'Selezionare', 0);
        component['fi'] = mockFireTheftCoverage;

        let mockFireThArray: FireTheftCoverage[] = [];
        mockFireThArray.push(mockFireTheftCoverage);
        mockSerInner.fireTheftCoverage = mockFireThArray;

        let mockRtiCov: RtiCoverage = new RtiCoverage(' ', ' ', 0);
        let mockRtiCovList: RtiCoverage[] = [];
        mockRtiCovList.push(mockRtiCov);
        mockSerInner.rtiCoverage = mockRtiCovList;

        component.service = mockSerInner;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm formFalse test', () => {

        let builder: FormBuilder = new FormBuilder();
        let mockFireCov: FireTheftCoverage = new FireTheftCoverage(' ', 'Selezionare', 0);

        let mockFormGroup: FormGroup = builder.group({
            polizFurtoIncendio: builder.control([]),
            furtoIncendio: builder.control([]),
            satellitare: builder.control([]),
            provincia: builder.control([]),
            compagniaDiAssicurzionFI: builder.control([]),
            durataFeelNew: builder.control(' '),
            riepilogoTotale: builder.control(0),
            durataVNFurtoIncendio: builder.control(mockFireCov),
        });
        component.feelNewForm = mockFormGroup;

        component.validateForm();
    });

    it('getServiceData isValid true test', () => {
        spyOn(component, 'validateForm').and.returnValue(true);
        component.isValid = true;
        component.getServiceData();
    });

    it('getServiceData isValid false test', () => {
        spyOn(component, 'validateForm').and.returnValue(true);
        component.isValid = false;
        component.getServiceData();
    });

});
