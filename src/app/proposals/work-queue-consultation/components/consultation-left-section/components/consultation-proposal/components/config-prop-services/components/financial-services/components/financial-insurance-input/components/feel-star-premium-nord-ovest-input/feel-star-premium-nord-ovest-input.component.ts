import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {IPremiumNordOvest} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-feel-star-premium-nord-ovest-input',
    templateUrl: './feel-star-premium-nord-ovest-input.component.html',
    styleUrls: ['./feel-star-premium-nord-ovest-input.component.scss']
})
export class FeelStarPremiumNordOvestInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelStarNordOvestForm: FormGroup;
    submitted = false;
    feelStarNordOvesInput: IPremiumNordOvest;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.feelStarNordOvestForm = this._formBuilder.group({
            provincia: [],
            satellitare: [],
            privacy: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelStarNordOvestForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelStarNordOvestForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.privacy)) this.feelStarNordOvestForm.controls['privacy'].setValue(this.financialValue.object.privacy);
        }
    }

    valideteForm(): boolean {
        this.submitted = true;
        return this.feelStarNordOvestForm.valid;
    }


    getServiceData(): FinancialValue {

        if (this.valideteForm) {
            // this.financialValue.code = 200;

            this.feelStarNordOvesInput = {
                commonFeelStar: {
                    provincia: this.feelStarNordOvestForm.value.provincia,
                    satellitare: this.feelStarNordOvestForm.value.satellitare,
                    privacy: this.feelStarNordOvestForm.value.privacy
                },
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelStarNordOvesInput;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}
