import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalCommercialComponent } from './proposal-commercial.component';
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {environment} from "../../../../../../../../../environments/environment.svil";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {CommonModule} from "@angular/common";
import {NgxSpinnerModule} from "ngx-spinner";
import {MbzDashboardModule} from "../../../../../../../../mbz-dashboard/mbz-dashboard.module";
import {DirectAccessGuard} from "../../../../../../../../shared/guard/direct-access/direct-access.guard";
import {createTranslateLoader} from "../../../../../../../../app.module";
import {AppRoutingModule} from "../../../../../../../../app-routing.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {AuthGuard} from "../../../../../../../../shared/guard";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AppComponent} from "../../../../../../../../app.component";
import {SharedModule} from "../../../../../../../../shared/shared.module";
import {ProposalsModule} from "../../../../../../../proposals.module";

describe('ProposalCommercialComponent', () => {
  let component: ProposalCommercialComponent;
  let fixture: ComponentFixture<ProposalCommercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
            CommonModule,
            BrowserModule,
            BrowserAnimationsModule,
            SharedModule,
            TranslateModule,
            HttpClientModule,
            MbzDashboardModule,
            ProposalsModule,
            NgxSpinnerModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: createTranslateLoader,
                    deps: [HttpClient]
                }
            }),
            AppRoutingModule,
            ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        ],
        declarations: [AppComponent],
        providers: [AuthGuard, DirectAccessGuard],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalCommercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
