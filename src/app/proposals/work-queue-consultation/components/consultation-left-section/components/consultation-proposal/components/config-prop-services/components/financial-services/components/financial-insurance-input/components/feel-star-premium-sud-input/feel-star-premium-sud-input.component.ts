import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {IPremiumSud} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-feel-star-premium-sud-input',
    templateUrl: './feel-star-premium-sud-input.component.html',
    styleUrls: ['./feel-star-premium-sud-input.component.scss']
})
export class FeelStarPremiumSudInputComponent implements OnInit {
    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelStarSudForm: FormGroup;
    submitted = false;
    feelStarSudInput: IPremiumSud;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.feelStarSudForm = this._formBuilder.group({
            provincia: [],
            satellitare: [],
            privacy: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelStarSudForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelStarSudForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.privacy)) this.feelStarSudForm.controls['privacy'].setValue(this.financialValue.object.privacy);
        }
    }

    valideteForm(): boolean {
        this.submitted = true;
        return this.feelStarSudForm.valid;
    }


    getServiceData(): FinancialValue {

        if (this.valideteForm) {
            // this.financialValue.code = 200;

            this.feelStarSudInput = {
                commonFeelStar: {
                    provincia: this.feelStarSudForm.value.provincia,
                    satellitare: this.feelStarSudForm.value.satellitare,
                    privacy: this.feelStarSudForm.value.privacy
                },
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelStarSudInput;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}

