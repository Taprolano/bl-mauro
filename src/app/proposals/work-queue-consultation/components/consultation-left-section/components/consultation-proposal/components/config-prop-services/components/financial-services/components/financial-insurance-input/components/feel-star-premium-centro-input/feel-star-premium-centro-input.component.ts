import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {FinancialConfigServiceResponseInner} from "../../../../../../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner";
import {IPremiumCentro} from "../../../../../../../../../../../../../../data-model/interface/IFinancialInput";
import {FinancialValue} from "../../../../../../../../../../../../../../data-model/class/FinancialValue";
import {LoggerService} from "../../../../../../../../../../../../../../shared/services/logger.service";
import {UtilsService} from "../../../../../../../../../../../../../../shared/services/utils.service";

@Component({
    selector: 'app-feel-star-premium-centro-input',
    templateUrl: './feel-star-premium-centro-input.component.html',
    styleUrls: ['./feel-star-premium-centro-input.component.scss']
})
export class FeelStarPremiumCentroInputComponent implements OnInit {

    @Input()
    service: FinancialConfigServiceResponseInner;

    @Input()
    financialValue: FinancialValue;

    feelStarCentroForm: FormGroup;
    submitted = false;
    feelStarCentroInput: IPremiumCentro;

    constructor(private _formBuilder: FormBuilder, private _logger: LoggerService, private _utils: UtilsService) {
    }

    ngOnInit() {
        this.feelStarCentroForm = this._formBuilder.group({
            provincia: [],
            satellitare: [],
            privacy: []
        });

        if (this.financialValue.object) {
            if (!this._utils.isVoid(this.financialValue.object.provincia)) this.feelStarCentroForm.controls['provincia'].setValue(this.financialValue.object.provincia);
            if (!this._utils.isVoid(this.financialValue.object.satellitare)) this.feelStarCentroForm.controls['satellitare'].setValue(this.financialValue.object.satellitare);
            if (!this._utils.isVoid(this.financialValue.object.privacy)) this.feelStarCentroForm.controls['privacy'].setValue(this.financialValue.object.privacy);
        }
    }

    valideteForm(): boolean {
        this.submitted = true;
        return this.feelStarCentroForm.valid;
    }


    getServiceData(): FinancialValue {

        if (this.valideteForm) {
            // this.financialValue.code = 200;

            this.feelStarCentroInput = {
                commonFeelStar: {
                    provincia: this.feelStarCentroForm.value.provincia,
                    satellitare: this.feelStarCentroForm.value.satellitare,
                    privacy: this.feelStarCentroForm.value.privacy
                },
                common: null
            };

            this.financialValue.message = '';
            this.financialValue.object = this.feelStarCentroInput;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.object);
        } else {
            this.financialValue.code = 500;
            this._logger.logInfo('FeelNewInputComponent', 'getServiceData', this.financialValue.code);
        }
        return this.financialValue;
    }
}
