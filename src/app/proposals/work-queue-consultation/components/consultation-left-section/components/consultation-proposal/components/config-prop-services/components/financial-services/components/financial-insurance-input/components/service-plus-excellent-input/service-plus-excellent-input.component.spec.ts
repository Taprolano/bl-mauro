import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ServicePlusExcellentInputComponent} from './service-plus-excellent-input.component';
import {SvgIconComponent} from '../../../../../../../../../shared/components/svg-icon/svg-icon.component';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '../../../../../../../../../../../node_modules/@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader} from '../../../../../../../../../app.module';
import {FinancialValue} from '../../../../../../../../../data-model/class/FinancialValue';
import {FinancialConfigServiceResponseInner} from '../../../../../../../../../data-model/class/FinancialConfigServiceResponseInner';
import {FireTheftCoverage} from '../../../../../../../../../data-model/class/FireTheftCoverage';

describe('ServicePlusExcellentInputComponent', () => {
    let component: ServicePlusExcellentInputComponent;
    let fixture: ComponentFixture<ServicePlusExcellentInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ServicePlusExcellentInputComponent,
                SvgIconComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ServicePlusExcellentInputComponent);
        component = fixture.componentInstance;

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            annualDistance: builder.control([]),
            valid: builder.control(true)
        });
        component.serviceExcellentForm = mockFormGroup;

        let mockObj = new Object();
        mockObj = {
            kmTotali: []
        };

        let mockFinVal: FinancialValue = new FinancialValue(' ', ' ', 200, 'test', mockObj, ' ', ' ', ' ', ' ', ' ');
        component.financialValue = mockFinVal;

        let mockSerInner: FinancialConfigServiceResponseInner = new FinancialConfigServiceResponseInner();
        let mockFireTheftCoverage: FireTheftCoverage = new FireTheftCoverage(' ', '', 0);
        let mockFireThArray: FireTheftCoverage[] = [];
        mockFireThArray.push(mockFireTheftCoverage);
        mockSerInner.fireTheftCoverage = mockFireThArray;
        component.service = mockSerInner;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('onInit test', () => {
        component.ngOnInit();
    });

    it('validateForm test', () => {
        component.validateForm();
    });

    it('validateForm formFalse test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            annualDistance: builder.control([]),
            valid: builder.control(true)
        });
        component.serviceExcellentForm = mockFormGroup;

        component.validateForm();
    });

    it('getServiceData isValid true test', () => {
        spyOn(component, 'validateForm').and.returnValue(true);
        component.getServiceData();
    });

    it('getServiceData isValid false test', () => {

        let builder: FormBuilder = new FormBuilder();

        let mockFormGroup: FormGroup = builder.group({
            annualDistance: builder.control([]),
            valid: builder.control(false),
        });
        component.serviceExcellentForm = mockFormGroup;

        component.getServiceData();
    });

});
