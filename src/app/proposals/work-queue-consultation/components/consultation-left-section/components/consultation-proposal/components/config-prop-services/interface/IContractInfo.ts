export interface IContractInfo {
    contractLength : number,
    distance : number,
    financiedAmount : number,
    frequency : number,
    tan : number,
    finProductCode : string,
    campainCode : string,
    vatPercentage : number,
    netCost : number,
    totalAmount : number
}
