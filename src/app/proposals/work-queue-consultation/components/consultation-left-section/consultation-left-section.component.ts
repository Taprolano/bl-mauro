import {Component, OnInit, Input} from '@angular/core';
// import {ConsultationProposalProductQueue} from 'src/app/data-model/class/Consultation/ConsultationProposalProductQueue';
// import {ConsultationSubjectQueue} from "../../../../data-model/class/Consultation/ConsultationSubjectQueue";
// import {ConsultationCommercialQueue} from "../../../../data-model/class/Consultation/ConsultationCommercialQueue";
// import {ConsultationFinancialQueue} from '../../../../data-model/class/Consultation/ConsultationFinancialQueue';
import { ConsultationSubjects } from 'src/app/data-model/class/Consultation/Subjects/ConsultationSubjects';

@Component({
    selector: 'app-consultation-left-section',
    templateUrl: './consultation-left-section.component.html',
    styleUrls: ['./consultation-left-section.component.scss']
})

export class ConsultationLeftSectionComponent implements OnInit {

    @Input() selectedTabKey: string;
    @Input() allLeftTabs;
    // @Input() proposalProduct: ConsultationProposalProductQueue;

    // @Input() subjectQueue: ConsultationSubjectQueue;
    // @Input() commQueue: ConsultationCommercialQueue;
    // @Input() financialQueue: ConsultationFinancialQueue;

    @Input() isEditing: boolean;
    @Input() subjectsList: ConsultationSubjects[];

    constructor() {
    }

    ngOnInit() {
    }

}
