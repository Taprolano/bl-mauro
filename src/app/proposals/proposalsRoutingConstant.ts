
import {AppRoutingConstant} from "../appRoutingConstat";
import {register} from "ts-node";

export class ProposalsRoutingConstant {
    public static LIST = 'list';
    public static PRODUCT_MODEL = 'product/model';
    public static PRODUCT_VERSION = 'product/model/version';
    public static COMMERCIAL = 'commercial';
    public static FINANCIAL = 'financial';
    public static REGISTRY = 'registry';
    public static REGISTRY_PROFILES = 'registry_profiles';

    public static path = {
        list:'list',
        workQueue:'work-queue',
        workQueueCreditRetail:'work-queue-credit-retail',
        workQueueCreditCorporate:'work-queue-credit-corporate',
        workQueueDealerContact:'work-queue-dealer-contact',
        workQueuePayout:'work-queue-payout',
        workQueueConsultation:'work-queue-consultation',
        workQueuePropsHistory: 'work-queue-props-history',


        model: 'product/model',
        version: 'product/model/version',
        commercial: 'commercial',
        financial:'financial',
        registry:'registry',
        registry_profiles:'registry/profiles',
        registry_documents:'registry/documents',
        preliminary:'preliminary',
        preliminary_privacy:'preliminary/privacy',
        contract:'contract',
        contract_frame:'contract/frame',
        contract_print:'contract/print',
        finalize:'finalize',
        finalize_payments:'finalize/payments',


    };

    public static fullPath(key: string){
        return AppRoutingConstant.fullPath('proposals')+'/'+this.path[key];
    };

    public static basePath(){
        return AppRoutingConstant.fullPath('proposals');
    };
}