// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    local: false,
    maxLoadingTimeUI: 60000,
    httpTimeout: 60000,
    config: {
        basepath: "/bl/"
    },
    //config: require('../../package.json').config,
    api: {

        user: {
            list: '',
            impersonate: '/bl/impersonate'
        },
        commercial: {
            amounts: '/bdw2serviceAuth/commercial/amounts/',
            optionals: '/bdw2serviceAuth//assets/optionals/',
            chassis: '/bdw2serviceAuth/assets/chassis/'
        },
        financial: {
            products: '/bdw2serviceAuth/financial/products',
            campaigns: '/bdw2serviceAuth/financial/campaigns',
            insurance: '/bdw2serviceAuth/financial/services/',
            configService: '/bdw2serviceAuth/financial/configService/',
            checkCompatibility: '/bdw2serviceAuth/financial/services/checkCompatibility/',
            plans: '/bdw2serviceAuth/financial/plans/',
            service: '/bdw2serviceAuth/financial/service/',
        },
        email: {
            priceQuote: '/bdw2serviceAuth/bdw/sendEmail'
        },
        proposals: {
            queue: {
                credit: '/workflow-service/proposal/codaLavoro',
                wfListOfValues: '/workflow-service/listofvalues',
                numProposalTab: '/workflow-service/proposal/numeroProposalTab',
                numProposalHeader: '/workflow-service/proposal/numeroProposalHeader',
                consultazioneProposta: '/workflow-service/proposal/consultazione/',
                consultazioneSoggetti: '/workflow-service/proposal/subjects/consultazione'
            },
            models: '/bdw2serviceAuth/assets/models',
            version: '/bdw2serviceAuth/assets/models/versions',
            proposal: '/bdw2serviceAuth/proposal',
            counters: '/bdw2serviceAuth/proposal/counters',
            mbcpos: '/bdw2serviceAuth/proposal/mbcpos',
            vdz: '/bdw2serviceAuth/proposal/onlinesales',
            simulation: '/bdw2serviceAuth/financial/simulations',
            partial: '/bdw2serviceAuth/proposal/documentation',
            offers: '/bdw2serviceAuth/financial/dealer/offers',
            priceQuote: '/bdw2serviceAuth/proposal/prospect/print/pdf',
            save: '/bdw2serviceAuth/proposal/save',
            proposalContext: '/bdw2serviceAuth/wf/proposal'
        },
        listOfValues: '/bdw2service/listofvalues/',
        listOfValuesEnhanced: '/bdw2serviceAuth/listofvaluesenhanced',
        me: "/bl/me",
        logout: "/bl/logout",
        document: '/bdw2serviceAuth/document/'
    },
    debuggingProfileInfo: false,
    redirectToNoAuth: "http://testbdweb2.merfina.it/BDWeb2/",
    loggerServiceActive: true
};


/*
* For easier debugging in development mode, you can import the following file
* to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
*
* This import should be commented out in production mode because it will have a negative impact
* on performance if an error is thrown.
*/
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
