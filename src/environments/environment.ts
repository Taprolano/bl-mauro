// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    local: true,
    maxLoadingTimeUI: 60000,
    httpTimeout: 60000,
    config: {
        basepath: "/bdweb2/"
    },
    api: {

        user: {
            list: '',
            impersonate: '/bl/impersonate'
        },
        commercial: {
            amounts: '/bdw2service/commercial/amounts/',
            optionals: '/bdw2service//assets/optionals/',
            chassis: '/bdw2service/assets/chassis/'
        },
        proposals: {
            queue: {
                credit:'/workflow-service/proposal/codaLavoro',
                wfListOfValues: '/workflow-service/listofvalues',
                numProposalTab: '/workflow-service/proposal/numeroProposalTab',
                numProposalHeader: '/workflow-service/proposal/numeroProposalHeader',
                consultazioneProposta: '/workflow-service/proposal/consultazione/',
                consultazioneSoggetti: '/workflow-service/proposal/subjects/consultazione'
            },
            models: '/bdw2service/assets/models',
            version: '/bdw2service/assets/models/versions',
            proposal: '/bdw2service/proposal',
            counters: '/bdw2service/proposal/counters'
        },
        listOfValues: '/bdw2service/listofvalues/',
        me: "/bl/me",
        logout: "/bl/logout"
    },
    debuggingProfileInfo: false,
    redirectToNoAuth: "http://testbdweb2.merfina.it/BDWeb2/",
    loggerServiceActive: true
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
