// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

process.env.CHROME_BIN = require('puppeteer').executablePath();



//old
module.exports = function (config) {
    const defaults = {
        basePath: '',
        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-firefox-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('@angular-devkit/build-angular/plugins/karma')
        ],
        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        coverageIstanbulReporter: {
            dir: require('path').join(__dirname, '../coverage'),
            reports: ['html', 'lcovonly'],
            fixWebpackSourcePaths: true
        },
        angularCli: {
            environment: 'dev'
        },
        files: [
            "../node_modules/jquery/dist/jquery.min.js"
        ],
        reporters: ['progress', 'kjhtml'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        browserDisconnectTimeout: 10000,
        browserDisconnectTolerance: 3,
        browserNoActivityTimeout: 60000,
        flags: [
            '--proxy-bypass-list=*',
            '--disable-web-security',
            '--disable-gpu',
            '--no-sandbox'
        ],
        singleRun: false
    };

    /*  if (process.env.TEST_CI) {
          Object.assign(defaults, {
            autoWatch: false,
            browsers: ['ChromeHeadlessNoSandbox'],
            singleRun: true,
            customLaunchers: {
              ChromeHeadlessNoSandbox: {
                base: 'ChromeHeadless',
                flags: ['--no-sandbox']
              }
            },
            browserNoActivityTimeout: 60000,
          })
      }*/

    if (process.env.TEST_CI) {
        Object.assign(defaults, {
            autoWatch: false,
            browsers: ['ChromeHeadlessNoSandbox'],
            singleRun: true,
            customLaunchers: {
                ChromeHeadlessNoSandbox: {
                    base: 'ChromeHeadless',
                    flags: [
                        '--disable-web-security',
                        '--disable-gpu',
                        '--no-sandbox',
                        '--disable-setuid-sandbox'
                    ]
                }
            },
            browserNoActivityTimeout: 60000
        })
    }

    config.set(defaults)
};

/*
'use strict';

var path = require('path');
var conf = require('./gulp/conf');

var _ = require('lodash');
var wiredep = require('wiredep');

var pathSrcHtml = [
    path.join(conf.paths.src, '/!**!/!*.html'),
    path.join(conf.paths.src_test, '/!**!/!*.html')
];

function listFiles() {
    var wiredepOptions = _.extend({}, conf.wiredep, {
        dependencies: true,
        devDependencies: true
    });

    return wiredep(wiredepOptions).js
        .concat([
            path.join(conf.paths.src, '/app/!**!/!*.module.js'),
            path.join(conf.paths.src, '/app/!**!/!*.js'),
            path.join(conf.paths.src, '/!**!/!*.spec.js'),
            path.join(conf.paths.src, '/!**!/!*.mock.js'),
            path.join(conf.paths.src_test, '/app/!**!/!*.module.js'),
            path.join(conf.paths.src_test, '/app/!**!/!*.js'),
            path.join(conf.paths.src_test, '/!**!/!*.spec.js'),
            path.join(conf.paths.src_test, '/!**!/!*.mock.js')
        ])
        .concat(pathSrcHtml);
}

module.exports = function(config) {

    var configuration = {
        files: listFiles(),

        singleRun: true,

        colors:    false,

        autoWatch: false,

        ngHtml2JsPreprocessor: {
            stripPrefix: conf.paths.src + '/',
            moduleName: 'TODO_PUT_HERE_YOUR_MODULE_NAME'
        },

        logLevel: 'WARN',

        frameworks: ['jasmine', 'angular-filesort'],

        angularFilesort: {
            whitelist: [path.join(conf.paths.src, '/!**!/!(*.html|*.spec|*.mock).js'), path.join(conf.paths.src_test, '/!**!/!(*.html|*.spec|*.mock).js')]
        },

        browsers: ['PhantomJS'],

        sonarQubeUnitReporter: {
            sonarQubeVersion: 'LATEST',
            outputFile: 'reports/ut_report.xml',
            useBrowserName: false
        },

        plugins: [
            'karma-phantomjs-launcher',
            'karma-angular-filesort',
            'karma-coverage',
            'karma-jasmine',
            'karma-ng-html2js-preprocessor',
            'karma-sonarqube-unit-reporter'
        ],

        coverageReporter: {
            type : 'lcov',
            dir : 'reports',
            subdir : 'coverage'
        },

        reporters: ['progress', 'sonarqubeUnit', 'coverage'],

        preprocessors: {
            'src/!**!/!*.js':   ['coverage'],
            'test/!**!/!*.js':   ['coverage']
        }
    };

    config.set(configuration);
};
*/



// karma.conf.js
/*
module.exports = function(config) {
    config.set({
        reporters: ['progress', 'junit'],

        // the default configuration
        junitReporter: {
            outputDir: '../reports/junit', // results will be saved as $outputDir/$browserName.xml
            outputFile: 'report', // if included, results will be saved as $outputDir/$browserName/$outputFile
            useBrowserName: false // add browser name to report and classes names
        }
    }
)
};
*/


// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

/*
module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('karma-sonarqube-unit-reporter'),
            require('karma-junit-reporter'),
            require('@angular-devkit/build-angular/plugins/karma')
        ],
        junitReporter: {
            outputDir: '../../../target/surefire-reports/', // results will be saved as $outputDir/$browserName.xml
            outputFile: undefined, // if included, results will be saved as $outputDir/$browserName/$outputFile
            suite: '', // suite will become the package name attribute in xml testsuite element
            useBrowserName: true, // add browser name to report and classes names
            nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
            classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
            properties: {} // key value pair of properties to add to the <properties> section of the report
        },
        sonarQubeUnitReporter: {
            sonarQubeVersion: 'LATEST',
            outputFile: '../../../target/reports/ut-report.xml',
            useBrowserName: false,
            overrideTestDescription: true,
            testPath: './src/main/typescript',
            testFilePattern: '.spec.ts'
        },
        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        coverageIstanbulReporter: {
            reports: ['html', 'lcovonly'],
            fixWebpackSourcePaths: true,
            dir: 'target/coverage/'
        },
        angularCli: {
            environment: 'dev'
        },
        reporters: ['progress', 'kjhtml', 'junit', 'sonarqubeUnit'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['ChromeHeadless', 'Chrome'],
        singleRun: false,
        concurrency: 1,
        browserNoActivityTimeout: 60000
    });
};*/
