import { TableData } from "./TableData";
import { LoggerService } from "src/app/shared/services/logger.service";
import { UtilsService } from "src/app/shared/services/utils.service";
import { utils } from "protractor";


export class TdProposalEntry extends TableData {

  constructor(cardinality: number, private _logger: LoggerService) {
    super(cardinality);
  }

  private _tableDetailProposalEntry;

  public static SINGLE_PROPOSAL_ENTRY = {
    asset: 1,
    proposalId: 1002007450,
    contract: 2880220,
    subject: "NameSubject",
    proposalState: 0,
    startDate: "01-12-2016",
    expirationDate: "01-12-2017",
    priority: "C",
    seller: "NameSeller",
    messageIcon: true,
    documentIcon: true
  }

  fillContent(): any {

    try {
      this._tableDetailProposalEntry = [];

      for(let i = 0; i < this.cardinality; i++){
        let singleProposalEntry = {...TdProposalEntry.SINGLE_PROPOSAL_ENTRY};
        singleProposalEntry.asset += i;
        singleProposalEntry.proposalId += i;
        singleProposalEntry.contract += i;
        singleProposalEntry.subject += i.toString();
        singleProposalEntry.proposalState += i;
        singleProposalEntry.seller += i.toString();

        this._tableDetailProposalEntry.push(singleProposalEntry)
      }
    }
    catch(e) {
      this._logger.logInfo('TdProposalEntry','fillContent', "Exception: [" + e + "]" );
      //console.log("[TdProposalEntry][fillContent] Exception: [" + e + "]");
    }

    return this._tableDetailProposalEntry;
  }
}