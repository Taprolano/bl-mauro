import {TableData} from "./TableData";
import { LoggerService } from "src/app/shared/services/logger.service";


export class GdAutoModel extends TableData{

  constructor(cardinality: number, private _logger: LoggerService) {
    super(cardinality);
  }

  private _gridDetailAutoElement;

  public static SINGLE_AUTO_ELEMENT = {
    imageAuto: "img",
    modelName: "auto",
    modelSelectPage: "https://www.google.com",
    printPromotion: ""
  }

  fillContent(): any {
    try {
      this._gridDetailAutoElement = [];

      for(let i = 0; i < this.cardinality; i++){
        let singleAutoElement = {...GdAutoModel.SINGLE_AUTO_ELEMENT};
        if(i==0){
          singleAutoElement.printPromotion = "https://www.google.com/gmail/"
        }
        singleAutoElement.imageAuto +=  (1+i).toString();
        singleAutoElement.modelName += (1+i).toString();

        this._gridDetailAutoElement.push(singleAutoElement)
      }
    }
    catch(e) {
      this._logger.logInfo('GdAutoModel','fillContent', "Exception: [" + e + "]");
      //console.log("[GdAutoModel][fillContent] Exception: [" + e + "]");
    }

    return this._gridDetailAutoElement;
  }
} 