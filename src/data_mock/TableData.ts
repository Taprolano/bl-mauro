import {UtilsService} from "../app/shared/services/utils.service";

export abstract class TableData {
  private _cardinality;
  _utils: UtilsService = new UtilsService();


  constructor (card: number) {
      this._cardinality  = card;
  };


  get cardinality() {
      return this._cardinality;
  }

  set cardinality(value) {
      this._cardinality = value;
  }

  public clone(obj:any): any {
      return this._utils.assign(obj);
  }

  abstract fillContent(list: any) :any;

}