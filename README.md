# Mercedes BL

Simple Dashboard  App built using Angular 6 and Bootstrap 4


## [Demo](http://rawgit.com/start-angular/SB-Admin-BS4-Angular-7/master/dist/)


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.9.

### Introduction

Tree structure
```
Mercedes BL
├── CHANGELOG.md
├── LICENSE.md
├── README.md
├── angular-cli.json
├── e2e
├── karma.conf.js
├── package.json
├── protractor.conf.js
├── src
│   ├── app
│   │   ├── app.component.css
│   │   ├── app.component.html
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   ├── app.module.ts
│   │   ├── app.routing.ts
│   │   ├── layout
│   │   │   ├── components
│   │   │   │    ├── header
│   │   │   │    │   ├── header.component.css
│   │   │   │    │   ├── header.component.html
│   │   │   │    │   ├── header.component.spec.ts
│   │   │   │    │   └── header.component.ts
│   │   │   │    └── sidebar
│   │   │   │        ├── sidebar.component.css
│   │   │   │        ├── sidebar.component.html
│   │   │   │        ├── sidebar.component.spec.ts
│   │   │   │        └── sidebar.component.ts
│   │   │   │
│   │   │   ├── blank-page
│   │   │   │    ├── blank-page.component.html
│   │   │   │    ├── blank-page.component.scss
│   │   │   │    ├── blank-page.component.spec.ts
│   │   │   │    ├── blank-page.component.ts
│   │   │   │    ├── blank-page.module.spec.ts
│   │   │   │    ├── blank-page.module.spec.ts
│   │   │   │    ├── blank-page.module.ts
│   │   │   │    └── blank-page-routing.module.ts
│   │   │   │            
│   │   │   ├── home
│   │   │   │    ├── home.component.html           
│   │   │   │    ├── home.component.scss           
│   │   │   │    ├── home.component.spec.ts           
│   │   │   │    ├── home.component.ts                
│   │   │   │    │     
│   │   │   │    ├── box-observable-example    
│   │   │   │    │    ├── box-observable-example.component.html   
│   │   │   │    │    ├── box-observable-example.component.scss   
│   │   │   │    │    ├── box-observable-example.component.spec.ts   
│   │   │   │    │    └── box-observable-example.component.ts  
│   │   │   │    │
│   │   │   │    ├── box-parent-to-child-example 
│   │   │   │    │    ├── box-parent-to-child-example.component.html   
│   │   │   │    │    ├── box-parent-to-child-example.component.scss    
│   │   │   │    │    ├── box-parent-to-child-example.component.spec.ts    
│   │   │   │    │    └── box-parent-to-child-example.component.ts   
│   │   │   │    │
│   │   │   │    ├── box-child-to-parent-example 
│   │   │   │    │    ├── box-child-to-parent-example.component.html   
│   │   │   │    │    ├── box-child-to-parent-example.component.scss   
│   │   │   │    │    ├── box-child-to-parent-example.component.spec.ts   
│   │   │   │    │    └── box-child-to-parent-example.component.ts  
│   │   │   │    │
│   │   │   │    ├── grid-group
│   │   │   │    │    ├── grid-group.component.html
│   │   │   │    │    ├── grid-group.component.scss
│   │   │   │    │    ├── grid-group.component.spec.ts
│   │   │   │    │    ├── grid-group.component.ts
│   │   │   │    │    └── grid-auto-element
│   │   │   │    │          ├── grid-auto-element.component.html
│   │   │   │    │          ├── grid-auto-element.component.scss
│   │   │   │    │          ├── grid-auto-element.component.spec.ts
│   │   │   │    │          └── grid-auto-element.component.ts
│   │   │   │    │ 
│   │   │   │    ├── table-group
│   │   │   │    │    ├── table-group.component.html
│   │   │   │    │    ├── table-group.component.scss
│   │   │   │    │    ├── table-group.component.spec.ts
│   │   │   │    │    └── table-group.component.ts
│   │   │   │
│   │   │   ├── layout-routing.module.ts
│   │   │   ├── layout.component.html
│   │   │   ├── layout.component.scss
│   │   │   ├── layout.component.spec.ts
│   │   │   ├── layout.component.ts
│   │   │   ├── layout.module.spec.ts
│   │   │   └── layout.module.ts





│   │   ├── dashboard
│   │   │   ├── dashboard.component.css
│   │   │   ├── dashboard.component.html
│   │   │   ├── dashboard.component.spec.ts
│   │   │   └── dashboard.component.ts
│   │   ├── icons
│   │   │   ├── icons.component.css
│   │   │   ├── icons.component.html
│   │   │   ├── icons.component.spec.ts
│   │   │   └── icons.component.ts
│   │   ├── layouts
│   │   │   └── admin-layout
│   │   │       ├── admin-layout.component.html
│   │   │       ├── admin-layout.component.scss
│   │   │       ├── admin-layout.component.spec.ts
│   │   │       ├── admin-layout.component.ts
│   │   │       ├── admin-layout.module.ts
│   │   │       └── admin-layout.routing.ts
│   │   ├── maps
│   │   │   ├── maps.component.css
│   │   │   ├── maps.component.html
│   │   │   ├── maps.component.spec.ts
│   │   │   └── maps.component.ts
│   │   ├── notifications
│   │   │   ├── notifications.component.css
│   │   │   ├── notifications.component.html
│   │   │   ├── notifications.component.spec.ts
│   │   │   └── notifications.component.ts
│   │   │
│   │   ├── shared
│   │   │   └── services
│   │   │        ├── api-mock.services.spec.ts
│   │   │        ├── api-mock.services.ts
│   │   │        ├──
│   │   │        
│   │   ├── table-list
│   │   │   ├── table-list.component.css
│   │   │   ├── table-list.component.html
│   │   │   ├── table-list.component.spec.ts
│   │   │   └── table-list.component.ts
│   │   ├── typography
│   │   │   ├── typography.component.css
│   │   │   ├── typography.component.html
│   │   │   ├── typography.component.spec.ts
│   │   │   └── typography.component.ts
│   │   └── user-profile
│   │       ├── user-profile.component.css
│   │       ├── user-profile.component.html
│   │       ├── user-profile.component.spec.ts
│   │       └── user-profile.component.ts
│   ├── assets
│   │   ├── i18n
│   │   ├── fonts
│   │   └── images
│   │
│   ├── data_mock
│   │   ├── GdAutoModel.ts
│   │   ├── GridData.ts
│   │   ├── TableData.ts
│   │   └── TdProposalEntry.ts
│   │
│   ├── environments
│   │    ├── environments.ts
│   │    └── user-profile.component.ts
│   ├── styles
│   │   ├── bootstrap
│   │   ├── _responsive.scss
│   │   ├── ...
│   │   └── app.scss
│   ├── favicon.ico
│   ├── index.html
│   ├── main.ts
│   ├── polyfills.ts
│   ├── styles.css
│   ├── test.ts
│   ├── tsconfig.app.json
│   ├── tsconfig.spec.json
│   └── typings.d.ts
├── tsconfig.json
├── tslint.json
└── typings
```


### How to start

**Note** that this seed project requires **node >=v8.9.0 and npm >=4**.

In order to start the project use:

# install the project's dependencies
```
$ npm install
watches your files and uses livereload by default run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
$ npm start
# prod build, will output the production application in `dist`
# the produced code can be deployed (rsynced) to a remote server
$ npm run build
```

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Karma/jasmine spiritual guide

Test are defined inside file with extension spec.ts
It's possible to define a test for each component.
Import all the angular testing tools that are going to be used. (TestBed, async etc.)
Import all the dependencies that this component has.

NOTE: inside "beforeEach" where are defined sections like "imports", "declarations", if an imported module is specified inside "imports" section, the components related to this module must
not be specified in "declarations" otherwise an error occurred and the test fails.

We use a “describe” to start our test block with the title matching the tested component name.
('fdescribe' to execute only the tests that use 'fdescribe')
We use an async before each. The purpose of the async is to let all the possible asynchronous code to finish before continuing.

- How to test a component that uses a service with an Observable

To test a component that calls a service it's necessary to create a mock (for example inside the spec.ts) and subscribe to this one, to create a mock can be usefoul to use method 'of' defined in rxjs.

- How to test a child component with communication parent -> child

To test this kind of component, in which the parent has an @input property,
after using TestBed.createComponent you have to call the 
nativeElement.querySelector(<tag-selector>).innerText if the property is binded inside it. 

- How to test a child component with communication child -> parent

To test this kind of component, in which the child communicates with the parent through event propagation, it's possible to use spyOn(component.eventEmitter, 'emit') after must be called 
the function that emits the value and finally
expect(component.eventEmitter.emit).toHaveBeenCalledWith(valueEmitted);

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
