// var proxy = require('redbird')({port: 80});

// OPTIONAL: Setup your proxy but disable the X-Forwarded-For header
var proxy = require('redbird')({port: 8006, xfwd: false});

// Route to any global ip
/*proxy.register({
    "protocol":"http:",
    "slashes":true,
    "auth":null,
    "host":"localhost",
    "port":"8080",
    "hostname":"localhost",
    "hash":null,
    "search":null,
    "query":null,
    "pathname":"/",
    "path":"/",
    "href":"http://localhost:8080/"
}, "bl2-bdwbe.dv4.space/bdw2service");*/

// Route to any local ip, for example from docker containers.
proxy.register("localhost:8080/bdw2service/", "http://bl2-bdwbe.dv4.space/bdw2service/");

proxy.register("oz.com:8006/bdw2service", "http://10.166.22.21/bdw2service");
proxy.register("oz.com:8006/bdw2service", "http://be.com/bdw2service/bdw2service/proposal?page=1&size=10");
proxy.register("oz.com:8006/bdw2service/", "http://bl2-bdwbe.dv4.space/bdw2service/proposal?page=1&size=10");
proxy.register("oz.com:8006/bdw2service", "http://be.com/bdw2service/bdw2service/proposal?page=1&size=10");

// Route from hostnames as well as paths
// proxy.register("example.com/static", "http://172.17.42.1:8002");
// proxy.register("example.com/media", "http://172.17.42.1:8003");
