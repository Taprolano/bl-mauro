# CHANGELOG Mercedes BDWEB


<a name="0.0.1"></a>

# [0.0.1](LINK_URL) (2019-03-30)

### Features

- feat(app): documentation coverage for files with lint-staged tools ([HASH](LINK_HASH)), closes [#NUM_ISSUE](LINK_ISSUE)

### Merged

- fix(UI): a few issues with the src on customLogo img [#NUM](LINK_NUM) Thanks [AUTHOR](URL_AUTHOR)
- fix: use consistent semver range specifiers [#NUM](LINK_NUM) Thanks [AUTHOR](URL_AUTHOR)

### Bug fixes

- fix(app): text ([8332344](https://github.com/compodoc/compodoc/commit/8332344)), closes [#NUM_ISSUE](LINK_ISSUE)


<a name="0.0.1"></a>