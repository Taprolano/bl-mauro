var gulp = require('gulp');
var sonarqubeScanner = require('sonarqube-scanner');

gulp.task('sonar-analysis', function(callback) {
    sonarqubeScanner({
        serverUrl : "http://bl2-sonarqube.dv4.space",
        token : "2ce39be767ebfa8c1772ac09082a34701b9dde4a",
        options : {
            "sonar.projectKey": "bl-fe",
            "sonar.sources": "src",
            "sonar.exclusions": "src/assets/**/*, src/styles/**/*, src/**/*.html, src/**/*.scss, src/environments/*, + " +
                                "src/data_mock/*, src/app/shared/services/mock-data.service.ts," +
                                "src/app/proposals/work-queue/credit-corporate/*," +
                                "src/app/proposals/work-queue/dealer-contact/*," +
                                "src/app/proposals/work-queue/payout/*,",
            "sonar.tests":"src",
            "sonar.test.inclusions":"**/*.spec.ts",
            "sonar.typescript.lcov.reportPaths":"coverage/lcov.info"
        }
    }, callback);
});


gulp.task('sonar-analysis-qa', function(callback) {
    sonarqubeScanner({
        serverUrl : "http://bl2-sonarqube.dv4.space",
        token : "2ce39be767ebfa8c1772ac09082a34701b9dde4a",
        options : {
            "sonar.projectKey": "bl-fe",
            "sonar.sources": "src",
            "sonar.exclusions": "src/assets/**/*, src/styles/**/*, src/**/*.html, src/**/*.scss, src/environments/*, + " +
            "src/data_mock/*, src/app/shared/services/mock-data.service.ts," +
            "src/app/proposals/work-queue/credit-corporate/*," +
            "src/app/proposals/work-queue/dealer-contact/*," +
            "src/app/proposals/work-queue/payout/*,",
            "sonar.tests":"src",
            "sonar.test.inclusions":"**/*.spec.ts",
            "sonar.typescript.lcov.reportPaths":"coverage/lcov.info"
        }
    }, callback);
});

// sonar-scanner \
//   -Dsonar.projectKey=bdweb-fe \
//   -Dsonar.sources=. \
//   -Dsonar.host.url=http://bl2-sonarqube.dv4.space \
// -Dsonar.login=e20997cb15d2bd488d87fcbb51f331dae923a3de